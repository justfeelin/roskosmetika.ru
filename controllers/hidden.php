<?php

class Controller_hidden extends Controller_Base
{
    /**
     * Noop action
     *
     * @param $arg
     */
    public function index($arg)
    {
        $this->nopage();
    }

    /**
     * Renders email template
     */
    public function email()
    {
        if ($this->_checkAccess()) {
            ob_start();

            set_error_handler(function ($errorNumber, $errorMessage) {
                throw new Exception($errorMessage);
            });

            $requestData = $this->_requestData();

            try {
                if (!empty($requestData['template'])) {
                    $data = json_decode($requestData['data'], true);

                    if (!$data || !is_array($data)) {
                        throw new Exception($data ? 'JSON data not of array type' : 'JSON data not parsed');
                    }

                    print Template::mail_template('mail_' . $requestData['template'], isset($requestData['header']) ? $requestData['header'] : '', $data);
                } else {
                    print Template::mail_template('mail_text', isset($requestData['header']) ? $requestData['header'] : '', $requestData['content']);
                }
            } catch (Exception $error) {
                ob_end_clean();

                header('HTTP/1.1 400 Bad request');
                header('X-ERROR: ' . $error->getMessage());

                if (DEBUG) {
                    print $error->getMessage();
                }

                exit(1);
            }

            ob_end_flush();

            exit(0);
        }
    }

    /**
     * Returns $_GET when debug mode, $_POST for production
     */
    private function _requestData()
    {
        return DEBUG ? $_GET : $_POST;
    }

    /**
     * Checks for access
     *
     * @return bool
     */
    private function _checkAccess()
    {
        $data = $this->_requestData();

        if (
            empty($data['k'])
            || $data['k'] !== env('HIDDEN_SECRET')
        ) {
            $this->nopage();

            return false;
        }

        return true;
    }

    /**
     * Outputs order info
     */
    public function calculate_order()
    {
        if ($this->_checkAccess()) {
            set_error_handler(function ($errorNumber, $errorMessage) {
                throw new Exception($errorMessage);
            });

            $requestData = $this->_requestData();

            try {
                $data = json_decode($requestData['data'], true);

                if (!$data || !is_array($data)) {
                    throw new Exception($data ? 'JSON data not of array type' : 'JSON data not parsed');
                }

                $result = Cart::orderCost(
                    $data['products'],
                    $data['clientAutoDiscount'],
                    $data['clientMainDiscount'],
                    $data['promoID'],
                    !empty($data['admitadPromo']),
                    isset($data['shipping']) ? $data['shipping'] : null,
                    isset($data['shippingTypeID']) ? $data['shippingTypeID'] : null,
                    isset($data['regionID']) ? $data['regionID'] : null,
                    false,
                    isset($data['referralDiscount']) ? $data['referralDiscount'] : 0,
                    isset($data['ownReferralDiscount']) ? $data['ownReferralDiscount'] : 0,
                    empty($data['noDiscount'])
                );

                $result['discountCode'] = $result['discountCode'] ?: '';
                $result['discountName'] = $result['discountName'] ?: '';
                $result['present'] = $result['present'] ?: 0;
            } catch (Exception $error) {
                $result = [
                    'errorMessage' => $error->getMessage(),
                ];
            }

            $this->jsonResponse($result);
        }
    }

    /**
     * Sets site_order_number for order, outputs it
     */
    public function set_order_site_number()
    {
        if ($this->_checkAccess()) {
            $requestData = $this->_requestData();

            if (!empty($requestData['id'])) {
                $randomID = Orders::random_id();

                if (!Orders::set_site_order_number($requestData['id'], $randomID)) {
                    $randomID = 0;
                }
            } else {
                $randomID = 0;
            }

            $this->jsonResponse($randomID);
        }
    }
    /**
     * Sending sms through Postiko service
     */

    public function postiko_send_sms()
    {
        if ($this->_checkAccess()) {
            $sms_answer=0;
            $requestData = $this->_requestData();

            if (!empty($requestData['phone']) and !empty($requestData['message'] )) {
                $sms_answer = sms::send_sms($requestData['phone'],$requestData['message']);
            }

            $this->jsonResponse($sms_answer);
        }
    }
    /**
     * Walletone invoice email generator
     */
    public function payment_invoice()
    {
        if ($this->_checkAccess()) {
            $requestData = $this->_requestData();

            try {
                $order = Orders::get_by_id($requestData['order_number']);
                $siteOrderNumber = $order['site_order_number'];
                $orderNumber = $requestData['order_number'];
                $cost = $requestData['order_cost'];
                $email = $requestData['email'];
                $fio = $requestData['fio'];

                $sberbank = !empty($requestData['sberbank']);

                $invoiceHash = $sberbank ? Payment::create_sberbank_invoice($orderNumber, $siteOrderNumber, $cost) : Payment::create_invoice($orderNumber, $cost);

                if ($sberbank) {
                    $invoiceHash = $invoiceHash['sberbank_id'];
                }

                $html = Template::mail_template(
                    'mail_payment_invoice',
                    'Счёт на оплату Роскосметика',
                    [
                        'hash' => $invoiceHash,
                        'name' => $fio,
                        'cost' => $cost,
                        'site_order_number' => $siteOrderNumber,
                        'sberbank' => $sberbank,
                    ]
                );

                mailer::mail(
                    'Счёт на оплату Роскосметика',
                    [
                        'sales@roskosmetika.ru' => 'Роскосметика',
                    ],
                    DEBUG ? [
                        env('ADMIN_EMAIL') => env('ADMIN_NAME'),
                    ] : [
                        $email => $fio,
                    ],
                    $html
                );

                if (!DEBUG) {
                    mailer::mail(
                        'CRM обработано. Автокопия счёта ' . ($sberbank ? 'СЭ' : 'W1') . ' на ' . $email . ' (Заказ Р-' . $order['site_order_number'] . ')',
                        [
                            'sales@roskosmetika.ru' => 'Роскосметика',
                        ],
                        [
                            '2255483@mail.ru' => 'Роскосметика',
                        ],
                        $html
                    );
                }

                $result = 1;
            } catch (Exception $e) {
                $result = 0;
            }

            $this->jsonResponse($result);
        }
    }

    /**
     * New order creation page
     *
     * Required keys: "name", "products" (URL-encoded)
     * Optional keys: "surname", "patronymic", "email", "phone", "client_id", "note", "address", "agent_id", "order_number" (If "order_number" specified - will output incremental order number too), "auto_order" (If "auto_order" specified - will mark order as automatically generated)
     */
    public function order()
    {
        if ($this->_checkAccess()) {
            try {
                $data = $this->_requestData();

                list($orderID, $siteOrderNumber) = Orders::insert_order(
                    $data['name'],
                    !empty($data['surname']) ? $data['surname'] : '',
                    !empty($data['patronymic']) ? $data['patronymic'] : '',
                    !empty($data['email']) ? $data['email'] : '',
                    !empty($data['phone']) ? $data['phone'] : '',
                    !empty($data['client_id']) ? $data['client_id'] : 0,
                    $data['products'],
                    !empty($data['note']) ? $data['note'] : '',
                    0,
                    !empty($data['address']) ? $data['address'] : '',
                    null,
                    null,
                    null,
                    !empty($data['agent_id']) ? $data['agent_id'] : null,
                    !empty($data['auto_order'])
                );

                $result = [
                    'site_order_number' => $siteOrderNumber,
                ];

                if (isset($data['order_number'])) {
                    $result['order_number'] = $orderID;
                }
            } catch (Exception $error) {
                $result = [
                    'errorMessage' => $error->getMessage(),
                ];
            }

            $this->jsonResponse($result);
        }
    }

    /**
     * Checks sberbank payments for order
     *
     * Required key: "order_number"
     */
    public function check_sberbank_payment()
    {
        if ($this->_checkAccess()) {
            try {
                $data = $this->_requestData();

                $invoices = Payment::get_sberbank_invoices_by_order_number($data['order_number'], false);

                for ($i = 0, $n = count($invoices); $i < $n; ++$i) {
                    Payment::check_sberbank_paid($invoices[$i]['sberbank_id'], true);
                }

                $result = 1;
            } catch (Exception $error) {
                $result = 0;
            }

            $this->jsonResponse($result);
        }
    }


    /**
     * view clients IP
     */
    public function get_user_ip()
    {
        if ($this->_checkAccess()) {

            $ip = '';

            if (isset($_SERVER['HTTP_CLIENT_IP']))
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_X_FORWARDED']))
                $ip = $_SERVER['HTTP_X_FORWARDED'];
            else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
                $ip = $_SERVER['HTTP_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_FORWARDED']))
                $ip = $_SERVER['HTTP_FORWARDED'];
            else if(isset($_SERVER['REMOTE_ADDR']))
                $ip = $_SERVER['REMOTE_ADDR'];
            else
                $ip = 'NO';
         
             printf($ip);
             exit;

        }
    }

    /**
     * Add email to base
     */
    public function add_email()
    {
        if ($this->_checkAccess()) {
            $requestData = $this->_requestData();

            if (!empty($requestData['email']) && check::email($requestData['email'])) {
               
                unsubscribe::add_self($requestData['email'], 'popup', 'roskosmetika.ru');

                $result = 1;

            } else {
                $result = 0;
            }

            $this->jsonResponse($result);
        }
    }


    /**
     * Return orders info
     */
    public function get_orders()
    {
        $result = 0; 

        if ($this->_checkAccess()) {
            $requestData = $this->_requestData();

            if (isset($requestData['orders'])) {

               preg_match_all('/\d{1,10}/', $requestData['orders'], $res);
               $orders = implode(',', $res[0]);

               if ($orders != '') {

                    $result = array('orders' => array());

                    DB::get_rows("SELECT o.order_number AS id, IF(s.ballance_type = 'sold', 'Y', IF(s.ballance_type = 'no', 'C', 'N')) AS status, o.cost AS summ, 0 AS goods
                                  FROM orders o, status s
                                  WHERE o.status_id = s.id
                                  AND o.order_number IN ($orders)",
                                  function ($order) use (&$result) {

                                        $order['goods'] = DB::get_rows('SELECT oi.product_id AS id, oi.quantity, oi.final_price AS price FROM orders_items oi WHERE oi.order_number = ' . $order['id']);
                                        $result['orders'][] = $order;

                                    }
                    );

               }
            }

            $this->jsonResponse($result);
        }
    }   
}