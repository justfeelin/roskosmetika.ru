<?php

/**
 * Working with reviews
 */
class Controller_Review extends Controller_base
{
    //  page geader
    const PAGE_HEADER = 'Роскосметика отзывы';
    //  quantity reviews on one page
    const STEP = 15;

    function index($args)
    {

        $page_num = 1;

        //  check $args        
        if (isset($args[0])) {
            $page_num = (int)$args[0];
            if ($page_num == 1) {
                header('HTTP/1.1 301 Moved Permanently');
                header("Location: /review");
            }
        }

        $quantity = (int)ceil(count(review::get_list(1, 0)) / self::STEP);

        if ($page_num && is_numeric($page_num) && $page_num <= $quantity) {
            //  set vars for template
            Template::set('all_comments', review::get_list(1, $page_num));
            Template::set('qantity_pages', $quantity);
            Template::set('bringing_date', '%d.%m.%Y');
            Template::set('current_page', $page_num);
            Template::set('crumb', 'Отзывы о магазине roskosmetika.ru');

            $id_list = agregation_products::get_personal_product_list(helpers::search_user_id(), 1, 4);

            //Template::set('mb_top_view', product::get_by_id(mindbox::get_recomendation('PoProsmotram4', FALSE, 4), true, true, false, false, false));
            Template::set('mb_top_view', product::get_by_id($id_list, true, true, false, false, false));

            Template::set_description(self::PAGE_HEADER . ($page_num > 1 ? ' - ' . $page_num . ' страница' : ''), '', '');

            if (auth::is_auth()) {
                //  autorizated client
                Template::set('auth', 1);

                if (isset($_POST['submit'])) {


                    $comment = $_POST['comment'];

                    if (Check::check_fullness($comment)) {
                        $msg['comment'] = 'Введите текст отзыва:';
                        $values['comment'] = '';
                    } else {

                        $result = Check::check_lengh_tags($comment);

                        $values['comment'] = $result['0'];
                        $msg['comment'] = $result['1'];

                        if ($result['2'] == TRUE) {

                            // manager's email
                            $to = DEBUG ? array(
                                env('ADMIN_EMAIL') => 'Технический отдел (DEBUG)',
                            ) : array('roskosmetika_it@mail.ru' => 'Технический отдел',
                                'roskosmetika_content@mail.ru' => 'Копирайтер');


                            $from = array('sales@roskosmetika.ru' => 'Роскосметика');
                            $subject = 'Отзыв Роскосметика';
                            $body = $this->get_msg_to_mng(Users::get_user_info(), $values['comment']);

                            @mailer::mail($subject, $from, $to, $body);

                            if (review::add_comment($values['comment'])) {
                                $text = 'Ваш комментарий будет опубликован после модерации. Спасибо за участие!';

                            } else {
                                $text = 'Приносим наши извинения, но в данный момент невозможно оставить отзыв.';
                            }

                            Template::set('text', $text);
                            Template::set_page('review', self::PAGE_HEADER);
                        }
                    }
                    Template::set_form($values, $msg);
                    Template::set_page('review', self::PAGE_HEADER);

                } else {
                    Template::set_page('review', self::PAGE_HEADER);
                }
            } else {
                // not autorizated
                Template::set('auth', 0);
                Template::set_page('review', self::PAGE_HEADER);
            }
        } else {

            $this->nopage();

        }

    }


    /**
     * Generate msg to managers
     * @param  string $client_info client information
     * @param  string $comment comment about shop
     * @return string                   html msg
     */
    protected function get_msg_to_mng($client_info, $comment)
    {
        $text = "<html>
                    <head>
                        <title>Отзыв Роскосметика</title>
                    </head>
                    <body>
                        <p><b>{$client_info['surname']} {$client_info['name']} </b> оставил(а) отзыв:</p>
                        <p>$comment</p>
                        <p><b>Контакты:</b></p>
                        <p>{$client_info['email']}</p>
                        <p>{$client_info['phone']}</p>
                    </body>
               </html>";
        return $text;
    }

}

?>
