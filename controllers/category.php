<?php

class Controller_category extends Controller_Base
{

    /**
     * Products of categories on lvl 1,2,3.
     * @param array $args URL Parameters
     */
    function index($args)
    {
        if (!isset($args[0])) {
            Template::set_page('products_page', 'Разделы', [
                'products' => Product::get_popular_pseudo_random(40, 'category-page', 31536000),
                'crumbs' => [
                    [
                        'name' => 'Разделы',
                    ],
                ],
            ]);

            return;
        }

        $category = categories::parse_url($args);

        if (!$category) {
            $this->nopage(AJAX_REQUEST);

            return;
        }

        if ($data = categories::client_output($category['ids'][0], $category['ids'][1], $category['ids'][2], true, AJAX_REQUEST)) {
            Pagination::setBaseUrl('/category/' . ($category['urls'][0] . ($category['urls'][1] ? ('/' . $category['urls'][1] . ($category['urls'][2] ? '/' . $category['urls'][2] : '')) : '')));
            Pagination::setTemplateSettings();

            if (AJAX_REQUEST) {
                Template::set('products', $data['products']);

                $this->jsonResponse(array(
                    'found' => true,
                    'products' => Template::get_tpl('product_list'),
                    'pagination' => Template::get_tpl('pagination'),
                    'count' => Pagination::getCount(),
                    'filters' => $data['filters'],
                    'tms' => $data['filter_tms'],
                    'prices' => $data['filter_prices'],
                    'countries' => $data['filter_countries'],
                    'show_desc' => $data['show_desc'],
                ));
            } else {
                mindbox::add_action('performOperation', 'ViewCategory', [
                    'data' => [
                        'action' => [
                            'productCategoryId' => $data['main']['id'],
                        ],
                    ],
                ]);
            }

            if (!$data['products']) {
                $similar = Search::remote_search($data['main']['name'], 'categories', 10, null, false);

                if (!$similar && $category['ids'][1]) {
                    $similar = array((int)$data['bread_' . ($category['ids'][2] ? '2' : '1')]['id']);
                }

                $data['similar'] = $similar ? Categories::get_by_id($similar) : null;
            }

            Template::add_admitad_code(Admitad::category_page($category['ids'][0], $category['ids'][1], $category['ids'][2]));
            //Template::add_addigital_code(Addigital::category_page());
            //Template::add_rocket_code(retail_rocket::category_page($category['ids'][0], $category['ids'][1], $category['ids'][2]));
            Template::set_description($data['main']['title'], $data['main']['seo_desc'], $data['main']['keywords'], null, helpers::open_graph(og::category($data)));
            //Template::add_criteo_code(criteo::product_list(array_slice($data['products'], 0, 3)));
            Template::add_vk_code(vk::view_category($data));

            Template::set_page('category', $data['main']['h1'], $data);
        } // no category
        else {
            if (!AJAX_REQUEST) {
                $this->nopage();
            } else {
                $this->jsonResponse(array(
                    'found' => false,
                    'products' => '<h2>Ничего не найдено</h2>',
                ));
            }
        }
    }

}