<?php

class Controller_email_approve extends Controller_base
{
    public static $_session_page_approve = '_ema_p';

    /**
     * Approve E-mail page
     *
     * @param $arg
     */
    public function index($arg)
    {
        if (empty($arg[0])) {
            if (!empty($_SESSION[self::$_session_page_approve])) {
                Template::set('month', check::month());
                Template::set('master_classes', mc::get_master_classes_list(true, 3));

                $id_list = agregation_products::get_personal_product_list(helpers::search_user_id(), 1, 4);

                //Template::set('mb_top_view', product::get_by_id(mindbox::get_recomendation('PoProsmotram4', FALSE, 4), true, true, false, false, false));
                Template::set('mb_top_view', product::get_by_id($id_list, true, true, false, false, false));

                $referralCampaign = referral::campaign(0);

                $referral = referral::referralInfo($referralCampaign['id'], $referralCampaign['user_discount'], $referralCampaign['referral_discount'], $referralCampaign['user_percentage_discount'], $referralCampaign['referral_percentage_discount']);

                Template::set('referral', $referral);

                Template::set_page('email_approve', 'Завершение регистрации', [
                    'success' => true,
                ]);

                if (isset($_SESSION['mindbox_reg'])) {
                    mindbox::add_action(
                        'identify',
                        'RegistrationMainForm',
                        [
                            'identificator' => [
                                'provider' => 'RoskosmetikaWebSiteId',
                                'identity' => $_SESSION['mindbox_reg'][0],
                            ],
                            'data' => [
                                'email' => $_SESSION['mindbox_reg'][1],
                                'subscriptions' => [
                                    [
                                        'pointOfContact' => 'Email',
                                        'isSubscribed' => true,
                                        'valueByDefault' => true,
                                    ],
                                ],
                                'PersonalDiscount' => '2',
                            ],
                        ]
                    );

                    unset($_SESSION['mindbox_id']);
                }
            } else {
                $this->nopage();
            }
        } else {
            if (
                !preg_match('/^[a-f0-9]{32}$/', $arg[0])
                || Auth::is_auth()
                || !($row = email_approve::get_by_token($arg[0]))
                || users::check_email($row['email'])
            ) {
                $this->nopage();
            } else {
                if ($row['approved']) {
                    redirect('/');
                }

                if (empty($_SESSION[self::$_session_page_approve])) {
                    $clientID = Users::insert_user($row['email'], $row['password']);

                    $success = !!$clientID;

                    if ($success) {
                        if ($row['name']) {
                            Users::update_info($clientID, $row['name'], '');
                        }

                        if ($clientID) {
                            email_approve::approve($row['id'], $clientID);

                            Auth::login($row['email'], $row['password']);
                        }

                        $theme = 'Регистрации в интернет-магазине Роскосметика завершена!';

                        try {
                            @mailer::mail(
                                $theme,
                                [
                                    'sales@roskosmetika.ru' => 'Роскосметика',
                                ],
                                DEBUG ? env('ADMIN_EMAIL') : $row['email'],
                                Template::mail_template(
                                    'mail_registration',
                                    $theme,
                                    [
                                        'email' => $row['email'],
                                        'password' => $row['password'],
                                    ]
                                )
                            );
                        } catch (Exception $e) {
                        }

                        $_SESSION['mindbox_reg'] = [
                            $clientID,
                            $row['email'],
                        ];
                    }

                    $_SESSION[self::$_session_page_approve] = $success;
                } else {
                    $success = true;
                }

                if ($success) {
                    redirect('/email_approve');
                } else {

                    $id_list = agregation_products::get_personal_product_list(helpers::search_user_id(), 1, 4);

                    //Template::set('mb_top_view', product::get_by_id(mindbox::get_recomendation('PoProsmotram4', FALSE, 4), true, true, false, false, false));
                    Template::set('mb_top_view', product::get_by_id($id_list, true, true, false, false, false));
                    Template::set_page('email_approve', 'Завершение регистрации', [
                        'success' => false,
                    ]);
                }
            }
        }
    }

    /**
     * Adds E-mail to to be approved table
     */
    public function add()
    {
        if (AJAX_REQUEST) {
            $response = [
                'success' => false,
            ];

            $emailOk = check::email($_POST['email']);
            $passwordOk = check::password($_POST['password']);

            if (
                !empty($_POST['email'])
                && !empty($_POST['password'])
                && $emailOk
                && $passwordOk
            ) {
                $email = mb_substr(trim($_POST['email']), 0, 50);
                $password = mb_substr(trim($_POST['password']), 0, 25);

                $response['email_error'] = false;
                $response['password_error'] = false;
                $response['msg'] = '';

                $row = null;

                if (
                    !users::check_email($email)
                    && (
                        !($row = email_approve::get_by_email($email))
                        || !$row['approved']
                    )
                ) {
                    $approveUrl = $row ? email_approve::build_url($row['token']) : email_approve::add($email, $password);

                    $response['success'] = !!$approveUrl;

                    if ($response['success']) {
                        $theme = 'Подтверждение E-mail для регистрации в интернет-магазине Роскосметика';

                        @mailer::mail(
                            $theme,
                            [
                                'sales@roskosmetika.ru' => 'Роскосметика',
                            ],
                            DEBUG ? env('ADMIN_EMAIL') : $email,
                            Template::mail_template(
                                'mail_email_approve',
                                $theme,
                                [
                                    'url' => $approveUrl,
                                ]
                            )
                        );
                    } else {
                        $response['msg'] = 'Не удалось зарегистрироваться, повторите попытку позже';
                    }
                } else {
                    if ($clientID = Auth::login($email, $password)) {
                        $this->jsonResponse([
                            'loggedIn' => $clientID,
                        ]);
                    }

                    $response['email_error'] = 'Такой E-mail уже зарегистрирован.<br>Попробуйте <a href="#" class="backup_psw">восстановить пароль</a>.';
                    $response['password_error'] = 'Введите корректный пароль';
                }
            } else {

                $response['email_error'] = $response['password_error'] = 0;

                if (!$emailOk) {
                    $response['email_error'] = 'Введите корректный E-mail';
                }

                if (!$passwordOk) {
                    $response['password_error'] = 'Введите корректный пароль';
                }
            }

            $this->jsonResponse($response);
        }

        $this->nopage();
    }
}
