<?php

class Controller_referral extends controller_base
{
    const PAGE_HEADER = 'Приведи друга';

    public function index($arg)
    {
        $page = referral::campaign(0);

        if (!empty($arg[0])) {
            if ($arg[0] === 'hello') {
                if (!$_SESSION[referral::REFERRAL_KEY]) {
                    redirect('/');
                }

                Template::set_page('referral_landing', 'Только для моих друзей скидка 20% на заказ в интернет-магазине Роскосметика!', array());
            } else {
                $this->nopage();
            }
        } else {
            $this->_page($page['id'], $page['name'], $page['description'], $page['user_discount'], $page['referral_discount'], $page['user_percentage_discount'], $page['referral_percentage_discount']);
        }
    }

    /**
     * Common referral-code generation page logic
     *
     * @param int $campaignID Campaign ID
     * @param string $header Page header
     * @param string $text Page text
     * @param int $userDiscount User discount for every successful referral code appliance
     * @param int $referralDiscount Referral discount
     * @param bool $userPercentageDiscount User gets discount as percentage
     * @param bool $referralPercentageDiscount Referral gets discount as percentage
     */
    private function _page($campaignID, $header, $text, $userDiscount, $referralDiscount, $userPercentageDiscount, $referralPercentageDiscount)
    {
        $id_list = agregation_products::get_personal_product_list(helpers::search_user_id(), 1, 4);

        //Template::set('mb_top_view', product::get_by_id(mindbox::get_recomendation('PoProsmotram4', FALSE, 4), true, true, false, false, false));
        Template::set('mb_top_view', product::get_by_id($id_list, true, true, false, false, false));

        Template::set_description($header, '', '');

        Template::set_page(
            'referral',
            self::PAGE_HEADER,
            [
                'header' => $header,
                'text' => $text,
            ] + referral::referralInfo($campaignID, $userDiscount, $referralDiscount, $userPercentageDiscount, $referralPercentageDiscount)
        );
    }
}
