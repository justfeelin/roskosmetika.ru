<?php

class Controller_mango extends Controller_Base
{
    /**
     * Noop action
     *
     * @param $arg
     */
    public function index($arg)
    {
        $this->nopage();
    }

    /**
     * Returns $_GET when debug mode, $_POST for production
     */
    private function _requestData()
    {
        return DEBUG ? $_GET : $_POST;
    }

    /**
     * Checks for access
     *
     * @return bool
     */
    private function _checkAccess()
    {
        $data = $this->_requestData();

        if (
            empty($data['k'])
            || $data['k'] !== env('HIDDEN_SECRET')
        ) {
            $this->nopage();

            return false;
        }

        return true;
    }


    /**
     * through mango service
     */

    public function send_mango_command()
    {
        if ($this->_checkAccess()) {
            $requestData = $this->_requestData();

            if (!empty($requestData['json']) and !empty($requestData['commandname'] )) {
                $commandname = $requestData['commandname'];
                $jsone =  $requestData['json'];
                $code ='0w1hsrng4h44sc1fn6hr5tnpl5xed1s4';
                $postdata = array(
                    'vpbx_api_key' => '0w1hsrng4h44sc1fn6hr5tnpl5xed1s4',
                    'sign'         => hash('sha256', '0w1hsrng4h44sc1fn6hr5tnpl5xed1s4' .$jsone . 'njb61pcww7qeaikner59biondcsh3abh'),
                    'json'         => $jsone
                );
                $post = http_build_query($postdata);
                $address = 'https://app.mango-office.ru/vpbx/'.$commandname;
                $ch = curl_init($address);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                $response = curl_exec($ch);
                curl_close($ch);
            }
            else {
                $sms_answer=0;
            }
            $this->jsonResponse($response);
        }
    }




}
