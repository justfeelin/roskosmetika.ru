<?php

/**
 * Working with promo codes
 */
class Controller_promo_codes extends Controller_base
{


    function index($args)
    {

        if (isset($_SERVER['HTTP_AJAX']) && Cart::orderID()) {

            if (isset($_POST['code'])) {
                //  json params
                $response = array(
                    'success' => FALSE,
                    'cart' => FALSE,
                    'msg' => FALSE,
                    'errors' => 'Нам очень стыдно, но что-то случилось с нашим сайтом, попробуйте попозже. Если ошибка повторится - Вы всегда можете нам позвонить и оформить заказ по телефону.'
                );

                $code = trim($_POST['code']);


                if (!empty($code)) {

                    $admitad_promo_code = Admitad::get_promo_code($code);

                    if (!$admitad_promo_code) {
                        $code_info = promo_codes::get_by_code($code);
                    } else {
                        $code_info = $admitad_promo_code;
                    }

                    if ($code_info && (empty($_SESSION[referral::REFERRAL_KEY]) || $code_info['discount_type'] == 1) && ($code_info['use'] == 0) && ($code_info['time'] == 1)) {
                        if (Cart::set_promo_discount($code_info['id'], !!$admitad_promo_code)) {
                            if ($code_info['type'] === 'uniq') {
                                if ($admitad_promo_code) {
                                    Admitad::set_promo_code_is_use($code_info['code'], $code_info['id']);
                                } else {
                                    promo_codes::set_is_use($code_info['code'], $code_info['id']);
                                }
                            }

                            Cart::setAgentID($admitad_promo_code ? Orders::AGENT_ID_ADMITAD_OFFLINE : ($code_info['discount_type'] ? Orders::AGENT_ID_PROMO_SIMPLE : Orders::AGENT_ID_PROMO));

                            Cart::setInfo();

                            $response['success'] = TRUE;
                            $response['errors'] = FALSE;
                            $response['cart'] = Cart::$data;
                        }
                    } else {

                        if (!$code_info){
                            $response['msg'] = 'Промо-код введён неверно';
                        }elseif (!(empty($_SESSION[referral::REFERRAL_KEY]) || $code_info['discount_type'] == 1)){
                            $response['msg'] = 'Промо-код не работает при переходе по приглашению';
                        }elseif (!is_null($code_info['end_date']) && $code_info['end_date']<time()){
                            $response['msg'] = 'Срок действия промокода истек '. date("d.m.Y", strtotime($code_info['end_date']));
                        }elseif ($code_info['use'] == 1){
                            $response['msg'] = 'Этот промокод был использован '. date("d.m.Y", strtotime($code_info['use_date']));
                        }

                        $response['msg'] .=". <br />Если у Вас возникли проблемы при использовании промо-кода, обратитесь к нашему <a onClick=\"LiveTex.showWelcomeWindow()\"с>менеджеру</a>.";
                        $response['errors'] = FALSE;
                    }
                } else {
                    $response['msg'] = 'Забыли ввести промо-код.';
                    $response['errors'] = FALSE;
                }

                //  load json
                echo json_encode($response);
                exit;

            } else {
                //  unactive form
                Template::set_page('service_info', 'Корзина', 'Чтобы оформить заказ и ввести промо-код, перейдите в <a href="/cart">корзину</a>.');
            }

        } else {

            //  No AJAX
            Template::set_page('service_info', 'Корзина', 'На Вашем браузере отключен javascript и в частности Ajax. Возможно у Вас старая версия браузера, попробуйте обновить Ваш браузер. Все равно не получилось? - звоните нам.');
        }
    }

    /**
     * Cancels current cart's promo code
     */
    public function cancel()
    {
        if (!AJAX_REQUEST) {
            $this->nopage();

            return;
        }

        $promo = Cart::get_promo();

        if ($promo) {
            promo_codes::cart_cancel(Cart::orderID(), $promo['type'] === 'uniq');

            $return = Cart::setInfo(false);
        } else {
            $return = false;
        }

        $this->jsonResponse($return);
    }
}
