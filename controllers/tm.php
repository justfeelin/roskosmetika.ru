<?php

class Controller_tm extends Controller_base
{
    /**
     * Page header
     */
    const PAGE_HEADER = 'Торговые марки нашего магазина';

    /**
     * TM with  products
     * @param array $args URL Parameters
     */
    function index($args)
    {

        //  tm id or url
        if (isset($args[0])) {
            $params = tm::parse_url($args);

            if (!$params) {
                $this->nopage(AJAX_REQUEST);

                return;
            }

            if ($data = tm::tm_output($params['ids'][0], $params['ids'][1], true, AJAX_REQUEST)) {
                Pagination::setBaseUrl('/tm/' . $params['urls'][0] . ($params['urls'][1] ? '/' . $params['urls'][1] : ''));
                Pagination::setTemplateSettings();

                Template::set('noindex_description', $data['noindex_description']);

                if (AJAX_REQUEST) {
                    Template::set('products', $data['products']);

                    $this->jsonResponse(array(
                        'found' => true,
                        'products' => Template::get_tpl('product_list'),
                        'pagination' => Template::get_tpl('pagination'),
                        'count' => Pagination::getCount(),
                        'filters' => $data['filters'],
                        'prices' => $data['filter_prices'],
                        'show_desc' => $data['show_desc'],
                    ));
                }

                Template::set('found', Pagination::getCount());
                Template::set_description($data['tm']['title'], $data['tm']['seo_desc'], $data['tm']['keywords'], null, helpers::open_graph(og::tm($data)));
                //Template::add_criteo_code(criteo::product_list(array_slice($data['products'], 0, 3)));
                Template::add_vk_code(vk::view_category($data, TRUE));
                Template::set_page('tm', $data['tm']['name'], $data);
            } else {
                if (!AJAX_REQUEST) {
                    $this->nopage();
                } else {
                    $this->jsonResponse(array(
                        'found' => false,
                        'products' => '<h2>Ничего не найдено</h2>',
                    ));
                }
            }
        } else {
            Template::set('brands_html', '');

            $id_list = agregation_products::get_personal_product_list(helpers::search_user_id(), 1, 4);

            //Template::set('mb_top_view', product::get_by_id(mindbox::get_recomendation('PoProsmotram4', FALSE, 4), true, true, false, false, false));
            Template::set('mb_top_view', product::get_by_id($id_list, true, true, false, false, false));

            Template::set_description(self::PAGE_HEADER, 'Торговые марки магазина Роскосметика', 'бренды, торговые марки, марки косметики');
            Template::set_page('tm_list', self::PAGE_HEADER, [
                'tmsHTML' => Cached::brands_list_page(),
            ]);
        }

    }

}
