<?php

class Controller_articles extends Controller_Base
{
    /**
     * Заголовок страницы.
     * @var String
     */
    const PAGE_HEADER = 'Советы косметолога';

    /**
     * Список статей или статья
     * @param array $args Праметры URL
     */
    function index($args)
    {

        $art_id = FALSE;

        if (isset($args[0])) {
            if (is_numeric($args[0])) {
                $art_id = (int)$args[0];
                $url = FALSE;
            } else {
                $art_id = $args[0];
                if (!get_magic_quotes_gpc()) {
                    $art_id = DB::mysql_secure_string($art_id);
                }
                $url = TRUE;
            }
        }


        $id_list = agregation_products::get_personal_product_list(helpers::search_user_id(), 1, 4);


        // Выводит статью
        if ($art_id && $art = Articles::get_art($art_id, FALSE, $url)) {

            if ($url == FALSE && !empty($art['article']['url'])) {
                header('HTTP/1.1 301 Moved Permanently');
                header("Location: /articles/{$art['article']['url']}");
            }
            //Template::set('mb_top_view', product::get_by_id(mindbox::get_recomendation('PoProsmotram4', FALSE, 4), true, true, false, false, false));
            Template::set('mb_top_view', product::get_by_id($id_list, true, true, false, false, false));
            Template::set_description($art['article']['title'], $art['article']['description'], $art['article']['keywords']);
            Template::set_page('article', $art['article']['name'], $art);
        } // Выводит список статей
        else {
            if (empty($art_id)) {
                Template::set('canonical', check::canonical_url());
                //Template::set('mb_top_view', product::get_by_id(mindbox::get_recomendation('PoProsmotram4', FALSE, 4), true, true, false, false, false));
                Template::set('mb_top_view', product::get_by_id($id_list, true, true, false, false, false));
                Template::set_description(self::PAGE_HEADER . ' | Роскосметика',
                    'Роскосметика представляет уникальные статьи от профессиональных косметологов о правильном и эффективном уходе за кожей лица и тела с помощью натуральной профессиональной косметики. Полезные знания и практические советы. Более 70 статей.',
                    'Статьи, советы косметолога, советы профессионалов, косметика, роскосметика.ру');
                Template::set('crumb', 'Советы косметолога(статьи)');
                Template::set_page('articles', self::PAGE_HEADER, [
                    'articles' => Articles::get_all(),
                ]);
            } else {
                $this->nopage();
            }
        }
    }

}

?>