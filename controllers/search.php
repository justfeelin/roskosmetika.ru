<?php

class Controller_search extends Controller_Base
{

    const PAGE_HEADER = 'Поиск';

    /**
     * return search result
     * @param <type> $args
     */
    function index($args)
    {
        $suggestions = isset($_GET['suggestions']) && AJAX_REQUEST;

        $ajaxFilter = !$suggestions && AJAX_REQUEST;

        $search = isset($_GET['search']) ? trim($_GET['search']) : '';

        $numeric = is_numeric($search);

        $productIDs = null;

        // if isset search
        if ($search && ($numeric || mb_strlen($search) > 2)) {
            if ($numeric) {
                $result = array(
                    'products' => Product::get_by_id(array((int)$search), null, !$suggestions),
                    'categories' => null,
                    'trademarks' => null,
                    'categories_html' =>'',
                );
            } else {
                $log = !$ajaxFilter && (!isset($_SERVER['HTTP_REFERER']) || (stristr($_SERVER['HTTP_REFERER'], 'yandex') === false && stristr($_SERVER['HTTP_REFERER'], 'google') === false));

                if ($ajaxFilter) {
                    $productIDs = Search::remote_search($search, 'products', null, 1, $log);

                    $result = array(
                        'products' => $productIDs ? Product::get_by_id($productIDs, null, true, true, $search) : null,
                        'categories' => null,
                        'trademarks' => null,
                    );
                } else {
                    $results = Search::remote_search($search, null, $suggestions ? array('products' => 10, 'categories' => 5, 'trademarks' => 5) : array('categories' => 24), 1, $log, $suggestions);

                    $productIDs = $results['products'] ?: null;

                    $counts = $suggestions ? array(
                        'products' => array_shift($results['products']),
                        'categories' => array_shift($results['categories']),
                        'trademarks' => array_shift($results['trademarks']),
                    ) : null;

                    $result = array(
                        'products' => $productIDs ? Product::get_by_id($productIDs, null, true, true, true) : null,
                        'categories' => $results['categories'] ? Categories::get_by_id($results['categories']) : null,
                        'trademarks' => $suggestions && $results['trademarks'] ? Tm::get_by_id($results['trademarks']) : null,
                        'counts' => $counts,
                        'sort_popularity' => true,
                    );
                }
            }

            $result['found'] = $result['products'] || ($ajaxFilter ? $result['categories'] || $result['trademarks'] : false);

            if (!$result['found']) {
                if (!$ajaxFilter) {
                    $popular = cache_item('search_popular_products', function () {
                        return Product::get_popular(true, 15);
                    }, 10800);

                    $result['products'] = $popular;
                } else {
                    $this->jsonResponse(array(
                        'found' => false,
                        'products' => '<h2>Ничего не найдено</h2>',
                    ));
                }
            }

            $escapedSearch = h($search);

            if ($suggestions) {
                if ($result['products'] || $result['categories'] || $result['trademarks']) {
                    Template::set('search', $escapedSearch);
                    Template::set('suggestions', $result);

                    Template::output('search_suggestions');
                }

                exit();
            } else {
                if ($result['found']) {
                    Pagination::setBaseUrl('/search?search=' . $escapedSearch);
                    Pagination::setTemplateSettings();

                    $where = $productIDs ? 'p.id IN (' . implode(', ', $productIDs) . ')' : null;
                } else {
                    $where = null;
                }

                if ($ajaxFilter) {
                    Template::set('products', $result['products']);
                    
                    $filter_condition = Product::filter_condition('p');
                    $tm_condition = Product::tm_condition('p');
                    $country_condition = Product::country_condition('p');
                    $price_condition = Product::price_condition('p');

                    $this->jsonResponse(array(
                        'found' => true,
                        'products' => Template::get_tpl('product_list'),
                        'pagination' => Template::get_tpl('pagination'),
                        'count' => Pagination::getCount(),
                        'filters' => $productIDs ? filters::available_filters($where . $tm_condition . $price_condition . $country_condition) : null,
                        'tms' => $productIDs ? filters::available_tms($where . $filter_condition . $price_condition . $country_condition) : null,
                        'countries' => $productIDs ? filters::available_countries($where . $tm_condition . $filter_condition . $price_condition) : null,
                        'prices' => $productIDs ? filters::price_range($where . $filter_condition . $tm_condition . $country_condition) : null,
                    ));
                }

                $result['filters'] = [];

                if ($result['found'] && $productIDs) {
                    $result['filters'] = filters::get_filters_for_products($productIDs);

                    $result['filter_tms'] = $productIDs ? filters::available_tms($where, null, 'p', false) : null;
                    $result['filter_countries'] = $productIDs ? filters::available_countries($where, null, 'p', false) : null;
                    $result['filter_prices'] = $productIDs ? filters::price_range($where) : null;
                    $result['categories_html'] = $result['categories'] ? Categories::search_categories_html($result['categories']) : '';
                }

                if (count($result['products']) > 0){
                    $id_list = agregation_products::get_personal_product_list(helpers::search_user_id(), 1, 4);

                    $result['mb_search'] = product::get_by_id($id_list, true, true, false, false, false);
                    //$result['mb_search'] = product::get_by_id(mindbox::get_recomendation('PoxozhieTovarySajt', Search::get_ids_string($result['products']), 4), true, true, false, false, false);
                }

                Template::set('found', Pagination::getCount());
                //Template::add_rocket_code(retail_rocket::search_result($search));
                Template::set('catsInColumn', $result['categories'] ? ceil(count($result['categories']) / 6) : 0);
                Template::set('search', $search);
                //Template::add_criteo_code(criteo::product_list(array_slice($result['products'], 0, 3)));
                Template::add_vk_code(vk::view_search($result, $escapedSearch));

                Template::set_page('search', self::PAGE_HEADER . ' "' . $escapedSearch . '"', $result);
            }
        } else {
            $this->nopage(AJAX_REQUEST);

            if (AJAX_REQUEST) {
                exit();
            }
        }
    }
}
