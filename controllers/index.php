<?php

class Controller_index extends Controller_Base
{

    /**
     * Main page
     * @param array $args url params
     */
    function index($args)
    {
        if ($page = pages::get_page('index')) {
            Template::add_script('jquery.plugin.min.js');
            Template::add_script('jquery.countdown.min.js');

            $reviews = cache_item(
                'index_review',
                function () {
                    return review::get_rand_comments(20);
                },
                300
            );

            shuffle($reviews);

            $review = $reviews[0];

            unset($reviews);

            Template::set('index_banners', cache_item('index_banners', function () {
                return Template::get_tpl('banners', null, banners::get_banners());
            }, 1200));//20 minutes
            Template::set('index_tms', cache_item((TM_FOR_PROF ? 'p_index_tms' : 'index_tms'), function () {
                return Template::get_tpl('tm_menu', null, tm::get_tm_menu());
            }, 1200));//20 minutes
            Template::set('index_day_item', Cached::day_item());
            Template::set('question_to_cosmetolog', questions_to_cosmetolog::get_question());
            //Template::set('index_mcs', Cached::index_mcs());
            Template::set('index_hurry', Cached::index_hurry());
            Template::set('main_spec', lines::main_special());
            Template::set('review', $review);
            Template::add_admitad_code(Admitad::index_page());
            //Template::add_criteo_code(criteo::main_page());
            Template::add_vk_code(vk::view_home());

            //Template::add_addigital_code(Addigital::index_page());

            Template::set_description($page['title'], $page['description'], $page['keywords'], null, helpers::open_graph(og::index_page()));
            Template::set_page('index', $page['header'], $page['content']);
        } else {
            $this->nopage();
        }
    }
}
