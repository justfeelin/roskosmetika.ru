<?php

class Controller_forgot extends controller_base
{

    /**
     * password recovery
     * @param array $arg URL params.
     */
    function index($arg)
    {

        $header = 'Магазин Роскосметика. Восcтановление пароля';

        if (isset($_SERVER['HTTP_AJAX'])) {

            $email = FALSE;

            if (isset($_POST['email'])) $email = mb_substr(trim($_POST['email']), 0, 50);

            if (!$email) {
                $msg['email'] = 'Забыли ввести E-mail';
            } elseif (!Check::email($email)) {
                $msg['email'] = 'Введите корректный E-mail';
            } elseif (!Users::check_email($email)) {
                $msg['email'] = 'Клиента с таким E-mail у нас нет, возможно Вы опечатались';
            } else {
                $values['email'] = $email;
            }

            // add new
            if (!isset($msg)) {

                $password = Users::get_password(Users::check_email($email));

                if ($password) {

                    $response = array(
                        'success' => true,
                        'msg' => 'На Ваш e-mail выслан пароль.<br><br>Пожалуйста, проверьте вашу почту.',
                        'errors' => false
                    );


                    $info = array('password' => $password);

                    //mail to user
                    $from = array('sales@roskosmetika.ru' => 'Роскосметика');
                    $body = Template::mail_template('mail_forgot', $header, $info);
                    @mailer::mail($header, $from, DEBUG ? env('ADMIN_EMAIL') : $email, $body);

                } else {
                    $response = array(
                        'success' => false,
                        'msg' => 'Нам очень стыдно, но что-то случилось с нашим сайтом, попробуйте попозже.',
                        'errors' => false
                    );
                }

            } // if fail
            else {
                $response = array(
                    'success' => false,
                    'msg' => 'Вы ввели неправильные данные',
                    'errors' => $msg
                );
            }

            //  load json
            echo json_encode($response);
            exit;
        } else {

            $this->nopage();
        }
    }
}
