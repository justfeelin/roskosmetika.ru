<?php

class Controller_cdek extends Controller_base
{
    function index($arg)
    {
        $this->nopage();
    }

    /** Get cdek point/points
     * @param $args
     */
    public function get_points($args)
    {
        if (isset($_SERVER['HTTP_AJAX'])) {
            $response = [
                'success' => false,
            ];

            if (!empty($_POST['cdek'])){
                $response['points'] = cdek::get_points((int)$_POST['cdek']);
                $response['success'] = true;
            }elseif(!empty($_POST['id_sdek'])){
                $response['point'] = cdek::get_point((int)$_POST['id_sdek']);
                $response['success'] = true;
            }

            echo json_encode($response);
            exit;
        } else {
            $this->nopage();
        }
    }

    /**Get prices for cdek points
     * @param $args
     */
    public function get_delivery_price($args)
    {
        if (isset($_SERVER['HTTP_AJAX'])) {
            $response = [
                'success' => false,
            ];

            if ((!empty($_POST['region']))&&(!empty($_POST['type']))){
                $response['price'] = cdek::get_price((int)$_POST['region'],(int)$_POST['type']);
                $response['success'] = true;
            }

            echo json_encode($response);
            exit;
        } else {
            $this->nopage();
        }
    }

}