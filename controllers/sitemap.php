<?php
class Controller_sitemap extends Controller_Base
{
    /**
     * Заголовок страницы.
     * @var String
     */
    const PAGE_HEADER = 'Карта сайта';

    /**
     * Sitemap
     * @param array $args Праметры URL
     */
    function index($args)
    {

        $content = array();

        $id_list = agregation_products::get_personal_product_list(helpers::search_user_id(), 1, 4);

        Template::set('canonical', check::canonical_url());
        Template::set('mb_top_view', product::get_by_id($id_list, true, true, false, false, false));

        Template::set_description('Карта сайта – список всех страниц сайта Роскосметика', 'Карта сайта содержит все разделы и категории интернет-магазина Роскосметика.', '');
        Template::set('crumb', self::PAGE_HEADER);


        $content = sitemap::prepare_array(Pages::get_list(), array(0 => 'name', 1 => 'header'), array(0 => 'url', 1 => 'name'), array(0 => DOMAIN_FULL . '/page/', 1 => ''));

        $content[0]['url'] = DOMAIN_FULL;

        $content[] = array('url' => DOMAIN_FULL . '/products/new',
                           'name' => 'Новинки магазина');
        $content[] = array('url' => DOMAIN_FULL . '/products/sales',
                           'name' => 'Специальные предложения, акции магазина');
        $content[] = array('url' => DOMAIN_FULL . '/products/hurry',
                           'name' => 'Распродажа');
        $content[] = array('url' => DOMAIN_FULL . '/products/hit',
                           'name' => 'Хиты продаж');
        $content[] = array('url' => DOMAIN_FULL . '/kosmetika',
                           'name' => 'Косметика по типам');;
        $content[] = array('url' => DOMAIN_FULL . '/master_classes',
                           'name' => 'Мастер-классы расписание');
        $content[] = array('url' => DOMAIN_FULL . '/review',
                           'name' => 'Отзывы о магазине');
        $content[] = array('url' => DOMAIN_FULL . '/question',
                           'name' => 'Вопросы косметологу');
        $content[] = array('url' => DOMAIN_FULL . '/articles',
                           'name' => 'Советы косметолога, статьи');
        $content[] = array('url' => DOMAIN_FULL . '/landing/kak-umenshit-objem-talii',
                           'name' => 'Как уменьшить объем талии');


        $tm = tm::get_list(FALSE, FALSE, FALSE, TRUE);
        $tm_output = array();

        for ($i=0; $i < count($tm); $i++)
        { 
            $tm_output[$i]['url'] =  DOMAIN_FULL . '/tm/' . $tm[$i]['url'];
            $tm_output[$i]['name'] =  $tm[$i]['name'];

            $lines = lines::get_list($tm[$i]['id']);

            if($lines)
            {
                $tm_output[$i]['children'] = sitemap::prepare_array($lines, array(0 => 'url', 1 => 'name'), array(0 => 'url', 1 => 'name'), array(0 => DOMAIN_FULL . '/tm/' . $tm[$i]['url'] . '/' , 1 => ''));
            }


        }

        $content[] = array('url' => DOMAIN_FULL . '/tm',
                           'name' => 'Торговые марки магазина (Бренды)',
                           'children' => $tm_output);


        $categories = DB::get_rows('SELECT rc.id, rc.name, CONCAT("/category/", rc._full_url) AS url FROM rk_categories rc WHERE rc.level = 1 AND rc.visible = 1');
        $cat_lvl_2 = array();

        for ($i=0; $i < count($categories); $i++) { 

            $parent_id = $categories[$i]['id'];
            $cat_lvl_2 = DB::get_rows("SELECT rc.name, CONCAT('/category/', rc._full_url) AS url FROM rk_categories rc WHERE rc.level = 2 AND rc.visible = 1 AND rc.parent_1 = $parent_id");

            if ($cat_lvl_2) {
                $categories[$i]['children'] = $cat_lvl_2;
            }

        $content[] = $categories[$i];
        }

        $content[] = array('url' => DOMAIN_FULL . '/sets',
                           'name' => 'Наборы продуктов',
                           'children' => sitemap::prepare_array(sets::get_sets(FALSE, FALSE, FALSE), array(0 => 'url', 1 => 'name'), array(0 => 'url', 1 => 'name'), array(0 => DOMAIN_FULL . '/sets/', 1 => '')));

        $content[] = array('url' => DOMAIN_FULL . '/country',
                           'name' => 'Страны-производители',
                           'children' => sitemap::prepare_array(country::get_countries(), array(0 => 'url', 1 => 'name'), array(0 => 'url', 1 => 'name'), array(0 => DOMAIN_FULL . '/country/', 1 => '')));


        Template::set_page('sitemap', self::PAGE_HEADER, $content);
        
    }

}
?>