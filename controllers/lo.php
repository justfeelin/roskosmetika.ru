<?php

/**
 * Класс live operator
 */
class Controller_lo extends Controller_base
{
    /**
     * Emails list.
     */
    const EMAIL = '2255483@mail.ru';

    /**
     * Handling request from LOH.
     * @param array $args
     */
    function index($args)
    {
        // List of legal hosts.
        $legal_hosts = array(
            'roskosmetika',
            'roskosmetika.ru',
            'veliniya.ru',
            'r-cosmetics.ru'
        );

        preg_match('/^(http:\/\/|https:\/\/)(www.)?([^\/]+)/i', $_SERVER['HTTP_REFERER'], $matches);
        $host = $matches[3];

        // Request from legal host and right parameter.
        if (in_array($host, $legal_hosts) &&
            isset($_GET['name']) &&
            isset($_GET['contacts']) &&
            isset($_GET['msg']) &&
            isset($_GET['callback']) &&
            $_GET['callback'] == 'reply'
        ) {

            $name = trim($_GET['name']);
            $contacts = trim($_GET['contacts']);
            $msg = ((string)$_GET['msg']==='')? '':trim(preg_replace("/[^0-9a-zA-Zа-яА-Я .,+\/\-]/i", '', (string)$_GET['msg']));
            $callback = $_GET['callback'];

            // Checking parameters. 
            if (empty($name) ||
                !Check::name($name) ||
                empty($contacts) ||
                !(Check::phone($contacts) || Check::email($contacts)) ||
                empty($msg)
            ) {

                $success = 0;
            } else {
                $success = 1;

                switch ($host) {
                    case 'roskosmetika.ru' :
                        $resource_rus = "Роскосметика";
                        break;
                    case 'veliniya.ru' :
                        $resource_rus = 'Велиния';
                        break;
                    case 'r-cosmetics.ru' :
                        $resource_rus = 'R-cosmetics';
                        break;
                    default :
                        $resource_rus = $host;
                }

                live_operator::add_message($name, $contacts, $msg, $host);

                @mailer::mail(
                    'CRM обработано. Запрос ' . $resource_rus,
                    [
                        'sales@roskosmetika.ru' => 'Liveoperator',
                    ],
                    self::EMAIL,
                    $this->get_msg($name, $contacts, $msg, $host)
                );
            }

            header("Content-type: text/javascript");
            echo "$callback({success:$success,name:'$name',contacts:'$contacts',msg:'$msg'})";
            exit();
        } // Request from unlegal host or wrong resuest. Output 404 page.
        else {
            $this->nopage();
        }
    }

    /**
     * Generate message for mail
     */
    protected function get_msg($name, $contacts, $question, $resource)
    {
        $msg = <<<EOD
<html>
<head>
    <title>Live operator</title>
    <meta http-equiv=\"Content-type\" content=\"text/html; charset=koi8-r\">
</head>
<body>
     <p><b>ФИО:</b> {$name}</p>
     <p><b>Контакты:</b> {$contacts}</p>
     <p><b>Ресурс:</b> {$resource}</p>
     <p><b>Вопрос:</b> {$question}</p>
</body>
</html>
EOD;
        return $msg;
    }

}