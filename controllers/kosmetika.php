<?php

class Controller_kosmetika extends Controller_base
{
    /**
     * Type pages list
     *
     * @param $arg
     */
    public function index($arg)
    {
        if (!isset($arg[0])) {
            Template::set_description(
                'Косметика - купить в интернет-магазине из Москвы по хорошей цене. Заказать косметику с доставкой из каталога Роскосметика',
                'Косметика от интернет магазина Роскосметика – это множество товаров по выгодной цене, удобные способы оплаты и оперативная доставка, включая возможность самовывоза!',
                ''
            );

            Template::set_page('kosmetika', 'Косметика', [
                'type_pages' => type_page::get_pages(),
            ]);
        } else {
            $baseUrl = $h1 = $title = $description = $view = null;

            if (!isset($arg[1])) {
                $data = type_page::output($arg[0], AJAX_REQUEST);

                if ($data) {
                    $baseUrl = $data['type_page']['url'];

                    if (!AJAX_REQUEST) {
                        $h1 = $data['type_page']['h1'];
                        $title = $data['type_page']['title'];
                        $description = $data['type_page']['description'];

                        $view = 'kosmetika_page';
                    }
                }
            } else {
                $data = !isset($arg[2]) ? type_page::output_tm($arg[0], $arg[1], AJAX_REQUEST) : null;

                if ($data) {
                    // kosmetika tm
                    $baseUrl = $data['info']['type_page_url'] . '/' . $data['info']['tm_url'];

                    if (!AJAX_REQUEST) {
                        $h1 = $data['info']['h1'];
                        $title = $data['info']['title'];
                        $description = $data['info']['description'];

                        $view = 'kosmetika_tm';
                    }
                } else {
                    // kosmetika category
                    $data = type_page::output_category($arg, AJAX_REQUEST);

                    if ($data) {
                        $baseUrl = implode('/', array_slice($arg, 1));

                        if (!AJAX_REQUEST) {
                            $data['crumbs'] = array_merge([
                                [
                                    'name' => 'Косметика',
                                    'url' => '/kosmetika',
                                ],
                                [
                                    'name' => $data['type_page']['name'],
                                    'url' => '/kosmetika/' . $arg[0],
                                ],
                            ], $data['crumbs']);

                            $h1 = $data['main']['h1'];
                            $title = $data['main']['title'];
                            $description = $data['main']['seo_desc'];

                            $view = 'category';
                        }
                    }
                }
            }

            if (!$data) {
                if (!AJAX_REQUEST) {
                    $this->nopage();

                    return;
                } else {
                    $this->jsonResponse([
                        'found' => false,
                        'products' => '<h2>Ничего не найдено</h2>',
                    ]);
                }
            }

            Pagination::setBaseUrl('/kosmetika/' . $baseUrl);
            Pagination::setTemplateSettings();

            if (!AJAX_REQUEST) {
                Template::set_description($title, $description, '');

                Template::set_page($view, $h1, $data);
            } else {
                Template::set('products', $data['products']);

                $this->jsonResponse(
                    [
                        'found' => true,
                        'products' => Template::get_tpl('product_list'),
                        'pagination' => Template::get_tpl('pagination'),
                        'count' => Pagination::getCount(),
                        'filters' => $data['filters'],
                        'prices' => $data['filter_prices'],
                    ] + (
                        isset($data['filter_tms'])
                            ? [
                                'tms' => $data['filter_tms'],
                            ]
                            : []
                    ) + (
                        isset($data['filter_countries'])
                            ? [
                                'countries' => $data['filter_countries'],
                            ]
                            : []
                    )
                );
            }
        }
    }
}
