<?php

class Controller_landing extends Controller_base
{
    /**
     * Landing page entry point
     *
     * @param array $args URL arguments
     */
    public function index($args)
    {
        if (
            !empty($args[0])
            && !preg_match('/[^a-z0-9-]/i', $args[0])
            && is_dir(SITE_PATH . '/templates_landing/' . $args[0])
        ) {
            $countersHtml = Template::get_tpl('counters');

            Template::setPaths((!DEBUG ? COMPRESSED_TEMPLATES . '/' : '') . 'templates_landing/' . $args[0]);

            Template::set('countersHtml', $countersHtml);

            $this->_page_logic($args[0]);
        } else {
            $this->nopage();
        }
    }

    /**
     * Page-specific logic
     *
     * @param string $url Landing page url
     */
    private function _page_logic ($url)
    {
        switch ($url) {
            case 'kak-umenshit-objem-talii':
                $productIDs = [
                    1339,   //skrab, 300ml
                    182,    //laminaria, 3kg
                    1332,   //krem, 300ml

                    1340,   //gel-skrab, 100ml
                    577,    //laminaria, 200g
                    1333,   //anticel. krem, 100ml
                    158,    //plenka

                    1339,   //gel-skrab, 300ml
                    //182
                    1330,   //anticel. krem, 300ml
                    199,    //prostynia

                    109,    //sol-skrab
                    14,     //laminaria, 5kg
                    74,    //anticel
                    //199
                ];

                $sets = [
                    [
                        'id' => 4637,
                        'products' => [
                            1340,
                            577,
                            1333,
                            158,
                        ],
                    ],
                    [
                        'id' => 4638,
                        'products' => [
                            1339,
                            182,
                            1330,
                            199,
                        ],
                    ],
                    [
                        'id' => 4639,
                        'products' => [
                            109,
                            14,
                            74,
                            199,
                        ],
                    ],
                ];

                $products = Product::get_by_id($productIDs, null, false, false, false, false);

                for ($i = 0, $n = count($products); $i < $n; ++$i) {
                    if ($products[$i]['id'] === '577') {
                        $products[$i]['price'] *= 2;
                    }

                    $products[$i]['price_formatted'] = $this->_formatPrice($products[$i]['price']);
                    $products[$products[$i]['id']] = $products[$i];

                    unset($products[$i]);
                }

                $viewSets = [];

                for ($i = 0, $n = count($sets); $i < $n; ++$i) {
                    $sum = 0;

                    $viewSets[$i] = $sets[$i]['products'];

                    for ($y = 0, $m = count($sets[$i]['products']); $y < $m; ++$y) {
                        $sum += (int)$products[$sets[$i]['products'][$y]]['price'];
                    }

                    $product = Product::get_by_id($sets[$i]['id']);

                    $sets[$i]['price'] = $this->_formatPrice((int)$product['special_price'] ?: $product['price']);
                    $sets[$i]['total'] = $this->_formatPrice($sum);
                }

                Template::set('products', $products);
                Template::set('sets', $sets);
                Template::set('viewSets', json_encode($viewSets));

                break;
        }
    }

    /**
     * Returns formatted price
     *
     * @param int $price Price
     *
     * @return string
     */
    private function _formatPrice($price)
    {
        return number_format((int)$price, 0, '', '&nbsp;');
    }
}
