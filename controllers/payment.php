<?php

class Controller_payment extends Controller_Base
{
    /**
     * Performs redirect to payment form from invoice
     *
     * @param $arg
     */
    public function index($arg)
    {
        if (
            empty($arg[0])
            || !($invoice = Payment::get_invoice_by_hash($arg[0]))
            || !($order = Orders::get_by_id($invoice['order_number']))
        ) {
            $this->nopage();

            return;
        }

        if ($invoice['cost'] > Orders::to_pay($order['order_number'])) {
            $this->_invoice_error_page($order['site_order_number'], $invoice['cost']);

            return;
        }

        $order['cost'] = $invoice['cost'];
        $order['ts'] = $invoice['ts'];

        Template::set('order', $order);

        Template::output('payment_form');

        die();
    }

    /**
     * Redirect to sberbank payment form
     *
     * @param array $args
     *
     * @throws Exception
     */
    public function sberbank($args)
    {
        if (
            empty($args[0])
            || !($order = Orders::get_by_id($args[0], true))
        ) {
            $this->nopage();

            return;
        }

        $sbrfInvoice = null;

        $cost = isset($args[1]) && $args[1] > 0 ? (float)$args[1] : null;

        if (
            ($toPay = Orders::to_pay($order['order_number'])) <= 0
            || ($cost !== null && $cost > $toPay)
            || !($sbrfInvoice = Payment::get_or_create_sberbank_invoice($order['order_number'], $cost ?: $toPay))
        ) {
            $this->_invoice_error_page($order['site_order_number'], $sbrfInvoice ? $sbrfInvoice['cost'] : null);

            return;
        }

        $_SESSION[Payment::SBERBANK_SESSION_KEY] = $sbrfInvoice['sberbank_id'];

        redirect($sbrfInvoice['form_url']);
    }

    /**
     * Sberbank after payment success redirect page
     */
    public function result()
    {
        if (!($invoice = $this->_payment_result())) {
            $this->nopage();

            return;
        }

        $paid = Payment::check_sberbank_paid($invoice['sberbank_id']);

        $hasCertificates = false;

        if ($paid) {
            $products = Orders::get_order_by_number($invoice['order_number'], true);

            for ($i = 0, $n = count($products); $i < $n; ++$i) {
                if ($products[$i]['id'] == Product::CERTIFICATE_ID) {
                    $hasCertificates = true;

                    break;
                }
            }

            if ($hasCertificates) {
                $order = Orders::get_by_id($invoice['order_number']);

                $sendEmail = DEBUG || $order['email'] && !isset($_SESSION['paid-cert-' . $invoice['order_number']]);

                $_SESSION['paid-cert-' . $invoice['order_number']] = true;

                if ($sendEmail) {
                    mailer::mail(
                        'Успешная оплата сертификата в интернет-магазине Роскосметика',
                        [
                            'sales@roskosmetika.ru' => 'Роскосметика',
                        ],
                        DEBUG ? [
                            env('ADMIN_EMAIL') => env('ADMIN_NAME'),
                        ] : [
                            $order['email'] => ($order['surname'] ? $order['surname'] . ' ' : '') . $order['name'] . ($order['patronymic'] ? ' ' . $order['patronymic'] : ''),
                        ],
                        Template::mail_template(
                            'mail_text',
                            '',
                            '<p>Вы успешно оплатили заказ с сертификатом.</p><p>Наш менеджер обязательно свяжется с Вами для уточнения деталей.</p>'
                        )
                    );
                }
            }
        }

        Template::set_page('payment_result', 'Оплата заказа', [
            'paid' => $paid,
            'certificates' => $hasCertificates,
            'emailCertificates' => false,
        ]);
    }

    /**
     * Sberbank after payment error redirect page
     */
    public function error()
    {
        if (!($invoice = $this->_payment_result())) {
            $this->nopage();

            return;
        }

        Template::set_page('payment_error', 'Оплата заказа');
    }

    /**
     * Returns sberbank invoice. Used in result() and error()
     *
     * @return array|null
     */
    private function _payment_result()
    {
        return
            empty($_GET['orderId'])
            || empty($_SESSION[Payment::SBERBANK_SESSION_KEY])
            || $_SESSION[Payment::SBERBANK_SESSION_KEY] !== $_GET['orderId']
            || !($invoice = Payment::get_sberbank_invoice($_SESSION[Payment::SBERBANK_SESSION_KEY]))
                ? null
                : $invoice;
    }

    /**
     * Prepares invoice error page
     *
     * @param int $siteOrderNumber Site order number
     * @param float $cost Invoice cost
     */
    private function _invoice_error_page($siteOrderNumber, $cost = null)
    {
        Template::set_page(
            'service_info',
            ' ',
            '<h2><strong class="red">Внимание, ошибка оплаты!</strong></h2>
            <p>
            <br>
            Заказ <strong>Р-' . $siteOrderNumber . '</strong>, на который выставлен счёт, был либо ранее оплачен, либо сумма счёта не соответсвует сумме заказа.
            </p>
            <p>
            Пожалуйста, свяжитесь с нашим менеджером по бесплатному телефону <a href="tel:8-800-775-54-83" class="phone_number">8&nbsp;800&nbsp;775-54-83</a>.
            </p>' . (
                $cost
                    ? '<p>
                        Сумма выставленного счёта: <strong>' . formatPrice($cost) . ' руб.</strong>
                        </p>'
                    : ''
            )
        );
    }
}
