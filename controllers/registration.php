<?php

class Controller_registration extends Controller_Base
{

    /**
     * New client registration.
     * @param array $args URL parameters.
     */
    function index($args)
    {

        $email = FALSE;
        $password = FALSE;

        if (isset($_POST['email']) && check::email($_POST['email'])) {
            $email = mb_substr(trim($_POST['email']), 0, 50);
        }

        if (isset($_POST['password']) && check::password($_POST['password'])) {
            $password = mb_substr(trim($_POST['password']), 0, 25);
        }

        if (isset($_SERVER['HTTP_AJAX'])) {

            $json['email_error'] = 0;
            $json['password_error'] = 0;
            $json['msg'] = 0;

            if ($email && $password) {
                if (!users::check_email($email)) {
                    $json['success'] = !!Users::insert_user($email, $password);
                    if ($json['success']) {
                        Auth::login($email, $password);

                        //  letter to client
                        $from = array('sales@roskosmetika.ru' => 'Роскосметика');
                        $theme = 'Регистрация в интернет-магазине Роскосметика';
                        $content['email'] = $email;
                        $content['password'] = $password;
                        $message_user = Template::mail_template('mail_registration', 'Регистрация в интернет-магазине Роскосметика', $content);
                        @mailer::mail($theme, $from, DEBUG ? env('ADMIN_EMAIL') : $email, $message_user);

                    } else {
                        $json['msg'] = 'Не удалось зарегистрироваться, повторите попытку позже';
                    }

                } else {
                    if (!!Auth::login($email, $password)) {
                        $this->jsonResponse([
                            'success' => true,
                        ]);
                    }

                    $json['success'] = 0;
                    $json['email_error'] = 'Такой E-mail уже зарегистрирован.<br>Попробуйте <a href="#" class="backup_psw">восстановить пароль</a>.';
                }

            } else {
                $json['success'] = 0;
                if (!$email) $json['email_error'] = 'Введите корректный E-mail';
                if (!$password) $json['password_error'] = 'Введите корректный пароль';
            }

            echo json_encode($json);
            exit();
        } else {
            if ($email && $password) Users::insert_user($email, $password);

            redirect('/');
        }
    }

    /**
     * Registration fvia professionals request
     */
    public function for_proff()
    {
        $errors = [];

        $name = !empty($_POST['name']) ? $_POST['name'] : null;
        $email = !empty($_POST['email']) && check::email($_POST['email']) ? $_POST['email'] : null;
        $product = !empty($_POST['product_id']) && $_POST['product_id'] > 0 ? product::get_by_id($_POST['product_id'], true, true) : null;
        $pack_id = !empty($_POST['pack_id']) && $_POST['pack_id'] > 0 ? $_POST['pack_id'] : null;
        $pack_n = null;

        if ($product) {
            if ($pack_id) {
                for ($i = 0, $n = count($product['packs']); $i < $n; ++$i) {
                    if ($product['packs'][$i]['id'] == $pack_id) {
                        $pack_n = $i;

                        break;
                    }
                }
            }
        }

        if (!$product || $pack_id && $pack_n === null) {
            $this->jsonResponse('Ошибка при регистрации');
        }

        if (!$name) {
            $errors[] = 'Необходимо указать ваше имя (Или название организации)';
        }

        if (!$email) {
            $errors[] = 'Необходимо указать E-mail';
        }

        if (!$errors) {
            $registered = Users::check_email($email);

            if (!$registered) {
                $password = dechex(crc32(rand()));

                $registered = !!Users::insert_user($email, $password);
            } else {
                $password = null;
            }

            if ($registered) {
                Auth::login($email, $password);

                $message_user = Template::mail_template('mail_registration_for_proff', 'Цена товара в интернет-магазине Роскосметика', [
                    'email' => $email,
                    'password' => $password,
                    'product' => $product,
                    'pack' => $pack_n,
                ]);

                @mailer::mail(
                    'Цена ' . html_entity_decode($product['name'] . ' (' . ($pack_id ? $product['packs'][$pack_n]['pack'] : $product['pack']) . ')' ) . ' в интернет-магазине Роскосметика',
                    [
                        'sales@roskosmetika.ru' => 'Роскосметика',
                    ],
                    DEBUG ? env('ADMIN_EMAIL') : $email,
                    $message_user
                );
            }

            $this->jsonResponse(!!$registered ?: 'Ошибка при регистрации');
        } else {
            $this->jsonResponse(implode('<br>', $errors));
        }
    }
}
