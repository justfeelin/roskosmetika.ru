<?php

class Controller_sets extends Controller_Base
{

    /**
     * Sets of the products
     * @param array $args URL Parameters
     */
    function index($args)
    {

        //  item of level 1
        if (isset($args[0])) {

            $redirect = FALSE;
            $id = FALSE;
            $flag = TRUE;

            $item = $args[0];
            if (is_numeric($item)) {
                $id = (int)$item;
                $redirect = sets::redirect($id);
                if (!$redirect) $id = FALSE;

            } else {
                $item = DB::mysql_secure_string($item);
                $id = sets::get_id_by_url($item);
            }

            if (!$id) $flag = FALSE;

            //  redirect
            if ($redirect) {
                header('HTTP/1.1 301 Moved Permanently');
                header('Location: /sets/' . ($redirect ? $redirect : $item));
            }

            if ($data = sets::set_output($id, $flag, AJAX_REQUEST)) {
                if (AJAX_REQUEST) {
                    Template::set('products', $data['products']);

                    $this->jsonResponse(array(
                        'found' => true,
                        'products' => Template::get_tpl('product_list'),
                        'count' => $data['found'],
                        'filters' => $data['filters'],
                        'tms' => $data['filter_tms'],
                        'countries' => $data['filter_countries'],
                        'prices' => $data['filter_prices'],
                    ));
                }

                Template::set('canonical', check::canonical_url());
                //Template::add_criteo_code(criteo::product_list(array_slice($data['products'], 0, 3)));
                Template::add_vk_code(vk::view_category($data, TRUE));
                Template::set_description($data['set']['title'], $data['set']['seo_desc'], $data['set']['keywords']);
                Template::set_page('set', $data['set']['name'], $data);
            } else {
                if (AJAX_REQUEST) {
                    $this->jsonResponse(array(
                        'found' => false,
                        'products' => '<h2>Ничего не найдено</h2>',
                    ));
                } else {
                    $this->nopage();
                }
            }
        } else {
            Template::set_description('Наборы товаров на сайте Роскосметика.ру', 'Наборы товаров магазина Росскосметик.ру. Косметика для комплексного ухода', 'Наборы продуктов, выборка товаров, наборы, продукты, косметика, роскосметика.ру');
            Template::set_page('sets', 'Наборы продуктов', [
                'sets' => sets::get_sets(false, false, false),
            ]);
        }
    }
}
