<?php

class Controller_room extends Controller_base
{
    /**
     * Constants difinition.
     */
    const PAGE_HEADER = 'Личный кабинет';
    const EMAIL = '"Отдел продаж Роскосметика" <2255483@mail.ru>, "RK_IT" <roskosmetika_it@mail.ru>';

    /**
     * Personal room home page.
     * @param array $args URL parameters.
     */
    function index($args)
    {

        // Проверяем авторизован ли пользователь
        if ($user_id = Auth::get_user_id()) {

            //  look data
            if (isset($_POST ['submit'])) {

                $name = mb_substr(trim($_POST ['name']), 0, 25);
                $surname = mb_substr(trim($_POST ['surname']), 0, 25);
                $patronymic = mb_substr(trim($_POST ['patronymic']), 0, 25);

                $values = array();

                //  check surname
                if (empty($surname)) {
                    $msg ['surname'] = 'Введите Вашу фамилию';
                } elseif (!Check::name($surname)) {
                    $msg ['surname'] = 'Была введена некорректная фамилию';
                } else {
                    $values ['surname'] = $surname;
                }

                //  check name
                if (empty($name)) {
                    $msg ['name'] = 'Введите Ваше имя';
                } elseif (!Check::name($name)) {
                    $msg ['name'] = 'Было введено некорректное имя';
                } else {
                    $values ['name'] = $name;
                }

                //  check pathronymic
                if (!empty($patronymic) && !Check::name($patronymic)) {
                    $msg ['patronymic'] = 'Было введено некорректное отчество';
                } else {
                    $values ['patronymic'] = $patronymic;
                }

                //  data ok
                if (empty($msg)) {

                    Users::update_info($user_id, $name, $surname, $patronymic);

                    $_SESSION['mindbox_room'] = true;

                    redirect('/room');
                } //  bad data
                else {
                    Template::set('active_aside', 1);

                    $id_list = agregation_products::get_personal_product_list(helpers::search_user_id(), 1, 4);
                    Template::set('mb_top_view', product::get_by_id($id_list, true, true, false, false, false));

                    //Template::set('mb_top_view', product::get_by_id(mindbox::get_recomendation('PoProsmotram4', FALSE, 4), true, true, false, false, false));
                    Template::set_form(Users::get_user_info() + $values, $msg);
                    Template::set_page('info', 'Личная информация');
                }
            } //  view form
            else {
                $user = Users::get_user_info();

                if (isset($_SESSION['mindbox_room'])) {
                    mindbox::add_action(
                        'performOperation',
                        'EditPersonal',
                        [
                            'data' => [
                                'firstName' => $user['name'],
                                'lastName' => $user['surname'],
                                'middleName' => $user['patronymic'],
                                'PersonalDiscount' => max($user['main_discount'], $user['auto_discount']),
                                'authenticationTicket' => mindbox::auth_ticket(mindbox::TICKET_TYPE_IDENTITY, $user['id']),
                            ],
                        ]
                    );

                    unset($_SESSION['mindbox_room']);
                }

                $this->_mindbox_favorites_check();

                Template::set('active_aside', 1);
                $id_list = agregation_products::get_personal_product_list(helpers::search_user_id(), 1, 4);
                Template::set('mb_top_view', product::get_by_id($id_list, true, true, false, false, false));

                //Template::set('mb_top_view', product::get_by_id(mindbox::get_recomendation('PoProsmotram4', FALSE, 4), true, true, false, false, false));
                Template::set_form($user);
                Template::set_page('info', 'Личная информация');
            }
        } // not autorizated user
        else {
            Template::set_page('service_info', self::PAGE_HEADER, 'Необходима авторизация.');
        }
    }

    /**
     * Orders history.
     * @param array $args URL parameters.
     */
    function history($args)
    {
        if ($user_id = Auth::get_user_id()) {
            $orders = Orders::get_user_orders($user_id);

            for ($i = 0, $n = count($orders); $i < $n; ++$i) {
                $orders[$i]['items'] = Orders::get_order_by_number($orders[$i]['order_number'], true);
                $orders[$i]['shipping_name'] = $orders[$i]['shipping_id'] ? Orders::$shippings[$orders[$i]['shipping_id']] : null;

                $hasCertificates = false;

                for ($y = 0, $m = count($orders[$i]['items']); $y < $m; ++$y) {
                    if ($orders[$i]['items'][$y]['id'] == Product::CERTIFICATE_ID) {
                        $hasCertificates = true;

                        break;
                    }
                }

                $orders[$i]['show_payment'] =
                    ($orders[$i]['to_pay'] = Orders::to_pay($orders[$i]['order_number'])) > 0
                    && (
                        $hasCertificates
                        || (
                            (int)$orders[$i]['shipping_id'] !== Orders::CUSTOM_SHIPPING
                            && $orders[$i]['region_id']
                            && in_array((int)$orders[$i]['status_id'], [Orders::STATUS_ID_CURRENT, Orders::STATUS_ID_CANCEL, Orders::STATUS_ID_CANCEL_CLIENT], true)
                            && in_array((int)$orders[$i]['payment_id'], [Orders::PAYMENT_ID_WALLETONE, Orders::PAYMENT_ID_ROBOKASSA, Orders::PAYMENT_ID_SBER_ACQUIRING, Orders::PAYMENT_ID_BEZNAL], true)
                            && in_array((int)$orders[$i]['payway_id'], [Orders::PAYWAY_ID_7SKY, Orders::PAYWAY_ID_RCOSMETIC], true)
                        )
                    );

                $orders[$i]['show_load_bill'] = $this->_canLoadBill($orders[$i]);

                if (!$orders[$i]['show_payment'] && in_array($orders[$i]['status_id'], [Orders::STATUS_ID_DONE, Orders::STATUS_ID_STOCK_DONE, Orders::STATUS_ID_DELIVERY])) {
                    $orders[$i]['certificates'] = promo_codes::order_certificates($orders[$i]['order_number']);
                } else {
                    $orders[$i]['certificates'] = null;
                }
            }

            Template::set('active_aside', 3);
            $id_list = agregation_products::get_personal_product_list(helpers::search_user_id(), 1, 4);
            Template::set('mb_top_view', product::get_by_id($id_list, true, true, false, false, false));

            //Template::set('mb_top_view', product::get_by_id(mindbox::get_recomendation('PoProsmotram4', FALSE, 4), true, true, false, false, false));
            Template::set_page('history', 'История заказов', array(
                'orders' => $orders,
            ));
        } else {
            Template::set_page('service_info', self::PAGE_HEADER, 'Необходима авторизация.');
        }
    }

    /**
     * Repeat old order.
     * @param array $args URL parameters.
     */
    function repeat($args)
    {
        if (isset($args[0])) {
            $order_id = (int)$args[0];

            // check autorization
            if ($user_id = Auth::get_user_id()) {

                //	add order to cart
                if (
                    Orders::check_order($order_id, $user_id)
                    && ($order = Orders::get_order_by_real_id($order_id))
                    && ($products = Orders::get_order_by_number($order['order_number']))
                ) {
                    for ($i = 0, $n = count($products); $i < $n; ++$i) {
                        Cart::add_to_cart($products[$i]['id'], $products[$i]['quantity']);
                    }

                    $order = Cart::setInfo(false);

                    Template::set('order_quantity', count($order['info']));
                    Template::set('order_sum', $order['info']['sum_with_discount']);
                    Template::set_page('service_info', self::PAGE_HEADER, 'Заказ повторно добавлен в корзину.');
                } //	cant add order
                else {

                    Template::set_page('service_info', self::PAGE_HEADER, 'Нам очень стыдно, но что-то случилось с нашим сайтом, попробуйте попозже..');
                }
            } //	not autorizated user
            else {

                Template::set_page('service_info', self::PAGE_HEADER, 'Необходима авторизация.');
            }
        }
    }

    /**
     * Edit delivery info.
     * @param array $args URL parameters.
     */
    function delivery($args)
    {

        if ($user_id = Auth::get_user_id()) {

            //	look data
            if (isset($_POST ['submit'])) {

                $address = trim($_POST ['address']);
                $phone = mb_substr(trim($_POST ['phone']), 0, 18);
                $values = array();

                //	check phone
                if (!empty($phone) && !Check::phone($phone)) {
                    $msg['phone'] = 'Введите корректный телефон';
                } else {
                    $values['phone'] = $phone;
                }

                $region = !empty($_POST['region']) ? Orders::get_regions($_POST['region']) : 0;

                if ($region) {
                    $region = (int)$region['id'];
                }

                $values['region_id'] = $region;

                //	check adress
                $values['address'] = $address;

                if (empty($msg)) {

                    Users::update_delivery($user_id, $phone, $region, $address);
                    redirect('/room/delivery');
                } //	bad data
                else {
                    Template::set('active_aside', 2);
                    Template::set_form($values, $msg);
                    Template::set('regions', Orders::get_regions());
                    $id_list = agregation_products::get_personal_product_list(helpers::search_user_id(), 1, 4);
                    Template::set('mb_top_view', product::get_by_id($id_list, true, true, false, false, false));

                    //Template::set('mb_top_view', product::get_by_id(mindbox::get_recomendation('PoProsmotram4', FALSE, 4), true, true, false, false, false));
                    Template::set_page('delivery', 'Информация о доставке');
                }
            } // 	view form
            else {
                Template::set('regions', Orders::get_regions());
                Template::set('active_aside', 2);
                $id_list = agregation_products::get_personal_product_list(helpers::search_user_id(), 1, 4);
                Template::set('mb_top_view', product::get_by_id($id_list, true, true, false, false, false));

                //Template::set('mb_top_view', product::get_by_id(mindbox::get_recomendation('PoProsmotram4', FALSE, 4), true, true, false, false, false));
                Template::set_form(Users::get_user_info());
                Template::set_page('delivery', 'Информация о доставке');
            }

            Template::add_css('smoothness/jquery-ui-1.11.4.custom.min.css');
            Template::add_script('jquery-ui.min.js');
        } //	not autorizated user
        else {

            Template::set_page('service_info', self::PAGE_HEADER, 'Необходима авторизация.');
        }
    }

    /**
     * Changing password.
     * @param array $args URL parameters.
     */
    function password($args)
    {

        //	check autorization
        if ($user_id = Auth::get_user_id()) {

            //	look data
            if (isset($_POST ['submit'])) {


                $password1 = trim(mb_substr($_POST['password1'], 0, 25));
                $password2 = trim(mb_substr($_POST['password2'], 0, 25));

                $values = array();

                //	check password
                if (empty($password1)) {
                    $msg['password1'] = 'Введите пароль:';
                } elseif (mb_strlen($password1) < 6) {
                    $msg['password1'] = 'Пароль должен состоять из 6 и более знаков:';
                } elseif (!Check::password($password1)) {
                    $msg['password1'] = 'Пароль должен состоять из букв, цифр, тире и знака подчеркивания:';
                } else {
                    $values['password1'] = $password1;
                    if (empty($password2)) {
                        $msg['password2'] = 'Подтвердите пароль:';
                    } elseif ($password1 != $password2) {
                        $msg['password1'] = 'Несовпадение паролей, введите пароль еще раз:';
                    } else {
                        $values['password2'] = $password2;
                    }
                }

                if (empty($msg)) {

                    Template::set('active_aside', 5);
                    Template::set('change_ok', 1);
                    $id_list = agregation_products::get_personal_product_list(helpers::search_user_id(), 1, 4);
                    Template::set('mb_top_view', product::get_by_id($id_list, true, true, false, false, false));

                    //Template::set('mb_top_view', product::get_by_id(mindbox::get_recomendation('PoProsmotram4', FALSE, 4), true, true, false, false, false));
                    Users::update_password($user_id, $password1);
                    Template::set_page('password', 'Изменение пароля');
                } //	bad data
                else {

                    Template::set('active_aside', 5);
                    $id_list = agregation_products::get_personal_product_list(helpers::search_user_id(), 1, 4);
                    Template::set('mb_top_view', product::get_by_id($id_list, true, true, false, false, false));

                    //Template::set('mb_top_view', product::get_by_id(mindbox::get_recomendation('PoProsmotram4', FALSE, 4), true, true, false, false, false));
                    Template::set_form($values, $msg);
                    Template::set_page('password', 'Изменение пароля');
                }
            } // 	view form
            else {

                Template::set('active_aside', 5);
                $id_list = agregation_products::get_personal_product_list(helpers::search_user_id(), 1, 4);
                Template::set('mb_top_view', product::get_by_id($id_list, true, true, false, false, false));
                //Template::set('mb_top_view', product::get_by_id(mindbox::get_recomendation('PoProsmotram4', FALSE, 4), true, true, false, false, false));
                Template::set_page('password', 'Изменение пароля');
            }
        } //	not autorizated user
        else {

            Template::set_page('service_info', self::PAGE_HEADER, 'Необходима авторизация.');
        }
    }

    /**
     * Output favorites products.
     * @param array $args URL parameters.
     */
    function favorites($args)
    {

        //	check autorization
        if ($user_id = Auth::get_user_id()) {
            $this->_mindbox_favorites_check();

            Template::set('active_aside', 4);
            $id_list = agregation_products::get_personal_product_list(helpers::search_user_id(), 1, 4);
            Template::set('mb_top_view', product::get_by_id($id_list, true, true, false, false, false));

            //Template::set('mb_top_view', product::get_by_id(mindbox::get_recomendation('PoProsmotram4', FALSE, 4), true, true, false, false, false));
            Template::set_page('favorites', 'Избранное', array(
                'items' => Favorites::show($user_id),
            ));
        } //	not autorizated user
        else {

            Template::set_page('service_info', self::PAGE_HEADER, 'Необходима авторизация.');
        }
    }

    /**
     * Add product to favorites.
     * @param array $args URL parameters.
     */
    function add_favorites($args)
    {

        $prod_id = FALSE;

        if (isset($args[0])) $prod_id = (int)$args[0];

        // Hear is ajax.
        if (isset($_SERVER['HTTP_AJAX'])) {

            if ($prod_id && Favorites::add($prod_id, Auth::get_user_id())) {
                $json['success'] = 1;
                $json['msg'] = 'Товар добавлен в избранное.';
                $json['products'] = Favorites::show(Auth::get_user_id());
            } else {
                $json['success'] = 0;
                $json['msg'] = 'Нам очень стыдно, но что-то случилось с нашим сайтом, попробуйте попозже.';
            }

            echo json_encode($json);
            exit();
        } // Hear is no ajax.
        else {

            if ($prod_id) {

                if ($user_id = Auth::get_user_id()) {

                    if (Favorites::add($prod_id, $user_id)) $this->go_back();
                    // cant add
                    else {
                        Template::set_page('service_info', 'Избранное', 'Нам очень стыдно, но что-то случилось с нашим сайтом, попробуйте попозже.');
                    }

                } else {
                    Template::set_page('service_info', self::PAGE_HEADER, 'Необходима авторизация.');
                }

            } else {
                $this->nopage();
            }

        }
    }

    /**
     * Remove product from favorites.
     * @param array $args URL parameters.
     */
    function del_favorites($args)
    {

        $prod_id = FALSE;

        if (isset($args[0])) $prod_id = (int)$args[0];

        if ($prod_id) {

            if ($user_id = Auth::get_user_id()) {
                Favorites::del($prod_id, $user_id);

                $_SESSION['mindbox_fav'] = 1;

                $this->go_back();
            } else {
                Template::set_page('service_info', self::PAGE_HEADER, 'Необходима авторизация.');
            }

        } else {
            $this->nopage();
        }

    }

    /**
     * Logout.
     * @param array $args URL parameters.
     */
    function logout($args)
    {

        if ($user_id = Auth::get_user_id()) {

            Users::logout($user_id);
            // session_regenerate_id();

            redirect('/');
        } //	not autorizated user
        else {

            Template::set_page('service_info', self::PAGE_HEADER, 'Необходима авторизация.');
        }
    }

    /**
     * Cancel order.
     * @param array $args URL parameters.
     */
    function cancel($args)
    {

        $order_id = FALSE;

        if (isset($args[0])) $order_id = (int)$args[0];

        if ($order_id) {

            if ($user_id = Auth::get_user_id()) {
                Orders::cancel_order($order_id, $user_id);

                $this->go_back();
            } else {
                Template::set_page('service_info', self::PAGE_HEADER, 'Необходима авторизация.');
            }

        } else {
            $this->nopage();
        }

    }

    /**
     * Load bill for download
     *
     * @param array $args URL parameters.
     * @return null File must be load
     */
    function load_bill($args)
    {

        $order_id = FALSE;

        if (isset($args[0])) $order_id = (int)$args[0];

        if ($order_id) {
            if ($user_id = Auth::get_user_id()) {
                //  order_info
                $order = Orders::get_order_by_real_id($order_id);

                if (
                    !$order
                    || $order['client_id'] != $user_id
                    || !$this->_canLoadBill($order)
                ) {
                    $this->nopage();

                    return;
                }

                if ($order['region_id'] && $order['shipping_id']) {
                    Cart::setShipping(Orders::get_regions($order['region_id']), $order['shipping_id'], false);
                }

                $order_items = Orders::get_order_by_number($order['order_number'], true);

                $order_items_qant = count($order_items);

                if (!empty($order) && $order_items_qant > 0) {

                    $user_info = Users::get_client_by_id($user_id);

                    // excel 
                    $excel = export::open_file('bill_blank.xls');
                    $excel->setActiveSheetIndex(0);
                    $bill = $excel->getActiveSheet();

                    $bill->setCellValue('B' . 17, 'СЧЕТ №' . $order['invoice'] . ' от');
                    $bill->setCellValue('C' . 17, date('j.m.Y', strtotime($order['added_date'])));
                    $bill->setCellValue('A' . 18, trim('Заказчик: ' . $user_info['surname'] . ' ' . $user_info['name'] . ' ' . $user_info['patronymic']));
                    $bill->setCellValue('A' . 19, 'ИНН: ' . ($user_info['inn'] != '' ? $user_info['inn'] : ''));
                    $bill->setCellValue('A' . 20, 'Адрес: ' . $user_info['address']);

                    if ($order['delivery']) {
                        $bill->setCellValue('F' . 27, $order['delivery']);
                    }

                    $stringPrice = helpers::priceToString($order['cost']);

                    $bill->setCellValue('A' . 28, mb_convert_case(mb_substr($stringPrice, 0, 1, 'UTF-8'), MB_CASE_UPPER, 'UTF-8') . mb_substr($stringPrice, 1, null, 'UTF-8'));
                    $bill->setCellValue('F' . 29, $order['cost']);

                    $rawSum = $discountSum = 0;

                    for ($i = 0; $i < $order_items_qant; ++$i) {
                        if ($i !== 0) {
                            $bill->insertNewRowBefore(24 + $i, 1);
                        }

                        $itemPrice = $order_items[$i]['old_price'] !== 0 ? $order_items[$i]['old_price'] : $order_items[$i]['price'];

                        $bill->setCellValue('A' . (24 + $i), $i + 1);
                        $bill->setCellValue('B' . (24 + $i), html_entity_decode($order_items[$i]['name'], ENT_NOQUOTES, 'UTF-8') . ', ' . html_entity_decode($order_items[$i]['pack'], ENT_NOQUOTES, 'UTF-8'));
                        $bill->setCellValue('D' . (24 + $i), $order_items[$i]['quantity']);
                        $bill->setCellValue('E' . (24 + $i), $itemPrice);
                        $bill->setCellValue('F' . (24 + $i), $itemPrice * $order_items[$i]['quantity']);

                        $rawSum += $itemPrice;
                        $discountSum += $order_items[$i]['price'];
                    }

                    $discountDiff = $rawSum - $discountSum;

                    $bill->setCellValue('F' . (24 + $order_items_qant), $rawSum);
                    $bill->setCellValue('F' . (25 + $order_items_qant), $discountDiff !== 0 ? $discountDiff : '');

                    if (ob_get_level()) {
                        ob_end_clean();
                    }

                    export::download_excel($excel, 'bill_roskosmetika');
                } else {
                    Template::set_page('service_info', self::PAGE_HEADER, 'Заказ не найден.');
                }
            } else {
                Template::set_page('service_info', self::PAGE_HEADER, 'Необходима авторизация.');
            }
        } else {
            $this->nopage();
        }
    }

    /**
     * Returns whether load bill is available for order
     *
     * @param array $order Order info
     *
     * @return bool
     */
    private function _canLoadBill($order)
    {
        return (int)$order['shipping_id'] !== Orders::CUSTOM_SHIPPING
            && $order['region_id']
            && in_array((int)$order['status_id'], [Orders::STATUS_ID_CURRENT, Orders::STATUS_ID_CANCEL, Orders::STATUS_ID_CANCEL_CLIENT], true)
            && (int)$order['payment_id'] === Orders::PAYMENT_ID_BEZNAL
            && (int)$order['payway_id'] === Orders::PAYWAY_ID_7SKY;
    }

    /**
     * Checks whether favorites data has to be sent to mindbox
     */
    private function _mindbox_favorites_check()
    {
        if (isset($_SESSION['mindbox_fav'])) {
            $data = [
                'data' => [
                    'action' => [
                        'personalOffers' => [],
                    ],
                ],
            ];

            $products = Favorites::show(Auth::get_user_id());

            for ($i = 0, $n = count($products); $i < $n; ++$i) {
                $data['data']['action']['personalOffers'][] = [
                    'productId' => $products[$i]['id'],
                    'count' => 1,
                    'price' => roundPrice($products[$i]['special_price'] ?: $products[$i]['price']),
                ];
            }

            mindbox::add_action(
                'performOperation',
                'UstanovitSostavSpiskaTovary',
                $data
            );

            unset($_SESSION['mindbox_fav']);
        }
    }
}
