<?php

class Controller_otzyvy extends Controller_Base
{
    public function index($arg)
    {
        Template::set_page('products_page', 'Отзывы', [
            'products' => Product::get_popular_pseudo_random(40, 'category-page', 31536000),
            'crumbs' => [
                [
                    'name' => 'Отзывы',
                ],
            ],
        ]);
    }

    /**
     * Category-based reviews page
     *
     * @param array $args
     */
    public function category($args)
    {
        $params = Categories::parse_url($args, '/otzyvy/category/', true);

        if (!$params) {
            $this->nopage();

            return;
        }

        $categoryID = $params['ids'][2] ?: ($params['ids'][1] ?: $params['ids'][0]);

        $category = Categories::get_by_id($categoryID, null, true);

        Pagination::setBaseUrl('/otzyvy/category/' . implode('/', $args));

        $organikCategory = $category['id'] == 1448 || $category['parent_1'] == 1448 || $category['parent_2'] == 1448;

        $cased = helpers::grammatical_case(
            $category['is_tag'] && $category['tag_name']
                ? $category['tag_name']
                : (
                    $organikCategory && $category['h1_2_organik']
                        ? $category['h1_2_organik']
                        : ($category['h1'] ?: $category['name'])
                ),
            helpers::CASE_TYPE_PREPOSITIONAL_EXTENDED,
            true
        );

        if ($category['name_on_products'] > 0) {
            $hasH1 = !!$category['h1'];

            $hasAltNumber = !!($hasH1 ? $category['h1_alt_number'] : $category['name_alt_number']);

            $categoryName = $hasAltNumber
                ? helpers::numeric_name(
                    $hasH1 ? $category['h1'] : $category['name'],
                    $hasH1 ? $category['h1_alt_number'] : $category['name_alt_number'],
                    $hasH1 ? $category['h1_plural'] : $category['name_plural'],
                    false
                )
                : (
                    $hasH1 ? $category['h1'] : $category['name']
                );
        } else {
            $categoryName = null;
        }

        $crumbs = [
            [
                'name' => helpers::mb_ucfirst($params['names'][0]),
                'url' => '/otzyvy/category/' . $params['urls'][0],
            ],
        ];

        if ($params['ids'][1]) {
            $crumbs[] = [
                'name' => helpers::mb_ucfirst($params['names'][1]),
                'url' => '/otzyvy/category/' . $params['urls'][1],
            ];
        }

        if ($params['ids'][2]) {
            $crumbs[] = [
                'name' => helpers::mb_ucfirst($params['names'][2]),
            ];
        }

        $this->_category_tm_page(
            $categoryID,
            $category['level'],
            'Отзывы ' . $cased,
            (
                $category['is_tag'] && $category['tag_name']
                    ? $category['tag_name']
                    : helpers::numeric_name(
                        $organikCategory && $category['h1_2_organik']
                            ? $category['h1_2_organik']
                            : ($category['h1'] ?: $category['name']),
                        $category['h1'] ? $category['h1_alt_number'] : $category['name_alt_number'],
                        $category['h1'] ? $category['h1_plural'] : $category['name_plural'],
                        false
                    )
            ) . ' – отзывы от покупателей интернет-магазина Роскосметика',
            'Читать подробные отзывы ' . $cased . '. Уже оставлено|оставлен|оставлено {reviewsN} отзывов|отзыв|отзыва о {productsN} товарах|товаре|товарах интернет-магазина Роскосметика – оставьте и свой отзыв!',
            $crumbs,
            $categoryName,
            $category['name_on_products']
        );
    }

    /**
     * Category-based reviews page
     *
     * @param array $args
     */
    public function category__tm($args)
    {
        $url = tm::parse_category_tm_url($args, '/otzyvy/category-tm/', true);

        if (!$url) {
            $this->nopage();

            return;
        }

        $categoryID = end($url['category_ids']);

        $tmInCat = tm_in_cat::get_pair($url['tm_id'], $categoryID);

        Pagination::setBaseUrl('/otzyvy/category-tm/' . implode('/', $args));

        $organikCategory = $tmInCat['cat_id'] == 1448 || $tmInCat['cat_parent_1'] == 1448 || $tmInCat['cat_parent_2'] == 1448;

        $organikH1 = $organikCategory && $tmInCat['cat_h1_1_organik'] && $tmInCat['cat_h1_2_organik'];

        $cased = tm::alt_name_format(
            helpers::grammatical_case(
                $organikH1 ? $tmInCat['cat_h1_2_organik'] : ($tmInCat['h1'] ?: $tmInCat['cat_name']),
                helpers::CASE_TYPE_PREPOSITIONAL_EXTENDED,
                true
            ) .
            (!$tmInCat['h1'] ? ' ' . $tmInCat['tm_name'] : ''),
            $tmInCat['tm_name'],
            $tmInCat['tm_alt_name']
        );

        $crumbs = [
            [
                'name' => helpers::mb_ucfirst($url['category_names'][0]),
                'url' => '/otzyvy/category/' . $url['category_urls'][0],
            ]
        ];

        if ($url['n'] > 2) {
            $crumbs[] = [
                'name' => helpers::mb_ucfirst($url['category_names'][1]),
                'url' => '/otzyvy/category/' . $url['category_urls'][0] . '/' . $url['category_urls'][1],
            ];
        }

        if ($url['n'] > 3) {
            $crumbs[] = [
                'name' => helpers::mb_ucfirst($url['category_names'][2]),
                'url' => '/otzyvy/category/' . $url['category_urls'][0] . '/' . $url['category_urls'][1] . '/' . $url['category_urls'][2],
            ];
        }

        $crumbs[] = [
            'name' => $tmInCat['tm_name'],
        ];

        $this->_category_tm_page(
            [
                $categoryID,
                $url['n'] - 1,
            ],
            $url['tm_id'],
            'Отзывы ' . $cased,
            ($organikH1 ? $tmInCat['cat_h1_2_organik'] : helpers::numeric_name($tmInCat['cat_name'], $tmInCat['cat_name_alt_number'], $tmInCat['cat_name_plural'], false)) .
            ' ' .
            ($tmInCat['tm_name'] ?: $tmInCat['tm_alt_name']) .
            ' – отзывы ' .
            helpers::grammatical_case(
                $organikH1 ? $tmInCat['cat_h1_1_organik'] : ($tmInCat['h1'] ?: helpers::numeric_name($tmInCat['cat_name'], $tmInCat['cat_name_alt_number'], $tmInCat['cat_name_plural'], true)),
                helpers::CASE_TYPE_PREPOSITIONAL_EXTENDED,
                true
            ) .
            ($organikH1 || !$tmInCat['h1'] ? ' ' . ($tmInCat['tm_alt_name'] ?: $tmInCat['tm_name']) : '') .
            ' от покупателей интернет-магазина Роскосметика',
            'Читать подробные отзывы ' . $cased . '. Уже оставлено|оставлен|оставлено {reviewsN} отзывов|отзыв|отзыва о {productsN} товарах|товаре|товарах интернет-магазина Роскосметика – оставьте и свой отзыв!',
            $crumbs
        );
    }

    /**
     * Trademark-based reviews page
     *
     * @param array $args
     */
    public function tm($args)
    {
        $params = tm::parse_url($args);

        if (!$params || $params['ids'][1]) {
            $this->nopage();

            return;
        }

        $tm = tm::get_by_id($params['ids'][0]);

        Pagination::setBaseUrl('/otzyvy/tm/' . implode('/', $args));

        $this->_category_tm_page(
            $tm['id'],
            false,
            'Отзывы о косметике ' . $tm['name'],
            'Отзывы о косметике ' . $tm['name'] . ' от покупателей интернет-магазина Роскосметика',
            'Читать подробные отзывы о косметике ' . $tm['name'] . '. Уже оставлено|оставлен|оставлено {reviewsN} отзывов|отзыв|отзыва о {productsN} товарах|товаре|товарах интернет-магазина Роскосметика – оставьте и свой отзыв!',
            [
                [
                    'name' => $tm['name'],
                ]
            ]
        );
    }

    /**
     * Type pages reviews
     *
     * @param array $args
     */
    public function kosmetika($args)
    {
        if (!isset($args[0])) {
            Template::set_page('products_page', 'Отзывы о косметике', [
                'products' => Product::get_popular_pseudo_random(40, 'category-page', 31536000),
                'crumbs' => [
                    [
                        'name' => 'Отзывы',
                        'url' => '/otzyvy'
                    ],
                    [
                        'name' => 'Косметика',
                    ],
                ],
            ]);

            return;
        }

        if (!isset($args[1])) {
            $data = type_page::get_type_page_by_url($args[0]);

            if (!$data) {
                $this->nopage();

                return;
            }

            list($reviews, $count) = review::product_reviews_type_page($data['id']);

            $cased = helpers::grammatical_case($data['h1'] ?: $data['name'], helpers::CASE_TYPE_PREPOSITIONAL_EXTENDED, true);

            $this->_page(
                $reviews,
                $count,
                type_page::products_count($data['id']),
                'Отзывы ' . $cased,
                ($data['h1'] ?: $data['name']) . ' – отзывы от покупателей интернет-магазина Роскосметика',
                'Читать подробные отзывы ' . $cased . '. Уже оставлено|оставлен|оставлено {reviewsN} отзывов|отзыв|отзыва о {productsN} товарах|товаре|товарах интернет-магазина Роскосметика – оставьте и свой отзыв!',
                [
                    [
                        'name' => 'Отзывы',
                        'url' => '/otzyvy',
                    ],
                    [
                        'name' => 'Косметика',
                        'url' => '/otzyvy/kosmetika',
                    ],
                    [
                        'name' => helpers::mb_ucfirst($data['name']),
                    ],
                ],
                $data['name'],
                $data['name_on_products']
            );
        } else {
            $data = type_page::get_type_page_tm_by_url($args[0], $args[1]);

            if (!$data) {
                $this->nopage();

                return;
            }

            list($reviews, $count) = review::product_reviews_type_page_tm($data['type_page_id'], $data['tm_id']);

            $cased = helpers::grammatical_case($data['h1'] ?: ($data['type_page_h1'] ?: $data['type_page_name']), helpers::CASE_TYPE_PREPOSITIONAL_EXTENDED, true);

            $hasH1 = !!$data['type_page_h1'];

            $this->_page(
                $reviews,
                $count,
                type_page::products_count($data['type_page_id'], $data['tm_id']),
                'Отзывы ' . $cased . ' ' . $data['tm_name'] . ($data['tm_alt_name'] ? ' (' . $data['tm_alt_name'] . ')' : ''),
                helpers::numeric_name(
                        $hasH1 ? $data['type_page_h1'] : $data['type_page_name'],
                        $hasH1 ? $data['type_page_h1_alt_number'] : $data['type_page_name_alt_number'],
                        $hasH1 ? $data['type_page_h1_plural'] : $data['type_page_name_plural'],
                        false
                    ) .
                    ' ' . $data['tm_name'] .
                    ' – отзывы ' .
                    helpers::grammatical_case(
                        helpers::numeric_name(
                            $hasH1 ? $data['type_page_h1'] : $data['type_page_name'],
                            $hasH1 ? $data['type_page_h1_alt_number'] : $data['type_page_name_alt_number'],
                            $hasH1 ? $data['type_page_h1_plural'] : $data['type_page_name_plural'],
                            true
                        ),
                        helpers::CASE_TYPE_PREPOSITIONAL_EXTENDED,
                        true
                    ) .
                    ' ' . ($data['tm_alt_name'] ?: $data['tm_name']) .
                    ' от покупателей интернет-магазина Роскосметика',
                'Читать подробные отзывы ' . $cased . ($data['h1'] ? '' : ' ' . $data['tm_name']) . '. Уже оставлено|оставлен|оставлено {reviewsN} отзывов|отзыв|отзыва о {productsN} товарах|товаре|товарах интернет-магазина Роскосметика – оставьте и свой отзыв!',
                [
                    [
                        'name' => 'Отзывы',
                        'url' => '/otzyvy',
                    ],
                    [
                        'name' => 'Косметика',
                        'url' => '/otzyvy/kosmetika',
                    ],
                    [
                        'name' => helpers::mb_ucfirst($data['h1'] ?: ($data['type_page_h1'] ?: $data['type_page_name'])),
                        'url' => '/otzyvy/kosmetika/' . $args[0],
                    ],
                    [
                        'name' => $data['tm_name'],
                    ],
                ],
                $data['type_page_name'],
                $data['type_page_name_on_products']
            );
        }
    }

    /**
     * Performs common reviews logic for category/tm pages. Fetches reviews and sets view variables
     *
     * @param int|array $id Category/trademark ID
     * @param int $categoryLevel Category level. If false - trademark page
     * @param string $header Page h1 tag content
     * @param string $title Page title tag content
     * @param string $description Page meta description content. {reviewsN} and {productsN} will be replaced with count of reviews and products respectively, count_morphology() will be applied
     * @param array $crumbs Breadcrumbs
     * @param string $listProductType Product type name in list
     * @param int $listProductTypeN Count of products to display type name in list
     *
     * @throws Exception
     */
    private function _category_tm_page($id, $categoryLevel, $header, $title, $description, $crumbs, $listProductType = null, $listProductTypeN = 0)
    {
        if (is_array($id)) {
            $tmID = $categoryLevel;

            $categoryLevel = $id[1];
            $id = $id[0];
        } else {
            $tmID = !$categoryLevel ? $id : null;
        }

        $productsN = Product::products_count($id, $categoryLevel, $tmID);

        list($reviews, $count) = $categoryLevel
            ? (
                $tmID
                    ? review::product_reviews_category_tm($id, $categoryLevel, $tmID)
                    : review::product_reviews_category($id, $categoryLevel)
            )
            : review::product_reviews_tm($id);

        $this->_page(
            $reviews,
            $count,
            $productsN,
            $header,
            $title,
            $description,
            array_merge(
                [
                    [
                        'name' => 'Отзывы',
                        'url' => '/otzyvy',
                    ]
                ],
                $crumbs
            ),
            $listProductType,
            $listProductTypeN
        );
    }

    /**
     * Performs common reviews logic. Fetches reviews and sets view variables
     *
     * @param array $reviews Reviews
     * @param int $count All reviews count
     * @param int $productsN All products count
     * @param string $header Page h1 tag content
     * @param string $title Page title tag content
     * @param string $description Page meta description content. {reviewsN} and {productsN} will be replaced with count of reviews and products respectively, count_morphology() will be applied
     * @param array $crumbs Breadcrumbs
     * @param string $listProductType Product type name in list
     * @param int $listProductTypeN Count of products to display type name in list
     *
     * @throws Exception
     */
    private function _page($reviews, $count, $productsN, $header, $title, $description, $crumbs = null, $listProductType = null, $listProductTypeN = 0)
    {
        Pagination::setTemplateSettings();

        Template::set_description(
            $title,
            count_morphology(
                str_replace(
                    [
                        '{reviewsN}',
                        '{productsN}',
                    ],
                    [
                        $count,
                        $productsN,
                    ],
                    $description
                )
            ),
            ''
        );

        Template::set_page(
            'otzyvy',
            '',
            [
                'h1' => $header,
                'reviews' => $reviews,
                'crumbs' => $crumbs,
                'product_type' => $listProductType,
                'product_type_n' => $listProductTypeN,
            ]
        );
    }
}
