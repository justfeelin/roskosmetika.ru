<?php

class Controller_products extends Controller_base
{

    /**
     * Output special offers group of products.
     * @param array $args ULR parameters.
     */
    function index($args)
    {

        if (isset($args[0])) {
            $type = $args[0];

            //  check url
            if ($type === 'new' || $type === 'sales' || $type === 'hurry' || $type === 'hit') {
                switch ($type) {
                    case 'new':
                        $header = 'Новинки нашего магазина';
                        $short_name = 'Новинки';

                        break;

                    case 'hit':
                        $header = 'Хиты продаж нашего магазина';
                        $short_name = 'Хиты продаж';

                        break;    

                    case 'sales':
                        $header = 'Специальные предложения нашего магазина';
                        $short_name = 'Акции';

                        break;

                    case 'hurry':
                        $header = 'Последний шанс купить';
                        $short_name = 'Успей купить';

                        break;
                }

                if ($data = products::special_group_output($type, AJAX_REQUEST)) {
                    Pagination::setBaseUrl('/products/' . $type);
                    Pagination::setTemplateSettings();

                    if (AJAX_REQUEST) {
                        Template::set('products', $data['products']);

                        $this->jsonResponse([
                            'found' => true,
                            'products' => Template::get_tpl('product_list'),
                            'pagination' => Template::get_tpl('pagination'),
                            'count' => Pagination::getCount(),
                            'tms' => $data['filter_tms'],
                            'countries' => $data['filter_countries'],
                            'categories' => $data['filter_categories'],
                            'prices' => $data['filter_prices'],
                        ]);
                    }

                    $data['short_name'] = $short_name;

                    //Template::add_criteo_code(criteo::product_list(array_slice($data['products'], 0, 3)));
                    Template::add_vk_code(vk::view_category($data, TRUE));
                    Template::set('found', Pagination::getCount());
                    Template::set_page('spec_groups', $header, $data);
                } else {
                    if (AJAX_REQUEST) {
                        echo '<h2>Ничего не найдено</h2>';
                        exit;
                    }

                    Template::set_page('service_info', $header, 'Нам очень стыдно, но что-то случилось с нашим сайтом, попробуйте попозже.');
                }
            } else {
                if (!AJAX_REQUEST) {
                    $this->nopage();
                } else {
                    echo '<h2>Ничего не найдено</h2>';

                    exit;
                }
            }

        } else {
            $this->nopage();
        }


    }

}
