<?php


class Controller_competition extends Controller_base
{
    /**
     * Output competition page
     * @param array $args URL parameters.
     */
    function index($args)
    {

        $competition = FALSE;

        if (isset($args[0])) {
            //check id
            if (is_numeric($args[0])) {

                $item = (int)$args[0];
                $redirect = competition::redirect($item);

                //  redirect
                if ($redirect) {
                    header('HTTP/1.1 301 Moved Permanently');
                    header("Location: /competition/$redirect");
                }

                $competition = competition::get_competition($item);

            } else {

                $item = $args[0];
                if (!get_magic_quotes_gpc()) $item = DB::mysql_secure_string($item);

                $item = competition::get_competition_id($item);

                if ($item) {
                    $competition = competition::get_competition($item);
                }
            }
        } else {
            $this->nopage();
        }


        if ($competition && ($competition['visible'] == 1)) {

            Template::add_script('../assets/competition/competition.js');
            //vd(mindbox::get_recomendation('PoZakazam4', FALSE, 4));exit;

            $id_list = agregation_products::get_personal_product_list(helpers::search_user_id(), 2, 4);

            //Template::set('mb_pages', product::get_by_id(mindbox::get_recomendation('PoZakazam4', FALSE, 4), true, true, false, false, false));
            Template::set('mb_pages', product::get_by_id($id_list, true, true, false, false, false));
            Template::set_page('competition', $competition['name'], $competition);


        } else {
            $this->nopage();
        }

    }

    /**
     * Add new client
     */
    public function subscribe($args)
    {

        if (isset($_SERVER['HTTP_AJAX'])) {
            $response = [
                'success' => false,
            ];

            $emailOk = check::email($_POST['email']);
            if (
                !empty($_POST['email'])
                && !empty($_POST['name'])
                && !empty($_POST['phone'])
                && $emailOk
            ) {
                $email = mb_substr(trim($_POST['email']), 0, 50);

                $response['email_error'] = false;
                $response['password_error'] = false;
                $response['msg'] = '';

                //Add clients in rk_competition_clients
                competition::add_client($_POST['name'], $_POST['phone'], $_POST['client_id'], $_POST['email'], $_POST['comp_id']);

                //Add clients in rk_unsubscribe
                unsubscribe::add_self($_POST['email'], 'comp', 'roskosmetika.ru', $_POST['name']);
                $response['success'] = true;

            } else {

                $response['email_error'] = 0;

                if (!$emailOk) {
                    $response['email_error'] = 'Введите корректный E-mail';
                } else {
                    $response['all_error'] = 'Введите все поля';
                }

            }

//           $this->jsonResponse($response);
//            exit;
            echo json_encode($response);
            exit;
        } else {
            $this->nopage();
            //  no data
            //Template::set_page('service_info', self::PAGE_HEADER, 'Нам очень стыдно, но что-то случилось с нашим сайтом, попробуйте попозже.');
        }
    }
}