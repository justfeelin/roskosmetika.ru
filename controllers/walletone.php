<?php

class Controller_walletone extends Controller_base
{
    /**
     * Payment confirmation handler
     *
     * @param $arg
     */
    public function index($arg)
    {
        if (!isset($_POST['WMI_SIGNATURE'])) {
            $this->_response(false, 'Отсутствует параметр WMI_SIGNATURE');
        }

        if (empty($_POST['WMI_PAYMENT_NO']) || $_POST['WMI_PAYMENT_NO'] < 1) {
            $this->_response(false, 'Отсутствует или неправильный параметр WMI_PAYMENT_NO');
        }

        if (empty($_POST['WMI_PAYMENT_AMOUNT']) || $_POST['WMI_PAYMENT_AMOUNT'] < 1) {
            $this->_response(false, 'Отсутствует или неправильный параметр WMI_PAYMENT_AMOUNT');
        }

        if (!isset($_POST['WMI_ORDER_STATE'])) {
            $this->_response(false, 'Отсутствует параметр WMI_ORDER_STATE');
        }

        $params = $_POST;

        unset($params['WMI_SIGNATURE']);

        uksort($params, 'strcasecmp');

        $signature = base64_encode(pack('H*', md5(implode('', $params) . env('WALLETONE_KEY'))));

        if ($signature === $_POST['WMI_SIGNATURE']) {
            if (strtoupper($_POST['WMI_ORDER_STATE']) === 'ACCEPTED') {
                if (!preg_match('/^(\d+)-(\d+(?:\.\d+)?)-(\d+)$/', $_POST['WMI_PAYMENT_NO'], $m)) {
                    $this->_response(false, 'Неверное значение номера заказа');
                }

                $siteOrderNumber = (int)$m[1];

                $order = Orders::get_order_by_site_number($siteOrderNumber);

                if (!$order) {
                    $this->_response(false, 'Заказ Р-' . $siteOrderNumber . ' не найден');

                    return;
                }

                $ts = (int)$m[3];

                if (Payment::find($order['order_number'], (float)$m[2], $ts)) {
                    $this->_response(true, 'Заказ Р-' . $siteOrderNumber . ' оплачен!');
                }

                if ($order['status_id'] === Orders::STATUS_ID_CANCEL) {
                    Orders::mark_as_current_callback($order['order_number']);
                }

                Payment::add($order['order_number'], $_POST['WMI_PAYMENT_AMOUNT'], $ts);

                helpers::add_action_log($order['order_number'], helpers::RK_USER_ID, Payment::LOG_CODE_ID, 'платёж w1 ' . floatraw($_POST['WMI_PAYMENT_AMOUNT']));

                $user = $order['client_id'] ? Users::get_client_by_id((int)$order['client_id']) : null;

                @mailer::mail(
                    'Оплата заказа Р-' . $siteOrderNumber,
                    [
                        'sales@roskosmetika.ru' => 'Роскосметика',
                    ],
                    '2255483@mail.ru',
                    '<p>' .
                    'Заказ Р-' . $siteOrderNumber .
                    '<br>' .
                    'Оплаченная сумма: ' . $_POST['WMI_PAYMENT_AMOUNT'] .
                    ($user ? '<br>Клиент: ' . $user['surname'] . ' ' . $user['name'] . ' ' . $user['patronymic'] : '') .
                    '</p>'
                );

                $this->_response(true, 'Заказ Р-' . $siteOrderNumber . ' оплачен!');
            } else {
                $this->_response(false, 'Неверное состояние '. $_POST['WMI_ORDER_STATE']);
            }
        } else {
            $this->_response(false, 'Неверная подпись ' . $_POST['WMI_SIGNATURE']);
        }
    }

    /**
     * Outputs proper response
     *
     * @param string $ok WMI_RESULT is OK or not
     * @param string $description WMI_DESCRIPTION value
     */
    private static function _response($ok, $description)
    {
        print 'WMI_RESULT=' . ($ok ? 'OK' : 'RETRY') . '&' .
            'WMI_DESCRIPTION=' . urlencode($description);

        exit();
    }
}
