<?php

class Controller_cart extends Controller_base
{
    /**
     * Working with cart
     */
    const PAGE_HEADER = 'Корзина';

    /**
     * Show cart
     * @param array $args URL params
     */
    function index($args)
    {
        unset($_SESSION['popup_discount_bar']);

        Template::add_css('../assets/jquery-select2/select2.css');
        Template::add_css('../assets/jquery-select2/select2-bootstrap.css');
        Template::add_css('smoothness/jquery-ui-1.11.4.custom.min.css');
        Template::add_script('../assets/jquery-select2/select2.min.js');
        Template::add_script('../assets/jquery-select2/select2_locale_ru.js');
        Template::add_script('jquery-ui.min.js');

        $regions = Orders::get_regions(null, true);

        Template::set('regions', $regions);
        Template::set('shippings', Orders::$shippings);

        Cart::setInfo();

        $productIDs = [];

        for ($i = 0, $n = count(Cart::$data['products']); $i < $n; ++$i) {
            $productIDs[] = Cart::$data['products'][$i]['id'];
        }

        Template::set('brands_html', '');

        $id_list = agregation_products::get_personal_product_list(helpers::search_user_id(), 1, 4);

        //Template::set('mb_top_view', product::get_by_id(mindbox::get_recomendation('PoProsmotram4', FALSE, 4), true, true, false, false, false));
        Template::set('mb_top_view', product::get_by_id($id_list, true, true, false, false, false));

        Template::add_gtm_code('cart', Cart::$data['info']['promo_sum'], $productIDs);

        Template::set('product_ids', $productIDs);
        Template::add_admitad_code(Admitad::cart_page(Cart::$data['products']));
        //Template::add_addigital_code(Addigital::cart_page(Cart::$data['products']));
        //Template::add_criteo_code(criteo::cart(Cart::$data['products']));
        Template::add_mytarget_code(mytarget::init_checkout(Cart::$data));
        Template::add_vk_code(vk::init_checkout(Cart::$data));
        Template::set_page('cart', self::PAGE_HEADER);
    }

    /**
     * Updates product in cart
     *
     * @param array $args First parameter - product ID, second - update state: -1 to remove one item, 1 to add one item, 0 to completely remove product from cart
     */
    public function update($args)
    {
        if (isset($_SERVER['HTTP_AJAX']) && isset($args[1]) && $args[0] > 0 && in_array((int)$args[1], [-1, 0, 1], true)) {
            Cart::orderID(true);

            $prod_id = (int)$args[0];
            $set = (int)$args[1];

            list($region, $shippingTypeCode) = Orders::checkPostShipping();

            if ($region !== null && $shippingTypeCode !== null) {
                Cart::setShipping($region, $shippingTypeCode, true);
            }

            if ($prod_id) {
                switch ($set) {
                    case -1:
                        $json['success'] = !!Cart::subtract($prod_id);

                        break;

                    case 1:
                        $json['success'] = !!Cart::add_to_cart($prod_id);

                        if ($json['success'] && empty($_POST['noPopup'])) {
                            $json['added'] = Cart::get_item($prod_id);
                        }

                        break;

                    default:
                        $json['success'] = !!Cart::del_prod($prod_id);

                        break;
                }

                if ($json['success']) {
                    $order = Cart::setInfo(false);

                    $json['products'] = $order['products'];
                    $json['info'] = $order['info'];

                    if ($set === 1 && $json['info']['present'] && empty($_SESSION['cart-present'])) {
                        $_SESSION['cart-present'] = true;

                        $json['presentPopup'] = true;
                    }
                } else {
                    switch ($set) {
                        case -1:
                            $msg = 'удалить единицу товара из корзины';

                            break;

                        case 1:
                            $msg = 'добавить товар в корзину';

                            break;

                        default:
                            $msg = 'удалить товар из корзины';

                            break;
                    }

                    $json['msg'] = 'Невозможно ' . $msg;
                }
            } else {
                $json['success'] = false;
                $json['msg'] = 'Ошибка добавления в корзину.';
            }

            $this->jsonResponse($json);
        } else {
            $this->nopage(AJAX_REQUEST);
        }
    }

    /**
     * Outputs cart info on shipping type & region select
     */
    public function shipping()
    {
        if (AJAX_REQUEST && Cart::orderID()) {
            list($region, $shippingTypeCode) = Orders::checkPostShipping();

            Cart::setShipping($region, $shippingTypeCode, true);

            $order = Cart::setInfo(false);

            $this->jsonResponse($order['info']);
        } else {
            $this->nopage(true);
        }
    }

    /**
     * Clear all positions in crt.
     * @param array $args URL parameters
     */
    function clear($args)
    {
        if (isset($_SERVER['HTTP_AJAX'])) {     // ajax
            Cart::clear_cart();

            $this->jsonResponse([
                'succes' => true,
                'msg' => 'Корзина очищена.',
                'quantity' => 0,
                'cost' => 0,
                'discount' => 0,
                'discount_cost' => 0,
            ]);
        } else {                                // not ajax
            Cart::clear_cart();
            redirect('/cart');
        }
    }

    /**
     * Address autocomplete handler
     */
    public function address()
    {
        if (!AJAX_REQUEST) {
            $this->nopage(false);
        } else {
            $return = false;

            if (!empty($_GET['term'])) {
                $addresses = helpers::find_address($_GET['term']);

                if ($addresses && !empty($addresses[0])) {
                    $return = [];

                    for ($i = 0, $n = count($addresses); $i < $n; ++$i) {
                        if ($addresses[$i]['value']) {
                            $return[] = $addresses[$i]['value'];
                        }
                    }
                }
            }

            $this->jsonResponse($return);
        }
    }

    /**
     * Returns JSON-encoded SDEK points
     */
    public function sdek()
    {
        json_headers(7200);

        print cache_item(
            'sdek_points',
            function () {
                $response = remote_request(
                    'https://int.cdek.ru/pvzlist.php?type=ALL',
                    [],
                    false,
                    false,
                    10
                );

                if (!$response) {
                    return '';
                }

                $xml = new SimpleXMLElement($response);

                unset($response);

                $points = [];

                foreach ($xml->Pvz as $xmlPoint) {
                    if ((string)$xmlPoint['Type'] !== 'PVZ') {
                        continue;
                    }

                    $points[] = [
                        'lat' => (float)((string)$xmlPoint['coordY']),
                        'lng' => (float)((string)$xmlPoint['coordX']),
                        'address' => (string)($xmlPoint['City']) . ', ' . (string)$xmlPoint['Address'],
                        'phone' => (string)$xmlPoint['Phone'],
                        'work' => (string)$xmlPoint['WorkTime'],
                        'name' => (string)$xmlPoint['Name'],
                        'type' => (string)$xmlPoint['Type'] === 'PVZ' ? 1 : 2,
                        'code' => (string)$xmlPoint['Code'],
                    ];
                }

                return json_encode($points);
            },
            7200// 2 hours
        );

        exit(0);
    }

    /**
     * Updates certificate price
     */
    public function certificate()
    {
        if (!AJAX_REQUEST || !Cart::orderID() || !isset($_POST['price']) || $_POST['price'] < 1) {
            $this->nopage(true);
        }

        $this->jsonResponse(Cart::setCertificatePrice((int)$_POST['price']) ? Cart::setInfo(false) : null);
    }


    /**
     * Updates certificate price
     */
    public function add_to_cart($args)
    {
        if(isset($_GET['yt-key']) && ($_GET['yt-key'] === env('YA_TURBO')) && isset($_GET['id']))
        {

            $id = (int)$_GET['id'];

            if(cart::add_to_cart($id))
            {
                redirect(DOMAIN_FULL . '/cart');
            } else
            {
                $this->nopage(); 
            }
             
        } else 
        {
           $this->nopage(); 
        }           
    }
}