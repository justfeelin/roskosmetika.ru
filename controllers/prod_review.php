<?php

class Controller_prod_review extends Controller_base
{
    /**
     * Part of page header.
     */
    const PAGE_HEADER = 'Отзыв о товаре';


    /**
     * show review for product
     * @param array $args Параметры URL.
     */
    function index($args)
    {

        $reviews = FALSE;

        if (isset($args[0])) {

            if (is_numeric($args[0])) {
                $item = (int)$args[0];
                $item = product::check_descr_id($item);
                $redirect = product::redirect($item);
                //  redirect
                if ($redirect) {
                    header('HTTP/1.1 301 Moved Permanently');
                    header("Location: /prod_review/$redirect");
                }
                $reviews = prod_review::get_all($item);

            } else {
                $item = $args[0];
                if (!get_magic_quotes_gpc()) $item = DB::mysql_secure_string($item);

                $item = product::get_id_by_url($item);

                if ($item) $reviews = prod_review::get_all($item);
            }

        } else {
            Template::set_page('products_page', 'Товары с отзывами', [
                'products' => Product::get_popular_pseudo_random(40, 'prod_review-page', 31536000),
                'crumbs' => [
                    [
                        'name' => 'Товары с отзывами',
                    ],
                ],
            ]);

            return;
        }

        //  output reviews
        if ($reviews && $reviews['reviews']) {
            Template::set('month', check::month());

            $id_list = agregation_products::get_personal_product_list(helpers::search_user_id(), 1, 4);

            //Template::set('mb_top_view', product::get_by_id(mindbox::get_recomendation('PoProsmotram4', FALSE, 4), true, true, false, false, false));
            Template::set('mb_top_view', product::get_by_id($id_list, true, true, false, false, false));

            Template::set_description(helpers::mb_ucfirst($reviews['product']['name']) . ' ' . ($reviews['product']['tm_name'] ?: $reviews['product']['tm_alt_name']) . ' – отзывы ' . helpers::grammatical_case($reviews['product']['name'], helpers::CASE_TYPE_PREPOSITIONAL_EXTENDED, true) . ' ' . ($reviews['product']['tm_alt_name'] ?: $reviews['product']['tm_name']) . ' от покупателей интернет-магазина Роскосметика', '', '');

            Template::set_page('all_product_reviews', 'Отзывы ' . helpers::grammatical_case($reviews['product']['name'], helpers::CASE_TYPE_PREPOSITIONAL_EXTENDED, true, false) . ' ' . ($reviews['product']['tm_name'] ?: $reviews['product']['tm_alt_name']) . ($reviews['product']['tm_alt_name'] ? ' (' . $reviews['product']['tm_alt_name'] . ')' : ''), $reviews);
        } //  output 404
        else {
            $this->nopage();
        }
    }

    /**
     * add review
     * @return json   json for ajax
     */
    function add_comment($args)
    {

        if (isset($_SERVER['HTTP_AJAX'])) {

            $our_email = DEBUG ? array(
                env('ADMIN_EMAIL') => 'Новый отзыв о товаре roskosmetika.ru (DEBUG)',
            ) : array(
                'roskosmetika_it@mail.ru' => 'Новый отзыв о товаре roskosmetika.ru',
                'roskosmetika_content@mail.ru' => 'Новый отзыв о товаре roskosmetika.ru',
            );

            $prod_id = trim($_POST['prod_id']);
            $name = trim($_POST['name']);
            $email = trim($_POST['email']);
            $city = trim($_POST['city']);
            $comment = trim($_POST['comment']);

            // check email
            if (empty($email)) {
                $msg['email'] = 'Забыли почту!';
            } elseif (!check::email($email)) {
                $msg['email'] = 'Что-то не так с почтой';
            } else {
                $values['email'] = $email;
            }

            // check comment
            if (empty($comment)) {
                $msg['comment'] = 'Забыли написать сам отзыв';
            } else {
                $values['comment'] = $comment;
            }

            // add new
            if (!isset($msg)) {
                $email = h($email);
                $name = h($name);
                $comment = h($comment);
                $city = h($city);

                if (prod_review::add_comment($prod_id, $email, $name, $comment, $city)) {
                    $response = array(
                        'success' => true,
                        'msg' => 'Ваш отзыв принят в обработку и будет показан после прохождения модерации.',
                        'errors' => false
                    );

                    //mail to managers
                    $from = array('sales@roskosmetika.ru' => 'Роскосметика');
                    $theme = 'Новый отзыв на товар';
                    $body = $this->get_msg_new_review($prod_id, $name, $email, $comment, $city);
                    @mailer::mail($theme, $from, $our_email, $body);

                } else {
                    $response = array(
                        'success' => false,
                        'msg' => 'Нам очень стыдно, но что-то случилось с нашим сайтом, попробуйте попозже.',
                        'errors' => false
                    );
                }

            } // Выводим снова форму оформаления записи на мастер-класс
            else {
                $response = array(
                    'success' => false,
                    'msg' => 'Вы ввели неправильные данные',
                    'errors' => $msg
                );
            }

            //  load json
            echo json_encode($response);
            exit;

        } else {
            //  no data
            Template::set_page('service_info', self::PAGE_HEADER, 'Нам очень стыдно, но что-то случилось с нашим сайтом, попробуйте попозже.');
        }
    }


    /**
     * New message to manager about
     * @param  int $prod_id product id
     * @param  string $name user's name
     * @param  string $email user's email
     * @param  string $comment User's comment
     * @param  string $city City of user
     * @return string          message to user
     */
    protected function get_msg_new_review($prod_id, $name, $email, $comment, $city)
    {

        $msg = "<html>
                    <head>
                        <title>Новый отзыв на товар</title>
                    </head>
                    <body>
                        <p>Добавлен новый отзыв о продукте <b>ID = $prod_id</b></p>";
        if ($name) $msg .= "<p>ФИО : <b>$name</b></p>";
        if ($city) $msg .= "<p>Откуда : <b>$city</b></p>";

        $msg .= "<p>E-mail: <b>$email</b></p>
                 <p>Отзыв: <b>$comment</b></p>
                 <br>
                 <p>Отзыв скрыт. Требуется модерация и  решение о публикации.</p>
                 </body>
                 </html>";

        return $msg;
    }

}

?>
