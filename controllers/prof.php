<?php

class Controller_prof Extends Controller_Base
{

    /**
     * Update cookie prof_manual flag
     * @param arrya $args URL parameters.
     */
    function index($args)
    {

        if (isset($_SERVER['HTTP_AJAX'])) {

            if (isset($_POST['prof']) && $_POST['prof'] == 1) {

                $current_val = 0; 

                if(isset($_COOKIE['prof_manual'])) {

                    $current_val = ($_COOKIE['prof_manual'] == 2 ? 1 : 2);
                    setcookie('prof_manual', $current_val, time()+60*60*24*180, '/', $_SERVER['HTTP_HOST'], false, true);

                } else {

                    setcookie('prof_manual', 2, time()+60*60*24*180, '/', $_SERVER['HTTP_HOST'], false, true);
                    $current_val = 2;

                }

                if($current_val) {

                    $response = array(
                        'success' => true,
                        'msg' => '<p align="center">Теперь Вам ' . ($current_val == 2 ? " доступен полный ассортимент&nbsp;товаров&nbsp;магазина.</p>" : " показываются только товары рекомендованные для профессионалов&nbsp;и&nbsp;организаций.</p>")
                    );

                } else {

                    $response = array(
                        'success' => false,
                        'msg' => 'Не удалось изменить режим отображения =('
                    );
                } 

            } else {

                $response = array(
                    'success' => false,
                    'msg' => 'Не удалось изменить режим отображения =('
                );

            } 

            //  load json
            echo json_encode($response);
            exit; 

        } else {
            //  no data
            Template::set_page('service_info', 'Профессиональный режим', 'Мы сожалеем, но Ваш браузер не поддерживает эту функцию. Отключен javascript.');
        }    
    }

    /**
     * Check and change flag TMs only for professional from front end data
     * @return bool true/false
     */
    function tm_prof_frontend($args) {

        if (isset($_SERVER['HTTP_AJAX'])) {

            if(!isset($_COOKIE['prof']) || !isset($_COOKIE['rk_id'])) {

                $user_id = 0;

                if ($_POST['phone'] !== 0) {
                    $phone = trim(DB::mysql_secure_string($_POST['phone']));
                    $phone_ok = check::phone($phone);

                    if ($phone_ok) {
                        $phone = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);
                        $phone = (mb_substr($phone, 0, 1) == 8 ? substr_replace($phone, '+7', 0, 1) : $phone);
                        $user_id = (int)DB::get_field('id', "SELECT id FROM clients WHERE REPLACE(REPLACE(REPLACE(phone, ' ',''), '(', ''), ')', '') = '$phone' LIMIT 1");
                    } 

                }

                if($_POST['email'] !== 0) {
                    $email = mb_substr(trim(DB::mysql_secure_string($_POST['email'])), 0, 50);
                    $email_ok = check::email($email);

                    if ($email_ok) {
                        $user_id = (int)DB::get_field('id', "SELECT id FROM clients WHERE email  = LOWER('$email') LIMIT 1");
                    } 
                } 
                
                if ($user_id) {

                    $user = users::get_client_by_id($user_id);
                    setcookie('prof', ($user['type_id'] == 3 ? 0 : 1), time()+60*60*24*180, '/', $_SERVER['HTTP_HOST'], false, true);
                    setcookie('rk_id', $user_id, time()+60*60*24*180, '/', $_SERVER['HTTP_HOST'], false, true);

                    $response = array(
                        'success' => true,
                        'msg' =>  ($user['type_id'] !== 3 ? 'ok_prof' : 'ok')
                    );
                } else {
                    
                    setcookie('prof', $user_id, time()+60*60*24*180, '/', $_SERVER['HTTP_HOST'], false, true);
                    $response = array(
                        'success' => false,
                        'msg' => 'no_uid'
                    );
                }


            } else {

                $response = array(
                    'success' => false,
                    'msg' => 'cookie_set'
                );

            }

            //  load json
            echo json_encode($response);
            exit;

        } else {
            //  no data
            Template::set_page('service_info', 'Профессиональный режим', 'Мы сожалеем, но Ваш браузер не поддерживает эту функцию. Отключен javascript.');
        }    

    }

}

?>