<?php

class Controller_order extends Controller_Base
{
    /**
     * Header of order page.
     */
    const PAGE_HEADER = 'Оформление заказа';

    /**
     * @var string Session key to be used with order ID for checking tracking codes set
     */
    private static $_tracking_key = 'set_track_';

    /**
     * Get new order.
     * @param array $arg URL params
     */
    function index($arg)
    {
        $dbOrderID = Cart::orderID();

        if (!$dbOrderID) {
            $this->nopage();

            return;
        }

        //  Adresses send from 
        $send_from = array('sales@roskosmetika.ru' => 'Роскосметика');

        // make order
        if (isset($_POST['flag_from_cart'])) {

            if (isset($_SERVER['HTTP_AJAX'])) {

                //  json params
                $response = array(
                    'success' => FALSE,
                    'fio_error' => FALSE,
                    'phone_error' => FALSE,
                    'errors' => 'Нам очень стыдно, но что-то случилось с нашим сайтом, попробуйте попозже. Если ошибка повторится - Вы всегда можете нам позвонить и оформить заказ по телефону.',
                    'order_id' => FALSE
                );

                $id = (int)$_POST['id'];
                $fio = trim($_POST['fio']);
                $email = trim($_POST['email']);
                $email = ((!empty($email) && check::email($email)) ? mb_substr($email, 0, 50) : '');
                $phone = trim($_POST['phone']);

                $etc = htmlspecialchars(strip_tags(trim($_POST['etc'])));

                if (strlen($etc) > 3000) $etc = (mb_substr($etc, 0, 3000));

                //   flag for organizations
                $organization = '';
                $organization = htmlspecialchars($organization);

                list($name, $surname, $patronymic) = helpers::parse_fio($fio);

                // check fio
                if (empty($fio)) {
                    $msg['fio'] = 'Представьтесь пожалуйста.';
                }

                // check phone
                if (empty($phone)) {
                    $msg['phone'] = 'Оставьте ваш телефон';
                } elseif (!check::phone($phone)) {
                    $msg['phone'] = 'Что-то не так с телефоном. Введите корректный номер телефона';
                }

                // if empty msg
                if (!isset($msg)) {
                    $note = '';

                    Cart::setInfo();

                    $productsN = count(Cart::$data['products']);

                    $certificateOnly = $productsN === 1 && Cart::$data['products'][0]['id'] === Product::CERTIFICATE_ID;

                    if (!$certificateOnly || !empty($_POST['deliverCertificate'])) {
                        list($region, $shippingTypeCode) = Orders::checkPostShipping();

                        $address = htmlspecialchars(strip_tags(trim($_POST['adress'])));

                        if (mb_strlen($address) > 4000) {
                            $address = mb_substr($address, 0, 4000);
                        }
                    } else {
                        $region = $shippingTypeCode = null;
                        $address = '';

                        if (Cart::$data['info']['region'] || Cart::$data['info']['shipping_price_original']) {
                            Cart::setShipping([
                                'id' => 0,
                            ], Orders::CUSTOM_SHIPPING, true);

                            Cart::setInfo();
                        }
                    }

                    $hasCertificate = $certificateOnly;

                    if (!$hasCertificate) {
                        for ($i = 0; $i < $productsN; ++$i) {
                            if (Cart::$data['products'][$i]['id'] === Product::CERTIFICATE_ID) {
                                $hasCertificate = true;

                                break;
                            }
                        }
                    }

                    if ($hasCertificate) {
                        $etc .= ($etc ? "\r\n\r\n" : '') . '!!! Есть сертификат !!!';
                    }

                    if ($region !== null && $shippingTypeCode !== null) {
                        Cart::setShipping($region, $shippingTypeCode, true);
                    }

                    if (!Cart::$data['products']) {
                        $this->jsonResponse([]);
                    }

                    //  Promo discount info
                    if (Cart::$data['info']['promo_discount'] > 0) {
                        $note .= "\r\nВнимание! Заказ с дополнительной скидкой по промо-коду! " . Cart::$data['info']['promo_discount'] . " процентов.";
                    }

                    $pickpoint = $sdek = null;

                    if (!empty($_POST['pickpoint']) && !empty($_POST['pickpoint_code'])) {
                        $pickpoint = $_POST['pickpoint_code'];

                        $etc .= ($etc ? "\r\n\r\n" : '') . 'Адрес Pick-Point: ' . $_POST['pickpoint'];

                        $address = $_POST['pickpoint'];
                    } else {
                        if (!empty($_POST['sdek']) && !empty($_POST['sdek_code'])) {
                            $sdek = $_POST['sdek_code'];

                            $etc .= ($etc ? "\r\n\r\n" : '') . 'Адрес СДЭК: ' . $_POST['sdek'];

                            $address = $_POST['sdek'];
                        }
                    }

                    $discount_sum = Cart::$data['info']['sum_with_discount'] - ceil(100 * Cart::$data['info']['sum_with_discount'] / (100 - Cart::$data['info']['discount']));

                    $clientID = Auth::is_auth();

                    if ($clientID && Cart::$data['info']['discount_code'] === Cart::OWN_REFERRAL_CODE) {
                        Cart::set_own_referral_discount(Cart::$data['info']['discount']);
                    }

                    list($order_id, $randomID) = Orders::new_order($name, $surname, $patronymic, $organization, $phone, $email, $region, $shippingTypeCode, $address, $etc, Cart::$data, $clientID, true, true, false, $pickpoint || $sdek ? ([$pickpoint ? 'pickpoint' : 'sdek', $pickpoint ?: $sdek]) : null);

                    if ($order_id) {
                        $_SESSION['order-' . $randomID] = true;
                        $_SESSION[self::$_tracking_key . $order_id] = true;

                        $productIDs = [];

                        for ($i = 0; $i < $productsN; ++$i) {
                            $productIDs[] = Cart::$data['products'][$i]['id'];
                        }

                        // Mail to client
                        if ($email != '') {
                            $content = [
                                    'order_id' => $randomID,
                                    'order_time' => date("H:i:s"),
                                    'order_date' => date("d.m.Y"),
                                    'products' => Cart::$data['products'],
                                    'discount_sum' => Cart::$data['info']['sum_with_discount'],
                                    'name' => $name,
                                    'surname' => $surname,
                                    'patronymic' => $patronymic,
                                    'organization' => $organization,
                                    'address' => $address,
                                    'phone' => $phone,
                                    'region' => $region ? $region['name'] : null,
                                    'duration' => $region ? (string)($region['duration_min'] . '-' . $region['duration_max'] . ' ' . helpers::num_ending((int)$region['duration_max'])) : NULL,
                                    'email' => $email,
                                    'id' => $id,
                                    'etc' => $note,
                                ] + Cart::$data['info'];

                            $content['shipping_info'] = ($content['shipping_price_original'] == NULL ? orders::shipping_info($content['final_sum']) : NULL);

                            $content['neighbours_html'] = Template::mail_neighbours(product::cart_neighbours($productIDs, false, 4));

                            $subject = 'Заказ в интернет-магазине Роскосметика';


                            $message_to_user = Template::mail_template('mail_order_user_confirm', 'Заказ в интернет-магазине Роскосметика', $content);

                            @mailer::mail($subject, $send_from, DEBUG ? env('ADMIN_EMAIL') : $email, $message_to_user);
                        }

                        Cart::orderID(0);

                        unset($_SESSION[Orders::CERTIFICATE_PAY_SESSION_KEY]);

                        if ($certificateOnly) {
                            $order = Orders::get_by_id($order_id);

                            $invoice = Payment::create_sberbank_invoice($order_id, $randomID, $order['cost']);

                            unset($order);

                            if ($invoice) {
                                $_SESSION[Payment::SBERBANK_SESSION_KEY] = $invoice['sberbank_id'];
                                $_SESSION[Orders::CERTIFICATE_PAY_SESSION_KEY] = $invoice['form_url'];
                            }
                        }

                        $response['products'] = Cart::$data['products'];
                        $response['discount'] = $discount_sum;
                        $response['sum'] = Cart::$data['info']['promo_sum'];
                        $response['shipping'] = Cart::$data['info']['shipping_price'];
                        $response['promo_discount'] = Cart::$data['info']['promo_discount'];
                        $response['success'] = TRUE;
                        $response['errors'] = FALSE;
                        $response['order_id'] = $randomID;
                        $response['order_number'] = $order_id;

                        $minboxPhone = $phone ? mindbox::prepare_phone($phone) : '';

                        $response['mindboxTicket'] = mindbox::auth_ticket($clientID ? mindbox::TICKET_TYPE_IDENTITY : ($email ? mindbox::TICKET_TYPE_EMAIL : mindbox::TICKET_TYPE_PHONE), $clientID ?: ($email ?: $minboxPhone));
                    }
                } else {
                    $fio = FALSE;
                    $phone = FALSE;

                    if (isset($msg['fio'])) $fio = $msg['fio'];
                    if (isset($msg['phone'])) $phone = $msg['phone'];

                    $response['success'] = FALSE;
                    $response['fio_error'] = $fio;
                    $response['phone_error'] = $phone;
                    $response['errors'] = 'Ошибка заполнения данных.';
                }

                $this->jsonResponse($response);
            } else {
                //  No AJAX
                Template::set_page('service_info', self::PAGE_HEADER, 'На Вашем браузере отключен javascript и в частности Ajax. Возможно у Вас старая версия браузера, попробуйте обновить Ваш браузер. Все равно не получилось? - звоните нам.');
            }

        } else {
            //  unactive order
            Template::set_page('service_info', self::PAGE_HEADER, 'Чтобы оформить заказ, перейдите в <a href="/cart">корзину</a>.');
        }
    }

    /**
     * One click buy handler
     */
    public function fast_order()
    {
        if (AJAX_REQUEST) {
            $result = false;

            if (
                !empty($_POST['phone']) && check::phone($_POST['phone'])
                && Cart::setInfo()
            ) {
                if (!empty($_POST['name'])) {
                    list($name, $surname, $patronymic) = helpers::parse_fio($_POST['name']);
                } else {
                    $name = $surname = $patronymic = '';
                }

                Orders::new_order(
                    $name,
                    $surname,
                    $patronymic,
                    '',
                    $_POST['phone'],
                    '',
                    0,
                    null,
                    '',
                    (Cart::$data['info']['present'] !== false ? '!!! Подарок за сумму заказа больше ' . Cart::$data['info']['present'] . " !!!\r\n\r\n\r\n" : '') .
                    '!!! Заказ в 1 клик !!!',
                    Cart::$data,
                    Auth::is_auth(),
                    'Заказ в 1 клик Роскосметика - '
                );

                Cart::orderID(0);

                $result = Cart::setInfo(false);
            }

            $this->jsonResponse($result);
        }

        $this->nopage(AJAX_REQUEST);
    }

    /**
     * End. Order OK/
     * @param array $args
     */
    function ok($args)
    {

        if (isset($args[0]) && isset($_SESSION['order-' . $args[0]])) {
            $siteID = (int)$args[0];

            if ($order = Orders::get_by_id($siteID, true)) {
                $order_price = Orders::get_order_products_sum($order['order_number']);
                $content = array('order_id' => $siteID, 'main' => '');

                $this->set_trade_tracking_code($order['order_number'], $order_price, $order['delivery']);

                Template::set('moscowRegion', $order['region_id'] == 77 || $order['region_id'] == 50);

                Template::set('month', check::month());
                Template::set('master_classes', mc::get_master_classes_list(true, 3));

                if (Auth::is_auth()) {
                    $referralCampaign = referral::campaign(0);

                    $referral = referral::referralInfo($referralCampaign['id'], $referralCampaign['user_discount'], $referralCampaign['referral_discount'], $referralCampaign['user_percentage_discount'], $referralCampaign['referral_percentage_discount']);
                } else {
                    $referral = null;
                }

                Template::set('referral', $referral);

                $content['payUrl'] = !empty($_SESSION[Orders::CERTIFICATE_PAY_SESSION_KEY]) ? $_SESSION[Orders::CERTIFICATE_PAY_SESSION_KEY] : null;

                // MD recomndation
                $products = Orders::get_order_by_number($order['order_number'], TRUE);

                if (count($products) > 0) {
                    $preID = array();

                    for ($i = 0; $i < count($products); $i++) {
                        $preID[] = $products[$i]['id'];
                    }

                    $id_list = agregation_products::get_personal_product_list(helpers::search_user_id(), 1, 4);

                    $content['mb_ok_page'] = product::get_by_id($id_list, true, true, false, false, false);

                    //$content['mb_ok_page'] = product::get_by_id(mindbox::get_recomendation('PoxozhieTovarySajt', implode(",", $preID), 4), true, true, false, false, false);
                }

                $text = Template::get_tpl('order_ok', '', $content);

                Template::add_script('jquery.plugin.min.js');
                Template::add_script('jquery.countdown.min.js');

                Template::set('noRecommendations', TRUE);

                Template::set_page('page', !$content['payUrl'] ? self::PAGE_HEADER : '', $text);
            } else {
                $this->nopage();
            }
        } else {
            $this->nopage();
        }
    }

    /**
     * Adds events to Metrica and Analytics event lists
     *
     * @param int $order_number Order number.
     * @param float $order_price Order price.
     * @param int $delivery Delivery price.
     * @return string
     */
    protected function set_trade_tracking_code($order_number, $order_price, $delivery)
    {   
        if (!isset($_SESSION[self::$_tracking_key . $order_number])) {
            return;
        }

        $order_items = Orders::get_order_by_number($order_number);

        unset($_SESSION[self::$_tracking_key . $order_number]);

        //  Admitad order code

        $admitad_uid = isset($_COOKIE['admitad_uid']) && check::sid(trim($_COOKIE['admitad_uid'])) ? trim($_COOKIE['admitad_uid']) : null;

        $admitad_promo = $admitad_uid ? null : Admitad::get_code_by_order_number($order_number);

        if ($admitad_uid || $admitad_promo) {

            if($admitad_uid) Template::add_admitad_code(Admitad::ok_page_new($order_number, $order_items, $admitad_uid));

            Admitad::add_admitad_order($order_number, $admitad_uid, $admitad_promo ? $admitad_promo['id'] : null);
        }

        $codes = Admitad::ok_page($order_number, $order_items, $order_price, $admitad_uid, $admitad_promo ? $admitad_promo['code'] : null);

        Template::add_admitad_code($codes['admitad_code']);

        if ($codes['admitad_counter']) {
            Template::set('admitad_counter', $codes['admitad_counter']);
        }

        //  Reteil Rocket
        //Template::add_rocket_code(retail_rocket::ok_page($order_number, $order_items));

        // Criteo
        //Template::add_criteo_code(criteo::ok_page($order_number, $order_items));
        
        // VK
        Template::add_vk_code(vk::purchase($order_items));

        // MyTarget
        Template::add_mytarget_code(mytarget::purchase($order_items, $order_price));

        $yandex = array(
            'order_id' => '' . $order_number,
            'order_price' => $order_price,
            'currency' => 'RUR',
            'exchange_rate' => 1,
            'goods' => array(),
        );

        // $inboxer = array(
        //     'orderID' => '' . $order_number,
        //     'price' => $order_price,
        //     'delivery' => (int)$delivery,
        //     'cart' => array()
        // );

        $productIDs = [];

        $sendpulse_order_items = array();

        for ($i = 0, $n = count($order_items); $i < $n; ++$i) {
            $name = htmlspecialchars($order_items[$i]['name']);
            $quantity = intval(intval($order_items[$i]['quantity']));

            $yandex['goods'][] = array(
                'id' => $order_items[$i]['id'],
                'name' => $name,
                'price' => $order_items[$i]['price'],
                'quantity' => intval($quantity),
            );


            $sendpulse_order_items['products'][$i] = array( 'id'          => $order_items[$i]['id'],
                                                            'name'        => helpers::str_prepare($name . ' ' . $order_items[$i]['pack']),
                                                            'quantity'    => $order_items[$i]['quantity'],
                                                            'final_price' => (int)$order_items[$i]['price']
                                                           );
            // $inboxer['cart'][] = array(
            //     'id' => $order_items[$i]['id'],
            //     'qnt' => intval($quantity),
            //     'price' => (int)$order_items[$i]['price']
            // );

            $productIDs[] = (int)$order_items[$i]['id'];

        }

        // Template::add_inboxer_code($inboxer);
        
        // Send data to sendpulse
        sendpulse::event_order($order_number, $sendpulse_order_items, 3);

        Template::add_gtm_code('purchase', $order_price, $productIDs);

        Template::addYandexEvent('make_order', $yandex);
    }

    /**
     * Make pre order
     * @param  array $args
     * @return JSON
     */
    function pre_order($args)
    {

        //  Adresses to send mail
        $send_to = DEBUG ? array(env('ADMIN_EMAIL') => 'DEBUG') : array('2255483@mail.ru' => 'Клиентский отдел roskosmetika.ru', 'roskosmetika_it@mail.ru' => 'Лукашин СА roskosmetika.ru');

        //  Adresses send from 
        $send_from = array('sales@roskosmetika.ru' => 'Роскосметика');

        if (isset($_SERVER['HTTP_AJAX'])) {
            // ajax

            $user_id = auth::is_auth();
            $fio = isset($_POST['name']) ? trim($_POST['name']) : null;

            $prod_id = (int)($_POST['prod_id']);
            $name = '';
            $surname = '';
            $patronymic = '';

            if (isset($_POST['check_mail']) && isset($_POST['check_sms'])) {
                $check_email = (int)($_POST['check_mail']);
                $check_sms = (int)($_POST['check_sms']);
            }else{
                $check_email = 1;
                $check_sms = 0;
            }

            $string_separate = explode(' ', $fio, 3);
            $fio_quan = count($string_separate);

            if ($fio_quan == 1) {
                if (isset($string_separate[0])) $name = trim($string_separate[0]);
            }
            if ($fio_quan == 2) {
                if (isset($string_separate[1])) $name = trim($string_separate[1]);
                if (isset($string_separate[0])) $surname = trim($string_separate[0]);
            }
            if ($fio_quan == 3) {
                if (isset($string_separate[1])) $name = trim($string_separate[1]);
                if (isset($string_separate[0])) $surname = trim($string_separate[0]);
                if (isset($string_separate[2])) $patronymic = trim($string_separate[2]);
            }

            $name = mb_substr($name, 0, 20);
            $surname = mb_substr($surname, 0, 20);
            $patronymic = mb_substr($patronymic, 0, 20);

            $phone = isset($_POST['phone']) ? mb_substr(trim($_POST['phone']), 0, 18) : null;

            if (DEBUG) {
                $email = env('ADMIN_EMAIL');
            } elseif (isset($_POST['phone'])) {
                $email = mb_substr(trim($_POST['email']), 0, 85);
            } else {
                $email = null;
            }


            //  check fio
            if (empty($fio)) {
                $msg['name'] = 'Представьтесь пожалуйста';
            } elseif (!Check::name($fio)) {
                $msg['name'] = 'Что-то не так с имененем';
            } else {
                $values['name'] = $fio;
            }

            // check phone
            if (empty($phone)) {
                $msg['phone'] = 'Введите Ваш контактный телефон. (Мы не шлем рекламные СМС)';
            } elseif (!Check::phone($phone)) {
                $msg['phone'] = 'Введите корректный контактный телефон';
            } else {
                $values['phone'] = $phone;
            }


            // check email
            if (empty($email)) {
                $msg['email'] = 'Забыли указать Email';
            } elseif (!Check::email($email)) {
                $msg['email'] = 'Что-то не так с email, проверьте и введите корректный адрес.';
            } else {
                $values['email'] = $email;
            }

            // check check_email
            if (!($check_email || $check_sms)) {
                $msg['check'] = 'Вы забыли выбрать тип оповещения';
            }


            $product = FALSE;

            if ($prod_id) {
                $product = Product::get_by_id($prod_id);
            }

            if ($product) {

                // add new
                if (!isset($msg)) {

                    if (orders::new_pre_order($prod_id, $user_id, $surname, $name, $patronymic, $email, $phone, $check_email, $check_sms)) {
                        $response = array(
                            'success' => true,
                            'msg' => 'Ваша заявка принята. Как только товар появится на складе мы сообщим Вам. Время ожидания не более месяца.',
                            'errors' => false
                        );

                    } else {
                        $response = array(
                            'success' => false,
                            'msg' => 'Нам очень стыдно, но что-то случилось с нашим сайтом, попробуйте попозже.',
                            'errors' => false
                        );
                    }

                    $last_id = Orders::get_max_pre_order_id();

                    //mail to managers
                    $theme = "Оформлен предзаказ в интернет-магазине Роскосметика";
                    $theme_to_user = "Ваша заявка принята";

                    $content = array('pre_order_id' => $last_id,
                        'pre_order_date' => date("d.m.Y"),
                        'name' => $name,
                        'surname' => $surname,
                        'patronymic' => $patronymic,
                        'phone' => $phone,
                        'email' => $email,
                        'src' => $prod_id . '.jpg',
                        'prod_name' => $product['name'],
                        'pack' => $product['pack'],
                        'url' => $product['url'],
                        'price' => $product['price'],
                        'available' => !!$product['available'],
                        'special_price' => $product['special_price'],
                        'prod_id' => $product['id'],
                        'check_email' => $check_email,
                        'check_sms' => $check_sms
                    );


                    $message_to_user = Template::mail_template('mail_pre_order_user', 'Ваша заявка принята', $content);

                    //  to user
                    @mailer::mail($theme_to_user, $send_from, $email, $message_to_user);

                    $body = "<html>
                                <head>
                                    <title>Оформлен предзаказ # $last_id в интернет-магазине Роскосметика</title>
                                </head>
                                <body>
                                    <p>Номер предзаказа - <b>$last_id</b></p>
                                    <p>Клиент <b>$surname $name $patronymic</b> оформил(а) предзаказ:</p>
                                    <p>{$product['name']} ({$product['pack']}). <b>ID = $prod_id</b>.</p>
                                    <p>Телефон: <b>$phone</b></p>
                                    <p>E-mail: <b>$email</b></p>
                                    <p>Ресурс: <b>Роскосметика.ру</b></p>" .
                        ($user_id ? "<p>Обратите ВНИМАНИЕ!!!<br>Заказ оформил наш клиент - <b>ID на Роскосметике - $user_id</b></p>" : '')
                        . "</body>
                         </html>";

                    @mailer::mail($theme, $send_from, $send_to, $body);

                } // Выводим снова форму быстрого заказа
                else {
                    $response = array(
                        'success' => false,
                        'msg' => 'Вы ввели неправильные данные',
                        'errors' => $msg
                    );
                }

                echo json_encode($response);
                exit();

            } else {
                //  no data
                Template::set_page('service_info', self::PAGE_HEADER, 'Нам очень стыдно, но что-то случилось с нашим сайтом, попробуйте попозже.');
            }
        } else {
            // not ajax
            Template::set_page('service_info', self::PAGE_HEADER, 'Мы сожалеем, но Ваш браузер не поддерживает эту функцию. Отключен javascript.');
        }

    }
}
