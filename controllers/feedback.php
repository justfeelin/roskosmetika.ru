<?php

class Controller_feedback extends Controller_base
{

    /**
     * feedback
     * @param array $args URL params
     */
    function index($args)
    {

        $page_header = 'Обратная связь';

        if (isset($_POST ['submit'])) {

            $contacts = mb_substr(trim($_POST ['contacts']), 0, 50);
            $message = trim($_POST ['message']);

            $msg = FALSE;
            $values = FALSE;


            // check contacts
            if (empty($contacts)) {
                $msg ['contacts'] = 'Забыли оставить контакт';
            } else {
                $isEmail = check::email($contacts);

                if (!$isEmail && !check::phone($contacts)) {
                    $msg ['contacts'] = 'Что-то не так с контактом';
                }

                $values ['contacts'] = $contacts;
            }

            //  check msg
            if (empty($message)) {
                $msg ['message'] = 'Введите текст сообщения:';
            } else {
                $values ['message'] = $message;
            }

            // send letter
            if (!$msg) {
                // add to base
                feedback::add($contacts, $message);

                $our_email = DEBUG ? array(env('ADMIN_EMAIL') => 'Обратная связь Roskosmetika.ru (DEBUG)') : array('2255483@mail.ru' => 'Обратная связь Roskosmetika.ru', 'roskosmetika_it@mail.ru' => 'Обратная связь Roskosmetika.ru');

                $from = array('sales@roskosmetika.ru' => 'Роскосметика');
                $body = $this->get_msg_to_mng($contacts, $message);
                @mailer::mail('CRM обработано. ' . $page_header, $from, $our_email, $body);

                Orders::insert_order(
                    '',
                    '',
                    '',
                    $isEmail ? $contacts : '',
                    $isEmail ? '' : $contacts,
                    Auth::is_auth(),
                    [
                        Orders::CONSULTATION_PRODUCT_ID,
                    ],
                    '!!! Заказ через обратную связь !!! Текст сообщения: ' . $message,
                    0,
                    '',
                    null,
                    null,
                    null,
                    null,
                    true
                );

                if ($isEmail) {
                    @mailer::mail(
                        'Обращение в интернет-магазин Роскосметика',
                        [
                            'sales@roskosmetika.ru' => 'Роскосметика',
                        ],
                        DEBUG ? env('ADMIN_EMAIL') : $contacts,
                        Template::mail_template('mail_text', '', '<p>Спасибо за обращение в интернет-магазин Роскосметика!</p><p>Ваш вопрос получен, мы обязательно свяжемся с Вами в течение ближайших трёх дней.</p><p>Ваше сообщение:<br>' . h($message) . '</p><p>С уважением,<br>Ваша Роскосметика</p>')
                    );
                }

                Template::set_page('service_info', $page_header, 'Ваше сообщение отправлено. В ближайшее время наши менеджеры Вам обязательно ответят.');
            } else {
                Template::set('main_spec', lines::main_special());
                Template::set('crumb', 'Обратная связь');
                Template::set_page('feedback', $page_header);
                Template::set_form($values, $msg);
            }
        } // Выводим форму отбратной связи
        else {
            Template::set('crumb', 'Обратная связь');
            Template::set('main_spec', lines::main_special());
            Template::set_page('feedback', $page_header);
        }
    }


    /**
     * Generate msg to managers
     * @param  string $contacts users contats
     * @param  string $message users message
     * @return string           html msg
     */
    protected function get_msg_to_mng($contacts, $message)
    {

        $text = "<html>
                    <head>
                        <title>Новое сообщение (обратная связь)</title>
                    </head>
                    <body>
                        <p>С формы обратной связи и помощи пришло следующее сообщение:</p>
                        <p>$message</p>
                        <p><b>Контакты:</b></p>
                        <p>$contacts</p>
                    </body>
               </html>";
        return $text;
    }


}

?>