<?php

class Controller_category__tm extends Controller_Base
{
    public function index($args)
    {
        if (!isset($args[0])) {
            Template::set_page('products_page', 'Разделы по брендам', [
                'products' => Product::get_popular_pseudo_random(40, 'category-tm-page', 31536000),
                'crumbs' => [
                    [
                        'name' => 'Разделы по брендам',
                    ],
                ],
            ]);

            return;
        }

        $url = tm::parse_category_tm_url($args);

        if ($url) {
            if ($data = tm::output_tm_in_category($url['tm_id'], $url['category_ids'][$url['n'] - 2], $url['n'] - 1, $url['n'] > 2 ? $url['category_ids'][$url['n'] - 3] : 0 , AJAX_REQUEST)) {
                $category_url = implode('/', array_slice($args, 0, $url['n'] - 1));

                Pagination::setBaseUrl('/category-tm/' . $category_url . '/' . $url['tm_url']);
                Pagination::setTemplateSettings();

                if (AJAX_REQUEST) {
                    Template::set('products', $data['products']);

                    $this->jsonResponse(array(
                        'found' => true,
                        'products' => Template::get_tpl('product_list'),
                        'pagination' => Template::get_tpl('pagination'),
                        'count' => Pagination::getCount(),
                        'filters' => $data['filters'],
                        'prices' => $data['filter_prices'],
                    ));
                }

                Template::set('categories_html', Template::get_tpl('category_tm_subcategories', null, array(
                    'categories' => $data['categories'],
                    'category' => $data['category'],
                    'subcategory_url' => $url['n'] < 4 ? $category_url : implode('/', array_slice($args, 0, $url['n'] - 2)),
                    'tm' => $data['tm'],
                    'category_url' => $category_url,
                    )));
                Template::set('found', Pagination::getCount());
                Template::set('category_url', $category_url);

                $breadcrumbs_array[] = array('name' => $data['tm']['name'], 'url' => '/tm/' . $data['tm']['url'], 'one_level' => 1);
                $breadcrumbs_array[] = array('name' =>  $data['h1']);
                Template::set('breadcrumbs', Categories::breadcrumbs($url['category_ids'][$url['n'] - ($url['n'] < 4 ? 2 : 3)], '/category/', $breadcrumbs_array, TRUE));
                Template::set_description($data['title'], $data['seo_desc'], $data['keywords'], null, helpers::open_graph(og::tm($data, TRUE)));
                //Template::add_criteo_code(criteo::product_list(array_slice($data['products'], 0, 3)));
                Template::add_vk_code(vk::view_category($data, TRUE));
                Template::set_page('category_tm', $data['tm']['name'], $data);
            } else {
                $url = false;
            }
        }

        if (!$url) {
            if (!AJAX_REQUEST) {
                $this->nopage();
            } else {
                $this->jsonResponse(array(
                    'found' => false,
                    'products' => '<h2>Ничего не найдено</h2>',
                ));
            }
        }
    }
}
