<?php

class Controller_master_classes extends Controller_base
{
    /**
     * Page header.
     */
    const PAGE_HEADER = 'Обучающие мастер-классы';


    /**
     * View master-classes page
     * @param array $args Параметры URL.
     */
    function index($args)
    {
        //  no pages
        if (!empty($args)) {
            $this->nopage();
        } else {
            $master_classes_description = Pages::get_page('master-class', false);
            $master_classes_timetable = mc::get_master_classes_list();
            Template::set('mc_down', Pages::get_page('master-classes-down', false));
            Template::set('month', Check::month());
            Template::set('master_classes_timetable', $master_classes_timetable);

            $id_list = agregation_products::get_personal_product_list(helpers::search_user_id(), 1, 4);

            //Template::set('mb_top_view', product::get_by_id(mindbox::get_recomendation('PoProsmotram4', FALSE, 4), true, true, false, false, false));
            Template::set('mb_top_view', product::get_by_id($id_list, true, true, false, false, false));
            Template::set_description($master_classes_description['title'], $master_classes_description['description'], $master_classes_description['keywords']);
            Template::set('crumb', 'Мастер-классы');
            Template::set_page('master_classes', $master_classes_description['header'], $master_classes_description['content']);
        }
    }

    /**
     * Subscribe new user
     * @return json   json for ajax
     */
    function subscribe($args)
    {

        if (isset($_SERVER['HTTP_AJAX'])) {
            $our_email = DEBUG ? array(
                env('ADMIN_EMAIL') => 'Мастер-классы (DEBUG)',
            ) : array(
                'roskosmetika_it@mail.ru' => 'мастер-классы',
                '2255483@mail.ru' => 'Отдел продаж Роскосметика',
                'setlana79579@yandex.ru' => 'setlana79579@yandex.ru',
            );
            $page_header = 'Запись на мастер-класс';

            $id = auth::is_auth();
            $fio = trim($_POST['name']);
            $name = '';
            $surname = '';
            $patronymic = '';

            $string_separate = explode(' ', $fio, 3);
            $fio_quan = count($string_separate);

            if ($fio_quan == 1) {
                if (isset($string_separate[0])) $name = trim($string_separate[0]);
            }
            if ($fio_quan == 2) {
                if (isset($string_separate[1])) $name = trim($string_separate[1]);
                if (isset($string_separate[0])) $surname = trim($string_separate[0]);
            }
            if ($fio_quan == 3) {
                if (isset($string_separate[1])) $name = trim($string_separate[1]);
                if (isset($string_separate[0])) $surname = trim($string_separate[0]);
                if (isset($string_separate[2])) $patronymic = trim($string_separate[2]);
            }

            $name = mb_substr($name, 0, 20);
            $surname = mb_substr($surname, 0, 20);
            $patronymic = mb_substr($patronymic, 0, 20);
            $phone = mb_substr(trim($_POST['phone']), 0, 16);
            $email = DEBUG ? env('ADMIN_EMAIL') : mb_substr(trim($_POST['email']), 0, 50);
            $master_class_id = (int)$_POST['mc_id'];


            //  check fio
            if (empty($fio)) {
                $msg['name'] = 'Представьтесь пожалуйста';
            } elseif (!Check::name($fio)) {
                $msg['name'] = 'Что-то не так с имененем';
            } else {
                $values['name'] = $fio;
            }

            // check phone
            if (empty($phone)) {
                $msg['phone'] = 'Введите Ваш контактный телефон';
            } elseif (!Check::phone($phone)) {
                $msg['phone'] = 'Введите корректный контактный телефон';
            } else {
                $values['phone'] = $phone;
            }


            // Check e-mail
            if (!empty($email)) {
                if (!Check::email($email)) {
                    $msg['email'] = 'Введите корректный E-mail:';
                } else {
                    $values['email'] = $email;
                }
            } else {
                $msg['email'] = 'Введите Ваш Email адрес';
            }


            // add new
            if (!isset($msg)) {

                if (mc::add_subscriber($name, $surname, $patronymic, $email, $phone, 0, $master_class_id, $id)) {
                    $response = array(
                        'success' => true,
                        'msg' => 'Вы удачно записались на мастер-класс.',
                        'errors' => false
                    );
                } else {
                    $response = array(
                        'success' => false,
                        'msg' => 'Нам очень стыдно, но что-то случилось с нашим сайтом, попробуйте попозже.',
                        'errors' => false
                    );
                }

                $mc_description = mc::get_master_class($master_class_id);


                $mc_info = array('user' => "$name",
                    'date' => date('d.m.Y. Начало в H:i', strtotime($mc_description['date'])),
                    'mc_name' => $mc_description['name'],
                    'webinar' => $mc_description['webinar'],
                    'mc_type' => ($mc_description['webinar'] ? 'вебинар' : 'мастер-класс'),
                    'new_address' => $mc_description['new_address']);

                //mail to managers
                $from = array('sales@roskosmetika.ru' => 'Роскосметика');
                $theme = 'Запись на мастер-класс';
                $body = $this->get_msg_masterclass($id, $name, $surname, $patronymic, $phone, $email, $mc_description['date'], $mc_description['name']);
                @mailer::mail($theme, $from, $our_email, $body);

                //mail to client
                if (!empty($email)) {
                    $message_user = Template::mail_template('mail_confirm_mc', $page_header, $mc_info);
                    @mailer::mail($theme, $from, $email, $message_user);
                }
            } // Выводим снова форму оформаления записи на мастер-класс
            else {
                $response = array(
                    'success' => false,
                    'msg' => 'Вы ввели неправильные данные',
                    'errors' => $msg
                );
            }

            //  load json
            echo json_encode($response);
            exit;

        } else {
            //  no data
            Template::set_page('service_info', self::PAGE_HEADER, 'Нам очень стыдно, но что-то случилось с нашим сайтом, попробуйте попозже.');
        }
    }

    /**
     * Generate message if client sing up for masterclass.
     * @param int $id Client ID.
     * @param string $name Client name.
     * @param string $surname Client surname.
     * @param string $patronymic Client patronymic.
     * @param string $phone Client phone.
     * @param string $email Client email.
     * @param string $region Client region.
     * @param date $master_class_date Дата проведения мастер-класса.
     * @return string Email message for sing up for masterclass.
     */
    protected function get_msg_masterclass($id, $name, $surname, $patronymic, $phone, $email, $master_class_date, $master_class_name)
    {
        $msg = "<html>
                    <head>
                        <title>Заявка на мастер-класс.</title>
                    </head>
                    <body>
                        <p><b>$surname $name $patronymic</b> проявил(а) желание принять участи в мастер-классе \"$master_class_name\" " . date('d.m.Y', strtotime($master_class_date)) . "</p>";
        if ($id) {
            $msg .= "<p>ID клинета: <b>$id-R</b></p>";
        }
        $msg .= "<p>Контактный телефон: <b>$phone</b></p>";
        if ($email) {
            $msg .= "<p>E-mail: <b>$email</b></p><p>Ресурс: <b>Роскосметика.ру</b></p>";
        }
        $msg .= "</body>
             </html>";
        return $msg;
    }

}
