<?php

class Controller_country extends Controller_base
{
    /**
     * Type pages list
     *
     * @param $arg
     */
    public function index($arg)
    {
        if (!isset($arg[0])) {
            Template::set_description(
                'Косметика - купить в интернет-магазине из Москвы по хорошей цене. Заказать косметику с доставкой из каталога Роскосметика',
                'Косметика от интернет магазина Роскосметика – это множество товаров по выгодной цене, удобные способы оплаты и оперативная доставка, включая возможность самовывоза!',
                ''
            );

            Template::set_page('country', 'Страны-производители', [
                'countries' => country::get_countries(),
            ]);
        } else {
            $baseUrl = $h1 = $title = $description = $view = null;

            if (!isset($arg[1])) {
                $data = country::output($arg[0], AJAX_REQUEST);
                //vd($arg[0]);exit();
                if ($data) {
                    $baseUrl = $data['country']['url'];

                    if (!AJAX_REQUEST) {
                        $h1 = $data['country']['h1'];
                        $title = $data['country']['title'];
                        $description = $data['country']['description'];

                        $view = 'country_page';
                    }
                }
            }

            if (!isset($data)) {
                if (!AJAX_REQUEST) {
                    $this->nopage();

                    return;
                } else {
                    $this->jsonResponse([
                        'found' => false,
                        'products' => '<h2>Ничего не найдено</h2>',
                    ]);
                }
            }

            Pagination::setBaseUrl('/country/' . $baseUrl);
            Pagination::setTemplateSettings();

            if (!AJAX_REQUEST) {
                Template::set_description($title, $description, '');

                Template::set_page($view, $h1, $data);
            } else {
                Template::set('products', $data['products']);

                $this->jsonResponse(
                    [
                        'found' => true,
                        'products' => Template::get_tpl('product_list'),
                        'pagination' => Template::get_tpl('pagination'),
                        'count' => Pagination::getCount(),
                        'filters' => $data['filters'],
                        'prices' => $data['filter_prices'],
                    ] + (
                        isset($data['filter_tms'])
                            ? [
                                'tms' => $data['filter_tms'],
                            ]
                            : []
                    ) + (
                        isset($data['filter_countries'])
                            ? [
                                'countries' => $data['filter_countries'],
                            ]
                            : []
                    )
                );
            }
        }
    }
}
