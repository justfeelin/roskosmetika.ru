<?php

class Controller_login extends Controller_Base
{

    /**
     * Autorization
     * @param array $args
     */
    function index($args)
    {
        $user = FALSE;
        $password = FALSE;
        $isEmail = false;

        if (
            !empty($_POST['user'])
            && (
                ($isEmail = check::email($_POST['user']))
                || check::phone($_POST['user'])
            )
        ) {
            $user = mb_substr(trim($_POST['user']), 0, 50);
        }

        if (isset($_POST['password']) && check::password($_POST['password'])) {
            $password = mb_substr(trim($_POST['password']), 0, 25);
        }

        if (isset($_SERVER['HTTP_AJAX'])) {
            $json['user_error'] = 0;
            $json['password_error'] = 0;
            $json['msg'] = 0;

            if ($user && $password) {
                $clientID = Auth::login($user, $password, $isEmail);

                $json['success'] = !!$clientID;

                if (!$json['success']) {
                    $json['msg'] = 'Не удалось выполнить вход, проверьте введённые данные';
                } else {
                    $user_info = Users::get_user_info();
                    $json['name'] = $user_info['surname'] . ' ' . $user_info['name'] . ' ' . $user_info['patronymic'];

                    $region = $user_info['region_id'] ? Orders::get_regions($user_info['region_id']) : null;

                    $json['region'] = $region ? $region['name'] : '';
                    $json['phone'] = $user_info['phone'];
                    $json['email'] = $user_info['email'];
                    $json['id'] = $clientID;
                }
            } else {
                $json['success'] = false;

                if (!$user) {
                    $json['user_error'] = 'Введите корректный E-mail или телефон';
                }

                if (!$password) {
                    $json['password_error'] = 'Введите корректный пароль';
                }
            }

            $this->jsonResponse($json);
        } else {
            if ($user && $password) {
                Auth::login($user, $password, $isEmail);
            }

            redirect('/');
        }
    }

    /**
     * vk.com login page
     */
    public function vk()
    {
        if (Auth::is_auth()) {
            redirect('/room');
        }

        $sessionKey = 'vk_login_redirect';

        if (!empty($_GET['r'])) {
            $_SESSION[$sessionKey] = $_GET['r'];
        }

        $info = vk::get_token(DOMAIN_FULL . '/login/vk', 'email', true);

        if (!empty($_GET['r'])) {
            print $info;

            exit(0);
        }

        if (empty($info['email'])) {
            Template::set_page(
                'service_info',
                'Авторизация через vk.com',
                '<p>Вы запретили доступ к вашему E-mail.</p><p>Доступ к вашему E-mail нам необходим для того чтобы мы могли Вас узнать и ни с кем не перепутать.</p>'
            );

            return;
        }

        if (
            empty($info['user_id'])
            || !(
                $userInfo = remote_request(
                        'https://api.vk.com/method/users.get',
                        [
                            'access_token' => $info['access_token'],
                            'v' => '5.60',
                        ],
                        false,
                        true,
                        5
                    )
            )
            || !isset($userInfo['response'][0]['first_name'])
        ) {
            $this->nopage();

            return;
        }

        $this->_socialLogin($info['email'], $userInfo['response'][0]['first_name'], $userInfo['response'][0]['last_name'], $info['user_id'], 'vk', $sessionKey);
    }

    /**
     * Facebook login page
     */
    public function fb()
    {
        if (Auth::is_auth()) {
            redirect('/room');
        }

        require_once SITE_PATH . 'includes/Facebook/autoload.php';

        $appID = env('FB_APP_ID');
        $appSecret = env('FB_APP_SECRET');

        $redirectURI = DOMAIN_FULL . '/login/fb';

        $sessionKey = 'fb_login_redirect';

        if (!empty($_GET['r'])) {
            $_SESSION[$sessionKey] = $_GET['r'];

            $fb = new Facebook\Facebook([
                'app_id' => $appID,
                'app_secret' => $appSecret,
                'default_graph_version' => 'v2.2',
            ]);

            $helper = $fb->getRedirectLoginHelper();

            $loginUrl = $helper->getLoginUrl(
                $redirectURI,
                [
                    'email',
                ]
            );

            print $loginUrl;

            exit(0);
        }

        $fb = new Facebook\Facebook([
            'app_id' => $appID,
            'app_secret' => $appSecret,
            'default_graph_version' => 'v2.2',
        ]);

        $helper = $fb->getRedirectLoginHelper();

        $accessToken = null;

        try {
            $accessToken = $helper->getAccessToken();
        } catch(Exception $e) {
        }

        if (
            !$accessToken
            || !($info = $fb->get('/me?fields=id,email,first_name,last_name', $accessToken)->getGraphUser())
        ) {
            $this->nopage();

            return;
        }

        $email = $info->getEmail();

        if (!$email) {
            Template::set_page(
                'service_info',
                'Авторизация через Facebook',
                '<p>К сожалению, в вашем профиле на Facebook не указан E-mail, необходимый для авторизации.</p><p>Пожалуйста, воспользуйтесь обычной формой входа/регистрации.</p>'
            );

            return;
        }

        $this->_socialLogin($email, $info->getFirstName(), $info->getLastName(), $info->getId(), 'fb', $sessionKey);
    }

    /**
     * ok.ru login page
     */
    public function ok()
    {
        if (Auth::is_auth()) {
            redirect('/room');
        }

        $appID = env('OK_APP_ID');
        $appPrivateKey = env('OK_APP_PRIVATE_KEY');

        $redirectURI = DOMAIN_FULL . '/login/ok';

        $sessionKey = 'ok_login_redirect';

        if (!empty($_GET['r'])) {
            $_SESSION[$sessionKey] = $_GET['r'];

            print 'https://connect.ok.ru/oauth/authorize?client_id=' . $appID . '&scope=VALUABLE_ACCESS,GET_EMAIL&response_type=code&redirect_uri=' . rawurlencode($redirectURI);

            exit(0);
        }

        if (!empty($_GET['error'])) {
            redirect('/');

            return;
        }

        if (empty($_GET['code'])) {
            $this->nopage();

            return;
        }

        $info = remote_request(
            'https://api.ok.ru/oauth/token.do?' . http_build_query(
                [
                    'client_id' => $appID,
                    'client_secret' => $appPrivateKey,
                    'code' => $_GET['code'],
                    'redirect_uri' => $redirectURI,
                    'grant_type' => 'authorization_code',
                ]
            ),
            []
            ,
            true,
            true,
            5
        );

        if (
            !$info
            || !empty($info['error'])
            || empty($info['access_token'])
        ) {
            $this->nopage();

            return;
        }

        $params = $sigParams = [
            'application_key' => env('OK_APP_PUBLIC_KEY'),
        ];

        $params['access_token'] = $info['access_token'];

        $sigString = '';

        ksort($sigParams);

        foreach ($sigParams as $key => $value) {
            $sigString .= $key . '=' . $value;
        }

        $sigString .= strtolower(md5($info['access_token'] . $appPrivateKey));

        $userInfo = remote_request(
            'https://api.ok.ru/api/users/getCurrentUser',
            $params + [
                'sig' => strtolower(md5($sigString)),
            ],
            false,
            true,
            5
        );

        if (empty($userInfo['email'])) {
            Template::set_page(
                'service_info',
                'Авторизация через Одноклассниках',
                '<p>К сожалению, в вашем профиле в Одноклассниках не указан E-mail, необходимый для авторизации.</p><p>Пожалуйста, воспользуйтесь обычной формой входа/регистрации.</p>'
            );

            return;
        }

        $this->_socialLogin($userInfo['email'], $userInfo['first_name'], $userInfo['last_name'], $userInfo['uid'], 'ok', $sessionKey);
    }

    /**
     * Performs client login/register based on social network's data
     *
     * @param string $email Client email
     * @param string $name Client name
     * @param string $surname Client surname
     * @param int $socialID Client social network's user ID
     * @param string $networkKey Social network key ('vk', 'fb', 'ok')
     * @param string $sessionKey Session key storing redirect URL
     */
    private function _socialLogin($email, $name, $surname, $socialID, $networkKey, $sessionKey)
    {
        $user = Users::get_by_email($email);

        if (!$user) {
            $password = random_string(6);

            $clientID = Users::insert_user($email, $password, $networkKey === 'vk' ? $socialID : 0, $networkKey === 'fb' ? $socialID : 0, $networkKey === 'ok' ? $socialID : 0);

            Users::update_info($clientID, $name, $surname, null);

            @mailer::mail(
                'Регистрация в интернет-магазине Роскосметика',
                [
                    'sales@roskosmetika.ru' => 'Роскосметика',
                ],
                DEBUG ? env('ADMIN_EMAIL') : $email,
                Template::mail_template(
                    'mail_registration',
                    'Регистрация в интернет-магазине Роскосметика',
                    [
                        'email' => $email,
                        'password' => $password,
                    ]
                )
            );
        } else {
            $socialKey = $networkKey === 'vk' ? 'vk_id' : ($networkKey === 'fb' ? 'fb_id' : 'ok_id');

            if (!$user[$socialKey]) {
                Users::set_social_id($user['id'], $networkKey, $socialID);
            }

            $password = $user['password'];
        }

        Auth::login($email, $password);

        $url = !empty($_SESSION[$sessionKey]) ? $_SESSION[$sessionKey] : '/';

        unset($_SESSION[$sessionKey]);

        redirect(DOMAIN_FULL . $url);
    }
}
