<?php

/**
 * Manage mailer
 */
class Controller_unsubscribe Extends Controller_Base
{


    //  page header
    const PAGE_HEADER = 'Почтовая рассылка';

    /**
     * Unsubscribe mailer
     * @param  array $args URL parameters
     */
    function index($args)
    {

        if (isset($args[0]) && isset($args[1])) {
            $email = mb_strtolower($args[0]);
            $hash = $args[1];
            if (get_magic_quotes_gpc()) {
                $email = DB::mysql_secure_string($email);
                $hash = DB::mysql_secure_string($hash);
            }

            Template::set('email', $email);
            Template::set('hash', $hash);
        } else {
            $msg = 'Ссылка некорректна!';
            Template::set('msg', $msg);
        }

        $id_list = agregation_products::get_personal_product_list(helpers::search_user_id(), 1, 4);

        Template::set('mb_top_view', product::get_by_id($id_list, true, true, false, false, false));

        //Template::set('mb_top_view', product::get_by_id(mindbox::get_recomendation('PoProsmotram4', FALSE, 4), true, true, false, false, false));
        Template::set_page('unsubscribe', self::PAGE_HEADER);
    }

    /**
     * Delete from mailer
     * @param array $args URL parameters
     */
    function add_unsubscribe($args)
    {

        if (isset($args[0]) && isset($args[1])) {
            $email = $args[0];
            $hash = $args[1];
        } else {
            $email = 0;
            $hash = 0;
        }

        if (unsubscribe::change_active($email, $hash)) {
            $msg = 'Вы отказались от рассылки.';
        } else {
            $msg = 'Ошибка обработки! Попробуйте повторить свой запрос позже.';
        }
        Template::set('msg', $msg);

        $id_list = agregation_products::get_personal_product_list(helpers::search_user_id(), 1, 4);

        Template::set('mb_top_view', product::get_by_id($id_list, true, true, false, false, false));
        //Template::set('mb_top_view', product::get_by_id(mindbox::get_recomendation('PoProsmotram4', FALSE, 4), true, true, false, false, false));
        Template::set_page('unsubscribe', 'Почтовая рассылка');
    }


    /**
     * restart mailer
     * @param array $args URL params
     */
    function set_active($args)
    {

        if (isset($args[0]) && isset($args[1])) {

            $email = mb_strtolower($args[0]);
            $hash = $args[1];

            if (unsubscribe::set_active($email, $hash)) {
                $return_source = unsubscribe::check_existence($email);
                $source = $return_source['source'];
                Template::set('msg', "<p>Вы подписались на информационную рассылку с ресурса <a href = \"http://$source/\"><strong>$source</strong></a>.<br>Ваш E-mail: <strong>$email</strong></p>");
            } else {
                Template::set('msg', '<p>Ошибка обработки! Попробуйте повторить свой запрос позже.</p>');
            }
        } else {
            Template::set('msg', 'Ссылка некорректна!');
        }
        $id_list = agregation_products::get_personal_product_list(helpers::search_user_id(), 1, 4);
        Template::set('mb_top_view', product::get_by_id($id_list, true, true, false, false, false));

        //Template::set('mb_top_view', product::get_by_id(mindbox::get_recomendation('PoProsmotram4', FALSE, 4), true, true, false, false, false));
        Template::set_page('unsubscribe', self::PAGE_HEADER);
    }


    /**
     * Add to mailer
     * @return array URL parameters
     */
    function sub()
    {

        if (isset($_SERVER['HTTP_AJAX'])) {

            //$our_email = array('2255483@mail.ru' => 'Отдел продаж Роскосметика'); 
            $our_email = DEBUG ? array(env('ADMIN_EMAIL') => 'Подписка на рассылку (DEBUG)') : array('roskosmetika_it@mail.ru' => 'Подписка на рассылку');
            $from = array('sales@roskosmetika.ru' => 'Роскосметика');

            $email = $_POST['sub'];

            // Check e-mail
            if (empty($email)) {
                $msg['email'] = 'Забыли e-mail';
            } elseif (!Check::email($email)) {
                $msg['email'] = 'Введите корректный E-mail:';
            } else {
                $values['email'] = $email;
            }

            if (!isset($msg)) {
                if (@unsubscribe::check_existence($email)) {
                    $return_source = unsubscribe::check_existence($email);
                    $active = (bool)$return_source['active'];
                    $hash = $return_source['hash'];

                    if ($return_source['status'] == 'spam') {
                        unsubscribe::change_number($email, 0);
                        unsubscribe::change_source($email, 'roskosmetika.ru');
                        unsubscribe::change_status($email, 'sub');
                    }

                    if ($active) {
                        $response = array(
                            'success' => true,
                            'msg' => 'Указанный email уже подписан на нашу рассылку',
                            'errors' => false,
                            'type' => 'just'
                        );
                    } else {
                        $response = array(
                            'success' => true,
                            'msg' => 'Вы отказались от подписки на нашу рассылку по указанному email',
                            'errors' => false,
                            'type' => 'next_one',
                            'link' => "/unsubscribe/set_active/$email/$hash"
                        );
                    }
                } else {

                    if (@unsubscribe::add_new_email($email, 'sub', -1, 'roskosmetika.ru')) {
                        $response = array(
                            'success' => true,
                            'msg' => 'Поздравляем, Вы подписаны на рассылку.',
                            'errors' => false,
                            'type' => 'ok'
                        );

                        $subject = 'Подписка на рассылку';

                        //  mag to managers
                        @mailer::mail($subject, $from, $our_email, $this->msg_self($email));
                        //  mag to user
                        @mailer::mail($subject, $from, $email, $this->msg_user($email));

                    } else {
                        $response = array(
                            'success' => false,
                            'msg' => 'Нам очень стыдно, но что-то случилось с нашим сайтом, попробуйте попозже.',
                            'errors' => false
                        );
                    }
                }

            } else {
                $response = array(
                    'success' => false,
                    'msg' => 'Вы ввели неправильные данные',
                    'errors' => $msg,
                );
            }
            //  load json
            echo json_encode($response);
            exit;

        } else {
            //  no data
            Template::set_page('service_info', self::PAGE_HEADER, 'Нам очень стыдно, но что-то случилось с нашим сайтом, попробуйте попозже.');
        }
    }

    /**
     * Adds email to subscription
     */
    public function popup()
    {
        if (
            AJAX_REQUEST
            && !empty($_POST['email'])
            && Check::email($_POST['email'])
        ) {
            setcookie('email_ok', 1, $_SERVER['REQUEST_TIME'] + 1000000000, '/');
            setcookie('popup_time', '', $_SERVER['REQUEST_TIME'] - 100, '/');

            $result = false;

            if (!Unsubscribe::check_existence($_POST['email'])) {
                Unsubscribe::add_self($_POST['email'], 'popup', 'roskosmetika.ru');

                $promo_code = promo_codes::generate_promo_code(10, 1);

                $code_info = promo_codes::get_by_code($promo_code);

                Cart::orderID(true);
                Cart::setInfo();

                Cart::set_promo_discount($code_info['id']);

                $promo_code = promo_codes::generate_promo_code(10, 1);

                $_SESSION['popup_discount_bar'] = true;

                mailer::mail(
                    'Скидка 10% на сайте роскосметика.ру',
                    [
                        'sales@roskosmetika.ru' => 'Роскосметика',
                    ],
                    DEBUG ? env('ADMIN_EMAIL') : $_POST['email'],
                    Template::mail_template(
                        'mail_popup',
                        'Скидки на сайте роскосметика.ру',
                        [
                            'promo_code' => $promo_code,
                        ],
                        $_POST['email']
                    )
                );

                $result = true;
            }

            $this->jsonResponse($result);
        } else {
            $this->nopage(AJAX_REQUEST);
        }
    }

    /**
     * Create letter to mngr
     * @param  string $email user email
     * @return string        html letter
     */
    protected function msg_self($email)
    {
        $msg = "<html>
                    <head>
                        <title>Подписка на рассылку.</title>
                    </head>
                    <body>
                        <p>E-mail: <b>$email</b></p><p>Ресурс: <b>Роскосметика.ру</b></p>
                    </body>
             </html>";
        return $msg;
    }

    /**
     * Create letter to user
     * @param  string $email user email
     * @return string        html letter
     */
    protected function msg_user($email)
    {
        $msg = "<html>
                    <head>
                        <title>Подписка на рассылку.</title>
                    </head>
                    <body>
                         <div>
                            <p>
                                <a target=\"blank\" href=\"" . DOMAIN_FULL . "/\"><img width=\"285\" height=\"50\" alt=\"roskosmetika.ru\"
                                src=\"" . DOMAIN_FULL . "/images/logo_new.png\" style=\"DISPLAY: block;\" ></a>
                            </p>
                             <p>Вы подписались на рассылку с ресурса <a target=\"blank\" href=\"" . DOMAIN_FULL . "\">roskosmetika.ru</a>.<br>Ваш E-mail для рассылки: <strong>$email</strong>. <p>
                                <a target=\"blank\" href=\"" . DOMAIN_FULL . "\" style=\"FONT-FAMILY: Tahoma, sans-serif; COLOR: #51324e; FONT-SIZE:
                                14pt\">www.roskosmetika.ru</a>
                            </p>
                        </div>
                    </body>
                </html>";
        return $msg;
    }

}

?>