<?php

class Controller_product extends Controller_base
{

    /**
     * Output product.
     * @global array $prod
     * @param array $args URL parameters.
     */
    function index($args)
    {

        $product = FALSE;

        if (isset($args[0])) {
            if (is_numeric($args[0])) {
                $item = (int)$args[0];
                $item = product::check_descr_id($item);
                $redirect = product::redirect($item);
                //  redirect
                if ($redirect) {
                    header('HTTP/1.1 301 Moved Permanently');
                    header("Location: /product/$redirect");
                }
                $product = product::get_product($item, FALSE);

            } else {
                $item = $args[0];
                if (!get_magic_quotes_gpc()) $item = DB::mysql_secure_string($item);
                $product = product::get_product($item);
            }
        } else {
            Template::set_page('products_page', 'Популярные товары', [
                'products' => Product::get_popular(true, 40),
                'crumbs' => [
                    [
                        'name' => 'Популярные товары',
                    ],
                ],
            ]);

            return;
        }

        //	output product if no error
        if ($product) {
            $total = 0;
            $packIDs = [];

            for ($i = 0, $n = count($product['packs']); $i < $n; ++$i) {
                $total += (int)($product['packs'][$i]['special_price'] ?: $product['packs'][$i]['price']);
                $packIDs[] = (int)$product['packs'][$i]['id'];
            }

            Template::add_gtm_code('product', $total, $packIDs);

            //  zoom script for product
            Template::add_script('jquery.zoom_pic.min.js');
            Template::set('reviews', prod_review::get_list($product['id']));
            Template::set('month', check::month());
            Template::add_admitad_code(Admitad::product_page($product));
            //Template::add_rocket_code(retail_rocket::product_page($product['id']));
            //Template::add_criteo_code(criteo::product($product['id']));
            Template::add_vk_code(vk::view_product($product));
            Template::add_mytarget_code(mytarget::view_product($product));
            Template::set_description($product['title'], $product['seo_desc'], $product['keywords'], null, helpers::open_graph(og::product($product)));
            Template::set_page('product', $product['name'], $product);

            mindbox::add_action('performOperation', 'ViewProduct', [
                'data' => [
                    'action' => [
                        'productId' => $product['id'],
                    ],
                ],
            ]);
        } //	output 404
        else {
            $this->nopage();
        }
    }

    /**
     * Found lower price handler
     */
    function found_lower_price()
    {
        $result = false;

        if (
            !empty($_POST['lower-price']) && is_numeric($productID = (int)$_POST['lower-price']) && $productID > 0
            && ($itemID = Product::check_descr_id($productID)) && ($product = Product::get_by_id($itemID, true, true, false, false, false))
            && !empty($_POST['name']) && check::name($_POST['name'])
            && !empty($_POST['email']) && check::notEmpty($_POST['email']) && check::email($_POST['email'])
            && !empty($_POST['phone']) && check::notEmpty($_POST['phone']) && check::phone($_POST['phone'])
            && !empty($_POST['url']) && preg_match('/[a-z0-9-\.]{2,}\.[a-z]{2,5}\/.+/', $_POST['url'])
        ) {
            if ($productID == $product['id']) {
                $pack = $product['pack'];
                $price = (int)$product['special_price'] ?: (int)$product['price'];
            } else {
                for ($i = 0, $n = count($product['packs']); $i < $n; ++$i) {
                    if ($product['packs'][$i]['id'] == $productID) {
                        $pack = $product['packs'][$i]['pack'];
                        $price = (int)$product['packs'][$i]['special_price'] ?: (int)$product['packs'][$i]['price'];

                        break;
                    }
                }
            }

            if (!empty($pack)) {
                $url = $_POST['url'];

                $userID = Auth::is_auth() ? Auth::get_user_id() : Users::check_email($_POST['email']);

                $fullUrl = !preg_match('#^https?://#', $url) ? 'http://' . $url : $url;

                $promoCode = promo_codes::generate_promo_code(10, 1);

                $user = $userID ? Users::get_client_by_id($userID) : null;

                mailer::mail(
                    'Роскосметика. Нашли товар дешевле? Дарим скидку 10%!',
                    [
                        'sales@roskosmetika.ru' => 'Роскосметика',
                    ],
                    DEBUG ? [
                        env('ADMIN_EMAIL') => env('ADMIN_NAME'),
                    ] : [
                        $_POST['email'] => $_POST['name'],
                    ],
                    Template::mail_template(
                        'mail_found_lower_price',
                        'Нашли товар дешевле - дарим скидку 10%!',
                        [
                            'promo_code' => $promoCode,
                            'name' => $user ? ($user['surname'] ? $user['surname'] . ' ' : '') . $user['name'] : null,
                        ]
                    )
                );

                $result = mailer::mail(
                    'CRM обработано. Нашли товар дешевле',
                    [
                        'sales@roskosmetika.ru' => 'Роскосметика',
                    ],
                    DEBUG ? [
                        env('ADMIN_EMAIL') => env('ADMIN_NAME'),
                    ] : [
                        '2255483@mail.ru' => 'roskosmetika.ru',
                    ],
                    '<p>
                        Товар: <a href="' . DOMAIN_FULL . '/product/' . $product['url'] . '">' . $product['name'] . ' (' . $pack . '), ' . $price . ' р. ID: ' . $productID . '</a>
                    </p>
                    <p>ФИО: ' . h($_POST['name']) . '</p>
                    <p>E-mail: ' . h($_POST['email']) . '</p>
                    <p>Телефон: ' . h($_POST['phone']) . '</p>' .
                    ($userID ? '<p>Internet ID клиента: ' . $userID . '</p>' : '') .
                    '<p>Ссылка с меньшей ценой: <a href="' . h($fullUrl) . '">' . h($url) . '</a></p>'
                );

                Orders::insert_order(
                    $_POST['name'],
                    '',
                    '',
                    $_POST['email'],
                    $_POST['phone'],
                    Auth::is_auth(),
                    [$productID],
                    '!!! Заказ через "Нашли дешевле". Ссылка на товар: ' . $fullUrl . ' !!!',
                    0,
                    '',
                    null,
                    null,
                    null,
                    null,
                    true
                );
            }
        }

        $this->jsonResponse($result);
    }
}
