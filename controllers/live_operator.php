<?php

/**
 * Класс live operator
 */
class Controller_live_operator extends Controller_base
{
    /**
     * Список e-mail-ов
     */
    const EMAIL = '"IT roskosmetika" <roskosmetika_it@mail.ru>, "Отдел продаж Роскосметика" <2255483@mail.ru>';

    /**
     * Добавляет сообщение.
     * @param array $args Параметры URL
     */
    function index($args)
    {

        if (isset($_SERVER['HTTP_AJAX'])) {     // ajax
            $name = mb_substr(trim($_POST['name']), 0, 30);
            $msg = mb_substr(trim($_POST['msg']), 0, 500);
            $contacts = mb_substr(trim($_POST['contacts']), 0, 50);
            $resource = mb_substr(trim($_POST['host']), 0, 50);

            // Проверяем имя
            if (empty($name)) {
                $json['name'] = 'Представьтесь';
            } elseif (!Check::name($name)) {
                $json['name'] = 'Введите корректное имя';
            }

            // Проверяем вопрос
            if (empty($msg)) {
                $json['msg'] = 'Введите Ваш вопрос';
            }
            // Проверяем контакты
            if (empty($contacts)) {
                $json['contacts'] = 'Оставьте Ваши контакты';
            } elseif (!(Check::phone($contacts) || Check::email($contacts))) {
                $json['contacts'] = 'Не корректный телефон или email';
            }

            if (empty($json['name']) || empty($json['contacts']) || empty($json['msg'])) {

                if (!get_magic_quotes_gpc()) {
                    $name = DB::mysql_secure_string($name);
                    $msg = DB::mysql_secure_string($msg);
                    $contacts = DB::mysql_secure_string($contacts);
                    $resource = DB::mysql_secure_string($resource);
                }

                live_operator::add_message($name, $contacts, $msg, $resource);

                switch ($resource) {
                    case 'roskosmetika.ru' :
                        $resource_rus = "Роскосметика";
                        break;
                    case 'veliniya.ru' :
                        $resource_rus = 'Велиния';
                        break;
                    case 'r-cosmetics.ru' :
                        $resource_rus = 'R-cosmetics';
                        break;
                    default :
                        $resource_rus = $resource;
                }

                $to = iconv('utf-8', 'koi8-r', self::EMAIL);
                $from = '=?koi8-r?B?' . base64_encode(iconv('utf-8', 'koi8-r', 'Liveoperator')) . '?=' . ' <sales@roskosmetika.ru>';

                $subject = '=?koi8-r?B?' . base64_encode(iconv('utf-8', 'koi8-r', "Запрос $resource_rus")) . '?=';
                $message = iconv('utf-8', 'koi8-r', $this->get_msg($name, $contacts, $msg, $resource));

                $header = "Content-type: text/html; charset=koi8-r\r\n";
                $header .= "From: $from\r\n";
                $header .= "MIME-Version: 1.0\r\n";
                $header .= "Content-type: text/html; charset=koi8-r\r\n";

                @mail($to, $subject, $message, $header);

                $json['status'] = 'ok';
            } else {
                $json['status'] = 'error';
            }

            echo json_encode($json);
            exit();
        } else {
            $this->nopage();
        }
    }

    /**
     * Generate message
     */
    protected function get_msg($name, $contacts, $question, $resource)
    {
        $msg = <<<EOD
<html>
<head>
    <title>Live operator</title>
    <meta http-equiv=\"Content-type\" content=\"text/html; charset=koi8-r\">
</head>
<body>
     <p><b>ФИО:</b> {$name}</p>
     <p><b>Контакты:</b> {$contacts}</p>
     <p><b>Ресурс:</b> {$resource}</p>
     <p><b>Вопрос:</b> {$question}</p>
</body>
</html>
EOD;
        return $msg;
    }

}