<?php

class Controller_page Extends Controller_Base
{

    /**
     * Output page.
     * @param arrya $args URL parameters.
     */
    function index($args)
    {
        // Getting page ID.
        if (isset($args[0]) && preg_match('/^[\w-]+$/', $args[0])) {
            $page_name = $args[0];
        }

        // Output page.
        if (!empty($page_name) && $page = pages::get_page($page_name, 1)) {
            if ($page_name==='delivery') {
                Template::add_css('../assets/jquery-select2/select2.css');
                Template::add_css('../assets/jquery-select2/select2-bootstrap.css');
                Template::add_script('../assets/jquery-select2/select2.min.js');
                Template::add_script('../assets/jquery-select2/select2_locale_ru.js');
                $geoip = geoip_record_by_name(helpers::get_user_ip());
                //$geoip = ['region'=>47];
                $regions_geo_id = (isset($geoip['region'])) ? (int)$geoip['region'] : 48;
                $regions = DB::get_rows('SELECT s.id, s.name, s.geo_id, s.cdek FROM shipping_regions s WHERE s.cdek > 0 and s.geo_id > 0');
                Template::set('regions_geo_id', $regions_geo_id);
                Template::set('regions', $regions);
                Template::set('flag_delivery', true);
            }
            
            Template::set_description($page['title'], $page['description'], $page['keywords']);
            Template::set('crumb', $page['crumb']);

            $id_list = agregation_products::get_personal_product_list(helpers::search_user_id(),2,4);

            //Template::set('mb_pages', product::get_by_id(mindbox::get_recomendation('PoZakazam4', FALSE, 4), true, true, false, false, false));
            Template::set('mb_pages', product::get_by_id($id_list, true, true, false, false, false));

            Template::set_page('page', $page['header'], $page['content']);
        } // There is no page.
        else {
            $this->nopage();
        }
    }
}

?>