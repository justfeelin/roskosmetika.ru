<?php

/**
 * Working with questions to cosmetolog
 */
class Controller_question extends Controller_base
{

    //  page geader
    const PAGE_HEADER = 'Вопросы косметологу';
    //  quantity questions on one page
    const STEP = 15;

    function index($args)
    {

        $page_num = 1;

        //  check $args        
        if (isset($args[0])) {
            $page_num = (int)$args[0];
            if ($page_num == 1) {
                header('HTTP/1.1 301 Moved Permanently');
                header("Location: /question");
            }
        }

        $quantity = (int)ceil(count(questions_to_cosmetolog::get_questions()) / self::STEP);

        if ($page_num && is_numeric($page_num) && $page_num <= $quantity) {
            Template::set_description(self::PAGE_HEADER . ($page_num > 1 ? ' - ' . $page_num . ' страница' : ''), '', '');

            Template::set('month', check::month());
            Template::set('qantity_pages', $quantity);
            Template::set('current_page', $page_num);

            $id_list = agregation_products::get_personal_product_list(helpers::search_user_id(), 1, 4);

            //Template::set('mb_top_view', product::get_by_id(mindbox::get_recomendation('PoProsmotram4', FALSE, 4), true, true, false, false, false));
            Template::set('mb_top_view', product::get_by_id($id_list, true, true, false, false, false));

            Template::set('crumb', 'Вопросы косметологу');
            Template::set_page('all_questions', self::PAGE_HEADER, [
                'questions' => questions_to_cosmetolog::get_questions($page_num),
            ]);
        } else {
            $this->nopage();
        }
    }


    /**
     * Add new question
     * @param array $args URL array
     */
    function add_new($args)
    {

        if (isset($_SERVER['HTTP_AJAX'])) {

            $our_email = DEBUG ? array(
                env('ADMIN_EMAIL') => 'Вопрос косметологу roskosmetika.ru (DEBUG)',
            ) : array('2255483@mail.ru' => 'Вопрос косметологу roskosmetika.ru',
                'roskosmetika_it@mail.ru' => 'Вопрос косметологу roskosmetika.ru');

            $page_header = 'CRM обработано. Вопрос косметологу';

            $fio = trim($_POST['name']);
            $email = mb_substr(trim($_POST['email']), 0, 50);
            $question = trim($_POST['question']);

            // check fio
            if (!empty($fio)) {
                if (!Check::name($fio)) $msg['name'] = 'Что-то не так с имененем';
            }

            // Check e-mail
            if (!Check::email($email)) {
                $msg['email'] = 'Введите корректный E-mail';
            } else {
                $values['email'] = $email;
            }

            // check question
            if (empty($question)) {
                $msg['question'] = 'Собственно, забыли вопрос';
            } else {
                $values['question'] = $question;
            }

            // add new
            if (!isset($msg)) {
                $fio = h($fio);
                $email = h($email);
                $question = h($question);

                if (questions_to_cosmetolog::add_message($fio, null, $email, $question)) {
                    $response = array(
                        'success' => true,
                        'msg' => 'Ваш вопрос отправлен нашему кометологу',
                        'errors' => false
                    );
                } else {
                    $response = array(
                        'success' => false,
                        'msg' => 'Нам очень стыдно, но что-то случилось с нашим сайтом, попробуйте попозже.',
                        'errors' => false
                    );
                }

                //mail to managers
                $from = array('sales@roskosmetika.ru' => 'Роскосметика');
                $body = $this->get_msg_to_mng($email, $question, $fio);
                @mailer::mail($page_header, $from, $our_email, $body);

            } // if fail
            else {
                $response = array(
                    'success' => false,
                    'msg' => 'Вы ввели неправильные данные',
                    'errors' => $msg
                );
            }

            //  load json
            echo json_encode($response);
            exit;

        } else {
            //  no data
            Template::set_page('service_info', self::PAGE_HEADER, 'Мы сожалеем, но Ваш браузер не поддерживает эту функцию. Отключен javascript.');
        }

    }


    /**
     * Add new question
     * @param array $args URL array
     */
    function add_new_all($args)
    {

        if (isset($_SERVER['HTTP_AJAX'])) {

            $our_email = DEBUG ? array(
                env('ADMIN_EMAIL') => 'Вопрос косметологу roskosmetika.ru (DEBUG)',
            ) : array(
                '2255483@mail.ru' => 'Вопрос косметологу roskosmetika.ru',
                'roskosmetika_it@mail.ru' => 'Вопрос косметологу roskosmetika.ru');

            $page_header = 'Вопрос от клиента с сайта Роскосметика';

            $fio = trim($_POST['name']);
            $phone = isset($_POST['phone']) ? mb_substr(trim($_POST['phone']), 0, 50) : null;
            $email = mb_substr(trim($_POST['email']), 0, 50);
            $question = trim(h($_POST['question']));
            $type = 'all';
            // check fio
            if (empty($fio)) {
                $msg['name'] = 'Забыли представиться';
            } elseif (!Check::name($fio)) {
                $msg['name'] = 'Что-то не так с имененем';
            } else {
                $values['name'] = $fio;
            }

            if (!$phone || !Check::phone($phone)) {
                $msg['phone'] = 'Введите телефон';
            } else {
                $values['phone'] = $phone;
            }

            // Check e-mail
            if (!Check::email($email)) {
                $msg['email'] = 'Введите корректный E-mail';
            } else {
                $values['email'] = $email;
            }

            // check question
            if (empty($question)) {
                $msg['question'] = 'Собственно, забыли вопрос';
            } else {
                $values['question'] = $question;
            }

            // add new
            if (!isset($msg)) {

                if (questions_to_cosmetolog::add_message($fio, $phone, $email, $question, $type)) {
                    $response = array(
                        'success' => true,
                        'msg' => 'Ваш вопрос отправлен в отдел по работе с клиентами. Мы постараемся ответить на Ваш вопрос по E-mail с 9:30 до 18:30 (пн-пт).',
                        'errors' => false
                    );
                } else {
                    $response = array(
                        'success' => false,
                        'msg' => 'Нам очень стыдно, но что-то случилось с нашим сайтом, попробуйте попозже.',
                        'errors' => false
                    );
                }

                //mail to managers
                $from = array('sales@roskosmetika.ru' => 'Роскосметика');
                $body = "<html>
                            <head>
                                <title>Новый вопрос</title>
                            </head>
                            <body>
                                <p><b>$fio</b> задал(а) следующий вопрос:</p>
                                <p>$question</p>
                                <p><b>Телефон:</b></p>
                                <p>$phone</p>
                                <p><b>E-mail:</b></p>
                                <p>$email</p>
                            </body>
                        </html>";
                @mailer::mail('CRM обработано. ' . $page_header, $from, $our_email, $body);

                Orders::insert_order(
                    $fio,
                    '',
                    '',
                    $email,
                    $phone,
                    Auth::is_auth(),
                    [
                        Orders::CONSULTATION_PRODUCT_ID,
                    ],
                    '!!! Заказ через "Задать вопрос" !!! Вопрос клиента: ' . $question,
                    0,
                    '',
                    null,
                    null,
                    null,
                    null,
                    true
                );

                @mailer::mail(
                    'Вопрос в интернет-магазине Роскосметика',
                    [
                        'sales@roskosmetika.ru' => 'Роскосметика',
                    ],
                    DEBUG ? env('ADMIN_EMAIL') : $email,
                    Template::mail_template('mail_text', '', '<p>Спасибо за обращение в интернет-магазин Роскосметика!</p><p>Ваш вопрос получен, мы обязательно свяжемся с Вами в течение ближайших трёх дней.</p><p>Ваш вопрос:<br>' . $question . '</p><p>С уважением,<br>Ваша Роскосметика</p>')
                );
            } // if fail
            else {
                $response = array(
                    'success' => false,
                    'msg' => 'Вы ввели неправильные данные',
                    'errors' => $msg
                );
            }

            //  load json
            echo json_encode($response);
            exit;

        } else {
            //  no data
            Template::set_page('service_info', 'Задать вопрос', 'Мы сожалеем, но Ваш браузер не поддерживает эту функцию. Отключен javascript.');
        }

    }


    /**
     * Generate msg to managers
     * @param  string $fio users name
     * @param  string $email users email
     * @param  string $question users question
     * @return string           html msg
     */
    protected function get_msg_to_mng($email, $question, $fio)
    {
        if (!$fio) $fio = 'Неизвестный (не оставил ФИО)';

        $text = "<html>
                    <head>
                        <title>Новый вопрос косметологу</title>
                    </head>
                    <body>
                        <p><b>$fio</b> задал(а) следующий вопрос:</p>
                        <p>$question</p>
                        <p><b>Контакты:</b></p>
                        <p>$email</p>
                    </body>
               </html>";
        return $text;
    }

}

?>