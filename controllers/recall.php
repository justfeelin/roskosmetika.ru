<?php

/**
 * recall to client
 */
class Controller_recall extends Controller_base
{

    //  page geader
    const PAGE_HEADER = 'Обратный звонок';

    /**
     * Add new recall
     * @param array $args URL array
     */
    function index($args)
    {

        //  Adresses to send mail
        $send_to = DEBUG ? array(env('ADMIN_EMAIL') => 'Клиентский отдел roskosmetika.ru (DEBUG)') : array('2255483@mail.ru' => 'Клиентский отдел roskosmetika.ru', 'roskosmetika_it@mail.ru' => 'Лукашин СА roskosmetika.ru');

        //  Adresses send from 
        $send_from = array('sales@roskosmetika.ru' => 'Роскосметика');

        if (isset($_SERVER['HTTP_AJAX'])) {
            // ajax

            $user_id = auth::is_auth();
            $fio = trim($_POST['name']);
            //$fio = 'test';
            $phone = mb_substr(trim($_POST['phone']), 0, 16);

            //  check fio
            if (empty($fio)) {
                $msg['name'] = 'Представьтесь пожалуйста';
            } elseif (!Check::name($fio)) {
                $msg['name'] = 'Что-то не так с имененем';
            } else {
                $values['name'] = $fio;
            }

            // check phone
            if (empty($phone)) {
                $msg['phone'] = 'Введите Ваш контактный телефон';
            } elseif (!Check::phone($phone)) {
                $msg['phone'] = 'Введите корректный контактный телефон';
            } else {
                $values['phone'] = $phone;
            }


            // add new
            if (!isset($msg)) {

                //mail to managers

                $theme = 'CRM обработано. Обратный звонок';
                $body = "<html>
                                <head>
                                    <title>Обратный звонок Роскосметика</title>
                                </head>
                                <body>
                                    <p>Клиент <b>$fio</b> попросил ему перезвонить.</p>
                                    <p>Контактный телефон: <b>$phone</b></p><p>Ресурс: <b>Роскосметика.ру</b></p>" .
                    ($user_id ? "<p>Обратите ВНИМАНИЕ!!!<br>Заявку отправил наш клиент - <b>ID на Роскосметике - $user_id</b></p>" : '')
                    . "</body>
                         </html>";

                if (mailer::mail($theme, $send_from, $send_to, $body)) {
                    $response = array(
                        'success' => true,
                        'msg' => 'Заявка отправлена. Мы постараемся перезвонить Вам с 9:30 до 18:30 (пн-пт).',
                        'errors' => false
                    );

                    Orders::insert_order(
                        $fio,
                        '',
                        '',
                        '',
                        $phone,
                        $user_id,
                        [
                            Orders::CONSULTATION_PRODUCT_ID,
                        ],
                        '!!! Заказ через "Обратный звонок" !!!',
                        0,
                        '',
                        null,
                        null,
                        null,
                        null,
                        true
                    );
                } else {
                    $response = array(
                        'success' => false,
                        'msg' => 'Нам очень стыдно, но что-то случилось с нашим сайтом, попробуйте попозже.',
                        'errors' => false
                    );
                }


            } // Выводим снова форму быстрого заказа
            else {
                $response = array(
                    'success' => false,
                    'msg' => 'Вы ввели неправильные данные',
                    'errors' => $msg
                );
            }

            echo json_encode($response);
            exit();


        } else {
            // not ajax
            Template::set_page('service_info', self::PAGE_HEADER, 'Мы сожалеем, но Ваш браузер не поддерживает эту функцию. Отключен javascript.');
        }
    }


}

?>