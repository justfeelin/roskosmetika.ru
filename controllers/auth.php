<?php

/**
 *  Users authorization by session id
 */
class Controller_auth extends Controller_base
{

    /**
     * For users who want to use this controller
     * @param  array $arg URL params
     * @return 404 template
     */

    function index($arg)
    {
        $this->nopage();
    }

    /**
     * Updates transitional user credentials
     */
    public function credentials()
    {
        $key = isset($_POST['name']) ? 'name' : (isset($_POST['email']) ? 'email' : (isset($_POST['phone']) ? 'phone' : null));

        if (
            $key
            && !empty($_POST[$key])
        ) {
            $value = (
                $key === 'name'
                || (
                    $key === 'email'
                        ? check::email($_POST[$key])
                        : check::phone($_POST[$key])
                )
            )
                ? $_POST[$key]
                : null;

            if ($value) {
                Users::updateCredentials($key, $value, Cart::orderID(true), !empty($_POST['session']) ? $_POST['session'] : null);
            }
        }

        $this->jsonResponse(true);
    }
}
