<?php
/**
 * @var array $sets
 * @var array $products
 * @var string $countersHtml
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <script src="//cdn.optimizely.com/js/3687571026.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Как похудеть в домашних условиях? Водоросли ламинарии – быстрый и эффективный способ уменьшения объема талии и бедер</title>
    <link rel="stylesheet" type="text/css" href="/l/kak-umenshit-objem-talii/css.css">
    <meta name="description" content="Как похудеть при помощи водорослей ламинарии в талии и бедрах на 1 сантиметр всего за два часа? Проверенный и эффективный метод от экспертов магазина профессиональной косметики «Роскосметика».">
</head>
<body>
<div class="container">
    <div class="row new_head">
        <div class="span4">
            <div class="logo">
                <a href="/"><img src="/l/kak-umenshit-objem-talii/img/logo.png" alt="Логотип"></a>
            </div>
        </div>
        <div class="span3 offset1">
            <div class="phone">8-800-775-54-83</div>
        </div>
        <div class="span3 social offset1">
            <a href="http://vkontakte.ru/share.php?url=http://www.roskosmetika.ru/landing/kak-umenshit-objem-talii" rel="nofollow" target="_blank"
               class="icon-vkontakte-rect"></a>
            <a href="http://www.facebook.com/sharer.php?u=http://www.roskosmetika.ru/landing/kak-umenshit-objem-talii" rel="nofollow" target="_blank"
               class="icon-facebook-1"></a>
            <a href="https://twitter.com/intent/tweet?text=http://www.roskosmetika.ru/landing/kak-umenshit-objem-talii" rel="nofollow" target="_blank"
               class="icon-twitter-bird"></a>
            <a href="http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl=http://www.roskosmetika.ru/landing/kak-umenshit-objem-talii"
               rel="nofollow" target="_blank" class="icon-odnoklassniki"></a>
        </div>
    </div>
</div>
<div class="wrap_main">
    <div class="container-box">
        <div class="container main_slogan">
            <p class="slogan1">Как уменьшить объём талии<br>на 1 сантиметр за 2 часа?</p>

            <p>Используйте ламинарию и следуйте<br>нашим советам!</p>
            <button class="btn btn-primary order-scroll" id="order">ЗАКАЗАТЬ</button>
            <p class="small">Курс всего от <?= $sets[0]['price'] ?> рублей!</p>
        </div>
    </div>
</div>
<div class="container result">
    <div class="row">
        <div class="caption h3">как добиться такого результата?</div>

        <div class="span3">
            <img src="/l/kak-umenshit-objem-talii/img/img1.jpg" alt="Скраб">

            <p><span>шаг 1:</span> Подготовка. Нанесите скраб или соль на влажную кожу, помассируйте и смойте. Очищение
                кожи позволит активным веществам проникнуть глубже.</p>
            <a href="#"
               class="to-cart"
               data-id="1339"
               target="_blank">
                <button class="bay" id="bay">Заказать</button>
                <span class="bayprice"><?= $products[1339]['price_formatted'] ?> р</span></a>
        </div>
        <div class="span3">
            <img src="/l/kak-umenshit-objem-talii/img/img2.jpg" alt="Водоросли">

            <p><span>шаг 2:</span> Обёртывание. Зафиксируйте ламинарию на проблемных местах при помощи плёнки. Полезные
                вещества начнут впитываться в вашу кожу.</p>
            <a href="#"
               class="order to-cart"
               data-id="182"
               target="_blank">
                <button class="bay" id="bay">Заказать</button>
                <span class="bayprice"><?= $products[182]['price_formatted'] ?> р</span></a>
        </div>
        <div class="span3">
            <img src="/l/kak-umenshit-objem-talii/img/img3.jpg" alt="Полотенце">

            <p><span>шаг 3:</span> Очищение. Снимите ламинарию через 30-40 минут и вытрите оставшийся на теле агар-агар
                тело сухим полотенцем. Не стоит удалять агар-агар целиком: он продолжает впитываться.</p>
        </div>
        <div class="span3">
            <img src="/l/kak-umenshit-objem-talii/img/img4.jpg" alt="Крем">

            <p><span>шаг 4:</span> Закрепление. Нанесите крем на обработанные участки. Закрепляет эффект от обертывания
                и продлевает действие ламинарии. Отлично подходит для ежедневного антицеллюлитного ухода.</p>
            <a href="#"
               class="to-cart" data-id="1332"
               target="_blank">
                <button class="bay" id="bay">Заказать</button>
                <span class="bayprice"><?= $products[1332]['price_formatted'] ?> р</span></a>
        </div>
    </div>
    <div class="row">
        <div class="span6 answer">
            <p><span>ЭТО ЕЩЕ НЕ ВСЕ!</span> Закажи прямо сейчас все необходимое<br>для курса 10 обертываний и получи
                скидку 20%!</p>
        </div>

        <div class="span4 offset2">
            <div class="wrapper">
                <div class="cell">
                    <div id="holder">
                        <div class="digits"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrap_thumbnail" id="violet_btns">
    <div class="container">
        <div class="row">
            <div class="span4">
                <div class="thumbnail">
                    <img src="/l/kak-umenshit-objem-talii/img/item1.jpg" alt="Лимфодренаж">

                    <div class="caption">
                        <div class="h3">Минус 1 сантиметр</div>

                        <div class="description">
                            <p>Проведите всего одну процедуру с косметикой Велиния, избавьтесь за 2&nbsp;часа от 1&nbsp;сантиметра
                                объёма в области талии или бёдер. Эффект сохранится 2-3&nbsp;дня.<br><br>Состав набора:
                            </p>
                            <ul>
                                <li>-<a class="by_violet"
                                        href="/product/massazhnyj-gel-skrab-s-jekstraktom-fukusa-dlja-tela#pack-1340"
                                        target="_blank"> массажный гель-скраб, 100&nbsp;мл</a></li>
                                <li>-<a class="by_violet"
                                        href="/product/listovaja-laminarija#pack-577"
                                        target="_blank"> листовая ламинария, 2шт. x 200&nbsp;гр</a></li>
                                <li>-<a class="by_violet"
                                        href="/product/anticelljulitnyj-massazhnyj-krem-dlja-tela-s-kofeinom-i-vodorosljami#pack-1333"
                                        target="_blank"> антицеллюлитный крем, 100&nbsp;мл</a></li>
                                <li>-<a class="by_violet"
                                        href="/product/pljonka-strejch-dlja-obertyvanija"
                                        target="_blank"> плёнка для обёртывания</a></li>
                            </ul>
                            <br>

                            <p>
                                <?= $products[1340]['price_formatted'] ?>&nbsp;+&nbsp;<?= $products[577]['price_formatted'] ?>&nbsp;+&nbsp;<?= $products[1333]['price_formatted'] ?>&nbsp;+&nbsp;<?= $products[158]['price_formatted'] ?>&nbsp;=&nbsp;
                                <strike><?= $sets[0]['total'] ?>&nbsp;р</strike>
                            </p>
                        </div>
                        <p class="zakaz">
                            <a href="/" class="order to-cart" data-id="4637">
                                <span class="buy">ЗАКАЗАТЬ</span>
                                <span><?= $sets[0]['price'] ?> р</span>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="thumbnail">
                    <img src="/l/kak-umenshit-objem-talii/img/item3.jpg" alt="Лимфодренаж">

                    <div class="caption">
                        <div class="h3">Быстрое похудение</div>

                        <div class="description">
                            <p>Позвольте вашей коже ощутить всю пользу самого чистого моря России! С косметикой Велиния
                                за 10 процедур вы добьётесь значительного уменьшения объёмов и подтяжки кожи.<br><br>
                                Состав набора:</p>
                            <ul>
                                <li>-<a class="by_violet"
                                        href="/product/massazhnyj-gel-skrab-s-jekstraktom-fukusa-dlja-tela#pack-1339"
                                        target="_blank"> массажный гель-скраб, 300&nbsp;мл</a></li>
                                <li>-<a class="by_violet"
                                        href="/product/listovaja-laminarija#pack-182"
                                        target="_blank"> листовая ламинария, 3&nbsp;кг</a></li>
                                <li>-<a class="by_violet"
                                        href="/product/krem-balzam-dlja-tela-anticelljulitnyj#pack-1330"
                                        target="_blank"> антицеллюлитный крем, 300&nbsp;мл</a></li>
                                <li>-<a class="by_violet"
                                        href="/product/prostynja-polijetilenovaja-dlja-obertyvanij#pack-199"
                                        target="_blank"> простыня для обёртывания, 2*1,6 м</a></li>
                            </ul>
                            <br>

                            <p>
                                <?= $products[1339]['price_formatted'] ?>&nbsp;+&nbsp;<?= $products[182]['price_formatted'] ?>&nbsp;+&nbsp;<?= $products[1332]['price_formatted'] ?>&nbsp;+&nbsp;<?= $products[199]['price_formatted'] ?>&nbsp;=&nbsp;
                                <strike><?= $sets[1]['total'] ?>&nbsp;р</strike>
                            </p>
                        </div>
                        <p class="zakaz">
                            <a href="/" class="order to-cart" data-id="4638">
                                <span class="buy">ЗАКАЗАТЬ</span>
                                <span><?= $sets[1]['price'] ?> р</span>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="thumbnail">
                    <img src="/l/kak-umenshit-objem-talii/img/item2.jpg" alt="Лимфодренаж">

                    <div class="caption">
                        <div class="h3">Похудение и лифтинг</div>

                        <div class="description">
                            <p>Использование французской косметики Thalaspa гарантирует профессиональный результат даже
                                в домашних условиях. Этой косметики хватит на 2 курса по 10 процедур.<br><br>Состав
                                набора:</p>
                            <ul>
                                <li>-<a class="by_violet"
                                        href="/product/sol-skrab-mertvogo-morja-nezhnyj-piling"
                                        target="_blank"> Соль-скраб Мертвого моря, 500&nbsp;г</a></li>
                                <li>-<a class="by_violet"
                                        href="/product/listovaja-laminarija#pack-14"
                                        target="_blank"> листовая ламинария, 5&nbsp;кг</a></li>
                                <li>-<a class="by_violet"
                                        href="/product/anticelljulitnyj-massazhnyj-krem-dlja-tela-s-kofeinom-i-vodorosljami#pack-74"
                                        target="_blank"> антицеллюлитный крем, 500&nbsp;мл</a></li>
                                <li>-<a class="by_violet"
                                        href="/product/prostynja-polijetilenovaja-dlja-obertyvanij#pack-199"
                                        target="_blank"> простыня для обёртывания, 2*1,6 м</a></li>
                            </ul>
                            <br>

                            <p>
                                <?= $products[109]['price_formatted'] ?>&nbsp;+&nbsp;<?= $products[14]['price_formatted'] ?>&nbsp;+&nbsp;<?= $products[74]['price_formatted'] ?>&nbsp;+&nbsp;<?= $products[199]['price_formatted'] ?>&nbsp;=&nbsp;
                                <strike><?= $sets[2]['total'] ?>&nbsp;р</strike>
                            </p>
                        </div>
                        <p class="zakaz">
                            <a href="/" class="order to-cart" data-id="4639">
                                <span class="buy">ЗАКАЗАТЬ</span>
                                <span><?= $sets[2]['price'] ?> р</span>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<a href="#" id="nabory-btm"></a>
<div class="container slider">
    <div class="row">
        <div class="span12">
            <div class="caption h3">Почему мы выбрали именно эти средства:</div>
        </div>
    </div>
    <div class="row" id="sredstva">
        <div class="span4 sredstvo1 active" data-id="1">
            <div class="thumb"></div>
            <p><u>Скраб >></u></p>
        </div>
        <div class="span4 sredstvo2" data-id="2">
            <div class="thumb"></div>
            <p><u>Ламинария >></u></p>
        </div>
        <div class="span4 sredstvo3" data-id="3">
            <div class="thumb"></div>
            <p><u>Антицеллюлитный крем >></u></p>
        </div>
    </div>
    <div class="row">
        <div class="span12" id="why_choose">
            <p class="sredstvo active" id="sredstvo1">Первый этап процедуры - очищение кожи при помощи скраба или соли. Важно выбрать
                правильное средство, которое не повредит кожу, а лишь удалит ороговевший слой. Скраб Велиния содержит
                экстракт фукуса и мельчайшие полимерные частички, что делает его более нежным и позволяет применять до 2
                раз в неделю. А морская соль с частицами ламинарии подойдёт для более интенсивной процедуры. Очищенная
                таким образом кожа более восприимчива к воздействию ламинарии. Для увеличения срока действия и
                дополнительного антицеллюлитного эффекта, после завершения процедуры не забудьте использовать крем.</p>

            <p class="sredstvo" id="sredstvo2">Ламинария R-cosmetics добывается в экологически чистом месте
                Белого моря, где впитывает все полезные морские микроэлементы, а затем высушивается. Такой метод
                подготовки позволяет сохранить все полезные вещества минимум на 3 года. Размочив ламинарию в воде, вы
                заметите на её поверхности студенистое вещество, агар-агар. В нём и сосредоточена вся польза ламинарии.
                Наносить агар-агар на кожу очень просто: достаточно приложить лист ламинарии к телу и зафиксировать его
                плёнкой или простынёй на 30-40 минут. Благодаря особенностям состава, ламинария увлажняет, тонизирует и
                подтягивает кожу, что приводит к антицеллюлитному эффекту и снижению лишнего веса.</p>

            <p class="sredstvo" id="sredstvo3">После завершения обёртывания оставшийся на коже агар-агар
                рекомендуют не смывать, а втереть в кожу: так он продолжит действовать в течение 24 часов, усиливая
                эффект. Для пролонгации эффекта и дополнительной защиты кожи рекомендуется использовать крем.
                Антицеллюлитный крем идеально подходит для завершения процедуры, позволяя усилить результат. Используя
                все 3 средства в комплексе, вы заметите результат уже после первого применения, и сможете закрепить его
                всего за 8-10 процедур.</p>
        </div>
    </div>
</div>

<div class="wrap_economy">
    <div class="container">
        <div class="row">
            <div class="span4 calendar">
                <p>Стабильного эффекта можно добиться за 10 обёртываний. Делайте их 2 раза в неделю</p>
            </div>
            <div class="span4 smile">
                <p class="economy85">Экономия до 85%</p>
            </div>
            <div class="span4 moneta">
                <p>В салоне курс из 10 процедур обойдётся в 20 000 рублей. Ваша экономия составит до 85%!</p>
            </div>
        </div>
        <div class="row">
            <div class="span12 forOrd">
                <button class="btn btn-primary order-scroll" id="order_set2">ЗАКАЗАТЬ</button>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="h3 caption">Секреты:</div>

    <div class="row">
        <div class="span12 advice">
            <ul>
                <li><i class="icon-ok"></i>Одни и те же листы водорослей можно использовать 2 раза; использованные один
                    раз водоросли хранят в холодильнике 3-4 дня.
                </li>
                <li><i class="icon-ok"></i>Вода, в которой замачивают ламинарию, вбирает в себя часть полезных веществ.
                    Ванна с такой водой способствует подтяжке и омоложению кожи.
                </li>
                <li><i class="icon-ok"></i>Воду из-под ламинарии можно заморозить и использовать для протирания лица.
                    Осторожно, такая процедура противопоказана при куперозе.
                </li>
                <li><i class="icon-ok"></i>Для обёртывания не нужен помощник. Порядок действий такой: на простыне нужно
                    разложить листья в том месте, где будет спина; затем сесть на простыню и обмотать ноги по спирали,
                    начиная со стоп; лечь на простыню и накрыть себя водорослями сверху. Запахнуть вокруг себя края
                    простыни не составит труда.
                </li>
                <li><i class="icon-ok"></i>Используйте скраб не чаще, чем 1 раз в неделю. С очисткой кожи от рогового
                    слоя справится мочалка-варежка.
                </li>
                <li><i class="icon-ok"></i>Ламинарию не рекомендуют наносить на грудь. Традиционные зоны для обработки -
                    живот, бёдра, ноги и руки.
                </li>
                <li><i class="icon-ok"></i>Все процедуры нужно проводить по направлению снизу вверх: начните с пальцев
                    ног, и обработайте плечи в последнюю очередь.
                </li>
                <li><i class="icon-ok"></i>Во время обёртывания закройте глаза слушайте звуки моря. Тепло, влажность и
                    солёный морской запах позволят вам на часок ощутить себя на морском берегу.
                </li>
                <li><i class="icon-ok"></i>Обёртывание ламинарией даёт дополнительное преимущество - избавляет кожу от
                    растяжек. Добавив обёртывания к общей программе похудения, можно добиться идеального тела с
                    красивой, здоровой и упругой кожей.
                </li>
            </ul>
        </div>
    </div>
    <div class="h3 caption">Противопоказания:</div>

    <div class="row">
        <div class="span4 attention">
            <p><i class="icon-attention"></i>аллергия на йод;</p>
        </div>
        <div class="span4 attention">
            <p><i class="icon-attention"></i>повышенная функция щитовидной железы;</p>
        </div>
        <div class="span4 attention">
            <p><i class="icon-attention"></i>беременность;</p>
        </div>
    </div>
    <div class="row">
        <div class="span4 attention">
            <p><i class="icon-attention"></i>период лактации;</p>
        </div>
        <div class="span4 attention">
            <p><i class="icon-attention"></i>воспалительные процессы на коже;</p>
        </div>
        <div class="span4 attention">
            <p><i class="icon-attention"></i>инфекционные заболевания кожи;</p>
        </div>
    </div>
    <div class="row">
        <div class="span4 attention">
            <p><i class="icon-attention"></i>аллергические реакции на коже;</p>
        </div>
        <div class="span4 attention">
            <p><i class="icon-attention"></i>лихорадочные состояния;</p>
        </div>
        <div class="span4 attention">
            <p><i class="icon-attention"></i>тяжелое течение сахарного диабета;</p>
        </div>
    </div>
    <div class="row">
        <div class="span4 attention">
            <p><i class="icon-attention"></i>тяжелая форма гипертонии;</p>
        </div>
        <div class="span4 attention">
            <p><i class="icon-attention"></i>выраженный варикоз;</p>
        </div>
        <div class="span4 attention">
            <p><i class="icon-attention"></i>тромбофлебит;</p>
        </div>
    </div>
    <div class="row">
        <div class="span4 attention"></div>
        <div class="span4 attention">
            <p><i class="icon-attention"></i>онкологические заболевания.</p>
        </div>
        <div class="span4 attention"></div>
    </div>
    <div class="row calltous">
        <div class="span6 answer">
            <p><span>Звоните!</span> Наш косметолог готов<br> ответить на все ваши вопросы.</p>
        </div>
        <div class="span6 call">
            <p>8-800-775-54-83</p>
        </div>
    </div>
</div>
<div class="wrap_reviews">
    <div class="container">
        <div class="caption h3">Отзывы наших покупателей:</div>

        <div class="row">
            <div class="span6">
                <img src="/l/kak-umenshit-objem-talii/img/mila.png" alt="Мила">

                <p><span>Мила</span></p>

                <p class="middle"> - Уже не в первый раз беру ламинарию. Очень нравится эффект от обертываний. После
                    процедуры кожа выглядит отлично - становится очень нежной. А если делать обертывания постоянно, то
                    уходят лишние сантиметры с проблемных зон. Правда, я перед обертыванием пользуюсь скрабом, для
                    достижения лучшего результата.</p>

                <p><a href="/prod_review/listovaja-laminarija" target="_blank">Смотреть другие
                        отзывы</a></p>
            </div>
            <div class="span6">
                <img src="/l/kak-umenshit-objem-talii/img/katya.png" alt="Катя">

                <p><span>Катя</span></p>

                <p class="middle"> - Качеством водорослей - я очень довольна. Ламинария чистая, без песка, удобного
                    размера ( по внешнему виду, каждого высушенного листа, можно точно подобрать количество и размер
                    водоросли, в зависимости от её назначения) - чем, кстати, не могут похвастаться большинство
                    продавцов, у которых водоросли скручены в комок, в котором не понятно что там внутри. Водоросли
                    хорошо упакованы, на каждой коробке подробная инструкция по применению. Так что, смело заказывайте
                    как для работы, так и для домашнего использования. Спасибо Вам за достойное качество! Ваш довольный
                    клиент! :)</p>

                <p><a href="/prod_review/listovaja-laminarija" target="_blank">Смотреть другие
                        отзывы</a></p>
            </div>
        </div>
        <div class="row">
            <div class="span12 forOrd">
                <button class="btn btn-primary order-scroll" id="order_set2">ЗАКАЗАТЬ</button>
            </div>
        </div>
    </div>
</div>
<div class="wrap_footer">
    <div class="container">
        <div class="row">
            <div class="span4">
                <p>ООО "Седьмое Небо"<br>E-mail: sales@roskosmetika.ru<br>Почтовый адрес: 125476 <br>г. Москва,
                    ул.Василия Петушкова, д.8</p>
            </div>
            <div class="span4 text-center">
                <div class="footer_phone">8-800-775-54-83</div>

                <p>Все права защищены &copy; 2005-2015 Роскосметика</p>
            </div>
            <div class="span4">
                <div class="footer_social">
                    <a href="http://vkontakte.ru/share.php?url=http://www.roskosmetika.ru/landing/kak-umenshit-objem-talii" rel="nofollow"
                       target="_blank" class="icon-vkontakte-rect"></a>
                    <a href="http://www.facebook.com/sharer.php?u=http://www.roskosmetika.ru/landing/kak-umenshit-objem-talii" rel="nofollow"
                       target="_blank" class="icon-facebook-1"></a>
                    <a href="https://twitter.com/intent/tweet?text=http://www.roskosmetika.ru/landing/kak-umenshit-objem-talii" rel="nofollow"
                       target="_blank" class="icon-twitter-bird"></a>
                    <a href="http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl=http://www.roskosmetika.ru/landing/kak-umenshit-objem-talii"
                       rel="nofollow" target="_blank" class="icon-odnoklassniki"></a>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="/l/kak-umenshit-objem-talii/js.js"></script>
<?= DB::output_queries() ?>
<div>
    <?= $countersHtml ?>
</div>
</body>
</html>
