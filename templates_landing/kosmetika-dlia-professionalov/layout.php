<?php
/**
 * @var string $countersHtml
 */
?>
<!DOCTYPE html>
<html>
<head>
    <title>Роскосметика</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="width=device-width" />
    <meta name="format-detection" content="telephone=no" />

    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />

    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=latin,cyrillic" >
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=PT+Sans&subset=latin,cyrillic" />
    <link rel="stylesheet" type="text/css" href="/l/kosmetika-dlia-professionalov/css.css" />
</head>

<body>

<div class="app">
    <div class="generalTop">
        <div class="width">
            <a class="logo" href="/"></a>

            <div class="phone">
                8 800 775-54-83<br />
                <small>Бесплатно по России (9:30-18:30)</small>
            </div>
        </div>
    </div><!-- .general.top -->

    <div class="wrap wrapBanner1">
        <div class="width">
            <h1 class="header">
                КОСМЕТИКА ДЛЯ ПРОФЕССИОНАЛОВ<br />
                НА ОСОБЫХ УСЛОВИЯХ
            </h1>

            <h2 class="subheader">
                Крупный и мелкий опт, косметика<br />
                для салонов.
            </h2>

            <div class="button">
                <a target="_blank" href="/feedback">ОСТАВИТЬ ЗАЯВКУ</a>
            </div>
        </div>
    </div><!-- .wrapBanner1 -->

    <div class="wrap wrapPartners">
        <div class="width">
            <div class="items">
                <div class="item">
                    <img src="/l/kosmetika-dlia-professionalov/img/wrapPartners_image1.png" alt="" />
                </div>
                <div class="item">
                    <img src="/l/kosmetika-dlia-professionalov/img/wrapPartners_image2.png" alt="" />
                </div>
                <div class="item">
                    <img src="/l/kosmetika-dlia-professionalov/img/wrapPartners_image3.png" alt="" />
                </div>
                <div class="item">
                    <img src="/l/kosmetika-dlia-professionalov/img/wrapPartners_image4.png" alt="" />
                </div>
            </div><!-- .items -->

            <div class="text">
                И другая профессиональная косметика – <a href="/">посмотреть ассортимент</a>
            </div>
        </div>
    </div><!-- .wrapPartners -->

    <div class="wrap wrapBuyers">
        <div class="width">
            <h1 class="header _header25">
                МЫ ПРЕДЛОЖИМ ВАМ СПЕЦИАЛЬНЫЕ<br />
                УСЛОВИЯ, ЕСЛИ ВЫ:
            </h1>

            <div class="margin">
                <div class="columns">
                    <div class="column">
                        <div class="image">
                            <img src="/l/kosmetika-dlia-professionalov/img/wrapBuyers_image1.jpg" alt="" />
                        </div>
                        <div class="wrapper">
                            <div class="title">
                                <span>КОСМЕТОЛОГ</span>
                            </div>
                            <div class="body">
                                Скидка 7% на всю продукцию при предъявлении  сертификата
                            </div>
                            <div class="button">
                                <a href="/page/sales">ПОДРОБНЕЕ</a>
                            </div>
                        </div>
                    </div>

                    <div class="column">
                        <div class="image">
                            <img src="/l/kosmetika-dlia-professionalov/img/wrapBuyers_image2.jpg" alt="" />
                        </div>
                        <div class="wrapper">
                            <div class="title">
                                <span>ЮРИДИЧЕСКОЕ ЛИЦО</span>
                            </div>
                            <div class="body">
                                Скидка 10%, если вы оформите заказ на юридическое лицо
                            </div>
                            <div class="button">
                                <a href="/page/sales">ПОДРОБНЕЕ</a>
                            </div>
                        </div>
                    </div>

                    <div class="column">
                        <div class="image">
                            <img src="/l/kosmetika-dlia-professionalov/img/wrapBuyers_image3.jpg" alt="" />
                        </div>
                        <div class="wrapper">
                            <div class="title">
                                <span>ОПТОВЫЙ ПОКУПАТЕЛЬ</span>
                            </div>
                            <div class="body">
                                При заказе на сумму свыше 1 000 000 руб. получите специальные условия
                            </div>
                            <div class="button">
                                <a href="/page/sales">ПОДРОБНЕЕ</a>
                            </div>
                        </div>
                    </div>
                </div><!-- .columns -->
            </div>
        </div>
    </div><!-- .wrapBuyers -->

    <div class="wrap wrapTrust">
        <div class="width">
            <h1 class="header _header25">
                НАМ ДОВЕРЯЮТ:
            </h1>

            <div class="margin">
                <div class="columns">
                    <div class="column">
                        <div class="wrapper">
                            <div class="num">1460</div>
                            <div class="title">массажистов</div>
                        </div>
                    </div>

                    <div class="column">
                        <div class="wrapper">
                            <div class="num">4025</div>
                            <div class="title">косметологов</div>
                        </div>
                    </div>

                    <div class="column">
                        <div class="wrapper">
                            <div class="num">2565</div>
                            <div class="title">салонов красоты</div>
                        </div>
                    </div>
                </div><!-- .columns -->
            </div>
        </div>
    </div><!-- .wrapTrust -->

    <div class="wrap wrapOffers">
        <div class="width">
            <h1 class="header _header25">
                ЧТО МЫ ВАМ ПРЕДЛАГАЕМ:
            </h1>

            <div class="items">
                <div class="item item1">7000 товаров в наличии.<br />Косметика для любого кошелька</div>
                <div class="item item2">Скидка от 50%<br />Косметика от производителя</div>
                <div class="item item3">Доставка в любую точку России<br />Бережное хранение</div>
                <div class="item item4">Бесплатные протоколы популярных услуг<br />Мастер-классы для косметологов</div>
                <div class="item item5">Персональный менеджер<br />Помощь опытного косметолога</div>
            </div><!-- .items -->

            <div class="call">
                <div class="left">
                    <strong>Звоните!</strong> Наш косметолог готовответить на все ваши вопросы.
                </div>

                <div class="right">
                    8 800 775 54 83
                </div>
            </div>
        </div>
    </div><!-- .wrapOffers -->

    <div class="wrap wrapBlock">
        <div class="width">
            <div class="margin">
                <div class="columns">
                    <div class="column column1">
                        <div class="text">
                            Свяжитесь с нашими менеджерами:
                            <a href="/feedback">оставьте телефон</a> или позвоните
                            <br>
                            по номеру 8 800 775 54 83
                        </div>
                    </div>

                    <div class="column column2">
                        <div class="text">
                            Отправьте нам свой сертификат
                            косметолога, чтобы получить
                            скидку 7% (текст зависит от выбранной
                            опции)
                        </div>
                    </div>

                    <div class="column column3">
                        <div class="text">
                            Персональный менеджер учтёт
                            все ваши пожелания и обеспечит
                            доставку заказа удобным для вас
                            способом
                        </div>
                    </div>
                </div><!-- .columns -->
            </div>
        </div>
    </div><!-- .wrapBlock -->

    <div class="wrap wrapPartners wrapPartners2">
        <div class="width">
            <div class="items">
                <div class="item">
                    <img src="/l/kosmetika-dlia-professionalov/img/wrapPartners_image1.png" alt="" />
                </div>
                <div class="item">
                    <img src="/l/kosmetika-dlia-professionalov/img/wrapPartners_image2.png" alt="" />
                </div>
                <div class="item">
                    <img src="/l/kosmetika-dlia-professionalov/img/wrapPartners_image3.png" alt="" />
                </div>
                <div class="item">
                    <img src="/l/kosmetika-dlia-professionalov/img/wrapPartners_image4.png" alt="" />
                </div>
            </div><!-- .items -->

            <div class="text">
                И другая профессиональная косметика – <a href="/">посмотреть ассортимент</a>
            </div>
        </div>
    </div><!-- .wrapPartners2 -->

    <div class="wrap wrapProducts">
        <div class="width">
            <div class="call">
                <div class="left">
                    <strong>Звоните!</strong> Наш косметолог готовответить на все ваши вопросы.
                </div>

                <div class="right">
                    8 800 775 54 83
                </div>
            </div>
        </div>
    </div><!-- .wrapPartners -->

    <div class="generalBottom">
        <div class="width">
            <div class="margin">
                <div class="columns">
                    <div class="column column1">
                        <a class="logo" href="/"></a>

                        <div class="links">
                            <a href="/page/order">Как сделать заказ</a><br />
                            <a href="/page/delivery">Способы оплаты</a><br />
                            <a href="/page/delivery">Доставка и самовывоз</a><br />
                            <a href="/page/vozvrat-tovara">Возврат товара</a>
                        </div>
                    </div>

                    <div class="column column2">
                        <div class="header">
                            AДРЕС
                        </div>

                        <div class="text address">
                            Офис в Москве<br />
                            Адрес: м. Тушинская / м. Сходненская,
                            ул. Василия Петушкова, д. 8, 4 этаж,
                            офис "Седьмое Небо".
                        </div>

                        <div class="text phone">
                            8 800 775 54 83<br />
                            Звонки по России бесплатные<br />
                            с 9:30 -18:30, сб-вс - выходные.
                        </div>
                    </div>

                    <div class="column column3">
                        <div class="header">
                            МЫ В СОЦСЕТЯХ
                        </div>

                        <div class="social">
                            <a class="s1" href="https://www.facebook.com/roskosmetika/" target="_blank"></a>
                            <a class="s2" href="https://twitter.com/ROSKOSMETIKAru" target="_blank"></a>
                            <a class="s4" href="https://www.instagram.com/roskosmetika_official/" target="_blank"></a>
                            <a class="s5" href="https://plus.google.com/u/0/111185151723588230627/posts" target="_blank"></a>
                            <a class="s6" href="https://www.youtube.com/channel/UCj_A0d-kX5x9KyQJHaBje5A/videos" target="_blank"></a>
                        </div>

                        <div class="text">
                            &copy; «Интернет магазин косметики Роскосметика».<br />
                            Все права защищены. При использовании
                            материалов с сайта активная ссылка
                            на первоисточник  обязательна.
                        </div>
                    </div>
                </div><!-- .columns -->
            </div>
        </div>
    </div><!-- .generalBottom -->
</div><!-- .app -->
<script type="text/javascript" src="/l/kosmetika-dlia-professionalov/js.js"></script>
<div>
    <?= $countersHtml ?>
</div>
<?= DB::output_queries() ?>
</body>
</html>
