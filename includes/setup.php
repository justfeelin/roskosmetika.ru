<?php

mb_internal_encoding('UTF-8');

require 'functions.php';

/**
 * Request done from shell
 */
define('CONSOLE', !isset($_SERVER['REQUEST_URI']));

/**
 * Debug mode state
 */
define('DEBUG', !!env('DEBUG'));

/**
 * Desktop site domain value
 */
define('DOMAIN', env('DOMAIN'));

/**
 * Full desktop site domain (With protocol) value
 */
define('DOMAIN_FULL', env('DOMAIN_FULL'));

/**
 * Mobile site domain value
 */
define('DOMAIN_MOBILE', env('DOMAIN_MOBILE'));

/**
 * Full mobile site domain (With protocol) value
 */
define('DOMAIN_MOBILE_FULL', env('DOMAIN_MOBILE_FULL'));

/**
 * Number of reviews on page
 */
define('REVIEWS_ON_PAGE', 20);

/**
 * Current page is cart
 */
define('CART_PAGE', !empty($_SERVER['REQUEST_URI']) && substr($_SERVER['REQUEST_URI'], 0, 5) === '/cart');

error_reporting(E_ALL);

/**
 * Site path (= includes/../)
 */
define('SITE_PATH', realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR);

/**
 * Images CDN domain with protocol
 */
define('IMAGES_DOMAIN_FULL', env('DOMAIN_IMAGES_FULL'));

/**
 * Directory containing compressed templates
 */
define('COMPRESSED_TEMPLATES', 'templates_compressed');

$sName = session_name();

if (
    isset($_COOKIE[$sName])
    && !preg_match('/^[-,a-zA-Z0-9]{1,128}$/', $_COOKIE[$sName])
) {
    unset($_COOKIE[$sName]);

    setcookie($sName, null, -10000, '/');
}

ini_set("session.save_path", SITE_PATH . 'tmp/');

session_start();

header('Content-Type: text/html; charset=utf-8');
ini_set("include_path", dirname(__FILE__));

/**
 * classes autoloader
 * @param  string $class_name file name
 * @return bool   TRUE - file exist FALSE - no file
 */
function my_autoloader($class_name)
{

    $file_name = strtolower($class_name) . '.php';
    $file = SITE_PATH . 'classes' . DIRECTORY_SEPARATOR . $file_name;


    if (!is_readable($file)) return false;
    else {
        include_once $file;
    }
}

spl_autoload_register('my_autoloader');

/**
 * @param  int $errno error number
 * @param  string $errstr error string
 * @param  string $errfile error file
 * @param  string $errline error line
 * @param  boolean $query error from query
 * @return BOOL             Send
 */
function error_info($errno, $errstr, $errfile, $errline)
{
    $backtrace = debug_backtrace();

    if (DEBUG) {
        echo 'Error: "' . $errstr . '" (#' . $errno . ')' . ($errfile ? $errfile . ($errline ? ':' . $errline : '') : '') . "\n\n";

        if (CONSOLE || defined('AJAX_REQUEST') && AJAX_REQUEST) {
            print_r(array_reverse($backtrace));
        } else {
            echo format_backtrace($backtrace);
        }
    } else {
        rk_error::send_error($errno, $errstr, $errfile, $errline, $backtrace);

        if (!CONSOLE) {
            header("Location: /500.php");
        }
    }

    exit;
}

set_error_handler('error_info', E_ALL);

//	connection to data base
DB::connect(env('DB_HOST'), env('DB_NAME'), env('DB_USER'), env('DB_PASSWORD'));
date_default_timezone_set("Europe/Moscow");

setlocale(LC_ALL, 'ru_RU.UTF-8');

if (!CONSOLE) {
    Auth::init_session();
}
