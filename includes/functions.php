<?php

/**
 * Applies current Template UTM tags values to link
 *
 * @param string $url Original link value
 * @param string $utm_content UTM content value
 *
 * @return string
 */
function apply_utm($url = null, $utm_content = null)
{
    return Template::$utm_source && Template::$utm_medium && Template::$utm_campaign
        ? $url .
            ($url === null || strpos($url, '?') === false ? '?' : '&') .
            'utm_medium=' . Template::$utm_medium .
            '&utm_source=' . Template::$utm_source .
            '&utm_campaign=' . Template::$utm_campaign .
            (
                Template::$utm_term
                    ? '&utm_term=' . Template::$utm_term
                    : ''
            ) .
            (
                $utm_content !== null
                    ? '&utm_content=' . $utm_content
                    : ''
            )
        : $url
    ;
}

/**
 * Wrap for partial templates caching
 *
 * @param string $key Cache key
 * @param callable $generate Function to be executed for template generation
 * @param int $time Cache duration (In seconds)
 *
 * @return mixed
 */
function cache_item($key, $generate, $time)
{
    $return = DEBUG ? null : caching::get($key);

    if (!$return) {
        $return = $generate();

        if (!DEBUG) {
            caching::set($key, $return, $time);
        }
    }

    return $return;
}

/**
 * Returns HTML string for debug backtrace. Depending on value type, different colors will be applied to output
 *
 * @param mixed $value Value to colorize
 * @param int $level Argument indentation level (For arrays & objects)
 *
 * @return string
 */
function colorize_debug($value, $level = 1)
{
    $maxKeys = 20;

    $export = true;

    $maxLength = 10000;
    $checkLength = false;

    $br = "<br />\n";

    switch (true) {
        case is_integer($value) || is_float($value):
            $color = '30a59d';

            break;

        case is_string($value):
            $color = 'd00000';

            break;

        case is_array($value):
            $checkLength = true;

            $export = 'array(' . $br;

            $i = 0;

            foreach ($value as $key => $arrayValue) {
                $export .= colorize_debug($key, $level + 1) . '&nbsp;=>&nbsp;' . colorize_debug($arrayValue, $level + 1) . ',' . $br;

                if (++$i === $maxKeys) {
                    $export .= '...' . $br;

                    break;
                }
            }

            $export .= ')';

            $color = '000000';

            break;

        case is_object($value):
            $checkLength = true;

            $export = 'class&nbsp;<span style="color: #6dc700;">' . get_class($value) . '</span><br />{';

            $i = 0;

            foreach (get_object_vars($value) as $field => $fieldValue) {
                $export .= $br . '<span style="color: #5d248e;">$' . $field . '</span>&nbsp;=&nbsp;' . colorize_debug($fieldValue, $level + 1) . ';';

                if (++$i === $maxKeys) {
                    $export .= $br . ' ...';

                    break;
                }
            }

            $export .= '<br />}';

            $color = '000000';

            break;

        case is_callable($value):
            $color = '336699';

            $export = 'function ' . $value . '()';

            break;

        default:
            $color = '001cd0';

            break;
    }

    if ($export === true) {
        $export = @var_export($value, true);
    }

    if ($checkLength && mb_strlen($export) > $maxLength) {
        $export = mb_substr($export, 0, $maxLength);

        $cleaned = tidy_parse_string($export);

        $cleaned = $cleaned->Body();

        $export = substr($cleaned, 6, strlen($cleaned) - 14) . ' ...';
    }

    $fullExport = '<span style="color: #' . $color . '">' . $export . '</span>';

    return $level > 1 ? str_replace("\n", "\n" . str_repeat('&nbsp;', $level * 3), $fullExport) : $fullExport;
}

/**
 * print() wrapper with support of previous message erasure
 *
 * @param string $message Message to print
 * @param bool $clear Erase previous message printed with console_print()
 */
function console_print($message, $clear = true)
{
    static $previousLength;

    if ($clear && $previousLength) {
        print "\r" . str_repeat(' ', $previousLength) . "\r";
    }

    print $message;

    $previousLength = strlen($message);
}

/**
 * Returns formatted russian count text
 *
 * @param string $text Text to format, delimited variants with | (0,5,6,7,8,9,11,12,13,14,15,16,17,18,19 variant|Single variant|2,3,4 variant)
 *
 * @return string
 */
function count_morphology($text)
{
    return preg_replace_callback(
        //1                                  2                                                                        3                                   4    5   6
        //.                                  позиций         |позиция                                                 -                                   2    1   .
        '/(^|[^' . helpers::RUS_LETTERS . '])([' . helpers::RUS_LETTERS . ']+(?:\|[' . helpers::RUS_LETTERS . ']+){2})([^\d' . helpers::RUS_LETTERS . ']+)(\d*)(\d)($|[^\d])/i',
        function ($matches) {
            $variants = explode('|', $matches[2]);

            $mod = (int)$matches[5];

            return $matches[1] . $variants[$mod === 1 ? 1 : ($mod > 1 && $mod < 5 ? 2 : 0)] . $matches[3] . $matches[4] . $matches[5] . $matches[6];
        },
        preg_replace_callback(
            //1    2                                   3                                                                        4
            //123  -                                   позиций |позиция                                                         .
            '/(\d+)([^\d' . helpers::RUS_LETTERS . ']+)([' . helpers::RUS_LETTERS . ']+(?:\|[' . helpers::RUS_LETTERS . ']+){2})($|[^|' . helpers::RUS_LETTERS . '])/i',
            function ($matches) {
                $variants = explode('|', $matches[3]);

                $preLast = (int)$matches[1] > 10 ? (int)substr($matches[1], -2, 1) : null;
                $lastDigit = (int)substr($matches[1], -1);

                return $matches[1] . $matches[2] . $variants[$lastDigit === 1 && $preLast !== 1 ? 1 : ($lastDigit > 1 && $lastDigit < 5 && $preLast !== 1 ? 2 : 0)] . $matches[4];
            },
            $text
        )
    );
}

/**
 * Outputs variables for debugging
 *
 * @param string $method Method name to be used to output ('print_r', 'var_dump' etc.)
 * @param array $variables Variables to output
 */
function debug_variables($method, $variables)
{
    echo "\n";

    if (!CONSOLE) {
        echo '<pre>';
    }

    ob_start();

    for ($i = 0, $n = count($variables); $i < $n; ++$i) {
        echo "\n";
        call_user_func($method, $variables[$i]);
        echo "\n";
    }

    $t = ob_get_contents();

    ob_end_clean();

    echo CONSOLE ? $t : h($t);

    if (!CONSOLE) {
        echo '</pre>';
    }
}

/**
 * Returns environment value from `env.ini` file
 *
 * @param string $name Variable name
 * @param string $default Default value
 *
 * @return mixed null if environment not found and default value not passed
 */
function env($name, $default = null)
{
    static $env;

    if ($env === null) {
        $env = parse_ini_file(dirname(__FILE__) . '/../env.ini' , FALSE, INI_SCANNER_RAW);

        if (!$env) {
            die('No environment values');
        }
    }
    
    return isset($env[$name]) ? $env[$name] : $default;
}

/**
 * Escapes CSV cell value
 *
 * @param string $value Original value
 *
 * @return string
 */
function escape_csv($value)
{
    return $value !== '' ? ('"' . str_replace('"', '""', $value) . '"') : '';
}

/**
 * Returns float value with dot as delimiter
 *
 * @param string|int|float $value Original value
 * @param int $decimals Decimal points count
 *
 * @return string
 */
function floatraw($value, $decimals = 2)
{
    return number_format((float)$value, $decimals, '.', '');
}

/**
 * Returns formatted price
 *
 * @param string|int|float $price Numberic price
 *
 * @return string
 */
function formatPrice($price)
{
    return number_format(roundPrice($price), 0, ',', ' ');
}

/**
 * Hides all links in text using Template::hidelink()
 *
 * @param string $text Original text
 * @param bool $addClass Prepend class attribute with 'hidden_link' value
 *
 * @return string
 */
function hideLinks($text, $addClass = false) {
    return mb_ereg_replace_callback('href="([^#"][^"]*)"', function ($matches) use ($addClass) {
        return ($addClass ? 'class="hidden_link" ' : '') . 'data-link="' . Template::hidelink($matches[1]) . '"';
    }, $text, 'im');
}

/**
 * Highlights a text by searching a word in it.
 *
 * @param string $text Full text
 * @param string $query Query to be highlighted
 * @param string $className Class name to add to highlight element
 *
 * @return string
 */
function highlight_query($text = '', $query = '', $className = 'highlight')
{
    return $query && $text
        ? mb_ereg_replace_callback(
            preg_quote($query),
            function ($matches) use ($className) {
                return '<span class="' . $className . '">' . $matches[0] . '</span>';
            },
            $text,
            'i'
        )
        : $text;
}

/**
 * htmlspecialchars() alias
 *
 * @param string $value Unescaped value
 *
 * @return string
 */
function h($value)
{
    return htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
}

/**
 * htmlspecialchars_decode() alias
 *
 * @param string $value Escaped value
 *
 * @return string
 */
function h_d($value)
{
    return htmlspecialchars_decode($value, ENT_QUOTES);
}

/**
 * Adds JSON response headers
 *
 * @param int $cacheSeconds Number of seconds client is allowed to cache result
 */
function json_headers($cacheSeconds = null)
{
    header('Content-Type: application/json; charset=utf-8');
    header('Cache-Control: ' . ($cacheSeconds ? 'max-age=' . $cacheSeconds : 'no-cache, no-store, must-revalidate'));
    header('Pragma: ' . ($cacheSeconds ? 'cache' : 'no-cache'));
    header('Expires: ' . ($cacheSeconds ? gmdate('D, d M Y H:i:s', $_SERVER['REQUEST_TIME'] + $cacheSeconds) . ' GMT' : 0));
}

/**
 * Unescapes value (With magic quotes) if magic quotes are enabled
 *
 * @param string $value Unescaped value
 * @param bool $gpc Whether value taken from Get/Post/Cookie
 *
 * @return string
 */
function magic_quotes_unescape($value, $gpc = true)
{
    if ($gpc ? get_magic_quotes_gpc() : get_magic_quotes_runtime()) {
        $value = stripcslashes($value);
    }

    return $value;
}

/**
 * Returns HTML-formatted error backtrace
 *
 * @param array $backtrace debug_backtrace() result
 *
 * @return string
 */
function format_backtrace($backtrace)
{
    $output = array();

    $stackLen = sizeof($backtrace);

    for ($i = 1; $i < $stackLen; ++$i) {
        $entry = $backtrace[$i];

        $func = '<i style="color: #4343d6">' . (!empty($entry['class']) ? '<span style="color: #de3728">' . $entry['class'] . '</span>' . $entry['type'] : '') . $entry['function'] . '</i>(';

        $arguments = array();

        if (isset($entry['args'])) {
               for ($j = 0, $argsLen = count($entry['args']); $j < $argsLen; ++$j) {
                $arguments[] = colorize_debug($entry['args'][$j]);
            }         
        }

        $func .= implode(', ', $arguments) . ')';

        $output[] = '<li><span style="color:green;">' . (!empty($entry['file']) ? $entry['file'] . '</span>:<b>' . $entry['line'] . '</b> - ' : '') . $func . '</li>';
    }

    return '<ol>' . implode("\n", array_reverse($output)) . '</ol>';
}

/**
 * Shortcut for print_r(), wrapped with &lt;pre&gt;
 *
 * @param mixed
 */
function pr()
{
    debug_variables('print_r', func_get_args());
}

/**
 * Shortcut to pr();die();
 */
function prdie()
{
    call_user_func_array('pr', func_get_args());

    die();
}

/**
 * Returns random string
 *
 * @param int $length String length
 * @param string $chars Available characters
 *
 * @return string
 */
function random_string($length, $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
{
    $charactersN = strlen($chars) - 1;

    $hash = '';

    for ($i = 0; $i < $length; ++$i) {
        $hash .= $chars[rand(0, $charactersN)];
    }

    return $hash;
}

/**
 * Makes 301 redirect
 *
 * @param string $url Redirect url. Current URL if not passed
 * @param bool $exit Exit on function call
 */
function redirect($url = null, $exit = true)
{
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . ($url ?: $_SERVER['REQUEST_URI']));

    if ($exit) {
        exit;
    }
}

/**
 * Performs request to URL
 *
 * @param string $url Url
 * @param array $data Fields (GET or POST)
 * @param bool $post Is POST request
 * @param bool $json Response is expected as JSON
 * @param int $timeout Response timeout
 * @param bool $mb if Mind Box API
 *
 * @return array|string JSON (If $json) or raw response
 */
function remote_request($url, $data = array(), $post = true, $json = true, $timeout = 1, $mb = FALSE)
{
    $curl = curl_init($url . ($post || !$data ? '' : ((strpos($url, '?') === false ? '?' : '&') . http_build_query($data))));

    curl_setopt_array($curl, array(
        CURLOPT_POST => $post,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_TIMEOUT => $timeout,
        CURLOPT_FAILONERROR => false,
    ));

    if ($json) {
        if($mb) {
            curl_setopt(
                $curl,
                CURLOPT_HTTPHEADER,
                [
                    'Accept: application/json',
                    'Authorization: DirectCrm key="' . env('MINDBOX_HASH_KEY') . '"',
                    'Host: roskosmetika-services.directcrm.ru', 

                ]
            );   
        } else {
            curl_setopt(
                $curl,
                CURLOPT_HTTPHEADER,
                [
                    'Accept: application/json',
                ]
            );   
        }

    }

    if ($post) {
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    }

    $response = curl_exec($curl);

    curl_close($curl);

    if ($json) {
        $response = $response !== false ? json_decode($response, true) : null;
    }

    return $response !== ($json ? null : false) ? $response : null;
}

/**
 * Returns rounded price
 *
 * @param int|float|string $price Price
 *
 * @return int
 */
function roundPrice($price)
{
    return (int)ceil((float)$price);
}

/**
 * Shortcut to var_dump(), wrapped with &lt;pre&gt;
 */
function vd()
{
    debug_variables('var_dump', func_get_args());
}

/**
 * Shortcut to vd();die();
 */
function vddie()
{
    call_user_func_array('vd', func_get_args());

    die();
}
