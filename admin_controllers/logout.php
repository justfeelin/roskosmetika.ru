<?php

class Controller_logout
{
    public function index()
    {
        unset($_SESSION['admin']);

        setcookie(admin::ACCESS_TOKEN, '', $_SERVER['REQUEST_TIME'] - 9999999, '/');

        header('Location: ' . DOMAIN_FULL . '/');

        die();
    }
}