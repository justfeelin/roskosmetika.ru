<?php
/**
 * Created by PhpStorm.
 * User: janpoul
 * Date: 09.04.18
 * Time: 15:20
 */

class Controller_country extends Controller_base
{
    /**
     * Список стран.
     * @param <type> $args.
     */
    function index($args)
    {
        $this->countries($args);
        $this->update_country_pages();
    }

   function update_country_pages(){
        $countryList = Country::get_list();
        if(count($countryList)>0){
            Country::update_table($countryList);
        }

    }

    /**
     * Country list
     *
     * @param $args
     */
    function countries($args)
    {
        if (empty($args[0])) {
            Template::set_page('countries', 'Косметика по странам', [
                'items' => country::get_countries(false),
            ]);
        } else {
            if (empty($args[1])&&(count($args)===1)){

                $pageID = (int)$args[0];

                $item = country::get_country($args[0]);

                if (!$item) {
                    $this->nopage();

                    return;

                }

                if (!empty($_POST['item'])) {
                    if (
                        !empty($_FILES['image'])
                        && !$_FILES['image']['error']
                        && is_uploaded_file($_FILES['image']['tmp_name'])
                    ) {
                        $item['image'] = $image = $_FILES['image']['name'];
                    } else {
                        $image = null;
                    }

                    $item = array_merge($item, $_POST['item']);

                    $visible = (isset($item['visible']) && $item['visible'] === 'on' ? 1 : 0);

                    try {
                        $id = country::update($pageID, $item['url'], $item['name'], $item['h1'], $item['title'], $item['description'], $item['keywords'], $visible, $item['text'], $image);

                        if ($image) {
                            move_uploaded_file($_FILES['image']['tmp_name'], SITE_PATH . 'www/images/country/' . $_FILES['image']['name']);
                        }

                        //redirect('/admin/seo/countries/' . $id);
                        redirect('/admin/country');
                    } catch (Exception $exception) {

                    }
                }

                Template::set_page('country', $pageID ? 'Редактирование косметика по странам' : 'Создание косметика по странам', [
                    'item' => $item,
                ]);

            } else {
                country::change_visible($args);
                redirect('/admin/country');
            }
        }
    }
}
