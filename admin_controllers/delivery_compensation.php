<?php

class Controller_delivery_compensation extends Controller_base
{
    public function index($arg)
    {
        if (!isset($arg[0])) {
            Template::set_page('delivery_compensation_regions', 'Компенсация доставки', [
                'regions' => Orders::get_regions(),
            ]);
        } else {
            $region = Orders::get_regions($arg[0]);

            if (!$region) {
                $this->nopage();

                return;
            }

            if (isset($_POST['delete_compensation'])) {
                $this->jsonResponse(Orders::delete_region_compensation($_POST['delete_compensation']));
            }

            if (!empty($_POST['data']['ids'])) {
                for ($i = 0, $n = count($_POST['data']['ids']); $i < $n; ++$i) {
                    if ($_POST['data']['ids'][$i]) {
                        Orders::update_region_compensation($_POST['data']['ids'][$i], $_POST['data']['sums'][$i], $_POST['data']['compensations_1'][$i], $_POST['data']['compensations_2'][$i], $_POST['data']['compensations_3'][$i], $_POST['data']['compensations_4'][$i], $_POST['data']['compensations_6'][$i]);
                    } else {
                        Orders::add_region_compensation($region['id'], $_POST['data']['sums'][$i], $_POST['data']['compensations_1'][$i], $_POST['data']['compensations_2'][$i], $_POST['data']['compensations_3'][$i], $_POST['data']['compensations_4'][$i], $_POST['data']['compensations_6'][$i]);
                    }
                }

                redirect();
            }

            Template::add_script('admin/region_compensation.js');

            Template::set_page('delivery_compensation_region', $region['name'], [
                'compensations' => Orders::get_region_compensations($region['id']),
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    protected function admin_access()
    {
        return admin::RIGHTS_COMMON | admin::RIGHTS_STATISTICS;
    }
}
