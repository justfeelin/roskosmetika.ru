<?php

class Controller_canonical extends Controller_Base
{

    /**
     * Импорт фильтров товаров
     */
    function index($arg)
    {
        //обработчик ошибок при импорте
        set_error_handler('catch_error');

        if (isset($_POST['submit'])) {

            $result = import::check_MIME('canonical', 'application/vnd.ms-excel');

            if ($result == 1) {

                $excel = import::load_file($_FILES['file_canonical']['tmp_name']);

                $fields = array(0 => 'main',
                    1 => 'link');

                $return_fields = import::check_fields($excel, $fields);

                if ($return_fields) {
                    //Все проверки файла пройдены. Начало импорта.               
                    //сообщение о строках без данных, повторяющемся url, ошибках 
                    $msg = array('main_fields' => '',
                        'error' => '');
                    //информационные сообщения о количестве импортированных и замененных данных
                    $info_msg = array('Категории с каноническими ссылками' => array('insert' => 0, 'update' => 0));

                    $excel->setActiveSheetIndex(0);
                    $main = $excel->getActiveSheet();
                    $max_row = $main->getHighestRow();
                    //в файле 2 столбца
                    $max_column = PHPExcel_Cell::columnIndexFromString($main->getHighestColumn());

                    //  обнуление старых значений
                    DB::query("UPDATE rk_categories SET canonical = NULL WHERE 1");

                    // парсинг листа и иимпорт
                    for ($i = 2; $i <= $max_row; $i++) {
                        //проверка на ошибки
                        if (isset($GLOBALS['main_error'])) {
                            $msg['error'] = $GLOBALS['main_error'];
                            break;
                        }

                        //импорт основных полей
                        $main_link = $main->getCellByColumnAndRow(0, $i)->getValue();
                        $link = $main->getCellByColumnAndRow(1, $i)->getValue();

                        $link = explode('/', trim($link));

                        $url_1 = $link[1];
                        $url_2 = $link[2];
                        $url_3 = $link[3];

                        $id_1 = categories::get_id_by_url($url_1, '0');
                        $id_2 = categories::get_id_by_url($url_2, $id_1);
                        $id_3 = categories::get_id_by_url($url_3, $id_2);

                        DB::query("UPDATE rk_categories SET canonical = '$main_link'  WHERE level = 3 AND id = $id_3 AND parent_1 = $id_2 AND parent_2 = $id_1");
                        $info_msg['Категории с каноническими ссылками'] = import::count_info($info_msg['Категории с каноническими ссылками']);

                    }

                    if (empty($msg['main_fields']) && empty($msg['error'])) {

                        $content = '<p>Категории с каноническими ссылками успешно загружены.</p>' . import::report_msg($info_msg, 'info');
                        //обновляем карту сайта
                        //sitemap::update_map();
                        //обновляем xml каталоги
                        //partner::update_all();
                        //отправка уведомления
                        //import::service_mail('filters', $content);

                        Template::set_page('index_success', 'Импорт категорий с каноническими ссылками', $content);
                    } else {

                        $content = import::report_msg($msg, 'error') . '<br><p>Загруженные категории с каноническими ссылками.</p>' . import::report_msg($info_msg, 'info');
                        //отправка уведомления
                        //import::service_mail('filters', $content);
                        Template::set_page('index_info', 'Импорт категорий с каноническими ссылками', $content);
                    }


                } else {
                    Template::set_form(null, 'Не найдены все необходимые поля.');
                    Template::set_page('import', 'Импорт категорий с каноническими ссылками', 'canonical');
                }
            } else {
                Template::set_form(null, $result);
                Template::set_page('import', 'Импорт категорий с каноническими ссылками', 'canonical');
            }


        } else {
            Template::set_page('import', 'Импорт категорий с каноническими ссылками', 'canonical');
        }

    }

}

?>