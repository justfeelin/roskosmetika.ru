<?php

class Controller_Import extends Controller_Base
{

    /**
     * Выгрузка меню импорта по умолчанию
     */
    function index($arg)
    {
        Template::set_page('menu_import', 'Импорт данных');
    }


    /**
     * Импорт описаний категорий
     */
    function categories($arg)
    {
        //обработчик ошибок при импорте
        set_error_handler('catch_error');

        if (isset($_POST['submit'])) {

            $result = import::check_MIME('categories', 'application/vnd.ms-excel');

            if ($result == 1) {

                $excel = import::load_file($_FILES['file_categories']['tmp_name']);

                $fields = array(0 => 'id',
                    1 => 'название',
                    2 => 'описание',
                    3 => 'синонимы',
                    4 => 'url',
                    5 => '<h1>',
                    6 => '<title>',
                    7 => '<description>',
                    8 => '<keywords>',
                    9 => 'фильтры');

                $return_fields = import::check_fields($excel, $fields);

                if ($return_fields) {
                    //Все проверки файла пройдены. Начало импорта.                
                    //проверка опций 
                    $options = import::check_options($excel, 2);
                    //сообщение о строках без данных, повторяющемся url, ошибках 
                    $msg = array('main_fields' => '', 'url' => '', 'error' => '');
                    //информационные сообщения о количестве импортированных и замененных данных
                    $info_msg = array('Основные' => array('insert' => 0, 'update' => 0),
                        'Описания' => array('insert' => 0, 'update' => 0),
                        'SEO' => array('insert' => 0, 'update' => 0));

                    //приведение текста примечания
                    $remark = import::convert_text($_POST['remark']);
                    //сохранение файла импорта
                    @move_uploaded_file($_FILES['file_categories']['tmp_name'], SITE_PATH . 'imported_files/categories/' . date('_y.m.d_H-i_') . '.xls');

                    $excel->setActiveSheetIndex(0);
                    $main = $excel->getActiveSheet();
                    $max_row = $main->getHighestRow();
                    //в файле 10 столбцов
                    $max_column = PHPExcel_Cell::columnIndexFromString($main->getHighestColumn());
                    // парсинг листа и иимпорт
                    for ($i = 2; $i <= $max_row; $i++) {
                        //проверка на ошибки
                        if (isset($GLOBALS['main_error'])) {
                            $msg['error'] = $GLOBALS['main_error'];
                            break;
                        }
                        //импорт основных полей
                        $id = $main->getCellByColumnAndRow(0, $i)->getValue() ? (int)trim($main->getCellByColumnAndRow(0, $i)->getValue()) : FALSE;
                        $name = $main->getCellByColumnAndRow(1, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(1, $i)->getValue()) : FALSE;
                        $url = $main->getCellByColumnAndRow(4, $i)->getValue() ? check::url($main->getCellByColumnAndRow(4, $i)->getValue()) : FALSE;
                        $h1 = $main->getCellByColumnAndRow(5, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(5, $i)->getValue()) : FALSE;
                        //проверка необходимых данных для импорта
                        if ($id && $name && $url && $h1) {
                            //импорт основных полей
                            $name = import::typograf($name);
                            //информаци о url - существование дублей, старый url
                            $url_info = import::check_url($id, $url, 'rk_categories');
                            //проверка на уникальность email
                            if ($url_info['check']) {

                                if ($options['update_categories']) {
                                    DB::query("UPDATE rk_categories SET name = '$name', url = '$url', h1 = '$h1' WHERE id = $id");
                                    $info_msg['Основные']['update'] = $info_msg['Основные']['update'] + DB::last_query_info();

                                    //изменения в файле редиректа, если менялся url
                                    if ($url != $url_info['url']) {
                                        $redirect = DB::get_row("SELECT (SELECT url FROM rk_categories WHERE id = c.parent_1) par_1,
                                                                 (SELECT url FROM rk_categories WHERE id = c.parent_2) par_2
                                                          FROM rk_categories c 
                                                          WHERE id = $id");
                                        $red_string = (is_null($redirect['par_2']) ? '' : "{$redirect['par_2']}/") . (is_null($redirect['par_1']) ? '' : "{$redirect['par_1']}/");

                                    }

                                }

                            } else {
                                $msg['url'] .= " '$url'-(строка-$i)";
                            }

                            //импорт описаний
                            if ($options['update_desc']) {

                                $desc = $main->getCellByColumnAndRow(2, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(2, $i)->getValue(), 0) : '';
                                $synonym = $main->getCellByColumnAndRow(3, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(3, $i)->getValue()) : '';

                                //конвертация в html и прогон типографом
                                $desc = import::markdown_to_html($desc);

                                $desc = import::typograf($desc);

                                $query = "REPLACE INTO rk_cat_desc (id, `desc`, synonym, vk_id) VALUES ($id, '$desc', '$synonym', IFNULL((SELECT vk_id FROM (SELECT cd.vk_id FROM rk_cat_desc AS cd WHERE cd.id = $id) AS c), 0))";
                                DB::query($query);
                                $info_msg['Описания'] = import::count_info($info_msg['Описания']);
                            }

                            //импорт seo данных
                            if ($options['update_seo']) {

                                $title = $main->getCellByColumnAndRow(6, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(6, $i)->getValue()) : '';
                                $seo_desc = $main->getCellByColumnAndRow(7, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(7, $i)->getValue()) : '';
                                $keywords = $main->getCellByColumnAndRow(8, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(8, $i)->getValue()) : '';

                                $query = "REPLACE INTO rk_cat_seo (id, title, description, keywords, name_plural, name_alt_number, h1_plural, h1_alt_number)
                                               VALUES ($id, '$title', '$seo_desc', '$keywords', IFNULL((SELECT name_plural FROM (SELECT name_plural FROM rk_cat_seo WHERE id = $id) AS c), 0), IFNULL((SELECT name_alt_number FROM (SELECT name_alt_number FROM rk_cat_seo WHERE id = $id) AS c), ''), IFNULL((SELECT h1_plural FROM (SELECT h1_plural FROM rk_cat_seo WHERE id = $id) AS c), 0), IFNULL((SELECT h1_alt_number FROM (SELECT h1_alt_number FROM rk_cat_seo WHERE id = $id) AS c), ''))";
                                DB::query($query);
                                $info_msg['SEO'] = import::count_info($info_msg['SEO']);


                            }

                            //импорт фильтров
                            if ($options['update_filters']) {
                                $filters = ($main->getCellByColumnAndRow(9, $i)->getValue() ? preg_split('/[,.]/', trim($main->getCellByColumnAndRow(9, $i)->getValue())) : FALSE);

                                if ($filters) {
                                    $query = "DELETE FROM rk_cat_filters WHERE cat_id = $id";
                                    DB::query($query);

                                    foreach ($filters as $value) {
                                        $value = (int)trim($value);

                                        $query = "INSERT INTO rk_cat_filters (cat_id, filter_id) VALUES ($id, $value)";
                                        DB::query($query);
                                    }

                                }

                            }

                        } else {

                            $msg['main_fields'] .= " $i";

                        }

                    }


                    if (empty($msg['main_fields']) && empty($msg['url']) && empty($msg['error'])) {
                        optimization::category_full_urls();

                        $content = '<p>Катагории успешно загружены.</p>' . import::report_msg($info_msg, 'info');

                        //добавляем лог
                        import::new_log($remark, 'default', $content, 'cat');

                        optimization::del_link_tables();
                        optimization::add_link_tables();                    
                        //отправка уведомления
                        //import::service_mail('categories', $content);

                        caching::clean();

                        Template::set_page('index_success', 'Импорт категорий', $content);
                    } else {

                        $content = import::report_msg($msg, 'error') . '<br><p>Загруженные категории.</p>' . import::report_msg($info_msg, 'info');
                        //добавляем лог
                        import::new_log($remark, 'default', import::convert_text($content, 0), 'cat');
                        //отправка уведомления
                        //import::service_mail('categories', $content);
                        Template::set_page('index_info', 'Импорт категорий', $content);
                    }

                } else {
                    Template::set_form(null, 'Не найдены все необходимые поля на листе "категории".');
                    Template::set_page('import', 'Импорт категорий', 'categories');
                }
            } else {
                Template::set_form(null, $result);
                Template::set_page('import', 'Импорт категорий', 'categories');
            }


        } else {
            Template::set_page('import', 'Импорт категорий', 'categories');
        }

    }


    /**
     * Импорт названий групп
     */
    function groups($arg)
    {
        //обработчик ошибок при импорте
        set_error_handler('catch_error');

        if (isset($_POST['submit'])) {

            $result = import::check_MIME('groups', 'application/vnd.ms-excel');

            if ($result == 1) {

                $excel = import::load_file($_FILES['file_groups']['tmp_name']);

                $fields = array(0 => 'id',
                    1 => 'название');

                $return_fields = import::check_fields($excel, $fields);

                if ($return_fields) {
                    //Все проверки файла пройдены. Начало импорта.                
                    //сообщение о строках без данных,  ошибках 
                    $msg = array('main_fields' => '', 'error' => '');
                    //информационные сообщения о количестве импортированных и замененных данных
                    $info_msg = array('Основные' => array('insert' => 0, 'update' => 0));

                    $excel->setActiveSheetIndex(0);
                    $main = $excel->getActiveSheet();
                    $max_row = $main->getHighestRow();
                    //в файле 10 столбцов
                    $max_column = PHPExcel_Cell::columnIndexFromString($main->getHighestColumn());
                    // парсинг листа и иимпорт
                    for ($i = 2; $i <= $max_row; $i++) {
                        //проверка на ошибки
                        if (isset($GLOBALS['main_error'])) {
                            $msg['error'] = $GLOBALS['main_error'];
                            break;
                        }
                        //импорт основных полей
                        $id = $main->getCellByColumnAndRow(0, $i)->getValue() ? (int)trim($main->getCellByColumnAndRow(0, $i)->getValue()) : FALSE;
                        $name = $main->getCellByColumnAndRow(1, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(1, $i)->getValue()) : FALSE;
                        //проверка необходимых данных для импорта
                        if ($id && $name) {
                            //импорт основных полей
                            $name = import::typograf($name);

                            DB::query("UPDATE rk_subcategories SET name = '$name' WHERE id = $id");
                            $info_msg['Основные']['update'] = $info_msg['Основные']['update'] + DB::last_query_info();

                        } else {

                            $msg['main_fields'] .= " $i";

                        }

                    }


                    if (empty($msg['main_fields']) && empty($msg['error'])) {

                        $content = '<p>Группы успешно загружены.</p>' . import::report_msg($info_msg, 'info');

                        //отправка уведомления
                        //import::service_mail('groups', $content);

                        Template::set_page('index_success', 'Импорт групп', $content);
                    } else {

                        $content = import::report_msg($msg, 'error') . '<br><p>Загруженные группы.</p>' . import::report_msg($info_msg, 'info');

                        //отправка уведомления
                        //import::service_mail('groups', $content);
                        Template::set_page('index_info', 'Импорт групп', $content);
                    }


                } else {
                    Template::set_form(null, 'Не найдены все необходимые поля на листе "группы".');
                    Template::set_page('import', 'Импорт групп', 'groups');
                }
            } else {
                Template::set_form(null, $result);
                Template::set_page('import', 'Импорт групп', 'groups');
            }


        } else {
            Template::set_page('import', 'Импорт групп', 'groups');
        }

    }


    /**
     * Импорт продуктов
     */
    function products($arg)
    {
        //обработчик ошибок при импорте
        set_error_handler('catch_error');

        if (isset($_POST['submit'])) {

            $result = import::check_MIME('products', 'application/vnd.ms-excel');

            if ($result == 1) {
                //загрузка файла
                $excel = import::load_file($_FILES['file_products']['tmp_name']);

                $fields = array(
                    0 => 'id фас*',
                    1 => 'id тов*',
                    2 => 'название товара',
                    3 => 'название фасовки*',
                    4 => 'английское название',
                    5 => 'краткое описание',
                    6 => 'полное описание',
                    7 => 'применение',
                    8 => 'состав',
                    9 => 'синонимы',
                    10 => 'url',
                    11 => '<title>',
                    12 => '<description>',
                    13 => '<keywords>',
                    14 => 'категории',
                    15 => 'фильтры',
                    16 => 'линейки',
                    17 => 'торговые марки',
                    18 => 'наборы',
                    19 => 'новинка',
                    20 => '<img src>',
                    21 => '<img alt>',
                    22 => '<img title>',
                    23 => 'видимость',
                );

                $return_fields = import::check_fields($excel, $fields);

                if ($return_fields) {
                    //Все проверки файла пройдены. Начало импорта.                
                    //проверка опций 
                    $options = import::check_options($excel, 5);
                    //сообщение о строках без данных, повторяющемся url, ошибках 
                    $msg = array('main_fields' => '',
                        'url' => '',
                        'error' => '',
                        'lenght' => '',
                        'no_cats' => '',
                        'no_filters' => '',
                        'no_tm' => '');
                    //информационные сообщения о количестве импортированных и замененных данных
                    $info_msg = array('Основные' => array('insert' => 0, 'update' => 0),
                        'Фасовки' => array('insert' => 0, 'update' => 0),
                        'Описания' => array('insert' => 0, 'update' => 0),
                        'SEO' => array('insert' => 0, 'update' => 0));


                    //приведение текста примечания
                    $remark = import::convert_text($_POST['remark']);
                    //сохранение файла импорта
                    @move_uploaded_file($_FILES['file_products']['tmp_name'], SITE_PATH . 'imported_files/products/' . date('_y.m.d_H-i_') . '.xls');
                    // парсинг листа и иимпорт

                    $excel->setActiveSheetIndex(0);
                    $main = $excel->getActiveSheet();
                    $max_row = $main->getHighestRow();
                    //в файле 23 столбца
                    $max_column = PHPExcel_Cell::columnIndexFromString($main->getHighestColumn());

                    for ($i = 2; $i <= $max_row; $i++) {
                        //проверка на ошибки
                        if (isset($GLOBALS['main_error'])) {
                            $msg['error'] = $GLOBALS['main_error'];
                            break;
                        }
                        //импорт основных полей
                        $id = $main->getCellByColumnAndRow(0, $i)->getValue() ? (int)trim($main->getCellByColumnAndRow(0, $i)->getValue()) : FALSE;
                        $descr_id = $main->getCellByColumnAndRow(1, $i)->getValue() ? (int)trim($main->getCellByColumnAndRow(1, $i)->getValue()) : 0;
                        $name = $main->getCellByColumnAndRow(2, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(2, $i)->getValue()) : FALSE;
                        $pack = $main->getCellByColumnAndRow(3, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(3, $i)->getValue()) : FALSE;
                        $short_desc = $main->getCellByColumnAndRow(5, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(5, $i)->getValue(), 0) : FALSE;
                        $desc = $main->getCellByColumnAndRow(6, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(6, $i)->getValue(), 0) : FALSE;
                        $url = $main->getCellByColumnAndRow(10, $i)->getValue() ? check::url($main->getCellByColumnAndRow(10, $i)->getValue()) : FALSE;
                        $new = $main->getCellByColumnAndRow(19, $i)->getValue() ? (int)$main->getCellByColumnAndRow(19, $i)->getValue() : 0;
                        $tm_id = (int)$main->getCellByColumnAndRow(17, $i)->getValue();
                        $visible = $main->getCellByColumnAndRow(23, $i)->getValue() ? 1 : 0;

                        //проверка необходимых данных для импорта
                        if ($id && $name && $pack && $short_desc && $desc && $url) {
                            //импорт основных полей
                            
                            //информаци о url - существование дублей, старый url
                            $url_info = import::check_url($id, $url, 'products');
                            //проверка на уникальность URL
                            if ($url_info['check']) {

                                if ($options['update_catalog']) {
                                    //типограф + markdown
                                    //проверка символов для краткого описания
                                    if (mb_strlen($short_desc) > 200) $msg['lenght'] .= " $i";

                                    $desc = import::markdown_to_html($desc);
                                    $desc = import::typograf($desc);

                                    $name = import::typograf($name);
                                    $pack = import::typograf($pack);
                                    $short_desc = import::typograf($short_desc);

                                    $old_descr_id = 0;
                                    $old_active = 0;
                                    $old_price = 0;
                                    $old_special_pice = 0;
                                    $old_main_stock = 0;
                                    $old_available = 0;
                                    $old_balance = 0;
                                    $old_for_proff = 0;
                                    $old_apply_discount = 0;
                                    $old_description_synth = '';
                                    $old__discount = 0;
                                    $old_purchase_price = 0;

                                    $prod_old_data = DB::get_row("SELECT main_id, tm_id, active, price, special_price, purchase_price, main_stock, visible, available, always_available, for_proff, description_synth, apply_discount, _discount FROM products WHERE id = $id");

                                    if ($prod_old_data) {
                                        $old_descr_id = $prod_old_data['main_id'];
                                        $old_active = $prod_old_data['active'];
                                        $tm_id = $tm_id ?: $prod_old_data['tm_id'];
                                        $old_price = $prod_old_data['price'];
                                        $old_special_pice = $prod_old_data['special_price'];
                                        $old_main_stock = $prod_old_data['main_stock'];
                                        $old_available = $prod_old_data['available'];
                                        $old_balance = $prod_old_data['always_available'];
                                        $old_for_proff = $prod_old_data['for_proff'];
                                        $old_apply_discount = $prod_old_data['apply_discount'];
                                        $old_description_synth = $prod_old_data['description_synth'];
                                        $old__discount = $prod_old_data['_discount'];
                                        $old_purchase_price = $prod_old_data['purchase_price'] ?: 0;
                                    }

                                    if ($old_descr_id > 0) {
                                        $tm_id = (int)DB::get_field('tm_id', 'SELECT p.tm_id FROM products AS p WHERE p.id = ' . ((int)$old_descr_id));
                                    }

                                    $query = "REPLACE INTO products (id, main_id, tm_id, active, pack, price, special_price, purchase_price, main_stock, visible, new, available, always_available, for_proff, name, short_description, `description`, description_synth, url, apply_discount, _discount)
                                                      VALUES ($id, $descr_id, $tm_id, $old_active, '$pack', $old_price,  $old_special_pice, $old_purchase_price, $old_main_stock, $visible, $new, $old_available, $old_balance, $old_for_proff, '$name', '$short_desc', '$desc', '" . DB::mysql_secure_string($old_description_synth) . "', '$url', $old_apply_discount, $old__discount)";
                                    DB::query($query);
                                    $info_msg['Основные'] = import::count_info($info_msg['Основные']);

                                    DB::query('INSERT INTO cdb_products (id) VALUES (' . $id . ') ON DUPLICATE KEY UPDATE id = id');

                                    optimization::lines_has_products();
                                }

                            } else {
                                $msg['url'] .= " '$url'-(строка-$i)";
                            }


                            //импорт описаний
                            if ($options['update_desc']) {

                                $eng_name = $main->getCellByColumnAndRow(4, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(4, $i)->getValue()) : '';
                                $use = $main->getCellByColumnAndRow(7, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(7, $i)->getValue(), 0) : '';
                                $ingredients = $main->getCellByColumnAndRow(8, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(8, $i)->getValue(), 0) : '';
                                $synonym = $main->getCellByColumnAndRow(9, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(9, $i)->getValue()) : '';
                                $tm = $main->getCellByColumnAndRow(17, $i)->getValue() ? (int)trim($main->getCellByColumnAndRow(17, $i)->getValue()) : 0;

                                //конвертация в html и прогон типографом
                                $use = import::markdown_to_html($use);
                                $ingredients = import::markdown_to_html($ingredients);

                                if ($use != '') $use = import::typograf($use);
                                if ($ingredients != '') $ingredients = import::typograf($ingredients);
                                if ($eng_name != '') $eng_name = import::typograf($eng_name);

                                $query = "REPLACE INTO rk_prod_desc (id, eng_name, `use`, ingredients, synonym, tm_id, yandex_market_visible, vk_id, vk_categorized, vk_updated)
                                               VALUES ($id, '$eng_name', '$use', '$ingredients', '$synonym', $tm, 1, IFNULL((SELECT vk_id FROM (SELECT pd.vk_id FROM rk_prod_desc AS pd WHERE pd.id = $id) AS p), 0), IFNULL((SELECT vk_categorized FROM (SELECT pd.vk_categorized FROM rk_prod_desc AS pd WHERE pd.id = $id) AS p), 0), IFNULL((SELECT vk_updated FROM (SELECT pd.vk_updated FROM rk_prod_desc AS pd WHERE pd.id = $id) AS p), '0000-00-00 00:00:00'))";

                                DB::query($query);
                                $info_msg['Описания'] = import::count_info($info_msg['Описания']);

                                //проверка на заполнение торговой марки
                                if (!$tm) $msg['no_tm'] .= " $i(id-$id)";

                            }

                            //импорт seo данных
                            if ($options['update_seo']) {

                                $title = $main->getCellByColumnAndRow(11, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(11, $i)->getValue()) : '';
                                $seo_desc = $main->getCellByColumnAndRow(12, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(12, $i)->getValue()) : '';
                                $keywords = $main->getCellByColumnAndRow(13, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(13, $i)->getValue()) : '';

                                $query = "REPLACE INTO rk_prod_seo (id, title, description, keywords)
                                                       VALUES ($id, '$title', '$seo_desc', '$keywords')";
                                DB::query($query);
                                $info_msg['SEO'] = import::count_info($info_msg['SEO']);

                            }

                            //импорт данных о фотографиях
                            if ($options['update_photos']) {

                                $alt = $main->getCellByColumnAndRow(21, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(21, $i)->getValue()) : '';
                                $title = $main->getCellByColumnAndRow(22, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(22, $i)->getValue()) : '';

                                $src = $id . '.jpg';

                                $query = "REPLACE INTO rk_prod_photo (id, src, alt, title)
                                                   VALUES ($id, '$src', '$alt', '$title')";
                                DB::query($query);

                            }

                            //импорт категорий
                            if ($options['update_categories']) {
                                $cats = ($main->getCellByColumnAndRow(14, $i)->getValue() ? preg_split('/[,.]/', trim($main->getCellByColumnAndRow(14, $i)->getValue())) : FALSE);

                                DB::query("DELETE rpc.* FROM rk_prod_cat rpc, rk_categories rc WHERE rpc.cat_id = rc.id AND rc.is_tag = 0 AND rpc.prod_id = $id");

                                if ($cats) {

                                    foreach ($cats as $cat) {
                                        $cat = (int)trim($cat);
                                        $query = "INSERT INTO rk_prod_cat (prod_id, cat_id) VALUES ($id, $cat) ON DUPLICATE KEY UPDATE prod_id = prod_id";
                                        DB::query($query);
                                    }

                                } else {
                                    //незаполненные категории
                                    $msg['no_cats'] .= " $i(id-$id)";
                                }
                            }

                            //импорт фильтров
                            if ($options['update_filters']) {
                                $filters = ($main->getCellByColumnAndRow(15, $i)->getValue() ? preg_split('/[,.]/', trim($main->getCellByColumnAndRow(15, $i)->getValue())) : FALSE);

                                DB::query("DELETE FROM rk_prod_filter WHERE prod_id = $id");

                                if ($filters) {                                    

                                    foreach ($filters as $filter) {
                                        $filter = (int)trim($filter);
                                        $query = "INSERT INTO rk_prod_filter (prod_id, filter_id) VALUES ($id, $filter)";
                                        DB::query($query);
                                    }

                                } else {
                                    //незаполненные фильтры
                                    $msg['no_filters'] .= " $i(id-$id)";
                                }
                            }

                            //импорт линеек
                            if ($options['update_lines']) {
                                $lines = ($main->getCellByColumnAndRow(16, $i)->getValue() ? preg_split('/[,.]/', trim($main->getCellByColumnAndRow(16, $i)->getValue())) : FALSE);

                                DB::query("DELETE FROM rk_prod_lines WHERE prod_id = $id");

                                if ($lines) {

                                    foreach ($lines as $line) {
                                        $line = (int)trim($line);
                                        $query = "INSERT INTO rk_prod_lines (prod_id, line_id) VALUES ($id, $line)";
                                        DB::query($query);
                                    }

                                }

                                optimization::lines_has_products();
                            }

                            //  импорт наборов
                            $sets = ($main->getCellByColumnAndRow(18, $i)->getValue() ? preg_split('/[,.]/', trim($main->getCellByColumnAndRow(18, $i)->getValue())) : FALSE);

                            DB::query("DELETE FROM rk_prod_sets WHERE prod_id = $id");

                            if ($sets) {

                                foreach ($sets as $set) {
                                    $set = (int)trim($set);
                                    $query = "INSERT INTO rk_prod_sets (prod_id, set_id) VALUES ($id, $set)";
                                    DB::query($query);
                                }

                            }

                        } else {
                            //проверяем является ли товар фасовкой
                            $main_id = $main->getCellByColumnAndRow(1, $i)->getValue() ? (int)trim($main->getCellByColumnAndRow(1, $i)->getValue()) : FALSE;
                            if ($main_id && ($main_id != $id) && $id && $pack) {
                                $pack = import::typograf($pack);
                                //если фасовка
                                $descr_old_descr_id = $main_id;
                                $descr_old_price = 0;
                                $descr_old_spec_price = 0;
                                $descr_old_main_stock = 0;
                                $descr_old_for_proff = 0;
                                $descr_old_available = 0;
                                $descr_old_active = 0;
                                $descr_old_purchase_price = 0;

                                $descr_old_data = DB::get_row("SELECT main_id, tm_id, price, special_price, purchase_price, main_stock, visible, available, for_proff, active FROM products WHERE id = $id");

                                if ($descr_old_data) {
                                    $tm_id = $tm_id ?: $descr_old_data['tm_id'];
                                    $descr_old_price = $descr_old_data['price'];
                                    $descr_old_spec_price = $descr_old_data['special_price'];
                                    $descr_old_main_stock = $descr_old_data['main_stock'];
                                    $descr_old_for_proff = $descr_old_data['for_proff'];
                                    $descr_old_available = $descr_old_data['available'];
                                    $descr_old_active = $descr_old_data['active'] ? 1 : 0;
                                    $descr_old_purchase_price = $descr_old_data['purchase_price'] ?: 0;
                                }

                                if ($descr_old_descr_id > 0) {
                                    $tm_id = (int)DB::get_field('tm_id', 'SELECT p.tm_id FROM products AS p WHERE p.id = ' . ((int)$descr_old_descr_id));
                                }

                                $query = "REPLACE INTO products (id, main_id, tm_id, pack, price, special_price, purchase_price, main_stock, visible, available, for_proff, active)
                                              VALUES    ($id,  $descr_old_descr_id, $tm_id , '$pack',  $descr_old_price, $descr_old_spec_price, $descr_old_purchase_price, $descr_old_main_stock, $visible, $descr_old_available, $descr_old_for_proff, $descr_old_active)";
                                DB::query($query);
                                $info_msg['Фасовки'] = import::count_info($info_msg['Фасовки']);

                                DB::query('INSERT INTO cdb_products (id) VALUES (' . $id . ') ON DUPLICATE KEY UPDATE id = id');

                                optimization::lines_has_products();

                                //импорт данных о фотографиях
                                if ($options['update_photos']) {

                                    $alt = $main->getCellByColumnAndRow(21, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(21, $i)->getValue()) : '';
                                    $title = $main->getCellByColumnAndRow(22, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(22, $i)->getValue()) : '';

                                    $src = $id . '.jpg';

                                    $query = "REPLACE INTO rk_prod_photo (id, src, alt, title)
                                                       VALUES ($id, '$src', '$alt', '$title')";
                                    DB::query($query);
                                }


                            } else {
                                //если это не фасовка
                                $msg['main_fields'] .= " $i";

                            }
                        }
                    }

                    DB::query(
                        'UPDATE products AS pu
                        INNER JOIN products AS p ON p.id = pu.main_id AND p.tm_id != 0
                        SET pu.tm_id = p.tm_id
                        WHERE pu.main_id != 0 AND pu.tm_id = 0'
                    );

                    if (empty($msg['main_fields']) &&
                        empty($msg['url']) &&
                        empty($msg['error']) &&
                        empty($msg['lenght']) &&
                        empty($msg['no_cats']) &&
                        empty($msg['no_filters']) &&
                        empty($msg['no_tm'])
                    ) {

                        $content = '<p>Товары успешно загружены.</p>' . import::report_msg($info_msg, 'info');

                        //добавляем лог
                        import::new_log($remark, 'default', $content, 'prod');
                        //отправка уведомления
                        //import::service_mail('products', $content);
                        //  create spec optimization tables

                        optimization::del_link_tables();
                        optimization::add_link_tables();

                        caching::clean();

                        Template::set_page('index_success', 'Импорт продуктов', $content);
                    } else {

                        $content = import::report_msg($msg, 'error') . '<br><p>Загруженные продукты.</p>' . import::report_msg($info_msg, 'info');
                        //добавляем лог
                        import::new_log($remark, 'default', import::convert_text($content, 0), 'prod');
                        //отправка уведомления
                        //import::service_mail('products', $content);
                        Template::set_page('index_info', 'Импорт продуктов', $content);
                    }


                } else {
                    Template::set_form(null, 'Не найдены все необходимые поля на листе "товары".');
                    Template::set_page('import', 'Импорт продуктов', 'products');
                }
            } else {
                Template::set_form(null, $result);
                Template::set_page('import', 'Импорт продуктов', 'products');
            }


        } else {
            Template::set_page('import', 'Импорт продуктов', 'products');
        }

    }


    /**
     * Импорт линеек товаров
     */
    function lines($arg)
    {
        //обработчик ошибок при импорте
        set_error_handler('catch_error');

        if (isset($_POST['submit'])) {

            $result = import::check_MIME('lines', 'application/vnd.ms-excel');

            if ($result == 1) {

                $excel = import::load_file($_FILES['file_lines']['tmp_name']);

                $fields = array(0 => 'id',
                                1 => 'торговая марка',
                                2 => 'название',
                                3 => 'краткое описание',
                                4 => 'полное описание',
                                5 => 'синонимы',
                                6 => 'url',
                                7 => '<h1>',
                                8 => '<title>',
                                9 => '<description>',
                                10 => '<keywords>',
                                11 => '<img src>',
                                12 => '<img alt>',
                                13 => '<img title>',
                                14 => 'альтернативное имя');

                $return_fields = import::check_fields($excel, $fields);         

                if ($return_fields) {
                    //сообщение о строках без данных, повторяющемся url, ошибках 
                    $msg = array('main_fields' => '',
                        'url' => '',
                        'error' => '',
                        'lenght' => '',
                        'no_photo' => '');
                    //информационные сообщения о количестве импортированных и замененных данных
                    $info_msg = array('Основные' => array('insert' => 0, 'update' => 0),
                                      'Описания' => array('insert' => 0, 'update' => 0),
                                      'SEO' => array('insert' => 0, 'update' => 0),
                                      'Фотографии' => array('insert' => 0, 'update' => 0));


                    //приведение текста примечания
                    $remark = import::convert_text($_POST['remark']);
                    //сохранение файла импорта
                    @move_uploaded_file($_FILES['file_lines']['tmp_name'], SITE_PATH . 'imported_files/lines/' . date('_y.m.d_H-i_') . '.xls');

                    $excel->setActiveSheetIndex(0);
                    $main = $excel->getActiveSheet();
                    $max_row = $main->getHighestRow();
                    //в файле 15 столбцов
                    $max_column = PHPExcel_Cell::columnIndexFromString($main->getHighestColumn());

                    // парсинг листа и иимпорт
                    for ($i = 2; $i <= $max_row; $i++) {
                        //проверка на ошибки
                        if (isset($GLOBALS['main_error'])) {
                            $msg['error'] = $GLOBALS['main_error'];
                            break;
                        }
                        //импорт основных полей
                        $tm_id = $main->getCellByColumnAndRow(1, $i)->getValue() ? (int)trim($main->getCellByColumnAndRow(1, $i)->getValue()) : FALSE;
                        $name = $main->getCellByColumnAndRow(2, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(2, $i)->getValue()) : FALSE;
                        $url = $main->getCellByColumnAndRow(6, $i)->getValue() ? check::url($main->getCellByColumnAndRow(6, $i)->getValue()) : FALSE;

                        //проверка необходимых данных для импорта
                        if ($name && $tm_id && $url) {
                            //добавление опционального поля id. Если не определен - автоинкремент.
                            $id = $main->getCellByColumnAndRow(0, $i)->getValue() ? (int)trim($main->getCellByColumnAndRow(0, $i)->getValue()) : 0;
                            $short_desc = $main->getCellByColumnAndRow(3, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(3, $i)->getValue(), 0) : '';
                            $desc = $main->getCellByColumnAndRow(4, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(4, $i)->getValue(), 0) : '';
                            //импорт основных полей
                            //информаци о url - существование дублей, старый url
                            $url_info = array('check' => (DB::get_row("SELECT id FROM `rk_lines` WHERE url = '$url' AND id <> $id AND tm_id = $tm_id") ? FALSE : TRUE),
                                              'url'   => DB::get_field('url', "SELECT url FROM `rk_lines`  WHERE  id = $id AND tm_id =$tm_id"));

                            //проверка на уникальность URL
                            if ($url_info['check']) {

                                //проверка символов для краткого описания
                                if (mb_strlen($short_desc) > 200) $msg['lenght'] .= " $i";
                                //типограф + markdown
                                $desc = import::markdown_to_html($desc);
                                $desc = import::typograf($desc);
                                $name = import::typograf($name);
                                $short_desc = import::typograf($short_desc);                                

                                $line_main_page = (int)DB::get_row("SELECT main_page FROM `rk_lines` WHERE id = $id");

                                $query = "REPLACE INTO `rk_lines` (id, tm_id, name, url, short_desc, `desc`, main_page, visible, _has_products)
                                                       VALUES ($id, $tm_id, '$name', '$url', '$short_desc', '$desc', $line_main_page, 1, " . ($id ? 'IFNULL((SELECT l._has_products FROM rk_lines AS l WHERE l.id = ' . $id . '), 0)' : 0) . ')';
                                DB::query($query);
                                $info_msg['Основные'] = import::count_info($info_msg['Основные']);

                                //если id вычислялся по автоинкременту
                                if (!$id) $id = DB::get_field('id', 'SELECT MAX(id) id FROM rk_lines');

                            } else {
                                $msg['url'] .= " '$url'-(строка-$i)";
                            }

                            //импорт описаний
                            $synonym  = $main->getCellByColumnAndRow(5, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(5, $i)->getValue()) : '';
                            $alt_name = $main->getCellByColumnAndRow(14, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(14, $i)->getValue()) : '';

                            $query = "REPLACE INTO rk_lines_desc (id, synonym, alt_name)
                                           VALUES ($id, '$synonym', '$alt_name')";

                            DB::query($query);
                            $info_msg['Описания'] = import::count_info($info_msg['Описания']);

                            //импорт seo данных
                            $h1 = $main->getCellByColumnAndRow(7, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(7, $i)->getValue()) : '';
                            $title = $main->getCellByColumnAndRow(8, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(8, $i)->getValue()) : '';
                            $seo_desc = $main->getCellByColumnAndRow(9, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(9, $i)->getValue()) : '';
                            $keywords = $main->getCellByColumnAndRow(10, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(10, $i)->getValue()) : '';

                            $query = "REPLACE INTO rk_lines_seo (id, h1, title, description, keywords)
                                                   VALUES ($id, '$h1', '$title', '$seo_desc', '$keywords')";
                            DB::query($query);
                            $info_msg['SEO'] = import::count_info($info_msg['SEO']);

                            //импорт данных о фотографиях
                            $src = $main->getCellByColumnAndRow(11, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(11, $i)->getValue()) : '';
                            $alt = $main->getCellByColumnAndRow(12, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(12, $i)->getValue()) : '';
                            $title = $main->getCellByColumnAndRow(13, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(13, $i)->getValue()) : '';

                            //Обработка имени фото
                            $src = import::check_img($id, $src, 'lines_photo');
                            //если нет фотографии
                            if ($src == 'none.jpg') $msg['no_photo'] .= " $i";

                            $query = "REPLACE INTO rk_lines_photo (id, src, alt, title)
                                               VALUES ($id, '$src', '$alt', '$title')";
                            DB::query($query);
                            $info_msg['Фотографии'] = import::count_info($info_msg['Фотографии']);


                        } else {
                            $msg['main_fields'] .= " $i";
                        }
                    }

                    if (empty($msg['main_fields']) &&
                        empty($msg['url']) &&
                        empty($msg['error']) &&
                        empty($msg['lenght']) &&
                        empty($msg['no_photo'])
                    ) {

                        $content = '<p>Линейки успешно загружены.</p>' . import::report_msg($info_msg, 'info');

                        //добавляем лог
                        import::new_log($remark, 'default', $content, 'lines');
                        //отправка уведомления
                        //import::service_mail('lines', $content);

                        Template::set_page('index_success', 'Импорт линеек', $content);
                    } else {

                        $content = import::report_msg($msg, 'error') . '<br><p>Загруженные линейки.</p>' . import::report_msg($info_msg, 'info');
                        //добавляем лог
                        import::new_log($remark, 'default', import::convert_text($content, 0), 'lines');
                        //отправка уведомления
                        //import::service_mail('lines', $content);
                        Template::set_page('index_info', 'Импорт линеек', $content);
                    }


                } else {
                    Template::set_form(null, 'Не найдены все необходимые поля на листе "Линейки".');
                    Template::set_page('import', 'Импорт линеек', 'products');
                }
            } else {
                Template::set_form(null, $result);
                Template::set_page('import', 'Импорт линеек', 'lines');
            }


        } else {
            Template::set_page('import', 'Импорт линеек', 'lines');
        }

    }


    /**
     * Импорт наборов товаров
     */
    function sets($arg)
    {
        //обработчик ошибок при импорте
        set_error_handler('catch_error');

        if (isset($_POST['submit'])) {

            $result = import::check_MIME('sets', 'application/vnd.ms-excel');

            if ($result == 1) {

                $excel = import::load_file($_FILES['file_sets']['tmp_name']);

                $fields = array(0 => 'id',
                                1 => 'название',
                                2 => 'краткое описание',
                                3 => 'полное описание',
                                4 => 'синонимы',
                                5 => 'url',
                                6 => '<title>',
                                7 => '<description>',
                                8 => '<keywords>',
                                9 => '<img src>',
                                10 => '<img alt>',
                                11 => '<img title>');

                $return_fields = import::check_fields($excel, $fields);         

                if ($return_fields) {
                    //сообщение о строках без данных, повторяющемся url, ошибках 
                    $msg = array('main_fields' => '',
                        'url' => '',
                        'error' => '',
                        'lenght' => '',
                        'no_photo' => '');
                    //информационные сообщения о количестве импортированных и замененных данных
                    $info_msg = array('Основные' => array('insert' => 0, 'update' => 0),
                                      'Описания' => array('insert' => 0, 'update' => 0),
                                      'SEO' => array('insert' => 0, 'update' => 0),
                                      'Фотографии' => array('insert' => 0, 'update' => 0));


                    //приведение текста примечания
                    $remark = import::convert_text($_POST['remark']);
                    //сохранение файла импорта
                    @move_uploaded_file($_FILES['file_sets']['tmp_name'], SITE_PATH . 'imported_files/sets/' . date('_y.m.d_H-i_') . '.xls');

                    $excel->setActiveSheetIndex(0);
                    $main = $excel->getActiveSheet();
                    $max_row = $main->getHighestRow();
                    //в файле 13 столбцов
                    $max_column = PHPExcel_Cell::columnIndexFromString($main->getHighestColumn());

                    // парсинг листа и иимпорт
                    for ($i = 2; $i <= $max_row; $i++) {
                        //проверка на ошибки
                        if (isset($GLOBALS['main_error'])) {
                            $msg['error'] = $GLOBALS['main_error'];
                            break;
                        }
                        //импорт основных полей
                        $name = $main->getCellByColumnAndRow(1, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(1, $i)->getValue()) : FALSE;
                        $url = $main->getCellByColumnAndRow(5, $i)->getValue() ? check::url($main->getCellByColumnAndRow(5, $i)->getValue()) : FALSE;

                        //проверка необходимых данных для импорта
                        if ($name && $url) {
                            //добавление опционального поля id. Если не определен - автоинкремент.
                            $id = $main->getCellByColumnAndRow(0, $i)->getValue() ? (int)trim($main->getCellByColumnAndRow(0, $i)->getValue()) : 0;
                            $short_desc = $main->getCellByColumnAndRow(2, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(2, $i)->getValue(), 0) : '';
                            $desc = $main->getCellByColumnAndRow(3, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(3, $i)->getValue(), 0) : '';
                            //импорт основных полей
                            //информаци о url - существование дублей, старый url
                            $url_info = import::check_url($id, $url, 'rk_sets');
                            //проверка на уникальность URL
                            if ($url_info['check']) {

                                //проверка символов для краткого описания
                                if (mb_strlen($short_desc) > 200) $msg['lenght'] .= " $i";
                                //типограф + markdown
                                $desc = import::markdown_to_html($desc);
                                $desc = import::typograf($desc);

                                $name = import::typograf($name);
                                $short_desc = import::typograf($short_desc);

                                $set_main_page = (int)DB::get_row("SELECT main_page FROM `rk_sets` WHERE id = $id");

                                $query = "REPLACE INTO `rk_sets` (id, name, url, short_desc, `desc`, main_page, visible)
                                                       VALUES ($id, '$name', '$url', '$short_desc', '$desc', $set_main_page, 1)";
                                DB::query($query);
                                $info_msg['Основные'] = import::count_info($info_msg['Основные']);

                                //если id вычислялся по автоинкременту
                                if (!$id) $id = DB::get_field('id', 'SELECT MAX(id) id FROM rk_sets');

                            } else {
                                $msg['url'] .= " '$url'-(строка-$i)";
                            }


                            //импорт описаний
                            $synonym = $main->getCellByColumnAndRow(4, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(4, $i)->getValue()) : '';

                            $query = "REPLACE INTO rk_sets_desc (id, synonym)
                                           VALUES ($id, '$synonym')";

                            DB::query($query);
                            $info_msg['Описания'] = import::count_info($info_msg['Описания']);

                            //импорт seo данных
                            $title = $main->getCellByColumnAndRow(6, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(6, $i)->getValue()) : '';
                            $seo_desc = $main->getCellByColumnAndRow(7, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(7, $i)->getValue()) : '';
                            $keywords = $main->getCellByColumnAndRow(8, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(8, $i)->getValue()) : '';

                            $query = "REPLACE INTO rk_sets_seo (id, title, description, keywords)
                                                   VALUES ($id, '$title', '$seo_desc', '$keywords')";
                            DB::query($query);
                            $info_msg['SEO'] = import::count_info($info_msg['SEO']);

                            //импорт данных о фотографиях
                            $src = $main->getCellByColumnAndRow(9, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(9, $i)->getValue()) : '';
                            $alt = $main->getCellByColumnAndRow(10, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(10, $i)->getValue()) : '';
                            $title = $main->getCellByColumnAndRow(11, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(11, $i)->getValue()) : '';

                            //Обработка имени фото
                            $src = import::check_img($id, $src, 'sets_photo');
                            //если нет фотографии
                            if ($src == 'none.jpg') $msg['no_photo'] .= " $i";

                            $query = "REPLACE INTO rk_sets_photo (id, src, alt, title)
                                               VALUES ($id, '$src', '$alt', '$title')";
                            DB::query($query);
                            $info_msg['Фотографии'] = import::count_info($info_msg['Фотографии']);


                        } else {
                            $msg['main_fields'] .= " $i";
                        }
                    }


                    if (empty($msg['main_fields']) &&
                        empty($msg['url']) &&
                        empty($msg['error']) &&
                        empty($msg['lenght']) &&
                        empty($msg['no_photo'])
                    ) {

                        $content = '<p>Наборы успешно загружены.</p>' . import::report_msg($info_msg, 'info');

                        //добавляем лог
                        import::new_log($remark, 'default', $content, 'sets');
                        //отправка уведомления
                        //import::service_mail('sets', $content);

                        Template::set_page('index_success', 'Импорт линеек', $content);
                    } else {

                        $content = import::report_msg($msg, 'error') . '<br><p>Загруженные наборы.</p>' . import::report_msg($info_msg, 'info');
                        //добавляем лог
                        import::new_log($remark, 'default', import::convert_text($content, 0), 'sets');
                        //отправка уведомления
                        //import::service_mail('sets', $content);
                        Template::set_page('index_info', 'Импорт наборов', $content);
                    }


                } else {
                    Template::set_form(null, 'Не найдены все необходимые поля на листе "Наборы".');
                    Template::set_page('import', 'Импорт наборов', 'products');
                }
            } else {
                Template::set_form(null, $result);
                Template::set_page('import', 'Импорт наборов', 'sets');
            }


        } else {
            Template::set_page('import', 'Импорт наборов', 'sets');
        }

    }


    /**
     * Импорт регионов и цены доставки
     */
    function delivery($arg)
    {
        //обработчик ошибок при импорте
        set_error_handler('catch_error');

        if (isset($_POST['submit'])) {

            $result = import::check_MIME('delivery', 'application/vnd.ms-excel');

            if ($result == 1) {

                $excel = import::load_file($_FILES['file_delivery']['tmp_name']);
      
                    //сообщение об ошибках 
                    $msg = array('main_fields' => '', 'error' => '');

                    //информационные сообщения о количестве импортированных и замененных данных
                    $info_msg = array('Доставка' => array('insert' => 0, 'update' => 0));


                    //приведение текста примечания
                    $remark = import::convert_text($_POST['remark']);
                    //сохранение файла импорта
                    @move_uploaded_file($_FILES['file_delivery']['tmp_name'], SITE_PATH . 'imported_files/delivery/' . date('_y.m.d_H-i_') . '.xls');

                    $excel->setActiveSheetIndex(0);
                    $main = $excel->getActiveSheet();
                    $max_row = $main->getHighestRow();
                    //в файле 13 столбцов
                    $max_column = PHPExcel_Cell::columnIndexFromString($main->getHighestColumn());

                    // парсинг листа и иимпорт
                    for ($i = 2; $i <= $max_row; $i++) {
                        //проверка на ошибки
                        if (isset($GLOBALS['main_error'])) {
                            $msg['error'] = $GLOBALS['main_error'];
                            break;
                        }
                        //импорт основных полей
                        $region = $main->getCellByColumnAndRow(1, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(1, $i)->getValue()) : FALSE;

                        //проверка необходимых данных для импорта
                        if ($region) {

                            $id = $main->getCellByColumnAndRow(0, $i)->getValue() ? (int)trim($main->getCellByColumnAndRow(0, $i)->getValue()) : 0;
                            $duration_min = $main->getCellByColumnAndRow(3, $i)->getValue() ? (int)trim($main->getCellByColumnAndRow(3, $i)->getValue()) : 0;
                            $duration_max = $main->getCellByColumnAndRow(4, $i)->getValue() ? (int)trim($main->getCellByColumnAndRow(4, $i)->getValue()) : 0;

                            $prices = [
                                1 => 5,
                                2 => 6,
                                3 => 7,
                                4 => 8,
                                6 => 9,
                            ];

                            foreach ($prices as $priceN => $fieldN) {
                                $value = trim($main->getCellByColumnAndRow($fieldN, $i)->getCalculatedValue());

                                ${'price_' . $priceN} = $value === '' ? 'NULL' : (int)$value;
                            }

                            $query = "REPLACE INTO shipping_regions (id, name, duration_min, duration_max, price_1, price_2, price_3, price_4, price_6)
                                                             VALUES ($id, '$region', $duration_min, $duration_max, $price_1, $price_2, $price_3, $price_4, $price_6)";
                            DB::query($query);

                            $info_msg['Доставка'] = import::count_info($info_msg['Доставка']);

                        } else {
                            $msg['main_fields'] .= " $i";
                        }
                    }

                    //  calculate rk_shipping_info
                    DB::query('TRUNCATE rk_shipping_info');

                    $prices = 4;

                    $shipping_data = array('min' => array(),
                                           'max' => array()
                                           );


                    for ($i=1; $i <= $prices; $i++) { 

                        $step = orders::calculate_shipping_info('MIN', $i, array(907));
                        $shipping_data['min']['prices'][] = $step['price'];
                        $shipping_data['min'][$step['price']] = $step;

                        $step = orders::calculate_shipping_info('MAX', $i, array(907));
                        $shipping_data['max']['prices'][] = $step['price'];
                        $shipping_data['max'][$step['price']] = $step;

                    }

                    $min_price = min($shipping_data['min']['prices']);
                    $max_price = max($shipping_data['max']['prices']);

                    DB::query("INSERT INTO rk_shipping_info(type, price, region, shipping_type, duration_min, duration_max) 
                                      VALUES('min', 
                                             $min_price, 
                                             '{$shipping_data['min'][$min_price]['region']}',
                                             '{$shipping_data['min'][$min_price]['shipping_type']}',
                                             {$shipping_data['min'][$min_price]['duration_min']},
                                             {$shipping_data['min'][$min_price]['duration_max']})");

                    DB::query("INSERT INTO rk_shipping_info(type, price, region, shipping_type, duration_min, duration_max) 
                                      VALUES('max', 
                                             $max_price, 
                                             '{$shipping_data['max'][$max_price]['region']}',
                                             '{$shipping_data['max'][$max_price]['shipping_type']}',
                                             {$shipping_data['max'][$max_price]['duration_min']},
                                             {$shipping_data['max'][$max_price]['duration_max']})");


                    $compensation = max(DB::get_row('SELECT max(compensation), max(compensation_1), max(compensation_2), max(compensation_3), max(compensation_4), max(compensation_6) FROM shipping_regions_compensations'));

                    DB::query("INSERT INTO rk_shipping_info(type, price) 
                                      VALUES('compensation', 
                                             $compensation)");

                    

                    if (empty($msg['main_fields']) && empty($msg['error'])) {

                        $content = '<p>Регионы и доставка успешно загружены.</p>' . import::report_msg($info_msg, 'info');

                        //добавляем лог
                        import::new_log($remark, 'default', $content, 'delivery');

                        Template::set_page('index_success', 'Импорт регионов и доставки', $content);
                    } else {

                        $content = import::report_msg($msg, 'error') . '<br><p>Загруженные данные.</p>' . import::report_msg($info_msg, 'info');
                        //добавляем лог
                        import::new_log($remark, 'default', import::convert_text($content, 0), 'delivery');
                        Template::set_page('index_info', 'Импорт регионов и доставки', $content);
                    }


                
            } else {
                Template::set_form(null, $result);
                Template::set_page('import', 'Импорт регионов и доставки', 'delivery');
            }


        } else {
            Template::set_page('import', 'Импорт регионов и доставки', 'delivery');
        }

    }


    /**
     * Импорт фильтров товаров
     */
    function filters($arg)
    {
        //обработчик ошибок при импорте
        set_error_handler('catch_error');

        if (isset($_POST['submit'])) {

            $result = import::check_MIME('filters', 'application/vnd.ms-excel');

            if ($result == 1) {

                $excel = import::load_file($_FILES['file_filters']['tmp_name']);

                $fields = array(0 => 'id',
                    1 => 'название',
                    2 => 'id родителя',
                    3 => 'видимость');

                $return_fields = import::check_fields($excel, $fields);

                if ($return_fields) {
                    //Все проверки файла пройдены. Начало импорта.                
                    //
                    //сообщение о строках без данных, повторяющемся url, ошибках 
                    $msg = array('main_fields' => '',
                        'error' => '',
                        'lenght' => '',
                        'no_group' => '');
                    //информационные сообщения о количестве импортированных и замененных данных
                    $info_msg = array('Группы' => array('insert' => 0, 'update' => 0),
                        'Фильтры' => array('insert' => 0, 'update' => 0));


                    //приведение текста примечания
                    $remark = import::convert_text($_POST['remark']);
                    //сохранение файла импорта
                    @move_uploaded_file($_FILES['file_filters']['tmp_name'], SITE_PATH . 'imported_files/filters/' . date('_y.m.d_H-i_') . '.xls');

                    $excel->setActiveSheetIndex(0);
                    $main = $excel->getActiveSheet();
                    $max_row = $main->getHighestRow();
                    //в файле 4 столбца
                    $max_column = PHPExcel_Cell::columnIndexFromString($main->getHighestColumn());

                    $last_group_id = FALSE;
                    // парсинг листа и иимпорт
                    for ($i = 2; $i <= $max_row; $i++) {
                        //проверка на ошибки
                        if (isset($GLOBALS['main_error'])) {
                            $msg['error'] = $GLOBALS['main_error'];
                            break;
                        }
                        //импорт основных полей
                        $name = $main->getCellByColumnAndRow(1, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(1, $i)->getValue()) : FALSE;
                        $visible = ((int)$main->getCellByColumnAndRow(3, $i)->getValue() === 1 || (int)$main->getCellByColumnAndRow(3, $i)->getValue() === 0) ? trim($main->getCellByColumnAndRow(3, $i)->getValue()) : FALSE;

                        //проверка необходимых данных для импорта
                        if ($name && ($visible !== FALSE)) {
                            //остальные поля
                            $id = $main->getCellByColumnAndRow(0, $i)->getValue() ? (int)trim($main->getCellByColumnAndRow(0, $i)->getValue()) : 0;
                            $parent_id = ((int)$main->getCellByColumnAndRow(2, $i)->getValue() || (int)$main->getCellByColumnAndRow(2, $i)->getValue() === 0) ? (int)trim($main->getCellByColumnAndRow(2, $i)->getValue()) : FALSE;

                            if ($parent_id === 0) {
                                //новая группа
                                DB::query("REPLACE INTO rk_filters (id, name, parent_id, visible)
                                                         VALUES ($id, '$name', $parent_id, $visible)");
                                $info_msg['Группы'] = import::count_info($info_msg['Группы']);

                                //получение последнего id группы
                                $last_group_id = $id;
                                if ($last_group_id == FALSE) $last_group_id = DB::get_field('id', 'SELECT MAX(id) id FROM rk_filters');

                                //проверка символов для имени
                                if (mb_strlen($name) > 115) $msg['lenght'] .= " $i";

                            } elseif ($parent_id !== FALSE) {
                                //новый фильтр
                                DB::query("REPLACE INTO rk_filters (id, name, parent_id, visible)
                                                         VALUES ($id, '$name', $parent_id, $visible)");
                                $info_msg['Фильтры'] = import::count_info($info_msg['Фильтры']);

                                //проверка символов для имени
                                if (mb_strlen($name) > 115) $msg['lenght'] .= " $i";

                            } else {
                                if ($last_group_id) {
                                    //новый фильтр
                                    DB::query("REPLACE INTO rk_filters (id, name, parent_id, visible)
                                                             VALUES ($id, '$name', $last_group_id, $visible)");
                                    $info_msg['Фильтры'] = import::count_info($info_msg['Фильтры']);

                                    //проверка символов для имени
                                    if (mb_strlen($name) > 115) $msg['lenght'] .= " $i";


                                } else {
                                    $msg['no_group'] .= " $i";
                                    break;
                                }

                            }

                        } else {
                            $msg['main_fields'] .= " $i";
                        }
                    }


                    if (empty($msg['main_fields']) && empty($msg['error']) && empty($msg['lenght']) && empty($msg['no_group'])) {

                        $content = '<p>Фильтры успешно загружены.</p>' . import::report_msg($info_msg, 'info');

                        //добавляем лог
                        import::new_log($remark, 'default', $content, 'filters');
                        //import::service_mail('filters', $content);

                        Template::set_page('index_success', 'Импорт фильтров', $content);
                    } else {

                        $content = import::report_msg($msg, 'error') . '<br><p>Загруженные Фильтры.</p>' . import::report_msg($info_msg, 'info');
                        //добавляем лог
                        import::new_log($remark, 'default', import::convert_text($content, 0), 'filters');
                        //отправка уведомления
                        //import::service_mail('filters', $content);
                        Template::set_page('index_info', 'Импорт фильтров', $content);
                    }


                } else {
                    Template::set_form(null, 'Не найдены все необходимые поля на листе "Фильтры".');
                    Template::set_page('import', 'Импорт фильтров', 'filters');
                }
            } else {
                Template::set_form(null, $result);
                Template::set_page('import', 'Импорт фильтров', 'filters');
            }


        } else {
            Template::set_page('import', 'Импорт фильтров', 'filters');
        }

    }


    /**
     * Импорт торговых марок
     */
    function tm($arg)
    {
        //обработчик ошибок при импорте
        set_error_handler('catch_error');

        if (isset($_POST['submit'])) {

            $result = import::check_MIME('tm', 'application/vnd.ms-excel');

            if ($result == 1) {

                $excel = import::load_file($_FILES['file_tm']['tmp_name']);

                $fields = array(0 => 'id',
                    1 => 'название',
                    2 => 'страна',
                    3 => 'краткое описание',
                    4 => 'полное описание',
                    5 => 'синонимы',
                    6 => 'url',
                    7 => '<title>',
                    8 => '<description>',
                    9 => '<keywords>',
                    10 => '<img src>',
                    11 => '<img alt>',
                    12 => '<img title>',
                    13 => 'альтернативное имя');

                $return_fields = import::check_fields($excel, $fields);

                if ($return_fields) {
                    //Все проверки файла пройдены. Начало импорта.                
                    //проверка опций 
                    $options = import::check_options($excel, 2);var_dump(1);
                    //сообщение о строках без данных, повторяющемся url, ошибках 
                    $msg = array('main_fields' => '',
                        'url' => '',
                        'error' => '',
                        'lenght' => '',
                        'no_photo' => '');
                    //информационные сообщения о количестве импортированных и замененных данных
                    $info_msg = array('Основные' => array('insert' => 0, 'update' => 0),
                        'Описания' => array('insert' => 0, 'update' => 0),
                        'SEO' => array('insert' => 0, 'update' => 0),
                        'Фотографии' => array('insert' => 0, 'update' => 0));


                    //приведение текста примечания
                    $remark = import::convert_text($_POST['remark']);
                    //сохранение файла импорта
                    @move_uploaded_file($_FILES['file_tm']['tmp_name'], SITE_PATH . 'imported_files/tm/' . date('_y.m.d_H-i_') . '.xls');

                    $excel->setActiveSheetIndex(0);
                    $main = $excel->getActiveSheet();
                    $max_row = $main->getHighestRow(); 

                    //в файле 13 столбцов
                    $max_column = PHPExcel_Cell::columnIndexFromString($main->getHighestColumn());

                    // парсинг листа и иимпорт
                    for ($i = 2; $i <= $max_row; $i++) { 
                        //проверка на ошибки
                        if (isset($GLOBALS['main_error'])) {
                            $msg['error'] = $GLOBALS['main_error'];
                            break;
                        }

                        //импорт основных полей
                        $name = $main->getCellByColumnAndRow(1, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(1, $i)->getValue()) : FALSE;
                        $short_desc = $main->getCellByColumnAndRow(3, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(3, $i)->getValue(), 0) : FALSE;
                        $desc = $main->getCellByColumnAndRow(4, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(4, $i)->getValue(), 0) : FALSE;
                        $url = $main->getCellByColumnAndRow(6, $i)->getValue() ? check::url($main->getCellByColumnAndRow(6, $i)->getValue()) : FALSE;

                        //проверка необходимых данных для импорта
                        if ($name && $short_desc && $desc && $url) {
                            //добавление опционального поля id. Если не определен - автоинкремент.
                            $id = $main->getCellByColumnAndRow(0, $i)->getValue() ? (int)trim($main->getCellByColumnAndRow(0, $i)->getValue()) : 0;

                            $country_id  = $main->getCellByColumnAndRow(2, $i)->getValue() ? (int)trim($main->getCellByColumnAndRow(2, $i)->getValue()) : 0;
                            //импорт основных полей
                            //информаци о url - существование дублей, старый url
                            $url_info = import::check_url($id, $url, 'tm');
                            
                            //проверка на уникальность URL
                            if ($url_info['check']) {

                                //проверка символов для краткого описания
                                if (mb_strlen($short_desc) > 150) $msg['lenght'] .= " $i";
                                //типограф + markdown
                                $desc = import::markdown_to_html($desc);
                                $desc = import::typograf($desc);

                                $short_desc = import::typograf($short_desc);

                                $old_param = DB::get_row("SELECT tm_menu, visible FROM tm WHERE id = $id");

                                if (!$old_param) {
                                    $old_param['tm_menu'] = 0;
                                    $old_param['visible'] = 1;
                                }

                                $query = "REPLACE INTO tm (id, name, url, short_desc, `desc`, tm_menu, visible, country_id)
                                                    VALUES ($id, '$name', '$url', '$short_desc', '$desc', {$old_param['tm_menu']}, {$old_param['visible']}, $country_id)";
                                DB::query($query);
                                $info_msg['Основные'] = import::count_info($info_msg['Основные']);

                                //если id вычислялся по автоинкременту
                                if (!$id) $id = DB::get_field('id', 'SELECT MAX(id) id FROM tm');

                            } else {
                                $msg['url'] .= " '$url'-(строка-$i)";
                            }

                            // импорт описаний
                            if ($options['update_desc']) {

                                $alt_name = $main->getCellByColumnAndRow(13, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(13, $i)->getValue()) : '';    
                                $synonym  = $main->getCellByColumnAndRow(5, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(5, $i)->getValue()) : '';

                                $query = "REPLACE INTO rk_tm_desc (id, alt_name, country, synonym)
                                               VALUES ($id, '$alt_name', (SELECT name FROM cdb_country WHERE id = $country_id), '$synonym')";

                                DB::query($query);
                                $info_msg['Описания'] = import::count_info($info_msg['Описания']);

                            }

                            // импорт seo данных
                            if ($options['update_seo']) {

                                $title = $main->getCellByColumnAndRow(7, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(7, $i)->getValue()) : '';
                                $seo_desc = $main->getCellByColumnAndRow(8, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(8, $i)->getValue()) : '';
                                $keywords = $main->getCellByColumnAndRow(9, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(9, $i)->getValue()) : '';

                                $query = "REPLACE INTO rk_tm_seo (id, title, description, keywords)
                                                       VALUES ($id, '$title', '$seo_desc', '$keywords')";
                                DB::query($query);
                                $info_msg['SEO'] = import::count_info($info_msg['SEO']);

                            }

                            //импорт данных о фотографиях
                            if ($options['update_photos']) {



                                $src = $main->getCellByColumnAndRow(10, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(10, $i)->getValue()) : '';
                                $alt = $main->getCellByColumnAndRow(11, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(11, $i)->getValue()) : '';
                                $title = $main->getCellByColumnAndRow(12, $i)->getValue() ? import::convert_text($main->getCellByColumnAndRow(12, $i)->getValue()) : '';

                                //Обработка имени фото
                                $src = import::check_img($id, $src, 'tm_photo');
                                //если нет фотографии
                                if ($src == 'none.jpg') $msg['no_photo'] .= " $i";

                                $query = "REPLACE INTO rk_tm_photo (id, src, alt, title)
                                                   VALUES ($id, '$src', '$alt', '$title')";
                                DB::query($query);
                                $info_msg['Фотографии'] = import::count_info($info_msg['Фотографии']);
                            }

                        } else {
                            $msg['main_fields'] .= " $i";
                        }
                                                   
                    }


                    if (empty($msg['main_fields']) &&
                        empty($msg['url']) &&
                        empty($msg['error']) &&
                        empty($msg['lenght']) &&
                        empty($msg['no_photo'])
                    ) {

                        $content = '<p>Торговые марки успешно загружены.</p>' . import::report_msg($info_msg, 'info');

                        //добавляем лог
                        import::new_log($remark, 'default', $content, 'tm');
                        //отправка уведомления
                        //import::service_mail('tm', $content);

                        caching::delete('index_tms');
                        caching::delete('brands_list');

                        Template::set_page('index_success', 'Импорт Торговых марок', $content);
                    } else {

                        $content = import::report_msg($msg, 'error') . '<br><p>Загруженные Торговые марки.</p>' . import::report_msg($info_msg, 'info');
                        //добавляем лог
                        import::new_log($remark, 'default', import::convert_text($content, 0), 'tm');
                        //отправка уведомления
                        //import::service_mail('tm', $content);
                        Template::set_page('index_info', 'Торговых марок', $content);
                    }


                } else {
                    Template::set_form(null, 'Не найдены все необходимые поля на листе "Торговые марки".');
                    Template::set_page('import', 'Импорт Торговых марок', 'products');
                }
            } else {
                Template::set_form(null, $result);
                Template::set_page('import', 'Импорт торговых марок', 'tm');
            }


        } else {
            Template::set_page('import', 'Импорт торговых марок', 'tm');
        }

    }


    /**
     * Import links "trade mark - category"
     */
    function tm_in_cat($arg)
    {
        //  error handler
        set_error_handler('catch_error');

        if (isset($_POST['submit'])) {

            $result = import::check_MIME('tm_in_cat', 'application/vnd.ms-excel');

            if ($result == 1) {

                $excel = import::load_file($_FILES['file_tm_in_cat']['tmp_name']);

                $fields = array(0 => 'tm_id',
                                1 => 'cat_id',
                                2 => 'h1',
                                3 => 'title',
                                4 => 'description',
                                5 => 'keywords',
                                6 => 'full_desc',
                                7 => 'help');

                $return_fields = import::check_fields($excel, $fields);

                if ($return_fields) {
                    //  check - ok. Start import + start errors(warning) report
                    $msg = array('main_fields' => '',
                                 'error' => '');
                    //  import logs
                    $info_msg = array('Торговые марки в категориях' => array('insert' => 0, 'update' => 0));

                    $excel->setActiveSheetIndex(0);
                    $main = $excel->getActiveSheet();
                    $max_row = $main->getHighestRow();

                    $max_column = PHPExcel_Cell::columnIndexFromString($main->getHighestColumn());

                    $last_group_id = FALSE;
                    //  parsing list
                    for ($i = 2; $i <= $max_row; $i++) {
                        //  Check errors
                        if (isset($GLOBALS['main_error'])) {
                            $msg['error'] = $GLOBALS['main_error'];
                            break;
                        }
                        //  import main fields
                        $tm_id = (int)$main->getCellByColumnAndRow(0, $i)->getValue() ? (int)$main->getCellByColumnAndRow(0, $i)->getValue() : FALSE;
                        $cat_id = (int)$main->getCellByColumnAndRow(1, $i)->getValue() ? (int)$main->getCellByColumnAndRow(1, $i)->getValue() : FALSE;

                        //  check main data
                        if ($tm_id && $cat_id) {
                            //  other data
                            $h1 = import::convert_text(trim($main->getCellByColumnAndRow(2, $i)->getValue()));
                            $title = import::convert_text(trim($main->getCellByColumnAndRow(3, $i)->getValue()));
                            $description = import::convert_text(trim($main->getCellByColumnAndRow(4, $i)->getValue()));
                            $keywords = import::convert_text(trim($main->getCellByColumnAndRow(5, $i)->getValue()));
                            $full_desc = import::convert_text(trim($main->getCellByColumnAndRow(6, $i)->getValue()));
                            $help = import::convert_text(trim($main->getCellByColumnAndRow(7, $i)->getValue()));

                            $full_desc = empty($full_desc) ? $full_desc : import::typograf(import::markdown_to_html($full_desc));

                            DB::query("REPLACE INTO rk_tm_in_cat (tm_id, cat_id, h1, title, description, keywords, full_desc, help)
                                                 VALUES ($tm_id, $cat_id, '$h1', '$title', '$description', '$keywords', '$full_desc', '$help')");

                            $info_msg['Торговые марки в категориях'] = import::count_info($info_msg['Торговые марки в категориях']);

                        } else {
                            $msg['main_fields'] .= " $i";
                        }
                    }


                    if (empty($msg['main_fields']) && empty($msg['error']) && empty($msg['lenght']) && empty($msg['no_group'])) {

                        $content = '<p>Торговые марки в категориях успешно загружены.</p>' . import::report_msg($info_msg, 'info');

                        //  add log
                        import::new_log($remark, 'default', $content, 'filters');
                        //  mail info to admin 
                        //import::service_mail('filters', $content);

                        Template::set_page('index_success', 'Импорт торговых марок в категориях', $content);
                    } else {
                        caching::delete('index_tms');
                        caching::delete('brands_list');

                        $content = import::report_msg($msg, 'error') . '<br><p>Загруженные торговые марки в категориях.</p>' . import::report_msg($info_msg, 'info');
                        //  add log
                        import::new_log($remark, 'default', import::convert_text($content, 0), 'filters');
                        //  mail info to admin 
                        //import::service_mail('filters', $content);
                        Template::set_page('index_info', 'Импорт торговых марок в категориях', $content);
                    }


                } else {
                    Template::set_form(null, 'Не найдены все необходимые поля на листе');
                    Template::set_page('import', 'Импорт торговых марок в категориях', 'tm_in_cat');
                }
            } else {
                Template::set_form(null, $result);
                Template::set_page('import', 'Импорт торговых марок в категориях', 'tm_in_cat');
            }


        } else {
            Template::set_page('import', 'Импорт торговых марок в категориях', 'tm_in_cat');
        }

    }
}
