<?php

/**
 * Уравление категориями.
 */
class Controller_categories extends Controller_Base
{

    /**
     * Список категорий либо форма редактирования категории.
     * @param array $args Параметры URL.
     */
    function index($args)
    {
        Template::add_css('flick/jquery-ui.min.css');
        Template::add_script('jquery-ui.min.js');
        Template::add_script('admin/categories.js');

        Template::set_page('categories', 'Редактировать описания категорий', Categories::get_categories());
    }

    /**
     * Добавляет новую категорию
     * @param array $args Параметры URL
     */
    function add_category($args)
    {
        $name = isset($_POST['name']) ? DB::mysql_secure_string(trim($_POST['name'])) : '';
        $parent_id = !empty($_POST['parent_id']) ? (int)$_POST['parent_id'] : 0;
        $subcat_id = !empty($_POST['subcat_id']) ? (int)$_POST['subcat_id'] : 0;
        $subcat = isset($_POST['subcat']) ? DB::mysql_secure_string(trim($_POST['subcat'])) : '';

        if ($name === '' || $parent_id < 0 || $parent_id > 0 && !($parent_info = Categories::get_category($parent_id)) || !empty($parent_info) && $parent_info['level'] > 1 && $subcat_id < 1 && $subcat === '') {
            $this->jsonResponse(false);
        }

        $parent_2 = 0;

        $level = 1;

        if ($parent_id > 0) {
            switch ((int)$parent_info['level']) {
                case 1:
                    $level = 2;

                    break;

                case 2:
                    $level = 3;

                    $parent_2 = $parent_info['parent_1'];

                    break;

                default:
                    $this->jsonResponse(false);

                    break;
            }
        }

        $parent_1 = $parent_id;

        $new_id = Categories::add_category($name, $level, $parent_1, $parent_2, !empty($_POS['is_tag']));

        $added = false;

        if ($new_id) {
            if ($subcat !== '') {
                $added_sub = Categories::add_subcategory($level, $subcat);

                if ($added_sub) {
                    $subcat_id = Categories::last_subcategory();

                    $subcat_id = $subcat_id['id'];

                    $added = true;
                }
            } else {
                if ($subcat_id) {
                    if (Categories::get_subcategory($subcat_id)) {
                        $added = true;
                    }
                }
            }
        }

        $result = $added ? ($subcat_id < 1 ?: Categories::set_subcat_link($new_id, $subcat_id)) : false;

        if ($result) {
            caching::clean();
        }

        $this->jsonResponse($result);
    }
}
