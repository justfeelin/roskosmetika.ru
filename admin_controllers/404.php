﻿<?php

/**
 * Обработка ошибки 404 в админке.
 *
 */
class Controller_404 extends Controller_Base
{

    function index($args)
    {
        header("HTTP/1.0 404 Not Found");
        Template::set_page('404', 'Ошибка 404', 'Раздел отсутствует.');
    }

}

?>