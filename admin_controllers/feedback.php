<?php

/**
 * Управление статьями.
 */
class Controller_feedback extends Controller_Base
{

    /**
     * Список статей.
     * @param array $args Параметры URL.
     */
    function index($args)
    {
        $page_header = 'Обратная связь';

        Pagination::setBaseUrl('/admin/feedback');

        Template::set_page('feedback', $page_header, [
            'items' => feedback::get_list(20),
        ]);
    }

    /**
     * @inheritdoc
     */
    protected function admin_access()
    {
        return admin::RIGHTS_COMMON | admin::RIGHTS_STATISTICS;
    }
}
