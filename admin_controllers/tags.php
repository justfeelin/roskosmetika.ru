<?php

class Controller_tags extends Controller_Base
{
    public function index($arg)
    {
        if (!isset($arg[0])) {
            Template::add_script('admin/tags.js');

            Template::set_page('tags', 'Теги', [
                'tags' => categories::get_tags(true, isset($_GET['sort_products']) ? $_GET['sort_products'] === 'asc' : null),
            ]);
        } else {
            $tag = categories::get_by_id((int)$arg[0]);

            if (!$tag || !$tag['is_tag']) {
                $this->nopage();

                return;
            }

            admin::products_search_form(
                function ($productIDs) use ($tag) {
                    categories::add_products($tag['id'], $productIDs);
                },
                function () use ($tag) {
                    return categories::linked_products($tag['id']);
                },
                function ($productIDs) use ($tag) {
                    categories::remove_products($tag['id'], $productIDs);
                },
                function ($tagIDs) use ($tag) {
                    categories::copy_from_tags($tag['id'], $tagIDs);
                },
                null,
                true
            );

            Template::set_page('tag', 'Тег', [
                'tag' => $tag,
            ]);
        }
    }

    /**
     * Clear products from tag
     *
     * @param array $args
     */
    public function clear_products($args)
    {
        categories::clear_products($args[0]);

        $this->_redirectBack();
    }

    /**
     * Toggles visibility for tag
     *
     * @param array $args
     */
    public function toggle_visibility($args)
    {
        categories::toggle_visibility($args[0]);

        $this->_redirectBack();
    }

    /**
     * Back url redirect with support of GET parameters
     */
    private function _redirectBack()
    {
        redirect(isset($_GET['bu']) ? $_GET['bu'] : '/admin/tags');
    }

    /**
     * @inheritdoc
     */
    protected function admin_access()
    {
        return admin::RIGHTS_COMMON | admin::RIGHTS_TAGS_EDITOR;
    }
}
