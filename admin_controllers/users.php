<?php

/**
 * Список клиентов.
 */
class Controller_users extends Controller_Base
{
    /**
     * Список всех зарегистрированных пользователей.
     * @param array $args Параметры URL.
     */
    function index($args)
    {
        $client_id = isset($args[0]) ? (int)$args[0] : 0;

        if ($client_id > 0) {
            $client = Users::get_client_by_id($client_id);

            if (!$client) {
                $this->nopage();
            } else {
                Template::set_page('client_info', 'Информация о клиенте', $client);
            }
        } else {
            Pagination::setBaseUrl('/admin/users');

            Template::set_page('users', 'Зарегистрированные пользователи', [
                'users' => Users::get_users(30),
            ]);
        }
    }
}
