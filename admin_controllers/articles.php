<?php

/**
 * Управление статьями.
 */
class Controller_articles extends Controller_Base
{

    /**
     * Список статей.
     * @param array $args Параметры URL.
     */

    function index($args)
    {
        Template::set_page('art_list', 'Редактирование статей', [
            'articles' => Articles::get_all(true),
        ]);
    }

    /**
     * Форма редактирования статьи или темы.
     * @param array $args Параметры URL.
     */
    function edit($args)
    {
        if (isset($args[0]))
            $art_id = (int)$args[0];

        // Сохраняем статью и выводим список статей.
        if (isset($_POST['save']) && !empty($_POST['name']) && !empty($_POST['url'])) {

            $id = (int)$_POST['id'];
            $url = $_POST['url'];
            $name = $_POST['name'];
            $title = (isset($_POST['title']) ? $_POST['title'] : '');
            $description = (isset($_POST['description']) ? $_POST['description'] : '');
            $keywords = (isset($_POST['keywords']) ? $_POST['keywords'] : '');
            $synonym = (isset($_POST['synonym']) ? $_POST['synonym'] : '');
            $text = (isset($_POST['text']) ? $_POST['text'] : '');
            $kol = Articles::check_url($url, $id);

            if ($kol['kol'] == 0 || $url != '') {
                Articles::set_art($id, $url, $name, $title, $description, $keywords, $synonym, $text);
            }

            Template::set_page('art_list', 'Редактирование статей', Articles::get_all(true));
        } // Выводим список статей.
        elseif (isset($_POST['save']) && empty($_POST['name'])) {
            Template::set_page('art_list', 'Редактирование статей', Articles::get_all(true));
        } // Выводим форму.
        else {
            Template::set_page('edit_art', 'Редактирование статей', Articles::get_art($art_id, true));
        }
    }

    /**
     * Форма добавление статьи или темы.
     * @param array $args Параметры URL.
     */
    function add($args)
    {

        if (isset($args[0])) {
            $art_id = (int)$args[0];
        } else {
            $art_id = 0;
        }

        // Сохраняем новую статью.
        if (isset($_POST['save']) && !empty($_POST['name']) && !empty($_POST['url'])) {

            $url = $_POST['url'];
            $name = $_POST['name'];
            $title = (isset($_POST['title']) ? $_POST['title'] : '');
            $description = (isset($_POST['description']) ? $_POST['description'] : '');
            $keywords = (isset($_POST['keywords']) ? $_POST['keywords'] : '');
            $synonym = (isset($_POST['synonym']) ? $_POST['synonym'] : '');
            $text = (isset($_POST['text']) ? $_POST['text'] : '');
            $parent_id = (int)$_POST['parent_id'];
            $kol = Articles::check_url($url, $art_id);
            if ($kol['kol'] == 0) {
                Articles::add_art($url, $name, $title, $description, $keywords, $text, $synonym, $parent_id);
            }

            Template::set_page('art_list', 'Редактирование статей', Articles::get_all(true));
        } elseif (isset($_POST['save']) && empty($_POST['name'])) {

            Template::set_page('art_list', 'Редактирование статей', Articles::get_all(true));
        } // Выводим форму для добавления статей.
        else {
            Template::set_page('edit_art', 'Редактирование статей', $art_id);
        }
    }

    /**
     * Скрывает/отображает статью/раздел.
     * @param array $args Параметры URL.
     */
    function show($args)
    {

        if (isset($args[0]))
            $art_id = (int)$args[0];

        if (!empty($art_id)) {
            Articles::show_art($art_id);
        }

        redirect('/admin/articles/');
    }

}

?>