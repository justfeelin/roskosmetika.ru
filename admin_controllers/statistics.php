<?php

class Controller_statistics extends Controller_Base
{
    /**
     * Statistics page
     *
     * @param $arg
     */
    public function index($arg)
    {
        $stats = $fields = null;

        if (isset($_GET['date_added_from'])) {
            $dtAddedFrom = !empty($_GET['date_added_from']) ? date('Y-m-d' . ($_GET['stat_type'] !== 'trademarks' ? ' 00:00:00' : ''), strtotime($_GET['date_added_from'])) : null;
            $dtAddedTo = (!empty($_GET['date_added_to'])) ? date('Y-m-d' . ($_GET['stat_type'] !== 'trademarks' ? ' 23:59:59' : ''), strtotime($_GET['date_added_to'])) : null;

            $dtDeliveryFrom = !empty($_GET['date_delivery_from']) ? date('Y-m-d' . ($_GET['stat_type'] !== 'trademarks' ? ' 00:00:00' : ''), strtotime($_GET['date_delivery_from'])) : null;
            $dtDeliveryTo = (!empty($_GET['date_delivery_to']) && $_GET['stat_type'] !== 'sku_sales' && $_GET['stat_type'] !== 'cart_stat') ? date('Y-m-d' . ($_GET['stat_type'] !== 'trademarks' ? ' 23:59:59' : ''), strtotime($_GET['date_delivery_to'])) : null;

            switch ($_GET['stat_type']) {
                case 'orders':
                    $stats = statistics::orders($dtAddedFrom, $dtAddedTo, $dtDeliveryFrom, $dtDeliveryTo);

                    break;

                case 'trademarks':
                    $byAdded = empty($_GET['by_delivery_date']);

                    $stats = statistics::trademarks($byAdded, $byAdded ? $dtAddedFrom : $dtDeliveryFrom, $byAdded ? $dtAddedTo : $dtDeliveryTo);

                    break;

                case 'clients':
                    $stats = statistics::clients($dtAddedFrom, $dtAddedTo, $dtDeliveryFrom, $dtDeliveryTo);

                    break;

                case 'days':
                    $stats = statistics::days($dtAddedFrom, $dtAddedTo);

                    break;

                case 'promo':
                    $stats = statistics::promo($dtAddedFrom, $dtAddedTo);

                    break;    

                case 'sku_sales':
                    $stats = statistics::sku_sales($dtAddedFrom, $dtAddedTo);

                    break;

                case 'cart_stat':
                    $stats = statistics::cart_stat($dtAddedFrom, $dtAddedTo);

                    break;
            }

            if ($stats) {
                $fields = statistics::fields($_GET['stat_type']);

                if (!empty($_GET['to_excel'])) {
                    $fieldsN = count($fields);

                    $excel = export::create_new_file();

                    $excel->setActiveSheetIndex(0);
                    $list = $excel->getActiveSheet();

                    for ($i = 0; $i < $fieldsN; ++$i) {
                        $column = chr(65 + $i);

                        $list->setCellValue($column . '1', $fields[$i]['name']);                       
                        $list->getStyle($column . '1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                        $list->getStyle($column . '1')->getFill()->getStartColor()->setRGB('fffe8d');
                        $list->getStyle($column . '1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                        $list
                            ->getColumnDimension($column)
                            ->setAutoSize(true);

                        $list
                            ->getStyle($column . '1')
                            ->getFont()
                            ->setBold(true);


                    }

                    for ($i = 0, $n = count($stats); $i < $n; ++$i) {
                        for ($y = 0; $y < $fieldsN; ++$y) {
                            $list->setCellValue(chr(65 + $y) . ($i + 2), $stats[$i][$fields[$y]['field']]);
                        }
                    }

                    export::download_excel(
                        $excel,
                        $_GET['stat_type'] .
                            (
                                $dtAddedFrom || $dtDeliveryFrom || $dtAddedTo || $dtDeliveryTo
                                    ? '_' .
                                        (
                                            $dtAddedFrom || $dtDeliveryFrom
                                                ? date(
                                                    'Y-m-d',
                                                    strtotime(
                                                        $dtAddedFrom
                                                            ? $_GET['date_added_from']
                                                            : $_GET['date_delivery_from']
                                                    )
                                                )
                                                : ''
                                        ) .
                                        (
                                            ($dtAddedFrom || $dtDeliveryFrom) && ($dtAddedTo || $dtDeliveryTo)
                                                ? '-'
                                                : ''
                                        ) .
                                        (
                                            $dtAddedTo || $dtDeliveryTo
                                                ? date(
                                                    'Y-m-d',
                                                    strtotime(
                                                        $dtAddedFrom
                                                            ? $_GET['date_added_to']
                                                            : $_GET['date_delivery_to']
                                                    )
                                                )
                                                : ''
                                        )
                                    : ''
                            )
                    );
                }
            }
        }

        Template::add_css('flick/jquery-ui.min.css');
        Template::add_script('jquery-ui.min.js');
        Template::add_script('jquery-ui-datepicker-ru.js');

        Template::add_script('admin/statistics.js');

        Template::set_page('statistics', 'Статистика', [
            'stats' => $stats,
            'fields' => $fields,
        ]);
    }

    /**
     * @inheritdoc
     */
    protected function admin_access()
    {
        return admin::RIGHTS_COMMON | admin::RIGHTS_STATISTICS;
    }
}
