<?php

/**
 * Вывод вопросов LO.
 */
class Controller_live_operator extends Controller_Base
{
    function index($args)
    {
        Pagination::setBaseUrl('/admin/live_operator');

        Template::set_page('live_operator', 'Live operator', [
            'items' => live_operator::get_messages(20),
        ]);
    }

     /**
     * @inheritdoc
     */
    protected function admin_access()
    {
        return admin::RIGHTS_COMMON | admin::RIGHTS_STATISTICS;
    }
}
