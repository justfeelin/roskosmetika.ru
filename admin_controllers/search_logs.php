<?php

/**
 * История поиска.
 */
class Controller_Search_logs extends Controller_Base
{
    const PAGE_HEADER = 'История поиска на сайте';

    /**
     * Выводит историю поиска по сайту.
     * @param <type> $args
     */
    public function index($args)
    {
        $onpage = 50;

        list($search, $where) = $this->_searchParams();

        $count = Search_Logs::get_count($where);

        $pagination = $this->pagination($count, $onpage, $search);

        $logs = Search_Logs::get_rows($pagination['page'], $onpage, $where);

        Template::add_css('flick/jquery-ui.min.css');

        Template::add_script('jquery-ui.min.js');
        Template::add_script('jquery-ui-datepicker-ru.js');

        Template::add_script('admin/search_logs.js');

        if ($search) {
            $search_url = array();

            foreach ($search as $key => $value) {
                $search_url[] = $key . '=' . h($value);
            }

            $search_url = '?' . implode('&', $search_url);
        } else {
            $search_url = '';
        }

        Template::set_page('search_logs', self::PAGE_HEADER, array(
            'logs' => $logs,
            'search' => $search,
            'search_url' => $search_url,
        ));
    }

    /**
     * Выгрузка файла логов поиска.
     */
    public function upload()
    {
        // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
        if (ob_get_level()) {
            ob_end_clean();
        }

        $where = $this->_searchParams(false);

        $logs = Search_Logs::get_rows(null, null, $where);

        $n = sizeof($logs);

        header('Content-Description: File Transfer');
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        $file = tempnam(sys_get_temp_dir(), 'logs');

        if (isset($_GET['excel'])) {
            $type = 'application/vnd.ms-excel';
            $filename = 'logs.xls';

            $excel = export::create_new_file();

            $excel->setActiveSheetIndex(0);
            $list = $excel->getActiveSheet();

            $list->setCellValue('A1', 'Фраза');
            $list->setCellValue('B1', 'Дата');

            $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
            $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);

            $excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
            $excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(12);

            for ($i = 0; $i < $n; ++$i) {
                $list->setCellValue('A' . ($i + 2), $logs[$i]['search']);
                $list->setCellValue('B' . ($i + 2), $logs[$i]['dt']);
            }

            $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
            $writer->save($file);
        } else {
            $type = 'plain/text';
            $filename = 'logs.txt';

            for ($i = 0; $i < $n; ++$i) {
                file_put_contents($file, '[' . $logs[$i]['dt'] . ']     ' . $logs[$i]['search'] . "\n", FILE_APPEND);
            }
        }

        header('Content-Type: ' . $type);
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        header('Content-Length: ' . filesize($file));

        @ini_set('max_execution_time', 240);

        readfile($file);

        unlink($file);

        exit;
    }

    /**
     * Parses GET search params
     *
     * @param bool $withSearch Whether to return $search and $where
     *
     * @return array [$search, $where]. $search: associative array of params, $where: SQL conditions (Without `WHERE`). Or only $where
     */
    private function _searchParams($withSearch = true)
    {
        $search = array();

        $search['query'] = !empty($_GET['query']) ? trim($_GET['query']) : null;
        $search['date_from'] = isset($_GET['date_from']) && preg_match('/^\d{4}-\d{2}-\d{2}$/', $_GET['date_from']) ? $_GET['date_from'] : null;
        $search['date_to'] = isset($_GET['date_to']) && preg_match('/^\d{4}-\d{2}-\d{2}$/', $_GET['date_to']) && $_GET['date_to'] >= $search['date_from'] ? $_GET['date_to'] : null;

        $where = array();

        if ($search['query']) {
            $where[] = 'search LIKE "%' . DB::mysql_secure_string($search['query']) . '%"';
        }

        if ($search['date_from']) {
            $where[] = 'dt >= "' . $search['date_from'] . ' 00:00:00"';
        }

        if ($search['date_to']) {
            $where[] = 'dt <= "' . $search['date_to'] . ' 23:59:59"';
        }

        $where = $where ? implode(' AND ', $where) : '';

        return $withSearch ? array($search, $where) : $where;
    }

    /**
     * @inheritdoc
     */
    protected function admin_access()
    {
        return admin::RIGHTS_COMMON | admin::RIGHTS_STATISTICS;
    }
}
