<?php

/**
 * Контроллер для создания списка рассылки с учетом отказавшихся от рассылки.
 *
 */
class Controller_Unsubscribe extends Controller_Base
{

    function index($arg)
    {
        Template::set('num_and_date', unsubscribe::number_and_date());
        Template::set_page('unsubscribe', 'Рассылка');
    }

    /*
     * Блокировка мертвых адресов
     */

    static function block_emails($args)
    {

        //	Проверяем отправлен ли файл
        if (isset($_POST['submit_block'])) {

            $result = import::check_MIME('block', 'application/vnd.ms-excel');
            //	Проверяем есть ли файл вообще

            //	Начинаем импорт
            if ($result == 1) {

                $excel = import::load_file($_FILES['file_block']['tmp_name']);
                $excel->setActiveSheetIndex(0);
                $main = $excel->getActiveSheet();
                $max_row = $main->getHighestRow();

                for ($i = 1; $i <= $max_row; $i++) {

                    $mail = $main->getCellByColumnAndRow(0, $i)->getValue();
                    $check_mail = unsubscribe::check_existence($mail);
                    if ($check_mail)
                        unsubscribe::change_block($check_mail['email']);
                }
                Template::set_page('service_info', 'Рассылка', 'Существущие адреса из файла заблокированы.');
            }
        }
    }

    /**
     * Форма редактирования комментария
     * @param array $args Параметры URL.
     */
    function edit_comment($args)
    {

        // вывод формы редактирования
        if (isset($args[0])) {
            $number = (int)$args[0];

            Template::set('number', $number);
            Template::set('comment', unsubscribe::get_comment($number));
            Template::set_page('unsub_comment', 'Редактирование комментариев');
        }


        // Сохрянение изменения
        if (isset($_POST['save'])) {
            $comment = $_POST['comment'];
            $number = (int)$_POST['number'];

            unsubscribe::edit_comment($number, $comment);
            redirect('/admin/unsubscribe');
        }
    }
}

?>
