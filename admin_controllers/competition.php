<?php
/**
 * Управление акциями
 */

class Controller_competition extends Controller_Base
{

    /**
     * Выводит список всех акций.
     * @param <type> $args
     */
    function index($args)
    {
        $this->comp_list();
    }

    /**
     * Выводит список всех акций
     */
    function comp_list()
    {
        $page_header = 'Список акций';
        Pagination::setBaseUrl('/admin/competition');
        Template::set_page('competition_list', $page_header, [
            'comp' => Competition::get_competition_list(false, 20, true),
        ]);
    }

    /**
     * Изменяет видимость акции.
     * @param array $args URL параметры.
     */
    function change_visibility($args)
    {
        competition::change_competition_visibility($args[0]);

        $this->go_back();
    }

    /**
     * Изменяет статус победителя.
     * @param array $args URL параметры.
     */
    function change_win($args)
    {
        competition::change_winner($args[0]);

        $this->go_back();
    }

    /**
     * Выводит форму добавления акции.
     * @param <type> $args
     */
    function add_competition($args)
    {
        $page_header = "Новая акция";
        Template::add_script('admin/banner.js');

        Template::add_css('flick/jquery-ui.min.css');
        Template::add_script('jquery-ui.min.js');
        Template::add_script('jquery-ui-datepicker-ru.js');
        Template::set('content', []);
        Template::set_page('competition_edit', $page_header);
    }

    /**
     * Сохраняет изменения в акциях
     * @param <type> $args
     */
    function save($args)
    {
        if (isset($_POST['submit'])) {
            $id = (isset($_POST['id']) ? (int)$_POST['id'] : 0);
            $name = $_POST['name'];
            $url = $_POST['link'];
            $alt = $_POST['alt'];
            $win_link = $_POST['win_link'];
            $description = $_POST['text'];
            $begin = $_POST['dates_begin'];
            $end = $_POST['dates_end'];
            $img_full = $_POST['file_name'];
            $img_name = explode(".", $img_full);
            $img = $img_name[0];
            $fileExt = $img_name[1];

            $visible = (isset($_POST['visible']) && $_POST['visible'] == 'on' ? 1 : 0);

            if (isset($_FILES['pic']))
                $pic = (($_FILES['pic']['error'] == UPLOAD_ERR_NO_FILE) ? FALSE : TRUE);
            else {
                $pic = FALSE;
            }

            if ($pic) {
                $fileExt = strtolower(pathinfo($_FILES['pic']['name'], PATHINFO_EXTENSION));

                if (!preg_match('/.+\.' . preg_quote($fileExt) . '$/i', $img_full)) {
                    $img_full .= '.' . $fileExt;
                }

                if ($id) {
                    $file_info = competition::get_competition($id);
                    $old_file = $file_info['file_name'].$file_info['type'];
                    if (!$old_file) $old_file = 'none';

                    if ($fileExt === 'jpg' || $fileExt === 'jpeg') {
                        if (file_exists('../images/competition/' . $img_full)) @unlink('../images/competition/' . $img_full);
                        if (file_exists('../images/competition/' . 'min_' . $img_full)) @unlink('../images/competition/' . 'min_' . $img_full);
                        if (file_exists('../images/competition/' . $old_file)) @unlink('../images/competition/' . $old_file);
                        if (file_exists('../images/competition/' . 'min_' . $old_file)) @unlink('../images/competition/' . 'min_' . $old_file);
                        $file_path = '../images/competition/' . $img_full;
                        @move_uploaded_file($_FILES['pic']['tmp_name'], $file_path);
                        @copy($file_path, '../images/competition/' . 'min_' . $img_full);
                        images::set_img_size('../images/competition/' . 'min_' . $img_full, 150, 95);
                    } elseif ($fileExt === 'gif') {
                        if (file_exists('../images/competition/' . $img_full)) @unlink('../images/competition/' . $img_full);
                        if (file_exists('../images/competition/' . 'min_' . $img_full)) @unlink('../images/competition/' . 'min_' . $img_full);
                        if (file_exists('../images/competition/' . $old_file)) @unlink('../images/competition/' . $old_file);
                        if (file_exists('../images/competition/' . 'min_' . $old_file)) @unlink('../images/competition/' . 'min_' . $old_file);
                        $file_path = '../mages/competition/' . $img_full;
                        move_uploaded_file($_FILES['pic']['tmp_name'], $file_path);
                        @copy($file_path, '../images/competition/' . 'min_' . $img_full);
                        images::set_img_size('../images/competition/' . 'min_' . $img_full, 150, 95);
                    } elseif ($fileExt === 'png') {
                        if (file_exists('../images/competition/' . $img_full)) @unlink('../images/competition/' . $img_full);
                        if (file_exists('../images/competition/' . 'min_' . $img_full)) @unlink('../images/competition/' . 'min_' . $img_full);
                        if (file_exists('../images/competition/' . $old_file)) @unlink('../images/competition/' . $old_file);
                        if (file_exists('../images/competition/' . 'min_' . $old_file)) @unlink('../images/competition/' . 'min_' . $old_file);
                        $file_path = '../images/competition/' . $img_full;
                        move_uploaded_file($_FILES['pic']['tmp_name'], $file_path);
                        @copy($file_path, '../images/competition/' . 'min_' . $img_full);
                        images::set_img_size('../images/competition/' . 'min_' . $img_full, 150, 95);
                    } else {
                        Template::add_css('flick/jquery-ui.min.css');
                        Template::add_script('jquery-ui.min.js');
                        Template::add_script('jquery-ui-datepicker-ru.js');

                        Template::set_page('competition_edit', 'Редактирование акции', $file_info);
                    }


                } else {

                    if ($fileExt === 'jpg' || $fileExt === 'jpeg') {
                        if (file_exists('../images/competition/' . $img_full)) @unlink('../images/competition/' . $img_full);
                        if (file_exists('../images/competition/' . 'min_' . $img_full)) @unlink('../images/competition/' . 'min_' . $img_full);
                        $file_path = '../images/mc/' . $img_full;
                        move_uploaded_file($_FILES['pic']['tmp_name'], $file_path);
                        @copy($file_path, '../images/competition/' . 'min_' . $img_full);
                        images::set_img_size('../images/competition/' . 'min_' . $img_full, 150, 95);
                    } elseif ($fileExt === 'gif') {
                        if (file_exists('../images/competition/' . $img_full)) @unlink('../images/competition/' . $img_full);
                        if (file_exists('../images/competition/' . 'min_' . $img_full)) @unlink('../images/competition/' . 'min_' . $img_full);
                        $file_path = '../mages/competition/' . $img_full;
                        move_uploaded_file($_FILES['pic']['tmp_name'], $file_path);
                        @copy($file_path, '../images/competition/' . 'min_' . $img_full);
                        images::set_img_size('../images/competition/' . 'min_' . $img_full, 150, 95);
                    } elseif ($fileExt === 'png') {
                        if (file_exists('../images/competition/' . $img_full)) @unlink('../images/competition/' . $img_full);
                        if (file_exists('../images/competition/' . 'min_' . $img_full)) @unlink('../images/competition/' . 'min_' . $img_full);
                        $file_path = '../images/mc/' . $img_full;
                        move_uploaded_file($_FILES['pic']['tmp_name'], $file_path);
                        @copy($file_path, '../images/competition/' . 'min_' . $img_full);
                        images::set_img_size('../images/competition/' . 'min_' . $img_full, 150, 95);
                    } else {

                        Template::add_css('flick/jquery-ui.min.css');
                        Template::add_script('jquery-ui.min.js');
                        Template::add_script('jquery-ui-datepicker-ru.js');

                        Template::set_page('competition_add', 'Добавление акции');
                    }
                }

            }
            // vd($fileExt); exit;
            competition::save_competition($id, $url, $description, $name, $img, $fileExt, $alt, $win_link, $begin, $end, $visible);

            caching::delete('index_mcs');
        }
        $this->comp_list();
    }

    /**
     * Выводит ворму редактирования акции.
     * @param <type> $args
     */
    function edit($args)
    {
        if (isset($args[0])) {
            $page_header = "Редактирование акции";
            $id = (int)$args[0];

            Template::add_script('admin/banner.js');

            Template::add_css('flick/jquery-ui.min.css');
            Template::add_script('jquery-ui.min.js');
            Template::add_script('jquery-ui-datepicker-ru.js');

            Template::set_page('competition_edit', $page_header, competition::get_competition($id));
        } // Ввыводим список мастер-классов.
        else {
            $this->index($args);
        }
    }

    function subscribes($args)
    {
        if (isset($args[0])) {
            $page_header = "Участниким акции";
            $id = (int)$args[0];

            $subscribes = competition::get_subscribes_list($id);
            $competition = competition::get_competition($id);

            Template::set('subscribes', $subscribes);
            Template::set('competition', $competition);
            Template::set_page('competition_subscribes', $page_header);
        }
        else {

        }
    }

    function upload($args)
    {
        if (isset($args[0])) {
            $competition = competition::get_competition($args[0]);

            if (!$competition) {
                $this->nopage();

                return;
            }

            $id = (int)$args[0];
        } else {
            $id = 0;
        }

        $excel = xls::get_competition_subscribes(competition::get_subscribes_list($id), $id ? $competition['name']  . ' ' . date('d.m.Y', strtotime($competition['end'])) : null);

        export::download_excel($excel, 'competition');
    }

}