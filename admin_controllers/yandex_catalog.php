<?php

class Controller_yandex_catalog extends Controller_Base
{

    /**
     * Import yandex catalog
     */
    function index($arg)
    {
        //error catch
        set_error_handler('catch_error');

        if (isset($_POST['submit'])) {

            $result = import::check_MIME('yandex_catalog', 'application/vnd.ms-excel');

            if ($result == 1) {

                $excel = import::load_file($_FILES['file_yandex_catalog']['tmp_name']);

                $fields = array(0 => 'id',
                                1 => 'parent_id',
                                2 => 'market_category',
                                3 => 'use',
                                4 => 'cat_full_url');

                $return_fields = import::check_fields($excel, $fields);

                if ($return_fields) {
                    //  Все проверки файла пройдены. Начало импорта.               
                    //  сообщение о строках без данных, повторяющемся url, ошибках 
                    $msg = array('main_fields' => '',
                        'error' => '');
                    //информационные сообщения о количестве импортированных и замененных данных
                    $info_msg = array('Связка Яндекс каталога' => array('insert' => 0, 'update' => 0));

                    $excel->setActiveSheetIndex(0);
                    $main = $excel->getActiveSheet();
                    $max_row = $main->getHighestRow();
                    //в файле 2 столбца
                    $max_column = PHPExcel_Cell::columnIndexFromString($main->getHighestColumn());

                    //  обнуление старых значений
                    DB::query("TRUNCATE __yandex_catalog");

                    // парсинг листа и иимпорт
                    for ($i = 2; $i <= $max_row; $i++) {
                        //проверка на ошибки
                        if (isset($GLOBALS['main_error'])) {
                            $msg['error'] = $GLOBALS['main_error'];
                            break;
                        }

                        //  импорт основных полей
                        $cat_id = (int)$main->getCellByColumnAndRow(0, $i)->getValue();
                        $parent_id = (int)$main->getCellByColumnAndRow(1, $i)->getValue();
                        $market_category = trim($main->getCellByColumnAndRow(2, $i)->getValue());
                        $use = (int)$main->getCellByColumnAndRow(3, $i)->getValue();
                        $cat_full_url = trim($main->getCellByColumnAndRow(4, $i)->getValue());
                        $cat_name = '';

                        $last_slash = strripos($market_category, '/');

                        if ($last_slash) {
                            $cat_name = substr($market_category, $last_slash + 1);
                        } else {
                            $cat_name = $market_category;
                        }

                        DB::query("INSERT INTO __yandex_catalog (cat_id, parent_id, name, market_category, `use`, cat_full_url) 
                                   VALUES ($cat_id, $parent_id, '$cat_name', '$market_category', $use, '$cat_full_url')");

                        $info_msg['Связка Яндекс каталога'] = import::count_info($info_msg['Связка Яндекс каталога']);

                    }

                    if (empty($msg['main_fields']) && empty($msg['error'])) {

                        $content = '<p>Импорт связей яндекс каталога успешно завершен.</p>' . import::report_msg($info_msg, 'info');
                        Template::set_page('index_success', 'Импорт связей яндекс каталога', $content);

                    } else {

                        $content = import::report_msg($msg, 'error') . '<br><p>Загруженные связи статья-продукт.</p>' . import::report_msg($info_msg, 'info');
                        Template::set_page('index_info', 'Импорт связей яндекс каталога', $content);
                    }


                } else {
                    Template::set_form(null, 'Не найдены все необходимые поля.');
                    Template::set_page('yandex_catalog', 'Импорт связей яндекс каталога', 'yandex_catalog');
                }
            } else {
                Template::set_form(null, $result);
                Template::set_page('yandex_catalog', 'Импорт связей яндекс каталога', 'yandex_catalog');
            }


        } else {
            Template::set_page('yandex_catalog', 'Импорт связей яндекс каталога', 'yandex_catalog');
        }

    }

}

?>