<?php

/**
 * Обработка файлов.
 */
define('header', 'Управление файлами загрузок');

class Controller_files extends Controller_Base
{

    function index($args)
    {
        Template::set_page('files', header);
    }

    function edit($args)
    {

        if (isset($args[0]))
            $file = DB::mysql_secure_string($args[0]);

        if (isset($_POST['save']) && isset($_FILES['filename'])) {

            //	Проверяем есть ли файл вообще
            if ($_FILES['filename']['error'] == UPLOAD_ERR_NO_FILE) {

                Template::set_page('files', header);
            } //	Проверяем тип файла
            elseif ($_FILES['filename']['type'] != 'application/vnd.ms-excel' &&
                $_FILES['filename']['type'] != 'application/vnd.ms-word' &&
                $_FILES['filename']['type'] != 'application/msword' &&
                $_FILES['filename']['type'] != 'application/octet-stream'
            ) {

                Template::set_page('files', header);
            } //	Начинаем импорт
            else {
                @unlink(SITE_PATH . 'uploads/roskosmetika_order_form.xls');
                move_uploaded_file($_FILES['filename']['tmp_name'], SITE_PATH . 'uploads/roskosmetika_order_form.xls');
                Template::set_page('files', header);
            }
        } else {

            Template::set_page('edit_files', header);

        }
    }


}

?>