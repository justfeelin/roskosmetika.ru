<?php

class Controller_partner extends Controller_Base
{

    /*
     * Вывод всех программ.
     */

    function index($arg)
    {
        partner::update_all();
        Template::set_page('partner', 'Партнерские программы', [
            'partners' => Partner::get_list(),
        ]);
    }


    /*
     * Настройки для партнерских программ
     * @param array $args Параметры URL.
     */

    function settings($arg)
    {

        //получаем строку настроек (id = 1)

        //Соответствие полей настройкам(только для id = 1)
        //name - название компании
        //file_name -  url компании
        //utm_medium - телефон
        //utm_source - валюта
        //phone - цена доставки
        //utm_campaign - примечания
        //catalog - имя магазина

        $settings = Partner::get_partner(1);

        //Выводим настройки для редактирования
        if (!empty($settings) && !isset($_POST['save'])) {
            Template::set_page('pp_settings', 'Партнерские программы. Настройки.', $settings);
        } // Сохраняем изменения
        elseif (isset($_POST['save'])) {

            $name = (isset($_POST['name']) ? $_POST['name'] : '');
            $url = (isset($_POST['url']) ? $_POST['url'] : '');
            $phone = (isset($_POST['phone']) ? $_POST['phone'] : '');
            $currency = (isset($_POST['currency']) ? $_POST['currency'] : '');
            $delivery = (int)$_POST['delivery'];
            $sales_notes = (isset($_POST['sales_notes']) ? $_POST['sales_notes'] : '');
            $shop_name = (isset($_POST['shop_name']) ? $_POST['shop_name'] : '');

            if (!get_magic_quotes_gpc()) {
                $name = DB::mysql_secure_string($name);
                $url = DB::mysql_secure_string($url);
                $phone = DB::mysql_secure_string($phone);
                $currency = DB::mysql_secure_string($currency);
                $delivery = DB::mysql_secure_string($delivery);
                $sales_notes = DB::mysql_secure_string($sales_notes);
                $shop_name = DB::mysql_secure_string($shop_name);
            }

            Partner::set_settings($name, $url, $shop_name, $phone, $currency, $delivery, $sales_notes);

            redirect('/admin/partner/');
        } // Выводим список страниц
        else {
            redirect('/admin/partner/');
        }


    }


    /**
     * Форма добавления партнерской программы
     * @param array $args Параметры URL.
     */
    function add($args)
    {

        // Сохраняем новый совет.
        if (isset($_POST['save']) && !empty($_POST['name']) && !empty($_POST['file_name'])) {

            $name = $_POST['name'];
            $file_name = $_POST['file_name'];
            $equip = (isset($_POST['equip']) ? $_POST['equip'] : 0);
            $utm_source = (isset($_POST['utm_source']) ? $_POST['utm_source'] : '');
            $utm_medium = (isset($_POST['utm_medium']) ? $_POST['utm_medium'] : '');
            $utm_campaign = (isset($_POST['utm_campaign']) ? $_POST['utm_campaign'] : '');
            $catalog = (isset($_POST['catalog']) ? $_POST['catalog'] : '');
            $shop_name = (isset($_POST['shop_name']) ? $_POST['shop_name'] : '');
            $company = (isset($_POST['company']) ? $_POST['company'] : '');
            $phone = (isset($_POST['phone']) ? $_POST['phone'] : '');
            $shop_url = (isset($_POST['shop_url']) ? $_POST['shop_url'] : '');
            $currency = (isset($_POST['currency']) ? $_POST['currency'] : '');
            $delivery = (isset($_POST['delivery']) ? $_POST['delivery'] : 0);
            $local_delivery_cost = (isset($_POST['local_delivery_cost']) ? (int)$_POST['local_delivery_cost'] : 0);


            $categories = (isset($_POST['categories']) ? (int)$_POST['categories'] : 0);
            $url = (isset($_POST['url']) ? (int)$_POST['url'] : 0);
            $price = (isset($_POST['price']) ? (int)$_POST['price'] : 0);
            $picture = (isset($_POST['picture']) ? (int)$_POST['picture'] : 0);
            $store = (isset($_POST['store']) ? (int)$_POST['store'] : 0);
            $pickup = (isset($_POST['pickup']) ? (int)$_POST['pickup'] : 0);
            $vendor = (isset($_POST['vendor']) ? (int)$_POST['vendor'] : 0);
            $description = (isset($_POST['description']) ? (int)$_POST['description'] : 0);
            $sales_notes = (isset($_POST['sales_notes']) ? $_POST['sales_notes'] : 0);
            $country_of_origin = (isset($_POST['country_of_origin']) ? (int)$_POST['country_of_origin'] : 0);

            if (!get_magic_quotes_gpc()) {
                $name = DB::mysql_secure_string($name);
                $file_name = DB::mysql_secure_string($file_name);
                $utm_source = DB::mysql_secure_string($utm_source);
                $utm_medium = DB::mysql_secure_string($utm_medium);
                $utm_campaign = DB::mysql_secure_string($utm_campaign);
                $catalog = DB::mysql_secure_string($catalog);
                $shop_name = DB::mysql_secure_string($shop_name);
                $company = DB::mysql_secure_string($company);
                $phone = DB::mysql_secure_string($phone);
                $currency = DB::mysql_secure_string($currency);
                $sales_notes = DB::mysql_secure_string($sales_notes);
            }

            $data = array('name' => $name, 'equip' => $equip, 'file_name' => $file_name, 'utm_source' => $utm_source, 'utm_medium' => $utm_medium, 'utm_campaign' => $utm_campaign, 'catalog' => $catalog, 'shop_name' => $shop_name, 'company' => $company, 'phone' => $phone, 'shop_url' => $shop_url, 'currency' => $currency, 'delivery' => $delivery,
                'local_delivery_cost' => $local_delivery_cost, 'categories' => $categories, 'url' => $url, 'price' => $price, 'picture' => $picture, 'store' => $store, 'pickup' => $pickup, 'vendor' => $vendor, 'description' => $description, 'sales_notes' => $sales_notes, 'country_of_origin' => $country_of_origin);

            $check = Partner::check_file_name($file_name);

            if ($check['kol'] < 1) {
                if (Partner::create_file($data))
                    Partner::add_partner($data);
            }

            Template::set_page('partner', 'Партнерские программы', Partner::get_list());
        } elseif (isset($_POST['save']) && empty($_POST['name'])) {

            Template::set_page('partner', 'Партнерские программы', Partner::get_list());
        } // Выводим форму для добавления партнерской программы.
        else {
            Template::set('content', []);
            Template::set_page('edit_partner', 'Новая партнерская программа');
        }
    }


    /**
     * Форма редактирование партнерских программ.
     * @param array $args Параметры URL.
     */
    function edit($args)
    {

        if (isset($args[0]))
            $id = (int)$args[0];

        //Выводим статью для редактирования
        if (!empty($id)) {
            ;
            Template::set_page('edit_partner', 'Партнерские программы. Редактирование.', Partner::get_partner($id));
        } // Сохраняем изменения
        elseif ((isset($_POST['save'])) && (!empty($_POST['name'])) && (!empty($_POST['file_name']))) {

            $id = (int)$_POST['id'];

            $name = $_POST['name'];
            $file_name = $_POST['file_name'];
            $equip = (isset($_POST['equip']) ? $_POST['equip'] : 0);
            $utm_source = (isset($_POST['utm_source']) ? $_POST['utm_source'] : '');
            $utm_medium = (isset($_POST['utm_medium']) ? $_POST['utm_medium'] : '');
            $utm_campaign = (isset($_POST['utm_campaign']) ? $_POST['utm_campaign'] : '');
            $catalog = (isset($_POST['catalog']) ? $_POST['catalog'] : '');
            $shop_name = (isset($_POST['shop_name']) ? $_POST['shop_name'] : '');
            $company = (isset($_POST['company']) ? $_POST['company'] : '');
            $phone = (isset($_POST['phone']) ? $_POST['phone'] : '');
            $shop_url = (isset($_POST['shop_url']) ? $_POST['shop_url'] : '');
            $currency = (isset($_POST['currency']) ? $_POST['currency'] : '');
            $delivery = (isset($_POST['delivery']) ? $_POST['delivery'] : 0);
            $local_delivery_cost = (isset($_POST['local_delivery_cost']) ? (int)$_POST['local_delivery_cost'] : 0);


            $categories = (isset($_POST['categories']) ? (int)$_POST['categories'] : 0);
            $url = (isset($_POST['url']) ? (int)$_POST['url'] : 0);
            $price = (isset($_POST['price']) ? (int)$_POST['price'] : 0);
            $picture = (isset($_POST['picture']) ? (int)$_POST['picture'] : 0);
            $store = (isset($_POST['store']) ? (int)$_POST['store'] : 0);
            $pickup = (isset($_POST['pickup']) ? (int)$_POST['pickup'] : 0);
            $vendor = (isset($_POST['vendor']) ? (int)$_POST['vendor'] : 0);
            $description = (isset($_POST['description']) ? (int)$_POST['description'] : 0);
            $sales_notes = (isset($_POST['sales_notes']) ? $_POST['sales_notes'] : 0);
            $country_of_origin = (isset($_POST['country_of_origin']) ? (int)$_POST['country_of_origin'] : 0);

            if (!get_magic_quotes_gpc()) {
                $name = DB::mysql_secure_string($name);
                $file_name = DB::mysql_secure_string($file_name);
                $utm_source = DB::mysql_secure_string($utm_source);
                $utm_medium = DB::mysql_secure_string($utm_medium);
                $utm_campaign = DB::mysql_secure_string($utm_campaign);
                $catalog = DB::mysql_secure_string($catalog);
                $shop_name = DB::mysql_secure_string($shop_name);
                $company = DB::mysql_secure_string($company);
                $phone = DB::mysql_secure_string($phone);
                $currency = DB::mysql_secure_string($currency);
                $sales_notes = DB::mysql_secure_string($sales_notes);
            }

            $data = array('id' => $id, 'name' => $name, 'equip' => $equip, 'file_name' => $file_name, 'utm_source' => $utm_source, 'utm_medium' => $utm_medium, 'utm_campaign' => $utm_campaign, 'catalog' => $catalog, 'shop_name' => $shop_name, 'company' => $company, 'phone' => $phone, 'shop_url' => $shop_url, 'currency' => $currency, 'delivery' => $delivery,
                'local_delivery_cost' => $local_delivery_cost, 'categories' => $categories, 'url' => $url, 'price' => $price, 'picture' => $picture, 'store' => $store, 'pickup' => $pickup, 'vendor' => $vendor, 'description' => $description, 'sales_notes' => $sales_notes, 'country_of_origin' => $country_of_origin);

            $check = Partner::check_file_name($file_name);

            if (($check['kol'] < 1) || (($check['id'] == $id) && ($check['kol'] == 1))) {
                if (Partner::create_file($data))
                    Partner::set_partner($id, $data);

            }
            redirect('/admin/partner/');
        } // Выводим список страниц
        else {
            redirect('/admin/partner/');
        }
    }


    /**
     * Скрывает/отображает партнерскую программу.
     * @param array $args Параметры URL.
     */
    function show($args)
    {

        if (isset($args[0]))
            $id = (int)$args[0];

        if (!empty($id)) {
            Partner::change_active($id);
        }

        redirect('/admin/partner/');
    }
}
