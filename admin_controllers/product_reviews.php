<?php

class Controller_Product_Reviews extends Controller_Base
{
    /**
     * Reviews list page
     *
     * @param array $arguments URL arguments
     */
    public function index($arguments)
    {
        if ($arguments) {
            $this->_review($arguments[0]);

            return;
        }

        $search = array();

        $search['read'] = isset($_GET['read']) && $_GET['read'] !== '' ? !empty($_GET['read']) : null;
        $search['visible'] = isset($_GET['visible']) && $_GET['visible'] !== '' ? !empty($_GET['visible']) : null;
        $search['negative'] = isset($_GET['negative']) && $_GET['negative'] !== '' ? !empty($_GET['negative']) : null;

        $onPage = 20;

        $count = prod_review::count($search['read'], $search['visible'], $search['negative']);

        $pagination = $this->pagination($count, $onPage, $search);

        $urlParams = [];

        if ($search['read'] !== null) {
            $urlParams[] = 'read=' . ($search['read'] ? 1: 0);
        }

        if ($search['visible'] !== null) {
            $urlParams[] = 'visible=' . ($search['visible'] ? 1: 0);
        }

        if ($search['negative'] !== null) {
            $urlParams[] = 'negative=' . ($search['negative'] ? 1: 0);
        }

        if ($pagination['showAll']) {
            $urlParams[] = 'showAll';
        } elseif ($pagination['page'] > 1) {
            $urlParams[] = 'p=' . $pagination['page'];
        }

        Template::set_page('product_reviews', 'Отзывы о товарах', array(
            'back_url_param' => $urlParams ? '?bu=' . rawurlencode(implode('&', $urlParams)) : '',
            'search' => $search,
            'reviews' => prod_review::search($search['read'], $search['visible'], $search['negative'], $pagination['page'], $onPage),
        ));
    }

    /**
     * Toggles review's negative flag
     *
     * @param $args
     */
    public function toggle_negative($args)
    {
        if (
            empty($args[0])
            || !prod_review::toggle_negative($args[0])
        ) {
            $this->nopage();
        } else {
            redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/admin/product_reviews');
        }
    }

    /**
     * One question page
     *
     * @param int $reviewID Review ID
     */
    private function _review($reviewID)
    {
        if (
            intval($reviewID) != $reviewID || $reviewID < 1
            || !($review = prod_review::one($reviewID))
        ) {
            Template::set_page('404', 'Страница не найдена');
        }

        if (!$review['read']) {
            DB::query('UPDATE rk_prod_review SET `read` = 1 WHERE id = ' . intval($reviewID));
        }

        $saveError = false;

        if (!empty($_POST['submitSave'])) {
            $review['name'] = isset($_POST['name']) ? h(trim($_POST['name'])) : '';
            $review['city'] = isset($_POST['city']) ? h(trim($_POST['city'])) : '';
            $review['date'] = isset($_POST['date']) ? h(trim($_POST['date'])) : '';
            $review['comment'] = isset($_POST['comment']) ? h(trim($_POST['comment'])) : '';

            if (
                mb_strlen(h_d($review['name'])) < 2
                || mb_strlen(h_d($review['city'])) < 2
                || !preg_match('/^\d{4}-(?:0\d|1[0-2])-(?:[0-2]\d|3[01])$/', $review['date'])
                || mb_strlen(h_d($review['comment'])) < 2
            ) {
                $saveError = true;
            }

            $review['visible'] = !empty($_POST['visible']);
            $review['negative'] = !empty($_POST['negative']);

            if (!$saveError) {
                prod_review::update($review['id'], $review['name'], $review['city'], $review['date'], $review['comment'], $review['visible'], $review['negative']);

                redirect($_SERVER['REQUEST_URI']);
            }
        }

        Template::add_css('flick/jquery-ui.min.css');

        Template::add_script('jquery-ui.min.js');
        Template::add_script('admin/product_review.js');

        Template::set_page('product_review', 'Отзыв к товару', array(
            'back_url_param' => isset($_GET['bu']) ? '?' . $_GET['bu'] : '',
            'saveError' => $saveError,
            'review' => $review,
        ));
    }
  
    /**
     * @inheritdoc
     */
    protected function admin_access()
    {
        return admin::RIGHTS_COMMON | admin::RIGHTS_STATISTICS;
    }
}
