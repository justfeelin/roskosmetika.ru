<?php

/**
 * Контроллер работы с отзывами.
 */
class Controller_Review extends Controller_base
{
    //Заголовок страницы
    const PAGE_HEADER = 'Отзывы';

    function index($args)
    {
        $count = (int)DB::get_field('cnt', 'SELECT COUNT(r.id) AS cnt FROM rk_review AS r');

        $pagination = Pagination::setValues($count, review::STEP);

        Pagination::setBaseUrl('/admin/review');

        Pagination::setTemplateSettings();

        $all_comments_list = review::get_list(false, $pagination['page']);
        Template::set('all_comments', $all_comments_list);
        Template::set('bringing_date', 'd.m.Y');
        Template::set_page('review', self::PAGE_HEADER);
    }

    /**
     * Форма редактирования
     * @param array $args Параметры URL.
     */
    function edit($args)
    {

        // вывод формы редактирования
        if (isset($args[0])) {
            $id = (int)$args[0];

            Template::set_form(review::get_comment($id));
            Template::set_page('review_edit', 'Редактирование отзывов');
        }


        // Сохрянение изменения
        if (isset($_POST['save'])) {
            $comment = $_POST['comment'];
            $id = (int)$_POST['id'];

            review::edit($id, $comment);
            redirect('/admin/review');
        }
    }


    /**
     * Форма редактирования города
     * @param array $args Параметры URL.
     */
    function edit_city($args)
    {

        // вывод формы редактирования
        if (isset($args[0])) {
            $id = (int)$args[0];

            Template::set('city', review::get_comment($id));
            Template::set_page('review_edit', 'Редактирование отзывов');
        }


        // Сохрянение изменения
        if (isset($_POST['save'])) {
            $city = $_POST['city'];
            $id = (int)$_POST['id'];

            review::edit_city($id, $city);
            redirect('/admin/review');
        }
    }

    /**
     * Скрывать / отображать комментарий
     * @param array $args Параметры URL.
     */
    function change_visible($args)
    {
        $id = (int)$args[0];

        review::change_visible($id);
        $this->go_back();
    }


    /**
     * Удалить комментарий
     * @param array $args Параметры URL.
     */
    function delete_comment($args)
    {
        $id = (int)$args[0];

        review::delete($id);
        $this->go_back();
    }


    /**
     * Добавление и редактирование ответа
     */
    function edit_answer($args)
    {
        // вывод формы редактирования
        if (isset($args[0])) {
            $id = (int)$args[0];
            Template::set_page('review_edit', 'Редактирование отзывов', review::get_comment($id));
        }

        // Сохрянение изменения
        if (isset($_POST['save'])) {
            $answer = $_POST['answer'];
            $id = (int)$_POST['id'];
            $comment_before = review::get_comment($id);
            $email = DEBUG ? env('ADMIN_EMAIL') : users::get_email($comment_before['client_id']);
            review::edit_answer($id, $answer);
            $comment_after = review::get_comment($id);
            // отправляем письмо, если ответ пишется впервые

            if (isset($email) && empty($comment_before['answer']) && !empty($comment_after['answer']) && $comment_after['visible']) {


                $from = array('sales@roskosmetika.ru' => 'Роскосметика');
                $theme = 'Отзыв Роскосметика';
                $review_info = array('comment' => $comment_before['comment'],
                    'answer' => $comment_after['answer'],
                    'time_message' => $comment_before['time_message']);

                $message_user = Template::mail_template('mail_review_answer', $theme, $review_info);

                @mailer::mail($theme, $from, $email, $message_user);


            }


            redirect('/admin/review');
        }


    }

    /**
     * @inheritdoc
     */
    protected function admin_access()
    {
        return admin::RIGHTS_COMMON | admin::RIGHTS_STATISTICS;
    }

}

?>
