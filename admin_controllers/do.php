<?php

/**
 * optiscript
 */
class Controller_do extends Controller_Base
{

    function index($args)
    {

/**
 * events counter
 * @var integer
 */
$counter = 0;

/**
 * Days for start event
 * @var integer
 */
$promo_days = 3;


/**
 * Recomended products
 * @var integer
 */
$r_limit = 6;

// show old prices in cart
Cart::$currentPrices = false;


//  lost carts promo end date 3 day 
$query = "SELECT ro.id AS order_id, ro.start_date,
                 IF (c.name = '' OR c.name IS NULL, rc.name, c.name) AS name,
                 c.email, rc.email AS c_email, c.phone, rpc.end_date, rpc.discount, rpc.code
          FROM rk_orders ro LEFT JOIN rk_promo_codes  rpc ON  ro.promo_code_id = rpc.id
                            LEFT JOIN clients AS c ON c.id = ro.client_id
                            LEFT JOIN rk_credentials AS rc ON rc.order_id = ro.id 
          WHERE ro.order_number = 0
          AND ro.promo_code_id <> 0
          AND rpc.end_date IS NOT NULL AND rpc.end_date >= NOW()
          AND rpc.end_date BETWEEN (NOW() + INTERVAL " . ($promo_days-1) . " DAY)   AND (NOW() + INTERVAL " . $promo_days . " DAY)
          AND ro.promo_end_1 = 0";

db::get_rows($query, function($order) use (&$counter, &$r_limit){

    //$email = $order['email'] && check::email($order['email']) ? $order['email'] : ($order['c_email'] && check::email($order['c_email']) ? $order['c_email'] : null);
    $email = 'demtsov@gmail.com';

    if($email) {

        $return = FALSE;

        Cart::$currentPrices = false;
        Cart::orderID($order['order_id']);
        $c_order = Cart::setInfo(false);

        $return['email'] = $email;
        $return['phone'] = $order['phone'];
        $return['action_date'] = $order['start_date'];
        $return['client_name'] = $order['name'];
        $return['client_name'] = $order['name'];
        $return['promo_code'] = $order['code'];
        $return['promo_code_discount'] = $order['discount'];
        $return['promo_code_end'] = $order['end_date'];
        $return['old_cost'] = $c_order['info']['raw_sum'];
        $return['discount_cost'] = $c_order['info']['promo_sum'];



        for ($i=0; $i < count($c_order['products']); $i++) { 
            
            $return['products'][$i]['id'] = $c_order['products'][$i]['id'];
            $return['products'][$i]['name'] = helpers::str_prepare($c_order['products'][$i]['name'] . ' ' . $c_order['products'][$i]['pack'] . ', ' . $c_order['products'][$i]['brand']);
            $return['products'][$i]['quantity'] = $c_order['products'][$i]['quantity'];
            $return['products'][$i]['price'] = $c_order['products'][$i]['price'];
            $return['products'][$i]['old_price'] = $c_order['products'][$i]['oldPrice'];
            $return['products'][$i]['url'] = $c_order['products'][$i]['url'];
            $return['products'][$i]['img'] = db::get_field('img', "SELECT IF(rpp.pic_quant > 0, 1, 0) AS img FROM rk_prod_photo AS rpp WHERE rpp.id = {$c_order['products'][$i]['id']}");
        }


        $recomended = product::cart_neighbours(array_column($c_order['products'], 'id'), FALSE, $r_limit);

        for ($i=0; $i < count($recomended); $i++) { 

          $return['recomended'][$i]['id'] = $recomended[$i]['id'];
          $return['recomended'][$i]['name'] = helpers::str_prepare($recomended[$i]['name'] . ' ' . $recomended[$i]['packs'][0]['pack'] . ', '. $recomended[$i]['tm']);
          $return['recomended'][$i]['price'] = $recomended[$i]['packs'][0]['special_price'] > 0 ?  (int)$recomended[$i]['packs'][0]['special_price'] : (int)$recomended[$i]['packs'][0]['price'];
          $return['recomended'][$i]['old_price'] = $recomended[$i]['packs'][0]['special_price'] > 0 ?  (int)$recomended[$i]['packs'][0]['price'] : 0;
          $return['recomended'][$i]['url'] = $recomended[$i]['url'];
          $return['recomended'][$i]['img'] = $recomended[$i]['packs'][0]['pic_quant'] > 0 ?  1 : 0;
          
        } 


        $result = sendpulse::promo_end_sale($return);

        if ($result['result']) {
            //send_plus($order['order_id'], 'lost_cart_1');
            $counter++;

        }


    }

});

echo $counter; exit;
        // $sendpulse_order_items = array();

        // $order_items = array(
        //   array('order_id' => '278151','site_order_number' => '9712218','payment_id' => NULL,'id' => '25557','name' => 'Наклейка под глаза для наращивания ресниц','pack' => '10&nbsp;пар','old_price' => '22.00','price' => '20.00','url' => 'naklejka-pod-glaza-dlja-narashhivanija-resnic-chistove','src' => '25557.jpg','quantity' => '2','sum' => '40.00'),
        //   array('order_id' => '278151','site_order_number' => '9712218','payment_id' => NULL,'id' => '25755','name' => 'Носки для парафинотерапии стандарт','pack' => '1&nbsp;пара','old_price' => '40.00','price' => '36.00','url' => 'noski-dlja-parafinoterapii-standart-chistove','src' => '25755.jpg','quantity' => '1','sum' => '36.00'),
        //   array('order_id' => '278151','site_order_number' => '9712218','payment_id' => NULL,'id' => '25653','name' => 'Одноразовая мочалка','pack' => '1&nbsp;шт.','old_price' => '11.00','price' => '10.00','url' => 'odnorazovaja-mochalka-chistove','src' => '25653.jpg','quantity' => '1','sum' => '10.00'),
        //   array('order_id' => '278151','site_order_number' => '9712218','payment_id' => NULL,'id' => '892','name' => 'Одноразовый трехслойный коврик для ног','pack' => '1&nbsp;шт.','old_price' => '26.00','price' => '24.00','url' => 'odnorazovyj-trexslojnyj-kovrik-dlya-nog','src' => '892.jpg','quantity' => '3','sum' => '72.00'),
        //   array('order_id' => '278151','site_order_number' => '9712218','payment_id' => NULL,'id' => '25694','name' => 'Разделители для пальцев, голубой','pack' => '1&nbsp;пара','old_price' => '14.00','price' => '13.00','url' => 'razdeliteli-dlja-palcev-goluboj-chistove','src' => '25694.jpg','quantity' => '1','sum' => '13.00')
        // );

        // for ($i = 0, $n = count($order_items); $i < $n; ++$i) 
        // {
        //     $name = htmlspecialchars($order_items[$i]['name']);

        //     $sendpulse_order_items['product_' . ($i+1)] = array( 'id'          => $order_items[$i]['id'],
        //                                                          'name'        => helpers::str_prepare($name . ' ' . $order_items[$i]['pack']),
        //                                                          'quantity'    => $order_items[$i]['quantity'],
        //                                                          'final_price' => (int)$order_items[$i]['price']
        //                                                         );        
        // }


        // $s = sendpulse::event_order(278151, $sendpulse_order_items, 3);
        // var_dump($s);
        // exit;



        $s = array('rk_order'   => 220937,
                   'email'      => 'demtsov@gmail.com',
                   'phone'      => '+79312808030',
                   'name'       => 'Дмитрий',
                    // 'email'      => 'lukashinsa@gmail.com',
                    // 'phone'      => '+79096507276',
                    // 'name'       => 'Сергей',
                   'start_date' => '2019-09-10 16:35:12');

        $b = sendpulse::event_lost_cart($s);
        vd($b);
        exit;

        $data = array("email" => "lukashinsa@gmail.com",
                      "phone" => "",
                      "product_name" => "Гель для бритья Kondor, 100&nbsp;мл",
                      "product_id" => 35736,
                      "product_price" => "245",
                      "event_date" => "2019-08-30",
                      "product_old_price" => "0",
                      "img" =>  0,
                      "recomended_1" => array("product_id" => 49423,
                                              "product_name" => "Крем «PINK GRAPEFRUIT» увлажняющий лифтинговый для тела ARAVIA ORGANIC, 300&nbsp;мл",
                                              "product_price" => "540",
                                              "product_old_price" => "0",
                                              "img" => 0)

                      );

        $data = json_encode($data);

        $curl = curl_init();


        curl_setopt_array($curl, [
            CURLOPT_URL => 'https://events.sendpulse.com/events/id/ce938ab2101e4ed38e43d194cdf52b2a/7198326',
            CURLOPT_POST => true,
            CURLOPT_HTTPHEADER => ['Content-Type: application/json',
                                   'Accept: application/json'],
            CURLOPT_POSTFIELDS => $data,                        
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_SSL_VERIFYPEER => FALSE,
            CURLOPT_SSL_VERIFYHOST => FALSE,
            CURLOPT_TIMEOUT => 60,
            CURLOPT_FAILONERROR => FALSE,
        ]);

        $result = curl_exec($curl);

        $result = json_decode($result, true);   

        if ($curl)
        {
            curl_close($curl);
        }

        vd($result);
        exit;



        // $post = FALSE;



        // if ($data) 
        // {
        //     $post = TRUE;

        //     $data = ($json ? json_encode($data) : http_build_query($data));
            
        //     curl_setopt($curl, CURLOPT_POSTFIELDS, $data);   
        // }

        // curl_setopt_array($curl, [
        //     CURLOPT_URL => $url,
        //     CURLOPT_POST => $post,
        //     CURLOPT_HTTPHEADER =>  $headers,
        //     CURLOPT_RETURNTRANSFER => TRUE,
        //     CURLOPT_SSL_VERIFYPEER => FALSE,
        //     CURLOPT_SSL_VERIFYHOST => false,
        //     CURLOPT_CUSTOMREQUEST => $method,
        //     CURLOPT_HEADER => FALSE,
        //     CURLOPT_TIMEOUT => 60,
        //     CURLOPT_FAILONERROR => FALSE,
        //     CURLOPT_FRESH_CONNECT => true,
        // ]);

        // $result = curl_exec($curl);

        // $result = json_decode($result, true);   

        // if ($curl)
        // {
        //     curl_close($curl);
        // }

        // return $result;






        optimization::article_cat();

        echo 'ok';exit;

        $q = '{"products" : [{"id":"456", "price":"832"}],"currency_code" : "RUR"}';

        vd(json_decode($q)); exit;


        // yamarket::set_yandexm_products();
        // vd(yamarket::check_id(21));
        // vd(yamarket::check_id(50462));
        // vd(yamarket::check_id(182));
        // vd(yamarket::check_id(42065));
       



        // DB::query('UPDATE products SET available = 1 WHERE id = 21');
        // vd(DB::last_query_info());

        // DB::query('UPDATE products SET available = 0 WHERE id = 21');
        // vd(DB::last_query_info());

        // exit;


        // $opts = array('http'=>array('method'=>"GET",
        //                             'header'=>'Authorization: OAuth oauth_token=AQAAAAAg4gd2AAWbRynSN_3TWEKTvP6HShwywxc, oauth_client_id=c784ece0618046b98b961f1e0bf3034b'));

        // $context = stream_context_create($opts);

        // $file = file_get_contents('https://api.partner.market.yandex.ru/v2/campaigns.json', false, $context);

        // vd($file);

        // exit;


        $token = Yamarket::get_token();

        if($token)
        {
            
            $headers = [
                            'Authorization: OAuth oauth_token=' . $token . ', oauth_client_id=' . env('YA_M_ID'),
                        ];


           // получение id фида             
           $result = yamarket::ya_request('https://api.partner.market.yandex.ru/v2/campaigns/' . env('YA_M_CID') . '/feeds.json', $headers, FALSE);

           $feed_id = $result['feeds'][0]['id'] ?? FALSE; 

            //Установка цен    
            

            $headers = [
                            'Authorization: OAuth oauth_token=' . $token . ', oauth_client_id=' . env('YA_M_ID'),
                            'Content-Type: application/json',
                        ];

           // $data = array('offers' => array(array('feed' => array('id' => $feed_id),
           //                                  'id' => 21,
           //                                  'price' => array('currencyId' => "RUR",
           //                                                   'value' => 1200.00,))));
                                 
           $data = json_decode($q);
           //$data =array();


            $result = yamarket::ya_request('https://api.partner.market.yandex.ru/v2/campaigns/' . env('YA_M_CID') . '/offer-prices/updates.json', $headers, $data, 'POST', TRUE);

            vd($result);
            exit;




            // удаление всех цен по API
            $headers = [
                            'Authorization: OAuth oauth_token=' . $token . ', oauth_client_id=' . env('YA_M_ID'),
                            'Content-Type: application/json',
                        ];

            $data =  [
                        'removeAll' =>  true,
                    ];
                                 


            $result = yamarket::ya_request('https://api.partner.market.yandex.ru/v2/campaigns/' . env('YA_M_CID') . '/offer-prices/removals.json', $headers, $data, 'POST', TRUE);

            vd($result);
            exit;



            $headers = [
                            'Authorization: OAuth oauth_token=' . $token . ', oauth_client_id=' . env('YA_M_ID'),
                        ];


           // получение id фида             
           $result = yamarket::ya_request('https://api.partner.market.yandex.ru/v2/campaigns/' . env('YA_M_CID') . '/feeds.json', $headers, FALSE);

           $feed_id = $result['feeds'][0]['id'] ?? FALSE; 

           // прайс обновлен
           $result = yamarket::ya_request('https://api.partner.market.yandex.ru/v2/campaigns/' . env('YA_M_CID') . '/feeds/' . $feed_id . '/refresh.json', $headers, FALSE, 'POST');




           echo "\n---\n";


                $params =  array(
                    'regionId' => 213,

                            );

//https://api.partner.market.yandex.ru/v2/models.json?query=iPhone+4s&regionId=2
           $result = yamarket::ya_request('https://api.partner.market.yandex.ru/v2/campaigns/21430571/offers.json', $headers, FALSE, 'GET');
           //$result = yamarket::ya_request('https://api.partner.market.yandex.ru/v2/regions.json?name=Ивановка', $headers, FALSE, 'GET');

           vd($result);

           
           exit;
 
        } else {
            echo 'Отсутствует токен, для генерации токена прейдите по <a href="https://www.roskosmetika.ru//admin/yamarket">ссылке</a> с интерфейс администратора Роскосметика.';
        }




  exit;

$pic_quant = 4;

           if ($pic_quant > 0) {
                for ($p=1; $p <= $pic_quant; $p++) { 
                    if($p === 1)
                    {
                        vd('первая фотка');
                    } else
                    {
                        vd('фоток больше одной');
                    }
                }
vd('-после цикла-');
            } else {
                 vd('Обычная одна фотка');
            }

exit;

vd(implode(",", $q));exit;
    switch ((int)date('w')) {
        case 0:
            $day = '6';

            break;

        case 5:
            $day = '6';

            break;

        default:
            $day = '5';

            break;
    }


    vd($day);
exit;

        $items = DB::get_rows('SELECT * FROM orders_items WHERE order_number = 278123');

        $a = admitad::ok_page(278123, $items, 2300, '6588c4728d9a206e5c266304e5aabe6c');

       Template::add_admitad_code($a['admitad_code']);   

       

        $b = admitad::ok_page_new(278123, $items, '6588c4728d9a206e5c266304e5aabe6c');
        
        Template::add_admitad_code($b);  



        vd(Template::$_admitad_code);

        exit;



DB::get_rows('SELECT cc.id, cc.name FROM cdb_country cc WHERE 1', 
             function($country) {
                DB::query("INSERT INTO rk_country (country_id, url) VALUES ({$country['id']}, '" . helpers::translit($country['name']) . "')");
             });

exit;

// hits to avito
$return = FALSE;

$query = "SELECT p.id, p.main_id, p.pack,
                 CONCAT(tm.name, '. ' ,IF(p.main_id > 0,(SELECT pp.name FROM products pp WHERE pp.id = p.main_id), p.name), ', ',  p.pack, '.') AS name, 
                 CONCAT(p.description, 'АКТИВНЫЕ КОМПОНЕНТЫ:', rpd.ingredients, 'СПОСОБ ПРИМЕНЕНИЯ:', rpd.use) AS full_descr, cpb.sold, CEIL(p.price) AS price
          FROM tm, cdb_product_ballance cpb, products p LEFT JOIN rk_prod_desc rpd ON rpd.id = p.id
          WHERE p.id = cpb.product_id
          AND p.tm_id = tm.id
          AND p.active = 1
          AND p.visible = 1
          AND p.available = 1
          AND (100 - CEIL((p.purchase_price/p.price) * 100)) > 30
          ORDER BY cpb.sold DESC
          LIMIT 80, 40";

$products = DB::get_rows($query);

$promo = 'СКИДКА 15% По промо коду "avito2018" ТОЛЬКО ДЛЯ ПОСЕТИТЕЛЕЙ АВИТО!';

for ($i=0; $i < count($products); $i++) { 
    
    $name = strip_tags($products[$i]['name']);
    $full_descr = strip_tags($products[$i]['full_descr']);

    $name = preg_replace(array('/&laquo;/', '/&raquo;/', '/&nbsp;/', '/&lt;p&gt;/', "/&lt;\/p&gt;/", '/&/', '/"/', '/>/', '/</', '/\'/'),
                                                              array('&quot;', '&quot;',' ', '', '', '&amp;', '&quot;', '&gt;', '&lt;', '&apos;'), $name);

    $full_descr = preg_replace(array('/&laquo;/', '/&raquo;/', '/&nbsp;/', '/&lt;p&gt;/', "/&lt;\/p&gt;/", '/&/', '/"/', '/>/', '/</', '/\'/'),
                                                              array('&quot;', '&quot;',' ', '', '', '&amp;', '&quot;', '&gt;', '&lt;', '&apos;'), $full_descr);
    $name = trim($name);
    $full_descr = trim($full_descr);

    $pack = $products[$i]['pack'] . ' - ' . $products[$i]['price'] . ' руб.';


    $packs = DB::get_rows("SELECT p.pack, CEIL(p.price) AS price FROM products p WHERE p.main_id = {$products[$i]['id']}");

    if(count($packs) > 0) {
        for ($j=0; $j < count($packs); $j++) { 
            $pack .=  "\n" . $packs[$j]['pack'] . ' - ' . $packs[$j]['price'] . ' руб.';
        }
    }

    $pack = strip_tags($pack);
    $pack = preg_replace(array('/&laquo;/', '/&raquo;/', '/&nbsp;/', '/&lt;p&gt;/', "/&lt;\/p&gt;/", '/&/', '/"/', '/>/', '/</', '/\'/'),
                                                              array('&quot;', '&quot;',' ', '', '', '&amp;', '&quot;', '&gt;', '&lt;', '&apos;'), $pack);

    $pack = trim($pack);

    $result_string = $name . "\n" . $pack . "\n\n" . $promo . "\n\n" . $full_descr;

    $return[] = array('id' => $products[$i]['id'],
                      'main_id' => $products[$i]['main_id'],
                      'text' => $result_string,
                      'price' => $products[$i]['price']);
}


        $excel = export::create_new_file();

        $sheet = $excel->getSheet(0);


        $sheet->setCellValue('A' . (1), 'ID');
        $sheet->setCellValue('B' . (1), 'Main ID');
        $sheet->setCellValue('C' . (1), 'Текст');
        $sheet->setCellValue('D' . (1), 'Цена');



        for ($i=0; $i < count($return); $i++) {

            $sheet->setCellValue('A' . ($i + 2), $return[$i]['id']);
            $sheet->setCellValue('B' . ($i + 2), $return[$i]['main_id']);
            $sheet->setCellValue('C' . ($i + 2), $return[$i]['text']);
            $sheet->setCellValue('D' . ($i + 2), $return[$i]['price']);
        }


        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename=' . 'avito-yula-end.xls');

        $writer = PHPExcel_IOFactory::createWriter($excel,'Excel5');
        $writer->save('php://output');

        exit;



//Product::cart_neighbours(Product::get_popular(FALSE, 10), false, 4)

        vd(geoip_record_by_name('62.76.168.7'));
        vd('62.76.168.7');

        exit;

        // export sale and hurry in xls

        $return = FALSE;

        $query = "SELECT  p.id, p.main_id AS descr_id, p.name, IF( p.url =  '', p.id, p.url ) url, 
                          CEIL(p.price) AS real_price, CEIL(p.special_price) AS real_special_price,
                          p.available, p.active,  p.pack, p._discount AS discount, p.main_stock,
                        tm.name tm, 
                        pc1.cat_id AS cat_lvl_1,
                        p.main_stock, IF(p.main_stock > 0, cpb.in_stock - cpb.hold, IF(cpb.in_stock > 0, cpb.in_stock - cpb.hold, 15)) AS stock, cpb.in_stock, cpb.hold, cp.supplier_name, cp.supplier_code, cs.name AS supplier, pd.discount_end, pd.discount_rate, p._discount
                FROM  tm, products p, cdb_product_ballance cpb, rk_prod_cat_lvl_1 pc1, rk_categories rc, cdb_products cp LEFT JOIN cdb_suppliers cs ON cp.supplier_id = cs.id
                                                                                                                         LEFT JOIN prod_discounts  pd ON cp.id = pd.product_id
                WHERE p.id = cp.id
                AND p.id = cpb.product_id 
                AND p.tm_id = tm.id
                AND pc1.cat_id = rc.id 
                AND rc.is_tag = 0
                AND pc1.prod_id = p.id
                AND p.main_id = 0
                AND p.visible = 1
                AND p.id NOT IN (12524, 12525, 12526, 22222, 5555)
                AND p.for_proff = 0 
                AND p.active = 1 
                AND p.available = 1
                AND p.special_price > 0
                AND p._discount >= 10
                AND IF(p.main_stock > 0, cpb.in_stock - cpb.hold, IF(cpb.in_stock > 0, cpb.in_stock, 15)) > 0
                GROUP BY pc1.prod_id ORDER BY pc1.prod_id";

        $products = db::get_rows($query);

        for ($i=0; $i < count($products); $i++) {

            $products[$i]['cat_lvl_2'] = db::get_field('cat_id', "SELECT pc2.cat_id FROM  rk_prod_cat_lvl_2 pc2, rk_categories rc WHERE pc2.cat_id = rc.id AND rc.is_tag = 0 AND pc2.prod_id = {$products[$i]['id']} ORDER BY pc2.cat_id LIMIT 1");
            $products[$i]['cat_lvl_3'] = db::get_field('cat_id', "SELECT pc3.cat_id FROM  rk_prod_cat pc3, rk_categories rc WHERE pc3.cat_id = rc.id AND rc.is_tag = 0 AND pc3.prod_id = {$products[$i]['id']} ORDER BY pc3.cat_id LIMIT 1");

            $products[$i]['for_legs'] = (int)db::get_field("for_legs", "SELECT IF( cs.subcat_id = 71 , 1, 0 ) for_legs FROM rk_cat_subcat cs WHERE cs.cat_id = {$products[$i]['cat_lvl_2']}");
            $products[$i]['group_id'] = 0;

            $yandex_info = DB::get_row("SELECT cat_id, parent_id, name, market_category FROM __yandex_catalog WHERE cat_id = {$products[$i]['cat_lvl_3']} AND `use` = 1");

            if ($yandex_info) {

                $products[$i]['market_category'] = $yandex_info['market_category'];
                $products[$i]['ya_id'] = $yandex_info['cat_id'];
                $products[$i]['ya_parent_id'] = $yandex_info['parent_id'];

            } else {

                $yandex_info = DB::get_row("SELECT cat_id, parent_id, name, market_category FROM __yandex_catalog WHERE cat_id = {$products[$i]['cat_lvl_2']} AND `use` = 1");

                if ($yandex_info) {

                    $products[$i]['market_category'] = $yandex_info['market_category'];
                    $products[$i]['ya_id'] = $yandex_info['cat_id'];
                    $products[$i]['ya_parent_id'] = $yandex_info['parent_id'];

                } else {

                    $yandex_info = DB::get_row("SELECT cat_id, parent_id, name, market_category FROM __yandex_catalog WHERE cat_id = {$products[$i]['cat_lvl_1']} AND `use` = 1");

                    if ($yandex_info) {

                        $products[$i]['market_category'] = $yandex_info['market_category'];
                        $products[$i]['ya_id'] = $yandex_info['cat_id'];
                        $products[$i]['ya_parent_id'] = $yandex_info['parent_id'];

                    } else {

                        $products[$i]['market_category'] = 'Все товары/Красота и здоровье';
                        $products[$i]['ya_id'] = 201;
                        $products[$i]['ya_parent_id'] = 200;
                    }

                }

            }



            $query =  "SELECT  p.id, p.main_id AS descr_id, p.name, IF( p.url =  '', p.id, p.url ) url, 
                      CEIL(p.price) AS real_price, CEIL(p.special_price) AS real_special_price,
                      p.available, p.active,  p.pack, p._discount AS discount, p.main_stock,
                    tm.name tm, 
                    pc1.cat_id AS cat_lvl_1,
                    p.main_stock, IF(p.main_stock > 0, cpb.in_stock - cpb.hold, IF(cpb.in_stock > 0, cpb.in_stock - cpb.hold, 15)) AS stock, cpb.in_stock, cpb.hold, cp.supplier_name, cp.supplier_code, cs.name AS supplier, pd.discount_end, pd.discount_rate, p._discount
            FROM  tm, products p, cdb_product_ballance cpb, rk_prod_cat_lvl_1 pc1, rk_categories rc, cdb_products cp LEFT JOIN cdb_suppliers cs ON cp.supplier_id = cs.id
                                                                                                                     LEFT JOIN prod_discounts  pd ON cp.id = pd.product_id
            WHERE p.main_id = {$products[$i]['id']} 
            AND p.id = cp.id
            AND p.id = cpb.product_id 
            AND p.tm_id = tm.id
            AND pc1.cat_id = rc.id 
            AND rc.is_tag = 0
            AND pc1.prod_id = p.id
            AND p.main_id > 0
            AND p.visible = 1
            AND p.id NOT IN (12524, 12525, 12526, 22222, 5555)
            AND p.for_proff = 0 
            AND p.active = 1 
            AND p.available = 1
            AND p.special_price > 0
            AND p._discount >= 10
            AND IF(p.main_stock > 0, cpb.in_stock - cpb.hold, IF(cpb.in_stock > 0, cpb.in_stock, 15)) > 0
            GROUP BY pc1.prod_id ORDER BY pc1.prod_id";

            $another_pack = db::get_rows($query);

            if ($another_pack) {

                $products[$i]['group_id'] = $products[$i]['id'];

                for ($j=0; $j < count($another_pack); $j++) {

                    $another_pack[$j]['name'] = $products[$i]['name'];
                    $another_pack[$j]['url'] = $products[$i]['url'] . "#pack-{$another_pack[$j]['id']}";
                    $another_pack[$j]['tm'] = $products[$i]['tm'];
                    $another_pack[$j]['cat_lvl_3'] = $products[$i]['cat_lvl_3'];
                    $another_pack[$j]['market_category'] = $products[$i]['market_category'];
                    $another_pack[$j]['ya_id'] = $products[$i]['ya_id'];
                    $another_pack[$j]['ya_parent_id'] = $products[$i]['ya_parent_id'];
                    $another_pack[$j]['for_legs'] = $products[$i]['for_legs'];
                    $another_pack[$j]['discount'] = $another_pack[$j]['discount'];


                    $return[] = $another_pack[$j];

                }

                $products[$i]['url'] = $products[$i]['url'] . "#pack-{$products[$i]['id']}";

            }



            $return[] = $products[$i];

        }


        $excel = export::create_new_file();

        $sheet = $excel->getSheet(0);


        $sheet->setCellValue('A' . (1), 'feed id');
        $sheet->setCellValue('B' . (1), 'offer id');
        $sheet->setCellValue('C' . (1), 'Категория');
        $sheet->setCellValue('D' . (1), 'Тип товара');
        $sheet->setCellValue('E' . (1), 'бренд/Вендор');
        $sheet->setCellValue('F' . (1), 'Ссылка на модель');
        $sheet->setCellValue('G' . (1), 'Название модели');
        $sheet->setCellValue('H' . (1), 'Цвет (если есть)');
        $sheet->setCellValue('I' . (1), 'Старая цена');
        $sheet->setCellValue('J' . (1), 'Новая (промо) цена');
        $sheet->setCellValue('K' . (1), 'Размер (глубина) скидки');
        $sheet->setCellValue('L' . (1), 'Размер (глубина) стока');


        for ($i=0; $i < count($return); $i++) {

            $sheet->setCellValue('A' . ($i + 2), '');
            $sheet->setCellValue('B' . ($i + 2), $return[$i]['id']);
            $sheet->setCellValue('C' . ($i + 2), $return[$i]['market_category']);
            $sheet->setCellValue('D' . ($i + 2), '');
            $sheet->setCellValue('E' . ($i + 2), preg_replace(array('/&laquo;/', '/&raquo;/', '/&nbsp;/', '/&lt;p&gt;/', "/&lt;\/p&gt;/", '/&/', '/"/', '/>/', '/</', '/\'/'),
                                                              array('&quot;', '&quot;',' ', '', '', '&amp;', '&quot;', '&gt;', '&lt;', '&apos;'), $return[$i]['tm']));
            $sheet->setCellValue('F' . ($i + 2), 'https://www.roskosmetika.ru/product/' . $return[$i]['url']);
            $sheet->setCellValue('G' . ($i + 2), preg_replace(array('/&laquo;/', '/&raquo;/', '/&nbsp;/', '/&lt;p&gt;/', "/&lt;\/p&gt;/", '/&/', '/"/', '/>/', '/</', '/\'/'),
                                                              array('&quot;', '&quot;',' ', '', '', '&amp;', '&quot;', '&gt;', '&lt;', '&apos;'), ($return[$i]['name'] . ", " . $return[$i]['pack'] . ' (' . $return[$i]['tm'] . ')')));
            $sheet->setCellValue('H' . ($i + 2), '');
            $sheet->setCellValue('I' . ($i + 2), $return[$i]['real_price']);
            $sheet->setCellValue('J' . ($i + 2), $return[$i]['real_special_price']);
            $sheet->setCellValue('K' . ($i + 2), $return[$i]['discount']);
            $sheet->setCellValue('L' . ($i + 2), $return[$i]['stock']);
        }


        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename=' . 'market-sale.xls');

        $writer = PHPExcel_IOFactory::createWriter($excel,'Excel5');
        $writer->save('php://output');

        exit;




exit;
            // правка фасовок для фидов
            
            $pack = '200&nbsp;мл';
            $string = preg_replace(array('/&laquo;/', '/&raquo;/', '/&nbsp;/', '/&lt;p&gt;/', "/&lt;\/p&gt;/", '/&/', '/"/', '/>/', '/</', '/\'/'),
            array('&quot;', '&quot;',' ', '', '', '&amp;', '&quot;', '&gt;', '&lt;', '&apos;'),
            $pack);

            $pack = $string;

            $pack_value = FALSE;

            if (preg_match('/[0-9,.]+\s?(шт|листов|л|мл|г|кг|мг|м|см|мм)/ui', $pack, $matches)) {
                $pack_value = trim(preg_replace('/[^0-9,.\s]/ui', '', $matches[0]));
            }

            $matches = array();

            if ($pack_value) {
                if (preg_match("/(шт|листов)/", $pack)) {            
                  $param = "\t\t\t\t<param name=\"Количество\">$pack_value</param>\n";
                } else {
                    if (preg_match("/(л|мл)/", $pack, $matches) != 0) {
                        $param = "\t\t\t\t<param name=\"Объем\" unit=\"{$matches[0]}\">$pack_value</param>\n";
                    } else {
                        if (preg_match("/(г|кг|мг)/", $pack, $matches) != 0) {
                            $param = "\t\t\t\t<param name=\"Вес\" unit=\"{$matches[0]}\">$pack_value</param>\n";
                        } else {
                            if (preg_match("/(м|см|мм)/", $pack, $matches) != 0) {
                                $param = "\t\t\t\t<param name=\"Высота\" unit=\"{$matches[0]}\">$pack_value</param>\n";
                            }
                        }
                    }              
                }
            }


            vd($pack);
            vd($pack_value);
            vd($param);

            exit;



        //
        // скрипт переноса главной категории в другую главную
        //
        $new_subcat = 42;    
        $array = array();

        $new_cat = DB::get_row('SELECT * FROM rk_categories WHERE id = 2');
        $old_cat = DB::get_row('SELECT * FROM rk_categories WHERE id = 1332');


        $array[] = array('old' => $old_cat['url'], 'new' => $new_cat['url'] . '/' . $old_cat['url']);

        // получаем текущие id 2-х категорий
        $cat_2 = db::get_rows("SELECT id, url, name, is_tag, _full_url, url, visible FROM rk_categories WHERE parent_1 = 1332 AND parent_2 = 0");

        // получаем id продуктов, прикрученных к категории 1 уровня
        $prod_cat_1 = db::get_rows("SELECT prod_id FROM rk_prod_cat_lvl_1 WHERE cat_id = 1332");

        //  меняем уровень главной категории из 1 в 2
        DB::query("UPDATE rk_categories rc SET rc.level = 2, rc.parent_1 = {$new_cat['id']}, rc._full_url =  CONCAT('{$new_cat['url']}', '/', rc.url), rc.norder = 0 WHERE rc.id = 1332");

        //  обновляем связку в субкатегории для 2-го уровня
        if((int)DB::get_field('kol',"SELECT COUNT(id) AS kol FROM rk_cat_subcat WHERE cat_id = 1332 AND subcat_id = 42") == 0) 
        {
            db::query("REPLACE INTO rk_cat_subcat SET cat_id = 1332, subcat_id = 42");
        }


        // перенос в связке первого уровня с одной категории на другую
        for ($i=0; $i <count($prod_cat_1) ; $i++) { 
            DB::query("DELETE FROM rk_prod_cat_lvl_1 WHERE prod_id = {$prod_cat_1[$i]['prod_id']} AND cat_id = 1332");
            DB::query("REPLACE INTO rk_prod_cat_lvl_1 SET prod_id = {$prod_cat_1[$i]['prod_id']}, cat_id = 2");
            DB::query("REPLACE INTO rk_prod_cat_lvl_2 SET prod_id = {$prod_cat_1[$i]['prod_id']}, cat_id = 1332");
        }


        // перенос категорий с 2 на 3 уровень
        for ($i=0; $i < count($cat_2); $i++) { 
            
            $this_subcats = DB::get_rows("SELECT * FROM rk_cat_subcat WHERE cat_id = {$cat_2[$i]['id']}");
            for ($j=0; $j < count($this_subcats); $j++) { 

                DB::query("UPDATE rk_subcategories SET `level` = '3' WHERE id = {$this_subcats[$j]['subcat_id']}");
            }

            DB::query("UPDATE rk_categories rc SET rc.level = '3', rc.parent_1 = 1332, rc.parent_2 = 2, rc._full_url =  CONCAT('{$new_cat['url']}', '/', '{$old_cat['url']}', '/', '{$cat_2[$i]['url']}'), rc.norder = 0 WHERE rc.id = {$cat_2[$i]['id']}");

            if((int)$cat_2[$i]['visible'] === 1)
            {
                $array[] = array('old' => $cat_2[$i]['_full_url'], 'new' => $new_cat['url'] . '/' . $old_cat['url'] . '/' . $cat_2[$i]['url']);
            }

            $rk_old_prod_cats = DB::get_rows("SELECT prod_id, cat_id FROM rk_prod_cat_lvl_2 WHERE cat_id = {$cat_2[$i]['id']}");

            for ($j=0; $j < count($rk_old_prod_cats) ; $j++) { 
                DB::query("DELETE FROM rk_prod_cat_lvl_2 WHERE cat_id = {$cat_2[$i]['id']}");
                DB::query("REPLACE INTO rk_prod_cat SET prod_id = {$rk_old_prod_cats[$j]['prod_id']}, cat_id = {$cat_2[$i]['id']}");
            }

            // получаем текущие id 3-х категорий
            $cats_3 = db::get_rows("SELECT id, url, name, is_tag, _full_url, url, visible, is_tag FROM rk_categories WHERE parent_1 = {$cat_2[$i]['id']}  AND parent_2 = 1332 AND `level` = '3'");

            for ($j=0; $j < count($cats_3) ; $j++) { 
                
                $prod_cat_lvl_3 = DB::get_rows("SELECT prod_id, cat_id FROM rk_prod_cat WHERE cat_id = {$cats_3[$j]['id']}");

                for ($k=0; $k < count($prod_cat_lvl_3) ; $k++) { 
                    DB::query("DELETE FROM rk_prod_cat WHERE cat_id = {$cats_3[$j]['id']}");
                    DB::query("REPLACE INTO rk_prod_cat SET prod_id = {$prod_cat_lvl_3[$k]['prod_id']}, cat_id = {$cat_2[$i]['id']}");
                }

                // получаем связку подкатегории + категории/ удаляем их
                $subcat_3 = DB::get_rows("SELECT subcat_id FROM rk_cat_subcat WHERE cat_id = {$cats_3[$j]['id']} GROUP BY subcat_id");

                for ($k=0; $k < count($subcat_3) ; $k++) { 
                    DB::query("DELETE FROM rk_subcategories WHERE id = {$subcat_3[$k]['subcat_id']} AND `level` = '3'");
                }

                DB::query("DELETE FROM rk_categories WHERE id = {$cats_3[$j]['id']} AND `level` = 3");

                if((int)$cats_3[$j]['visible'] === 1)
                {
                    $array[] = array('old' => $cats_3[$j]['_full_url'], 'new' => $new_cat['url'] . '/' . $old_cat['url'] . '/' . $cat_2[$i]['url']);
                }
                
            }

        }


        $excel = export::create_new_file();

        $sheet = $excel->getSheet(0);

        $sheet->getStyle('A' . (1))->getFont()->setBold(true);
        $sheet->setCellValue('A' . (1), 'Redirects');

        $columns = [
            'A' => '#'
        ];

        for ($i=0; $i < count($array); $i++) {
            $sheet->setCellValue('A' . ($i + 2), 'RewriteRule ' . 'category/' . $array[$i]['old'] . '$ https://www.roskosmetika.ru/category/' . $array[$i]['new'] . ' [R=301,L]');
        }


        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename=' . 'redirects.xls');

        $writer = PHPExcel_IOFactory::createWriter($excel,'Excel5');
        $writer->save('php://output');

        exit;


        // конец скрипта переноса






 Cart::setInfo();
 vd(Cart::$_orderID);
 vd(Cart::$data['products']);
        exit;

                    DB::query('TRUNCATE rk_shipping_info');

                    $prices = 4;

                    $shipping_data = array('min' => array(),
                                           'max' => array()
                                           );


                    for ($i=1; $i <= $prices; $i++) { 

                        $step = orders::calculate_shipping_info('MIN', $i, array(907));
                        $shipping_data['min']['prices'][] = $step['price'];
                        $shipping_data['min'][$step['price']] = $step;

                        $step = orders::calculate_shipping_info('MAX', $i, array(907));
                        $shipping_data['max']['prices'][] = $step['price'];
                        $shipping_data['max'][$step['price']] = $step;

                    }

                    $min_price = min($shipping_data['min']['prices']);
                    $max_price = max($shipping_data['max']['prices']);

                    DB::query("INSERT INTO rk_shipping_info(type, price, region, shipping_type, duration_min, duration_max) 
                                      VALUES('min', 
                                             $min_price, 
                                             '{$shipping_data['min'][$min_price]['region']}',
                                             '{$shipping_data['min'][$min_price]['shipping_type']}',
                                             {$shipping_data['min'][$min_price]['duration_min']},
                                             {$shipping_data['min'][$min_price]['duration_max']})");

                    DB::query("INSERT INTO rk_shipping_info(type, price, region, shipping_type, duration_min, duration_max) 
                                      VALUES('max', 
                                             $max_price, 
                                             '{$shipping_data['max'][$max_price]['region']}',
                                             '{$shipping_data['max'][$max_price]['shipping_type']}',
                                             {$shipping_data['max'][$max_price]['duration_min']},
                                             {$shipping_data['max'][$max_price]['duration_max']})");


                    $compensation = max(DB::get_row('SELECT max(compensation), max(compensation_1), max(compensation_2), max(compensation_3), max(compensation_4), max(compensation_6) FROM shipping_regions_compensations'));

                    DB::query("INSERT INTO rk_shipping_info(type, price) 
                                      VALUES('compensation', 
                                             $compensation)");
echo "-FIN-";
exit;

// Cart::setInfo();
//                             $content = [
//                                 'order_id' => 'ordID',
//                                 'order_time' => date("H:i:s"),
//                                 'order_date' => date("d.m.Y"),
//                                 'products' => Cart::$data['products'],
//                                 'discount_sum' => Cart::$data['info']['sum_with_discount'],
//                                 'name' => 'имя',
//                                 'surname' => 'фами',
//                                 'patronymic' => 'отчест',
//                                 'organization' => 'орг',
//                                 'address' => 'адрес',
//                                 'phone' => 'телефон',
//                                 'duration' => 'дни доставки',
//                                 'email' => 'email',
//                                 'id' => 'cid',
//                                 'etc' => 'note',
//                             ] + Cart::$data['info'];



// vd($content);
// exit;


$query = "SELECT o.order_number, o.client_id, o.payment_id, o.cost, o.delivery, (SELECT SUM(pd.amount) FROM payments_done pd WHERE pd.order_number = o.order_number) AS payment_done, (o.cost - (SELECT SUM(pd.amount) FROM payments_done pd WHERE pd.order_number = o.order_number)) AS dif
FROM orders o 
WHERE o.status_id = 1
AND o.payway_id = 3
AND (SELECT COUNT(pd.id) FROM payments_done pd WHERE pd.order_number = o.order_number) > 0
AND o.cost <> (SELECT SUM(pd.amount) FROM payments_done pd WHERE pd.order_number = o.order_number)";


    $orders = DB::get_rows($query);
  
    for ($i=0; $i < count($orders); $i++) 
    { 
        if ((int)$orders[$i]['dif'] < 0) {

            $amount = DB::get_field('id', "SELECT id FROM payments_done WHERE order_number = {$orders[$i]['order_number']} LIMIT 1");
            DB::query("UPDATE payments_done pd SET pd.amount = (pd.amount + {$orders[$i]['dif']}) WHERE pd.id = $amount");

        }

    }          

echo"FIN";
exit;






        $fieldsN = count($fields);
        $excel->setActiveSheetIndex(0);
        $list = $excel->getActiveSheet();

        for ($i = 0; $i < $fieldsN; ++$i) {
            $column = chr(65 + $i);

            $list->setCellValue($column . '1', $fields[$i]['name']);                       
            $list->getStyle($column . '1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $list->getStyle($column . '1')->getFill()->getStartColor()->setRGB('fffe8d');
            $list->getStyle($column . '1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $list
                ->getColumnDimension($column)
                ->setAutoSize(true);

            $list
                ->getStyle($column . '1')
                ->getFont()
                ->setBold(true);


        }











$a =  (string)'0' ?: 'qqq';
vd($a);exit;

                        $time_start = microtime(true);
                        optimization::del_link_tables();
                        optimization::add_link_tables();
                        $time_end = microtime(true);
                        $time = $time_end - $time_start;

vd($time);exit;

vd($_SERVER['REQUEST_TIME']);
vd('---');

vd(time());
exit;
$user = '8 (909) 650 kdwekpweслцдсдцтс 7276 ';    

            $user = filter_var($user, FILTER_SANITIZE_NUMBER_INT);
            $user = (mb_substr($user, 0, 1) == 8 ? substr_replace($user, '+7', 0, 1) : $user);

var_dump($user);exit;

$gzip = function_exists('gzencode');

$file = SITE_PATH . 'tmp/' . uniqid('mindbox_export_clients_') . '.csv' . ($gzip ? '.gz' : '');

$f = fopen($file, 'w');

fputs(
    $f,
    implode(
        ';',
        [
            'RoskosmetikaWebSiteId',
            'Email',
            'MobilePhone',
            'TypeOfCustomer',
            'SourceDateTime',
            'SourceActionTemplate',
            'SourcePointOfContact',
            'LastUpdateDateTime',
            'LastUpdateActionTemplate',
            'LastUpdatePointOfContact',
            'IsSubscribedByEmail',
        ]
    ) . "\r\n"
);

DB::get_rows(
    'SELECT c.id, IFNULL(c.email, "") AS email, IFNULL(c.phone, "") AS phone,
        IF(c.type_id = 1, "Cosmo", IF(c.type_id = 2, "YurLic", "FizLic")) AS customer,
        IF(u.created IS NULL OR u.created = "0000-00-00 00:00:00", IF(u.date IS NULL OR u.date = "0000-00-00", c.date_of_change, CONCAT(u.date, " 00:00:00")), u.created) AS create_date,
        IF(u.email IS NULL, "PopalVBazyNotReg", "RegistrationOnSite") AS registration_type,
        "' . env('MINDBOX_IDENTITY') . '" AS source_type,
        IFNULL(c.date_of_change, "' . date('Y-m-d H:i:s') . '") AS change_date,
        "RedaktirovanieVLk" AS change_source,
        "' . env('MINDBOX_IDENTITY') . '" AS change_type,
        IF(u.active = 1, "true", "false") AS active
    FROM clients AS c
    LEFT JOIN rk_unsubscribe AS u ON u.email = c.email
    WHERE c.archive = 0
    ORDER BY c.id',
    function ($order) use (&$f) {
        $order['email'] = mb_convert_encoding($order['email'] , 'UTF-8' , 'UTF-8');
        $order['phone'] = mindbox::prepare_phone(mb_convert_encoding($order['phone'] , 'UTF-8' , 'UTF-8'));

        if ($order['create_date'] === $order['change_date']) {
            $order['change_date'] = date('Y-m-d H:i:s');
        }

        array_walk($order, function (&$value) {
            $value = escape_csv($value);
        });

        fputs(
            $f,
            implode(';', $order) .
            "\r\n"
        );
    }
);

fclose($f);

if ($gzip) {
    file_put_contents($file, gzencode(file_get_contents($file)));
}

$ch = curl_init(
    'https://roskosmetika-services.directcrm.ru/v2/import?' .
    http_build_query(
        [
            'operation' => 'DirectCrm.Customers.Import',
            'externalSystem' => 'RoskosmetikaWebSiteId',
            'brand' => env('MINDBOX_BRAND'),
            'csvCodePage' => 65001,
            'csvColumnDelimiter' => ';',
            'csvTextQualifier' => '"',
        ]
    )
);

curl_setopt_array(
    $ch,
    [
        CURLOPT_POST => true,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_TIMEOUT => 60,
        CURLOPT_FAILONERROR => false,
        CURLOPT_POSTFIELDS => file_get_contents($file),
        CURLOPT_HTTPHEADER => array_merge(
            [
                'Accept: application/xml',
                'Authorization: Basic ' . base64_encode(env('MINDBOX_USER') . ':' . env('MINDBOX_PASSWORD')),
                'Content-Type: text/csv',
                'Content-Length' => filesize($file),
            ],
            $gzip
                ? [
                'Content-Encoding: gzip',
            ]
                : []
        ),
    ]
);

$response = curl_exec($ch);

$info = curl_getinfo($ch);

if (($errno = curl_errno($ch)) || $info['http_code'] >= 300) {
    print 'Curl error: ' . curl_error($ch) . ' (Error number: ' . $errno . "). Request info:\n" . print_r($info, true) . "\n";

    if ($response) {
        print "\nResponse:\n" . $response . "\n";
    }
} else {
    file_put_contents(__DIR__ . '/.mindbox-clients.log', date('r') . ":\n" . $response . "\n-----------------------\n", FILE_APPEND);
}

curl_close($ch);

unlink($file);


vd('ок');

exit;










        // youtube test
    
        define('CREDENTIALS_PATH', '~/php-yt-oauth2.json');

        $client = new Google_Client();
        $client->setApplicationName('web-client-1');
        $client->setScopes('https://www.googleapis.com/auth/youtube.force-ssl');
        // Set to name/location of your client_secrets.json file.
        $client->setAuthConfig('client_secrets.json');
        $client->setAccessType('offline');

          // Load previously authorized credentials from a file.
          $credentialsPath =




           expandHomeDirectory(CREDENTIALS_PATH);
          if (file_exists($credentialsPath)) {
            $accessToken = json_decode(file_get_contents($credentialsPath), true);
          } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));

            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

            // Store the credentials to disk.
            if(!file_exists(dirname($credentialsPath))) {
              mkdir(dirname($credentialsPath), 0700, true);
            }
            file_put_contents($credentialsPath, json_encode($accessToken));
            printf("Credentials saved to %s\n", $credentialsPath);
          }
          $client->setAccessToken($accessToken);

          // Refresh the token if it's expired.
          if ($client->isAccessTokenExpired()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
          }

        





        $api_key = env('GOOGLE_API_KEY');

        require_once SITE_PATH . 'includes/Google/vendor/autoload.php';




        $client = new Google_Client();
        $client->setClientId('1011352013931-ob1ti4o4gslos4fqrgrk1l8fhek5ihra.apps.googleusercontent.com');
        $client->setClientSecret('YO4uFMKpdAWg1qLNX5E9hT81');
        $client->setScopes('https://www.googleapis.com/auth/youtube');
        $client->setDeveloperKey($api_key);
        $service = new Google_Service_YouTube($client);

        $response = $service->channelSections->listChannelSections('snippet,contentDetails',  array('mine' => true));

        vd($response);
        exit;





        $url = "https://roskosmetika-services.directcrm.ru/v2/recommendation-mechanics/get-product-recommendations?operation=PoZakazam6&customerIdentity=7691&limit=6";
        //$url = "https://roskosmetika-services.services.directcrm.ru/v2/recommendation-mechanics/get-product-recomendations?operation=SEtimTovaromPokupayut&products=18626&categories=248&limit=2";
        vd(remote_request($url, [], false, true, 3, true));
        exit;

        var_dump(yml::get_products_merchant());
       
        

        $items = DB::get_rows('SELECT * FROM cart WHERE order_id = 244090');

        $a = admitad::ok_page(90314, $items, 2300, '6588c4728d9a206e5c266304e5aabe6c');

        var_dump($a);

        $b = admitad::ok_page(90314, $items, 2300, 0);

        var_dump($b);

        exit;

        //optimization::category_tm();

        // $query = "SELECT tm_id, cat_id, full_desc
        //           FROM cat_in_tm
        //           WHERE full_desc != ''";

        // $full_descs = DB::get_rows($query);


        // foreach ($full_descs as $full_desc) {
        //     $query = "UPDATE rk_tm_in_cat SET full_desc = '{$full_desc['full_desc']}' WHERE tm_id = {$full_desc['tm_id']} AND cat_id = {$full_desc['cat_id']}";
        //     DB::query($query); 

        //  } 
        
        // sitemap::update_map();
        // 
        // 
        
        $string_1 = DB::get_field('address', 'SELECT address FROM orders WHERE ID = 244088');

        // var_dump($string_1);

        $n_string = trim(str_replace(array("\\r\\n", "\\r", "\\n"), array(' ', ' ', ' '), $string_1));


        // var_dump($string);

        var_dump($n_string);exit;


        Template::set_page('index_success', 'Обновление файликов', 'Все прошло по плану!=) Красауэла, да!');

    }

}
?>