<?php

/**
 * Update packs
 */
class Controller_update_packs extends Controller_Base
{

    function index($args)
    {

        $packs= db::get_rows("SELECT p.id,p.url,p.name,p.short_description,p.description,p.description_synth,p.main_id, 
          (SELECT pp.id FROM products pp WHERE pp.main_id = p.id AND pp.active = 1 LIMIT 1)
          AS wrong_pack, 
          (SELECT COUNT(pp.id) FROM products pp WHERE pp.main_id = p.id AND pp.active = 1)
            quantity FROM products p WHERE p.active = 0 
          AND p.main_id = 0 
        AND (SELECT COUNT(pp.id)
        FROM products pp WHERE pp.main_id = p.id AND pp.active = 1) > 0 ");

        for ($i=0;$i<count($packs);$i++)
        {
            $name=$packs[$i]['name'];
            $id=$packs[$i]['id'];
            $url=$packs[$i]['url'];
            $main_id=$packs[$i]['main_id'];
            $wrong_pack=$packs[$i]['wrong_pack'];
            $short_description=$packs[$i]['short_description'];
            $description=$packs[$i]['description'];
            $description_synth=$packs[$i]['description_synth'];
            //echo"$id,$main_id,$name,$short_description,$description_synth,$wrong_pack";

            db::query(
                "UPDATE products 
	            SET 
	               main_id='0',
	               url='$url',
	                short_description='$short_description',
	                description='$description',
	                description_synth='$description_synth',
	                name='$name'
	               WHERE id = $wrong_pack;
	        ");

            db::query(
                "UPDATE products 
	            SET 
	               main_id='$wrong_pack',
	               url='',
	                short_description='',
	                description='',
	                description_synth='',
	                name=''
	               WHERE id = $id;
	        ");
            db::query(
                "DELETE FROM rk_prod_desc
	               WHERE id ='$wrong_pack';
	        ");
            db::query(
                "UPDATE rk_prod_desc 
	            SET 
	               id='$wrong_pack' 
	               WHERE id = $id;
	        ");
            db::query(
                "DELETE FROM rk_prod_seo
	               WHERE id ='$wrong_pack';
	        ");
            db::query(
                "UPDATE rk_prod_seo
	            SET 
	               id='$wrong_pack' 
	               WHERE id = $id;
	        ");
            db::query(
                "UPDATE rk_prod_lines
	            SET 
	               prod_id='$wrong_pack' 
	               WHERE prod_id = $id;
	        ");
            db::query(
                "UPDATE rk_prod_sets
	            SET 
	               prod_id='$wrong_pack' 
	               WHERE prod_id = $id;
	        ");
            db::query(
                "UPDATE rk_prod_type_page
	            SET 
	               product_id='$wrong_pack' 
	               WHERE product_id = $id;
	        ");
            db::query(
                "UPDATE rk_prod_filter
	            SET 
	               prod_id='$wrong_pack' 
	               WHERE prod_id = $id;
	        ");


        }
        if (count($packs)==0)
        {
            Template::set_page('index_info', 'что-то пошло не так', 'нечего исправлять');
        }

        else
        {
            Template::set_page('index_success', 'отработанно', 'Все прошло успешно. Исправленно ' . count($packs) . ' id');
        }



    }

}

?>