<?php

class Controller_prod_cert extends Controller_Base
{
    /**
     * Certificates list page
     *
     * @param $arg
     */
    public function index($arg)
    {
        Template::set_page('prod_certs', 'Сертификаты', [
            'certificates' => ProdCert::get_list(false, true, isset($_GET['sort']) ? $_GET['sort'] : null, isset($_GET['sort_dir']) && $_GET['sort_dir'] === 'desc' ? false : true),
        ]);
    }

    /**
     * Create/edit product certificate page
     *
     * @param $arg
     */
    public function edit($arg)
    {
        if (isset($arg[0])) {
            $data = ProdCert::get_by_id($arg[0]);

            if (!$data) {
                $this->nopage();

                return;
            }
        } else {
            $data = [
                'id' => null,
                'name' => '',
                'folder_url' => '',
                'end_date' => '',
            ];
        }

        if (!empty($_POST['delete_image'])) {
            $this->jsonResponse(!!ProdCert::delete_image($_POST['delete_image']));
        }

        if (!empty($_POST['data'])) {
            $data = array_merge($data, $_POST['data']);

            $id = null;

            try {
                $id = ProdCert::update($data['id'], $data['name'], $data['folder_url'], $data['end_date']);
            } catch (Exception $e) {
                Template::set('error', $e->getMessage());
            }

            if ($id) {
                if ($data['id']) {
                    if (!empty($data['images_ids'])) {
                        for ($i = 0, $n = count($data['images_ids']); $i < $n; ++$i) {
                            if (
                                !empty($_FILES['images_files']['name'][$i])
                                && !$_FILES['images_files']['error'][$i]
                                && is_uploaded_file($_FILES['images_files']['tmp_name'][$i])
                                && preg_match('/\.jpe?g$/i', $_FILES['images_files']['name'][$i])
                            ) {
                                if ($data['images_ids'][$i]) {
                                    ProdCert::update_image($data['images_ids'][$i], $_FILES['images_files']['tmp_name'][$i]);
                                } else {
                                    ProdCert::add_image($id, $_FILES['images_files']['tmp_name'][$i]);
                                }
                            }
                        }
                    }
                }

                redirect('/admin/prod_cert/edit/' . $id);
            }
        }

        if ($data['id']) {
            admin::products_search_form(
                function ($productIDs) use ($data) {
                    ProdCert::add_products($data['id'], $productIDs);
                },
                function () use ($data) {
                    return ProdCert::linked_products($data['id']);
                },
                function ($productIDs) use ($data) {
                    ProdCert::remove_products($data['id'], $productIDs);
                },
                null,
                [
                    'search',
                    'categories',
                    'tms',
                    'added',
                    'certificates',
                ]
            );
        }

        Template::add_css('flick/jquery-ui.min.css');
        Template::add_script('jquery-ui.min.js');
        Template::add_script('jquery-ui-datepicker-ru.js');

        Template::add_script('admin/prod_cert.js');

        Template::set_page('prod_cert', $data['id'] ? 'Редактирование сертификата' : 'Создание сертификата', [
            'data' => $data,
        ]);
    }

    /**
     * Delete product certificate
     *
     * @param $arg
     */
    public function delete($arg)
    {
        ProdCert::delete($arg[0]);

        redirect('/admin/prod_cert');
    }

    /**
     * @inheritdoc
     */
    protected function admin_access()
    {
        return admin::RIGHTS_COMMON | admin::RIGHTS_PRODUCT_CERTIFICATES;
    }
}
