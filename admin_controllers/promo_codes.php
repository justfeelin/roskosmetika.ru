<?php

/**
 * Work with promo codes
 */
class Controller_promo_codes extends Controller_base
{

    const PAGE_HEADER = 'Добавление промо-кодов';

    /**
     * Form for adding.
     * @param array $args URL params.
     */
    function index($args)
    {
        $search = [];

        $search['code'] = isset($_GET['code']) && $_GET['code'] !== '' ? $_GET['code'] : null;
        $search['action_type_id'] = isset($_GET['action_type_id']) && $_GET['action_type_id'] > 0 ? (int)$_GET['action_type_id'] : null;

        $count = promo_codes::get_promo_codes(null, $search['code'], $search['action_type_id']);

        $pagination = $this->pagination($count, promo_codes::ON_PAGE, $search);

        $urlParams = [];

        if ($search['code'] !== null) {
            $urlParams[] = 'code=' . rawurlencode($search['code']);
        }

        if ($search['action_type_id'] !== null) {
            $urlParams[] = 'action_type_id=' . $search['action_type_id'];
        }

        if ($pagination['showAll']) {
            $urlParams[] = 'showAll';
        } elseif ($pagination['page'] > 1) {
            $urlParams[] = 'p=' . $pagination['page'];
        }

        Template::set_page('promo_codes', self::PAGE_HEADER, [
            'back_url_param' => $urlParams ? '?bu=' . rawurlencode(implode('&', $urlParams)) : '',
            'search' => $search,
            'promo_codes' => promo_codes::get_promo_codes($pagination['page'], $search['code'], $search['action_type_id']),
        ]);
    }

    /**
     * Add codes by query
     * @param array $args URL params
     */
    function add_by_query($args)
    {

        if ((isset($_POST['save'])) && (!empty($_POST['query'])) && (!empty($_POST['discount']))) {

            $main_query = mb_strtolower($_POST['query']);

            $discount = (int)$_POST['discount'];

            $danger_words = array('update', 'delete', 'truncate', 'replace', 'insert', 'select *', 'create', 'create table', 'show columns');
            $main_query = str_replace($danger_words, 'DANGER WORD!!!', $main_query);

            if (strpos($main_query, 'DANGER WORD!!!') || $discount == 0) redirect('/admin/promo_codes/');

            $main_query = str_replace(array('\\.', '\\', '*#+-'), array('*#+-', '', '\\.'), $main_query);

            $data = DB::get_rows($main_query);

            $time_start = microtime(1);

            foreach ($data as $key => $value) {

                $promo_code = promo_codes::generate_promo_code($discount);

                $query = "UPDATE rk_unsubscribe
                          SET promo_code = '$promo_code' WHERE email = '{$value['email']}'";

                DB::query($query);

                $time_end = microtime(1);
                $time = $time_end - $time_start;

                if ($time > 25)
                    break;
            }

            if (count($data) != ($key + 1)) {

                $content = array('query' => $main_query, 'discount' => $discount, 'key_start' => $key);
                Template::set_page('promo_add_by_query_next', self::PAGE_HEADER, $content);

            } else {
                $key = $key + 1;
                Template::set_page('index_info', self::PAGE_HEADER, "Коды добавлены. Количество адресов: $key");
            }
        } // load query template
        else {
            Template::set_page('promo_add_by_query', self::PAGE_HEADER, '');
        }
    }

    /**
     * Continue add codes
     * @param array $args URL params
     */
    function continue_add_by_query($args)
    {

        if ((isset($_POST['save'])) && (!empty($_POST['query'])) && (!empty($_POST['discount'])) && (!empty($_POST['key_start']))) {

            $main_query = mb_strtolower($_POST['query']);

            $discount = (int)$_POST['discount'];
            $key_start = $_POST['key_start'];

            $danger_words = array('update', 'delete', 'truncate', 'replace', 'insert', 'select *', 'create', 'create table', 'show columns');
            $main_query = str_replace($danger_words, 'DANGER WORD!!!', $main_query);

            if (strpos($main_query, 'DANGER WORD!!!') || $discount == 0) redirect('/admin/promo_codes/');

            $main_query = str_replace(array('\\.', '\\', '*#+-'), array('*#+-', '', '\\.'), $main_query);

            $data = DB::get_rows($main_query);
            $quant = count($data);

            $time_start = microtime(1);

            for ($i = ($key_start + 1); $i < $quant; $i++) {

                $promo_code = promo_codes::generate_promo_code($discount);

                $query = "UPDATE rk_unsubscribe
                          SET promo_code = '$promo_code' WHERE email = '{$data[$i]['email']}'";

                DB::query($query);

                $time_end = microtime(1);
                $time = $time_end - $time_start;

                if ($time > 25)
                    break;
            }

            if ($quant != ($i + 1)) {

                $content = array('query' => $main_query, 'discount' => $discount, 'key_start' => $i);
                Template::set_page('promo_add_by_query_next', self::PAGE_HEADER, $content);

            } else {
                $i = $i + 1;
                Template::set_page('index_info', self::PAGE_HEADER, "Коды добавлены. Количество адресов: $i");
            }
        } // Выводим список страниц
        else {
            Template::set_page('promo_add_by_query', self::PAGE_HEADER, '');
        }
    }

    /**
     * Generate code ad output in Excel file
     * @return XLS Excel file
     */
    static function generete_in_excel()
    {

        if ((isset($_POST['save'])) && (!empty($_POST['code_quan'])) && (!empty($_POST['discount']))) {

            $discount = (int)$_POST['discount'];
            $quan = (int)$_POST['code_quan'];

            if ($discount && $quan) {

                $excel = export::create_new_file();

                $excel->setActiveSheetIndex(0);
                $codes_list = $excel->getActiveSheet();

                $codes_list->setCellValue('A' . 1, 'Promo Codes');

                $excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
                $excel->getActiveSheet()->getStyle("A1")->getFont()->setSize(14);

                for ($i = 2; $i <= $quan + 1; $i++) {

                    $promo_code = promo_codes::generate_promo_code($discount);
                    if ($promo_code) $codes_list->setCellValue('A' . $i, $promo_code);

                }

                if (ob_get_level()) {
                    ob_end_clean();
                }

                //   load file 
                header("Content-type:  application/vnd.ms-excel");
                header("Content-Disposition: attachment; filename=promo_codes.xls");

                $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
                $writer->save('php://output');
                exit;

            } else {
                Template::set_page('promo_add_and_get_excel', self::PAGE_HEADER, '');
            }

        } else {
            Template::set_page('promo_add_and_get_excel', self::PAGE_HEADER, '');
        }

    }

    /**
     * Promo code create/edit page
     *
     * @param $args
     */
    public function edit($args) {
        if (isset($args[0])) {
            $data = promo_codes::get_by_id($args[0]);

            if (!$data) {
                $this->nopage();

                return;
            }

            $data['discount_roubles'] = $data['discount'] < 0;
        } else {
            $data = [
                'id' => null,
                'discount_roubles' => false,
            ];
        }

        if ($data['id']) {
            if (!empty($data['certificate']) && isset($_GET['generate_certificate'])) {
                promo_codes::generate_certificate($data);
            }
        }

        $error = null;

        if (!empty($_POST['promo_code'])) {
            $data = array_merge($data, $_POST['promo_code']);

            $id = promo_codes::update(
                $data['id'],
                $data['code'],
                (int)$data['action_type_id'] === promo_codes::ACTION_TYPE_CHEAPEST_PRODUCT_DISCOUNT
                    ? abs((int)$data['cheapest_discount'])
                    : (!empty($data['discount_roubles']) ? -1 : 1) * abs($data['discount']),
                $data['type'] === 'uniq',
                $data['end_date'],
                $data['discount_type'] == 0 ? 0 : 1,
                $data['min_sum'],
                !empty($data['use']),
                $data['action_type_id'],
                $data['min_action_products_count'],
                $data['comment'],
                !empty($data['certificate'])
            );

            if ($id) {
                if (empty($data['certificate'])) {
                    $trimElements = function ($element) {
                        return trim($element);
                    };

                    switch ((int)$data['action_type_id']) {
                        case promo_codes::ACTION_TYPE_PRODUCTS:
                            promo_codes::update_action_products($id, array_map($trimElements, explode("\n", $_POST['action']['product_ids'])));

                            break;

                        case promo_codes::ACTION_TYPE_TMS:
                            promo_codes::update_action_tms($id, (array)$_POST['action']['tm_ids'] ?: null);

                            break;

                        case promo_codes::ACTION_TYPE_CATEGORIES:
                            promo_codes::update_action_categories($id, (array)$_POST['action']['category_ids'] ?: null);

                            break;

                        case promo_codes::ACTION_TYPE_CHEAPEST_PRODUCT_DISCOUNT:
                            promo_codes::update_action_cheapest_discount_products($id, empty($_POST['action']['cheapest_product_discount_all']) ? array_map($trimElements, explode("\n", $_POST['action']['cheapest_product_discount_ids'])) : null);

                            break;
                    }
                }

                redirect('/admin/promo_codes/edit/' . $id);
            } else {
                $error = 'Промо-код с таким кодом уже существует';
            }
        }

        Template::add_css('flick/jquery-ui.min.css');

        Template::add_script('jquery-ui.min.js');
        Template::add_script('jquery-ui-datepicker-ru.js');

        Template::add_script('admin/promo_code.js');

        Template::set_page('promo_code', $data['id'] ? 'Редактирование промо-кода' : 'Создание промо-кода', [
            'promo_code' => $data,
            'error' => $error,
            'tms' => tm::get_list(false, false, false, false),
            'categories' => Categories::get_categories(),
            'actionProducts' => promo_codes::get_action_product_ids($data['id']),
            'actionTrademarks' => promo_codes::get_action_tm_ids($data['id']),
            'actionCategories' => promo_codes::get_action_category_ids($data['id']),
            'actionCheapestDiscountProducts' => promo_codes::get_action_cheapest_discount_product_ids($data['id']),
        ]);
    }

    /**
     * @inheritdoc
     */
    protected function admin_access()
    {
        return admin::RIGHTS_COMMON | admin::RIGHTS_PROMO_CODES;
    }
}
