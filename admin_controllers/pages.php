<?php

/**
 * Уравление страницами.
 */
class Controller_pages extends Controller_base
{

    const PAGE_HEADER = 'Редактирование страниц';

    /**
     * Список страниц.
     * @param <type> $args Параметры URL.
     */
    function index($args)
    {

        Template::set_page('pages', self::PAGE_HEADER, [
            'pages' => Pages::get_list(false),
        ]);
    }

    /**
     * Форма редактирование страниц.
     * @param array $args Параметры URL.
     */
    function edit($args)
    {

        if (isset($args[0]))
            $page_id = (int)$args[0];

        //Выводим статью для редактирования
        if (!empty($page_id)) {
            Template::set_form(Pages::get_page($page_id, 0));
            Template::set_page('edit_page', self::PAGE_HEADER);
        } // Сохраняем изменения
        elseif ((isset($_POST['save'])) && (!empty($_POST['header'])) && (!empty($_POST['name']))) {

            if ($_POST['type'] != 'limit') {
                $_POST['begin'] = NULL;
                $_POST['end'] = NULL;
            }

            $begin = $_POST['begin_year'] . '-' . $_POST['begin_month'] . '-' . $_POST['begin_day'];
            $end = $_POST['end_year'] . '-' . $_POST['end_month'] . '-' . $_POST['end_day'];


            $check = Pages::check_name($_POST['name']);

            if (($check['kol'] < 1) || (($check['id'] == $_POST['id']) && ($check['kol'] == 1))) {
                Pages::set_page($_POST['id'], $_POST['name'], $_POST['header'], $_POST['text'], $_POST['title'], $_POST['seo_desc'], $_POST['keywords'], $_POST['type'], $begin, $end);
            }
            redirect('/admin/pages/');
        } // Выводим список страниц
        else {
            redirect('/admin/pages/');
        }
    }

    /**
     * Форма добавления страницы
     * @param array $args Параметры URL.
     */
    function add($args)
    {

        // Сохраняем новую страницу.
        if (isset($_POST['save']) && !empty($_POST['name'])) {
            $name = $_POST['name'];
            $type = $_POST['type'];
            $begin = $_POST['begin_year'] . '-' . $_POST['begin_month'] . '-' . $_POST['begin_day'];
            $end = $_POST['end_year'] . '-' . $_POST['end_month'] . '-' . $_POST['end_day'];
            $header = (isset($_POST['header']) ? $_POST['header'] : '');
            $title = (isset($_POST['title']) ? $_POST['title'] : '');
            $keywords = (isset($_POST['keywords']) ? $_POST['keywords'] : '');
            $description = (isset($_POST['seo_desc']) ? $_POST['seo_desc'] : '');
            $content = (isset($_POST['text']) ? $_POST['text'] : '');

            if (!get_magic_quotes_gpc()) {
                $name = DB::mysql_secure_string($name);
                $type = DB::mysql_secure_string($type);
                $begin = DB::mysql_secure_string($begin);
                $end = DB::mysql_secure_string($end);
                $title = DB::mysql_secure_string($title);
                $keywords = DB::mysql_secure_string($keywords);
                $description = DB::mysql_secure_string($keywords);
                $content = DB::mysql_secure_string($content);
            }

            if ($type != 'limit') {
                $begin = NULL;
                $end = NULL;
            }

            $check = Pages::check_name($name);

            if ($check['kol'] < 1) {
                $pageID = Pages::add_page($name, $header, $content, $title, $description, $keywords, $type, $begin, $end);
                redirect('/admin/pages/edit/' . $pageID);
            }

        } elseif (isset($_POST['save']) && empty($_POST['name'])) {
            Template::set_page('edit_page', 'Добавить страницу');
        } // Выводим форму для добавления страницы.
        else {
            Template::set_page('edit_page', 'Добавить страницу');
        }
    }

    /**
     * Скрывает/отображает cтраницу.
     * @param array $args Параметры URL.
     */
    function show($args)
    {

        if (isset($args[0]))
            $id = (int)$args[0];

        if (!empty($id)) {
            Pages::change_visibile($id);
        }

        redirect('/admin/pages/');
    }
}
