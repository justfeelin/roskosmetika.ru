<?php

class Controller_article_prod extends Controller_Base
{

    /**
     * Import article - product link
     */
    function index($arg)
    {
        //error catch
        set_error_handler('catch_error');

        if (isset($_POST['submit'])) {

            $result = import::check_MIME('article_prod', 'application/vnd.ms-excel');

            if ($result == 1) {

                $excel = import::load_file($_FILES['file_article_prod']['tmp_name']);

                $fields = array(0 => 'article_id',
                    1 => 'prod_id');

                $return_fields = import::check_fields($excel, $fields);

                if ($return_fields) {
                    //  Все проверки файла пройдены. Начало импорта.               
                    //  сообщение о строках без данных, повторяющемся url, ошибках 
                    $msg = array('main_fields' => '',
                        'error' => '');
                    //информационные сообщения о количестве импортированных и замененных данных
                    $info_msg = array('Связь статья-товар' => array('insert' => 0, 'update' => 0));

                    $excel->setActiveSheetIndex(0);
                    $main = $excel->getActiveSheet();
                    $max_row = $main->getHighestRow();
                    //в файле 2 столбца
                    $max_column = PHPExcel_Cell::columnIndexFromString($main->getHighestColumn());

                    //  обнуление старых значений
                    DB::query("TRUNCATE rk_articleprods");

                    // парсинг листа и иимпорт
                    for ($i = 2; $i <= $max_row; $i++) {
                        //проверка на ошибки
                        if (isset($GLOBALS['main_error'])) {
                            $msg['error'] = $GLOBALS['main_error'];
                            break;
                        }

                        //  импорт основных полей
                        $article_id = $main->getCellByColumnAndRow(0, $i)->getValue();
                        $prod_id = $main->getCellByColumnAndRow(1, $i)->getValue();

                        DB::query("INSERT INTO rk_articleprods (article_id, prod_id) VALUES ($article_id, $prod_id)");

                        $info_msg['Связь статья-товар'] = import::count_info($info_msg['Связь статья-товар']);

                    }

                    if (empty($msg['main_fields']) && empty($msg['error'])) {

                        //   add link prod-cat
                        optimization::article_cat();

                        $content = '<p>Связи статья-продукт успешно загружены.</p>' . import::report_msg($info_msg, 'info');
                        Template::set_page('index_success', 'Импорт связи статья-продукт', $content);

                    } else {

                        $content = import::report_msg($msg, 'error') . '<br><p>Загруженные связи статья-продукт.</p>' . import::report_msg($info_msg, 'info');
                        Template::set_page('index_info', 'Импорт связи статья-продукт', $content);
                    }


                } else {
                    Template::set_form(null, 'Не найдены все необходимые поля.');
                    Template::set_page('article_prod', 'Импорт связи статья-продукт', 'article_prod');
                }
            } else {
                Template::set_form(null, $result);
                Template::set_page('article_prod', 'Импорт связи статья-продукт', 'article_prod');
            }


        } else {
            Template::set_page('article_prod', 'Импорт связи статья-продукт', 'article_prod');
        }

    }

}

?>