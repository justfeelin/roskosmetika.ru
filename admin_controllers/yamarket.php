<?php

class Controller_yamarket extends Controller_base
{
    /**
     * View generate token page
     */
    function index($arg)
    {
        Template::set_page('yamarket', 'Обновить/получить токен API');
    }

    /**
     * Generate or view yandex market API token
     */
    public function token($args)
    {

        $client_id = env('YA_M_ID');
        $client_secret = env('YA_M_PSWR');

        $tokenFile = __DIR__ . '/../yatoken/.yandex_token';

        $msg = '';
        $token = null;

        if (file_exists($tokenFile)) {
            $tokenInfo = explode("\n", file_get_contents($tokenFile));

            if (
                isset($tokenInfo[1])
                && $tokenInfo[1] > $_SERVER['REQUEST_TIME'] + 300
            ) {
                $token = $tokenInfo[0];
            }
        }

        if(!$token)
        {

            if (!isset($_GET['code'])) {

                header('Location: https://oauth.yandex.ru/authorize?response_type=code&client_id=' . $client_id);
                exit(0);
            }

       
            if (isset($_GET['code'])) {

                $result = false;

                $params =  array(
                                'grant_type'=> 'authorization_code',
                                'code'=> $_GET['code'],
                                'client_id' => $client_id,
                                'client_secret' => $client_secret,
                            );

                $headers =  array(
                                'Content-type: application/x-www-form-urlencoded',
                            );


                $result = yamarket::ya_request('https://oauth.yandex.ru/token', $headers, $params);
             
            }


            if (!empty($result['access_token'])) {
                $token = $result['access_token'];

                file_put_contents($tokenFile, $token . "\n" . (time() + $result['expires_in']));

                $msg .=  'Токен успешно создан. (' . $token . ')';

            } else {
                $msg .=  'Ошибка создания токена.';
            }

        } else {
            $msg .= 'Токен актуален и уже создан (' . $token . ')';
        }

        Template::set_page('index_info', 'Обновить/получить токен API', $msg);
    }
}