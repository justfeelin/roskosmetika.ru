<?php

/**
 * Work with day items
 */
class Controller_day_item extends Controller_base
{

    /**
     * List of day itesm with next week items.
     * @param <type> $args URL params.
     */

    const PAGE_HEADER = 'Товары дня';

    function index($args)
    {
        Template::set_page('day_item', self::PAGE_HEADER, [
            'items' => day_items::items_to_add(),
        ]);
    }

    /**
     * Replace or add new day items
     * @param array $args URL params.
     */
    function save($args)
    {

        // ajax
        if (isset($_SERVER['HTTP_AJAX'])) {

            $begin = trim($_POST['begin']);
            $end = trim($_POST['end']);
            $prod_id = (int)($_POST['prod_id']);
            $sale = (int)($_POST['sale']);
            $tag = trim($_POST['tag']);

            //  check prod_id
            if ($prod_id == 0) $msg['prod_id'] = 'Нет ID товара';

            // check sale
            if ($sale == 0) $msg['sale'] = 'Не указана скидка';

            // add new
            if (!isset($msg)) {

                if (day_items::add_day_item($begin, $end, $prod_id, $sale, $tag)) {
                    $response = array(
                        'success' => true,
                        'msg' => 'Товар дня добавлен',
                        'errors' => false
                    );

                } else {
                    $response = array(
                        'success' => false,
                        'msg' => 'Сайт превращается в уточку - крякнул. Повторная попытка ',
                        'errors' => false
                    );
                }

            } // Выводим снова форму быстрого заказа
            else {
                $response = array(
                    'success' => false,
                    'msg' => 'Не ввели необходимые данные',
                    'errors' => $msg
                );
            }

            echo json_encode($response);
            exit();


        } else {
            // not ajax
            Template::set_page('service_info', self::PAGE_HEADER, 'Ушел AJAX. Возможно проблемы с кармой. Прочитайте молитву Кришне об избавлении от ложных гуру.');
        }
    }

}

?>