<?php

class Controller_newsletter_images extends Controller_base
{
    public function index($arg)
    {
        $date = '';

        $errors = [];

        if (!empty($_POST)) {
            $dir = null;

            $filesN = 0;

            if (
                empty($_POST['date'])
                || !preg_match('/^\d{4}-\d{2}-\d{2}$/', $_POST['date'])
            ) {
                $errors['date'] = 'Необходимо указать дату в правильном формате';
            } else {
                $date = $_POST['date'];

                $dir = $_SERVER['DOCUMENT_ROOT'] . '/images/mail/' . $date;
            }

            $upload = !empty($_FILES['images']['name'][0]);

            if ($upload) {
                if ($_FILES['images']['error'][0] === UPLOAD_ERR_NO_FILE) {
                    $errors['images'] = 'Необходимо выбрать файлы';
                } else {
                    for ($i = 0, $filesN = count($_FILES['images']['name']); $i < $filesN; ++$i) {
                        if ($_FILES['images']['error'][$i] !== UPLOAD_ERR_OK) {
                            $errors['images'] = 'Что-то не так с файлами';
                        } else {
                            if ($dir && file_exists($dir . '/' . $_FILES['images']['name'][$i])) {
                                $errors['images'] = 'Файл ' . $_FILES['images']['name'][$i] . ' уже существует';

                                break;
                            }
                        }
                    }
                }
            }

            if (!$errors) {
                if ($upload) {
                    if (!is_dir($dir)) {
                        mkdir($dir);
                    }

                    for ($i = 0; $i < $filesN; ++$i) {
                        if (is_uploaded_file($_FILES['images']['tmp_name'][$i])) {
                            move_uploaded_file($_FILES['images']['tmp_name'][$i], $dir . '/' . $_FILES['images']['name'][$i]);
                        }
                    }

                    $_SESSION['images_uploaded'] = $upload;
                }

                if (is_dir($dir)) {
                    $_SESSION['upload_images_date'] = $_POST['date'];
                } else {
                    $_SESSION['upload_images_date_error'] = $_POST['date'];
                }

                header('Location: ' . $_SERVER['REQUEST_URI']);

                die();
            }
        }

        if (isset($_SESSION['upload_images_date'])) {
            Template::set('uploaded_images', scandir($_SERVER['DOCUMENT_ROOT'] . '/images/mail/' . $_SESSION['upload_images_date']));
            Template::set('images_dir', $_SESSION['upload_images_date']);

            if (isset($_SESSION['images_uploaded'])) {
                unset($_SESSION['images_uploaded']);

                Template::set('success', true);
            }

            unset($_SESSION['upload_images_date']);
        }

        if (isset($_SESSION['upload_images_date_error'])) {
            Template::set('upload_dir_error', $_SESSION['upload_images_date_error']);

            unset($_SESSION['upload_images_date_error']);
        }

        Template::set('date', $date);
        Template::set('errors', $errors);
        Template::set_page('newsletter_images', 'Загрузка картинок для рассылки');
    }

    /**
     * @inheritdoc
     */
    protected function admin_access()
    {
        return admin::RIGHTS_COMMON | admin::RIGHTS_NEWSLETTER_IMAGES;
    }
}
