<?php

class Controller_Export extends Controller_Base
{

    /**
     * Выгрузка меню экспорта по умолчанию
     */
    function index($arg)
    {
        Template::set_page('menu_export', 'Экспорт таблиц');
    }

    /**
     * Disabled main products which contain child products
     */
    function main_products()
    {
        $inVisible = isset($_GET['invisible']);
        $notActive = isset($_GET['not_active']);

        if ($inVisible || $notActive) {
            $products = Product::get_main_disabled($inVisible, $notActive);
        } else {
            $products = null;
        }

        Template::set_page('main_products', 'Главные фасовки', [
            'invisible' => $inVisible,
            'not_active' => $notActive,
            'products' => $products,
        ]);
    }

    /**
     * Экспорт категорий по описаниям
     */
    function categories($arg)
    {

        //проверка типов данных для экспорта
        if (isset($_POST['all']) || isset($_POST['choise'])) {

            //дополнительные параметры выборки
            $param = '';

            if (isset($_POST['choise'])) {

                if (!empty($_POST['categoryID'])) $param['id'] = intval($_POST['categoryID']);
                if (isset($_POST['desc'])) $param[] = 'c_desc.desc';
                if (isset($_POST['synonym'])) $param[] = 'c_desc.synonym';
                if (isset($_POST['title'])) $param[] = 'seo.title';
                if (isset($_POST['seo_desc'])) $param[] = 'seo_desc';
                if (isset($_POST['keywords'])) $param[] = 'seo.keywords';
                if (isset($_POST['filters'])) $param[] = '(SELECT GROUP_CONCAT(cf.filter_id) FROM rk_cat_filters cf WHERE cf.cat_id = cat.id)';

            }
            //Выборка категорий
            $categories = categories::all_with_filters($param);
            if ($categories) {
                //открытие файла
                $excel = export::open_file('categories.xlsm');
                //выбор листа фильтров
                $excel->setActiveSheetIndex(1);
                $filtel_list = $excel->getActiveSheet();
                //получение фильтров
                $filters = filters::get_filters('cat');
                //экспорт фильтров
                foreach ($filters as $key => $filter) {
                    $filtel_list->setCellValue('A' . ($key + 2), $filter['id']);
                    $filtel_list->setCellValue('B' . ($key + 2), $filter['name']);
                }

                //выбор листа категорий
                $excel->setActiveSheetIndex(0);
                $categories_list = $excel->getActiveSheet();
                //экспорт категорий
                foreach ($categories as $key => $category) {
                    $categories_list->setCellValue('A' . ($key + 2), $category['id']);
                    $categories_list->setCellValue('B' . ($key + 2), export::anti_typograf($category['name']));
                    $categories_list->setCellValue('C' . ($key + 2), export::html_to_markdown($category['desc']));
                    $categories_list->setCellValue('D' . ($key + 2), $category['synonym']);
                    $categories_list->setCellValue('E' . ($key + 2), $category['url']);
                    $categories_list->setCellValue('F' . ($key + 2), $category['h1']);
                    $categories_list->setCellValue('G' . ($key + 2), $category['title']);
                    $categories_list->setCellValue('H' . ($key + 2), $category['seo_desc']);
                    $categories_list->setCellValue('I' . ($key + 2), $category['keywords']);
                    $categories_list->setCellValue('J' . ($key + 2), $category['filters']);
                }


                if (ob_get_level()) {
                    ob_end_clean();
                }

                export::download_excel($excel, 'categories' . date('_y.m.d_H-i_'), true);
            } else {
                Template::set_form(null, 'Нет категорий для данной выборки.');
                Template::set_page('cat_export', 'Экспорт категорий');
            }

        } else {
            Template::set_page('cat_export', 'Экспорт категорий', array(
                'categories' => Categories::get_categories(false, true),
            ));
        }

        Template::add_css('../assets/jquery-select2/select2.css');
        Template::add_css('../assets/jquery-select2/select2-bootstrap.css');

        Template::add_script('../assets/jquery-select2/select2.min.js');
        Template::add_script('../assets/jquery-select2/select2_locale_ru.js');
    }


    /**
     * Экспорт категорий по структуре
     */
    function cat_structure($arg)
    {

        if (!empty($arg[0]) && !empty($arg[1])) {

            $id = (int)$arg[0];
            $type = $arg[1];

            //Выборка категорий
            $categories = categories::cat_by_structure($id, $type);

            if ($categories) {
                $json['success'] = TRUE;
                echo json_encode($json);
                //открытие файла
                $excel = export::open_file('categories.xlsm');
                //выбор листа фильтров
                $excel->setActiveSheetIndex(1);
                $filtel_list = $excel->getActiveSheet();
                //получение фильтров
                $filters = filters::get_filters('cat');
                //экспорт фильтров
                foreach ($filters as $key => $filter) {
                    $filtel_list->setCellValue('A' . ($key + 2), $filter['id']);
                    $filtel_list->setCellValue('B' . ($key + 2), $filter['name']);
                }

                //выбор листа категорий
                $excel->setActiveSheetIndex(0);
                $categories_list = $excel->getActiveSheet();
                //экспорт категорий
                foreach ($categories as $key => $category) {
                    $categories_list->setCellValue('A' . ($key + 2), $category['id']);
                    $categories_list->setCellValue('B' . ($key + 2), export::anti_typograf($category['name']));
                    $categories_list->setCellValue('C' . ($key + 2), export::html_to_markdown($category['desc']));
                    $categories_list->setCellValue('D' . ($key + 2), $category['synonym']);
                    $categories_list->setCellValue('E' . ($key + 2), $category['url']);
                    $categories_list->setCellValue('F' . ($key + 2), $category['h1']);
                    $categories_list->setCellValue('G' . ($key + 2), $category['title']);
                    $categories_list->setCellValue('H' . ($key + 2), $category['seo_desc']);
                    $categories_list->setCellValue('I' . ($key + 2), $category['keywords']);
                    $categories_list->setCellValue('J' . ($key + 2), $category['filters']);
                }


                if (ob_get_level()) {
                    ob_end_clean();
                }

                export::download_excel($excel, 'categories' . date('_y.m.d_H-i_'), true);
            }

        }

    }


    /**
     * Экспорт продуктов
     */
    function products($arg)
    {

        //проверка типов данных для экспорта
        if (isset($_POST['all']) || isset($_POST['choise']) || isset($_POST['blank'])) {
            //открытие файла
            $excel = export::open_file('products.xlsm');
            //выбор листа товаров
            $excel->setActiveSheetIndex(0);
            $product_list = $excel->getActiveSheet();

            if (isset($_POST['choise'])) {
                //параметры выборки на вывод
                $param = FALSE;
                //выборка по id и tm
                if (isset($_POST['ids'])) $param[1]['id_line'] = $_POST['id_line'];
                if (isset($_POST['tms'])) $param[1]['tm'] = $_POST['tm'];

                //описания товаров
                if (isset($_POST['eng'])) $param[2]['eng'] = $_POST['eng'];
                if (isset($_POST['use'])) $param[2]['use'] = $_POST['use'];
                if (isset($_POST['made_of'])) $param[2]['made_of'] = $_POST['made_of'];
                if (isset($_POST['synonym'])) $param[2]['synonym'] = $_POST['synonym'];

                //Описания страниц
                if (isset($_POST['title'])) $param[3]['title'] = $_POST['title'];
                if (isset($_POST['seo_desc'])) $param[3]['seo_desc'] = $_POST['seo_desc'];
                if (isset($_POST['keywords'])) $param[3]['keywords'] = $_POST['keywords'];

                //Фотографии
                if (isset($_POST['alt'])) $param[4]['alt'] = $_POST['alt'];
                if (isset($_POST['photo_title'])) $param[4]['photo_title'] = $_POST['photo_title'];

                //Каталог
                if (isset($_POST['no_cat'])) $param[5]['no_cat'] = $_POST['no_cat'];
                if (isset($_POST['no_filters'])) $param[5]['no_filters'] = $_POST['no_filters'];
                if (isset($_POST['no_lines'])) $param[5]['no_lines'] = $_POST['no_lines'];
                if (isset($_POST['no_tm'])) $param[5]['no_tm'] = $_POST['no_tm'];
                if (isset($_POST['news'])) $param[5]['news'] = $_POST['news'];

                //дополнительно
                if (isset($_POST['no_pack'])) $param[6]['no_pack'] = $_POST['no_pack'];

                if (!empty($_POST['categories']) && is_array($_POST['categories'])) {
                    $param['categories'] = $_POST['categories'];
                }

                $products = product::get_export_products($param);

                //экспорт товаров
                foreach ($products as $key => $product) {
                    $descr_id = ($product['main_id'] == 0 ? '' : $product['main_id']);
                    $tm_id = ($product['tm_id'] == 0 ? '' : $product['tm_id']);
                    $desc = export::anti_typograf($product['description']);
                    $use = export::anti_typograf($product['use']);
                    $ingredients = export::anti_typograf($product['ingredients']);

                    $product_list->setCellValue('A' . ($key + 2), $product['id']);
                    $product_list->setCellValue('B' . ($key + 2), $descr_id);
                    $product_list->setCellValue('C' . ($key + 2), export::anti_typograf($product['name']));
                    $product_list->setCellValue('D' . ($key + 2), export::anti_typograf($product['pack']));
                    $product_list->setCellValue('E' . ($key + 2), export::anti_typograf($product['eng_name']));
                    $product_list->setCellValue('F' . ($key + 2), export::anti_typograf($product['short_description']));
                    $product_list->setCellValue('G' . ($key + 2), export::html_to_markdown($desc));
                    $product_list->setCellValue('H' . ($key + 2), export::html_to_markdown($use));
                    $product_list->setCellValue('I' . ($key + 2), export::html_to_markdown($ingredients));
                    $product_list->setCellValue('J' . ($key + 2), $product['synonym']);
                    $product_list->setCellValue('K' . ($key + 2), $product['url']);
                    $product_list->setCellValue('L' . ($key + 2), $product['title']);
                    $product_list->setCellValue('M' . ($key + 2), $product['seo_desc']);
                    $product_list->setCellValue('N' . ($key + 2), $product['keywords']);
                    $product_list->setCellValue('O' . ($key + 2), $product['categories']);
                    $product_list->setCellValue('P' . ($key + 2), $product['filters']);
                    $product_list->setCellValue('Q' . ($key + 2), $product['lines']);
                    $product_list->setCellValue('R' . ($key + 2), $tm_id);
                    $product_list->setCellValue('S' . ($key + 2), $product['sets']);
                    $product_list->setCellValue('T' . ($key + 2), $product['new']);
                    $product_list->setCellValue('U' . ($key + 2), $product['src']);
                    $product_list->setCellValue('V' . ($key + 2), $product['alt']);
                    $product_list->setCellValue('W' . ($key + 2), $product['photo_title']);
                    $product_list->setCellValue('X' . ($key + 2), $product['visible'] ? 1 : 0);
                }

            } elseif (isset($_POST['all'])) {
                $products = product::get_export_products(FALSE);
                //экспорт товаров
                foreach ($products as $key => $product) {
                    $descr_id = ($product['main_id'] == 0 ? '' : $product['main_id']);
                    $tm_id = ($product['tm_id'] == 0 ? '' : $product['tm_id']);
                    $desc = export::anti_typograf($product['desc']);
                    $use = export::anti_typograf($product['use']);
                    $ingredients = export::anti_typograf($product['ingredients']);

                    $product_list->setCellValue('A' . ($key + 2), $product['id']);
                    $product_list->setCellValue('B' . ($key + 2), $descr_id);
                    $product_list->setCellValue('C' . ($key + 2), export::anti_typograf($product['name']));
                    $product_list->setCellValue('D' . ($key + 2), export::anti_typograf($product['pack']));
                    $product_list->setCellValue('E' . ($key + 2), export::anti_typograf($product['eng_name']));
                    $product_list->setCellValue('F' . ($key + 2), export::anti_typograf($product['short_description']));
                    $product_list->setCellValue('G' . ($key + 2), export::html_to_markdown($product['description']));
                    $product_list->setCellValue('H' . ($key + 2), export::html_to_markdown($product['use']));
                    $product_list->setCellValue('I' . ($key + 2), export::html_to_markdown($product['ingredients']));
                    $product_list->setCellValue('J' . ($key + 2), $product['synonym']);
                    $product_list->setCellValue('K' . ($key + 2), $product['url']);
                    $product_list->setCellValue('L' . ($key + 2), $product['title']);
                    $product_list->setCellValue('M' . ($key + 2), $product['seo_desc']);
                    $product_list->setCellValue('N' . ($key + 2), $product['keywords']);
                    $product_list->setCellValue('O' . ($key + 2), $product['categories']);
                    $product_list->setCellValue('P' . ($key + 2), $product['filters']);
                    $product_list->setCellValue('Q' . ($key + 2), $product['lines']);
                    $product_list->setCellValue('R' . ($key + 2), $tm_id);
                    $product_list->setCellValue('S' . ($key + 2), $product['sets']);
                    $product_list->setCellValue('T' . ($key + 2), $product['new']);
                    $product_list->setCellValue('U' . ($key + 2), $product['src']);
                    $product_list->setCellValue('V' . ($key + 2), $product['alt']);
                    $product_list->setCellValue('W' . ($key + 2), $product['photo_title']);
                    $product_list->setCellValue('X' . ($key + 2), $product['visible'] ? 1 : 0);
                }
            }


            //выбор листа фильтров
            $excel->setActiveSheetIndex(1);
            $categories_list = $excel->getActiveSheet();

            //Генерация листа категорий
            $categories = Categories::get_categories();
            $cursor = 2;

            foreach ($categories['lvl_1'] as $category_1) {
                //++$cursor;

                $categories_list->setCellValue('A' . $cursor, $category_1['id']);
                $categories_list->setCellValue('B' . $cursor, $category_1['name']);


                if (isset($categories['lvl_2'][$category_1['id']])) {
                    $cursor_group_1 = $cursor;
                    foreach ($categories['lvl_2'][$category_1['id']] as $group_1) {
                        if ($cursor_group_1 != $cursor) {
                            $categories_list->setCellValue('A' . $cursor, $category_1['id']);
                            $categories_list->setCellValue('B' . $cursor, '');
                        }

                        $categories_list->setCellValue('C' . $cursor, $group_1['id']);
                        $categories_list->setCellValue('D' . $cursor, $group_1['name']);

                        $cursor_cat_2 = $cursor;
                        foreach ($group_1['cats'] as $category_2) {

                            if ($cursor_cat_2 != $cursor) {
                                $categories_list->setCellValue('A' . $cursor, $category_1['id']);
                                $categories_list->setCellValue('B' . $cursor, '');
                                $categories_list->setCellValue('C' . $cursor, $group_1['id']);
                                $categories_list->setCellValue('D' . $cursor, '');
                            }

                            $categories_list->setCellValue('E' . $cursor, $category_2['id']);
                            $categories_list->setCellValue('F' . $cursor, $category_2['name']);

                            if (isset($categories['lvl_3'][$category_2['id']])) {
                                $cursor_group_2 = $cursor;
                                foreach ($categories['lvl_3'][$category_2['id']] as $group_2) {

                                    if ($cursor_group_2 != $cursor) {
                                        $categories_list->setCellValue('A' . $cursor, $category_1['id']);
                                        $categories_list->setCellValue('B' . $cursor, '');
                                        $categories_list->setCellValue('C' . $cursor, $group_1['id']);
                                        $categories_list->setCellValue('D' . $cursor, '');
                                        $categories_list->setCellValue('E' . $cursor, $category_2['id']);
                                        $categories_list->setCellValue('F' . $cursor, '');
                                    }

                                    $categories_list->setCellValue('G' . $cursor, $group_2['id']);
                                    $categories_list->setCellValue('H' . $cursor, $group_2['name']);

                                    $cursor_cat_3 = $cursor;
                                    foreach ($group_2['cats'] as $category_3) {

                                        if ($cursor_cat_3 != $cursor) {

                                            $categories_list->setCellValue('A' . $cursor, $category_1['id']);
                                            $categories_list->setCellValue('B' . $cursor, '');
                                            $categories_list->setCellValue('C' . $cursor, $group_1['id']);
                                            $categories_list->setCellValue('D' . $cursor, '');
                                            $categories_list->setCellValue('E' . $cursor, $category_2['id']);
                                            $categories_list->setCellValue('F' . $cursor, '');
                                            $categories_list->setCellValue('G' . $cursor, $group_2['id']);
                                            $categories_list->setCellValue('H' . $cursor, '');
                                        }

                                        $categories_list->setCellValue('I' . $cursor, $category_3['id']);
                                        $categories_list->setCellValue('J' . $cursor, $category_3['name']);

                                        ++$cursor;
                                    }
                                }
                            } else {
                                ++$cursor;
                            }

                        }

                    }

                } else {
                    ++$cursor;
                }

            }

            //Генерация листа фильтров
            $excel->setActiveSheetIndex(2);
            $filter_list = $excel->getActiveSheet();

            $filters = filters::get_filters('prod');

            $cursor = 2;
            foreach ($filters as $main_filter) {

                $filter_list->setCellValue('A' . $cursor, $main_filter['id']);
                $filter_list->setCellValue('B' . $cursor, $main_filter['name']);

                if (isset($main_filter['filters']) || !empty($main_filter['filters'])) {
                    $cursor_filter = $cursor;
                    foreach ($main_filter['filters'] as $filter) {
                        if ($cursor_filter != $cursor) {

                            $filter_list->setCellValue('A' . $cursor, $main_filter['id']);
                            $filter_list->setCellValue('B' . $cursor, '');
                        }

                        $filter_list->setCellValue('C' . $cursor, $filter['id']);
                        $filter_list->setCellValue('D' . $cursor, $filter['name']);
                        ++$cursor;
                    }
                } else {
                    ++$cursor;
                }

            }

            //Генерация наборов
            $excel->setActiveSheetIndex(3);
            $sets_list = $excel->getActiveSheet();

            $sets = sets::get_sets(FALSE, FALSE, FALSE);

            foreach ($sets as $set_key => $set) {
                $sets_list->setCellValue('A' . ($set_key + 2), $set['id']);
                $sets_list->setCellValue('B' . ($set_key + 2), $set['name']);
            }

            //Генерация линеек
            $excel->setActiveSheetIndex(4);
            $lines_list = $excel->getActiveSheet();

            $lines = lines::get_lines(FALSE, FALSE, FALSE);

            foreach ($lines as $line_key => $line) {
                $lines_list->setCellValue('A' . ($line_key + 2), $line['id']);
                $lines_list->setCellValue('B' . ($line_key + 2), $line['name']);
            }

            //генерация торговых марок
            $excel->setActiveSheetIndex(6);
            $tm_list = $excel->getActiveSheet();

            $tms = tm::get_list(FALSE, FALSE, FALSE);

            foreach ($tms as $tm_key => $tm) {
                $tm_list->setCellValue('A' . ($tm_key + 2), $tm['id']);
                $tm_list->setCellValue('B' . ($tm_key + 2), $tm['name']);
            }


            if (!isset($_POST['blank']) && isset($products) && empty($products)) {
                Template::set_form(null, 'Не найдены товары для этой выборки.');
                Template::set_page('prod_export', 'Экспорт товаров', [
                    'tm' => tm::get_list(false, false, false),
                ]);
            } else {

                if (ob_get_level()) {
                    ob_end_clean();
                }

                export::download_excel($excel, 'products' . date('_y.m.d_H-i_'), true);
            }


        } else {
            Template::set_page('prod_export', 'Экспорт товаров', [
                'tm' => tm::get_list(false, false, false),
                'categories' => Categories::get_categories(false, true),
            ]);
        }
    }


    /**
     * Экспорт линеек товаров
     */
    function lines($arg)
    {

        //проверка типов данных для экспорта
        if (isset($_POST['all']) || isset($_POST['choise']) || isset($_POST['blank'])) {

            //открытие файла
            $excel = export::open_file('lines.xlsm');

            // подготовка файла импорта генерация торговых марок
            $excel->setActiveSheetIndex(1);
            $tm_list = $excel->getActiveSheet();

            $tms = tm::get_list(FALSE, FALSE, FALSE);

            foreach ($tms as $tm_key => $tm) {
                $tm_list->setCellValue('A' . ($tm_key + 2), $tm['id']);
                $tm_list->setCellValue('B' . ($tm_key + 2), $tm['name']);
            }

            //выбор листа линеек
            $excel->setActiveSheetIndex(0);
            $lines_list = $excel->getActiveSheet();


            if (isset($_POST['choise'])) {
                //дополнительные параметры выборки
                $param = '';

                if (isset($_POST['synonym'])) $param[] = 'ld.synonym';
                if (isset($_POST['title'])) $param[] = 'ls.title';
                if (isset($_POST['seo_desc'])) $param[] = 'ls.description';
                if (isset($_POST['keywords'])) $param[] = 'ls.keywords';
                if (isset($_POST['src'])) $param[] = 'lp.src';
                if (isset($_POST['alt'])) $param[] = 'lp.alt';
                if (isset($_POST['photo_title'])) $param[] = 'lp.title';

                //Выборка линеек
                $lines = lines::get_export_lines($param);
                //экспорт линеек
                foreach ($lines as $key => $line) {
                    $desc = export::anti_typograf($line['desc']);
                    $src = ($line['src'] == 'none.jpg' ? '' : $line['src']);

                    $lines_list->setCellValue('A' . ($key + 2), $line['id']);
                    $lines_list->setCellValue('B' . ($key + 2), ($line['tm_id'] ? $line['tm_id'] : ''));
                    $lines_list->setCellValue('C' . ($key + 2), export::anti_typograf($line['name']));
                    $lines_list->setCellValue('D' . ($key + 2), export::anti_typograf($line['short_desc']));
                    $lines_list->setCellValue('E' . ($key + 2), export::html_to_markdown($desc));
                    $lines_list->setCellValue('F' . ($key + 2), $line['synonym']);
                    $lines_list->setCellValue('G' . ($key + 2), $line['url']);
                    $lines_list->setCellValue('H' . ($key + 2), $line['h1']);
                    $lines_list->setCellValue('I' . ($key + 2), $line['title']);
                    $lines_list->setCellValue('J' . ($key + 2), $line['seo_desc']);
                    $lines_list->setCellValue('K' . ($key + 2), $line['keywords']);
                    $lines_list->setCellValue('L' . ($key + 2), $src);
                    $lines_list->setCellValue('M' . ($key + 2), $line['alt']);
                    $lines_list->setCellValue('N' . ($key + 2), $line['photo_title']);
                    $lines_list->setCellValue('O' . ($key + 2), $line['alt_name']);
                }


            } elseif (isset($_POST['all'])) {
                //Выборка  всех линеек
                $lines = lines::get_lines();

                foreach ($lines as $key => $line) {
                    $desc = export::anti_typograf($line['desc']);
                    $src = ($line['src'] == 'none.jpg' ? '' : $line['src']);

                    $lines_list->setCellValue('A' . ($key + 2), $line['id']);
                    $lines_list->setCellValue('B' . ($key + 2), ($line['tm_id'] ? $line['tm_id'] : ''));
                    $lines_list->setCellValue('C' . ($key + 2), export::anti_typograf($line['name']));
                    $lines_list->setCellValue('D' . ($key + 2), export::anti_typograf($line['short_desc']));
                    $lines_list->setCellValue('E' . ($key + 2), export::html_to_markdown($desc));
                    $lines_list->setCellValue('F' . ($key + 2), $line['synonym']);
                    $lines_list->setCellValue('G' . ($key + 2), $line['url']);
                    $lines_list->setCellValue('H' . ($key + 2), $line['h1']);
                    $lines_list->setCellValue('I' . ($key + 2), $line['title']);
                    $lines_list->setCellValue('J' . ($key + 2), $line['seo_desc']);
                    $lines_list->setCellValue('K' . ($key + 2), $line['keywords']);
                    $lines_list->setCellValue('L' . ($key + 2), $src);
                    $lines_list->setCellValue('M' . ($key + 2), $line['alt']);
                    $lines_list->setCellValue('N' . ($key + 2), $line['photo_title']);
                    $lines_list->setCellValue('O' . ($key + 2), $line['alt_name']);
                }
            }


            if (!isset($_POST['blank']) && isset($lines) && empty($lines)) {
                Template::set_form(null, 'Не найдены линейки для этой выборки.');
                Template::set_page('line_export', 'Экспорт линеек');
            } else {

                if (ob_get_level()) {
                    ob_end_clean();
                }

                export::download_excel($excel, 'lines' . date('_y.m.d_H-i_'), true);
            }
        } else {
            Template::set_page('line_export', 'Экспорт линеек');
        }

    }

    /**
     * Экспорт наборов товаров
     */
    function sets($arg)
    {

        //проверка типов данных для экспорта
        if (isset($_POST['all']) || isset($_POST['choise']) || isset($_POST['blank'])) {

            //открытие файла
            $excel = export::open_file('sets.xlsm');

            //выбор листа наборов
            $excel->setActiveSheetIndex(0);
            $sets_list = $excel->getActiveSheet();


            if (isset($_POST['choise'])) {
                //дополнительные параметры выборки
                $param = '';

                if (isset($_POST['synonym'])) $param[] = 'sd.synonym';
                if (isset($_POST['title'])) $param[] = 'ss.title';
                if (isset($_POST['seo_desc'])) $param[] = 'ss.description';
                if (isset($_POST['keywords'])) $param[] = 'ss.keywords';
                if (isset($_POST['src'])) $param[] = 'sp.src';
                if (isset($_POST['alt'])) $param[] = 'sp.alt';
                if (isset($_POST['photo_title'])) $param[] = 'sp.title';

                //Выборка линеек
                $sets = sets::get_export_sets($param);
                //экспорт линеек
                foreach ($sets as $key => $set) {
                    $desc = export::anti_typograf($set['desc']);

                    $sets_list->setCellValue('A' . ($key + 2), $set['id']);
                    $sets_list->setCellValue('B' . ($key + 2), export::anti_typograf($set['name']));
                    $sets_list->setCellValue('C' . ($key + 2), export::anti_typograf($set['short_desc']));
                    $sets_list->setCellValue('D' . ($key + 2), export::html_to_markdown($desc));
                    $sets_list->setCellValue('E' . ($key + 2), $set['synonym']);
                    $sets_list->setCellValue('F' . ($key + 2), $set['url']);
                    $sets_list->setCellValue('G' . ($key + 2), $set['title']);
                    $sets_list->setCellValue('H' . ($key + 2), $set['seo_desc']);
                    $sets_list->setCellValue('I' . ($key + 2), $set['keywords']);
                    $sets_list->setCellValue('J' . ($key + 2), $set['src']);
                    $sets_list->setCellValue('K' . ($key + 2), $set['alt']);
                    $sets_list->setCellValue('L' . ($key + 2), $set['photo_title']);
                }


            } elseif (isset($_POST['all'])) {
                //Выборка  всех линеек
                $sets = sets::get_sets();

                foreach ($sets as $key => $set) {
                    $desc = export::anti_typograf($set['desc']);
                    $sets_list->setCellValue('A' . ($key + 2), $set['id']);
                    $sets_list->setCellValue('B' . ($key + 2), export::anti_typograf($set['name']));
                    $sets_list->setCellValue('C' . ($key + 2), export::anti_typograf($set['short_desc']));
                    $sets_list->setCellValue('D' . ($key + 2), export::html_to_markdown($desc));
                    $sets_list->setCellValue('E' . ($key + 2), $set['synonym']);
                    $sets_list->setCellValue('F' . ($key + 2), $set['url']);
                    $sets_list->setCellValue('G' . ($key + 2), $set['title']);
                    $sets_list->setCellValue('H' . ($key + 2), $set['seo_desc']);
                    $sets_list->setCellValue('I' . ($key + 2), $set['keywords']);
                    $sets_list->setCellValue('J' . ($key + 2), $set['src']);
                    $sets_list->setCellValue('K' . ($key + 2), $set['alt']);
                    $sets_list->setCellValue('L' . ($key + 2), $set['photo_title']);
                }
            }


            if (!isset($_POST['blank']) && isset($sets) && empty($sets)) {
                Template::set_form(null, 'Не найдены наборы для этой выборки.');
                Template::set_page('sets_export', 'Экспорт наборов');
            } else {

                if (ob_get_level()) {
                    ob_end_clean();
                }


                export::download_excel($excel, 'sets' . date('_y.m.d_H-i_'), true);
            }
        } else {
            Template::set_page('sets_export', 'Экспорт наборов');
        }

    }


    /**
     * Экспорт фильтров товаров
     */
    function filters($arg)
    {

        //открытие файла
        $excel = export::open_file('filters.xls');
        //выбор листа фильтров
        $excel->setActiveSheetIndex(0);
        $filters_list = $excel->getActiveSheet();
        //получение фильтров
        $filters = filters::get_filters('prod');

        $line = 2;
        foreach ($filters as $filter) {
            $filters_list->setCellValue('A' . $line, $filter['id']);
            $filters_list->setCellValue('B' . $line, $filter['name']);
            $filters_list->setCellValue('C' . $line, $filter['parent_id']);
            $filters_list->setCellValue('D' . $line, $filter['visible']);
            ++$line;

            if (!empty($filter['filters'])) {
                foreach ($filter['filters'] as $child) {
                    $filters_list->setCellValue('A' . $line, $child['id']);
                    $filters_list->setCellValue('B' . $line, $child['name']);
                    $filters_list->setCellValue('C' . $line, $child['parent_id']);
                    $filters_list->setCellValue('D' . $line, $child['visible']);
                    ++$line;
                }
            }


        }

        if (ob_get_level()) {
            ob_end_clean();
        }

        export::download_excel($excel, 'filters' . date('_y.m.d_H-i_'));
    }


    /**
     * Экспорт торговых марок
     */
    function tm($arg)
    {
        //проверка типов данных для экспорта
        if (isset($_POST['all']) || isset($_POST['choise']) || isset($_POST['blank'])) {

            //открытие файла
            $excel = export::open_file('tm.xlsm');


            //   Заполнение листа справочника стран
            $excel->setActiveSheetIndex(1);
            $countries_list = $excel->getActiveSheet();

            $countries = tm::list_of_countries();

            foreach ($countries as $c_key => $country) {
                $countries_list->setCellValue('A' . ($c_key + 2), $country['id']);
                $countries_list->setCellValue('B' . ($c_key + 2), $country['name']);
            }


            //выбор листа брендов
            $excel->setActiveSheetIndex(0);
            $tms_list = $excel->getActiveSheet();


            if (isset($_POST['choise'])) {

                if (isset($_POST['tms']) && isset($_POST['tm'])) {
                    //дополнительные параметры выборки
                    $tm_id = $_POST['tm'];

                    $tm = tm::get_export_tm($tm_id);
                    //экспорт торговой марки

                    $desc = export::anti_typograf($tm['desc']);
                    $src = ($tm['src'] == 'none.jpg' ? '' : $tm['src']);

                    $tms_list->setCellValue('A' . (2), $tm['id']);
                    $tms_list->setCellValue('B' . (2), export::anti_typograf($tm['name']));
                    $tms_list->setCellValue('C' . (2), $tm['country_id']);
                    $tms_list->setCellValue('D' . (2), export::anti_typograf($tm['short_desc']));
                    $tms_list->setCellValue('E' . (2), export::html_to_markdown($desc));
                    $tms_list->setCellValue('F' . (2), $tm['synonym']);
                    $tms_list->setCellValue('G' . (2), $tm['url']);
                    $tms_list->setCellValue('H' . (2), $tm['title']);
                    $tms_list->setCellValue('I' . (2), $tm['seo_desc']);
                    $tms_list->setCellValue('J' . (2), $tm['keywords']);
                    $tms_list->setCellValue('K' . (2), $src);
                    $tms_list->setCellValue('L' . (2), $tm['alt']);
                    $tms_list->setCellValue('M' . (2), $tm['photo_title']);
                    $tms_list->setCellValue('N' . (2), $tm['alt_name']);

                }
            } elseif (isset($_POST['all'])) {
                //Выборка  всех торговых марок
                $tms = tm::get_list();

                foreach ($tms as $key => $tm) {
                    $desc = export::anti_typograf($tm['desc']);
                    $src = ($tm['src'] == 'none.jpg' ? '' : $tm['src']);

                    $tms_list->setCellValue('A' . ($key + 2), $tm['id']);
                    $tms_list->setCellValue('B' . ($key + 2), export::anti_typograf($tm['name']));
                    $tms_list->setCellValue('C' . ($key + 2), $tm['country_id']);
                    $tms_list->setCellValue('D' . ($key + 2), export::anti_typograf($tm['short_desc']));
                    $tms_list->setCellValue('E' . ($key + 2), export::html_to_markdown($desc));
                    $tms_list->setCellValue('F' . ($key + 2), $tm['synonym']);
                    $tms_list->setCellValue('G' . ($key + 2), $tm['url']);
                    $tms_list->setCellValue('H' . ($key + 2), $tm['title']);
                    $tms_list->setCellValue('I' . ($key + 2), $tm['seo_desc']);
                    $tms_list->setCellValue('J' . ($key + 2), $tm['keywords']);
                    $tms_list->setCellValue('K' . ($key + 2), $src);
                    $tms_list->setCellValue('L' . ($key + 2), $tm['alt']);
                    $tms_list->setCellValue('M' . ($key + 2), $tm['photo_title']);
                    $tms_list->setCellValue('N' . ($key + 2), $tm['alt_name']);
                }

            }

            if (ob_get_level()) {
                ob_end_clean();
            }


            if (!isset($_POST['blank']) && isset($tms) && empty($tms)) {
                Template::set_form(null, 'Не найдены торговые марки для этой выборки.');
                Template::set_page('tm_export', 'Экспорт торговых марок');
            } else {
                export::download_excel($excel, 'tm' . date('_y.m.d_H-i_'), true);
            }
        } else {
            Template::set_page('tm_export', 'Экспорт торговых марок', [
                'tm' => tm::get_list(false, false, false),
            ]);
        }

    }


    /**
     * Экспорт фильтров товаров
     */
    function groups($arg)
    {

        //проверка типов данных для экспорта
        if (isset($_POST['all']) || isset($_POST['choise'])) {

            //открытие файла
            $excel = export::open_file('tm.xlsm');
            //выбор листа фильтров
            $excel->setActiveSheetIndex(0);
            $tms_list = $excel->getActiveSheet();


            if (isset($_POST['choise'])) {
                //дополнительные параметры выборки
                $param = '';

                if (isset($_POST['level_1'])) $param[] = 'td.synonym';
                if (isset($_POST['level_2'])) $param[] = 'td.country';


                //Выборка торговых марок
                $tms = tm::get_export_tm($param);
                //экспорт торговых марок
                foreach ($tms as $key => $tm) {
                    $desc = export::anti_typograf($tm['desc']);
                    $src = ($product['src'] == 'none.jpg' ? '' : $product['src']);

                    $tms_list->setCellValue('A' . ($key + 2), $tm['id']);
                    $tms_list->setCellValue('B' . ($key + 2), export::anti_typograf($tm['name']));
                    $tms_list->setCellValue('C' . ($key + 2), export::anti_typograf($tm['country']));
                    $tms_list->setCellValue('D' . ($key + 2), export::anti_typograf($tm['short_desc']));
                    $tms_list->setCellValue('E' . ($key + 2), export::html_to_markdown($desc));
                    $tms_list->setCellValue('F' . ($key + 2), $tm['synonym']);
                    $tms_list->setCellValue('G' . ($key + 2), $tm['url']);
                    $tms_list->setCellValue('H' . ($key + 2), $tm['title']);
                    $tms_list->setCellValue('I' . ($key + 2), $tm['seo_desc']);
                    $tms_list->setCellValue('J' . ($key + 2), $tm['keywords']);
                    $tms_list->setCellValue('K' . ($key + 2), $src);
                    $tms_list->setCellValue('L' . ($key + 2), $tm['alt']);
                    $tms_list->setCellValue('M' . ($key + 2), $tm['photo_title']);
                }


            } elseif (isset($_POST['all'])) {
                //Выборка  всех торговых марок
                $tms = tm::get_list();

                foreach ($tms as $key => $tm) {
                    $desc = export::anti_typograf($tm['desc']);
                    $src = ($product['src'] == 'none.jpg' ? '' : $product['src']);

                    $tms_list->setCellValue('A' . ($key + 2), $tm['id']);
                    $tms_list->setCellValue('B' . ($key + 2), export::anti_typograf($tm['name']));
                    $tms_list->setCellValue('C' . ($key + 2), export::anti_typograf($tm['country']));
                    $tms_list->setCellValue('D' . ($key + 2), export::anti_typograf($tm['short_desc']));
                    $tms_list->setCellValue('E' . ($key + 2), export::html_to_markdown($desc));
                    $tms_list->setCellValue('F' . ($key + 2), $tm['synonym']);
                    $tms_list->setCellValue('G' . ($key + 2), $tm['url']);
                    $tms_list->setCellValue('H' . ($key + 2), $tm['title']);
                    $tms_list->setCellValue('I' . ($key + 2), $tm['seo_desc']);
                    $tms_list->setCellValue('J' . ($key + 2), $tm['keywords']);
                    $tms_list->setCellValue('K' . ($key + 2), $src);
                    $tms_list->setCellValue('L' . ($key + 2), $tm['alt']);
                    $tms_list->setCellValue('M' . ($key + 2), $tm['photo_title']);
                }

            }

            if (ob_get_level()) {
                ob_end_clean();
            }


            if (!isset($_POST['blank']) && isset($tms) && empty($tms)) {
                Template::set_form(null, 'Не найдены торговые марки для этой выборки.');
                Template::set_page('tm_export', 'Экспорт торговых марок');
            } else {
                export::download_excel($excel, 'tm' . date('_y.m.d_H-i_'), true);
            }
        } else {
            Template::set_page('group_export', 'Экспорт групп категорий');
        }

    }


    /**
     * Export trade marks in categories
     */
    function tm_in_cat($arg)
    {

        if (isset($_POST['choise']) && isset($_POST['tm'])) {

            //  open file
            $excel = export::open_file('tm_in_cat.xls');
            //  choice list
            $excel->setActiveSheetIndex(0);
            $tm_list = $excel->getActiveSheet();

            $tm_id = (int)$_POST['tm'];
            $data_fom_base = tm_in_cat::get_by_tm($tm_id);

            if (!empty($data_fom_base)) {

                $line = 2;

                for ($i=0; $i < count($data_fom_base); $i++) { 
                    $tm_list->setCellValue('A' . $line, $data_fom_base[$i]['tm_id']);
                    $tm_list->setCellValue('B' . $line, $data_fom_base[$i]['cat_id']);
                    $tm_list->setCellValue('C' . $line, $data_fom_base[$i]['h1']);
                    $tm_list->setCellValue('D' . $line, $data_fom_base[$i]['title']);
                    $tm_list->setCellValue('E' . $line, $data_fom_base[$i]['description']);
                    $tm_list->setCellValue('F' . $line, $data_fom_base[$i]['keywords']);
                    $tm_list->setCellValue('G' . $line, export::html_to_markdown($data_fom_base[$i]['full_desc']));
                    $tm_list->setCellValue('H' . $line, $data_fom_base[$i]['help']);
                    ++$line;
                }

                if (ob_get_level()) {
                    ob_end_clean();
                }

                export::download_excel($excel, 'tm_in_cat' . date('_y.m.d_H-i_'));

            } else {
                Template::set_form(null, 'Что-то пошло не так, попробуте повторить.');
                Template::set_page('tm_in_cat_export', 'Экспорт торговых марок', [
                    'items' => tm::get_list(false, false, false),
                ]);
            }


        } else {
            Template::set_page('tm_in_cat_export', 'Экспорт торговых марок', [
                'items' => tm::get_list(false, false, false),
            ]);
        }
    }

    /**
     * Returns products without photos
     *
     * @throws Exception
     */
    public function prods_no_photos()
    {
        $excel = export::create_new_file();

        $sheet = $excel->getSheet(0);

        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);

        $sheet->setCellValue('A1', 'ID товара');
        $sheet->setCellValue('B1', 'Бренд');
        $sheet->setCellValue('C1', 'Название товара');
        $sheet->setCellValue('D1', 'URL');

        $line = 2;

        DB::get_rows(
            'SELECT p.id, t.name AS tm, pr.name, IF(pr.url != "", pr.url, pr.id) AS url
                FROM products AS p
                INNER JOIN products AS pr ON pr.id = IF(p.main_id > 0, p.main_id, p.id)
                INNER JOIN tm AS t ON t.id = pr.tm_id
                INNER JOIN rk_prod_photo AS pp ON pp.id = p.id AND pp.src = "none.jpg"',
            function ($product) use (&$sheet, &$line) {
                $sheet->setCellValue('A' . $line, $product['id']);
                $sheet->setCellValue('B' . $line, $product['tm']);
                $sheet->setCellValue('C' . $line, html_entity_decode($product['name'], ENT_QUOTES, 'UTF-8'));
                $sheet->setCellValue('D' . $line, DOMAIN_FULL . '/product/' . $product['url']);

                ++$line;
            }
        );

        export::download_excel($excel, 'prods_no_photo_' . date('y.m.d_H-i_'));
    }

    /**
     * Search wrong photo and find mistakes
     * @return file xls file
     */
    public function wrong_photo() {

        // FTP connection
        $conn_id = ftp_connect(env('PHOTO_FTP_SERVER')) or die("Не удалось установить соединение с $ftp_server"); 

        // try to enter
        if (@ftp_login($conn_id, env('PHOTO_FTP_USER'), env('PHOTO_FTP_PASSWORD'))) {

            ftp_chdir($conn_id, "/roscosmetika.ru/www/images/prod_photo/");

            $files = ftp_nlist($conn_id, ".");

            DB::query('TRUNCATE TABLE __prod_images_map');

            for ($i=0; $i <count($files) ; $i++) { 
                if ($files[$i] !== '.' && $files[$i] !== '..') {

                    DB::query("INSERT INTO __prod_images_map (file_name) VALUES ('" . DB::mysql_secure_string($files[$i]) . "')");
                }
            }

            ftp_close($conn_id); 

        }

        $high_register = DB::get_rows('SELECT * FROM __prod_images_map WHERE file_name LIKE BINARY "%.JPG%"');
        $error_files = DB::get_rows('SELECT * FROM __prod_images_map WHERE file_name NOT LIKE "%.jpg%"');
        $not_found = array();

        $products = DB::get_rows("SELECT p.id, IF(p.main_id > 0,(SELECT pp.name FROM products pp WHERE pp.id = p.main_id),p.name) AS name, tm.name AS tm_name, p.url, pp.src, pp.pic_quant 
                                  FROM products p LEFT JOIN rk_prod_photo pp ON (pp.id = p.id) 
                                                  LEFT JOIN tm ON (tm.id = p.tm_id)
                                  WHERE p.active = 1
                                  AND   p.id NOT IN(5555)
                                  AND   pp.src !='none.jpg'");


        for ($i=0; $i < count($products); $i++) { 
           if($products[$i]['pic_quant'] > 0){
                if(images::check_photo('s_' . $products[$i]['id'] . '.jpg') == 0) {
                    $products[$i]['file'] = 's_' . $products[$i]['id'] . '.jpg';
                    $not_found[] = $products[$i];
                }
                if(images::check_photo('min_' . $products[$i]['id'] . '.jpg') == 0) {
                    $products[$i]['file'] = 'min_' . $products[$i]['id'] . '.jpg';
                    $not_found[] = $products[$i];
                }

                for ($j=1; $j <= (int)$products[$i]['pic_quant']; $j++) { 

                    if(images::check_photo($products[$i]['id'] . '_' . $j . '.jpg') == 0) {
                        $products[$i]['file'] = $products[$i]['id'] . '_' . $j . '.jpg'; 
                        $not_found[] = $products[$i];
                    }
                    if(images::check_photo($products[$i]['id'] . '_' . $j . '_pre.jpg') == 0) {
                        $products[$i]['file'] = $products[$i]['id'] . '_' . $j . '_pre.jpg';
                        $not_found[] = $products[$i];
                    }
                    if(images::check_photo($products[$i]['id'] . '_' . $j . '_max.jpg') == 0) {
                        $products[$i]['file'] = $products[$i]['id'] . '_' . $j . '_max.jpg';
                        $not_found[] = $products[$i];
                    }
                }


           } else {

                if(images::check_photo($products[$i]['id'] . '.jpg') == 0) {
                    $products[$i]['file'] = $products[$i]['id'] . '.jpg';
                    $not_found[] = $products[$i];
                }
                if(images::check_photo('s_' . $products[$i]['id'] . '.jpg') == 0) {
                    $products[$i]['file'] = 's_' . $products[$i]['id'] . '.jpg';
                    $not_found[] = $products[$i];
                }
                if(images::check_photo('min_' . $products[$i]['id'] . '.jpg') == 0) {
                    $products[$i]['file'] = 'min_' . $products[$i]['id'] . '.jpg';
                    $not_found[] = $products[$i];
                }
           }
        }


        $excel = export::create_new_file();

        $sheet = $excel->getSheet(0);
        $sheet->setTitle('Ошибки с фото');


        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);


        $sheet->setCellValue('A1', 'ID товара');
        $sheet->setCellValue('B1', 'Бренд');
        $sheet->setCellValue('C1', 'Название товара');
        $sheet->setCellValue('D1', 'URL');
        $sheet->setCellValue('E1', 'Нет файла или косячный');
        $sheet->setCellValue('F1', 'Большие фото. Количество');

        $line = 2;

        if(!empty($not_found)) {
            for ($i=0; $i < count($not_found); $i++) { 

                $sheet->setCellValue('A' . $line, $not_found[$i]['id']);
                $sheet->setCellValue('B' . $line, $not_found[$i]['tm_name']);
                $sheet->setCellValue('C' . $line, html_entity_decode($not_found[$i]['name'], ENT_QUOTES, 'UTF-8'));
                $sheet->setCellValue('D' . $line, DOMAIN_FULL . '/product/' . $not_found[$i]['url']);
                $sheet->setCellValue('E' . $line, $not_found[$i]['file']);
                $sheet->setCellValue('F' . $line, $not_found[$i]['pic_quant']);

                ++$line;
            }
        }

        $excel->createSheet();
        $sheet = $excel->getSheet(1);
        $sheet->setTitle('Файлы с верхним регистром');

        $sheet->getColumnDimension('B')->setAutoSize(true);

        $sheet->setCellValue('A1', 'Номер П-П');
        $sheet->setCellValue('B1', 'Имя файла');


        $line = 2;

        if(!empty($high_register)) {
            for ($i=0; $i < count($high_register); $i++) { 

                $sheet->setCellValue('A' . $line, $i);
                $sheet->setCellValue('B' . $line, $high_register[$i]['file_name']);

                ++$line;
            }
        }

        $excel->createSheet();
        $sheet = $excel->getSheet(2);
        $sheet->setTitle('Файлы не формат');

        $sheet->getColumnDimension('B')->setAutoSize(true);

        $sheet->setCellValue('A1', 'Номер П-П');
        $sheet->setCellValue('B1', 'Имя файла');


        $line = 2;

        if(!empty($error_files)) {
            for ($i=0; $i < count($error_files); $i++) { 

                $sheet->setCellValue('A' . $line, $i);
                $sheet->setCellValue('B' . $line, $error_files[$i]['file_name']);

                ++$line;
            }
        }

        DB::query('TRUNCATE TABLE __prod_images_map');

        export::download_excel($excel, 'wrong_photo_' . date('y.m.d_H-i_'));
        
    }

    /**
     * Create all_time admitad orders report
     * @return file xls file
     */
    public function admitad_orders() {

      setlocale(LC_NUMERIC, "C");

      $query = "SELECT ao.order_id, ao.date, o.status_id, s.name AS status_name, o.cost, ao.admitad_uid, o.order_number
                  FROM status AS s, rk_admitad_orders AS ao, orders o
                  WHERE ao.order_id <= 116260
                  AND   o.site_order_number =  ao.order_id
                  AND   o.status_id = s.id
                  AND   status_id != 4

                  UNION

                  SELECT ao.order_id, ao.date, o.status_id, s.name AS status_name, o.cost, ao.admitad_uid, o.order_number
                  FROM status AS s, rk_admitad_orders AS ao, orders o
                  WHERE ao.order_id > 116260
                  AND   o.order_number =  ao.order_id
                  AND   o.status_id = s.id
                  AND   status_id != 4

                  ORDER BY order_id ASC";          

       $orders = db::get_rows($query);

       for ($i=0; $i <count($orders) ; $i++) { 
          $user_id= 0;
          $total_orders = (int)DB::get_field('total', 'SELECT COUNT(order_id) AS total FROM rk_admitad_orders WHERE admitad_uid = "' . $orders[$i]['admitad_uid'] . '" AND order_id < ' . $orders[$i]['order_number']);
          
          if ($total_orders ==  0){

              $user_id = (int)DB::get_field('client_id', "SELECT client_id FROM orders WHERE order_number = {$orders[$i]['order_number']}");

              if ($user_id == 0) {
                  $user_info = DB::get_row('SELECT p.client_id, p.email, p.phone FROM orders AS o INNER JOIN shipping_persons AS p ON p.id = o.shipping_person_id WHERE o.order_number = ' . $orders[$i]['order_number']);

                  if ((int)$user_info['client_id'] > 0) {
                      $user_id = $user_info['client_id'];
                  } else {

                      if (!is_null($user_info['email']) && $user_info['email']!= '') {
                          $user_id = (int)DB::get_field('id', "SELECT id FROM clients WHERE email = '{$user_info['email']}' LIMIT 1");
                      }

                      if($user_id == 0 && !is_null($user_info['phone']) && $user_info['phone'] != '') {
                              $user_id = (int)DB::get_field('id', "SELECT id FROM clients WHERE phone = '{$user_info['phone']}' LIMIT 1");
                      }

                  }
            
              }

          }   

          if ($user_id > 0) {
            $total_orders = (int)DB::get_field('total', "SELECT COUNT(o.order_number) AS total FROM orders o WHERE o.order_number != '{$orders[$i]['order_number']}' AND o.client_id = $user_id");
          }
          
           $orders[$i]['orders_before'] = $total_orders;

          if ($total_orders > 0) {
            $orders[$i]['client_type'] = 'Старый клиент';
            $orders[$i]['reward'] = round(($orders[$i]['cost'] * 0.03), 2, PHP_ROUND_HALF_EVEN);
          } else {
            $orders[$i]['client_type'] = 'Новый клиент';
            $orders[$i]['reward'] = round(($orders[$i]['cost'] * 0.12), 2, PHP_ROUND_HALF_EVEN);
          }
       }


        $excel = export::create_new_file();

        $sheet = $excel->getSheet(0);
        $sheet->setTitle('Заказы ADMITAD');


        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);


        $sheet->setCellValue('A1', 'ID заказа');
        $sheet->setCellValue('B1', 'Дата заказа');
        $sheet->setCellValue('C1', 'Статус заказа');
        $sheet->setCellValue('D1', 'Цена заказа');
        $sheet->setCellValue('E1', 'admitad UID');
        $sheet->setCellValue('F1', 'Заказов ДО');
        $sheet->setCellValue('G1', 'Клиент');
        $sheet->setCellValue('H1', 'Вознаграждение');

        $line = 2;


        for ($i=0; $i < count($orders); $i++) { 

            $sheet->setCellValue('A' . $line, $orders[$i]['order_id']);
            $sheet->setCellValue('B' . $line, $orders[$i]['date']);
            $sheet->setCellValue('C' . $line, $orders[$i]['status_name']);
            $sheet->setCellValue('D' . $line, $orders[$i]['cost']);
            $sheet->setCellValue('E' . $line, $orders[$i]['admitad_uid']);
            $sheet->setCellValue('F' . $line, $orders[$i]['orders_before']);
            $sheet->setCellValue('G' . $line, $orders[$i]['client_type']);
            $sheet->setCellValue('H' . $line, $orders[$i]['reward']);

            ++$line;
        }

        export::download_excel($excel, 'admitad_orders_' . date('y.m.d_H-i_'));

    }

    /**
     * Экспорт списков на обзвон
     */
    function clients_call($arg)
    {

        if (isset($_POST['choise'])) 
        {
            //параметры выборки на вывод
            $c_limit = $managers = $days = $org = $info_all = $c_type = $m_list = FALSE;

            if (isset($_POST['c_limit'])) $c_limit = (int)$_POST['c_limit'];
            if (isset($_POST['managers'])) $managers = (int)$_POST['managers'];
            if (isset($_POST['days'])) $days = (int)$_POST['days'];
            if (isset($_POST['org'])) $org = $_POST['org'];
            if (isset($_POST['info_all'])) $info_all = $_POST['info_all'];

            $days = ($days > 500 ? 500 : $days);
            $c_type = ($org ? '1, 2' : '3');


            if($c_limit && $managers && $days) 
            {
                $query = "SELECT DISTINCT c.id, CONCAT(IFNULL(c.surname, ''), ' ', IFNULL(c.name, '')) AS name, sr.name AS region, c.address, cct.name AS      type, c.company_note, c.discount_note, c.main_discount, c.auto_discount, su.name AS last_manager,
                          (SELECT count(w.order_number) FROM orders w WHERE w.client_id = c.id AND w.status_id IN(1,8,9)) orders_quant,
                          (SELECT sum(w.cost) FROM orders w WHERE w.client_id = c.id AND w.status_id IN(1,8,9)) total_cost,
                          (SELECT r.added_date FROM orders r WHERE r.client_id = c.id AND r.status_id IN(1,8,9) AND r.order_number = (SELECT MAX(q.order_number) FROM orders q WHERE q.client_id = c.id AND q.status_id IN(1,8,9))) last_order, 
                          (SELECT x.order_number FROM orders x WHERE x.client_id = c.id AND x.status_id IN (1,8,9) AND x.order_number = (SELECT MAX(qq.order_number) FROM orders qq WHERE qq.client_id = c.id AND qq.status_id IN (1,8,9))) last_order_id,
                          (SELECT x.cost FROM orders x WHERE x.client_id = c.id AND x.status_id IN (1,8,9) AND x.order_number = (SELECT MAX(qq.order_number) FROM orders qq WHERE qq.client_id = c.id AND qq.status_id IN (1,8,9))) last_order_sum
                          FROM clients c LEFT JOIN cdb_users su ON c.last_user_id = su.id
                                         LEFT JOIN shipping_regions sr ON c.region_id = sr.id 
                                         LEFT JOIN cdb_client_types cct ON c.type_id = cct.id
                                         LEFT JOIN orders o ON c.id = o.client_id AND  o.added_date BETWEEN DATE_SUB(NOW(), INTERVAL $days DAY) AND NOW() WHERE o.client_id IS  NULL
                          AND (SELECT count(oo.order_number) FROM orders oo WHERE oo.client_id = c.id AND oo.status_id IN (1,8,9)) > 1
                          AND c.archive = 0
                          AND c.dinamo = 0
                          AND c.is_supplier = 0
                          AND sr.id IN(50, 77)
                          AND c.type_id IN($c_type)
                          ORDER BY orders_quant, total_cost DESC
                          LIMIT $c_limit";

                $clients = db::get_rows($query);

            } 


            if (count($clients) == 0) {
                Template::set_form(null, 'Не найдены клиенты для этой выборки.');
                Template::set_page('clients_for_call_export', 'Экспорт клиентов на обзвон');
            } else {

                $excel = export::create_new_file();

                for ($i=0; $i < $managers; $i++) { 

                    //$m_list[$i] = array();
                    
                    if($i == 0) {
                        $sheet = $excel->getSheet($i);
                        $sheet->setTitle('Менеджер_' . $i);
                    } else {
                        $sheet = $excel->createSheet($i);
                        $sheet = $excel->getSheet($i);
                        $sheet->setTitle('Менеджер_' . $i);
                    }


                    $sheet->getColumnDimension('B')->setAutoSize(true);
                    $sheet->getColumnDimension('C')->setAutoSize(true);
                    $sheet->getColumnDimension('D')->setAutoSize(true);
                    $sheet->getColumnDimension('E')->setAutoSize(true);
                    $sheet->getColumnDimension('F')->setAutoSize(true);
                    $sheet->getColumnDimension('G')->setAutoSize(true);
                    $sheet->getColumnDimension('H')->setAutoSize(true);
                    $sheet->getColumnDimension('I')->setAutoSize(true);
                    $sheet->getColumnDimension('J')->setAutoSize(true);
                    $sheet->getColumnDimension('K')->setAutoSize(true);
                    $sheet->getColumnDimension('L')->setAutoSize(true);
                    $sheet->getColumnDimension('M')->setAutoSize(true);

                    $sheet->setCellValue('A1', 'ID клиента');
                    $sheet->setCellValue('B1', 'Имя');
                    $sheet->setCellValue('C1', 'Регион');
                    $sheet->setCellValue('D1', 'Тип');
                    $sheet->setCellValue('E1', 'Примечания по клиенту');
                    $sheet->setCellValue('F1', 'Примесания по скидке');
                    $sheet->setCellValue('G1', 'Скидка ручная');
                    $sheet->setCellValue('H1', 'Скидка Авто');
                    $sheet->setCellValue('I1', 'Последний манагер');
                    $sheet->setCellValue('J1', 'Количество заказов');
                    $sheet->setCellValue('K1', 'Последний заказ');
                    $sheet->setCellValue('L1', 'Последний заказ ID');
                    $sheet->setCellValue('M1', 'Последний заказ цена');



                    if ($info_all) {
                        $sheet->getColumnDimension('N')->setAutoSize(true);
                        $sheet->setCellValue('N1', 'Сумма всех заказов');
                    }   

                }              

                $k = 0;
                $line = 2;

                for ($i=0; $i < count($clients); $i++) { 

                    if($k === $managers)
                    {
                        $k = 0;
                        ++$line;
                    }

                    //$m_list[$k][] = $clients[$i];

                    $sheet = $excel->getSheet($k);


                    $sheet->setCellValue('A' . $line, $clients[$i]['id']);
                    $sheet->setCellValue('B' . $line, $clients[$i]['name']);
                    $sheet->setCellValue('C' . $line, $clients[$i]['region']);
                    $sheet->setCellValue('D' . $line, $clients[$i]['type']);
                    $sheet->setCellValue('E' . $line, $clients[$i]['company_note']);
                    $sheet->setCellValue('F' . $line, $clients[$i]['discount_note']);
                    $sheet->setCellValue('G' . $line, $clients[$i]['main_discount']);
                    $sheet->setCellValue('H' . $line, $clients[$i]['auto_discount']);
                    $sheet->setCellValue('I' . $line, $clients[$i]['last_manager']);
                    $sheet->setCellValue('J' . $line, $clients[$i]['orders_quant']);
                    $sheet->setCellValue('K' . $line, $clients[$i]['last_order']);
                    $sheet->setCellValue('L' . $line, $clients[$i]['last_order_id']);
                    $sheet->setCellValue('M' . $line, $clients[$i]['last_order_sum']);

                    if ($info_all) {
                        $sheet->setCellValue('N' . $line, $clients[$i]['total_cost']);
                    }  


                    $k++;
               
                }


                if (ob_get_level()) {
                    ob_end_clean();
                }

                export::download_excel($excel, 'clients-for-call_' . date('y.m.d'));
            }   

        } else {
            Template::set_page('clients_for_call_export', 'Экспорт клиентов на обзвон');
        }    
    }
}