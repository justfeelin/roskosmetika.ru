<?php

class Controller_vk extends Controller_Base
{
    /**
     * @param $arg
     */
    public function index($arg)
    {
        $token = null;

        if (isset($_GET['get_token_error'])) {
            Template::set('get_token_error', $_GET['get_token_error'] ?: true);
        } else {
            $token = vk::get_admin_token(false);
        }

        $tokenOK = $token && (!$token['expires'] || $token['expires'] >= $_SERVER['REQUEST_TIME'] + 600);

        if ($tokenOK) {
            Template::set('new_products', vk::new_products_count());
            Template::set('update_products', vk::not_updated_products_count());
            Template::set('not_categorized', vk::not_categorized_products_count());
        }

        Template::set_page('vk', 'Интеграция vk.com', [
            'token' => $token,
            'tokenOK' => $tokenOK,
        ]);
    }

    /**
     * Retrieves API token
     */
    public function get_token()
    {
        //       photos   market      offline
        $scope = 4      | 134217728 | 65536;

        $error = null;

        $token = vk::get_token(DOMAIN_FULL . '/admin/vk/get_token', $scope);

        if (!$token || !empty($token['error'])) {
            $error = !empty($token['error_description']) ? $token['error_description'] : '';
        } else {
            if ($token['user_id'] != env('VK_ADMIN_ID')) {
                $error = 'Пользователь на vk.com не является администратором группы';
            }
        }

        if ($error !== null) {
            redirect('/admin/vk?get_token_error=' . rawurlencode($error));

            return;
        }

        $token = [
            'token' => $token['access_token'],
            'expires' => $token['expires_in'] ? $_SERVER['REQUEST_TIME'] + $token['expires_in'] : 0,
        ];

        vk::set_admin_token($token);

        caching::delete(vk::ADMIN_TOKEN_MESSAGE_CACHE_KEY);

        redirect('/admin/vk');
    }

    /**
     * Export not exported products to vk.com
     */
    public function insert_products()
    {
        $token = vk::get_admin_token();

        if ($token) {
            vk::export_new_products($token['token'], env('VK_GROUP_ID'), 100);
        }

        redirect('/admin/vk');
    }

    /**
     * Updates previously exported to vk.com products
     */
    public function update_products()
    {
        $token = vk::get_admin_token();

        if ($token) {
            vk::update_products($token['token'], env('VK_GROUP_ID'), 100);
        }

        redirect('/admin/vk');
    }

    /**
     * Categorizes products on vk.com
     */
    public function categorize_products()
    {
        $token = vk::get_admin_token();

        if ($token) {
            vk::categorize_products($token['token'], env('VK_GROUP_ID'), 100);
        }

        redirect('/admin/vk');
    }
}
