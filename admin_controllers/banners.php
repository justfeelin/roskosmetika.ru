<?php

/**
 * Уравление баннерами.
 */
class Controller_banners extends Controller_base
{

    const PAGE_HEADER = 'Редактирование баннеров';

    /**
     * Список баннеров.
     * @param <type> $args Параметры URL.
     */
    function index($args)
    {
        Pagination::setBaseUrl('/admin/banners');

        Template::set_page('banners', self::PAGE_HEADER, [
            'banners' => Banners::get_list(false, 20),
        ]);
    }

    /**
     * Форма редактирование баннеров.
     * @param array $args Параметры URL.
     */
    function edit($args)
    {

        if (isset($args[0]))
            $id = (int)$args[0];

        //Выводим баннер для редактирования
        if (!empty($id)) {
            $info = Banners::get_banner($id);

            Template::add_script('admin/banner.js');

            Template::add_css('flick/jquery-ui.min.css');
            Template::add_script('jquery-ui.min.js');
            Template::add_script('jquery-ui-datepicker-ru.js');

            Template::set_page('edit_banner', self::PAGE_HEADER, $info);
        } // Сохраняем изменения
        elseif ((isset($_POST['save'])) && (!empty($_POST['name'])) && (!empty($_POST['file_name']))) {
            $id = (int)$_POST['id'];
            $name = $_POST['name'];
            $file_name = trim($_POST['file_name']);
            $alt = (isset($_POST['alt']) ? $_POST['alt'] : '');
            $menu_tag = (isset($_POST['menu_tag']) ? $_POST['menu_tag'] : '');
            $link = (isset($_POST['link']) ? $_POST['link'] : '');
            $position = (int)$_POST['position'];
            $map_area  = trim(strip_tags($_POST['map_area'], '<area>'));
            $pic = (($_FILES['pic']['error'] == UPLOAD_ERR_NO_FILE) ? FALSE : TRUE);
            $copyArea = !empty($_POST['copy_area_enabled']) ? DB::escape($_POST['copy_area']) : '';
            $copyAreaText = !empty($_POST['copy_area_text']) ? DB::escape($_POST['copy_area_text']) : '';

            $name = DB::mysql_secure_string($name);
            $file_name = DB::mysql_secure_string($file_name);
            $alt = DB::mysql_secure_string($alt);
            $menu_tag = DB::mysql_secure_string($menu_tag);
            $link = DB::mysql_secure_string($link);
            $map_area = DB::mysql_secure_string($map_area);

            $check = Banners::check_file_name($file_name);

            if (($check['kol'] < 1) || (($check['id'] == $id) && ($check['kol'] == 1))) {

                if ($pic) {
                    $file_info = Banners::get_banner($id);
                    $old_file = $file_info['banner']['file_name'];
                    $old_type = $file_info['banner']['type'];

                    $type = pathinfo($_FILES['pic']['name'], PATHINFO_EXTENSION);

                    if ($type === 'jpeg') {
                        $type = 'jpg';
                    }

                    if (in_array($type, ['jpg', 'gif', 'png'])) {
                        clearstatcache();

                        if (file_exists(SITE_PATH . 'www/images/banners/' . $file_name . '.' . $type)) {
                            @unlink(SITE_PATH . 'www/images/banners/' . $file_name . '.' . $type);
                        }

                        if (file_exists(SITE_PATH . 'www/images/banners/' . $old_file . '.' . $old_type)) {
                            @unlink(SITE_PATH . 'www/images/banners/' . $old_file . '.' . $old_type);
                        }

                        $file_path = SITE_PATH . 'www/images/banners/' . $file_name . '.' . $type;

                        move_uploaded_file($_FILES['pic']['tmp_name'], $file_path);
                    } else {
                        Template::set_page('edit_banner', self::PAGE_HEADER, Banners::get_banner($id));
                    }
                }

                $old_data = Banners::get_banner($id);
                if (!isset($type))
                    $type = $old_data['banner']['type'];

                $dates = [];

                if (!empty($_POST['dates_begin'])) {
                    for ($i = 0, $n = count($_POST['dates_begin']); $i < $n; ++$i) {
                        if ($_POST['dates_begin'][$i]) {
                            $dates[] = [
                                $_POST['dates_begin'][$i],
                                $_POST['dates_end'][$i],
                            ];
                        } else {
                            break;
                        }
                    }
                }

                if (Banners::set_banner($id, $name, $file_name, $menu_tag, $type, $alt, $link, $position, $map_area, $copyArea, $copyAreaText, $dates)) {
                    if (($old_data['banner']['file_name'] != $file_name) && !$pic) {
                        @rename('../images/banners/' . $old_data['banner']['file_name'] . '.' . $old_data['banner']['type'], '../images/banners/' . $file_name . '.' . $type);
                    }

                    caching::delete('index_banners');
                }
            }
            redirect('/admin/banners/');
        } // Выводим список банннеров
        else {
            redirect('/admin/banners/');
        }
    }

    /**
     * Форма добавления баннера
     * @param array $args Параметры URL.
     */
    function add($args)
    {

        // Сохраняем новую страницу.
        if (isset($_POST['save']) && !empty($_POST['name']) && !empty($_POST['file_name'])) {

            $name = $_POST['name'];
            $file_name = $_POST['file_name'];
            $alt = (isset($_POST['alt']) ? $_POST['alt'] : '');
            $menu_tag = (isset($_POST['menu_tag']) ? $_POST['menu_tag'] : '');
            $link = (isset($_POST['link']) ? $_POST['link'] : '');
            $pic = (($_FILES['pic']['error'] == UPLOAD_ERR_NO_FILE) ? FALSE : TRUE);
            $use_under = (isset($_POST['use_under']) ? (int)$_POST['use_under'] : false);
            $map_area  = trim(strip_tags($_POST['map_area'], '<area>'));
            $position = (int)$_POST['position'];


            if (!get_magic_quotes_gpc()) {
                $name = DB::mysql_secure_string($name);
                $file_name = DB::mysql_secure_string($file_name);
                $menu_tag = DB::mysql_secure_string($menu_tag);
                $alt = DB::mysql_secure_string($alt);
                $link = DB::mysql_secure_string($link);
                $map_area = DB::mysql_secure_string($map_area);
            }

            $type = 'jpg';
            $check = Banners::check_file_name($file_name);

            if ($check['kol'] < 1) {
                if ($pic) {
                    $type = pathinfo($_FILES['pic']['name'], PATHINFO_EXTENSION);

                    if ($type === 'jpeg') {
                        $type = 'jpg';
                    }

                    if (in_array($type, ['jpg', 'gif', 'png'])) {
                        clearstatcache();

                        if (file_exists(SITE_PATH . 'www/images/banners/' . $file_name . '.' . $type)) {
                            @unlink(SITE_PATH . 'www/images/banners/' . $file_name . '.' . $type);
                        }

                        $file_path = SITE_PATH . 'www/images/banners/' . $file_name . '.' . $type;

                        move_uploaded_file($_FILES['pic']['tmp_name'], $file_path);
                    } else {
                        Template::set_page('add_banner', 'Добавить баннер');
                    }
                }

                $dates = [];

                if (!empty($_POST['dates_begin'])) {
                    for ($i = 0, $n = count($_POST['dates_begin']); $i < $n; ++$i) {
                        if ($_POST['dates_begin'][$i]) {
                            $dates[] = [
                                $_POST['dates_begin'][$i],
                                $_POST['dates_end'][$i],
                            ];
                        } else {
                            break;
                        }
                    }
                }

                Banners::add_banner($name, $file_name, $menu_tag, $type, $alt, $link, $position, $map_area, $dates);

                caching::delete('index_banners');
            }

            Template::set_page('banners', self::PAGE_HEADER, [
                'banners' => Banners::get_list(),
            ]);
        } elseif (isset($_POST['save']) && empty($_POST['file_name'])) {

            Pagination::setBaseUrl('/admin/banners');

            Template::set_page('banners', self::PAGE_HEADER, [
                'banners' => Banners::get_list(false, 20),
            ]);
        } // Выводим форму для добавления страницы.
        else {
            Template::add_script('admin/banner.js');

            Template::add_css('flick/jquery-ui.min.css');
            Template::add_script('jquery-ui.min.js');
            Template::add_script('jquery-ui-datepicker-ru.js');

            Template::set_page('edit_banner', 'Добавить баннер');
        }
    }

    /**
     * Скрывает/отображает баннер.
     * @param array $args Параметры URL.
     */
    function show($args)
    {

        if (isset($args[0]))
            $id = (int)$args[0];

        if (!empty($id)) {
            Banners::change_visibile($id);
        }

        caching::delete('index_banners');

        redirect('/admin/banners/');
    }

}
