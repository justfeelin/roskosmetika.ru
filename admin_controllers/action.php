<?php

/**
 * Картинки акций.
 */
class Controller_action extends Controller_base
{
    const PAGE_HEADER = 'Картинки для акций';

    /**
     * Список акций.
     * @param <type> $args.
     */
    function index($args)
    {

        Pagination::setBaseUrl('/admin/action');

        Template::set_page('action', self::PAGE_HEADER, [
            'actions' => Action::get_list(),
        ]);
    }

    /**
     * Форма редактирование акции.
     * @param array $args Параметры URL.
     */
    function edit($args)
    {

        if (isset($args[0]))
            $id = (int)$args[0];

        //Выводим акцию для редактирования
        if (!empty($id)) {
            Template::set_page('edit_action', self::PAGE_HEADER, action::get_pic($id));
        } else {
            $this->_edit();
            redirect('/admin/action/');
        }
    }


    /**
     * Форма добавления картинки для акции
     * @param array $args Параметры URL.
     */
    function add($args)
    {
        // Сохраняем новую картинку.
        $this->_edit();

        if (isset($_POST['save']) && empty($_POST['name'])) {
            Template::set_page('action', self::PAGE_HEADER, Action::get_list());
        } // Выводим форму для добавления акции.
        else {
            Template::set('content', []);
            Template::set_page('edit_action', 'Добавление картинки для акции');
        }
    }

    /**
     * Активация/деактивация акции
     * @param array $args Параметры URL.
     */

    function active($args)
    {
        if (isset($args[0])) {
            $id = (int)$args[0];
            Action::change_active($id);
        }

        redirect('/admin/action/');
    }

    private function _edit()
    {
        if (isset($_POST['save']) && isset($_POST['name'])) {
            $isEdit = isset($_POST['id']);

            $id = $isEdit ? (int)$_POST['id'] : (int)action::last_id() + 1;
            $name = $_POST['name'];
            $prods = (isset($_POST['prods']) ? $_POST['prods'] : '');
            $link = (isset($_POST['link']) ? $_POST['link'] : '');
            $active = !empty($_POST['active']);

            $check = action::check_name($name);
            if ($check['kol'] < 1 || $isEdit && ($check['id'] == $id) && ($check['kol'] == 1)) {
                if (Action::set_action($id, $name, $prods, $link, $active))
                    action::set_for_prod($id, $prods, 1);
            }

            redirect('/admin/action/edit/' . $id);
        }
    }
}
