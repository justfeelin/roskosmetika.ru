<?php

class Controller_Questions extends Controller_Base
{
    /**
     * Questions list page
     *
     * @param array $arguments URL arguments
     */
    public function index($arguments)
    {
        if ($arguments) {
            $this->_question($arguments[0]);

            return;
        }

        $search = array();

        $search['read'] = isset($_GET['read']) && $_GET['read'] !== '' ? !empty($_GET['read']) : null;
        $search['answered'] = isset($_GET['answered']) && $_GET['answered'] !== '' ? !empty($_GET['answered']) : null;

        $count = questions_to_cosmetolog::get_questions_count($search['read'], $search['answered']);

        $pagination = $this->pagination($count, questions_to_cosmetolog::STEP, $search);

        $urlParams = [];

        if ($search['read'] !== null) {
            $urlParams[] = 'read=' . ($search['read'] ? 1: 0);
        }

        if ($search['answered'] !== null) {
            $urlParams[] = 'answered=' . ($search['answered'] ? 1: 0);
        }

        if ($pagination['showAll']) {
            $urlParams[] = 'showAll';
        } elseif ($pagination['page'] > 1) {
            $urlParams[] = 'p=' . $pagination['page'];
        }

        Template::set_page('questions', 'Вопросы косметологу', array(
            'back_url_param' => $urlParams ? '?bu=' . rawurlencode(implode('&', $urlParams)) : '',
            'search' => $search,
            'questions' => questions_to_cosmetolog::get_questions($pagination['page'], false, $search['answered'], $search['read']),
        ));
    }

    /**
     * One question page
     *
     * @param int $questionID Question ID
     */
    private function _question($questionID)
    {
        if (
            intval($questionID) != $questionID || $questionID < 1
            || !($question = questions_to_cosmetolog::get_one_question($questionID))
        ) {
            Template::set_page('404', 'Страница не найдена');
        }

        if (!$question['read']) {
            DB::query('UPDATE rk_questions_to_cosmetolog SET `read` = 1 WHERE id = ' . intval($questionID));
        }

        $answerError = false;

        if (!empty($_POST['submitAnswer'])) {
            $question['answer'] = isset($_POST['answer']) ? magic_quotes_unescape(trim($_POST['answer'])) : '';
            $question['question'] = isset($_POST['answer']) ? h(magic_quotes_unescape(trim($_POST['question']))) : '';
            $question['name'] = isset($_POST['name']) ? h(magic_quotes_unescape(trim($_POST['name']))) : '';

            if (
                mb_strlen($question['answer']) < 2
                || mb_strlen(h_d($question['question'])) < 2
                || mb_strlen(h_d($question['name'])) < 2
            ) {
                $answerError = true;
            }

            $question['visible'] = !empty($_POST['visible']);

            if (!$answerError) {
                questions_to_cosmetolog::post_answer($questionID, $question['question'], $question['answer'], $question['name'], $question['visible'], !empty($_POST['email']));

                redirect($_SERVER['REQUEST_URI']);
            }
        }

        Template::set_page('question', 'Вопрос к косметологу', array(
            'back_url_param' => isset($_GET['bu']) ? '?' . $_GET['bu'] : '',
            'answerError' => $answerError,
            'question' => $question,
        ));
    }

    /**
     * @inheritdoc
     */
    protected function admin_access()
    {
        return admin::RIGHTS_COMMON | admin::RIGHTS_STATISTICS;
    }
}
