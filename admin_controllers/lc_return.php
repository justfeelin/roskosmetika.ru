<?php

/**
 * optiscript
 */
class Controller_lc_return extends Controller_Base
{

    function index($args)
    {
        /**
         * Day interval for select orders
         * @var integer
         */
        $day_interval = 135;
        /**
         * Day interval start
         * @var integer
         */
        $start_interval = 48;
        /**
         * Month auantity for calculate statistics 
         * @var integer
         */
        $orders_month_perion = 6;
        /**
         * Min orders for calculated period
         * @var integer
         */
        $max_orders_by_perion = 2;
        /**
         * Order statuses for select
         * @var array
         */
        $orders_status = '1, 8, 9'; 
        /**
         * Cancel orders status
         * @var string
         */
        $cancel_orders_status = '2, 5, 10'; 
        /**
         * Payments ids for calculate real margin
         * @var array
         */
        $non_cash_ids = array('3', '5', '7', '9', '10');
        /**
         * Discoun for no cash orders
         * @var integer
         */
        $no_cash_discount = 3;
        /**
         * Min calculated cash for promo code
         * @var integer
         */
        $min_code_cash = 100;
        /**
         * Min last order cost
         * @var integer
         */
        $min_order_cost = 3500;
        /**
         * Discounts cell by margin
         * @var array
         */
        $margin_cell = array('margin'    => array(0 => 40, 1 => 30, 2 => 20),
                             'discounts' => array(0 => 20, 1 => 16, 2 => 12));
        /**
         * Days for use promo-code
         * @var integer
         */
        $days_for_use = 15;
        /**
         * Sending emails for report
         * @var integer
         */
        $organization_min_margin = 30;
        /**
         * Max client discount in roubles
         * @var integer
         */
        $max_discount = 800;


            $to_send = array();

            //  get orders
            $query = "SELECT o.order_number, c.id AS client_id, c.type_id, o.shipping_person_id, o.cost, 
                            (SELECT SUM(p.purchase_price * oi.quantity) FROM orders_items oi, products p WHERE oi.order_number = o.order_number AND oi.product_id = p.id) AS purchase_cost,
                             IFNULL(o.delivery, 0) AS delivery_cost, 
                             IFNULL(co.old_delivery, 0) - IFNULL(o.delivery, 0) AS delivery_compensation, IF(co.shift_delivery_date IS NOT NULL, co.shift_delivery_date, co.delivery_date) AS d_date, 
                             IF((c.email IS NOT NULL OR c.email <> ''), c.email, (SELECT sp.email FROM shipping_persons sp WHERE sp.id = o.shipping_person_id AND sp.email IS NOT NULL AND sp.email <> '' LIMIT 1)) AS c_email, 
                             IF((o.email IS NOT NULL OR o.email <> ''), o.email, (SELECT sp.email FROM shipping_persons sp WHERE sp.id = o.shipping_person_id AND sp.email IS NOT NULL AND sp.email <> '' LIMIT 1)) AS o_email, 
                             CONCAT(IFNULL(c.name, ''), ' ', IFNULL(c.patronymic, '')) AS name, o.payment_id
                      FROM  cdb_orders co, orders AS o
                      LEFT JOIN clients AS c ON o.client_id = c.id
                      WHERE o.order_number = co.order_number 
                      AND o.auto_order = 0
                      AND co.lc_return_mail = 0
                      AND (c.email IS NOT NULL OR o.email IS NOT NULL OR (SELECT COUNT(sp.id) FROM shipping_persons sp WHERE sp.id = o.shipping_person_id AND sp.email IS NOT NULL AND sp.email <> '') > 0)
                      AND (c.email <> '' OR o.email <> '' OR (SELECT COUNT(sp.id) FROM shipping_persons sp WHERE sp.id = o.shipping_person_id AND sp.email IS NOT NULL AND sp.email <> '') > 0)
                      AND o.status_id IN (" . $orders_status . ")
                      AND IF(co.shift_delivery_date IS NOT NULL, co.shift_delivery_date, co.delivery_date) BETWEEN (CURRENT_DATE() - INTERVAL " . ($start_interval + $day_interval) . " DAY) AND (CURRENT_DATE() - INTERVAL " . $start_interval . " DAY)";

            $orders = DB::get_rows($query);          

            for ($i=0; $i < count($orders); $i++)
            {
                $client_email = $orders[$i]['c_email'] && check::email($orders[$i]['c_email']) ? $orders[$i]['c_email'] : ($orders[$i]['o_email'] && check::email($orders[$i]['o_email']) ? $orders[$i]['o_email'] : FALSE);  

                $query = "SELECT COUNT(o.order_number) AS orders  
                          FROM orders o, cdb_orders co 
                          WHERE o.order_number = co.order_number 
                          AND (o.client_id = " . $orders[$i]['client_id'] . " OR o.shipping_person_id = " . $orders[$i]['shipping_person_id'] . ")  
                          AND o.status_id ";

                $result = (int)DB::get_field('orders', $query . " NOT IN (" . $cancel_orders_status . ") AND o.added_date > '" . $orders[$i]['d_date'] . "'");

                $result = ($result > 0 ? FALSE : TRUE);

                if ($result) 
                {
                    $result = (int)DB::get_field('orders', $query . "IN (" . $orders_status . ") AND IF(co.shift_delivery_date IS NOT NULL, co.shift_delivery_date, co.delivery_date) BETWEEN (NOW() - INTERVAL " . $orders_month_perion . " MONTH) AND NOW()");
                    $result =  ($result <= $max_orders_by_perion ? TRUE : FALSE);        
                }



                if ($result &&  $client_email && $orders[$i]['cost'] > 0)
                {  
                  $margin_rub = floor($orders[$i]['cost'] - $orders[$i]['delivery_cost'] - $orders[$i]['purchase_cost'] - (in_array($orders[$i]['payment_id'], $non_cash_ids) ? $orders[$i]['cost'] * ($no_cash_discount/100) : 0) - $orders[$i]['delivery_compensation']);
                  $margin_percent = floor(($margin_rub/($orders[$i]['cost'] - $orders[$i]['delivery_cost'])) * 100);

                  $discount = 0;

                  foreach ($margin_cell['margin'] as $key => $value) 
                  {
                      if ($margin_percent >= $value) 
                      {
                          if ($orders[$i]['type_id'] != 3 && $margin_percent < $organization_min_margin)
                          {
                            continue;
                          } 
                          $discount = $margin_cell['discounts'][$key];
                          break;

                      }
                  }

                  $discount_cash = floor($margin_rub * ($discount/100)); 

                  $discount_cash = $discount_cash > $max_discount ? $max_discount : $discount_cash;

                  if ($discount_cash >= $min_code_cash) {

                        $c_name = trim($orders[$i]['name']);


                        do {

                            $pre_code = optimization::make_code(6);

                        } while (DB::get_row("SELECT p.id FROM rk_promo_codes AS p WHERE p.code = '$pre_code'"));

                        $pre_code = 'RC' . $pre_code;  

                        $query = "INSERT INTO rk_promo_codes( code,
                                                           type,
                                                           create_date,
                                                           end_date,
                                                           min_sum,
                                                           discount,
                                                           discount_type,
                                                           comment, 
                                                           `use`)
                                         VALUES ('$pre_code',
                                                 'uniq',
                                                 NOW(),
                                                 (NOW() + INTERVAL " . $days_for_use . " DAY), 
                                                 $min_order_cost,
                                                 -$discount_cash,
                                                 1, '" .
                                                 'Реактивация рассылка через INBOXER. Персональный промо-код для клиента ID ' . $orders[$i]['client_id'] . ($c_name ? ' ' . $c_name : '') . ' ' . date("Y-m-d")  . "',
                                                 0)";

                        DB::query($query);
                        $promo_code =  DB::get_row('SELECT * FROM rk_promo_codes p WHERE p.id = ' . DB::get_last_id());

                        $order_id = (int)$orders[$i]['order_number'];
                        DB::query("UPDATE cdb_orders SET lc_return_mail = 1 WHERE order_number = $order_id");


                        $client_id = (int)$orders[$i]['client_id'];
                        DB::query("UPDATE clients c SET c.company_note = CONCAT( c.company_note, ' * " . date("Y-m-d") . ' INBOXER клиенту отправлен персональный промо-код: ' . $promo_code['code'] . ', на скидку -' . $discount_cash . " руб.') WHERE c.id = $client_id");

                        $to_send[] = array('client_id' => $orders[$i]['client_id'],
                                           'email' => $client_email,
                                           'name' => $c_name,
                                           'type_id' => $orders[$i]['type_id'],
                                           'promo_code' => $promo_code['code'],
                                           'discount_cash' => $discount_cash);
                        
                  }
                }
            }

        $excel = export::create_new_file();

        $sheet = $excel->getSheet(0);


        $sheet->setCellValue('A' . (1), 'ID клиента');
        $sheet->setCellValue('B' . (1), 'Email');
        $sheet->setCellValue('C' . (1), 'Имя клиента');
        $sheet->setCellValue('D' . (1), 'Тип клиента');
        $sheet->setCellValue('E' . (1), 'Промо-код');
        $sheet->setCellValue('F' . (1), 'Скидка в руб.');



        for ($i=0; $i < count($to_send); $i++) {

            $sheet->setCellValue('A' . ($i + 2), $to_send[$i]['client_id']);
            $sheet->setCellValue('B' . ($i + 2), $to_send[$i]['email']);
            $sheet->setCellValue('C' . ($i + 2), $to_send[$i]['name']);
            $sheet->setCellValue('D' . ($i + 2), $to_send[$i]['type_id']);
            $sheet->setCellValue('E' . ($i + 2), $to_send[$i]['promo_code']);
            $sheet->setCellValue('F' . ($i + 2), $to_send[$i]['discount_cash']);

        }


        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename=' . 'lc-return.xls');

        $writer = PHPExcel_IOFactory::createWriter($excel,'Excel5');
        $writer->save('php://output');

        exit;

    }

}

?>