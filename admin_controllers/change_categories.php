<?php

/**
 * optiscript
 */
class Controller_change_categories extends Controller_Base
{

    function index($args)
    {
        
        $main_id = 1160;
        $main_url = 'dlya-makiyazha';

        //  меняем уровень главной категории из 2 в 1
        DB::query("UPDATE rk_categories SET level = 1, parent_1 = 0, name = 'Макияж', _full_url = '$main_url', norder = 2 WHERE id = $main_id");

        // получаем id 3-их категорий
        $cat_3 = db::get_rows("SELECT id, url, name FROM rk_categories WHERE parent_1 = $main_id");

        // получаем id продуктов, прикрученных к категории уровня 2
        $prod_cat_2 = db::get_rows("SELECT prod_id FROM rk_prod_cat_lvl_2 WHERE cat_id = $main_id");

        // удаляем связки продуктов на 2-м уровне для главной категории
        db::query("DELETE FROM rk_prod_cat_lvl_2 WHERE cat_id = $main_id");

        //  удаляем связку в субкатегории
        db::query("DELETE FROM rk_cat_subcat WHERE cat_id = $main_id");


        for ($i=0; $i <count($prod_cat_2) ; $i++) { 
            // прописываем старые связки из 2-го уровня на 1 для главной категории
            DB::query("REPLACE INTO rk_prod_cat_lvl_1 (prod_id, cat_id) VALUES ({$prod_cat_2[$i]['prod_id']}, $main_id)");


        }

        for ($i=0; $i <count($cat_3) ; $i++) { 
            
            //  получаем id продуктов, прикрученных к категориям уровня 3
            $prod_cat_3 = db::get_rows("SELECT prod_id, cat_id FROM rk_prod_cat WHERE cat_id = {$cat_3[$i]['id']}");

            //  меняем уровень главной категории из 3 в 2
            DB::query("UPDATE rk_categories SET level = 2, parent_1 = $main_id, parent_2 = 0, _full_url = '" . $main_url . '/' . $cat_3[$i]['url'] . "' WHERE id = {$cat_3[$i]['id']}");

            //  удаляем связки продуктов на 3-м уровне для категорий, которые будут 2-го уровня
            db::query("DELETE FROM rk_prod_cat WHERE cat_id = {$cat_3[$i]['id']}");

            //   Ищем субкатегории в связке с категорией 3-го уровня, котрые будут 2-го уровня
            $subcat_id_3 = DB::get_rows("SELECT subcat_id FROM rk_cat_subcat WHERE cat_id = {$cat_3[$i]['id']}");

            for ($j=0; $j < count($subcat_id_3) ; $j++) { 
                 // Меняем уровень субкатегории с 3 на 2
                 db::query("UPDATE rk_subcategories SET level = '2' WHERE level ='3' AND id = {$subcat_id_3[$j]['subcat_id']}");

             } 


             // переменные для новой категории
             $cat_3_name = trim(mb_strtolower($cat_3[$i]['name']));

             if(mb_strrpos($cat_3_name, 'для') !== FALSE) {

                $cat_3_url = 'vsjo-' . $cat_3[$i]['url'];                

             } else {
                $cat_3_url = 'vse-' . $cat_3[$i]['url'];
             }

             $cat_3_name = "Все&nbsp;" . $cat_3_name;


             // Добавляем субкатегорию
             db::query("INSERT INTO rk_subcategories (level, name) VALUES ('3', 'В ассортименте')");

             $subcat_3_id = db::get_field('id', 'SELECT MAX(id) AS id FROM rk_subcategories');
             
             // добавляем категорию
             db::query("INSERT INTO rk_categories(name, level, parent_1, parent_2, url, _full_url, visible, is_tag, _has_products)
                        VALUES ('$cat_3_name', 3, {$cat_3[$i]['id']}, $main_id, '$cat_3_url', '" . $main_url . '/' . $cat_3[$i]['url'] . '/' . $cat_3_url . "', 1, 0, 1)");

             $cat_3_id = db::get_field('id', 'SELECT MAX(id) AS id FROM rk_categories');

             //  связка категория 3-го уровня субкатегоря 3-го уровня

             db::query("INSERT INTO rk_cat_subcat (cat_id, subcat_id) VALUES ($cat_3_id, $subcat_3_id)");


            for ($j=0; $j <count($prod_cat_3) ; $j++) { 
                // прописываем старые связки из 3-го уровня на 2 для категорий которые будут 2-го уровня
                DB::query("INSERT INTO rk_prod_cat_lvl_2 (prod_id, cat_id) VALUES ({$prod_cat_3[$j]['prod_id']}, {$cat_3[$i]['id']})");
                DB::query("INSERT INTO rk_prod_cat (prod_id, cat_id) VALUES ({$prod_cat_3[$j]['prod_id']}, $cat_3_id)");

            }

        }


        $main_id_2 = 102;

        //  меняем родителя 
        DB::query("UPDATE rk_categories SET  parent_1 = $main_id, _full_url = '" . $main_url . '/dlya-snyatiya-makiyazha' . "' WHERE id = $main_id_2");

        $cat_3 = db::get_rows("SELECT id, url, name FROM rk_categories WHERE parent_1 = $main_id_2");

        //  удаляем связку в субкатегории
        db::query("DELETE FROM rk_cat_subcat WHERE cat_id = $main_id_2");

        // добавляем новую связку
        db::query("INSERT INTO rk_cat_subcat (cat_id, subcat_id) VALUES (102, 16446)");


        for ($i=0; $i <count($cat_3) ; $i++) { 

            //  меняем url категорий
            DB::query("UPDATE rk_categories SET  parent_2 = $main_id, _full_url = '" . $main_url . '/dlya-snyatiya-makiyazha/' . $cat_3[$i]['url'] . "' WHERE id = {$cat_3[$i]['id']}");

            //  получаем id продуктов, прикрученных к категориям уровня 3
            $prod_cat_3 = db::get_rows("SELECT prod_id, cat_id FROM rk_prod_cat WHERE cat_id = {$cat_3[$i]['id']}");

            for ($j=0; $j <count($prod_cat_3) ; $j++) { 

                //  удаляем старую связку на 1 уровне
                db::query("DELETE FROM rk_prod_cat_lvl_1 WHERE cat_id = 1 AND prod_id = {$prod_cat_3[$j]['prod_id']}");

                DB::query("REPLACE INTO rk_prod_cat_lvl_1 (prod_id, cat_id) VALUES ({$prod_cat_3[$j]['prod_id']}, $main_id)");


            }

        }


        //   обновления старых категорий
        DB::query("UPDATE rk_categories SET name = 'Лицо', norder = 1 WHERE id = 1");
        DB::query("UPDATE rk_categories SET name = 'Тело', norder = 3 WHERE id = 2");
        DB::query("UPDATE rk_categories SET name = 'Волосы', norder = 4 WHERE id = 5");
        DB::query("UPDATE rk_categories SET name = 'Руки&nbsp;и&nbsp;ноги', norder = 5 WHERE id = 3");
        DB::query("UPDATE rk_categories SET name = 'Душ&nbsp;и&nbsp;ванна', norder = 6 WHERE id = 4");
        DB::query("UPDATE rk_categories SET norder = 7 WHERE id = 6");
        DB::query("UPDATE rk_categories SET norder = 8 WHERE id = 7");
        DB::query("UPDATE rk_categories SET norder = 9 WHERE id = 1332");
        

        vd('Усе!');exit;

    }

}

?>