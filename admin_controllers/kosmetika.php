<?php

/**
 * Вывод вопросов LO.
 */
class Controller_kosmetika extends Controller_Base
{
    public function index($arg)
    {
        if (!isset($arg[0])) {
            Template::add_script('admin/kosmetika.js');

            Template::set_page('type_pages', 'Типы косметики', [
                'type_pages' => type_page::get_pages(false, true, isset($_GET['sort_products']) ? $_GET['sort_products'] === 'asc' : null),
            ]);
        } else {
            $type_page = type_page::get_page((int)$arg[0]);

            if (!$type_page) {
                $this->nopage();

                return;
            }

            admin::products_search_form(
                function ($productIDs) use ($type_page) {
                    type_page::add_products($type_page['id'], $productIDs);
                },
                function () use ($type_page) {
                    return type_page::linked_products($type_page['id']);
                },
                function ($productIDs) use ($type_page) {
                    type_page::remove_products($type_page['id'], $productIDs);
                },
                function ($tagIDs) use ($type_page) {
                    type_page::copy_from_tags($type_page['id'], $tagIDs);
                }
            );

            Template::set_page('type_page', 'Тип косметики', [
                'type_page' => $type_page,
            ]);
        }
    }

    /**
     * Clear products from tag
     *
     * @param array $args
     */
    public function clear_products($args)
    {
        type_page::clear_products($args[0]);

        $this->_redirectBack();
    }

    /**
     * Toggles visibility for tag
     *
     * @param array $args
     */
    public function toggle_visibility($args)
    {
        type_page::toggle_visibility($args[0]);

        $this->_redirectBack();
    }

    /**
     * Back url redirect with support of GET parameters
     */
    private function _redirectBack()
    {
        redirect(isset($_GET['bu']) ? $_GET['bu'] : '/admin/tags');
    }
}
