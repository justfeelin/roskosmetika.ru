<?php

class Controller_seo extends Controller_Base
{
    /**
     * Global search page
     *
     * @param $arg
     */
    public function index($arg)
    {
        $this->nopage();
    }

    /**
     * Homepage edit
     */
    public function home()
    {
        $page = pages::get_page('index', false);

        if (!empty($_POST['page'])) {
            try {
                $saved = !!pages::set_page(
                    $page['id'],
                    $page['name'],
                    $_POST['page']['header'],
                    $_POST['page']['content'],
                    $_POST['page']['title'],
                    $_POST['page']['description'],
                    $_POST['page']['keywords'],
                    $page['type'],
                    $page['begin'],
                    $page['end']
                );
            } catch (Exception $e) {
                $saved = false;
            }

            if ($saved) {
                redirect($_SERVER['REQUEST_URI']);
            }
        }

        Template::set_page('seo_homepage', 'Главная страница', [
            'page' => $page,
        ]);
    }

    /**
     * Categories/category pages
     *
     * @param $args
     */
    public function categories($args)
    {
        if (empty($args[0])) {
            $onPage = 30;

            $get = [];

            if (isset($_GET['q'])) {
                $get['q'] = $_GET['q'];
            }

            $tagsOnly = isset($_GET['tag']);

            if ($tagsOnly) {
                $get['tag'] = 1;
            }

            $search = !empty($get['q']) ? $get['q'] : null;

            $count = Categories::get_list(null, $search, true, null, $tagsOnly);

            $pagination = $this->pagination($count, $onPage, $get);

            Template::set_page('seo_categories', 'Категории', [
                'categories' => Categories::get_list(null, $search, $onPage, $pagination['page'] > 1 ? ($pagination['page'] - 1) * $onPage : 0, $tagsOnly),
                'tags_only' => $tagsOnly,
            ]);
        } else {
            $category = Categories::get_by_id($args[0]);

            if (!$category) {
                $this->nopage();

                return;
            }

            if (!empty($_POST['category'])) {
                try {
                    $saved = Categories::update_seo($category['id'], $_POST['category']['h1'], $_POST['category']['title'], $_POST['category']['description'], $_POST['category']['keywords'], $_POST['category']['desc'], $category['is_tag'] ? $_POST['category']['anchor'] : null, $category['is_tag'] ? $_POST['category']['tag_name'] : null, (int)$_POST['category']['name_on_products']);

                    if ($saved) {
                        if ($category['is_tag']) {
                            Categories::update_tag_tm_ids($category['id'], !empty($_POST['category']['tm_ids']) ? $_POST['category']['tm_ids'] : []);
                        }
                    }
                } catch (Exception $e) {
                    $saved = false;
                }

                if ($saved) {
                    redirect($_SERVER['REQUEST_URI']);
                }
            }

            if ($category['is_tag']) {
                $tms = tm_in_cat::get_by_category($category['id']);

                $tmIDs = [];

                for ($i = 0, $n = count($tms); $i < $n; ++$i) {
                    $tmIDs[] = (int)$tms[$i]['tm_id'];
                }
            } else {
                $tmIDs = null;
            }

            Template::set_page('seo_category', $category['is_tag'] ? 'Тег' : 'Категория', [
                'category' => $category,
                'tms' => $category['is_tag'] ? Tm::get_list(false, false, false, false) : null,
                'selected_tms' => $tmIDs,
            ]);
        }
    }

    /**
     * List/edit trademark pages
     *
     * @param $args
     */
    public function tm($args)
    {
        if (empty($args[0])) {
            Template::set_page('seo_tms', 'Торговые марки', [
                'tms' => tm::get_list(),
            ]);
        } else {
            $tm = tm::get_by_id($args[0], true);

            if (!$tm) {
                $this->nopage();

                return;
            }

            if (isset($_POST['tm'])) {
                try {
                    $saved = tm::update_seo($tm['id'], $_POST['tm']['alt_name'], $_POST['tm']['title'], $_POST['tm']['description'], $_POST['tm']['keywords'], $_POST['tm']['desc']);
                } catch (Exception $e) {
                    $saved = false;
                }

                if ($saved) {
                    redirect($_SERVER['REQUEST_URI']);
                }
            }

            Template::set_page('seo_tm', 'Торговая марка', [
                'tm' => $tm,
            ]);
        }
    }

    /**
     * List/edit category-trademark pages
     *
     * @param $args
     */
    public function category__tm($args)
    {
        if (empty($args)) {
            if (!empty($_GET['q'])) {
                $items = tm_in_cat::get_list($_GET['q']);
            } else {
                $items = null;
            }

            Template::set_page('seo_category_tms', 'Категории + бренды', [
                'items' => $items,
            ]);
        } else {
            if (empty($args[1])) {
                $this->nopage();

                return;
            }

            $tmInCat = tm_in_cat::get_pair($args[1], $args[0]);

            if (!$tmInCat) {
                $this->nopage();

                return;
            }

            if (isset($_POST['ct'])) {
                try {
                    $saved = tm_in_cat::update_seo($tmInCat['tm_id'], $tmInCat['cat_id'], $_POST['ct']['h1'], $_POST['ct']['title'], $_POST['ct']['description'], $_POST['ct']['keywords'], $_POST['ct']['full_desc']);
                } catch (Exception $e) {
                    $saved = false;
                }

                if ($saved) {
                    redirect($_SERVER['REQUEST_URI']);
                }
            }

            Template::set_page('seo_category_tm', 'Категория + бренд', [
                'item' => $tmInCat,
            ]);
        }
    }

    /**
     * List/edit trademark lines pages
     *
     * @param $args
     */
    public function lines($args)
    {
        if (empty($args[0])) {
            $tms = tm::get_list(false, false, false, false);

            if (!empty($_GET['tm_id'])) {
                $tmID = (int)$_GET['tm_id'];
                $lines = lines::get_list($tmID, false, true);
            } else {
                $lines = null;
                $tmID = null;
            }

            Template::set_page('seo_lines', 'Бренды + коллекции', [
                'tms' => $tms,
                'tmID' => $tmID,
                'lines' => $lines,
            ]);
        } else {
            $line = lines::get_by_id($args[0]);

            if (!$line) {
                $this->nopage();

                return;
            }

            if (isset($_POST['line'])) {
                try {
                    $saved = lines::update_seo($line['id'], $_POST['line']['h1'], $_POST['line']['title'], $_POST['line']['description'], $_POST['line']['keywords'], $_POST['line']['desc'], $_POST['line']['short_desc']);
                } catch (Exception $e) {
                    $saved = false;
                }

                if ($saved) {
                    redirect($_SERVER['REQUEST_URI']);
                }
            }

            Template::set_page('seo_line', 'Бренд + коллекция', [
                'line' => $line,
            ]);
        }
    }

    /**
     * List/edit products pages
     *
     * @param $args
     */
    public function products($args)
    {
        if (empty($args[0])) {
            $onPage = 30;

            $get = [];

            if (isset($_GET['q'])) {
                $get['q'] = $_GET['q'];
            }

            $search = !empty($get['q']) ? $get['q'] : null;

            $count = Product::search($search, true);

            $pagination = $this->pagination($count, $onPage, $get);

            Template::set_page('seo_products', 'Товары', [
                'products' => Product::search($search, $onPage, $pagination['page'] > 1 ? ($pagination['page'] - 1) * $onPage : 0),
            ]);
        } else {
            $product = Product::search((int)$args[0], null, null, true);

            if (!$product) {
                $this->nopage();

                return;
            }

            $product = $product[0];

            if (isset($_POST['product'])) {
                try {
                    $saved = Product::update_seo($product['id'], $_POST['product']['title'], $_POST['product']['description'], $_POST['product']['keywords'], $_POST['product']['page_description'], $_POST['product']['short_description']);
                } catch (Exception $e) {
                    $saved = false;
                }

                if ($saved) {
                    redirect($_SERVER['REQUEST_URI']);
                }
            }

            Template::set_page('seo_product', 'Товар', [
                'product' => $product,
            ]);
        }
    }

    /**
     * Create tag page
     */
    public function tag_page()
    {
        $error = null;

        $data = [
            'parent_id' => 0,
            'anchor' => '',
            'url' => '',
            'tag_name' => '',
            'parent_type_page' => false,
            'type_page_id' => 0,
        ];

        if (!empty($_POST['tag'])) {
            try {
                $parent = $_POST['tag']['parent_id'] ? Categories::get_by_id($_POST['tag']['parent_id']) : null;

                $parentIsCategory = empty($_POST['tag']['parent_type_page']);

                if (Categories::get_id_by_url(DB::escape($_POST['tag']['url']), (int)($_POST['tag'][$parentIsCategory ? 'parent_id' : 'type_page_id']), false, $parentIsCategory)) {
                    throw new Exception('Тег или категория с таким URL уже существует');
                }

                if ($newID = Categories::add_category(DB::escape($_POST['tag']['anchor']), $parent ? (int)$parent['level'] + 1 : 1, $parent ? $parent['id'] : 0, $parent && $parent['level'] == 2 ? $parent['parent_1'] : 0, $_POST['tag']['tag_name'] ?: $_POST['tag']['anchor'], DB::escape($_POST['tag']['url']), !empty($_POST['tag']['parent_type_page']) ? (int)$_POST['tag']['type_page_id'] : 0)) {
                    optimization::category_full_urls();

                    redirect('/admin/seo/categories/' . $newID);
                }
            } catch (Exception $e) {
                $data = array_merge($data, $_POST['tag']);

                $error = $e->getMessage();
            }
        }

        Template::add_css('../assets/jquery-select2/select2.css');
        Template::add_css('../assets/jquery-select2/select2-bootstrap.css');

        Template::add_script('../assets/jquery-select2/select2.min.js');
        Template::add_script('../assets/jquery-select2/select2_locale_ru.js');

        Template::add_script('admin/seo_tag_page.js');

        Template::set_page('seo_tag_page', 'Создание теговой странцы', [
            'data' => $data,
            'categories' => Categories::get_categories(false, true),
            'error' => $error,
            'type_pages' => type_page::get_pages(),
        ]);
    }

    /**
     * Links base page
     */
    public function link_base()
    {
        $error = $info = null;

        if (!empty($_FILES['file'])) {
            if (
                !empty($_FILES['file']['error'])
                || !is_uploaded_file($_FILES['file']['tmp_name'])
            ) {
                $error = 'Ошибка при загрузке файла';
            } else {
                if (
                !preg_match('/\.xlsx?$/', $_FILES['file']['name'])
                ) {
                    $error = 'Расширение загруженного файла не .xlsx, .xls или файл в неправильном формате';
                } else {
                    $lineI = 1;

                    $succeed = 0;

                    $excel = import::load_file($_FILES['file']['tmp_name']);

                    $sheet = $excel->getActiveSheet();

                    $max_row = $sheet->getHighestRow();

                    $errors = [];

                    for ($i = 1; $i <= $max_row; $i++) {
                        $anchor = $sheet->getCellByColumnAndRow(2, $i)->getValue();

                        if ($anchor) {
                            $donor = link_base::parse_url($sheet->getCellByColumnAndRow(0, $i)->getValue());
                            $acceptor = link_base::parse_url($sheet->getCellByColumnAndRow(1, $i)->getValue());

                            if ($donor && $acceptor) {
                                link_base::update($donor, $acceptor, $anchor);

                                ++$succeed;
                            } else {
                                if (!$donor) {
                                    $errors[] = 'Неправильный донор на строке ' . $lineI;
                                }

                                if (!$acceptor) {
                                    $errors[] = 'Неправильный акцептор на строке ' . $lineI;
                                }
                            }
                        } else {
                            $errors[] = 'Неверное количество столбцов на строке ' . $lineI;
                        }

                        ++$lineI;
                    }

                    if ($errors) {
                        $error = implode('<br>', $errors);
                    }

                    if ($succeed) {
                        $info = 'Обновлено позиций: ' . $succeed;

                        link_base::update_links();
                    }
                }
            }
        }

        Template::set_page('seo_link_base', 'Перелинковка', [
            'error' => $error,
            'info' => $info,
        ]);
    }

    /**
     * Type pages page
     *
     * @param $args
     */
    public function type_pages($args)
    {
        if (empty($args[0])) {
            Template::set_page('seo_type_pages', 'Страницы типов косметики', [
                'items' => type_page::get_pages(false),
            ]);
        } else {
            if ($args[0] === 'new') {
                $pageID = null;

                $item = [
                    'url' => '',
                    'name' => '',
                    'image' => null,
                    'h1' => '',
                    'title' => '',
                    'description' => '',
                    'text' => '',
                    'name_on_products' => 10,
                ];
            } else {
                $pageID = (int)$args[0];

                $item = type_page::get_page($args[0]);

                if (!$item) {
                    $this->nopage();

                    return;
                }
            }

            if (!empty($_POST['item'])) {
                if (
                    !empty($_FILES['image'])
                    && !$_FILES['image']['error']
                    && is_uploaded_file($_FILES['image']['tmp_name'])
                ) {
                    $item['image'] = $image = $_FILES['image']['name'];
                } else {
                    $image = null;
                }

                $item = array_merge($item, $_POST['item']);

                try {
                    $id = type_page::update($pageID, $item['url'], $item['name'], $item['h1'], $item['title'], $item['description'], $item['text'], $image, (int)$item['name_on_products']);

                    if ($image) {
                        move_uploaded_file($_FILES['image']['tmp_name'], SITE_PATH . 'www/images/kosmetika/' . $_FILES['image']['name']);
                    }

                    redirect('/admin/seo/type_pages/' . $id);
                } catch (Exception $exception) {

                }
            }

            Template::set_page('seo_type_page', $pageID ? 'Редактирование страницы типа косметики' : 'Создание страницы типа косметики', [
                'item' => $item,
            ]);
        }
    }

    /**
     * Trademarks + type pages rows
     *
     * @param $args
     */
    public function tm_type_pages($args)
    {
        if (empty($args[0])) {
            $typePages = type_page::get_pages(false);

            $items = !empty($_GET['type_page_id']) ? type_page::get_type_pages_tms($_GET['type_page_id']) : null;

            Template::set_page('seo_tm_type_pages', 'Типы косметики + бренды', [
                'type_pages' => $typePages,
                'items' => $items,
            ]);
        } else {
            if (
                empty($args[1])
                || !($item = type_page::get_type_page_tm($args[0], $args[1]))
            ) {
                $this->nopage();

                return;
            }

            if (!empty($_POST['item'])) {
                try {
                    type_page::update_type_page_tm($args[0], $args[1], $_POST['item']['h1'], $_POST['item']['title'], $_POST['item']['description'], $_POST['item']['text']);

                    redirect($_SERVER['REQUEST_URI']);
                } catch (Exception $e) {
                    $item = array_merge($item, $_POST['item']);
                }
            }

            Template::set_page('seo_tm_type_page', 'Тип косметики + бренд', [
                'item' => $item,
            ]);
        }
    }


    /**
     * @inheritdoc
     */
    protected function admin_access()
    {
        return admin::RIGHTS_SEO;
    }
}
