<?php

/**
 * Управление мастер-классами.
 */
class Controller_master_classes extends Controller_Base
{

    /**
     * Выводит список всех мастер-классов.
     * @param <type> $args
     */
    function index($args)
    {
        $this->mc_list($args);
    }

    /**
     * Выводит список всех мастер-классов
     * @param array $args Параметры URL.
     */
    function mc_list($args)
    {
        $page_header = 'Список мастер-классов';
        Pagination::setBaseUrl('/admin/master_classes');
        Template::set_page('master_classes_list', $page_header, [
            'mcs' => mc::get_master_classes_list(false, 20, true),
        ]);
    }

    /**
     * Изменяет видимость мастер-класса.
     * @param array $args URL параметры.
     */
    function change_vibibility($args)
    {
        mc::change_master_class_visibility($args[0]);

        $this->go_back();
    }

    /**
     * Выводит форму добавления мастер-класса.
     * @param <type> $args
     */
    function add_master_class($args)
    {
        $page_header = "Новый мастер-класс";
        Template::set('content', []);
        Template::set_page('master_classes_edit', $page_header);
    }

    /**
     * Сохраняет изменения в мастер-классах
     * @param <type> $args
     */
    function save($args)
    {
        if (isset($_POST['submit'])) {
            $id = (isset($_POST['id']) ? (int)$_POST['id'] : 0);
            $name = $_POST['name'];
            $series = $_POST['series'];
            $short_desc = $_POST['short_desc'];
            $description = $_POST['text'];
            $date = $_POST['year'] . '-' . $_POST['month'] . '-' . $_POST['day'] . ' ' . $_POST['hour'] . ':' . $_POST['minute'];
            $duration = $_POST['duration'];
            $img = $_POST['file_name'];
            $visibility = (isset($_POST['visibility']) && $_POST['visibility'] == 'on' ? 1 : 0);
            $new_address = (isset($_POST['new_address']) && $_POST['new_address'] == 'on' ? 1 : 0);
            $webinar = (isset($_POST['webinar']) && $_POST['webinar'] == 'on' ? 1 : 0);
            if (isset($_FILES['pic']))
                $pic = (($_FILES['pic']['error'] == UPLOAD_ERR_NO_FILE) ? FALSE : TRUE);
            else {
                $pic = FALSE;
            }

            if ($pic) {
                $fileExt = strtolower(pathinfo($_FILES['pic']['name'], PATHINFO_EXTENSION));

                if (!preg_match('/.+\.' . preg_quote($fileExt) . '$/i', $img)) {
                    $img .= '.' . $fileExt;
                }

                if ($id) {
                    $file_info = mc::get_master_class($id);
                    $old_file = $file_info['img'];
                    if (!$old_file) $old_file = 'none';

                    if ($fileExt === 'jpg' || $fileExt === 'jpeg') {
                        if (file_exists('../images/mc/' . $img)) @unlink('../images/mc/' . $img);
                        if (file_exists('../images/mc/' . 'min_' . $img)) @unlink('../images/mc/' . 'min_' . $img);
                        if (file_exists('../images/mc/' . $old_file)) @unlink('../images/mc/' . $old_file);
                        if (file_exists('../images/mc/' . 'min_' . $old_file)) @unlink('../images/mc/' . 'min_' . $old_file);
                        $file_path = '../images/mc/' . $img;
                        @move_uploaded_file($_FILES['pic']['tmp_name'], $file_path);
                        @copy($file_path, '../images/mc/' . 'min_' . $img);
                        images::set_img_size('../images/mc/' . 'min_' . $img, 150, 95);
                    } elseif ($fileExt === 'gif') {
                        if (file_exists('../images/mc/' . $img)) @unlink('../images/mc/' . $img);
                        if (file_exists('../images/mc/' . 'min_' . $img)) @unlink('../images/mc/' . 'min_' . $img);
                        if (file_exists('../images/mc/' . $old_file)) @unlink('../images/mc/' . $old_file);
                        if (file_exists('../images/mc/' . 'min_' . $old_file)) @unlink('../images/mc/' . 'min_' . $old_file);
                        $file_path = '../mages/mc/' . $img;
                        move_uploaded_file($_FILES['pic']['tmp_name'], $file_path);
                        @copy($file_path, '../images/mc/' . 'min_' . $img);
                        images::set_img_size('../images/mc/' . 'min_' . $img, 150, 95);
                    } elseif ($fileExt === 'png') {
                        if (file_exists('../images/mc/' . $img)) @unlink('../images/mc/' . $img);
                        if (file_exists('../images/mc/' . 'min_' . $img)) @unlink('../images/mc/' . 'min_' . $img);
                        if (file_exists('../images/mc/' . $old_file)) @unlink('../images/mc/' . $old_file);
                        if (file_exists('../images/mc/' . 'min_' . $old_file)) @unlink('../images/mc/' . 'min_' . $old_file);
                        $file_path = '../images/mc/' . $img;
                        move_uploaded_file($_FILES['pic']['tmp_name'], $file_path);
                        @copy($file_path, '../images/mc/' . 'min_' . $img);
                        images::set_img_size('../images/mc/' . 'min_' . $img, 150, 95);
                    } else {
                        Template::set_page('master_classes_edit', 'Редактирование мастер-класса', $file_info);
                    }


                } else {

                    if ($fileExt === 'jpg' || $fileExt === 'jpeg') {
                        if (file_exists('../images/mc/' . $img)) @unlink('../images/mc/' . $img);
                        if (file_exists('../images/mc/' . 'min_' . $img)) @unlink('../images/mc/' . 'min_' . $img);
                        $file_path = '../images/mc/' . $img;
                        move_uploaded_file($_FILES['pic']['tmp_name'], $file_path);
                        @copy($file_path, '../images/mc/' . 'min_' . $img);
                        images::set_img_size('../images/mc/' . 'min_' . $img, 150, 95);
                    } elseif ($fileExt === 'gif') {
                        if (file_exists('../images/mc/' . $img)) @unlink('../images/mc/' . $img);
                        if (file_exists('../images/mc/' . 'min_' . $img)) @unlink('../images/mc/' . 'min_' . $img);
                        $file_path = '../mages/mc/' . $img;
                        move_uploaded_file($_FILES['pic']['tmp_name'], $file_path);
                        @copy($file_path, '../images/mc/' . 'min_' . $img);
                        images::set_img_size('../images/mc/' . 'min_' . $img, 150, 95);
                    } elseif ($fileExt === 'png') {
                        if (file_exists('../images/mc/' . $img)) @unlink('../images/mc/' . $img);
                        if (file_exists('../images/mc/' . 'min_' . $img)) @unlink('../images/mc/' . 'min_' . $img);
                        $file_path = '../images/mc/' . $img;
                        move_uploaded_file($_FILES['pic']['tmp_name'], $file_path);
                        @copy($file_path, '../images/mc/' . 'min_' . $img);
                        images::set_img_size('../images/mc/' . 'min_' . $img, 150, 95);
                    } else {
                        Template::set_page('master_classes_add', 'Добавление мастер-класса');
                    }
                }

            }

            mc::save_master_class($id, $name, $new_address, $series, $short_desc, $description, $date, $duration, $img, $visibility, $webinar);

            caching::delete('index_mcs');
        }
        $this->mc_list($args);
    }

    /**
     * Выбодит ворму редактирования мастер-класса.
     * @param <type> $args
     */
    function edit($args)
    {
        if (isset($args[0])) {
            $page_header = "Редактирование мастер-класса";
            $id = (int)$args[0];

            Template::set_page('master_classes_edit', $page_header, mc::get_master_class($id));
        } // Ввыводим список мастер-классов.
        else {
            $this->index($args);
        }
    }

    function subscribes($args)
    {
        if (isset($args[0])) {
            $page_header = "Записавшиеся на мастер-класс";
            $master_class_id = (int)$args[0];

            $subscribes = mc::get_subscribes_list($master_class_id);
            $master_class = mc::get_master_class($master_class_id);

            Template::set('subscribes', $subscribes);
            Template::set('master_class', $master_class);
            Template::set_page('master_class_subscribes', $page_header);
        } // Выводим список мастер классов
        else {

        }
    }

    function upload($args)
    {
        if (isset($args[0])) {
            $master_class = mc::get_master_class($args[0]);

            if (!$master_class) {
                $this->nopage();

                return;
            }

            $master_class_id = (int)$args[0];
        } else {
            $master_class_id = 0;
        }

        $excel = xls::get_master_class_subscribes(mc::get_subscribes_list($master_class_id), $master_class_id ? $master_class['name']  . ' ' . date('d.m.Y', strtotime($master_class['date'])) : null);

        export::download_excel($excel, 'mc');
    }

    function  mc_mail($args)
    {

        if (isset ($args[0])) {

            $id = (int)$args[0];
            $subscribe_list = mc::get_subscribes_list($id);
            $mc = mc::get_master_class($id);
            foreach ($subscribe_list as $man) {
                if ($man['source'] == 'roskosmetika.ru') {
                    $to = iconv('utf-8', 'koi8-r', $man['email']);
                    $from = '=?koi8-r?B?' . base64_encode(iconv('utf-8', 'koi8-r', 'Роскосметика')) . '?=' . ' <news@roskosmetika.ru>';

                    $subject = '=?koi8-r?B?' . base64_encode(iconv('utf-8', 'koi8-r', 'Напоминание о времени мастер-класса')) . '?=';
                    $message = iconv('utf-8', 'koi8-r', $this->to_roskosmetika($mc['name'], $mc['date']));

                    $header = "Content-type: text/html; charset=koi8-r\r\n";
                    $header .= "From: $from\r\n";
                    $header .= "MIME-Version: 1.0\r\n";
                    $header .= "Content-type: text/html; charset=koi8-r\r\n";

                    @mail($to, $subject, $message, $header);
                } elseif ($man['source'] == 'r-cosmetics.ru') {

                    $to = iconv('utf-8', 'koi8-r', $man['email']);
                    $from = '=?koi8-r?B?' . base64_encode(iconv('utf-8', 'koi8-r', 'R-cosmetics')) . '?=' . ' <news@r-cosmetics.ru>';

                    $subject = '=?koi8-r?B?' . base64_encode(iconv('utf-8', 'koi8-r', 'Напоминание о времени мастер-класса')) . '?=';
                    $message = iconv('utf-8', 'koi8-r', $this->to_rcosmetics($mc['name'], $mc['date']));

                    $header = "Content-type: text/html; charset=koi8-r\r\n";
                    $header .= "From: $from\r\n";
                    $header .= "MIME-Version: 1.0\r\n";
                    $header .= "Content-type: text/html; charset=koi8-r\r\n";

                    @mail($to, $subject, $message, $header);
                } else {
                    $to = iconv('utf-8', 'koi8-r', $man['email']);
                    $from = '=?koi8-r?B?' . base64_encode(iconv('utf-8', 'koi8-r', 'Велиния')) . '?=' . ' <contact@veliniya.ru>';

                    $subject = '=?koi8-r?B?' . base64_encode(iconv('utf-8', 'koi8-r', 'Напоминание о времени мастер-класса')) . '?=';
                    $message = iconv('utf-8', 'koi8-r', $this->to_veliniya($mc['name'], $mc['date']));

                    $header = "Content-type: text/html; charset=koi8-r\r\n";
                    $header .= "From: $from\r\n";
                    $header .= "MIME-Version: 1.0\r\n";
                    $header .= "Content-type: text/html; charset=koi8-r\r\n";

                    @mail($to, $subject, $message, $header);
                }

            }

            Template::set_page('index', 'Напоминание о мастер-классах', 'Уведомления записавшимся пользователям отправлены.');

        } else {
            $this->mc_list($args);
        }
    }

    function to_roskosmetika($name, $date)
    {

        if (date('w', strtotime($date)) == 0) {
            $day = 'воскресенье';
        } elseif (date('w', strtotime($date)) == 1) {
            $day = 'понедельник';
        } elseif (date('w', strtotime($date)) == 2) {
            $day = 'вторник';
        } elseif (date('w', strtotime($date)) == 3) {
            $day = 'среда';
        } elseif (date('w', strtotime($date)) == 4) {
            $day = 'четверг';
        } elseif (date('w', strtotime($date)) == 5) {
            $day = 'пятница';
        } else {
            $day = 'суббота';
        }
        $msg = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">
<HTML><HEAD>
<META content=\"text/html; charset=windows-1251\" http-equiv=Content-Type><BODY>
<META name=GENERATOR content=\"MSHTML 8.00.6001.23091\"></HEAD>
<BODY>
<TABLE border=0 cellPadding=9 width=\"100%\" align=center>
  <TBODY>
  <TR>
    <TD>
      <DIV style=\"MARGIN-LEFT: 18px\"><A href=\"" . DOMAIN_FULL . "\"
      target=_blank><IMG alt=\"Интернет-магазин косметики Роскосметика\"
      src=\"" . DOMAIN_FULL . "/images/logo.png\" width=254 height=57></A>
      </DIV></TD>
    <TD vAlign=top rowSpan=3 width=200>
      <TABLE width=\"100%\">
        <TBODY>
        <TR>
          <TD align=right><IMG
            src=\"" . DOMAIN_FULL . "/images/flowers/3.png\"> </TD></TR>
        <TR>
          <TD>
            <DIV
            style=\"BORDER-BOTTOM: #dfdfdf 1px solid; BORDER-LEFT: #dfdfdf 1px solid; MARGIN-BOTTOM: 20px; BORDER-TOP: #dfdfdf 1px solid; BORDER-RIGHT: #dfdfdf 1px solid\">
            <TABLE cellSpacing=0 cellPadding=0 width=\"100%\">
              <TBODY>
              <TR>
                <TD bgColor=#ed53dd>
                  <DIV
                  style=\"MARGIN-TOP: 8px; FONT-FAMILY: Arial,sans-serif; MARGIN-BOTTOM: 8px; MARGIN-LEFT: 10px; FONT-SIZE: 11pt\">Обратите
                  внимание:</DIV></TD></TR>
              <TR>
                <TD bgColor=#fcf6ff>
                  <DIV
                  style=\"LINE-HEIGHT: 12px; MARGIN-TOP: 10px; FONT-FAMILY: Arial, sans-serif; MARGIN-BOTTOM: 10px; MARGIN-LEFT: 10px; FONT-SIZE: 10pt\">
                  <P><A
                  style=\"FONT-FAMILY: Arial, sans-serif; COLOR: #a33888; FONT-SIZE: 10pt\"
                  href=\"" . DOMAIN_FULL . "/products/new/\"
                  target=_blank>Новинки магазина</A></P>
                  <P><A
                  style=\"FONT-FAMILY: Arial, sans-serif; COLOR: #a33888; FONT-SIZE: 10pt\"
                  href=\"" . DOMAIN_FULL . "/page/sales/\"
                  target=_blank>Скидки в нашем магазине</A></P>
                  <P><A
                  style=\"FONT-FAMILY: Arial, sans-serif; COLOR: #a33888; FONT-SIZE: 10pt\"
                  href=\"" . DOMAIN_FULL . "/master_classes/\"
                  target=_blank>Обучающие мастер-классы</A></P>
                  <P><A
                  style=\"FONT-FAMILY: Arial, sans-serif; COLOR: #a33888; FONT-SIZE: 10pt\"
                  href=\"" . DOMAIN_FULL . "/price/\" target=_blank>Наш
                  прайс-лист</A></P></DIV></TD></TR></TBODY></TABLE></DIV></TD></TR>
       
       </TBODY></TABLE></TD></TR>
  <TR>
    <TD align=middle>
      <DIV style=\"TEXT-ALIGN: center; MARGIN-TOP: 15px; MARGIN-BOTTOM: 20px\"><A
      style=\"TEXT-ALIGN: center; FONT-FAMILY: Arial, sans-serif; COLOR: #a33888; FONT-SIZE: 10pt\"
      href=\"" . DOMAIN_FULL . "/category/34\"
      target=_blank> Водоросли  для обертывания </A> <A
      style=\"TEXT-ALIGN: center; FONT-FAMILY: Arial, sans-serif; COLOR: #a33888; MARGIN-LEFT: 5px; FONT-SIZE: 10pt\"
      href=\"" . DOMAIN_FULL . "/category/17\"
      target=_blank> Химические пилинги </A> <A
      style=\"TEXT-ALIGN: center; FONT-FAMILY: Arial, sans-serif; COLOR: #a33888; MARGIN-LEFT: 5px; FONT-SIZE: 10pt\"
      href=\"" . DOMAIN_FULL . "/category/11\"
      target=_blank> Альгинатные маски </A> <A
      style=\"TEXT-ALIGN: center; FONT-FAMILY: Arial, sans-serif; COLOR: #a33888; MARGIN-LEFT: 5px; FONT-SIZE: 10pt\"
      href=\"" . DOMAIN_FULL . "/category/22\"
      target=_blank> Шоколадная косметика </A> <A
      style=\"TEXT-ALIGN: center; FONT-FAMILY: Arial, sans-serif; COLOR: #a33888; MARGIN-LEFT: 5px; FONT-SIZE: 10pt\"
      href=\"" . DOMAIN_FULL . "/category/10\"
      target=_blank> Активные сыворотки </A> </DIV></TD></TR>
  <TR>
    <TD vAlign=top>
      <DIV style=\"MARGIN-BOTTOM: 5px; MARGIN-LEFT: 18px; MARGIN-RIGHT: 10px\">
      <H1
      style=\"FONT-FAMILY: Arial, sans-serif; COLOR: #000000; FONT-SIZE: 14pt; FONT-WEIGHT: normal\">Дорогой
      клиент!</H1>
      <P style=\"FONT-FAMILY: Arial, sans-serif; FONT-SIZE: 10pt\">Напоминаем, что
      Вы записались на мастер-класс <B>\"$name\"</B>,
      который состоится " . date('d.m.Y в H:i', strtotime($date)) . " ($day).</P>
      <P style=\"FONT-FAMILY: Arial, sans-serif; FONT-SIZE: 10pt\">Мастер-классы
      проходят в Москве. Адрес: ул. Василия Петушкова, д. 8, офис \"Седьмое небо\",
      метро \"Тушинская\" (последний вагон из центра). Из стеклянных дверей - направо.
      Выход в город налево вверх по лестнице. Пройти к автобусной остановке у обувного магазина \"Терволина\",
      напротив главного входа на \"Тушинский вещевой рынок\". Далее автобусом номер 88, 777 или маршрутным такси номер 475 до остановки \"Спортивная школа\".
      Пройти направо по тропинке через рощу. Далее через мост к д.8 (здание бывшей трикотажной фабрики).
      3-й подъезд. Вход через проходную. Наш офис находится на 4 этаже, направо по коридору до конца.
      Обязательно возьмите с собой паспорт для оформления пропуска.<BR>
      Если у Вас есть дополнительные вопросы, позвоните нам по телефону 8 800 775-54-83 (Звонок по России бесплатный).</P>
      <P style=\"FONT-FAMILY: Arial, sans-serif; FONT-SIZE: 10pt\">Расписание
      следующих мастер-классов и запись на них <A style=\"COLOR: #a33888\"
      href=\"" . DOMAIN_FULL . "/master_classes/\"
      target=_blank>здесь</A>.</P>
      <P style=\"FONT-FAMILY: Arial, sans-serif; FONT-SIZE: 10pt\"><I>До скорого,
      Ваша Роскосметика!</I></P></DIV>
  <TR>
    <TD colSpan=2>
      <TABLE width=\"100%\">
        <TBODY>
        <TR>
          <TD vAlign=bottom>
            <DIV style=\"MARGIN-BOTTOM: 5px; MARGIN-LEFT: 18px\"><A
            style=\"FONT-FAMILY: Arial, sans-serif; COLOR: #A33888; FONT-SIZE: 11pt\"
            href=\"" . DOMAIN_FULL . "\"
            target=_blank>www.roskosmetika.ru</A> </DIV></TD>
          <TD align=right>
            <DIV
            style=\"MARGIN-TOP: 5px; COLOR: #A33888; MARGIN-BOTTOM: 5px; MARGIN-RIGHT: 18px\">
            <P
            style=\"TEXT-ALIGN: right; MARGIN-TOP: 5px; FONT-FAMILY: Arial, sans-serif; MARGIN-BOTTOM: 5px; FONT-SIZE: 13pt\">8 800 775-54-83 (Звонок по России бесплатный)</P></DIV></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></BODY></HTML>";

        return $msg;
    }


    function to_rcosmetics($name, $date)
    {

        if (date('w', strtotime($date)) == 0) {
            $day = 'воскресенье';
        } elseif (date('w', strtotime($date)) == 1) {
            $day = 'понедельник';
        } elseif (date('w', strtotime($date)) == 2) {
            $day = 'вторник';
        } elseif (date('w', strtotime($date)) == 3) {
            $day = 'среда';
        } elseif (date('w', strtotime($date)) == 4) {
            $day = 'четверг';
        } elseif (date('w', strtotime($date)) == 5) {
            $day = 'пятница';
        } else {
            $day = 'суббота';
        }

        $msg = "<HTML><HEAD>
<META content=\"text/html; charset=windows-1251\" http-equiv=Content-Type>
<META name=GENERATOR content=\"MSHTML 8.00.6001.23091\"></HEAD>
<BODY style=\"MARGIN: 0px\">
<TABLE border=0 cellSpacing=0 cellPadding=0 width=\"98%\" align=center>
<TBODY>
<TR>
<TD colSpan=3>
<TABLE border=0 cellSpacing=0 cellPadding=0 width=\"100%\">
<TBODY>
<TR>
<TD height=116 align=left>
<DIV style=\"MARGIN-BOTTOM: -5px\"><IMG style=\"DISPLAY: block\" alt=\"\" src=\"http://www.r-cosmetics.ru/images/mail/rcosmetics1.png\" width=495 height=116></DIV></TD>
<TD align=right><A href=\"http://www.r-cosmetics.ru/\" target=_blank><IMG style=\"DISPLAY: block\" alt=R-cosmetics.ru src=\"http://www.r-cosmetics.ru/images/mail/rcosmetics2.png\" width=305 height=116></A></TD></TR></TBODY></TABLE></TD></TR>
<TR>
<TD vAlign=top rowSpan=2 width=176><IMG alt=\"\" src=\"http://www.r-cosmetics.ru/images/mail/rcosmetics3.png\" width=176 height=211></TD>
<TD align=middle>
<DIV style=\"TEXT-ALIGN: center; LINE-HEIGHT: 18px; MARGIN: 2px\"><A style=\"TEXT-ALIGN: center; FONT-FAMILY: Arial, sans-serif; COLOR: #8ac350; MARGIN-LEFT: 5px; FONT-SIZE: 10pt\" href=\"http://www.r-cosmetics.ru/category/102\" target=_blank>Живые&nbsp;водоросли</A> <A style=\"TEXT-ALIGN: center; FONT-FAMILY: Arial, sans-serif; COLOR: #8ac350; MARGIN-LEFT: 5px; FONT-SIZE: 10pt\" href=\"http://www.r-cosmetics.ru/category/104\" target=_blank>Активные&nbsp;маски</A> <A style=\"TEXT-ALIGN: center; FONT-FAMILY: Arial, sans-serif; COLOR: #8ac350; MARGIN-LEFT: 5px; FONT-SIZE: 10pt\" href=\"http://www.r-cosmetics.ru/category/106\" target=_blank>Шоколадная&nbsp;косметика</A> <A style=\"TEXT-ALIGN: center; FONT-FAMILY: Arial, sans-serif; COLOR: #8ac350; MARGIN-LEFT: 5px; FONT-SIZE: 10pt\" href=\"http://www.r-cosmetics.ru/category/107\" target=_blank>Голубая&nbsp;глина</A> <A style=\"TEXT-ALIGN: center; FONT-FAMILY: Arial, sans-serif; COLOR: #8ac350; MARGIN-LEFT: 5px; FONT-SIZE: 10pt\" href=\"http://www.r-cosmetics.ru/category/105\" target=_blank>Альгинатные&nbsp;маски</A> </DIV></TD>
<TD rowSpan=2 width=175>&nbsp;&nbsp;</TD></TR>
<TR>
<TD>
<DIV style=\"MARGIN: 20px 10px 40px 18px\">
<DIV style=\"MARGIN-BOTTOM: 40px; MARGIN-LEFT: 18px; MARGIN-RIGHT: 10px\">
<H1 style=\"FONT-FAMILY: Arial, sans-serif; COLOR: #000000; FONT-SIZE: 14pt; FONT-WEIGHT: normal\">Дорогой клиент!</H1>
<P style=\"FONT-FAMILY: Arial, sans-serif; FONT-SIZE: 10pt\">Напоминаем, что Вы записались на мастер-класс <B>\"$name\"</B>, который состоится " . date('d.m.Y в H:i', strtotime($date)) . " ($day).</P>
<P style=\"FONT-FAMILY: Arial, sans-serif; FONT-SIZE: 10pt\">Мастер-классы проходят в Москве.Адрес: ул. Василия Петушкова, д. 8, офис \"Седьмое небо\",  метро \"Тушинская\" (последний вагон из центра). Из стеклянных дверей - направо. Выход в город налево вверх по лестнице. Пройти к автобусной остановке у обувного магазина \"Терволина\", напротив главного входа на \"Тушинский вещевой рынок\". Далее автобусом номер 88, 777 или маршрутным такси номер 475 до остановки \"Спортивная школа\".  Пройти направо по тропинке через рощу. Далее через мост к д.8 (здание бывшей трикотажной фабрики). 3-й подъезд. Вход через проходную. Наш офис находится на 4 этаже, направо по коридору до конца. Обязательно возьмите с собой паспорт для оформления пропуска.<br>Если у Вас есть дополнительные вопросы, позвоните нам по телефону (+7 495) 225-54-83.</P>
<P style=\"FONT-FAMILY: Arial, sans-serif; FONT-SIZE: 10pt\">Расписание следующих мастер-классов и запись на них <A style=\"COLOR: #8ac350\" href=\"http://r-cosmetics.ru/master_classes\" target=_blank>здесь</A>.</P>
<P>&nbsp;</P>       
</DIV></DIV></TD></TR></TBODY></TABLE>
<DIV></DIV></TD></TR><TR><TD colspan=\"3\">
<TABLE border=0 width=\"100%\" height=108>
<TBODY>
<TR>
<TD vAlign=bottom>
<DIV style=\"MARGIN-BOTTOM: 32px; MARGIN-LEFT: 47px\"><A style=\"FONT-FAMILY: Tahoma, sans-serif; COLOR: #8ab255; FONT-SIZE: 14pt\" href=\"http://www.r-cosmetics.ru/\" target=_blank ?>www.R-сosmetics.ru</A> </DIV></TD>
<TD align=right>
<DIV style=\"MARGIN-TOP: 54px; MARGIN-BOTTOM: 32px; MARGIN-RIGHT: 29px\">
<P style=\"TEXT-ALIGN: right; MARGIN-TOP: 5px; FONT-FAMILY: Tahoma, sans-serif; MARGIN-BOTTOM: 5px; COLOR: #8ab255; FONT-SIZE: 14pt\">Москва: (495) 225-54-83</P>
<P style=\"TEXT-ALIGN: right; MARGIN-TOP: 5px; FONT-FAMILY: Tahoma, sans-serif; MARGIN-BOTTOM: 5px; COLOR: #8ab255; FONT-SIZE: 14pt\">Санкт-Петербург: (812) 640-72-24</P></DIV></TD></TR></TBODY></TABLE></TD></TR></TABLE></BODY></HTML>";

        return $msg;
    }

    function to_veliniya($name, $date)
    {


        if (date('w', strtotime($date)) == 0) {
            $day = 'воскресенье';
        } elseif (date('w', strtotime($date)) == 1) {
            $day = 'понедельник';
        } elseif (date('w', strtotime($date)) == 2) {
            $day = 'вторник';
        } elseif (date('w', strtotime($date)) == 3) {
            $day = 'среда';
        } elseif (date('w', strtotime($date)) == 4) {
            $day = 'четверг';
        } elseif (date('w', strtotime($date)) == 5) {
            $day = 'пятница';
        } else {
            $day = 'суббота';
        }


        $msg = "
<html>
                    <head>
                        <title>Заявка на мастер-класс.</title>
                        <meta http-equiv=\"Content-type\" content=\"text/html; charset=koi8-r\">
                    </head>
                    <body>
         <div>
        <p>
        <a target=\"blank\" href=\"http://www.veliniya.ru/\"><img width=\"243\" alt=\"veliniya.ru\"
        src=\"http://veliniya.ru/images/logo_old.jpg\" style=\"DISPLAY: block;\" height=\"37\"></a>
        </p>
             <H1 style=\"FONT-FAMILY: Arial, sans-serif; COLOR: #000000; FONT-SIZE: 14pt; FONT-WEIGHT: normal\">Дорогой клиент!</H1>
             <p>Напоминаем, что Вы записались на мастер-класс <B>\"$name\"</B>, который состоится " . date('d.m.Y в H:i', strtotime($date)) . " ($day).
               <br>
                  Адрес: ул. Василия Петушкова, д. 8, офис&nbsp;\"Седьмое небо\",
                <br>
                метро \"Тушинская\" (последний вагон из центра). Из стеклянных дверей - направо.
                <br>
                Выход в город налево вверх по лестнице. Пройти к автобусной остановке у обувного магазина \"Терволина\",
                <br>
                напротив главного входа на \"Тушинский вещевой рынок\". Далее автобусом номер 88, 777 или маршрутным такси номер 475 до остановки \"Спортивная школа\".
                <br>
                Пройти направо по тропинке через рощу. Далее через мост к д.8 (здание бывшей трикотажной фабрики).
               <br>
                3-й подъезд. Вход через проходную. Наш офис находится на 4 этаже, направо по коридору до конца.
               <br>
                Обязательно возьмите с собой паспорт для оформления пропуска.
                <br>
                <br>
                <strong><span style=\"color:red\">Внимание</span></strong>. Запись на все мастер-классы производится Вами самостоятельно, без дополнительного уведомления по телефону с нашей стороны.
                <br>
                Если Вы все-таки не можете придти - не беда, мы будем рады Вас видеть на других мастер-классах. Для этого достаточно еще раз записаться.
                <br>
                Если у Вас есть дополнительные вопросы, позвоните нам по  телефону <strong>(495) 225-54-83</strong>.
                <br>
                <br>
                Ждем Вас на мастер-классе!
                </p>
                <p>
                <a target=\"blank\" href=\"http://www.veliniya.ru/\" style=\"FONT-FAMILY: Tahoma, sans-serif; COLOR: #blue; FONT-SIZE:
                14pt\">www.veliniya.ru</a>
                </p>
                </div></body></html>";

        return $msg;
    }


}

?>