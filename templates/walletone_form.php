<?php
/**
 * @var array $order
 * @var string $formID
 * @var string $label
 */
if (!isset($label)) {
    $label = 'Оплатить';
}
?>
<form method="post" action="https://wl.walletone.com/checkout/checkout/Index" class="payment-form"<?= isset($formID) ? ' id="' . $formID . '"' : '' ?> target="_blank">
    <?= Template::walletoneFormFields($order) ?>
    <button type="submit" class="submit-pay"><?= $label ?><span
            class="pointer_cart"></span></button>
</form>
