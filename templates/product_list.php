<?php
/**
 * @var array $products
 * @var bool $no_description
 * @var bool $noindex_description
 * @var string $product_type
 * @var int $product_type_n
 */
$no_description = !empty($no_description) ? true : null;
$noindex_description = isset($noindex_description) ? true : null;
$product_type = !empty($product_type) ? $product_type : null;
$product_type_n = !empty($product_type_n) ? (int)$product_type_n : 0;

?>
<div class="row">
    <div class="col-xs-12">
        &nbsp;
    </div>
</div>

<div class="products-loading<?= $product_type ? ' typed-products' : '' ?>">
    <div class="row">
        <?php
        for ($i = 0, $n = count($products); $i < $n; ++$i) {
            Template::partial(
                'product_in_list',
                [
                    'product' => $products[$i],
                    'no_description' => $no_description,
                    'noindex_description' => $noindex_description,
                    'product_type' => $product_type && $product_type_n > $i ? $product_type : null,
                ]
            );

            if (!(($i + 1) % 4)) {
        ?>
                </div><div class="row">
        <?php
            }
        }
        ?>
    </div>
</div>
