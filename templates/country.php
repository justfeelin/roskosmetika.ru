<?php
/**
 * @var array $content
 */
Template::partial(
    'breadcrumbs',
    [
        'crumbs' => [
            [
                'name' => 'Страны-производители',
            ],
        ],
    ]
);
?>
<div id="country" class="row">
    <h1>Страны-производители</h1>

    <div class="row">
        <?php for ($i = 0, $n = count($content['countries']); $i < $n; ++$i) { ?>
            <div class="col-xs-3 country-item">
                <div class="box">
                    <?php if ($content['countries'][$i]['image']) { ?>
                        <a href="/country/<?= $content['countries'][$i]['url'] ?>">
                            <img src="/images/countries/<?= $content['countries'][$i]['image'] ?>" width="50" height="50">
                        </a>
                    <?php } ?>
                    <a href="/country/<?= $content['countries'][$i]['url'] ?>" class="link">
                        <?= $content['countries'][$i]['h1'] ?>
                    </a>
                    <div class="clearfix"></div>
                </div>
            </div>
        <?php
            if (!(($i + 1) % 4)) {
        ?>
            </div>
            <div class="row">
        <?php
            }
        }
        ?>
    </div>
</div>
