<div class="col-xs-6">
    <div class="pay ps_short">
        <div class="row">
            <h4 class="col-xs-12 pay_title">К&nbsp;оплате&nbsp;принимаем:</h4>
        </div>
        <div class="row pay_systems">
            <p class="col-xs-2 web_money"></p>

            <p class="col-xs-2 ya_money"></p>

            <p class="col-xs-2 visa"></p>

            <p class="col-xs-2 mastercard"></p>

            <p class="col-xs-2 maestro"></p>

            <p class="col-xs-2 mir"></p>
        </div>
    </div>
    <div class="shipping">
        <div class="row">
            <h4 class="col-xs-12 shipping_title">Мы&nbsp;доставляем:</h4>
        </div>
        <div class="row shipping_comp">
            <p class="col-xs-2 ppoint"></p>

            <p class="col-xs-2 post_rf"></p>

            <p class="col-xs-2 ems"></p>

            <p class="col-xs-2 sdek"></p>
        </div>
    </div>
</div>
