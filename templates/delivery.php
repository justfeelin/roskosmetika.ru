<?php
/**
 * @var string $header
 * @var int $active_aside
 * @var array $msg
 * @var array $values
 * @var array $regions
 */
?>
<section class="room" id="room-delivery">
    <div class="row">
        <h1 class="col-xs-6 col-xs-offset-3"><?= $header ?></h1>
    </div>
    <div class="row">
        <?php
        Template::partial(
            'room_left_menu',
            [
                'active_aside' => $active_aside,
            ]);
        ?>
        <div class="col-xs-6">
            <form action="/room/delivery" method="POST">
                <div class="form-group row <?= isset($msg['phone']) ? 'has-error' : '' ?>">
                    <p class="col-xs-3">Телефон:</p>

                    <div class="col-xs-9">
                        <input type="text" id="phone" name="phone" value="<?= !empty($values['phone']) ? $values['phone'] : '' ?>"
                               class="form-control">
                        <?= isset($msg['phone']) ? '<p class="form_error">' . $msg['phone'] . '</p>' : '' ?>
                    </div>
                </div>

                <div class="form-group row">
                    <p class="col-xs-3">Ваш&nbsp;регион:</p>

                    <div class="col-xs-9">
                        <select name="region" class="form-control">
                            <option></option>
                            <?php for ($i = 0, $n = count($regions); $i < $n; ++$i) { ?>
                            <option value="<?= $regions[$i]['id'] ?>" <?= $regions[$i]['id'] == $values['region_id'] ? 'selected="selected"' : '' ?>><?= $regions[$i]['name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group row <?= isset($msg['address']) ? 'has-error' : '' ?>">
                    <p class="col-xs-3">Адрес&nbsp;доставки:</p>

                    <div class="col-xs-9">
                        <textarea name="address" class="form-control" id="delivery-address"
                                  rows="8"><?= !empty($values['address']) ? $values['address'] : '' ?></textarea>
                        <?php if (isset($msg['address'])) { ?>
                            <p class="form_error"><?= $msg['address'] ?></p>
                        <?php } ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-3"></div>
                    <button type="submit" name="submit" class="col-xs-9 form_save">Сохранить</button>
                </div>
            </form>
        </div>
        <div class="col-xs-3">
        </div>
    </div>
    <?php Template::partial('mind_box', [
                    'mb_is_auth' => $is_auth,
                    'mb_top_view' => $mb_top_view,
                ]); ?>
</section>
