<?php
/**
 * @var array $content
 */
?>
<?php
if ($content) {
    $n = count($content);

    $hasCopyAreas = false;
?>
    <div class="row">
        <div class="banner col-xs-12">
            <?php for ($i = 0; $i < $n; ++$i) { ?>
                <img class="banner_<?= $i ?>" src="<?= $content[$i]['file'] ?>" alt="<?= $content[$i]['alt'] ?>" width="<?= $content[$i]['width'] ?>"
                     height="<?= $content[$i]['height'] ?>" usemap="#active_link_<?= $i ?>"/>

                <?php
                if ($content[$i]['copy_area'] && $content[$i]['copy_area_text']) {
                    $coords = explode(',', $content[$i]['copy_area']);

                    if (count($coords) === 4) {
                        $hasCopyAreas = true;
                ?>
                        <div title="Нажмите чтобы скопировать" style="<?= $i === 0 ? 'display: block; ' : '' ?>left: <?= $coords[0] ?>px; top: <?= $coords[1] ?>px; width: <?= $coords[2] ?>px; height: <?= $coords[3] ?>px;" class="banner-copy-area banner_<?= $i ?>" data-clipboard-text="<?= h($content[$i]['copy_area_text']) ?>"></div>
            <?php
                    }
                }
            }
            ?>
            <div class="row">
                <div class="col-xs-3 banner_menu">
                    <?php for ($i = 0; $i < $n; ++$i) { ?>
                        <p class="banner_<?= $i . ($i === 0 ? ' active_banner_menu' : '') ?>"><?= $content[$i]['menu_tag'] ?></p>
                    <?php } ?>
                </div>
                <?php for ($i = 0; $i < $n; ++$i) { ?>
                    <map class="banner_<?= $i ?>" name="active_link_<?= $i ?>">
                        <?php
                        if ($content[$i]['map_area']) {
                            echo hideLinks($content[$i]['map_area'], true);
                        } else { ?>
                        <area <?= $content[$i]['blank'] ? 'target="_blank"' : 'target="_self"' ?> class="hidden_link" shape="rect" coords="250,0,1000,285" alt="<?= $content[$i]['alt'] ?>"
                              data-link="<?= Template::hidelink($content[$i]['link']) ?>">
                        <?php } ?>
                    </map>
                <?php } ?>
            </div>
        </div>
    </div>
<?php
    if ($hasCopyAreas) {
        Template::add_script('../assets/zeroclipboard/ZeroClipboard.min.js');
    }
}
