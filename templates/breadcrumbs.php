<?php
/**
 * @var array $crumbs
 */
?>
<div class="row" itemscope itemtype="http://schema.org/WebPage" id="breadcrumbs">
    <div class="col-xs-12">
        <a itemprop="breadcrumb" href="/">Главная</a>
        <?php for ($i = 0, $n = count($crumbs); $i < $n; ++$i) { ?>
            <?php if (isset($crumbs[$i]['one_level'])) { ?>
            &nbsp;|&nbsp;
            <?php } else { ?>
            &mdash;
            <?php } ?>    
            <?php if ($i === $n - 1) { ?>
                <span itemprop="breadcrumb"><?= $crumbs[$i]['name'] ?></span>
            <?php } else { ?>
                <a itemprop="breadcrumb" href="<?= $crumbs[$i]['url'] ?>"><?= $crumbs[$i]['name'] ?></a>
            <?php } ?>
        <?php } ?>
    </div>
</div>
