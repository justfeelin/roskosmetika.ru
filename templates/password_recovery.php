<div class="modal fade" id="psw-recover-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="true">
    <div class="modal-dialog modal__frame">
        <div class="modal-content modal__content">
            <form class="form-horizontal" role="form" id="psw-recover-form">
                <div class="modal-header modal__header">
                    <button type="button" class="close modal__close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <p class="modal-title modal__title" id="myModalLabel">
                        Забыли пароль?
                    </p>
                </div>
                <div class="modal-body modal__body">
                    <div class="row modal__row">
                        <div class="col-xs-9 col-xs-offset-3 modal__notes">Введите Ваш e-mail, куда будет выслан пароль.</div>
                    </div>
                    <div class="form-group modal__row">
                        <label for="prf_email" class="col-xs-3 control-label modal__label">Email:</label>
                        <div class="col-xs-9">
                            <input type="email" class="form-control form-email" id="prf_email">
                        </div>
                    </div>
                </div>
                <div class="modal-footer modal__footer">
                    <button type="submit" class="btn btn-primary button-common">Восстановить&nbsp;пароль</button>
                </div>
            </form>
        </div>
    </div>
</div>
