<div class="modal fade" id="for-proff-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="true">
    <div class="modal-dialog modal__frame">
        <div class="modal-content modal__content">
            <form class="form-horizontal" role="form">
                <div class="modal-header modal__header">
                    <button type="button" class="close modal__close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <p class="modal-title modal__title" id="myModalLabel">
                        Узнать цену
                    </p>
                </div>
                <div class="modal-body modal__body">
                    <div class="form-group modal__row">
                        <div class="col-xs-9 pre_p_name"></div>
                        <div class="col-xs-3 pre_p_img"><img width="85" height="85" src=""></div>
                    </div>
                    <div class="error"></div>
                    <div class="form-group modal__row">
                        <label for="name" class="col-xs-4 control-label modal__label">Как Вас зовут? -<span class="form-required">*</span></label>
                        <div class="col-xs-8">
                            <input type="text" name="name" class="form-control l-name" required="required">
                        </div>
                    </div>
                    <div class="form-group modal__row">
                        <label for="email" class="col-xs-4 control-label modal__label">Email:<span class="form-required">*</span></label>
                        <div class="col-xs-8">
                            <input type="email" name="email" class="form-control l-email" required="required">
                        </div>
                    </div>
                    <div class="form-group modal__row">
                        <label for="phone" class="col-xs-4 control-label modal__label">Номер телефона:</label>
                        <div class="col-xs-8">
                            <input type="tel" name="phone" class="form-control l-phone">
                        </div>
                    </div>
                    <input type="hidden" name="product_id" class="prod-id">
                    <input type="hidden" name="pack_id" class="pack-id">
                    <div class="row modal__row">
                        <div class="col-xs-12  modal__notes">Цена данного товара доступна только после регистрации.<br><span class="form-required">*</span> - обязательные поля</div>
                    </div>
                </div>
                <div class="modal-footer modal__footer">
                    <button type="submit" class="btn btn-primary button-common" name="submit">Узнать цену</button>
                </div>
            </form>
        </div>
    </div>
</div>
