<?php
/**
 * @var array $content
 */

Template::partial(
    'product_filter',
    [
        'filters' => $content['filters'],
        'name' => isset($content['name']) ? $content['name'] : '',
        'categories_html' => isset($content['categories_html']) ? $content['categories_html'] : '',
        'found' => isset($content['found']) ? $content['found'] : '',
    ],
    true
);
