<?php
/**
 * @var array $content
 */
?>
<div class="row">
    <div class="col-xs-12">
        <h1 class="page_h1">Оплата заказа</h1>
        <?php
        if ($content['paid']) {
            if ($content['certificates']) {
        ?>
                <p>
                    Вы успешно оплатили сертификат!
                    <?php if ($content['emailCertificates']) { ?>
                        Для вашего удобства мы отправили копию на вашу почту.
                    <?php } ?>
                </p>

                <?php if ($content['certificates'] === true) { ?>
                    Наш менеджер обязательно свяжется с Вами в ближайшее время для уточнения деталей.
                <?php
                } else {
                    for ($i = 0, $n = count($content['certificates']); $i < $n; ++$i) {
                ?>
                        <p class="text-center">
                            <img src="<?= DOMAIN_FULL . '/' . promo_codes::CERTIFICATE_DIR . '/' . rawurlencode($content['certificates'][$i]) ?>">
                        </p>
            <?php
                    }
                }
            } else {
            ?>
                Ваш заказ успешно оплачен!
        <?php
            }
        } else {
        ?>
            Ваш платёж поступил в обработку.
            <br>
            <br>
            Вы можете обновить страницу через минуту, чтобы обновить состояние платежа.
        <?php } ?>
    </div>
</div>
