<?php
/**
 * @var array $content
 */
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <h1 style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 10px;margin-bottom: 10px;"><?= $content['surname'] ?>
                &nbsp;<?= $content['name'] . '&nbsp;' . $content['patronymic'] ?>, здравствуйте!</h1>

            <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 12px;">
                Вы оформили заявку на оповещение в интернет-магазине "Роскосметика". Дата
                оформления: <?= $content['pre_order_date'] ?>. Как только товар появится на складе мы сообщим Вам
                <?=($content['check_email'])? 'по электронной почте ':''?><?=($content['check_email'] && $content['check_sms'])? 'и ':''?><?=($content['check_sms'])? 'по смс, на номер '.$content['phone']:''?>. Время ожидания не более месяца.</p>
        </td>
    </tr>
    <tr>
        <td>
            <table width="600" border="0" cellspacing="0" cellpadding="0">
                <tr height="28">
                    <td width="500" bgcolor="#8dc655"
                        style="padding-left:10px; font-family: Tahoma, Geneva, sans-serif;  font-size: 13px; color: #FFF; text-align: left; text-decoration: none; vertical-align: middle;">
                        Товар в заявке
                    </td>
                    <td width="100" bgcolor="#8dc655"
                        style="font-family: Tahoma, Geneva, sans-serif; font-size: 13px; color: #FFF; text-align: left; text-decoration: none; vertical-align: middle;">
                        Цена
                    </td>
                </tr>
                <tr>
                    <td width="500" style="padding-left:10px; padding-right:10px">
                        <table width="480" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="100"><p style="margin-top: 12px;margin-bottom: 6px;">
                                        <a href="<?= DOMAIN_FULL ?>/product/<?= $content['url'] ?>"
                                           target="_blank"><img
                                                src="<?= IMAGES_DOMAIN_FULL ?>/images/prod_photo/<?= Product::img_url('min_' . $content['src'], $content['url']) ?>"
                                                width="85" height="85"
                                                alt="<?= $content['prod_name'] ?>"/></a></p></td>
                                <td width="380"><p
                                        style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 6px;">
                                        <a href="<?= DOMAIN_FULL ?>/product/<?= $content['url'] ?>"
                                           style="color: #9b1d97;"
                                           target="_blank"><?= $content['prod_name'] ?></a>,&nbsp;<?= $content['pack'] ?>
                                    </p>

                                    <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 10px;color: #1B1B1B;margin-top: 6px;margin-bottom: 6px;">
                                        Артикул:&nbsp;<?= $content['prod_id'] ?>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="100">
                        <?php if ($content['available'] && (int)$content['special_price'] > 0) { ?>
                        <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 6px;">
                            <s><?= formatPrice($content['price']) ?></s>&nbsp;р.</p>
                        <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;margin-top: 6px;margin-bottom: 6px;color:red;"><?= formatPrice($content['special_price']) ?>
                            &nbsp;р.</p>
                        <?php } else { ?>
                        <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 6px;"><?= formatPrice($content['price']) ?>
                            &nbsp;р.</p>
                        <?php } ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
