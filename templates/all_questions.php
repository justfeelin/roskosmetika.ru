<?php
/**
 * @var string $header
 * @var array $content
 * @var array $month
 * @var int $qantity_pages
 * @var int $current_page
 */
if (!empty($crumb)) {
    Template::partial(
        'breadcrumbs',
        [
            'crumbs' => [
                [
                    'name' => $crumb,
                ],
            ],
        ]
    );
}
?>
<section class="row row-container">
    <h1 class="page_h1"><?= $header ?></h1>

    <div class="row">
        <button class="btn btn-primary button-common center-block question__submit">Задать&nbsp;вопрос</button>
    </div>
    <?php
    $n = count($content['questions']);

    if ($n !== 0) {
        for ($i = 0; $i < $n; ++$i) {
    ?>
            <span itemscope itemtype="http://schema.org/Question">
                <div class="row">
                    <div class="col-xs-8">
                        <div class="prod_reviewer"><span itemprop="author"><?= $content['questions'][$i]['name'] ?></span>
                            <time itemprop="dateCreated" class="review_date_city"
                                  datetime="<?= Template::date('Y-m-d H:i+04:00', $content['questions'][$i]['date']) ?>">,&nbsp;<?= $content['questions'][$i]['day'] ?>
                                &nbsp;<?= $month[$content['questions'][$i]['month']] ?></time>
                        </div>
                        <div class="question_box" itemprop="text"><?= $content['questions'][$i]['question'] ?></div>
                    </div>
                    <span class="col-xs-4"></span>
                </div>
                <div class="row answer" itemprop="acceptedAnswer" itemscope itemtype="http://schema.org/Answer">
                    <span class="col-xs-4"></span>

                    <div class="col-xs-8">
                        <div class="prod_reviewer align_right" itemprop="author" itemscope itemtype="http://schema.org/Person">
                            <span itemprop="name">Светлана&nbsp;Юдина</span>,&nbsp;<span class="review_date_city"
                                                                                         itemprop="jobTitle">Эксперт</span>
                        </div>
                        <div class="answer_box" itemprop="text"><?= $content['questions'][$i]['answer'] ?></div>
                    </div>
                </div>
            </span>
    <?php } ?>
        <div class="row align_center">
            <?php
            if ($qantity_pages !== 1) {
                for ($i = 1, $n = $qantity_pages + 1; $i < $n; ++$i) {
                    if ($i === $current_page) {
            ?>
                        <b class="review_active"><?= $i ?></b>
                    <?php } else { ?>
                        <a class="link"
                           href="/question<?= $i > 1 ? '/' . $i : '' ?>"><?= $i ?></a>

            <?php
                    }
                }
            }
            ?>
        </div>
    <?php } else { ?>
        <div class="no_items">Вопросы косметологу еще не задавали</div>
    <?php
    }

    Template::partial('mind_box', [
                    'mb_is_auth' => $is_auth,
                    'mb_top_view' => $mb_top_view,
                ]);

    Template::partial('cosmetolog_question_box');
    ?>
</section>
