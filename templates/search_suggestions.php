<?php
/**
 * @var array $suggestions
 * @var string $search
 */
if (!$suggestions['found']) { ?>
    <h4>Ничего не найдено</h4>
    <p>Популярные товары</p>
<?php
}

if ($suggestions['products']) {
?>
<div class="suggestion-block ua-product-list" data-list="<?= $suggestions['found'] ? 'Поисковые подсказки' : 'Поисковые подсказки: популярные' ?>">
    <?php if ($suggestions['found']) { ?>
        <h5>
            <span>В товарах</span>
            <?php if (!empty($suggestions['counts']['products']) && $suggestions['counts']['products'] > 1) { ?>
                <a href="/search?search=<?= $search ?>#products" class="show-all-btn">Показать все (<?= $suggestions['counts']['products'] ?>)</a>
            <?php } ?>
        </h5>
    <?php
        }

    for ($i = 0, $n = count($suggestions['products']); $i < $n; ++$i) {
    ?>
        <div class="suggestion-item products-item product-in-list">
            <div class="hidden product-tm"><?= $suggestions['products'][$i]['tm_url'] ?></div>
            <div class="hidden cart_info variant" data-id="<?= $suggestions['products'][$i]['id'] ?>">
                <div class="pack-name"><?= $suggestions['products'][$i]['pack'] ?></div>
            </div>
            <a class="name product-item__name" href="/product/<?= $suggestions['products'][$i]['url'] ?: $suggestions['products'][$i]['id'] ?>"><?= $suggestions['products'][$i]['name'] ?></a>
            <a href="/product/<?= $suggestions['products'][$i]['url'] ?>">
                <img width="30" height="30" src="<?= IMAGES_DOMAIN_FULL ?>/images/prod_photo/<?= Product::img_url('min_' . $suggestions['products'][$i]['src'], $suggestions['products'][$i]['url']) ?>">
            </a>
        </div>
    <?php } ?>
</div>
<?php
}

if ($suggestions['categories']) {
?>
<div class="suggestion-block categories">
    <h5>
        <span>В категориях</span>
    </h5>

    <?php for ($i = 0, $n = count($suggestions['categories']); $i < $n; ++$i) { ?>
    <div class="suggestion-item">
        <a href="/category/<?= $suggestions['categories'][$i]['_full_url'] ?>"><?= $suggestions['categories'][$i]['h1'] ?></a>
    </div>
    <?php } ?>
</div>
<?php
}

if ($suggestions['trademarks']) {
?>
<div class="suggestion-block">
    <h5>
        <span>В производителях</span>
    </h5>

    <?php for ($i = 0, $n = count($suggestions['trademarks']); $i < $n; ++$i) { ?>
    <div class="suggestion-item tm-item">
        <a class="name" href="/tm/<?= $suggestions['trademarks'][$i]['url'] ?>"><?= $suggestions['trademarks'][$i]['name'] ?></a>
        <?php if ($suggestions['trademarks'][$i]['src'] !== 'none.jpg') { ?>
        <a href="/tm/<?= $suggestions['trademarks'][$i]['url'] ?>">
            <img width="45" height="30" src="/images/tm_photo/<?= $suggestions['trademarks'][$i]['id'] . '_min.jpg' ?>">
        </a>
        <?php } ?>
    </div>
    <?php } ?>
</div>
<?php
}
