<?php
/**
 * @var string $header
 * @var array $regions
 * @var array $shippings
 * @var array $product_ids
 */
?>
<section class="cart" id="cart-page">
    <div class="row">
        <h1 class="col-xs-12"><?= $header ?></h1>
    </div>
    <div class="row cart_form_and_items">
        <div ng-show="cart.products.length > 0" ng-init="cart.info.cheapestDiscount = false">
            <div class="col-xs-6 cart_items ng-init">
                <h2 class="cart_h2">Мои товары</h2>

                <div class="cart_items_block">
                    <div class="cart_header">
                        <div class="row">
                            <div class="col-xs-8">Товар</div>
                            <div class="col-xs-2">Кол-во</div>
                            <div class="col-xs-2">Итого</div>
                        </div>
                    </div>
                    <div ng-show="cart.info.present !== false" class="cart_item present row">
                        <div class="col-xs-2 cart_photo">
                            <img width="85" height="85" src="/images/present.jpg">
                        </div>
                        <div class="col-xs-9 inf">
                            <h3>Подарок к заказу</h3>
                            За заказ от <span ng-bind="cart.info.present"></span> рублей.
                        </div>
                    </div>
                    <div ng-repeat="product in cart.products" class="{{'product-in-list row cart_item pi_' + product.id}}" ng-init="cart.info.cheapestDiscount = !!(cart.info.cheapestDiscount || product.cheapestDiscount && product.quantity > 1)">
                        <div class="hidden product-tm" ng-bind="product.brand_url"></div>
                        <h3 ng-show="product.showAction">Товары по акции</h3>
                        <a target="_blank" ng-href="{{'/product/' + product.url}}" class="col-xs-2 cart_photo ua-product-link"><img
                                width="85" height="85" ng-alt="{{product.alt}}" ng-title="{{product.photo_title}}"
                                ng-src="{{ '<?= IMAGES_DOMAIN_FULL ?>/images/prod_photo/' + imgUrl('min_' + product.src, product.url) }}"></a>
                        <div class="col-xs-6 cart_desc">
                            <a target="_blank" ng-href="/product/{{product.url}}" class="link ua-product-link product-item__name" ng-bind-html="product.name"></a>

                            <div class="cart_info variant" data-id="{{product.id}}">
                                <p class="cart_pack pack-name" ng-bind-html="product.pack"></p>
                            </div>

                            <p ng-show="product.price > 0 && product.id !== <?= Product::CERTIFICATE_ID ?>" class="{{product.oldPrice !== product.price ? 'w_new_price' : ''}}">
                                <span class="red" ng-show="product.cheapestDiscount && product.quantity > 1">*</span>
                                <span ng-hide="product.oldPrice === product.price" class="old_price">
                                    <span ng-bind="product.oldPrice|price"></span>
                                    <span class="rub">p</span></span>
                                <span class="cart_ip" ng-bind="product.price|price"></span>
                                <span class="{{'rub' + (product.oldPrice !== product.price ? ' red' : '')}}">p</span>
                                <span class="red" ng-show="product.oldPrice > product.price">
                                    (-<span ng-bind="Math.ceil(100.0 - product.price / product.oldPrice * 100.0)"></span>%)
                                </span>
                                <span ng-hide="product.apply_discount" class="no-discount pre_cart_icon">i</span>
                            </p>

                            <div ng-show="product.id === <?= Product::CERTIFICATE_ID ?>" class="certificate-box">
                                Номинал сертификата:
                                <p>
                                    <input ng-value="product.price">
                                    <span class="rub_h">руб</span>
                                    <span class="rub">p</span>
                                </p>
                            </div>
                        </div>
                        <div class="col-xs-2 cart_quan">
                            <div class="in_cart"><a class="cart_min" href="#"
                                                    ng-click="cartProduct(product.id, -1, $event)"
                                                    title="Удалить единицу товара" rel="nofollow"></a><span
                                    class="ci_quantity" ng-bind="product.quantity"></span><a class="cart_plus" href="#"
                                                                                             ng-click="cartProduct(product.id, 1, $event, true)"
                                                                                             rel="nofollow"
                                                                                             title="Добавить единицу товара"></a>
                            </div>
                            <a class="cart_del" href="#" ng-click="cartProduct(product.id, 0, $event)" title="Удалить товар из корзины"
                               rel="nofollow">Удалить</a>
                        </div>
                        <div class="col-xs-2 cart_item_price">
                            <div class="price-box">
                                <div ng-show="product.price > 0">
                                    <span class="ci_sum" ng-bind="product.sum|price"></span>
                                    <span class="rub_h">руб</span>
                                    <span class="rub">p</span>
                                </div>
                            </div>
                            <div ng-show="product.price === 0">
                                Бесплатно
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row discount-info">
                    <p class="red col-xs-12" ng-show="cart.info.cheapestDiscount">* - скидка на 1 единицу товара</p>

                    <div ng-show="cart.info.discount > 0" class="col-xs-12">
                        Скидка до <span ng-bind="cart.info.discount"></span>% - <span ng-bind-html="cart.info.discount_type"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-7 cart_bill">
                        <div class="total_info">
                            <div ng-show="cart.info.final_sum !== 0" class="full_sum">
                                <span ng-bind="cart.info.promo_discount > 0 || cart.info.shipping_price_original !== null ? 'Подытог' : 'Итого'"></span>:
                                <span ng-bind="cart.info.sum_with_discount|price"></span>
                                <span class="rub_h">руб</span>
                                <span class="rub">p</span>
                            </div>
                            <div ng-show="cart.info.promo_diff > 0">
                                <div ng-show="!cart.info.certificate && !cart.info.promo_in_roubles">
                                    Скидка по купону: до <span ng-show="cart.info.promo_discount > 0"><span ng-bind="cart.info.promo_discount"></span>%</span>
                                    <span ng-show="cart.info.promo_discount > 0 && cart.info.promo_diff > 0">(</span><span ng-bind="cart.info.promo_diff|price"></span>
                                    <span class="rub_h">руб</span>
                                    <span class="rub">p</span><span ng-show="cart.info.promo_discount > 0 && cart.info.promo_diff > 0">)</span>  
                                </div>
                                <div ng-show="cart.info.certificate">
                                    Подарочный сертификат: -<span ng-bind="cart.info.promo_diff|price"></span>
                                    <span class="rub_h">руб</span>
                                    <span class="rub">p</span>
                                </div>
                                <div ng-show="!cart.info.certificate && cart.info.promo_in_roubles">
                                    Скидка по промо-коду: -<span ng-bind="cart.info.promo_diff|price"></span>
                                    <span class="rub_h">руб</span>
                                    <span class="rub">p</span>
                                </div>
                            </div>    
                            <div ng-show="cart.info.shipping_price !== undefined && cart.info.shipping_price !== null && !(cart.products.length === 1 && cart.products[0].id === <?= Product::CERTIFICATE_ID ?> && !cart.info.deliverCertificate)" class="full_sum">
                                Доставка:&nbsp;
                                <span ng-show="cart.info.shipping_compensation && cart.info.shipping_price_original > 0" class="old-shipping">
                                    <span ng-bind="cart.info.shipping_price_original|price" class="price-value"></span>
                                    <span class="rub_h">руб</span>
                                    <span class="rub gray">p</span></span>
                                <span ng-show="cart.info.shipping_price > 0">
                                    <span ng-bind="cart.info.shipping_price|price"></span>
                                    <span class="rub_h">руб</span>
                                    <span class="rub">p</span>
                                </span>
                                <span ng-hide="cart.info.shipping_price > 0">Бесплатно</span>
                                <span ng-show="cart.info.shipping_compensation">
                                    <span class="shipping-compensation-info pre_cart_icon">i</span>
                                </span>
                            </div>
                            <p ng-show="(cart.info.final_sum - cart.info.shipping_price) < 4000 && cart.info.region != 907 && cart.info.shipping != 5 && cart.info.shipping != 6" class="cart_delivery_info">Скидки на доставку при заказе от 4000 руб.</p>
                            <p ng-show="(cart.info.final_sum - cart.info.shipping_price) < 5500 && (cart.info.region == 77 || cart.info.region == 78) && cart.info.shipping != 5 && cart.info.shipping != 6" class="cart_delivery_info">Бесплатная доставка от 5500 руб.</p>
                            <div class="full_sum"><strong>Стоимость&nbsp;заказа:&nbsp;</strong>
                                <span ng-show="cart.info.promo_sum + cart.info.shipping_price > 0">
                                    <span ng-bind="(cart.info.promo_sum + (cart.products.length === 1 && cart.products[0].id === <?= Product::CERTIFICATE_ID ?> && !cart.info.deliverCertificate ? 0 : cart.info.shipping_price))|price"></span>
                                    <span class="rub_h">руб</span>
                                    <span class="rub">p</span>
                                </span>
                                <span ng-show="cart.info.promo_sum + cart.info.shipping_price === 0">Бесплатно</span>
                                <p ng-show="cart.info.promo_end_days.end_days" class="cart_promo_end">
                                    Купон (промо-код) истекает через&nbsp;<span ng-bind="cart.info.promo_end_days.end_days"></span>&nbsp;
                                    <span ng-bind="cart.info.promo_end_days.end_days_num"></span>
                                </p>  
                            </div>
                        </div>
                        <p id="prepay-only" ng-show="cart.info.hasParfumery">*Товары из раздела &laquo;Парфюмерия&raquo; доступны только по предоплате</p>
                    </div>
                    <div class="col-xs-5">
                        <input type="hidden" class="cu_uid" name="id"
                               ng-bind-value="{{cart.user.id ? cart.user.id : 0}}">
                        <button type="button" class="pull-right cart_checkout">Оформить заказ</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <p class="hurry_notice_info">Нажимая на кнопку «Оформить заказ», вы соглашаетесь на обработку персональных данных в соответствии с <a class="oferta_link" href="/page/uslovija-obrabotki-personalynih-dannyh" target="_blank">условиями</a>.</p>
                    </div>    
                </div>
            </div>
            <div class="col-xs-6 cart_form form-horizontal ng-init" id="cart-form">
                <h2 class="cart_h2">Контактная информация</h2>

                <div class="cart_user_info">
                    <div class="cu_pre_order">
                        <div class="form-group">
                            <label class="col-xs-3">Ваше ФИО:<span class="red">*</span></label>

                            <div class="col-xs-9">
                                <input type="text" class="form-control cu_fio l-name" tabindex="1" name="fio"
                                       ng-bind-value="{{cart.user.surname ? cart.user.surname + '&nbsp;' : ''}}{{cart.user.name ? cart.user.name + '&nbsp;' : ''}}{{cart.user.patronymic ? cart.user.patronymic + '&nbsp;' : ''}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3">Телефон:<span class="red">*</span></label>

                            <div class="col-xs-9">
                                <input type="text" id="phone" tabindex="2" class="form-control cu_phone l-phone" name="phone"
                                       ng-bind-value="{{cart.user.phone}}"
                                       placeholder="+7 (номер без 8-ки)">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3">E-mail:<span class="red">*</span></label>

                            <div class="col-xs-9">
                                <input type="text" class="form-control cu_email l-email" name="email" tabindex="3"
                                       ng-bind-value="{{cart.user.email}}">
                            </div>
                        </div>
                        <div ng-show="cart.products.length === 1 && cart.products[0].id === <?= Product::CERTIFICATE_ID ?>">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" ng-model="cart.info.deliverCertificate" id="deliver-certificate" value="1"> Доставить бумажный сертификат
                                </label>
                                <br>
                                <br>
                            </div>
                        </div>
                        <div ng-hide="cart.products.length === 1 && cart.products[0].id === <?= Product::CERTIFICATE_ID ?> && !cart.info.deliverCertificate">
                            <div class="form-group">
                                <label class="col-xs-3">Ваш&nbsp;регион:</label>

                                <div class="col-xs-9">
                                    <select name="region" class="form-control l-region cu_region" tabindex="4" id="cart-region" ng-model="cart.user.region">
                                        <?php for ($i = 0, $n = count($regions); $i < $n; ++$i) { ?>
                                            <option value="<?= $regions[$i]['id'] ?>"
                                                data-duration-min="<?= $regions[$i]['duration_min'] ?>"
                                                data-duration-max="<?= $regions[$i]['duration_max'] ?>"
                                                data-1-price="<?= $regions[$i]['price_1'] ?>"
                                                data-2-price="<?= $regions[$i]['price_2'] ?>"
                                                data-3-price="<?= $regions[$i]['price_3'] ?>"
                                                data-4-price="<?= $regions[$i]['price_4'] ?>"
                                                data-6-price="<?= $regions[$i]['price_6'] ?>"
                                            ><?= $regions[$i]['name'] ?></option>
                                        <?php } ?>
                                    </select>
                                    <div class="duration-info cart-i">
                                        <div ng-show="cart.duration_min && cart.duration_max" ng-bind="'Срок сборки и доставки ' + (cart.duration_min == cart.duration_max ? cart.duration_min : cart.duration_min + '-' + cart.duration_max)|duration"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group none" id="cart-shipping-box">
                                <label class="col-xs-3">Доставка:</label>

                                <div class="col-xs-9">
                                    <select class="form-control l-shipping" tabindex="5" id="cart-shipping">
                                        <?php foreach ($shippings as $i => $shipping) { ?>
                                            <option value="<?= $i ?>" data-name="<?= $shipping ?>"><?= $shipping ?></option>
                                        <?php } ?>
                                    </select>
                                    <div class="cart-i pickup">
                                        <div class="shipping" ng-show="!(cart.info.region === 77 && cart.info.shipping === 1) && cart.info.shipping > 0 && cart.info.shipping < 4">При заказе больше 3 кг цена может быть выше</div>
                                        <div ng-show="cart.info.shipping === 4" class="inf">
                                            <span ng-bind="cart.info.pickpoint ? 'Pick-Point: ' + cart.info.pickpoint : 'Pick-Point?'"></span>
                                            <a href="#" id="pp-choose" ng-bind="cart.info.pickpoint ? 'Изменить адрес' : 'Выбрать адрес'"></a>

                                            <br>

                                            <span ng-attr-title="{{ cart.info.sdek ? cart.info.sdek : '' }}" class="sdek-address" ng-bind="cart.info.sdek ? 'СДЭК: ' + cart.info.sdek: 'СДЭК?'"></span>
                                            <a href="#" id="sdek-choose" ng-bind="cart.info.sdek ? 'Изменить адрес' : 'Выбрать адрес'"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3">Адрес доставки:</label>

                                <div class="col-xs-9">
                                    <textarea class="form-control cu_adress l-address" tabindex="6" rows="3" id="cart-address"
                                              name="address"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3">Примечания:</label>

                            <div class="col-xs-9">
                                <textarea class="form-control cu_etc" tabindex="7" rows="2" name="etc"></textarea>
                            </div>
                        </div>
                        <div class="form-group promo_fg">
                            <div ng-show="cart.info.promo_discount || cart.info.promo_applied" class="ng-init">
                                <label class="col-xs-3">Купон:<div class="sub">(Промо-код)</div></label>
                                <div class="col-xs-9 promo_use">
                                    <p>
                                        Удачно введен<span ng-show="!cart.info.promo_applied">, Ваша дополнительная скидка<span ng-bind="cart.info.promo_discount"></span>%</span>!
                                        <span ng-show="cart.info.promo_min_sum > 0 && cart.info.raw_sum < cart.info.promo_min_sum">
                                            <br>
                                            Минимальная сумма заказа
                                            <br>
                                            для получения скидки
                                            <span ng-bind="cart.info.promo_min_sum|price"></span>
                                            <span class="rub">p</span>
                                        </span>

                                        <button class="btn btn-sm btn-danger" id="cancel-promo-btn">Отменить</button>
                                    </p>
                                </div>
                            </div>
                            <div ng-hide="cart.info.promo_discount || cart.info.promo_applied" class="ng-init">
                                <div class="link_to_promo">
                                    <div class="col-xs-3"></div>
                                    <div class="col-xs-9">Есть купон / промо-код?<br><span class="view_promo_form">Нажмите чтобы ввести.</span>
                                    </div>
                                </div>
                                <div class="promo_form none">
                                    <label class="col-xs-3">Купон:<div class="sub">(Промо-код)</div></label>

                                    <div class="col-xs-7 promo_input">
                                        <input type="text" class="form-control cu_promo" name="promo"
                                               placeholder="Введите здесь и нажмите ОК" tabindex="8">
                                        <div id="close-promo-input" title="Закрыть">×</div>
                                    </div>
                                    <div class="col-xs-2 promo_btn">
                                        <button type="button" class="promo_checkout btn" tabindex="9">OK
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <p class="col-xs-12 cart_ps"><span class="red">*</span>&nbsp;-&nbsp;Обязательные&nbsp;поля
                            </p>
                        </div>
                        <div class="row">
                            <div class="col-xs-7"></div>
                            <div class="col-xs-5">
                                <input type="hidden" class="cu_uid" name="id"
                                       ng-bind-value="{{cart.user.id ? cart.user.id : 0}}">
                                <button type="button" class="pull-right cart_checkout" tabindex="9">Оформить заказ
                                </button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <p class="hurry_notice_info">Нажимая на кнопку «Оформить заказ», вы соглашаетесь на обработку персональных данных в соответствии с <a class="oferta_link" href="/page/uslovija-obrabotki-personalynih-dannyh" target="_blank">условиями</a>.</p>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=kDX*5xsZvppyvUq07dSQ39GKRMU93Ks*Pl54aYDxrRTev89TpV2tu*bEVHRQinRcGxUhs9bEzqdMaTDXLKQj4fkRPWgEQxKFVHX4jo9hAd*TxD9TV1s2DbhZB5E1lB1r9n*C*oD9g0J34y4pu4YKjyVfiJSsx4R8sksbB3ZFaZM-';</script>
        </div>
        <div ng-hide="cart.products.length > 0" class="ng-init">
            <div class="no_items col-xs-12">
                <p>Нет&nbsp;товаров&nbsp;в&nbsp;корзине.</p>
            </div>
            <?php  Template::partial('mind_box', [
                        'mb_is_auth' => $is_auth,
                        'mb_top_view' => $mb_top_view,
                    ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div data-retailrocket-markup-block="57a075a865bf192200bd5cd0" product-id="<?= implode(',', $product_ids) ?>"></div>
        </div>
    </div>
</section>
<!-- Google Code for www.roskosmetika.ru/cart Conversion Page -->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 964422581;
    var google_conversion_language = "en";
    var google_conversion_format = "3";
    var google_conversion_color = "ffffff";
    var google_conversion_label = "mg1LCM3wtlwQtdfvywM";
    var google_remarketing_only = false;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt=""
             src="//www.googleadservices.com/pagead/conversion/964422581/?label=mg1LCM3wtlwQtdfvywM&amp;guid=ON&amp;script=0"/>
    </div>
</noscript>

<script type="text/javascript" src="https://pickpoint.ru/select/postamat.js"></script>

<script type="text/javascript" src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>

<div class="modal fade" tabindex="-1" role="dialog" id="sdek-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal__content">
            <div class="modal-header modal__header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Пункты выдачи и постаматы СДЭК</h4>
            </div>
            <div class="modal-body">
                <div id="sdek-map"></div>
            </div>
        </div>
    </div>
</div>
