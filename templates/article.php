<?php
/**
 * @var string $header
 * @var array $content
 */
?>
<div itemscope itemtype="http://schema.org/Article" id="article">
    <div class="row crumbs">
        <div class="crumb_lvl_1">
            <a href="/articles" itemprop="articleSection">Все&nbsp;статьи</a>
        </div>
    </div>
    <div class="row">
        <?= isset($header) ? '<h1 class="col-xs-8" itemprop="headline">' . $header . '</h1>' : '' ?>
        <div class="col-xs-4 article_prod_title"><?= $content['products'] ? 'Рекомендованные&nbsp;товары' : '' ?></div>
    </div>
    <div class="row">
        <div class="col-xs-9" itemprop="articleBody">
            <div class="article_text"><?= $content['article']['text'] ?></div>
            <?php if ($content['categories']) { ?>
                <div class="row">
                    <div class="article_prod_title">Категории&nbsp;товаров&nbsp;по&nbsp;теме&nbsp;статьи</div>
                </div>
                <div class="row">
                    <div class="col-xs-12 article_cats">
                        <?php for ($i = 0, $n = count($content['categories']); $i < $n; ++$i) { ?>
                            <p class="prod_see_all article_cat">
                                <a href="/category/<?= $content['categories'][$i]['_full_url'] ?>"><?= $content['categories'][$i]['h1'] ?: $content['categories'][$i]['name'] ?></a>
                            </p>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="col-xs-3">
            <div class="products">
                <div class="row ua-product-list" data-list="Страница статьи">
                    <?php
                    if ($content['products']) {
                        for ($i = 0, $n = count($content['products']); $i < $n; ++$i) {
                            Template::partial(
                                'product_in_list',
                                [
                                    'product' => $content['products'][$i],
                                    'single' => true,
                                ]
                            );
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
Template::partial('talk_to_all');
Template::partial('mind_box', [
                'mb_is_auth' => $is_auth,
                'mb_top_view' => $mb_top_view,
            ]);