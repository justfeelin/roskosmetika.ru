<?php
/**
 * @var array $question_to_cosmetolog
 */
?>
<div class="col-xs-4 question">
    <a class="info_title_link" href="/question">Вопрос&nbsp;косметологу</a>

    <div class="row">
        <div class="col-xs-4 costetolog_photo">
            <img src="/images/kosmetolog.jpg" class="img-circle" width="90" height="90"
                 alt="Эксперт">
        </div>
        <div class="col-xs-8 cosmetolog">
            <p class><a href="/question" class="link">Светлана Юдина</a></p>

            <p class="under_link">Эксперт</p>

            <div class="cosmetolog_desc"><?= $question_to_cosmetolog['main_answer'] ?></div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <button class="btn btn-primary button-common center-block question__submit">
                Задать вопрос
            </button>
        </div>
    </div>
    <div class="question_body">
        <p>Вопрос</p>

        <div><?= $question_to_cosmetolog['question'] ?></div>
    </div>
    <div class="question_body">
        <p>Ответ</p>

        <div><?= $question_to_cosmetolog['answer'] ?></div>
    </div>
    <div class="row all_questions">
        <a class="col-xs-11 see_all" href="/question">Читать&nbsp;все&nbsp;вопросы&nbsp;косметологу<span
                class="pointer_see_all"></span></a>
    </div>
</div>
