<?php
/**
 * @var array $main_spec
 * @var bool $is_auth
 */
$n = count($main_spec['groups']);

for ($i = 0; $i < $n; ++$i) {
?>
    <div class="special">
        <div class="row">
            <ul class="1nav 1nav-tabs special-menu">
                <?php
                if ($i === 0) {
                    for ($y = 0; $y < $n; ++$y) {
                ?>
                        <li class="<?= $y === 0 ? 'active_spec_menu ' : '' ?>special-menu__item">
                            <a class="item_<?= $i ?> special-menu__item-text hidden_link"
                               href="javascript:void(0)"
                               data-link="<?= !empty($main_spec['lines'][$y]['line_url']) ? Template::hidelink($main_spec['lines'][$y]['line_url']) : '' ?>"><?= $main_spec['groups'][$y] ?></a>
                        </li>
                <?php
                    }
                } else {
                ?>
                        <li class="active_spec_menu special-menu__item">
                            <a class="item_<?= $i ?> special-menu__item-text hidden_link"
                                href="javascript:void(0)"
                                data-link="<?= !empty($main_spec['lines'][$i]['line_url']) ? Template::hidelink($main_spec['lines'][$i]['line_url']) : '' ?>"><?= $main_spec['groups'][$i] ?></a>
                        </li>
                <?php } ?>
            </ul>
        </div>
        <div class="ua-product-list special_prods">
            <div class="item_<?= $i ?> active_spec_prod">
                <div class="row">
                    <?php
                    for ($y = 0, $m = count($main_spec['lines'][$i]['products']); $y < $m; ++$y) {
                        Template::partial('product_in_list', [
                            'is_auth' => $is_auth,
                            'product' => $main_spec['lines'][$i]['products'][$y],
                            'special_item' => true,
                            'rr' => 'popular',
                        ]);
                    }
                    ?>
                </div>
                <?php if ($main_spec['lines'][$i]['line_url']) { ?>
                    <div class="row">
                        <div class="col-xs-12 see_all">
                            <a href="<?= $main_spec['lines'][$i]['line_url'] ?>"><?= $main_spec['lines'][$i]['tag_url'] ?>
                                <span class="pointer_see_all"></span></a>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php
}
