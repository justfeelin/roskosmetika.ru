<?php
/**
 * @var string $header
 * @var array $content
 * @var string $found
 * @var int $catsInColumn
 * @var array $Pagination
 * @var string $search
 */
if ($content['found'] && $content['products']) { ?>
    <a href="#" id="products" class="anchor"></a>
<?php
}

if ($content['found']) {
    Template::partial(
        'product_filter',
        [
            'name' => !empty($header) ? $header : '',
            'found' => $found,
            'content' => $content,
            'categories_html' => $content['categories_html'],
            'filters' => $content['filters'],
            'multicol' => true,
            'relevance' => true,
        ]
    );
} else {
?>
    <h2>К сожалению, мы ничего не нашли :(</h2>
    <p>Но мы готовы предложить вам гораздо лучше:</p>
<?php
}

$paginationHTML = isset($Pagination) ? Template::get_tpl(
    'pagination_wrap',
    [
        'Pagination' => $Pagination,
    ]
) : '';

echo $paginationHTML;

if ($content['found']) {
?>
    <h2>Найденные товары</h2>
<?php } ?>
<div class="col-xs-12 products">
    <div class="ua-product-list product-list" data-list="Поиск">
        <?php
        Template::partial(
            'product_list',
            [
                'products' => $content['products'],
            ]
        )
        ?>
    </div>
    <?= $paginationHTML ?>
</div>
<?php if (!$content['found']) { ?>
    <div class="row">
        <div class="col-xs-6 subscribe">
            <div class="row sub_backgroung">
                <div class="col-xs-11">
                    <div class="rew_sub_area">
                        <p class="rew_sub_title">Ничего&nbsp;не&nbsp;нашли?!&nbsp;-&nbsp;Поможем!</p>
                        <div class="sub_desc">
                            <p>Позвоните нам с 10:00 до 19:00</p>
                            <p class="phone_number">8 800 775-54-83<br><span class="nowrap">(Звонок по России бесплатный)</span></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-6"></div>
    </div>

<?php } 
if (!empty($content['mb_search'])) {
?>

 <div class="row active_spec_prod ua-product-list" data-list="Те, кто искал <?= $search ?> интересовались этими товарами">
        <div class="product-h">
            <h3>Те, кто искал <?= $search ?> интересовались этими товарами:</h3>
        </div>
        <?php
        for ($i = 0, $n = count($content['mb_search']); $i < $n; ++$i) {
            Template::partial('product_in_list', [
                'is_auth' => $is_auth,
                'product' => $content['mb_search'][$i],
            ]);
        }
        ?>
    </div>
    <?php
}
?>