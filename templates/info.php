<?php
/**
 * @var string $header
 * @var int $active_aside
 * @var array $msg
 */
?>
<section class="room">
    <div class="row">
        <h1 class="col-xs-6 col-xs-offset-3"><?= $header ?></h1>
    </div>
    <div class="row">
        <?php
        Template::partial(
            'room_left_menu',
            [
                'active_aside' => $active_aside,
            ]);
        ?>
        <div class="col-xs-6">
            <form action="/room" method="POST">
                <div class="form-group row <?= isset($msg['surname']) ? 'has-error' : '' ?>">
                    <p class="col-xs-3">Фамилия</p>

                    <div class="col-xs-9">
                        <input type="text" name="surname" value="<?= isset($values['surname']) ? $values['surname'] : '' ?>"
                               class="form-control">
                        <?= isset($msg['surname']) ? '<p class="form_error">' . $msg['surname'] . '</p>' : '' ?>
                    </div>
                </div>
                <div class="form-group row <?= isset($msg['name']) ? 'has-error' : '' ?>">
                    <p class="col-xs-3">Имя</p>

                    <div class="col-xs-9">
                        <input type="text" name="name" value="<?= isset($values['name']) ? $values['name'] : '' ?>"
                               class="form-control">
                        <?= isset($msg['name']) ? '<p class="form_error">' . $msg['name'] . '</p>' : '' ?>
                    </div>
                </div>
                <div class="form-group row <?= isset($msg['patronymic']) ? 'has-error' : '' ?>">
                    <p class="col-xs-3">Отчество</p>

                    <div class="col-xs-9">
                        <input type="text" name="patronymic"
                               value="<?= isset($values['patronymic']) ? $values['patronymic'] : '' ?>" class="form-control">
                        <?= isset($msg['patronymic']) ? '<p class="form_error">' . $msg['patronymic'] . '</p>' : '' ?>
                    </div>
                </div>

                <div class=" form-group row">
                    <p class="col-xs-3">
                        Ваша
                        <br>
                        скидка
                    </p>
                    <div class="col-xs-3 personal_discount"><?= max($values['auto_discount'], $values['main_discount']) . ' %' ?></div>
                </div>

                <div class="row">
                    <div class="col-xs-3"></div>
                    <button type="submit" name="submit" class="col-xs-9 form_save">Сохранить</button>
                </div>
            </form>
        </div>
        <div class="col-xs-3">
        </div>
    </div>
    <?php Template::partial('mind_box', [
                    'mb_is_auth' => $is_auth,
                    'mb_top_view' => $mb_top_view,
                ]); ?>
</section>
