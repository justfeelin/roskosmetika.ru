<?php
/**
 * @var string $crumb
 * @var string $header
 * @var string $content
 * @var integer $regions_geo_id
 * @var array $regions
 * @var bool $flag_delivery
 * @var bool $noRecommendations
 */
if (!empty($crumb)) {
    Template::partial(
        'breadcrumbs',
        [
            'crumbs' => [
                [
                    'name' => $crumb,
                ],
            ],
        ]
    );
}

?>
<?php if (isset($flag_delivery) &&($flag_delivery)){?>
    <script type="text/javascript" src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
 <?php  }?>
<div class="row" itemscope itemtype="http://schema.org/WebPage">
    <div class="col-xs-12" itemprop="text">

   <?php if (isset($flag_delivery) &&($flag_delivery)){?>
       <div class="row" id="static-page">
    <div class="delivery center-block ">
        <div class="panel panel-default">
            <div class="panel-heading"><h2>Доставка</h2></div>
            <div class="panel-body">
                <div class="row">
                    <div class="tabs ">
                        <ul class="nav nav-tabs tabstyle">
                            <li class="active"><a href="#tab1" data-toggle="tab">Курьер</a></li>
                            <li><a href="#tab2" data-toggle="tab">Пункт выдачи</a></li>
                            <li><a href="#tab3" data-toggle="tab">Самовывоз (офис)</a></li>


                            <div class="tab-content">
                                <div class="tab-pane active" id="tab1">
                                    <br>
                                    <select id="courier" class="form-control courier-select2" name="courier_region">

                                        <?php foreach($regions as $region){ ?>

                                            <?php if($region['geo_id']==$regions_geo_id){$selected='selected="selected"';}else{$selected='';}?>
                                            <option value="<?=$region['cdek']?>" <?=$selected?>><?=$region['name']?></option>

                                        <?php }; ?>

                                    </select>
                                    <p>Минимальная сумма заказа для оформления доставки<span class="red">*</span>&nbsp;&mdash; <strong>1 000</strong>&nbsp;рублей.</p>
                                    <p><strong>Для заказов свыше трех килограмм и превышающих стандарный объем<span class="red">**</span> — индивидуальный расчет</strong></p>
                                    <p><strong>Стоимость доставки для заказов весом до 3-х килограмм и не превышающих стандарный объем:</strong></p>
                                    <div id="deliv_mes_courier">
                                    </div>
                                    <div class="delivery_page_notes">
                                        <p><span class="red">*</span> Доставка осуществляется при условии полной или частичной оплаты заказа.</p>
                                        <p><span class="red">**</span> Размер стандартной коробки курьерской службы, которую выбрал клиент.</p>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab2">
                                    <br>
                                    <label for="cdek" class="delivery_page_label">Регион:</label>
                                    <select id="cdek" class="form-control delivery-select2" name="region">

                                        <?php foreach($regions as $region){ ?>

                                        <?php if($region['geo_id']==$regions_geo_id){$selected='selected="selected"';}else{$selected='';}?>
                                        <option value="<?=$region['cdek']?>" <?=$selected?>><?=$region['name']?></option>

                                        <?php }; ?>

                                    </select>
                                    <label for="cdek_point" class="delivery_page_label">Адрес:</label>
                                    <select id="cdek_point" class="form-control point-select2" name="point">
                                    </select>
                                    <p>Минимальная сумма заказа для оформления доставки<span class="red">*</span>&nbsp;&mdash; <strong>1 000</strong>&nbsp;рублей.</p>
                                    <p><strong>Для заказов свыше трех килограмм и превышающих стандарный объем<span class="red">**</span> — индивидуальный расчет</strong></p>
                                    <p><strong>Стоимость доставки для заказов весом до 3-х килограмм и не превышающих стандарный объем:</strong></p>
                                    <div id="deliv_mes_cdek">
                                    </div>
                                    <div class="delivery_page_notes">
                                        <p><span class="red">*</span> Доставка в пункт выдачи осуществляется только при условии полной оплаты заказа</p>
                                        <p><span class="red">**</span> Размер стандартной коробки курьерской службы, которую выбрал клиент.</p>
                                    </div>
                                    <div class="col-xs-12" id="first_map"></div>
                                </div>

                                <div class="tab-pane" id="tab3">
                                    <br>
                                    <br>
                                    <p><strong>Бесплатный самовывоз осуществляется только из главного офиса</strong> по адресу: ул. Василия Петушкова д.8.
                                    <br>Cхема проезда в разделе&nbsp;<a href="/page/contacts" target="_blank" style="text-decoration: underline; font-size: 10pt;">контакты</a>.
                                    <br>Внимание!!! По данному адресу находится не магазин, а офис. Пожалуйста, оформляйте заказ на сайте заранее!!!</p>
                                    <p>Время самовывоза строго ограничено <strong>с 11-00 до 19-00. ПН-ПТ, c 11-00 до 18-00 СБ</strong></p>
                                    <p class="red">Перед приездом обязательно позвонить в офис для подтверждения готовности заказа.</p>
                                    <p>На некоторые группы товаров предоплата 100%<span class="red">*</span></p>
                                    <p><strong>Срок хранения самовывоза - 3 дня.</strong></p>
                                    <div class="delivery_page_notes">
                                        <p><span class="red">*</span>Только для товаров с дальнего склада, чтобы гарантировать выкуп товара покупателем и минимизировать потерю транспортных расходов по его доставке до офиса, в случае отказа.</p>
                                    </div>
                                    <div class="col-xs-12" id="first_map"></div>
                                </div>

                            </div>
                    </div>

                </div>
                <div>

                </div>

            </div>
        </div>

   <?php  }?>

        <?= $content ?>
    </div>
</div>
<?php
Template::partial('talk_to_all');

if (!empty($mb_pages)) {
    ?>
    <div class="row active_spec_prod ua-product-list" data-list="Вам может подойти">
        <div class="product-h">
            <h3>Вам&nbsp;может&nbsp;подойти</h3>
        </div>
        <?php
        for ($i = 0, $n = count($mb_pages); $i < $n; ++$i) {
            Template::partial('product_in_list', [
                'is_auth' => $is_auth,
                'product' => $mb_pages[$i],
            ]);
        }
        ?>
    </div>
    <?php
}
