<?php
/**
 * @var array $reviews
 * @var string $product_name
 * @var array $month
 */
for ($i = 0, $n = count($reviews); $i < $n; ++$i) { ?>
    <div class="col-xs-6" itemprop="review" itemscope itemtype="http://schema.org/Review">
        <meta itemprop="itemReviewed" content="Отзыв о товаре <?= !empty($product_name) ? $product_name : $reviews[$i]['product_name'] ?>" />
        <div class="prod_reviewer"><span itemprop="author"><?= $reviews[$i]['name'] ?></span>
            <time itemprop="datePublished" class="review_date_city"
                  datetime="<?= Template::date('c', $reviews[$i]['date']) ?>">,&nbsp;<?= $reviews[$i]['city'] ?>
                &nbsp;<?= !empty($reviews[$i]['day']) ? $reviews[$i]['day'] . '&nbsp;' . $month[$reviews[$i]['month']] : Template::date('d.m.Y', $reviews[$i]['date']) ?></time>
        </div>
        <div class="review_box" itemprop="reviewBody"><?= $reviews[$i]['comment'] ?></div>
    </div>
<?php
}
