<?php
/**
 * @var array $content
 */
?>
<div id="referral" class="<?= !$content['code'] ? 'not-registered' : '' ?>">
    <div class="row banner-box">
        <div class="banner-image"></div>
        <div class="col-xs-12">
            <h1><?= $content['code'] ? 'Как получить скидку? Всё просто!' : 'Мы раскроем секрет скидки только зарегистрированным пользователям' ?></h1>
        </div>
    </div>
    <div class="main-content">
        <?php if (!$content['code']) { ?>
            <div class="bg"></div>

            <div class="login-form dologin">
                <div class="switch">
                    <a href="#" class="login active">Вход</a>
                    <a href="#" class="registration">Регистрация</a>
                </div>

                <form class="form auth">
                    <input type="text" name="user" class="text-input login_user reg_email" placeholder="Ваш E-mail или телефон">
                    <input type="password" name="password" class="text-input login_password reg_password" placeholder="Ваш пароль">


                    <input type="submit" class="login-button dologin" value="Вход" data-url="/referral">
                </form>

                <a href="#" class="backup_psw">Забыли пароль?</a>
            </div>
        <?php } ?>
        <div class="row steps">
            <div class="col-xs-3 c1">
                <div class="img"></div>
                <h3>Расскажите про наш магазин своим друзьям</h3>
                <p>Нажмите на иконку соцсети или отправьте ссылку почтой<br>своим друзьям</p>
            </div>
            <div class="col-xs-3 c2">
                <div class="img"></div>
                <h3>Ваши друзья сразу<br>получат скидку</h3>
                <p>При переходе по ссылке<br>и оформлении заказа<br>на нашем сайте</p>
            </div>
            <div class="col-xs-3 c3">
                <div class="img"></div>
                <h3>Скидка 20% для вас</h3>
                <p>После получения оплаты<br>от вашего друга скидка автоматически отобразится<br>у вас при оформлении заказа</p>
            </div>
            <div class="col-xs-3 c4">
                <div class="img"></div>
                <h3>Наслаждайтесь экономией до бесконечности</h3>
                <p>Количество ваших заказов<br>со скидкой равно количеству<br>заказов ваших друзей</p>
            </div>
        </div>
        <div class="row links">
            <div class="col-xs-12">
                <hr>

                <h3>Просто делись с друзьями</h3>

                <?php
                Template::partial(
                    'referral_box',
                    [
                        'referral_discount' => $content['referral_discount'],
                        'url' => $content['url'],
                        'code' => $content['code'],
                    ]
                );
                ?>

                <hr>

                <p>
                    Скидки не суммируются. Действует правило большей скидки.
                </p>
            </div>
        </div>
    </div>
</div>
<?php
Template::partial('mind_box', [
                    'mb_is_auth' => $is_auth,
                    'mb_top_view' => $mb_top_view,
                ]);