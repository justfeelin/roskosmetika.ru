<div class="modal fade" id="all-question-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="true">
    <div class="modal-dialog modal__frame">
        <div class="modal-content modal__content">
            <form class="form-horizontal" role="form" id="all-question-form">
                <div class="modal-header modal__header">
                    <button type="button" class="close modal__close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <p class="modal-title modal__title" id="myModalLabel">
                        Задать интересующий вопрос
                    </p>
                </div>
                <div class="modal-body modal__body">
                    <div class="form-group modal__row">
                        <label for="aqb-name" class="col-xs-3 control-label modal__label">ФИО:<span class="form-required">*</span></label>
                        <div class="col-xs-9">
                            <input type="text" class="form-control l-name form-name" id="aqb-name">
                        </div>
                    </div>
                    <div class="form-group modal__row">
                        <label for="q-phone" class="col-xs-3 control-label modal__label">Телефон:<span class="form-required">*</span></label>
                        <div class="col-xs-9">
                            <input type="tel" class="form-control l-phone phoneInput" id="q-phone" required="required">
                        </div>
                    </div>
                    <div class="form-group modal__row">
                        <label for="aqb-email" class="col-xs-3 control-label modal__label">Email:<span class="form-required">*</span></label>
                        <div class="col-xs-9">
                            <input type="email" class="form-control l-email form-email" id="aqb-email">
                        </div>
                    </div>
                    <div class="form-group modal__row">
                        <label for="aqb-question" class="col-xs-3 control-label modal__label">Вопрос:<span class="form-required">*</span></label>
                        <div class="col-xs-9">
                            <textarea class="form-control form-question" rows="5" id="aqb-question"></textarea>
                        </div>
                    </div>
                    <div class="row modal__row">
                        <div class="col-xs-12  modal__notes">Мы постараемся ответить на Ваш вопрос по E-mail с 9:30 до 18:30 (пн-пт).<br><span class="form-required">*</span> - обязательные поля</div>
                    </div>
                </div>
                <div class="modal-footer modal__footer">
                    <button type="submit" class="btn btn-primary button-common">Задать вопрос</button>
                    <p class="hurry_notice_info">Нажимая на кнопку «Задать вопрос», вы соглашаетесь на обработку персональных данных в соответствии с <a class="oferta_link" href="/page/uslovija-obrabotki-personalynih-dannyh" target="_blank">условиями</a>.</p>
                </div>
            </form>
        </div>
    </div>
</div>
