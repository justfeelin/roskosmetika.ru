<?php
/**
 * @var string $title
 * @var string $header
 * @var string $canonical
 * @var array $css
 * @var string $css_path
 * @var string $keywords
 * @var string $description
 * @var string $page
 * @var string $search
 * @var string $main_menu
 * @var string $brands_html
 * @var string $incartlink
 * @var bool $is_auth
 * @var string $favourites
 * @var string $trackingEventsCode
 * @var string $ABTests
 * @var string $admitad_counter
 * @var string $cartInfo
 * @var array $scripts
 * @var string $javascripts_path
 * @var bool $referral_popup
 * @var bool $isDesktop
 */
?>
<!DOCTYPE html>
<html ng-app="rk">
<head>
    <link rel="stylesheet" href="/eskimobi/mcss.css">
    <script type="text/javascript" src="/eskimobi/mjs.js"></script>
    
    <meta charset="UTF-8">

    <title><?= isset($title) && $title ? $title : $header ?></title>

    <meta name="viewport" content="width=1030, user-scalable=yes" />

    <link rel="canonical" href="<?= DOMAIN_FULL . $canonical ?>">

    <meta name="theme-color" content="#8BD245">

    <link rel="stylesheet" href="/css.css">

    <?php for ($i = 0, $n = count($css); $i < $n; ++$i) { ?>
        <link rel="stylesheet" href="<?= $css_path . $css[$i] ?>">
    <?php } ?>

    <link rel="shortcut icon" href="/favicon.ico">
    <link href="https://plus.google.com/115410748880955207800" rel="publisher"/>
    <link rel="manifest" href="/manifest.json">

    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>
    <meta name='yandex-verification' content='429731831e943293'/>
    <meta name="google-site-verification" content="kJlenMkKkMLtVRvupz_r973WuiHU9-Cr8KDY92aLvq8"/>

    <?php if (isset($keywords)) { ?>
        <meta name="keywords" content="<?= str_replace('"', '&quot;', $keywords) ?>">
    <?php } ?>
    <?php if (isset($description)) { ?>
        <meta name="description" content="<?= str_replace('"', '&quot;', $description) ?>">
    <?php } ?>
    <?php if (isset($open_graph)) { ?>
        <?= $open_graph ?>
    <?php } ?>

    <?= Pagination::meta() ?>
</head>
<body ng-controller="cart"> 
   
<?php Template::partial('google_tag_manager') ?>
<div id="popup-discount-bar"<?= !isset($_SESSION['popup_discount_bar']) ? ' class="none"' : '' ?>>
    <a href="/cart">
        Ваша персональная скидка 10% на первую покупку уже в корзине!
    </a>
</div>
<div class="container" id="container">
    <?php
    Template::partial('header', [
        'page' => $page,
        'incartlink' => $incartlink,
        'is_auth' => $is_auth,
        'search' => isset($search) ? $search : null,
        'main_menu' => $main_menu,
    ]);

    echo
        $brands_html;

    Template::partial($page, true);

    if ($page != 'index') {
        if ($is_auth || $page == 'cart') {
            Template::partial('pay_delivery_long');
        } else {
            ?>
            <div class="row review_and_sub">
                <?php
                Template::partial('subscribe', [
                    'page' => $page,
                ]);
                Template::partial('pay_delivery_short');
                ?>
            </div>
            <?php
        }
    }
    ?>
    <div id="footer-push"></div>
</div>

<?php
Template::partial('footer', [
    'page' => $page,
    'incartlink' => $incartlink,
]);
Template::partial('bottom_cart', [
    'is_auth' => $is_auth,
    'incartlink' => $incartlink,
    'favourites' => $is_auth ? $favourites : null,
]);
?>

<div class="container-fluid navbar-fixed-top menu-fixed-top">
    <div class="container menu-fixed-top-container"></div>
</div>

<div class="right_buttons">
    <?php if ($isDesktop) { ?>
        <p class="recall_button" title="Заказать звонок"><span class="recall_icon"></span></p>

        <p class="right_question" title="Задать вопрос"><span class="right_question_icon"></span></p>
    <?php } else { ?>
        <a class="left_chat whatsapp" title="Whatsapp" href="whatsapp://tel:+79269524893"></a>

        <a class="left_chat sms" title="SMS" href="sms:+79269524893"></a>
    <?php } ?>
</div>
<!-- sendpulse -->
<div>
    <script src="//static-login.sendpulse.com/apps/fc3/build/loader.js" sp-form-id="4f474b59e8c4834baf1f4ed490486409e759257ab47bcc1b62bcce82ded2677b"></script>
    <script src="//static-login.sendpulse.com/apps/fc3/build/loader.js" sp-form-id="495f7a7ad253c875564ae48ea7c856aff8d7e2d460d2f7c2702ede8d5401f86b"></script>
</div>
<!-- /sendpulse -->
<div id="bot">
    <?php Template::partial('counters') ?>

    <!-- {{{ --><script type="text/javascript">
        <?= $trackingEventsCode ?>
    </script><!-- }}} -->

    <!-- {{{ --><script type="text/javascript">
        window.ABTests = <?= $ABTests ?>;
    </script><!-- }}} -->

    <script src="/js.js"></script>

    <?php for ($i = 0, $n = count($scripts); $i < $n; ++$i) { ?><!-- {{{ --><script src="<?= $javascripts_path . $scripts[$i] ?>"></script><!-- }}} --><?php } ?>

    <script src="https://apis.google.com/js/plusone.js"></script>
    <script src="https://yandex.st/share/share.js"></script>

    <?= isset($admitad_counter) ? $admitad_counter : '' ?>

    <!-- {{{ --><script type="text/javascript">
        common.cartInfo = <?= $cartInfo ?>;
    </script><!-- }}} -->
</div>

<a id="sale-g" class="sale-s" href="/page/sales" target="_blank"></a>
<a id="sale-r" class="sale-s" href="/page/sales" target="_blank"></a>

<?php
Template::partial('one-click-buy-popup');

Template::partial('cart-present-popup');

Template::partial('modal-alert');

Template::partial('password_recovery');

Template::partial('ask_question_box');

Template::partial('pre_order_box');

Template::partial('recall_box');

Template::partial('for_prof_box');

if (!empty($email_popup) && empty($referral_popup)) {
    Template::partial('email_popup');
}

if (!empty($referral_popup)) {
    Template::partial('referral_popup');
}

echo DB::output_queries();
?>

</body>
</html>
