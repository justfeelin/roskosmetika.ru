<?php
/**
 * @var array $content
 */

$link = DOMAIN_FULL . '/payment/' . ($content['sberbank'] ? 'sberbank/' . $content['site_order_number'] . '/' . floatraw($content['cost']) : $content['hash']);
?>
<style>
    .rk-pay-btn:hover {
        background-color: #9ddc5e !important;
    }
</style>
<div style="color: #3a3a3a; font-size: 17px">
    <div>Здравствуйте, <?= h($content['name']) ?></div>

    <div>Вам выставлен счёт по заказу Р-<?= $content['site_order_number'] ?> на сумму <?= h($content['cost']) ?> руб.</div>

    <div style="padding-top: 15px;">Чтобы оплатить счёт, перейдите по <a href="<?= $link ?>" target="_blank">ссылке</a> или нажмите на кнопку:</div>

    <div style="margin: 25px 0; text-align: center">
        <a class="rk-pay-btn" style="box-shadow: 2px 1px 2px #bbbbbb; -o-box-shadow: 2px 1px 2px #bbbbbb; -moz-box-shadow: 2px 1px 2px #bbbbbb; -webkit-box-shadow: 2px 1px 2px #bbbbbb; display: inline-block; font-size: 20px; background-color: #8dc655; color: white; padding: 12px 25px; text-decoration: none; text-transform: uppercase" href="<?= $link ?>" target="_blank">Оплатить</a>
    </div>
</div>
