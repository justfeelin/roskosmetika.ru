<?php
/**
 * @var array $content
 */
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <h1 style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 10px;margin-bottom: 10px;">
                Мы рады приветствовать Вас в интернет-магазине Роскосметика!</h1>

            <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 12px;">
                Теперь Вы являетесь зарегистрированным клиентом. </p>

            <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 12px;">
                В свой личный кабинет Вы можете всегда зайти с главной страницы
                интернет-магазина <a href="<?= DOMAIN_FULL ?>/" target="_blank"
                                     style="color: #9b1d97;">Роскосметика</a>.</p>

            <div style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 12px;">
                <p>В личном кабинете Вы сможете:</p>
                <ul>
                    <li>смотреть статус своих новых заказов;</li>
                    <li>просматривать и повторять предыдущие заказы, тратя при этом минимум
                        времени;
                    </li>
                    <li>добавлять интересные для Вас товары в избранное.</li>
                </ul>
            </div>
            <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 12px;">
                Ваш адрес электронной почты&nbsp;(он же login):&nbsp;<b><?= $content['email'] ?></b></p>

            <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 12px;">
                Ваш пароль: <b><?= $content['password'] ?></b></p>
        </td>
    </tr>
</table>
