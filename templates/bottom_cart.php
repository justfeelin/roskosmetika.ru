<?php
/**
 * @var bool $is_auth
 * @var string $incartlink
 * @var string $favourites
 */
?>
<div class="navbar-default navbar-fixed-bottom">
    <!-- <span class="ny_pig_hello" ng-click="cartProduct(12526, 1, $event, true)"></span> -->
    <div class="account_menu container-fluid">
        <div class="container">
            <div class="row bottom-panel-row">
                <div class="col-xs-3">
                    <?php if ($is_auth) { ?>
                        <div class="account">
                            <a <?= $incartlink ?> href="/room/">
                                <span class="account_icon"></span>
                                <span class="ac_menu_solid">Личный&nbsp;кабинет</span>
                            </a>
                        </div>
                    <?php } else { ?>
                        <div class="auth bottom_auth">
                            <div class="login"><span>Вход</span></div>
                            <div class="reg"><span>Регистрация</span></div>
                            <form class="reg_window hidden reg_in_bottom">
                                <div class="form-group">
                                    <p>E-mail</p>
                                    <input type="email" class="form-control reg_email has-error">
                                </div>
                                <div class="form-group">
                                    <p>Пароль</p>
                                    <input type="password" class="form-control reg_password">
                                </div>
                                <button class="reg_btn">Зарегистрировать</button>
                            </form>
                            <form class="login_window hidden login_in_bottom">
                                <div class="form-group">
                                    <p>E-mail или телефон</p>
                                    <input type="text" class="form-control login_user has-error">
                                </div>
                                <div class="form-group">
                                    <p>Пароль</p>
                                    <input type="password" class="form-control login_password">
                                </div>
                                <div class="row rem_and_back">
                                    <div class="remember col-xs-6">Забыли&nbsp;пароль?&nbsp;-&nbsp;легко</div>
                                    <p class="col-xs-6 backup_psw">Восстановить&nbsp;пароль</p>
                                </div>
                                <button class="log_in_btn">Войти</button>
                                <div class="login_win_bottom">
                                    <div>Еще&nbsp;не&nbsp;зарегистрированы?</div>
                                    <div>Пройти&nbsp;<span class="fast_reg">Быструю&nbsp;регистрацию</span></div>
                                </div>
                            </form>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-xs-2" ng-controller="viewed">
                    <div id="viewed-products" class="dnone">
                        <div class="controls">
                            <a href="#" class="close">Закрыть<span class="glyphicon glyphicon-remove-circle"></span></a>
                            <a href="#" class="clear">Очистить</a>

                            <span class="arrow prev">&lt;</span>
                            <span class="arrow next">&gt;</span>
                        </div>
                        <div class="products-box">
                            <div class="products">
                                <div class="product" ng-repeat="product in products" viewed-directive>
                                    <a href="#" ng-href="{{'/product/' + product.url}}">
                                        <img ng-src="{{ '<?= IMAGES_DOMAIN_FULL ?>/images/prod_photo/' + imgUrl((product.img ? 's_' + product.id : 'none') + '.jpg', product.url) }}" width="175" height="175">
                                        <p ng-bind-html="product.name"></p>
                                        <div>
                                            Цена:
                                            <span ng-bind="product.price|price"></span>
                                            <span class="rub_h">руб</span>
                                            <span class="rub">p</span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <span ng-show="products.length > 0">
                        <a href="#" id="toggle-viewed" ng-bind="'Вы смотрели (' + products.length + ')'"></a>
                    </span>
                </div>
                <div class="col-xs-2">
                    <div class="ac_favourite">
                        <?php if ($is_auth) { ?>
                            <a <?= $incartlink ?> class="hidden_link" href="#" rel="nofollow"
                               data-link="<?= Template::hidelink('/room/favorites') ?>" title="Посмотреть любимые товары">
                                <span class="underline_dahsed">Избранное</span>
                                <span class="bottom_menu_tag cart_favourites"><?= $favourites ?></span>
                            </a>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-xs-3 bottom-cart cart-info-badge bottom-badge">
                    <div class="ac_cart">
                        <a class="hidden_link" href="#" rel="nofollow" data-link="<?= Template::hidelink('/cart') ?>"
                        title="Перейти в корзину для оформления заказа">
                        <span class="cart_icon"></span>
                        <span class="ac_menu_solid">Корзина</span>
                        <span class="bottom_menu_tag" ng-bind="cart.info.quantity"></span>
                        </a>
                        <span ng-show="cart.info.final_sum">
                            <span class="account_menu_price" ng-bind="(cart.info.final_sum - (cart.products.length === 1 && cart.products[0].id === <?= Product::CERTIFICATE_ID ?> && !cart.info.deliverCertificate ? cart.info.shipping_price : 0))|price"></span>
                            <span class="rub_h">руб</span>
                        </span>
                        <span ng-show="cart.info.final_sum" class="rub ng-init">p</span>
                    </div>
                </div>
                <div class="show_cart hidden" ng-controller="added-product">
                    <div class="cart_body">
                        <p>Товар&nbsp;добавлен&nbsp;в&nbsp;корзину</p>

                        <div class="cart_body_info">
                            <a href="#" ng-href="{{'/product/' + product.url}}" class="pull-left"><img width="85" height="85" ng-title="{{product.photo_title}}" ng-alt="{{product.alt}}"
                                                                                                       ng-src="{{ '<?= IMAGES_DOMAIN_FULL ?>/images/prod_photo/' + imgUrl('min_' + product.src, product.url) }}"></a>
                            <a class="clearfix" href="#" ng-href="{{'/product/' + product.url}}">
                                <span ng-bind-html="product.name"></span>
                                <div class="pack" ng-bind-html="product.pack"></div>
                            </a>
                        </div>

                        <div ng-show="product.special.line.id || product.special.article.id">
                            <p class="cs_recomendation">рекомендуем посмотреть</p>

                            <div class="cart_body_links">
                                <a href="#" ng-href="{{product.special.line.url}}" ng-bind-html="product.special.line.name"></a>
                                <a href="#" ng-href="{{product.special.article.url}}" ng-bind-html="product.special.article.name"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-2 get_order_place">
                    <span ng-show="cart.info.sum_with_discount !== 0">
                        <a class="ac_button hidden_link" href="#" rel="nofollow" data-link="<?= Template::hidelink('/cart') ?>" ng-bind="'Оформить&nbsp;заказ'"></a>
                    </span>
                    <span ng-show="cart.info.sum_with_discount === 0">
                        <a <?= $incartlink ?> class="hidden_link no_ac_button" href="#" rel="nofollow"
                           data-link="<?= Template::hidelink('/page/order') ?>" ng-bind="'Как&nbsp;оформить&nbsp;заказ'"></a>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
