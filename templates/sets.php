<?php
/**
 * @var string $header
 * @var array $content
 */
?>
<div class="row">
    <h1 class="page_h1" content="headline"><?= $header ?></h1>

    <div class="col-xs-12 all_sets">
        <?php for ($i = 0, $n = count($content['sets']); $i < $n; ++$i) { ?>
            <div class="set_link" itemscope itemtype="http://schema.org/SomeProducts">
                <a itemprop="url" href="/sets/<?= $content['sets'][$i]['url'] ?: $content['sets'][$i]['id'] ?>" class="link"><span
                        itemprop="name"><?= $content['sets'][$i]['name'] ?></span></a>

                <p itemprop="description"><?= $content['sets'][$i]['short_desc'] ?></p>
            </div>
        <?php } ?>
    </div>
</div>
