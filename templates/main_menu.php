<?php
/**
 * @var array $content
 * @var string $incartlink
 */
?>
<ul class="menu" itemscope itemtype="http://schema.org/SiteNavigationElement">
    <?php for ($i = 0, $n = count($content); $i < $n; ++$i) { ?>
        <li class="menu__item">
            <?php if ($i) { ?>   
            <a <?= $incartlink ?> href="/category/<?= $content[$i]['url'] ?>" class="menu__item-link"><?= $content[$i]['name'] ?></a>
            <?php } else { ?> 
            <a <?= $incartlink ?> href="/category/<?= $content[$i]['url'] ?>" class="menu__item-link organic"><span class="organic_leaf glyphicon glyphicon-leaf"></span><?= $content[$i]['name'] ?></a>
            <?php } ?>

            <div class="sub_menu">
                <?php foreach ($content[$i]['lvl_2'] as $subcat => $lvl_2_group) { ?>
                    <div class="submenu__col">
                        <p class="submenu__col-head"><?= $subcat ?></p>
                        <ul>
                            <?php for ($y = 0, $m = count($lvl_2_group['items']); $y < $m; ++$y) { ?>
                                <li>
                                    <a
                                        <?= $incartlink ?>
                                        <?php if (!empty($lvl_2_group['category-tm'])) { ?>
                                            href="<?= '/' . ($lvl_2_group['items'][$y]['cat_not_one'] ? 'category-' : '') . 'tm/' . ($lvl_2_group['items'][$y]['cat_not_one'] ? $content[$i]['url'] . '/' : '') . $lvl_2_group['items'][$y]['url'] ?>"
                                        <?php } else { ?>
                                            href="/category/<?= $content[$i]['url'] . '/' . $lvl_2_group['items'][$y]['url'] ?>"
                                        <?php } ?>
                                    >
                                        <?= $lvl_2_group['items'][$y]['name'] ?>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } ?>
            </div>
        </li>
    <?php } ?>
</ul>