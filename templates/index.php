<?php
/**
 * @var string $brands_html
 * @var string $index_banners
 * @var string $index_tms
 * @var array $main_spec
 * @var string $index_day_item
 * @var array $question_to_cosmetolog
 * @var string $index_mcs
 * @var string $page
 * @var bool $is_auth
 * @var array $review
 * @var string $header
 * @var string $content
 */

echo
    $index_banners .
    $index_tms;

Template::partial('main_special', [
    'is_auth' => $is_auth,
    'main_spec' => $main_spec,
]);
?>
<div class="row">
    <?= $index_day_item ?>
    <?php Template::partial('question_to_cosmetolog', [
        'question_to_cosmetolog' => $question_to_cosmetolog,
    ]); ?>
    <!$index_mcs>
    <?= $index_hurry ?>
</div>
<div class="review_and_sub row">
    <?php Template::partial($is_auth ? 'pay_delivery_short' : 'subscribe', [
        'page' => $page,
    ]); ?>
    <?php Template::partial('main_review', [
        'review' => $review,
    ]); ?>
</div>
<?php Template::partial('talk_to_all'); ?>
<div class="row" itemscope itemtype="http://schema.org/WebPage">
    <h1 itemprop="headline"><?= $header ?></h1>

    <div itemprop="description"><?= $content ?></div>
</div>
<?php

if (!$is_auth) {
    Template::partial('pay_delivery_long');
}

Template::partial('master_class_register_box');

Template::partial('cosmetolog_question_box');
