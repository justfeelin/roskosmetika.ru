<?php
/**
 * @var array $content
 */
if ($content['categories']) {
    $n = count($content['categories']);
?>
    <div class="lvl_3 expandable dropdown-gray-style" itemscope itemtype="http://schema.org/SiteNavigationElement">
        <div class="lvl_3_group">
            <h3 class="lvl_3_group_title">Тип продукции <i class="arrow"></i></h3>
            <div class="lvl_3_block c<?= $n ?> filters-pad">
                <?php for ($i = 0; $i < $n; ++$i) { ?>
                    <ul>
                        <?php for ($y = 0, $m = count($content['categories'][$i]); $y < $m; ++$y) { ?>
                            <li>
                                <a href="/category/<?= $content['categories'][$i][$y]['_full_url'] ?>"><?= $content['categories'][$i][$y]['name'] ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                <?php } ?>
            </div>
        </div>
    </div>
<?php
}
