<?php
/**
 * @var array $content
 */
$letterI = 0;
?>
<div id="brands-list">
    <div class="letters">
        <?php foreach ($content['letters'] as $letter => $brands) { ?>
            <div class="letter">
                <span class="letter-name"><?= $letter ?></span><?php if ($content['lastLetter'] !== $letter) { ?><span class="bull">&bull;</span><?php } ?>
                <div class="brands<?= $letterI > 21 ? ' right' : '' ?>" count-br="<?php echo count($brands);?>" >
                    <?php for ($i = 0, $n = count($brands); $i < $n; ++$i) { ?>
                        <a href="/tm/<?= $brands[$i]['url'] ?>"><?= $brands[$i]['name'] ?></a>
                    <?php } ?>
                </div>
            </div>
        <?php
            ++$letterI;
        }
        ?>
    </div>
    <div class="lbl"><a href="/tm">Бренды:</a></div>
</div>
