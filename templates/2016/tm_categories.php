<?php
/**
 * @var array $content
 */
if ($content['categories']) {
?>
    <div class="lvl_3 expandable dropdown-gray-style" itemscope itemtype="http://schema.org/SiteNavigationElement">
        <div class="lvl_3_group">
            <h3 class="lvl_3_group_title">Тип продукции <i class="arrow"></i></h3>
            <div class="lvl_3_block c1 lvl_3_last filters-pad">
                <ul>
                    <?php for ($i = 0, $n = count($content['categories']); $i < $n; ++$i) { ?>
                        <li><a href="/category-tm/<?= $content['categories'][$i]['url'] .'/' . $content['tm_url'] ?>">
                                <?= $content['categories'][$i]['name'] ?></a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
<?php
}

if ($content['lines']) {
?>
    <div class="lvl_3 expandable dropdown-gray-style" itemscope itemtype="http://schema.org/SiteNavigationElement">
        <div class="lvl_3_group">
            <h3 class="lvl_3_group_title">Линейки <i class="arrow"></i></h3>
            <div class="lvl_3_block c1 lvl_3_last filters-pad">
                <ul>
                    <?php for ($i = 0, $n = count($content['lines']); $i < $n; ++$i) { ?>
                        <li><a href="/tm/<?= $content['tm_url'] . '/' . $content['lines'][$i]['url'] ?>">
                                <?= $content['lines'][$i]['name'] ?></a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
<?php
}
