<div class="modalContainer popup" id="email-popup">
    <div class="bg"></div>
    <section>
        <div class="headd"><span class="close popup-close"></span></div>
        <div class="bodys">
            <div class="li_pop">
                <ul>
                    <li>Специальные предложения</li>
                    <li>Обзоры новинок</li>
                    <li>Мастер-классы</li>
                    <li>Советы косметолога</li>
                </ul>
                <img class="prize" src="/assets/email-popup/images/prize.png">
            </div>
            <form class="fright" data-url="/products/sales">
                <div>
                        <div class="block_sales">
                            <div class="sale">Дарим Вам скидку</div>
                            <div class="procent">-10&thinsp;%</div>
                            <div class="sale1">на первую покупку<sup>*</sup></div>
                        </div>
                        <input type="email" id="email-popup-input" placeholder="Введите свой e-mail" required="required"/>
                        <div class="checkbox">
                            <label>
                                <div class="check checked " id="check">Хочу получать рассылку</div>
                            </label>
                            <input class="checks" type="checkbox" name="option2" value="a2" id="email-popup-checkbox" checked="checked">
                        </div>
                        <div class="buttonn">
                            <br><br>
                            <button class="yes" type="submit">Хочу скидку</button>
                            <button class="no popup-close refuse" type="submit">Отказаться</button>
                        </div>
                        <br><br>
                        <div class="disclaimer">
                            * Не распространяется на продукцию Yamaguchi, Anatomico, Us Medica, Bestec, Fujiiryoki, Hakuju.
                            <br>
                            Скидки не суммируются, действует правило большей скидки, <a href="/page/sales">подробнее</a>.
                        </div>
                </div>
            </form>
            <div id="email-popup-success">
                <div>
                    <div class="sale">
                        Скидка 10% для Вас
                        <br>
                        уже доступна!
                        <br>
                        <br>
                        Просто перейдите
                        <br>
                        в корзину,
                        <br>
                        когда определитесь
                        <br>
                        с покупкой.
                        <br>
                        <br>
                    </div>
                </div>

                <button class="no popup-close" type="submit">Закрыть окно (<span id="email-popup-success-time">10</span>)</button>
            </div>
        </div>
    </section>
</div>
