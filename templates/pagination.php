<?php
/**
 * @var array $Pagination
 */
if (!empty($Pagination)) {
    if ($Pagination['pages'] > 1) {
?>
        <div class="pagination products-loading">
            <?php if ($Pagination['showAll']) { ?>
                <a href="<?= $Pagination['baseUrl'] ?>" data-page="1" class="link">По страницам</a>
            <?php } else { ?>
                <p class="ph6">Страницы</p>

                <?php if ($Pagination['showPrev']) { ?>
                    <a class="prev link" data-page="<?= $Pagination['page'] - 1 ?>" href="<?= $Pagination['page'] > 2 ? $Pagination['url'] . 'p=' . ($Pagination['page'] - 1) : $Pagination['baseUrl'] ?>"><span class="glyphicon glyphicon-arrow-left"></span></a>
                <?php
                }

                if ($Pagination['showFirst']) {
                ?>
                    <a class="first link" data-page="1" href="<?= $Pagination['baseUrl'] ?>">1</a>
                <?php
                }

                if ($Pagination['showPrevDots']) {
                ?>
                    <span class="dots">...</span>
                <?php } ?>

                <span class="numbers">
                    <?php
                    for ($i = $Pagination['start'], $n = $Pagination['end'] + 1; $i < $n; ++$i) {
                        if ($i === $Pagination['page']) {
                    ?>
                            <span class="current"><?= $i ?></span>
                        <?php } else { ?>
                            <a class="page link" data-page="<?= $i ?>" href="<?= $i > 1 ? $Pagination['url'] . 'p=' . $i : $Pagination['baseUrl'] ?>"><?= $i ?></a>
                    <?php
                        }
                    }
                    ?>
                </span>

                <?php if ($Pagination['showNextDots']) { ?>
                    <span class="dots">...</span>
                <?php
                }

                if ($Pagination['showLast']) {
                ?>
                    <a class="last link" data-page="<?= $Pagination['pages'] ?>" href="<?= $Pagination['url'] . 'p=' . $Pagination['pages'] ?>"><?= $Pagination['pages'] ?></a>
                <?php
                }

                if ($Pagination['showNext']) {
                ?>
                    <a class="next link" data-page="<?= $Pagination['page'] + 1 ?>" href="<?= $Pagination['url'] . 'p=' . ($Pagination['page'] + 1) ?>"><span class="glyphicon glyphicon-arrow-right"></span></a>
                <?php
                }

                if ($Pagination['count'] <= 300) {
                ?>
                    <a href="<?= $Pagination['url'] ?>showAll" class="link">Показать все</a>
            <?php
                }
            }
            ?>
        </div>
    <?php
    }

    if (!$Pagination['showAll'] && $Pagination['onPages'] && $Pagination['count'] > $Pagination['onPages'][0]) {
    ?>
        <div class="on-page products-loading">
            <span class="lbl">Показывать по:</span>
            <?php
            for ($i = 0, $n = count($Pagination['onPages']); $i < $n; ++$i) {
                if ($Pagination['onPage'] === $Pagination['onPages'][$i]) {
            ?>
                    <span><?= $Pagination['onPages'][$i] ?></span>
                <?php } else { ?>
                    <a href="#" class="link"><?= $Pagination['onPages'][$i] ?></a>
            <?php
                }

                if ($Pagination['count'] < $Pagination['onPages'][$i]) {
                    break;
                }
            }
            ?>
        </div>
<?php
    }
}
