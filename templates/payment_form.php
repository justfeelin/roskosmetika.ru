<?php
/**
 * @var array $order
 */
Template::partial(
    'walletone_form',
    [
        'order' => $order,
        'formID' => 'payment-form',
    ]
);
?>
<h1 id="pay-hint" style="display: none;">Выполняется переход на страницу оплаты...</h1>
<script type="text/javascript">
    (function () {
        var form;

        form = document
            .getElementById('payment-form');

        form
            .style
            .display = 'none';

        form
            .submit();

        document
            .getElementById('pay-hint')
            .style
            .display = '';
    })();
</script>
