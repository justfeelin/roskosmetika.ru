<?php
/**
 * @var int $active_aside
 */
$pages = [
    1 => [
        'url' => '/room',
        'name' => 'Персональные&nbsp;данные',
    ],
    2 => [
        'url' => '/room/delivery',
        'name' => 'Адрес&nbsp;доставки',
    ],
    3 => [
        'url' => '/room/history',
        'name' => 'История&nbsp;заказов',
    ],
    4 => [
        'url' => '/room/favorites',
        'name' => 'Избранное',
    ],
    5 => [
        'url' => '/room/password',
        'name' => 'Сменить&nbsp;пароль',
    ],
];
?>
<aside class="col-xs-3 cart_left_menu">
    <?php foreach ($pages as $index => $page) { ?>
        <a href="<?= $page['url'] ?>" class="<?= $active_aside === $index ? 'cart_aside_active' : 'cart_aside_done' ?>"><span
            <?= $active_aside !== $index ? 'class="underline_dahsed"' : ''?>><?= $page['name'] ?></span>
        </a>
    <?php } ?>
    <a href="/room/logout" class="cart_aside_done"><span class="underline_dahsed">Выход</span></a>
</aside>
