<div class="row">
    <div class="col-xs-6 pay">
        <div class="row">
            <h4 class="col-md-12 pay_title">К оплате принимаем:</h4>
        </div>
        <div class="row pay_systems">
            <p class="col-xs-2 web_money"></p>

            <p class="col-xs-2 ya_money"></p>

            <p class="col-xs-2 visa"></p>

            <p class="col-xs-2 mastercard"></p>

            <p class="col-xs-2 maestro"></p>

            <p class="col-xs-2 mir"></p>
        </div>
    </div>
    <div class="col-xs-6 shipping">
        <div class="row">
            <h4 class="col-md-12 shipping_title">Мы доставляем:</h4>
        </div>
        <div class="row shipping_comp">
            <p class="col-xs-2 ppoint"></p>

            <p class="col-xs-2 post_rf"></p>

            <p class="col-xs-2 ems"></p>

            <p class="col-xs-2 sdek"></p>
        </div>
    </div>
    <span class="col-md-6"></span>
</div>
