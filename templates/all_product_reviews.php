<?php
/**
 * @var array $content
 * @var string $header
 * @var array $month
 */
?>
<section class="row" itemscope itemtype="http://schema.org/Product">
    <div class="row">
        <div class="col-xs-7 crumbs">
            <div class="crumb_lvl_1_only"><a href="/product/<?= $content['product']['url'] ?>" itemprop="url"><span
                        itemprop="name"><?= $content['product']['name'] ?></span></a></div>
        </div>
        <div class="col-xs-5 cat_art_link"></div>
    </div>
    <h1><?= $header ?></h1>
    <?php if ($content['reviews']) { ?>
        <div class="row">
            <?php for ($i = 0, $n = count($content['reviews']); $i < $n; ++$i) { ?>
                <div class="col-xs-6" itemprop="review" itemscope itemtype="http://schema.org/Review">
                    <meta itemprop="itemReviewed" content="Отзыв о товаре <?= $content['product']['name'] ?>"/>
                    <div class="prod_reviewer"><span itemprop="author"><?= $content['reviews'][$i]['name'] ?></span>
                        <time itemprop="datePublished" class="review_date_city"
                              datetime="<?= Template::date('Y-m-d H:i+04:00', $content['reviews'][$i]['date']) ?>">,&nbsp;<?= $content['reviews'][$i]['city'] ?>
                            &nbsp;<?= $content['reviews'][$i]['day'] . '&nbsp;' . $month[$content['reviews'][$i]['month']] ?></time>
                    </div>
                    <div class="review_box" itemprop="reviewBody"><?= $content['reviews'][$i]['comment'] ?></div>
                </div>
                <?php if (!(($i + 1) % 2)) { ?>
                    </div>
                    <div class="row">
                <?php
                }
            }
            ?>
        </div>
    <?php } else { ?>
        <div class="no_items">Об этом товаре еще никто не отзывался.</div>
    <?php
    }

    Template::partial('mind_box', [
                'mb_is_auth' => $is_auth,
                'mb_top_view' => $mb_top_view,
            ]);
    ?>
</section>
