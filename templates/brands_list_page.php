<?php
/**
 * @var array $content
 */

$allBrands = [];

$min = null;

$countries = [];
$countriesLink = [];

foreach ($content['tm'] as $letter => $brands) {
    $count = count($brands);

    if ($min === null || $min > $count) {
        $min = $count;
    }

    if (preg_match('/[a-z]/i', $letter)) {
        $allBrands[$letter] = $brands;
    }

    for ($i = 0, $n = count($brands); $i < $n; ++$i) {
        $country = trim($brands[$i]['country']);

        if (!isset($countries[$brands[$i]['country_id']])) {
            $countries[$brands[$i]['country_id']] = $country;

            $countriesLink[$brands[$i]['country_id']]['country_name'] = trim($brands[$i]['country_name']);
            $countriesLink[$brands[$i]['country_id']]['country_url'] = $brands[$i]['country_url'];
            $countriesLink[$brands[$i]['country_id']]['country_visible'] = (int)$brands[$i]['country_visible'];
        }
    }
}

$allBrands['_'] = $content['tm']['_'];

$inColumnMax = $min;

$minDifference = null;

$bestInColumn = $inColumnMax;

$attempts = 0;

do {
    $column = 0;

    $columns = [0];

    foreach ($allBrands as $brands) {
        $count = count($brands);

        if (!$columns[$column] || $column === 3 || ($columns[$column] + $count) <= $inColumnMax) {
            $columns[$column] += $count;
        } else {
            $columns[++$column] = $count;
        }
    }

    if ($column === 3) {
        $min = min($columns);
        $max = max($columns);

        $newDifference = $max - $min;

        if ($minDifference === null || $newDifference < $minDifference) {
            $minDifference = $newDifference;

            $bestInColumn = $inColumnMax;
        }
    } else {
        break;
    }

    // small evil
    if (++$attempts > 500) {
        break;
    }

    ++$inColumnMax;
} while (true);

asort($countries);
asort($countriesLink);
?>
<div class="row" id="all-tm">
    <div class="row">
        <div class="col-xs-12 countries">
            <?php foreach ($countries as $countryID => $country) { ?>
                <span class="country" data-i="<?= $countryID ?>">
                    <?= $country ?>
                </span>
            <?php } ?>
            <span id="clear-countries" class="hidden">&times;</span>
        </div>
    </div>
    <div class="col-xs-3">
        <?php
        $inColumn = 0;

        $column = 1;

        $letterI = 0;

        foreach ($allBrands as $letter => $brands) {
        $n = count($brands);

        if ($inColumn && ($inColumn + $n > $bestInColumn) && $column !== 4) {
        $inColumn = 0;

        ++$column;
        ?>
    </div>
    <div class="col-xs-3">
        <?php
        }

        $rus = !preg_match('/[a-z]/i', $letter);
        ?>
        <div class="brand">
            <?php if (!$rus) { ?>
                <div class="letter"><?= $letter ?></div>
                <?php
            } else { ?>
                <div class="rus-flag"></div>
            <?php } ?>
            <ul>
                <?php for ($i = 0; $i < $n; ++$i, ++$inColumn) { ?>
                    <li itemscope itemtype="http://schema.org/Brand" class="tm vis c<?= $brands[$i]['country_id'] ?>">
                        <a itemprop="url" href="/tm/<?= $brands[$i]['url'] ?>">
                            <span itemprop="name"><?= $brands[$i]['name'] ?></span>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <?php } ?>
    </div>
    <div class="row">
        <div class="col-xs-12 countries_link">
            <?php foreach ($countriesLink as $countryID => $countryLink) { ?>
                <?php if($countryLink['country_visible']===1){?>
                <a class="country_link vis d<?= $countryID ?>" href="/country/<?= $countryLink['country_url'] ?>">
                    <span data-name="<?= $countryLink['country_name'] ?>"><?= $countryLink['country_name'] ?></span>
                </a>
                <?php } ?>
            <?php } ?>
            <span id="clear-countries" class="hidden">&times;</span>
        </div>
    </div>
</div>
