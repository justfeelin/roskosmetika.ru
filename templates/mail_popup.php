<?php
/**
 * @var array $content
 */
?>
<p style="font-family: Tahoma, Geneva, sans-serif;font-size: 15px;color: #1B1B1B;margin-top: 5px;margin-bottom: 4px;">
    Специальный подарок от Роскосметики - промо код на скидку 10% для Вас и ваших друзей!
    <br>
    При оформлении заказа в <a href="<?= DOMAIN_FULL ?>/cart" style="color: #9b1d97;">корзине</a> введите в поле "Промо код" Ваш подарочный промо код: <b>"<?= $content['promo_code'] ?>"</b> и нажмите ОК.
</p>

<div style="margin-top: 5px; font-size: 90%; font-style: italic">Внимание! Скидка не распространяется на товары по акции.</div>

<p style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 25px;margin-bottom: 4px;">
    Напоминаем, что для зарегистрированных клиентов действует <strong>постоянная</strong> скидка <b>2%</b>. Чтобы воспользоваться скидкой, просто зарегистрируйтесь на нашем сайте <a href="<?= DOMAIN_FULL ?>/" style="color: #9b1d97;">Роскосметика</a>.
</p>
