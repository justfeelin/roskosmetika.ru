<?php
/**
 * @var bool $rr Retail-rocket based block
 * @var bool $single
 * @var string $product_type
 * @var bool $special_item
 * @var array $product
 * @var bool $no_description
 * @var bool $noindex_description
 * @var $is_auth
 */

$rr = !empty($rr) ? $rr : null;
?>
<div class="col-xs-<?= empty($single) ? '3' : '12' ?> product-in-list inlist <?= $rr ? 'rr-product' : ''?> <?= empty($special_item) ? 'product-item' : 'main_spec_name' ?>">
    <div class="tag_boxes_small">
        <?php 
            if ($product['available'] && $product['is_sales']) {
        ?>
            <a
                href="#"
                rel="nofollow"
                data-link="<?= Template::hidelink('/products/sales') ?>"
                class="hidden_link">
                <span class="spec_price_tag_1">Акция</span>
                <span class="small_spec_price_box">-<?= $product['discount'] ?>%</span>
                <span class="spec_price_tag_2">осталось</span>
                <span class="spec_price_tag_3"><?= $product['is_sales'] ?>&nbsp;<span><?= helpers::num_ending($product['is_sales']) ?></span></span>
            </a>

        <?php } 

            if ($product['available'] && $product['discount'] && ($product['is_sales'] == 0)) {
        ?>
            <a
                href="#"
                rel="nofollow"
                data-link="<?= Template::hidelink('/products/hurry') ?>"
                class="hidden_link">
                <span class="hurry_tag_1">Распродажа</span>
                <span class="small_hurry_box">-<?= $product['discount'] ?>%</span>
            </a>

        <?php
        }   
            if ($product['hit'] || $product['pack_hit']) {
        ?> 

            <a
                href="#"
                rel="nofollow"
                data-link="<?= Template::hidelink('/products/hit') ?>"
                class="hidden_link small_hit_box">Хит<br>продаж</a>

        <?php
        }         
            if (($product['new'] || $product['pack_new']) && !$product['soon']) {
        ?>

            <a
                href="#"
                rel="nofollow"
                data-link="<?= Template::hidelink('/products/new') ?>"
                class="hidden_link small_new_box">Новинка</a>

        <?php
        }        
            if ($product['soon']) {
        ?>
            <span class="small_action_box">Скоро<br>в продаже</span>
        <?php
        } 
        ?>
        
    </div>
    <div class="holder">
        <div class="top-holder">
            <div class="spec_item_img">
                <a
                    href="/product/<?= $product['url'] ?>"
                    rel="nofollow"
                    <?php if ($rr) { ?>
                        onclick="try {rrApi.recomMouseDown(<?= $product['id'] ?>, {methodName: '<?= $rr ?>'});} catch(e){}"
                    <?php } ?>
                >
                <img
                    src="<?= IMAGES_DOMAIN_FULL ?>/images/prod_photo/<?= Product::img_url('s_' . $product['src'], $product['url']) ?>"
                    width="185"
                    height="185"
                    class="product-image"
                    alt="<?= h($product['alt'] ?: $product['name'] . ' от ' . $product['tm']) ?>"
                    title="<?= h($product['alt'] ?: ($product['name'] . ' от ' . $product['tm'] . ' за ' . roundPrice($product['available'] && (int)$product['special_price'] ? $product['special_price'] : $product['price']) . ' руб')) ?>">
                </a>
            </div>
            <p class="prod_link_name" data-brand="<?= $product['tm_url'] ?>">
                <a
                    class="link product-item__name ua-product-link"
                    <?php if ($rr) { ?>
                        onclick="try {rrApi.recomMouseDown(<?= $product['id'] ?>, {methodName: '<?= $rr ?>'});} catch(e){}"
                    <?php } ?>
                    href="/product/<?= $product['url'] ?>"><?= $product['tm'] . ' - '  . $product['name'] ?></a>
            </p>
            <?php if (!empty($product_type)) { ?>
                <strong>Тип: <?= $product_type ?></strong>
            <?php } ?>
            <div class="desc">
                <?php
                if (empty($no_description)) {
                    if (isset($noindex_description)) { ?>
                        <noindex>
                        <?php
                    }

                    echo $product['short_description'];

                    if (isset($noindex_description)) {
                        ?>
                        </noindex>
                        <?php
                    }
                }
                ?>
            </div>
        </div>
        <div class="variants">
            <?php for ($i = 0, $n = count($product['packs']); $i < $n; ++$i) { ?>
                <div class="row cart_info variant<?= $product['packs'][$i]['available'] && !$product['packs'][$i]['proff_price'] ? ' available' : '' ?>"
                     data-id="<?= $product['packs'][$i]['id'] ?>"
                     data-prod-url="<?= $product['url'] ?>">
                    <div class="col-xs-4 pack">
                        <div class="pack-name"><?= $product['packs'][$i]['pack'] ?></div>
                        <?php if (!$product['packs'][$i]['available'] && !$product['packs'][$i]['proff_price']) { ?>
                            <div class="row">
                                <div class="col-xs-9 pre_cart_title waiting"
                                     data-id="<?= $product['packs'][$i]['id'] ?>"
                                     data-prod-url="<?= $product['url'] ?>"
                                     data-name="<?= $product['name'] . ' ' . $product['packs'][$i]['pack'] ?>">
                                    Ожидается
                                    <span class="pre_cart_icon">i</span>
                                </div>
                            </div>
                        <?php
                        }

                        if ($product['packs'][$i]['available'] && !$product['packs'][$i]['proff_price']) {
                        ?>
                            <span class="stock-label">(В наличии)</span>
                        <?php } ?>
                    </div>
                    <?php if ($product['packs'][$i]['proff_price'] && !REAL_PROF) { ?>
                        <div class="col-xs-4 price fproff">
                            для салонов
                        </div>
                        <div class="col-xs-3 to_cart profi">
                            <a
                                class="for_proff"
                                title="Стоимость и условия продажи продукции профессионального объема"
                                href="#"
                                data-prod-name="<?= $product['name'] . ' ' . $product['packs'][$i]['pack'] ?>"
                                data-prod-id="<?= $product['id'] ?>"
                                data-prod-url="<?= $product['url'] ?>"
                                data-pack-id="<?= $product['packs'][$i]['id'] ?>"
                                data-prod-img="<?= $product['src'] !== 'none.jpg' ? '1' : '' ?>"
                            ></a>
                        </div>
                    <?php } else { ?>
                        <div class="col-xs-4 price">
                            <?php if ($product['packs'][$i]['available'] && (int)$product['packs'][$i]['special_price']) { ?>
                                <div class="old_price">
                                    <?= formatPrice($product['packs'][$i]['price']) ?>
                                    <span class="rub_h">руб</span>
                                    <span class="rub gray">p</span>
                                </div>
                            <?php } ?>
                            <div class="<?= $product['packs'][$i]['available'] && (int)$product['packs'][$i]['special_price'] ? 'new_price' : '' ?>">
                                <?= formatPrice($product['packs'][$i]['available'] && (int)$product['packs'][$i]['special_price'] ? $product['packs'][$i]['special_price'] : $product['packs'][$i]['price']) ?>
                                <span class="rub_h">руб</span>
                                <span class="rub <?= $product['packs'][$i]['available'] && (int)$product['packs'][$i]['special_price'] ? 'red' : 'black' ?>">p</span>
                            </div>
                        </div>
                        <?php if (!$product['packs'][$i]['available']) { ?>
                            <p
                                class="col-xs-3 glyphicon glyphicon glyphicon-time waiting"
                                data-id="<?= $product['packs'][$i]['id'] ?>"
                                data-prod-url="<?= $product['url'] ?>"
                                data-name="<?= $product['name'] . ' ' . $product['packs'][$i]['pack'] ?>"
                                title="Сообщить о поступлении"></p>
                        <?php } else { ?>
                            <a
                                href="#"
                                class="col-xs-3 to_cart"
                                ng-click="cartProduct(<?= $product['packs'][$i]['id'] ?>, 1, $event, false, null, <?= $rr ? "'" . $rr . "'" : 'null' ?>, <?= $rr ? $product['packs'][$i]['id'] : 0 ?>)"
                                title="Купить"
                            >Купить</a>
                        <?php } ?>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
