<?php
/**
 * @var array $content
 * @var array $month
 * @var array $master_classes
 * @var array $referral
 */
if ($content['success']) {
?>
    <div class="row">
        <h1>Регистрация успешно завершена!</h1>

        <p>Добро пожаловать в интернет-магазин Роскосметика!</p>
    </div>

    <div class="row mc-order-ok">
        <?php
        Template::partial(
            'mc_order_ok',
            [
                'month' => $month,
                'master_classes' => $master_classes,
            ]
        );
        ?>
    </div>

    <div id="referral" class="small">
        <h2>Поделись с друзьями и получи скидку!</h2>
        <div class="links">
            <?php Template::partial('referral_box', $referral); ?>
        </div>
        <p>
            Отправьте ссылку своим друзьям и получите скидку на новый заказ за каждую покупку!
            <a href="/referral">Подробнее</a>
        </p>
    </div>

<?php
Template::partial('mind_box', [
                    'mb_is_auth' => $is_auth,
                    'mb_top_view' => $mb_top_view,
                ]);
} else {
?>
    <p>Возникли проблемы при завершении регистрации, попробуйте повторить позже.</p>
<?php
}
