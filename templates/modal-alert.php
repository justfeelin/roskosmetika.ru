<div class="modal fade" id="alert-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal__frame-sm">
        <div class="modal-content modal__content">
            <div class="modal-header modal__header">
                <button type="button" class="close modal__close" data-dismiss="modal"
                        aria-hidden="true">&times;</button>
                <p class="modal-title modal__title alert-head">&nbsp;</p>
            </div>
            <div class="modal-body modal__body">
                <p class="alert-msg"></p>
            </div>
            <div class="modal-footer modal__footer">
                <button type="submit" class="btn btn-primary button-common" data-dismiss="modal">Ок</button>
            </div>
        </div>
    </div>
</div>
