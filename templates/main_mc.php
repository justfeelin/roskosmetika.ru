<?php
/**
 * @var array $content
 */
$max = 0;

for ($i = 0, $n = count($content['mcs']); $i < $n; ++$i) {
    $str_legnth = mb_strlen($content['mcs'][$i]['name']);

    if ($str_legnth > $max) {
        $max = $str_legnth;
    }
}
?>
<div class="col-xs-4 nearest_mc str<?=ceil($max/30) ?>">
    <div class="row">
        <div class="col-xs-12">
            <a class="info_title_link" href="/master_classes">Ближайшие&nbsp;мастер-классы</a>
        </div>
    </div>
    <?php for ($i = 0; $i < $n; ++$i) { ?>
        <div class="row mc-item <?= $i === 0 ? 'active-mc' : 'hidden' ?>">
            <div class="col-xs-12" itemscope itemtype="http://schema.org/EducationEvent">
                <div class="row">
                    <div class="col-xs-2">
                        <div class="pointer_left"></div>
                    </div>
                    <div class="col-xs-8 nearest_mc_link p-l-0">
                        <p><a class="link" href="/master_classes" itemprop="name"><?= $content['mcs'][$i]['name'] ?></a></p>

                        <p class="under_link"><?= $content['mcs'][$i]['series'] ?></p>
                    </div>
                    <div class="col-xs-2">
                        <div class="pointer_right"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <?php if ($content['mcs'][$i]['img']) { ?>
                        <a href="/master_classes"><img src="/images/mc/<?= $content['mcs'][$i]['img'] ?>" width="250" height="140"
                                                      alt="<?= $content['mcs'][$i]['name'] ?>"></a>
                        <?php } ?>
                        <time class="n_mc_date" itemprop="startDate"
                              datetime="<?= Template::date('c', $content['mcs'][$i]['date']) ?>">
                            <?= $content['mcs'][$i]['day'] . '&nbsp;' . $content['month'][$content['mcs'][$i]['month']] . '&nbsp;' . Template::date('H:i', $content['mcs'][$i]['date']) ?>
                            &nbsp;-&nbsp;<?= Template::date('H:i', $content['mcs'][$i]['duration']) ?>
                        </time>
                        <div class="desc" itemprop="description"><?= $content['mcs'][$i]['short_desc'] ?></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <button class="btn btn-primary button-common center-block master-class__submits"
                                data-id="<?= $content['mcs'][$i]['id'] ?>" data-name="<?= h($content['mcs'][$i]['name']) ?>">
                            Записаться
                        </button>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="row">
        <a class="col-xs-11 see_all" href="/master_classes">Все&nbsp;мастер-классы<span class="pointer_see_all"></span></a>
    </div>
</div>
