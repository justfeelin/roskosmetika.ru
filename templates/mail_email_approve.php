<?php
/**
 * @var array $content
 */
?>
<h1 style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 10px;margin-bottom: 10px;">
    Мы рады приветствовать Вас в интернет-магазине Роскосметика!</h1>

<p style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 12px;">
    Для завершения регистрации необходимо подтвердить Ваш E-mail. Для этого перейдите по ссылке ниже:
    <br>
    <a href="<?= $content['url'] ?>">ПОДТВЕРДИТЬ E-MAIL</a>
</p>
