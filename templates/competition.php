<?php
/**
 * @var array $competition
 * @var array $msg
 * @var array $values
 * @var bool $is_auth
 * @var array $main_spec
 */
?>
<?php

$user_id = Auth::is_auth();
?>

    <div class="row">
        <div class="col-xs-12">

            <img class="banner_<?= $content['id'] ?>" src="/images/competition/<?= $content['file_name'] ?>.<?= $content['type'] ?>" alt="<?= $content['alt'] ?>" width="1000>"
                 height="285" usemap="#active_link_<?= $content['id'] ?>"/>


        </div>
    </div>

    <div class="row competition">

        <div class="col-xs-6 col-xs-offset-2">
            <h1><?= $content['name'] ?></h1>
            <div><?= $content['description'] ?></div>
            <?php

            if ($content['day']>0) {
                ?>
                <form action="/competition" method="post" comp_id="<?= $content['id'] ?>" comp_user="<?= $user_id ?>">
                    <div class="form-group <?= isset($msg['e_mail']) ? 'has-error' : '' ?>">
                        <p>Ваш E-mail<span class="red">*<span></p>
                        <input name="e_mail" class="form-control fb_contacts"
                            <?= isset($values['e_mail']) ? 'value="' . h($values['e_mail']) . '"' : '' ?>>
                        <?= isset($msg['e_mail']) ? '<p class="form_error">' . $msg['e_mail'] . '</p>' : '' ?>
                    </div>
                    <div class="form-group <?= isset($msg['phone']) ? 'has-error' : '' ?>">
                        <p>Ваш телефон<span class="red">*<span></p>
                        <input id="phone" name="phone" class="form-control fb_contacts l-phone"
                            <?= isset($values['phone']) ? 'value="' . h($values['phone']) . '"' : '' ?>>
                        <?= isset($msg['phone']) ? '<p class="form_error">' . $msg['phone'] . '</p>' : '' ?>
                    </div>
                    <div class="form-group <?= isset($msg['name']) ? 'has-error' : '' ?>">
                        <p>Ваше Имя<span class="red">*<span></p>
                        <input name="name" class="form-control fb_contacts"
                            <?= isset($values['name']) ? 'value="' . h($values['name']) . '"' : '' ?>>
                        <?= isset($msg['name']) ? '<p class="form_error">' . $msg['name'] . '</p>' : '' ?>
                    </div>
                    <p><span class="red">*</span>&nbsp;-&nbsp;Обязательные&nbsp;поля</p>
                    <button class="reg_comp" type="submit" name="submit">Участвовать</button>
                    <p class="hurry_notice_info">Нажимая на кнопку «Участвовать», вы соглашаетесь на обработку персональных данных в соответствии с <a class="oferta_link" href="/page/uslovija-obrabotki-personalynih-dannyh" target="_blank">условиями</a>.</p>
                </form>
                <?php
            }else{
                ?>
                <div class="winner">
                    <a href="<?= isset($content['win_link'])? $content['win_link']:'#'?>" target="_blank">Победители здесь!</a>
                </div>
                <?php
            }

            ?>
        </div>

        <div class="col-xs-4">

            <?php
            if ($content['day']>0) {
                ?>
                <div class="comp-day">
                    <span class="comp-font-1">До конца акции осталось</span>
                    <span class="comp-font-2"><?= $content['day']?></span>
                    <span class="comp-font-3"><?= helpers::num_ending($content['day']); ?></span>
                </div>
                <?php
            }else {
                ?>
                <div>Акция завершена</div>
                <?php
            }
            ?>

        </div>
    </div>


<?php
Template::partial('talk_to_all');

if (!empty($mb_pages)) {
    ?>
    <div class="row active_spec_prod ua-product-list" data-list="Вам может подойти">
        <div class="product-h">
            <h3>Вам&nbsp;может&nbsp;подойти</h3>
        </div>
        <?php
        for ($i = 0, $n = count($mb_pages); $i < $n; ++$i) {
            Template::partial('product_in_list', [
                'is_auth' => $is_auth,
                'product' => $mb_pages[$i],
            ]);
        }
        ?>
    </div>
    <?php
}
//Template::partial(
//    'main_special',
//    [
//        'is_auth' => $is_auth,
//        'main_spec' => $main_spec,
//    ]
//);
