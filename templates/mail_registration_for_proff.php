<?php
/**
 * @var array $content
 */
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <h1 style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 10px;margin-bottom: 10px;">
                Мы рады приветствовать Вас в интернет-магазине Роскосметика!</h1>

            <div style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 12px;">
                <div style="position: relative; margin-bottom: 40px">
                    <a href="<?= DOMAIN_FULL ?>/product/<?= $content['product']['url'] ?>" target="_blank">
                        <img
                            src="<?= IMAGES_DOMAIN_FULL ?>/images/prod_photo/<?= Product::img_url('s_' . $content['product']['id'] . '.jpg', $content['product']['url']) ?>"
                            width="185" height="185"
                            style="float: right; margin-left: 15px"
                            alt="<?= $content['product']['name'] ?>"/>
                    </a>
                    <div>Цена интересующего вас товара:</div>
                    <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 15px;color: #848484;margin-top: 6px;margin-bottom: 12px;">
                        <?= $content['product']['name'] . ($content['pack'] ? $content['product']['packs'][$content['pack']]['pack'] : $content['product']['pack']) ?>
                    </p>
                    <div style="font-family: Tahoma, Geneva, sans-serif;font-size: 15px;color: #1F1F1F;margin-top: 1px;margin-bottom: 6px;">
                        <?=
                            formatPrice(
                                $content['pack'] === null
                                    ? (
                                        $content['product']['available'] && (int)$content['product']['special_price'] > 0
                                            ? $content['product']['special_price']
                                            : $content['product']['price']
                                    )
                                    : (
                                        $content['product']['packs'][$content['pack']]['available'] && (int)$content['product']['packs'][$content['pack']]['special_price'] > 0
                                            ? $content['product']['packs'][$content['pack']]['special_price']
                                            : $content['product']['packs'][$content['pack']]['price']
                                    )
                            )
                        ?>
                        &nbsp;р.
                    </div>
                    <div style="clear: both;"></div>
                </div>
            </div>

            <?php if ($content['password']) { ?>
                <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 12px;color: #3A3A3A;margin-top: 6px;margin-bottom: 12px;">
                    Чтобы всегда видеть цены на доступные товары, используйте следующие данные для входа на сайт:
                    <br>
                    Адрес электронной почты&nbsp;(Он же login):&nbsp;<b><?= $content['email'] ?></b>
                    <br>
                    Пароль: <b><?= $content['password'] ?></b>
                </p>
            <?php } ?>
        </td>
    </tr>
</table>
