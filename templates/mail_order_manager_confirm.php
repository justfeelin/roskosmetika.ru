<?php
/**
 * @var array $content
 */
?>
<p>Номер заказа: Р-<?= $content['order_id'] ?></p>

<p>Время заказа: <?= $content['order_time'] ?></p>

<p>Дата заказа: <?= $content['order_date'] ?></p>

<?php
Template::partial(
    'mail_cart_items',
    [
        'items' => $content['products'],
        'discount' => $content['discount'],
        'discount_type' => $content['discount_type'],
        'promo_discount' => $content['promo_discount'],
        'shipping_name' => $content['shipping_name'],
        'shipping_price' => $content['shipping_price'],
        'final_sum' => $content['final_sum'],
        'duration' => $content['duration'],
        'shipping_info' => $content['shipping_info'],
    ]
);
?>
<p>Информация о клиенте:</p>
<table border=1 cellspacing=0>
    <tr>
        <th>Статус</th>
        <td>ок</td>
    </tr>
    <tr>
        <th>ID</th>
        <td></td>
    </tr>
    <tr>
        <th>Юридическое имя</th>
        <td><?= $content['surname'] . '&nbsp;' . $content['name'] . '&nbsp;' . $content['patronymic'] .'&nbsp;' . $content['organization'] ?></td>
    </tr>
    <tr>
        <th>Юридический адрес</th>
        <td></td>
    </tr>
    <tr>
        <th>Фактический адрес</th>
        <td><?= $content['address'] ?></td>
    </tr>
    <tr>
        <th>Телефон</th>
        <td><?= $content['phone'] ?></td>
    </tr>
    <?php if ($content['region']) { ?>
        <tr>
            <th>Регион</th>
            <td><?= $content['region'] ?></td>
        </tr>
    <?php } ?>
    <tr>
        <th>Тип</th>
        <td>частное лицо</td>
    </tr>
    <tr>
        <th>E-mail</th>
        <td><?= $content['email'] ?></td>
    </tr>
    <tr>
        <th>% скидки</th>
        <td><?= $content['discount'] ?>%</td>
    </tr>
    <tr>
        <th>Примечение по доставке</th>
        <td><?= str_replace("\n", '<br>', $content['etc']) ?></td>
    </tr>
    <tr>
        <th>Запас ID</th>
        <td><?= $content['id'] ?></td>
    </tr>
</table>
