<?php
/**
 * @var string $header
 *
 * @var array $content
 */
?>
<div class="row">
    <h1><?= $header ?></h1>
</div>
<?php
echo $content['tmsHTML'];

Template::partial('mind_box', [
                    'mb_is_auth' => $is_auth,
                    'mb_top_view' => $mb_top_view,
                ]);