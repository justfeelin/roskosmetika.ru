<?php
/**
 * @var string $header
 * @var string $content
 * @var string $timetableHTML
 * @var array $master_classes_timetable
 * @var array $month
 * @var array $mc_down
 */
if (!empty($crumb)) {
    Template::partial(
        'breadcrumbs',
        [
            'crumbs' => [
                [
                    'name' => $crumb,
                ],
            ],
        ]
    );
}
?>
<div class="row">
    <span class="col-xs-2 mc_fist_coll "></span>

    <div class="col-xs-8">
        <h1 class="page_h1"><?= $header ?></h1>

        <p><?= $content ?></p>
    </div>
    <span class="col-xs-2"></span>
</div>
<?php
Template::partial(
    'mc_timetable',
    [
        'master_classes' => $master_classes_timetable,
        'month' => $month,
    ]
);
?>
<div class="row">
    <span class="col-xs-2 mc_fist_coll "></span>

    <div class="col-xs-8"><?= $mc_down['content'] ?></div>
    <span class="col-xs-2"></span>
</div>
<?php
Template::partial('mind_box', [
                'mb_is_auth' => $is_auth,
                'mb_top_view' => $mb_top_view,
            ]);

Template::partial('master_class_register_box');
