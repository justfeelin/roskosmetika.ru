<div id="cart-present-popup" class="modal-popup once">
    <div class="bg"></div>

    <div class="box">
        <div class="holder">
            <div class="close-btn close">×</div>
            <p class="ph3">МЫ ДАРИМ ВАМ <span>ПОДАРОК</span></p>

            <p>В вашей корзине товары на сумму > <?= Orders::PRESENT_SUM ?> руб.</p>
            <p>и поэтому мы добавили в ваш заказ подарок, который</p>
            <p>доставят вам с покупкой. Просто оформите заказ ;)</p>

            <a href="#" class="close-btn continue lnk">ПРОДОЛЖИТЬ ПОКУПКИ</a>
            <a href="<?= CART_PAGE ? '#' : '/cart' ?>" class="proceed lnk<?= CART_PAGE ? ' close-btn' : '' ?>">ОФОРМИТЬ ЗАКАЗ</a>
        </div>
        <div class="img"></div>
    </div>
</div>
