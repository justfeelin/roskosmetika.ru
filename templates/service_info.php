<?php
/**
 * @var string $header
 * @var string $content
 */
?>
<div class="row page">
    <?= isset($header) ? '<h1>' . $header . '</h1>' : '' ?>
    <div class="col-xs-12">
        <div class="no_items"><?= $content ?></div>
    </div>
</div>
