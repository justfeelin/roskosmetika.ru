<?php
/**
 * @var array $content
 */
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <h1 style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 10px;margin-bottom: 10px;">
                Коллектив интернет-магазина Роскосметика приветствует Вас.</h1>

            <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 12px;">
                Ваш пароль: <b><?= $content['password'] ?></b></p>
        </td>
    </tr>
</table>
