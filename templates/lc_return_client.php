<?php
/**
 * @var array $content
 */
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <?= $content['text'] ?>
        </td>
    </tr>

    <?= $content['neighbours_html'] ?>
</table>