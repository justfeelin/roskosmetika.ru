<div id="social-walls">
    <div class="row">
        <div class="col-xs-12 col vk">
            <h4 class="info_title_link">Мы в социальных сетях</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4 col">
            <script type="text/javascript" src="//vk.com/js/api/openapi.js?117"></script>

            <!-- VK Widget -->
            <div id="vk_groups"></div>
            <script type="text/javascript">
                VK.Widgets.Group("vk_groups", {mode: 0, width: "300", height: "400", color1: 'FFFFFF', color2: '5A9320', color3: '8CD345'}, 21757533);
            </script>
        </div>
        <div class="col-xs-4 col ok">
            <div id="ok_group_widget"></div>
            <script>
                !function (d, id, did, st) {
                    var js = d.createElement("script");
                    js.src = "https://connect.ok.ru/connect.js";
                    js.onload = js.onreadystatechange = function () {
                        if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {
                            if (!this.executed) {
                                this.executed = true;
                                setTimeout(function () {
                                    OK.CONNECT.insertGroupWidget(id,did,st);
                                }, 0);
                            }
                        }}
                    d.documentElement.appendChild(js);
                }(document,"ok_group_widget","53145173885166","{width:300,height:400}");
            </script>
        </div>
        <div class="col-xs-4 col fb">
            <div id="fb-root"></div>
            <script>(function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.5";
                    js.setAttribute('async', '');
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
            <div class="fb-page" data-href="https://www.facebook.com/roskosmetika/" data-width="300" data-height="400" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"></div>
        </div>
    </div>
</div>
