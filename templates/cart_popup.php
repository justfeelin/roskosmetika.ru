<div id="cart-popup">
    <div class="products">
        <div ng-repeat="product in cart.products" class="row">
            <div class="col-xs-2">
                <a ng-href="{{'/product/' + product.url}}">
                    <img width="60" height="60" ng-alt="{{product.alt}}" ng-title="{{product.photo_title}}" ng-src="{{'<?= IMAGES_DOMAIN_FULL ?>/images/prod_photo/' + imgUrl('min_' + product.src, product.url) }}">
                </a>
            </div>
            <div class="col-xs-7">
                <a ng-href="{{'/product/' + product.url}}">
                    <div ng-bind-html="product.name" class="product-name link"></div>
                    <p ng-bind-html="product.pack"></p>
                </a>
            </div>
            <div class="col-xs-3 price-box">
                <span ng-show="product.price > 0">
                    <span ng-bind="product.price|price" class="price-label"></span>
                    <span class="rub_h">руб</span>
                    <span class="rub">p</span>
                </span>
                <span ng-show="product.price === 0">
                    Бесплатно
                </span>
            </div>
        </div>
    </div>
    <div class="bottom">
        <a data-link="<?= Template::hidelink('/cart') ?>" class="hidden_link" href="#">Оформить заказ</a>

        <p>Сумма заказа</p>
        <div class="price-box">
            <span ng-bind="cart.info.final_sum|price" class="final-sum"></span>
            <span class="rub_h">руб</span>
            <span class="rub">p</span>
        </div>
    </div>
</div>
