<div id="one-click-buy-popup" class="modal-popup">
    <div class="bg"></div>

    <div class="box">
        <div class="holder">
            <div class="close-btn close">×</div>
            <p class="ph3">Оформить заказ в один клик</p>

            <a href="#" id="one-click-buy-prev" class="arrow inactive">&lt;</a>
            <a href="#" id="one-click-buy-next" class="arrow next">&gt;</a>

            <div class="prs">
                <div class="prs-holder">
                    <div ng-repeat="product in cart.products" class="pr">
                        <a ng-href="{{'/product/' + product.url}}">
                            <img
                                width="85" height="85" ng-alt="{{product.alt}}" ng-title="{{product.photo_title}}"
                                ng-src="{{'<?= IMAGES_DOMAIN_FULL ?>/images/prod_photo/' + imgUrl('min_' + product.src, product.url)}}">
                            <div ng-bind-html="product.name"></div>
                        </a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

            <form class="form-horizontal">
                <div class="form-group">
                    <label for="ocb-phone" class="col-xs-3 control-label">Телефон<sup>*</sup></label>
                    <div class="col-xs-8">
                        <input type="tel" class="form-control phone-input l-phone" id="ocb-phone" name="phone" required="required">
                    </div>
                </div>
                <div class="form-group">
                    <label for="ocb-name" class="col-xs-3 control-label">Ваше имя</label>
                    <div class="col-xs-8">
                        <input type="text" class="form-control l-name" id="ocb-name" name="name">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-offset-3 col-xs-8">
                        <button type="submit" class="cart_checkout">Оформить заказ</button>
                        <p>* &mdash; обязательное поле</p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
