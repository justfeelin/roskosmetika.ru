<?php
/**
 * @var array $content
 * @var array $Pagination
 * @var string $header
 * @var string $found
 */

if ($content['products']) {
?>
    <?php
    Template::partial(
        'product_filter',
        [
            'filters' => isset($content['filters']) ? $content['filters'] : [],
            'name' => $header,
            'found' => $found,
            'content' => $content,
        ]
    );

    $paginationHTML = Template::get_tpl(
        'pagination_wrap',
        [
            'Pagination' => $Pagination,
        ]
    );

    echo $paginationHTML;
    ?>
    <div class="col-xs-12 products">
        <div class="ua-product-list product-list" data-list="<?= $content['short_name'] ?>">
            <?php
            Template::partial(
                'product_list',
                [
                    'products' => $content['products'],
                ]
            );
            ?>
        </div>
        <?= $paginationHTML ?>
    </div>
<?php } else { ?>
    <h2 class="no_items">К сожалению группа пуста. Товары не найдены.</h2>
<?php
}
