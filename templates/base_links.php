<?php
/**
 * @var array $links
 */
if (!$links) {
    return;
}

$n = count($links);
?>
<div class="row base-links">
    <div class="col-xs-12">
        <h3>Рекомендуем посмотреть</h3>

        <?php for ($i = 0; $i < $n; ++$i) { ?>
            <a class="link" href="<?= $links[$i]['url'] ?>"><?= $links[$i]['anchor'] ?></a>
        <?php } ?>
    </div>
</div>
