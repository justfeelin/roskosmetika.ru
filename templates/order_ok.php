<?php
/**
 * @var array $content
 * @var array $referral
 * @var bool $moscowRegion
 * @var array $month
 * @var array $master_classes
 */
?>
<div class="row">
    <div class="col-xs-12<?= $content['payUrl'] ? ' order-ok-pay' : '' ?>">
        <p>Ваш заказ номер <span class="full_sum"><b>Р-<?= $content['order_id'] ?></b></span> оформлен.</p>

        <?php if (!$content['payUrl']) { ?>
            <p>Менеджер отдела продаж свяжется с Вами в ближайшее время.</p>
        <?php } else { ?>
            <p>Чтобы оплатить сертификат, перейдите по ссылке:</p>

            <a class="btn btn-primary button-common" href="<?= $content['payUrl'] ?>">
                Оплатить
            </a>

            <p>Сразу после оплаты Вы автоматически получите сертификат на Вашу электронную почту.</p>
        <?php } ?>
    </div>
</div>
<div class="row mc-order-ok">
    <?php
    Template::partial(
        'mc_order_ok',
        [
            'month' => $month,
            'master_classes' => $master_classes,
        ]
    );
    ?>
</div>
<div class="row">
    <?php if ($referral) { ?>
        <div id="referral" class="small">
            <h2>Поделись с друзьями и получи скидку!</h2>
            <div class="links">
                <?php Template::partial('referral_box', $referral); ?>
            </div>
            <p>
                Отправьте ссылку своим друзьям и получите скидку на новый заказ за каждую покупку!
                <a href="/referral">Подробнее</a>
            </p>
        </div>
    <?php
    }
    ?>
</div>
<div class="row">
    <div class="col-xs-6">
        <h2>Стоимость доставки в&nbsp;Москве и Санкт-Петербурге:</h2>
        <ul>
            <li>Заказов&nbsp;на&nbsp;сумму&nbsp;<b>менее&nbsp;4&nbsp;000&nbsp;рублей</b>&nbsp;&mdash;&nbsp;299&nbsp;рублей<span
                    class="red">*</span>.
            </li>
            <li>Заказов на&nbsp;сумму <b>от 4&nbsp;000 до 5&nbsp;500 рублей</b><strong>&nbsp;&mdash;&nbsp;<span
                        class="font_accent">98&nbsp;рублей(Мск),&nbsp;219&nbsp;рублей(Спб)</span></strong><span class="red">*</span>.
            </li>
            <li>Заказов на&nbsp;сумму <b>более 5&nbsp;500 рублей</b><strong>&nbsp;&mdash;&nbsp;<span
                        class="font_accent">бесплатно(Мск),&nbsp;108&nbsp;рублей(Спб)</span></strong><span class="red">*</span>.
            </li>
        </ul>
        <p class="italic_font"><span class="red">*</span>&nbsp;Тариф зависит от удаленности от МКАД (КАД), веса и объема
            заказа, и способа его доставки (Почта России, транспортная компания или курьерская служба).</p>
    </div>
    <div class="col-xs-6">
        <h2>Стоимость доставки в другие регионы России:</h2>
        <ul>
            <li>заказов на&nbsp;сумму <b>менее 4&nbsp;000 рублей</b>&nbsp;&mdash; от&nbsp;355&nbsp;рублей<span
                    class="red">*</span>.
            </li>
            <li>заказов на&nbsp;сумму <b>более 4&nbsp;000 рублей</b><strong>&nbsp;&mdash;&nbsp;<span
                        class="font_accent">от&nbsp;154&nbsp;рубля</span></strong><span class="red">**</span>.
            </li>
        </ul>
        <p class="italic_font"><span class="red">*</span>&nbsp;Тариф зависит от веса, объема и способа доставки заказа
            (Почта России, транспортная компания или курьерская служба).</p>

        <p class="italic_font"><span class="red">**</span>&nbsp;Доставка осуществляется бесплатно, если тариф
            перевозчика не превышает 181 рубль. Если тариф перевозчика превышает 181 рублей, стоимость доставки
            рассчитывается по формуле: тариф минус 181 рубль.</p>
    </div>
</div>
<div class="row">
    <!-- Google Code for &#1054;&#1092;&#1086;&#1088;&#1084;&#1083;&#1077;&#1085;&#1080;&#1077; &#1079;&#1072;&#1082;&#1072;&#1079; Conversion Page -->
    <!-- {{{ -->
    <script type="text/javascript"><?= $content['main'] ?>
        /* <![CDATA[ */
        var google_conversion_id = 1016792231;
        var google_conversion_language = "ru";
        var google_conversion_format = "1";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "UOf3COnr0QIQp4ns5AM";
        var google_conversion_value = 0;
        /* ]]> */
    </script>
    <!-- }}} -->
    <script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt=""
                 src="https://www.googleadservices.com/pagead/conversion/1016792231/?label=UOf3COnr0QIQp4ns5AM&amp;guid=ON&amp;script=0"/>
        </div>
    </noscript>
</div>
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=W66lQO4HLhzFjChX2JJmfXkEYZCJwnaclpc8ORcceR3HTNcq8EWRXrkm7ZW8H6CvUQ/RjBxaNOwnLph73yBXObyjdsfy4zWh8FTFCqWKN8BoZmL69tR*NZh057gOxItahnC2QOcYNbgT2YSAXTiqq6vdUxVBEJqcKPBYS7ql1e8-';</script>

<?php

Template::partial('master_class_register_box');

Template::partial('cosmetolog_question_box');

if (!empty($content['mb_ok_page'])) {
    ?>
    <div class="row active_spec_prod ua-product-list" data-list="Вам может подойти">
        <div class="product-h">
            <h3>Вам может подойти</h3>
        </div>
        <?php
        for ($i = 0, $n = count($content['mb_ok_page']); $i < $n; ++$i) {
            Template::partial('product_in_list', [
                'is_auth' => $is_auth,
                'product' => $content['mb_ok_page'][$i],
            ]);
        }
        ?>
    </div>
    <?php
}
?>
