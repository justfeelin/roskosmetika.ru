<?php
/**
 * @var string $header
 * @var array $products
 */
?>
<div class="personalized">
    <h3><?= $header ?: 'Вам может подойти' ?></h3>

    <div class="col-xs-12 products">
        <div class="ua-product-list product-list" data-list="Блок «Вам может подойти»">
            <?php Template::partial(
                'product_list',
                [
                    'products' => $products,
                ]
            ); ?>
        </div>
    </div>
</div>
