<?php
/**
 * @var string $header
 * @var array $content
 */
if (!empty($content['crumbs'])) {
    Template::partial(
        'breadcrumbs',
        [
            'crumbs' => $content['crumbs'],
        ]
    );
}

if (!empty($header)) {
?>
<div class="row">
    <div class= "col-xs-12">
        <div class="row products-header filters-box">
            <div class="col-xs-12 products-header-top">
                <div class="prods_title_sort">
                    <h1><?= $header ?></h1>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<div class="col-xs-12 products">
    <div class="ua-product-list product-list">
        <?php Template::partial(
            'product_list',
            [
                'products' => $content['products'],
            ]
        ); ?>
    </div>
</div>
