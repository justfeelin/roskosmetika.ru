<?php
/**
 * @var array $content
 */
for ($i = 0, $n = count($content['categories']); $i < $n; ++$i) {
?>
    <div class="lvl_3 expandable dropdown-gray-style" itemscope itemtype="http://schema.org/SiteNavigationElement">
        <div class="lvl_3_group">
            <h3 class="lvl_3_group_title"><?= $content['categories'][$i]['sub_name'] ?> <i class="arrow"></i></h3>
            <div class="lvl_3_block c<?= count($content['categories'][$i]['cats']) ?> <?= $i === $n - 1 ? 'lvl_3_last' : '' ?> filters-pad">
                <?php for ($y = 0, $m = count($content['categories'][$i]['cats']); $y < $m; ++$y) { ?>
                    <ul>
                        <?php for ($z = 0, $o = count($content['categories'][$i]['cats'][$y]); $z < $o; ++$z) { ?>
                            <li<?= $content['categories'][$i]['cats'][$y][$z]['id'] == $content['level_3_id'] ? ' class="lvl_3_active"' : '' ?>>
                                <a href="/category/<?= $content['categories'][$i]['cats'][$y][$z]['_full_url'] ?>">
                                    <?= $content['categories'][$i]['cats'][$y][$z]['name'] ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                <?php } ?>
            </div>
        </div>
    </div>
<?php
}
