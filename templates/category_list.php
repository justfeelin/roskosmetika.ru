<?php
/**
 * @var string $header
 * @var array $categories
 */
?>
<div class="row lvl_3">
    <div class="lvl_3_group lvl_3_last">
        <?php if (!empty($header)) { ?>
            <h3 class="lvl_3_group_title"><?= $header ?></h3>
        <?php
        }

        if (!isset($inColumn)) {
            $inColumn = 4;
        }

        $urls = 0;
        ?>
        <div class="lvl_3_block lvl_3_last">
            <ul>
                <?php for ($i = 0, $n = count($categories); $i < $n; ++$i) { ?>
                    <li>
                        <a href="/category/<?= $categories[$i]['_full_url'] ?>"><?= $categories[$i]['h1'] ?></a>
                    </li>
                    <?php if (!(($i + 1) % $inColumn)) { ?>
                        </ul>
                        <?php
                        ++$urls;

                        if (!($urls % 6)) { ?>
                            <div class="pclear"></div>
                        <?php } ?>
                        <ul>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>
