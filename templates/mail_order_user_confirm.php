<?php
/**
 * @var array $content
 */
?>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <h1 style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 10px;margin-bottom: 10px;">
                            Благодарим за покупку в нашем магазине, <?= $content['surname'] . '&nbsp;' . $content['name'] ?>
                            &nbsp;<?= $content['patronymic'] ?>!</h1>

                        <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 12px;">
                            Ваш заказ &#8470;&nbsp;<b>Р-<?= $content['order_id'] ?></b> от <?= $content['order_date'] ?>
                            принят к
                            исполнению.
                            <?php if ($content['address'] || $content['region']) { ?>
                                <br>Адрес доставки: <?= ($content['region'] ? $content['region'] . ($content['address'] ? ', ' : '') : '') . $content['address'] ?>.
                            <?php
                            }

                            if ($content['etc']) {
                            ?>
                                <br>Примечания по заказу: <?= $content['etc'] ?>.
                            <?php } ?>
                            <br><br>
                            В ближайшее время по контактному телефону <?= $content['phone'] ?>, который Вы указали,
                            с вами свяжется наш менеджер для уточнения деталей заказа. </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php
                        Template::partial(
                            'mail_cart_items',
                            [
                                'items' => $content['products'],
                                'discount' => $content['discount'],
                                'discount_type' => $content['discount_type'],
                                'promo_discount' => $content['promo_discount'],
                                'shipping_name' => $content['shipping_name'],
                                'shipping_price' => $content['shipping_price'],
                                'final_sum' => $content['final_sum'],
                                'duration' => $content['duration'],
                                'shipping_info' => $content['shipping_info'],
                            ]
                        );
                        ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td>
            <h3 style="font-size: 16px; text-align: center; margin: 30px 0; color: #555555;">ПРИВОДИТЕ ДРУЗЕЙ И ПОЛУЧИТЕ СКИДКУ 20%!</h3>

            <a style="margin-left: 195px; background-color: #9b5ba5; padding: 15px 35px; text-transform: uppercase; font-size: 16px; text-decoration: none; border-radius: 30px; color: #fff; -moz-border-radius: 30px; -webkit-border-radius: 30px; " target="_blank" href="<?= DOMAIN_FULL ?>/referral<?= apply_utm('', 'order-ok-referral') ?>">Хочу скидку 20%!</a>
            <div style="height: 20px;"></div>
        </td>
    </tr>

    <?= $content['neighbours_html'] ?>

    <tr>
        <td>
            <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 11px;color: #1B1B1B;margin-top: 6px;margin-bottom: 6px; font-style: italic;">
                Напоминаем, что Вы всегда можете изменить заказ, просмотреть историю покупок, а
                также отредактировать персональную информацию в "Личном кабинете" на
                <a href="<?= DOMAIN_FULL . '/' . apply_utm('', 'order-ok-bottom') ?>" target="_blank">сайте</a>.
            </p>
        </td>
    </tr>
</table>
