<div class="modalContainer popup" id="referral-popup">
    <div class="bg"></div>
    <section>
        <header>добро пожаловать на Roskosmetika.ru <span class="popup-close close">&times;</span></header>
        <div class="modalContent">
            <h3>Вы получили скидку 20% по приглашению друга!</h3>

            <p>Оформи заказ &mdash; получи скидку 20% на каждый товар!</p>

            <form action="#">
                <h4>Ещё больше скидок!</h4>
                <input id="email-popup-input" type="email" placeholder="Введите свой E-mail">
                <button type="submit">Подписаться</button>
            </form>
        </div>
        <footer>
            <ul>
                <li>удобная доставка</li>
                <li>гибкая система скидок</li>
                <li>оплата любым способом</li>
            </ul>
            <a href="#" class="popup-close">перейти на сайт</a>
        </footer>
    </section>
</div>
