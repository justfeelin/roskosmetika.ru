<?php
/**
 * @var string $header
 * @var string $msg
 * @var string $email
 * @var string $hash
 */
?>
<div class="row">
    <h1><?= $header ?></h1>

    <div id="content">
        <?php if (isset($msg)) { ?>
            <p><?= $msg ?></p>
        <?php } else { ?>
        <p>Уважаемые покупатели!</p>
        <p>Мы стараемся держать Вас в курсе последних новостей нашего интернет-магазина. Из наших рассылок Вы
            сможете оперативно узнавать о <a href="/products/new">новинках косметики</a>, ближайших <a
                href="/master_classes">мастер-классах</a>, <a href="/products/sales">специальных акциях</a> и
            других полезных новостях!</p>
        <p>Отписаться от рассылки Вы можете по этой <a
                href="/unsubscribe/add_unsubscribe/<?= $email . '/' . $hash ?>">ссылке</a>.</p>
        <?php
        }

        Template::partial('mind_box', [
                        'mb_is_auth' => $is_auth,
                        'mb_top_view' => $mb_top_view,
                    ]);
        ?>
    </div>
</div>
