<?php
/**
 * @var string $header
 * @var int $active_aside
 * @var array $content
 */
?>
<section class="room">
    <div class="row">
        <h1 class="col-xs-6 col-xs-offset-3"><?= $header ?></h1>
    </div>
    <div class="row">
        <?php
        Template::partial(
            'room_left_menu',
            [
                'active_aside' => $active_aside,
            ]);
        ?>
        <div class="col-xs-7">
            <?php
            if (!empty($content['items'])) {
                for ($i = 0, $n = count($content['items']); $i < $n; ++$i) {
            ?>
                    <div class="row favourite_item">
                        <a href="/product/<?= $content['items'][$i]['url'] ?>" rel="nofollow"
                           class="col-xs-2 cart_photo"><img width="85" height="85"
                                                            src="<?= IMAGES_DOMAIN_FULL ?>/images/prod_photo/<?= Product::img_url('min_' . $content['items'][$i]['src'], $content['items'][$i]['url']) ?>"></a>

                        <div class="col-xs-7 cart_desc">
                            <a href="/product/<?= $content['items'][$i]['url'] ?>"
                               class="link"><?= $content['items'][$i]['name'] ?></a>

                            <p class="cart_pack"><?= $content['items'][$i]['pack'] ?></p>

                            <p><?= formatPrice($content['items'][$i]['price']) ?> <span class="rub">p</span></p>
                        </div>
                        <div class="col-xs-3 favorite_btns">
                            <?php if ($content['items'][$i]['available'] && $content['items'][$i]['visible']) { ?>
                                <a href="#" ng-click="cartProduct(<?= $content['items'][$i]['id'] ?>, 1, $event)"
                                   class="to_cart">Купить</a>
                            <?php
                            } else {
                                if (!$content['items'][$i]['visible']) {
                            ?>
                                    <p class="cant_repeat">Нет&nbsp;в&nbsp;продаже</p>
                                <?php } else { ?>
                                    <p class="cant_repeat">Ожидается</p>
                            <?php
                                }
                            }
                            ?>
                            <a href="/room/del_favorites/<?= $content['items'][$i]['id'] ?>"
                               class="order_cancel">Убрать</a>
                        </div>
                    </div>
            <?php
                }
            } else {
            ?>
                <div class="no_items">У&nbsp;Вас&nbsp;нет&nbsp;избранных&nbsp;товаров.</div>
            <?php } ?>
        </div>
        <div class="col-xs-2"></div>
    </div>
    <?php Template::partial('mind_box', [
                    'mb_is_auth' => $is_auth,
                    'mb_top_view' => $mb_top_view,
                ]); ?>
</section>
