<?php
/**
 * @var array $content
 * @var bool $is_auth
 */
?>
<div class="row pre_404">
    <div class="col-xs-6">
        <p>Страница, которую вы ищете больше не существует, либо Вы случайно ошиблись в адресе.</p>

        <p>Попробуйте перейти на <a href="/">главную</a> страницу или воспользоваться поиском выше.</p>
    </div>
    <div class="col-xs-6 subscribe">
        <div class="row sub_backgroung">
            <div class="col-xs-11">
                <div class="rew_sub_area">
                    <p class="rew_sub_title">Есть вопросы и нужна помощь?</p>

                    <div class="sub_desc">
                        <p>Позвоните нам с 10:00 до 19:00</p>

                        <p class="phone_number">8 800 775-54-83<br><span class="nowrap">(Звонок по России бесплатный)</span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" style="position: relative">
    <img class="error_img" src="/images/404.png">
    <img style="position: absolute; top: -10px; left: 270px" src="/images/travolta.gif">
</div>
<?php
Template::partial(
    'main_special',
    [
        'main_spec' => $content['main_spec'],
        'is_auth' => $is_auth,
    ]
);
Template::partial('mind_box', [
                'mb_is_auth' => $is_auth,
                'mb_top_view' => $mb_top_view,
            ]);
