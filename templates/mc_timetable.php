<?php
/**
 * @var array $master_classes
 * @var array $month
 */
?>
<div class="row">
    <span class="col-xs-2 mc_fist_coll "></span>

    <p class="col-xs-8 mcs_title">Расписание&nbsp;курсов&nbsp;и&nbsp;мастер-классов</p>
    <span class="col-xs-2"></span>
</div>
<?php

if ($master_classes) {
    for ($i = 0, $n = count($master_classes); $i < $n; ++$i) {
?>
        <div class="row master_class" itemscope itemtype="http://schema.org/EducationEvent" id="mc-<?= $master_classes[$i]['id'] ?>" name="mc-<?= $master_classes[$i]['id'] ?>">
            <div class="col-xs-2 align_center mc_fist_coll mc_img_coll">
                <time itemprop="startDate"
                      datetime="<?= Template::date('Y.m.d H:i', $master_classes[$i]['date']) ?>"><?= $master_classes[$i]['day'] ?>
                    &nbsp;<?= $month[$master_classes[$i]['month']] . '&nbsp;' . Template::date('H:i', $master_classes[$i]['date']) ?></time>
                <?php if ($master_classes[$i]['img']) { ?>
                    <p><img width="150" height="95" src="/images/mc/<?= $master_classes[$i]['img'] ?>"></p>
                <?php } ?>
                <button class="btn btn-primary button-common center-block master-class__submits"
                        data-id="<?= $master_classes[$i]['id'] ?>"
                        data-name="<?= h($master_classes[$i]['name']) ?>">
                    Записаться
                </button>
            </div>
            <div class="col-xs-8">
                <h2 itemprop="name"><?= $master_classes[$i]['name'] ?></h2>

                <div itemprop="description"><?= $master_classes[$i]['description'] ?></div>
            </div>
            <span class="col-xs-2"></span>
        </div>
<?php
    }
} else {
?>
    <div class="row">
        <span class="col-xs-2 mc_fist_coll "></span>

        <p class="col-xs-8">Записаться на мастер-класс и узнать время проведения ближайшего мастер-класса можно у
            менеджера по телефону 8 800 775-54-83 (Звонок по России бесплатный).</p>
        <span class="col-xs-2"></span>
    </div>
<?php
}
