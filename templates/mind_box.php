<?php
/**
 * @var array $mb_top_view
 * @var int   $mb_is_auth
 */
?>

<?php
if (!empty($mb_top_view)) {
    ?>
    <div class="row active_spec_prod ua-product-list" data-list="Вам может подойти">
        <div class="product-h">
            <h3>Вам&nbsp;может&nbsp;подойти</h3>
        </div>
        <?php
        for ($i = 0, $n = count($mb_top_view); $i < $n; ++$i) {
            Template::partial('product_in_list', [
                'is_auth' => $mb_is_auth,
                'product' => $mb_top_view[$i],
            ]);
        }
        ?>
    </div>
    <?php
}
