<?php
/**
 * @var array $content
 * @var array $Pagination
 */

Template::partial(
    'breadcrumbs',
    [
        'crumbs' => [
            [
                'name' => 'Косметика',
                'url' => '/kosmetika',
            ],
            [
                'name' => $content['info']['type_page_name'],
                'url' => '/kosmetika/' . $content['info']['type_page_url'],
            ],
            [
                'name' => $content['info']['tm_name'],
            ],
        ],
    ]
);

Template::partial('product_filter_wrap', [
    'content' => [
        'filters' => $content['filters'],
        'filter_prices' => $content['filter_prices'],
        'name' => $content['info']['h1'],
        'found' => Pagination::getCount(),
    ],
]);

if ($content['products']) {
    $paginationHTML = Template::get_tpl('pagination_wrap', null, [
        'Pagination' => !empty($Pagination) ? $Pagination : null,
    ]);
    ?>
    <div class="row">
        <div class="col-xs-12">
            <?= $paginationHTML ?>
        </div>
    </div>
    <div class="col-xs-12 products">
        <div class="ua-product-list product-list" data-list="<?= 'Косметика ' . $content['info']['type_page_name'] . ' ' . $content['info']['tm_name'] ?>">
            <?php Template::partial(
                'product_list',
                [
                    'products' => $content['products'],
                    'product_type' => $content['info']['type_page_name'],
                    'product_type_n' => $content['info']['type_page_name_on_products'],
                ]
            ); ?>
        </div>

        <?php

        echo $paginationHTML;

        if (!empty($content['popular_products'])) {
            ?>
            <h3>Популярные товары</h3>
            <?php
            Template::partial(
                'product_list',
                [
                    'products' => $content['popular_products'],
                    'no_description' => true,
                ]
            );
        }

        if ($content['tms']) {
        ?>
            <div class="row bottom-tms tms">
                <div class="col-xs-12">
                    <div class="all-tms tms-box">
                        <div class="tm-menu_all">
                            <div class="lvl_3_group_title">Другие торговые марки этой косметики</div>
                        </div>
                        <div class="sub_tm_menu">
                            <?php for ($i = 0, $n = count($content['tms']); $i < $n; ++$i) { ?>
                                <a class="tm_menu_title" href="/kosmetika/<?= $content['info']['type_page_url'] . '/' . $content['tms'][$i]['url'] ?>"><?= $content['tms'][$i]['name'] ?></a>
                            <?php } ?>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        <?php

        }
        if ($content['reviews']) {
        ?>
            <a class="info_title_link" href="/otzyvy/kosmetika/<?= $content['info']['type_page_url'] . '/' . $content['info']['tm_url'] ?>">Отзывы <?= $content['reviews_title'] ?></a>
            <div class="row">
                <?php Template::partial(
                    'product_reviews',
                    [
                        'reviews' => $content['reviews'],
                    ]
                ); ?>
            </div>
        <?php } ?>
    </div>
<?php } else { ?>
    <div class="no_items">К сожалению, в данный момент товары в этой категории не представлены<?= !empty($content['similar']) ? ',<br>но Вы можете ознакомиться с похожими разделами:' : '.' ?></div>
    <?php
    Template::personalized_products();
}
?>
<div class="row prods_title_sort" itemscope itemtype="http://schema.org/WebPage">
    <div class="col-xs-12">
        <?php if ($content['info']['text']) { ?>
            <div class="read_more_desc" itemprop="description" data-size="145">
                <?= $content['info']['text'] ?>
            </div>
        <?php } ?>
    </div>
</div>
<?php
Template::partial(
    'base_links',
    [
        'links' => $content['base_links'],
    ]
);
