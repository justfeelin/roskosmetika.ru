<div class="modal fade" id="review-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="true">
    <div class="modal-dialog modal__frame">
        <div class="modal-content modal__content">
            <form class="form-horizontal" role="form" id="comment-form">
                <div class="modal-header modal__header">
                    <button type="button" class="close modal__close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title modal__title" id="myModalLabel">
                        Отзыв о товаре
                        "<span class="prod-name"></span>"
                    </h4>
                </div>
                <div class="modal-body modal__body">
                    <div class="form-group modal__row">
                        <label for="cf_name" class="col-xs-3 control-label modal__label">ФИО:</label>
                        <div class="col-xs-9">
                            <input type="text" class="form-control l-name form-name" id="cf_name">
                        </div>
                    </div>
                    <div class="form-group modal__row">
                        <label for="city" class="col-xs-3 control-label modal__label">Откуда Вы:</label>
                        <div class="col-xs-9">
                            <input type="text" class="form-control l-address" id="city">
                        </div>
                    </div>
                    <div class="form-group modal__row">
                        <label for="cf_email" class="col-xs-3 control-label modal__label">Email:<span class="form-required">*</span></label>
                        <div class="col-xs-9">
                            <input type="text" class="form-control l-email form-email" id="cf_email">
                        </div>
                    </div>
                    <div class="form-group modal__row">
                        <label for="cf_comment" class="col-xs-3 control-label modal__label">Отзыв:<span class="form-required">*</span></label>
                        <div class="col-xs-9">
                            <textarea class="form-control form-comment" rows="5" id="cf_comment"></textarea>
                        </div>
                    </div>
                    <input type="hidden" name="id" id="prod_id">
                    <div class="row modal__row">
                        <div class="col-xs-9 col-xs-offset-3 modal__notes"><span class="form-required">*</span> - обязательные поля</div>
                    </div>
                </div>
                <div class="modal-footer modal__footer">
                    <button type="submit" class="btn btn-primary button-common">Оставить&nbsp;отзыв</button>
                    <p class="hurry_notice_info">Нажимая на кнопку «Оставить&nbsp;отзыв», вы соглашаетесь на обработку персональных данных в соответствии с <a class="oferta_link" href="/page/uslovija-obrabotki-personalynih-dannyh" target="_blank">условиями</a>.</p>
                </div>
            </form>
        </div>
    </div>
</div>
