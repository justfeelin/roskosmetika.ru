<?php
/**
 * @var array $content
 */
?>
<table width="600" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <p style="font-family: Tahoma, Geneva, sans-serif; font-size: 13px; color: #1B1B1B; margin-top: 6px; margin-bottom: 6px;">
            <h1 style="font-family: Tahoma, Geneva, sans-serif; font-size: 14px; color: #1B1B1B; margin-top: 10px; margin-bottom: 10px;">
                <?= $content['name'] ?>, здравствуйте!
            </h1>
            <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 6px;">
                Рады сообщить, что заинтересовавший Вас товар появился в наличии и его можно <a
                    href="<?= DOMAIN_FULL ?>/product/<?= $content['url'] ?>?utm_source=site&utm_medium=newsletter&utm_campaign=pre_order_mail&utm_term=pre_order_mail"
                    style="color: #9b1d97;">купить</a> прямо сейчас!</p>
            <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 20px;">
                <strong><?= $content['date'] ?></strong> Вы оставляли заявку на информирование о появлении товара в продаже:</p>
        </td>
    </tr>
    <tr>
        <td>
            <table width="600" border="0" cellspacing="0" cellpadding="0">
                <tr height="28">
                    <td width="500" bgcolor="#8dc655"
                        style="padding-left:10px; font-family: Tahoma, Geneva, sans-serif;  font-size: 13px; color: #FFF; text-align: left; text-decoration: none; vertical-align: middle;">
                        Интересующий Вас товар
                    </td>
                    <td width="100" bgcolor="#8dc655"
                        style="font-family: Tahoma, Geneva, sans-serif; font-size: 13px; color: #FFF; text-align: left; text-decoration: none; vertical-align: middle;">
                        Цена
                    </td>
                </tr>
                <tr>
                    <td width="500" style="padding-left:10px; padding-right:10px">
                        <table width="480" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="85"><p style="margin-top: 12px;margin-bottom: 6px;"><a
                                            href="<?= DOMAIN_FULL ?>/product/<?= $content['url'] ?>?utm_source=site&utm_medium=newsletter&utm_campaign=pre_order_mail&utm_term=pre_order_mail"><img
                                                src="<?= IMAGES_DOMAIN_FULL ?>/images/prod_photo/<?= Product::img_url('min_' . $content['prod_id'] . '.jpg', $content['url']) ?>"
                                                width="85" height="85"/></a></p></td>
                                <td width="335"><p
                                        style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 6px;">
                                        <a href="<?= DOMAIN_FULL ?>/product/<?= $content['url'] ?>?utm_source=site&utm_medium=newsletter&utm_campaign=pre_order_mail&utm_term=pre_order_mail"
                                           style="color: #9b1d97;">
                                            <?= $content['prod_name'] ?></a>, <?= $content['pack'] ?></p>
                                    <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 10px;color: #1B1B1B;margin-top: 6px;margin-bottom: 6px;">
                                        Артикул: <?= $content['prod_id'] ?>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="100">
                        <?php
                        if ($content['percent'] > 0) {
                            $content['percent'] = 100 - $content['percent'];
                        ?>
                            <p style="font-family: Tahoma, Geneva, sans-serif; font-size: 13px; color: #1B1B1B; margin-top: 6px; margin-bottom: 2px;">
                                <s><?= formatPrice($content['price']) ?> р.</s></p>
                            <p style="font-family: Tahoma, Geneva, sans-serif; font-size: 13px; margin-top: 2px; margin-bottom: 6px; color:red;"><?= formatPrice($content['special_price']) ?>
                                р.</p>
                            <p style="font-family: Tahoma, Geneva, sans-serif; font-size: 10px; color: #1B1B1B; margin-top: 2px; margin-bottom: 6px; font-style: italic;">
                                со скидкой <?= $content['percent'] ?>%</p>
                        <?php } else { ?>
                            <p style="font-family: Tahoma, Geneva, sans-serif; font-size: 13px; color: #1B1B1B; margin-top: 6px; margin-bottom: 6px;"><?= formatPrice($content['price']) ?>
                                р.</p>
                        <?php } ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
