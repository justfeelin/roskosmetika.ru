<?php
/**
 * @var string $header
 * @var array $content
 */
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
    <title><?= $header ?></title>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family: Tahoma, Arial, sans-serif">
    <tr>
        <td bgcolor="#C5C5C5">
            <table border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
                <tr bgcolor="#C5C5C5">
                    <td colspan="3" height="32">
                        &nbsp;
                    </td>
                </tr>

                <tr>
                    <td width="20">&nbsp;</td>
                    <td width="600">
                        <table width="600" border="0" cellspacing="0" cellpadding="0" align="center">

                            <tr>
                                <td height="75" style="vertical-align:central;">
                                    <table>
                                        <tr>
                                            <td width="350">
                                                <a href="<?= DOMAIN_FULL . '/' . apply_utm('', 'logo') ?>" target="_blank" border="0"><img
                                                        src="<?= DOMAIN_FULL ?>/images/logo_2018.png" width="222" height="90"
                                                        alt="" border="0"></a>
                                            </td>
                                            <td width="250">
                                                <a href="tel:8-495-662-58-44" style="display: block; text-decoration: none !important; color: #9b1d97;font-family:Tahoma, Geneva, sans-serif; font-size:22px; text-align:right; margin-top:5px;margin-bottom: 0px;">
                                                8 495 662-58-44</a>
                                                <p style="color: #8dc655; font-family:Tahoma, Geneva, sans-serif; font-size:11px; text-align:right; margin-top:0px">
                                                    Москва и МО (9:00-21:00)</p>
                                                <a href="tel:8-800-775-54-83" style="display: block; text-decoration: none !important; color: #9b1d97;font-family:Tahoma, Geneva, sans-serif; font-size:22px; text-align:right; margin-top:5px;margin-bottom: 0px;">
                                                    8 800 775-54-83</a>
                                                <p style="color: #8dc655; font-family:Tahoma, Geneva, sans-serif; font-size:11px; text-align:right; margin-top:0px">
                                                    Бесплатно по РФ (9:00-21:00)</p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <?php
                                    Template::partial($content['template'], $content);
                                    ?>
                                </td>
                            </tr>

                            <tr>
                                <td height="20"></td>
                            </tr>

                            <tr>
                                <td
                                    bgcolor="#8dc655"
                                    height="20"
                                    style="font-family: Tahoma, Geneva, sans-serif;font-size: 12px;  color: #FFF;text-align: center;text-decoration: none;vertical-align: middle;">
                                    <a href="<?= DOMAIN_FULL ?>/category/kosmetika-dlja-lica<?= apply_utm('', 'bottom-seaweed') ?>" style="text-decoration:none; color:#FFF;" target="_blank">Для лица</a>
                                    •
                                    <a href="<?= DOMAIN_FULL ?>/category/kosmetika-dlja-tela<?= apply_utm('', 'bottom-pillings') ?>" style="text-decoration:none;  color:#FFF;" target="_blank">Для тела</a>
                                    •
                                    <a href="<?= DOMAIN_FULL ?>/category/kosmetika-dlja-ruk-i-nog<?= apply_utm('', 'bottom-masks') ?>" style="text-decoration:none; color:#FFF;" target="_blank">Для рук и ног</a>
                                    •
                                    <a href="<?= DOMAIN_FULL ?>/category/sredstva-dlja-dusha-i-vann<?= apply_utm('', 'bottom-chocolate') ?>" style="text-decoration:none;  color:#FFF;" target="_blank">Для душа и ванны</a>
                                    •
                                    <a href="<?= DOMAIN_FULL ?>/category/uhod-za-volosami<?= apply_utm('', 'bottom-chocolate') ?>" style="text-decoration:none;  color:#FFF;" target="_blank">Для волос</a>
                                    •
                                    <a href="<?= DOMAIN_FULL ?>/category/instrumenty-i-raskhodniki<?= apply_utm('', 'bottom-chocolate') ?>" style="text-decoration:none;  color:#FFF;" target="_blank">Инструменты</a>
                                    •
                                    <a href="<?= DOMAIN_FULL ?>/category/oborudovanie<?= apply_utm('', 'bottom-chocolate') ?>" style="text-decoration:none;  color:#FFF;" target="_blank">Оборудование</a>
                                </td>
                            </tr>

                            <tr>
                                <td height="20">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <td width="20">&nbsp;</td>
                </tr>

                <tr bgcolor="#C5C5C5">
                    <td colspan="3">
                        <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 9px;color: #666;text-decoration: none;text-align: right;margin-top: 7px;margin-bottom: 7px;">
                            <a href="<?= DOMAIN_FULL ?>/review<?= apply_utm('', 'review') ?>" target="_blank" style="color: #666">Оставить
                                отзыв</a>
                            • <a
                                href="https://market.yandex.ru/shop/50173/reviews/add?retpath=http%3A%2F%2Fmarket.yandex.ru%2Fshop%2F50173%2Freviews%3Fsuggest%3D1"
                                target="_blank" style="color: #666">Отзыв на Яндекс Маркете</a>
                            <?php if ($content['unsubscribeLink']) { ?>
                                • <a
                                      target="_blank"
                                      style="color: #666"
                                      href="<?= DOMAIN_FULL ?>/unsubscribe/<?= $content['unsubscribeLink']['email'] . '/' . $content['unsubscribeLink']['hash'] ?>">Отписаться от рассылки</a>
                            <?php } ?>
                            <br/>
                            © «Интернет магазин косметики Роскосметика». Все права защищены.</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
