<?php
/**
 * @var array $master_classes
 * @var array $month
 */
$n = count($master_classes);

if ($n === 0) {
    return;
}

$colClass = 12 / $n;
?>
<h3 class="info_title mc-h3">Ближайшие мастер-классы</h3>
<div class="row" id="mc-order-ok">
    <?php for ($i = 0; $i < $n; ++$i) { ?>
        <div class="mc-item col-xs-<?= $colClass ?>">
            <a href="/master_classes#mc-<?= $master_classes[$i]['id'] ?>"><?= $master_classes[$i]['name'] ?></a>
            <p class="under_link"><?= $master_classes[$i]['series'] ?></p>
            <?php if ($master_classes[$i]['img']) { ?>
                <div class="text-с">
                    <img src="/images/mc/<?= $master_classes[$i]['img'] ?>">
                </div>
            <?php } ?>
            <div class="dt"><?= $master_classes[$i]['day'] . '&nbsp;' . $month[$master_classes[$i]['month']] . '&nbsp;' . Template::date('H:i', $master_classes[$i]['date']) ?></div>
            <div class="desc_ok"><?= $master_classes[$i]['short_desc'] ?></div>
            <div class="text-с">
                <button class="btn btn-primary button-common center-block master-class__submits"
                        data-id="<?= $master_classes[$i]['id'] ?>" data-name="<?= h($master_classes[$i]['name']) ?>">
                    Записаться
                </button>
            </div>
        </div>
    <?php } ?>
</div>
