<?php
/**
 * @var string $header
 * @var array $content
 */
if (!empty($crumb)) {
    Template::partial(
        'breadcrumbs',
        [
            'crumbs' => [
                [
                    'name' => $crumb,
                ],
            ],
        ]
    );
}
?>
<div class="row page_sitemap">
    <h1 class="page_h1"><?= $header ?></h1>
    <?php for ($i=0; $i < count($content); $i++) { ?>
        <div class="col-xs-12" temscope itemtype="http://schema.org/WebPage">
            <p>
                <a class="sitemap_lvl_1" href="<?= $content[$i]['url'] ?>" itemprop="url"><span itemprop="name"><?= $content[$i]['name'] ?></span></a>
            </p>

            <?php if (isset($content[$i]['children'])) { ?>
            <div>
                <?php for ($j=0; $j < count($content[$i]['children']); $j++) { ?>
                    <p>
                        <a class="sitemap_lvl_2" href="<?= $content[$i]['children'][$j]['url'] ?>" itemprop="url"><span
                                itemprop="name"><?= $content[$i]['children'][$j]['name'] ?></span></a>
                    </p>
                    <?php if (isset($content[$i]['children'][$j]['children'])) { ?>
                    <div>
                        <?php for ($k=0; $k < count($content[$i]['children'][$j]['children']); $k++) { ?>
                            <p><a class="sitemap_lvl_3" href="<?= $content[$i]['children'][$j]['children'][$k]['url'] ?>" itemprop="url"><span
                                        itemprop="name"><?= $content[$i]['children'][$j]['children'][$k]['name'] ?></span></a></p>
                        <?php } ?>
                    </div>
                    <?php } ?>
                <?php } ?>
            </div>
            <?php } ?>
        </div>
    <?php } ?>
</div>
<?php
Template::partial('mind_box', [
                'mb_is_auth' => $is_auth,
                'mb_top_view' => $mb_top_view,
            ]);
