<?php
/**
 * @var array $content
 */
$displayed = 0;
$n = count($content);
?>
<div class="row tm-menu-container tms-box">
    <ul class="nav nav-pills<?= $n > 10 ? ' nav-justified' : '' ?> tm-menu">
        <?php
        for ($i = 0; $i < $n; ++$i ) {
            if ($content[$i]['tm_menu']) {
                ++$displayed;
        ?>
                <li class="tm-menu__item">
                    <a href="/tm/<?= $content[$i]['url'] ?>" class="tm-menu__item-link"><?= $content[$i]['name'] ?></a>
                </li>
            <?php
            }
        }

        if ($displayed > 7) {
        ?>
            <li class="tm-menu__item tm-menu_all">
                <div>Все бренды<span class="pointer"></span></div>
            </li>
        <?php } ?>
    </ul>
    <?php
    if ($displayed > 7) {
        $cols = floor($n / 6);
    ?>
        <div class="sub_tm_menu">
            <div class="col">
                <?php for ($i = 0; $i < $n; ++$i ) { ?>
                    <a class="tm_menu_title" href="/tm/<?= $content[$i]['url'] ?>"><?= $content[$i]['name'] ?></a>
                    <?php if (!(($i + 1) % $cols)) { ?>
                        </div>
                        <div class="col">
                    <?php
                    }
                }
                ?>
            </div>
            <div class="clearfix"></div>
        </div>
    <?php } ?>
</div>
