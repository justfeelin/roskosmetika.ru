<?php
/**
 * @var array $content
 * @var array $Pagination
 */

$paginationHTML = Template::get_tpl(
    'pagination_wrap',
    [
        'Pagination' => $Pagination,
    ]
);

if ($content['crumbs']) {
    Template::partial('breadcrumbs', [
        'crumbs' => $content['crumbs'],
    ]);
}

$productType = !empty($content['product_type']) ? $content['product_type'] : null;
$productTypeN = !empty($content['product_type_n']) ? (int)$content['product_type_n'] : null;
?>
<div class="row prod-reviews-aggr">
    <h1 class="page_h1"><?= $content['h1'] ?></h1>

    <?php

    echo $paginationHTML;

    if ($content['reviews']) {
        for ($i = 0, $n = count($content['reviews']); $i < $n; ++$i) {
            $imgTitle = $content['reviews'][$i]['alt'] ?: $content['reviews'][$i]['product_name'] . ' от ' . $content['reviews'][$i]['tm'] . ' за ' . roundPrice($content['reviews'][$i]['available'] && (int)$content['reviews'][$i]['special_price'] ? $content['reviews'][$i]['special_price'] : $content['reviews'][$i]['price']) . ' руб';
    ?>
            <div class="row review-box">
                <div class="col-xs-10">
                    <div class="prod_reviewer"><span><?= $content['reviews'][$i]['name'] ?></span>
                        <time class="review_date_city"
                              datetime="<?= Template::date('Y-m-d H:i+04:00', $content['reviews'][$i]['date']) ?>">
                            ,&nbsp;<?= ($content['reviews'][$i]['city'] ? ',&nbsp;' . $content['reviews'][$i]['city'] : '') . '&nbsp;' . Template::date('d.m.Y', $content['reviews'][$i]['date']) ?></time>
                    </div>
                    <div class="question_box">
                        <img
                            class="pull-right"
                            src="<?= IMAGES_DOMAIN_FULL ?>/images/prod_photo/<?= Product::img_url('min_' . $content['reviews'][$i]['product_id'] . '.jpg', $content['reviews'][$i]['product_url']) ?>"
                            width="85"
                            height="85"
                            alt="<?= $imgTitle ?>"
                            title="<?= $imgTitle ?>">
                        <a class="product-name"
                           href="/product/<?= $content['reviews'][$i]['product_url'] ?>"><?= $content['reviews'][$i]['product_name'] ?></a>
                        <?php if ($productType && $Pagination['page'] === 1 && $productTypeN > $i) { ?>
                            <p>
                                <strong>
                                    Тип: <?= $productType ?>
                                </strong>
                            </p>
                        <?php } ?>
                        <div class="comment read_more_desc" data-size="70">
                            &laquo;<?= $content['reviews'][$i]['comment'] ?>&raquo;
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
    <?php
        }
    } else {
    ?>
        <h2>К сожалению, отзывов на данную продукцию пока нет</h2>
    <?php
    }

    echo $paginationHTML;
    ?>
</div>
