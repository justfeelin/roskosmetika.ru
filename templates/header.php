<?php
/**
 * @var string $page
 * @var string $incartlink
 * @var bool $is_auth
 * @var string $main_menu
 */
?>
<div class="header" itemscope itemtype="http://schema.org/WPHeader">
    <div class="row">
        <span class="col-xs-3 header_shipping"></span>

        <div class="col-xs-6 header_menu">
                <a href="/page/contacts">Контакты</a>
                <a href="/page/delivery">Оплата&nbsp;и&nbsp;доставка</a>
                <a href="/page/sales">Система скидок</a>
                <a href="/articles">База знаний</a>
                <a href="/products/hurry" class="sales-link">Распродажа %</a>
        </div>
        <?php
        if (!$is_auth) {
        ?>
            <div class="col-xs-3 auth">
                <div class="reg pull-right"><span>Регистрация</span></div>
                <div class="login pull-right"><span>Вход</span></div>
                <form class="reg_window hidden reg_in_top">
                    <div class="form-group">
                        <p>E-mail</p>
                        <input type="email" class="form-control reg_email has-error">
                    </div>
                    <div class="form-group">
                        <p>Пароль</p>
                        <input type="password" class="form-control reg_password">
                    </div>
                    <button class="reg_btn" type="submit">Зарегистрировать</button>
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <div class="h">Через социальные сети</div>
                            <a href="#" class="auth-btn vk" data-type="vk" title="Регистрация через vk.com"></a>
                            <a href="#" class="auth-btn fb" data-type="fb" title="Регистрация через Facebook"></a>
                            <a href="#" class="auth-btn ok" data-type="ok" title="Регистрация через Одноклассники"></a>
                        </div>
                    </div>
                </form>
                <form class="login_window hidden login_in_top">
                    <div class="form-group">
                        <p>E-mail или телефон</p>
                        <input type="text" class="form-control login_user has-error">
                    </div>
                    <div class="form-group">
                        <p>Пароль</p>
                        <input type="password" class="form-control login_password">
                    </div>
                    <div class="row rem_and_back">
                        <div class="remember col-xs-6">Забыли&nbsp;пароль?&nbsp;-&nbsp;легко</div>
                        <p class="col-xs-6 backup_psw">Восстановить&nbsp;пароль</p>
                    </div>
                    <button class="log_in_btn">Войти</button>
                    <div class="login_win_bottom">
                        <div>Еще&nbsp;не&nbsp;зарегистрированы?</div>
                        <div>Пройти&nbsp;<span class="fast_reg">Быструю&nbsp;регистрацию</span></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <div class="h">Вход через социальные сети</div>
                            <a href="#" class="auth-btn vk" data-type="vk" title="Авторизация через vk.com"></a>
                            <a href="#" class="auth-btn fb" data-type="fb" title="Авторизация через Facebook"></a>
                            <a href="#" class="auth-btn ok" data-type="ok" title="Авторизация через Одноклассники"></a>
                        </div>
                    </div>
                </form>
            </div>
        <?php } else { ?>
            <div class="col-xs-3 header_room">
                <a <?= $incartlink ?> href="/room"><span class="account_icon"></span>Личный&nbsp;кабинет</a>
            </div>
        <?php } ?>
    </div>
    <div class="menu-static">
        <div class="menu-container row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-3 logo">
                        <a <?= $incartlink ?> href="/">
                            <img src="/images/logo_2018.png" alt="Роскосметика - интернет-магазин натуральной косметики">
                        </a>
                        <!-- <span class="ny_pig"></span> -->
                    </div>
                    <div class="col-xs-6 search_block">
                        <form id="search-form" class="search" method="GET" action="/search" autocomplete="off">
                            <input class="form-control search-input" type="text" name="search" value="<?= !empty($search) ? h($search) : '' ?>" placeholder="Поиск по сайту" autocomplete="off" id="search-input">
                            <button class="btn" type="submit"><span></span></button>
                            <div id="search-suggestions"></div>
                        </form>
                        <span id="top-cart" class="cart-info-badge">
                        <a class="hidden_link" href="#" rel="nofollow" data-link="<?= Template::hidelink('/cart') ?>" title="Перейти в корзину для оформления заказа">
                            <p class="header_cart">
                                <span class="header_price" ng-bind="cart.info.quantity"></span>
                            </p>
                            <div class="cart-info ng-init" ng-show="cart.info.quantity > 0">
                                <strong>Корзина</strong>
                                <div>
                                    <span ng-bind="(cart.info.final_sum - (cart.products.length === 1 && cart.products[0].id === <?= Product::CERTIFICATE_ID ?> && !cart.info.deliverCertificate ? cart.info.shipping_price : 0))|price"></span>
                                    <span class="rub_h">руб</span>
                                    <span class="rub">p</span>
                                </div>
                            </div>
                        </a>
                        </span>
                        <?php Template::partial('cart_popup') ?>
                    </div>
                    <div class="col-xs-3 phone top-phone">
                        <a href="tel:+7(495)662-58-44" class="phone_number">8&nbsp;495&nbsp;662-58-44</a>

                        <p class="under_number">
                            Москва и МО
                        </p>

                        <a href="tel:8-800-775-54-83" class="phone_number">8&nbsp;800&nbsp;775-54-83</a>

                        <p class="under_number">
                            Бесплатно&nbsp;по&nbsp;РФ
                        </p>
                    </div>
                </div>
                <div class="row">
                    <?= $main_menu ?>
                    <div class="menu_d_line">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
