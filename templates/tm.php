<?php
/**
 * @var array $content
 * @var array $Pagination
 * @var string $found
 * @var bool $noindex_description
 */
Template::partial(
    'breadcrumbs',
    [
        'crumbs' => $content['crumbs'],
    ]
);

$tmImage = $content['tm']['src'] !== 'none.jpg';

if ($content['products']) {
?>
    <div class="row">
        <div class= "col-xs-12">
            <?php
            Template::partial(
                'product_filter',
                [
                    'filters' => $content['filters'],
                    'name' => ($content['line'] ? '' : 'Косметика ') . $content['tm']['name'],
                    'subname' => $content['tm']['country'],
                    'found' => $found,
                    'schema_scope' => 'Brand',
                    'schema_name' => 'name',
                    'categories_html' => $content['categories_html'],
                    'content' => $content,
                    'img' => $tmImage ? '/images/tm_photo/' . $content['tm']['src'] : null,
                    'imgAlt' => $tmImage ? $content['tm']['alt'] : null,
                    'imgTitle' => $tmImage ? $content['tm']['photo_title'] : null,
                ]
            );

            $paginationHTML = Template::get_tpl('pagination_wrap', null, [
                'Pagination' => !empty($Pagination) ? $Pagination : null,
            ]);
            ?>
            <div class="row">
                <div class="col-xs-12">
                    <?= $paginationHTML ?>
                </div>
            </div>
            <div class="products ua-product-list product-list" data-list="<?= $content['line'] ? 'Линейка' : 'Бренд' ?> «<?= $content['tm']['name'] ?>»">
                <?php
                Template::partial(
                    'product_list',
                    [
                        'products' => $content['products'],
                        'noindex_description' => $noindex_description,
                    ]
                );
                ?>
            </div>
            <?php
            echo
                $paginationHTML;

            if (!empty($content['popular_products'])) {
            ?>
                <h3>Популярные товары</h3>
            <?php
                Template::partial(
                    'product_list',
                    [
                        'products' => $content['popular_products'],
                    ]
                );
            }

            if (!empty($content['reviews'])) {
            ?>
                <a class="info_title_link" href="/otzyvy/tm/<?= $content['tm']['url'] ?>">Отзывы о косметике <?= $content['tm']['name'] ?></a>
                <div class="row">
                    <?php
                    Template::partial(
                        'product_reviews',
                        [
                            'reviews' => $content['reviews'],
                        ]
                    );
                    ?>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="row prods_title_sort" itemscope itemtype=<?= !empty($content['cur_cat']['id']) ? 'http://schema.org/WebPage' : 'http://schema.org/Brand' ?>>
            <div class="col-xs-12 tm_block listing_desc">
                <?php if ($Pagination['page'] == 1 && $content['tm']['desc']) { ?>
                <div itemprop="description" data-size="145">
                    <?= $content['tm']['desc'] ?>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } else { ?>
    <h1 class="no_items">Нет продуктов.</h1>
<?php
}

Template::partial(
    'base_links',
    [
        'links' => $content['base_links'],
    ]
);
