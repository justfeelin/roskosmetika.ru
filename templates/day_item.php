<?php
/**
 * @var array $content
 */
?>
<div class="col-xs-4 day_item" itemscope itemtype="http://schema.org/Product">
    <div class="info_title">Товар&nbsp;дня</div>
    <div class="row">
        <div class="col-xs-7 day_item_img">
            <a class="hidden_link" href="#" rel="nofollow"
               data-link="<?= Template::hidelink('/product/' . $content['url']) ?>"><img
                    itemprop="image" src="<?= IMAGES_DOMAIN_FULL ?>/images/prod_photo/<?= Product::img_url('s_' . $content['src'], $content['url']) ?>"
                    width="185" height="185" alt="<?= $content['alt'] ?: $content['name'] ?>"></a>
        </div>
        <div class="col-xs-1 tag_pointer"></div>
        <div class="col-xs-3 tag_label"><span><?= $content['sale'] ? '-' . $content['sale'] . '%' : '' ?></span> <?= $content['tag'] ?></div>
    </div>
    <span itemprop="brand" itemscope itemtype="http://schema.org/Brand">
        <a class="hidden_link under_link" itemprop="url" href="#" rel="nofollow"
           data-link="<?= Template::hidelink('/tm/' . $content['tm_url']) ?>"><span
                itemprop="name"><?= $content['tm'] ?></span></a>
    </span>

    <p class="prod_link_name"><a href="/product/<?= $content['url'] ?>" class="link" name="day_item"
                                 itemprop="name"><?= $content['name'] ?></a></p>

    <div class="desc" itemprop="description"><?= $content['short_description'] ?></div>
    <div class="row cart_info" itemprop="offers" itemscope itemtype="http://schema.org/AggregateOffer">
        <div class="col-xs-3 pack">
            <div><?= $content['pack'] ?></div>
            <?php if (!$content['available']) { ?>
                <link itemprop="availability" href="http://schema.org/OutOfStock">
                <div class="row">
                    <div class="col-xs-9 pre_cart_title">Ожидается<span class="pre_cart_icon">i</span></div>
                </div>
            <?php } else { ?>
                <link itemprop="availability" href="http://schema.org/InStock">
            <?php } ?>
        </div>
        <?php if ($content['available'] && (int)$content['special_price']) { ?>
            <div class="col-xs-3 price spec_price">
                <div class="old_price" itemprop="highPrice">
                    <?= formatPrice($content['price']) ?>
                    <span class="rub_h">руб</span>
                    <span class="rub">p</span>
                </div>
                <div class="new_price" itemprop="lowPrice">
                    <?= formatPrice($content['special_price']) ?>
                    <span class="rub_h">руб</span>
                    <span class="rub">p</span>
                </div>
            </div>
        <?php } else { ?>
            <div class="col-xs-3 price">
                <div>
                    <?= formatPrice($content['price']) ?>
                    <span class="rub_h">руб</span>
                    <span class="rub">p</span>
                </div>
            </div>
        <?php } ?>
        <meta itemprop="priceCurrency" content="RUB"/>
        <?php if (!$content['available']) { ?>
            <p class="col-xs-4 waiting" data-id="<?= $content['id'] ?>"
               data-name="<?= $content['name'] . ' ' . $content['pack'] ?>">
                Предзаказ</p>
        <?php } else { ?>
            <p class="col-xs-4 to_cart" ng-click="cartProduct(<?= $content['id'] ?>, 1, $event)">Купить</p>
        <?php } ?>
    </div>
    <div id="st-anchor" class="row action_end">
        <div class="end_cd hidden"><?= $content['end'] ?></div>
        <div class="col-xs-3 action_end_tag">До конца акции</div>
        <div class="col-xs-2 end_time cd_hour"></div>
        <div class="col-xs-1 two_spot"></div>
        <div class="col-xs-2 end_time cd_min"></div>
        <div class="col-xs-1 two_spot"></div>
        <div class="col-xs-2 end_time cd_sec"></div>
    </div>
</div>
