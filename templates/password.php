<?php
/**
 * @var string $header
 * @var int $active_aside
 * @var bool $change_ok
 * @var array $msg
 * @var array $values
 */
?>
<section class="room">
    <div class="row">
        <h1 class="col-xs-6 col-xs-offset-3"><?= $header ?></h1>
    </div>
    <div class="row">
        <?php
        Template::partial(
            'room_left_menu',
            [
                'active_aside' => $active_aside,
            ]);
        ?>
        <div class="col-xs-6">
            <?php if (isset($change_ok)) { ?>
                <div class="no_items">Ваш&nbsp;пароль&nbsp;успешно&nbsp;изменен.</div>
            <?php } else { ?>
                <form action="/room/password" method="POST">
                    <div class="form-group row <?= isset($msg['password1']) ? 'has-error' : '' ?>">
                        <p class="col-xs-4">Новый&nbsp;пароль:</p>

                        <div class="col-xs-8">
                            <input type="password" name="password1"
                                   value="<?= isset($values['password1']) ? $values['password1'] : '' ?>" class="form-control">
                            <?= isset($msg['password1']) ? '<p class="form_error">' . $msg['password1'] . '</p>' : '' ?>
                        </div>
                    </div>

                    <div class="form-group row <?= isset($msg['password2']) ? 'has-error' : '' ?>">
                        <p class="col-xs-4">Пароль&nbsp;еще&nbsp;раз:</p>

                        <div class="col-xs-8">
                            <input type="password" name="password2"
                                   value="<?= isset($values['password2']) ? $values['password2'] : ''?>" class="form-control">
                            <?= isset($msg['password2']) ? '<p class="form_error">' . $msg['password2'] . '</p>' : '' ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4"></div>
                        <button type="submit" name="submit" class="col-xs-8 form_save">Сохранить</button>
                    </div>
                </form>
            <?php } ?>
        </div>
        <div class="col-xs-3"></div>
    </div>
</section>
