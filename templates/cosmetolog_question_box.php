<div class="modal fade" id="question-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="true">
    <div class="modal-dialog modal__frame">
        <div class="modal-content modal__content">
            <form class="form-horizontal" role="form" id="question-form">
                <div class="modal-header modal__header">
                    <button type="button" class="close modal__close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title modal__title" id="myModalLabel">
                        Вопрос косметологу
                    </h4>
                </div>
                <div class="modal-body modal__body">
                    <div class="form-group modal__row">
                        <label for="qf_name" class="col-xs-3 control-label modal__label">ФИО:</label>
                        <div class="col-xs-9">
                            <input type="text" class="form-control l-name form-name" id="qf_name">
                        </div>
                    </div>
                    <div class="form-group modal__row">
                        <label for="qf_email" class="col-xs-3 control-label modal__label">Email:<span class="form-required">*</span></label>
                        <div class="col-xs-9">
                            <input type="email" class="form-control l-email form-email" id="qf_email">
                        </div>
                    </div>
                    <div class="form-group modal__row">
                        <label for="qf_question" class="col-xs-3 control-label modal__label">Вопрос:<span class="form-required">*</span></label>
                        <div class="col-xs-9">
                            <textarea class="form-control form-question" rows="5" id="qf_question"></textarea>
                        </div>
                    </div>
                    <div class="row modal__row">
                        <div class="col-xs-9 col-xs-offset-3 modal__notes"><span class="form-required">*</span> - обязательные поля</div>
                    </div>
                </div>
                <div class="modal-footer modal__footer">
                    <button type="submit" class="btn btn-primary button-common">Задать вопрос</button>
                    <p class="hurry_notice_info">Нажимая на кнопку «Задать вопрос», вы соглашаетесь на обработку персональных данных в соответствии с <a class="oferta_link" href="/page/uslovija-obrabotki-personalynih-dannyh" target="_blank">условиями</a>.</p>
                </div>
            </form>
        </div>
    </div>
</div>
