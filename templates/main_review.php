<?php
/**
 * @var array $review
 */
?>
<div class="review col-xs-6">
    <div class="rew_sub_area">
        <div class="rew_sub_title">Отзывы&nbsp;о&nbsp;нашем&nbsp;магазине</div>
        <div class="review_text">
            &laquo;<?= $review['comment'] ?>&raquo;
        </div>
        <div class="reviewer"><?= $review['name'] . '&nbsp;' . $review['surname'] ?>
            &nbsp;<?= $review['city']
                ? 'город&nbsp;' . $review['city']
                : (
                    $review['region_id'] && $review['region_name']
                        ? $review['region_name']
                        : ''
                ) ?></div>
        <div class="see_all">
            <a href="/review">Все&nbsp;отзывы<span class="pointer_see_all"></span></a>
        </div>
    </div>
</div>
