<?php
/**
 * @var array $content
 */
if ($content['tags']) {
    $tags_max_length = 100;
    $tags_length = 0;
?>
    <div class="row tm-menu-container cat-tm cat-tags tms-box">
        <div class="col-xs-12">
            <h3 class="lvl_3_group_title">Вас может заинтересовать</h3>
            <ul class="nav nav-pills<?= count($content['tags']) > 7 ? ' nav-justified' : '' ?> tm-menu">
                <?php
                for ($i = 0, $n = count($content['tags']); $i < $n; ++$i) {
                    $tags_length += mb_strlen($content['tags'][$i]['name']);

                    if ($tags_length < $tags_max_length) {
                ?>
                        <li class="tm-menu__item">
                            <a href="/category/<?= $content['tags'][$i]['_full_url'] ?>" class="tm-menu__item-link"><?= $content['tags'][$i]['name'] ?></a>
                        </li>
                <?php
                    } else {
                        break;
                    }
                }

                if ($tags_length >= $tags_max_length) {
                ?>
                    <li class="tm-menu__item tm-menu_all">
                        <div>Показать все<span class="pointer"></span></div>
                    </li>
                <?php } ?>
            </ul>
            <?php if ($tags_length >= $tags_max_length) { ?>
                <div class="sub_tm_menu">
                    <?php for (; $i < $n; ++$i) { ?>
                        <a class="tm_menu_title" href="/category/<?= $content['tags'][$i]['_full_url'] ?>"><?= $content['tags'][$i]['name'] ?></a>
                    <?php } ?>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
        </div>
    </div>
<?php
}
