<div class="modal fade" id="recall-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="true">
    <div class="modal-dialog modal__frame">
        <div class="modal-content modal__content">
            <form class="form-horizontal" role="form" id="recall-form">
                <div class="modal-header modal__header">
                    <button type="button" class="close modal__close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <p class="modal-title modal__title" id="myModalLabel">
                        Заказать обратный звонок
                    </p>
                </div>
                <div class="modal-body modal__body">
                    <div class="form-group modal__row">
                        <label for="recf_name" class="col-xs-4 control-label modal__label">Как Вас зовут? -<span class="form-required">*</span></label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control l-name form-name" id="recf_name">
                        </div>
                    </div>
                    <div class="form-group modal__row">
                        <label for="recf_phone" class="col-xs-4 control-label modal__label">Номер телефона:<span class="form-required">*</span></label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control l-phone form-phone" id="recf_phone">
                        </div>
                    </div>
                    <div class="row modal__row">
                        <div class="col-xs-12  modal__notes">Мы постараемся перезвонить Вам с 9:30 до 18:30 (пн-пт).<br><span class="form-required">*</span> - обязательные поля</div>
                    </div>
                </div>
                <div class="modal-footer modal__footer">
                    <button type="submit" class="btn btn-primary button-common">Заказать звонок</button>
                    <p class="hurry_notice_info">Нажимая на кнопку «Заказать звонок», вы соглашаетесь на обработку персональных данных в соответствии с <a class="oferta_link" href="/page/uslovija-obrabotki-personalynih-dannyh" target="_blank">условиями</a>.</p>
                </div>
            </form>
        </div>
    </div>
</div>
