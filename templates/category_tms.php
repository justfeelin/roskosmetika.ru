<?php
/**
 * @var bool $small
 * @var array $tms
 * @var string $category_url
 */
?>
<div class="<?= empty($small) ? 'row tm-menu-container cat-tm' : 'all-tms' ?> tms-box">
    <?php
    if (empty($small)) {
        $tm_length = 0;

        $n = count($tms);
    ?>
        <div class="col-xs-12">
            <h3 class="lvl_3_group_title">Торговые марки</h3>
            <ul class="nav nav-pills<?= $n > 7 ? ' nav-justified' : '' ?> tm-menu">
                <?php
                for ($i = 0; $i < $n; ++$i) {
                    $tm_length += mb_strlen($tms[$i]['name']);

                    if ($tm_length < 115) {
                ?>
                        <li class="tm-menu__item">
                            <a href="/category-tm/<?= $category_url . '/' . $tms[$i]['url'] ?>"
                               class="tm-menu__item-link"
                            ><?= $tms[$i]['name'] ?></a>
                        </li>
                <?php
                    } else {
                        break;
                    }
                }

                if ($tm_length >= 115) {
                ?>
                <li class="tm-menu__item tm-menu_all">
                    <div>Все бренды категории<span class="pointer"></span></div>
                </li>
                <?php } ?>
            </ul>
        </div>
    <?php } else { ?>
        <div class="tm-menu_all">
            <div class="lvl_3_group_title">Все бренды категории</div>
        </div>
    <?php
    }

    if (!empty($small) || $tm_length >= 115) {
    ?>
        <div class="sub_tm_menu">
            <?php for ($i = 0, $n = count($tms); $i < $n; ++$i) { ?>
                <a class="tm_menu_title" href="/category-tm/<?= $category_url . '/' . $tms[$i]['url'] ?>"><?= $tms[$i]['name'] ?></a>
            <?php } ?>
            <div class="clearfix"></div>
        </div>
    <?php } ?>
</div>
