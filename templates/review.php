<?php
/**
 * @var string $text
 * @var bool $auth
 * @var array $msg
 * @var array $values
 * @var array $all_comments
 * @var int $qantity_pages
 * @var int $current_page
 */

if (!empty($crumb)) {
    Template::partial(
        'breadcrumbs',
        [
            'crumbs' => [
                [
                    'name' => $crumb,
                ],
            ],
        ]
    );
}

?>
<div class="row row-container">
    <h1 class="page_h1">Отзывы&nbsp;наших&nbsp;клиентов</h1>
    <?php if (isset($text)) { ?>
        <div class="col-xs-12">
            <div class="no_items"><?= $text ?></div>
        </div>
    <?php } else { ?>
        <div class="review_desc">
            <p>Уважаемые покупатели, участвуйте в жизни нашего интернет-магазина!</p>

            <p>Мы будем признательны Вам за все отзывы и пожелания по качеству предлагаемой продукции, ассортименту,
                работе клиентского отдела и службы доставки. Помогите нам узнать Ваше мнение, чтобы стать ближе к
                Вам.</p>

            <p>Коллектив магазина "Роскосметика"</p>
        </div>
    <?php if ($auth) { ?>
        <form action="/review" method="POST" class="review_form">
            <div class="form-group <?= isset($msg['comment']) ? 'has-error' : ''?>">
                <p>Ваш отзыв</p>
                        <textarea name="comment" class="form-control rewiew_textarea"
                                  rows="8"><?= isset($values['comment']) ? $values['comment'] : '' ?></textarea>
                <?= isset($msg['comment']) ? '<p class="form_error">' . $msg['comment'] . '</p>' : '' ?>
            </div>
            <div class="row">
                <button type="submit" name="submit" class="form_save">Опубликовать</button>
            </div>
        </form>
    <?php } else { ?>
        <br>
        <p class="shop_review_auth">Чтобы оставить отзыв, необходимо зарегистрироваться и авторизоваться.</p>
    <?php } ?>
    <span itemscope itemtype="http://schema.org/Organization">
        <meta itemprop="name" content="Интерент-магазин Роскосметика"/>
        <meta itemprop="address" content="Москва ул. Василия Петушкова д.8"/>
        <meta itemprop="telephone" content="8-800-775-54-83"/>
        <?php for ($i = 0, $n = count($all_comments); $i < $n; ++$i) { ?>
            <span itemprop="review" itemscope itemtype="http://schema.org/Review">
                <div class="row <?= !$all_comments[$i]['answer'] ? 'rev_comment' : '' ?>">
                    <div class="col-xs-8">
                        <meta itemprop="itemReviewed" content="Отзыв о работе интернет-магазина Роскосметика"/>
                        <div class="prod_reviewer"><span itemprop="author"><?= $all_comments[$i]['surname'] . '&nbsp;' . $all_comments[$i]['name'] ?></span>
                            <time itemprop="datePublished" class="review_date_city"
                                  datetime="<?= Template::date('Y-m-d H:i+04:00', $all_comments[$i]['time_message']) ?>">
                                ,&nbsp;<?= $all_comments[$i]['city'] ? $all_comments[$i]['city'] : ($all_comments[$i]['region_name'] ? $all_comments[$i]['region_name'] : '') ?>
                                &nbsp;<?= Template::date('d.m.Y', $all_comments[$i]['time_message']) ?></time>
                        </div>
                        <div class="question_box" itemprop="reviewBody"><?= $all_comments[$i]['comment'] ?></div>
                    </div>
                    <span class="col-xs-4"></span>
                </div>
                <?php if ($all_comments[$i]['answer']) { ?>
                    <div class="row answer" itemprop="comment" itemscope itemtype="http://schema.org/UserComments">
                        <span class="col-xs-4"></span>

                        <div class="col-xs-8">
                            <div class="prod_reviewer align_right"><span itemprop="creator">Интернет-магазин&nbsp;Роскосметика</span>,&nbsp;<span
                                    class="review_date_city"
                                    itemprop="commentTime"><?= Template::date('d.m.Y', $all_comments[$i]['answer_time']) ?></span>
                            </div>
                            <div class="answer_box" itemprop="commentText"><?= $all_comments[$i]['answer'] ?></div>
                        </div>
                    </div>
                <?php } ?>
            </span>
        <?php } ?>
    </span>
    <div class="pages row align_center">
        <?php
        if ($qantity_pages !== 1) {
            for ($i = 1, $n = $qantity_pages + 1; $i < $n; ++$i) {
                if ($i === $current_page) {
        ?>
                    <b class="review_active"><?= $i ?></b>
                <?php } else { ?>
                    <a class="link"
                       href="/review<?= $i > 1 ? '/' . $i : '' ?>"><?= $i ?></a>

        <?php
                }
            }
        }
        ?>
    </div>
    <?php } ?>
</div>
<?php
Template::partial('mind_box', [
            'mb_is_auth' => $is_auth,
            'mb_top_view' => $mb_top_view,
        ]);