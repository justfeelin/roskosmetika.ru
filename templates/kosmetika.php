<?php
/**
 * @var array $content
 */
Template::partial(
    'breadcrumbs',
    [
        'crumbs' => [
            [
                'name' => 'Косметика',
            ],
        ],
    ]
);
?>
<div id="kosmetika" class="row">
    <h1>Косметика</h1>

    <div class="row">
        <?php for ($i = 0, $n = count($content['type_pages']); $i < $n; ++$i) { ?>
            <div class="col-xs-3 kosmetika-item">
                <div class="box">
                    <?php if ($content['type_pages'][$i]['image']) { ?>
                        <a href="/kosmetika/<?= $content['type_pages'][$i]['url'] ?>">
                            <img src="/images/kosmetika/<?= $content['type_pages'][$i]['image'] ?>" width="50" height="50">
                        </a>
                    <?php } ?>
                    <a href="/kosmetika/<?= $content['type_pages'][$i]['url'] ?>" class="link">
                        <?= $content['type_pages'][$i]['name'] ?>
                    </a>
                    <div class="clearfix"></div>
                </div>
            </div>
        <?php
            if (!(($i + 1) % 4)) {
        ?>
            </div>
            <div class="row">
        <?php
            }
        }
        ?>
    </div>
</div>
