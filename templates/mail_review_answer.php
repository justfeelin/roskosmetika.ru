<?php
/**
 * @var array $content
 */
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <h1 style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 10px;margin-bottom: 10px;">
                Роскосметика благодарит Вас за участие в жизни нашего интернет-магазина.</h1>

            <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 12px;">
                Ваш отзыв от <b><?= Template::date('d.m.Y', $content['time_message']) ?></b>:<br><span
                    style="font-style: italic;">"<?= $content['comment'] ?>"</span><br>
                не остался без внимания:<br><span style="font-style: italic;">"<?= $content['answer'] ?>
                    "</span>
            </p>

            <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 12px;">
                <strong>C уважением, Ваша Роскосметика.</strong></p>
        </td>
    </tr>
</table>
