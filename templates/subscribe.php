<?php
/**
 * @var string $page
 */
?>
<div class="subscribe col-xs-6">
    <div class="row sub_backgroung">
        <div class="col-xs-11">
            <div class="rew_sub_area">
                <div class="rew_sub_title">Подпишитесь на рассылку</div>
                <div class="sub_desc">И получите доступ к&nbsp;
                    <?php if ($page !== 'index') { ?>
                        <a class="hidden_link" href="#" rel="nofollow" data-link="<?= Template::hidelink('/products/sales') ?>">специальным
                        предложениям</a>
                        ,&nbsp;
                        <a class="hidden_link" href="#" rel="nofollow" data-link="<?= Template::hidelink('/products/new') ?>">обзорам
                        новинок</a>
                        ,&nbsp;
                        <a class="hidden_link" href="#" rel="nofollow" data-link="<?= Template::hidelink('/master_classes') ?>">мастер-классам</a>
                        ,&nbsp;
                        <a class="hidden_link" href="#" rel="nofollow" data-link="<?= Template::hidelink('/question') ?>">советам
                        косметолога</a>
                        .
                    <?php } else { ?>
                        <a href="/products/sales">специальным предложениям</a>
                        ,&nbsp;
                        <a href="/products/new">обзорам новинок</a>
                        ,&nbsp;
                        <a href="/master_classes">мастер-классам</a>
                        ,&nbsp;
                        <a href="/question">советам косметолога</a>
                        .
                    <?php } ?>
                </div>
                <form class="row sub_form">
                    <input class="form-control col-xs-7 sub_mail" type="email" placeholder="Ваша электронная почта">
                    <button class="col-xs-3 subscribe_button">Подписаться</button>
                </form>
            </div>
        </div>
    </div>
</div>
