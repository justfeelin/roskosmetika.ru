<?php
/**
 * @var array $content
 * @var array $Pagination
 */
Template::partial(
    'breadcrumbs',
    [
        'crumbs' => $content['crumbs'],
    ]
);

echo
    $content['filters_html'];

if ($content['products']) {
    $paginationHTML = Template::get_tpl('pagination_wrap', null, [
        'Pagination' => !empty($Pagination) ? $Pagination : null,
    ]);

    if ($content['main']['name_on_products'] > 0) {
        $hasH1 = !!$content['main']['h1'];

        $hasAltNumber = !!($hasH1 ? $content['main']['h1_alt_number'] : $content['main']['name_alt_number']);

        $categoryName = $hasAltNumber
            ? helpers::numeric_name(
                $hasH1 ? $content['main']['h1'] : $content['main']['name'],
                $hasH1 ? $content['main']['h1_alt_number'] : $content['main']['name_alt_number'],
                $hasH1 ? $content['main']['h1_plural'] : $content['main']['name_plural'],
                false
            )
            : (
                $hasH1 ? $content['main']['h1'] : $content['main']['name']
            );
    } else {
        $categoryName = null;
    }
?>
    <div class="row">
        <div class="col-xs-12">
            <?= $paginationHTML ?>
        </div>
    </div>
    <div class="col-xs-12 products">
        <div class="ua-product-list product-list" data-list="<?= isset($content['crumbs_text']) ? 'Категория «' . $content['crumbs_text'] . '»' : '' ?>">
            <?php Template::partial(
                'product_list',
                [
                    'products' => $content['products'],
                    'product_type' => $categoryName,
                    'product_type_n' => $content['main']['name_on_products'],
                ]
            ); ?>
        </div>
        <?php
        echo
            $paginationHTML;

        if (!empty($content['popular_products'])) {
        ?>
            <h3>Популярные товары</h3>
        <?php
            Template::partial(
                'product_list',
                [
                    'products' => $content['popular_products'],
                    'no_description' => true,
                ]
            );
        }
    
        if ($content['tms']) {
        ?>
            <div class="row bottom-tms tms">
                <div class="col-xs-12">
                    <?php Template::partial(
                        'category_tms',
                        [
                            'tms' => $content['tms'],
                            'category_url' => $content['main']['_full_url'],
                            'small' => true,
                        ]
                    ); ?>
                </div>
            </div>
        <?php
        }

        echo
            $content['tags_html'];
    
        if ($content['reviews']) {
        ?>
            <a class="info_title_link" href="/otzyvy/category/<?= $content['main']['_full_url'] ?>">Отзывы <?= $content['reviews_title'] ?></a>
            <div class="row">
                <?php Template::partial(
                    'product_reviews',
                    [
                        'reviews' => $content['reviews'],
                    ]
                ); ?>
            </div>
        <?php } ?>
    </div>
    <?php if ($content['main'][$content['main']['level'] == 1 ? 'id' : ($content['main']['level'] == 2 ? 'parent_1' : 'parent_2')] == Categories::PARFUMERY_ID) { ?>
        <div class="clearfix"></div>
        <noindex>
            <div id="prepay-only">*Товары из раздела &laquo;Парфюмерия&raquo; доступны только по предоплате</div>
        </noindex>
<?php
    }
} else {
?>
    <div class="no_items">К сожалению, в данный момент товары в этой категории не представлены<?= !empty($content['similar']) ? ',<br>но Вы можете ознакомиться с похожими разделами:' : '.' ?></div>

    <?php
    if (!empty($content['similar'])) {
        Template::partial(
            'category_list',
            [
                'categories' => $content['similar'],
                'header' => '',
            ]
        );
    }

    echo
        $content['tags_html'];

    Template::personalized_products();
}
?>

<div class="row prods_title_sort" itemscope itemtype="http://schema.org/WebPage">
    <div class="col-xs-12 listing_desc">
        <?php if ($Pagination['page'] == 1 && $content['main']['desc']) { ?>
            <div itemprop="description" data-size="145">
                <?= $content['main']['desc'] ?>
            </div>
        <?php } ?>
    </div>
</div>

<?php
Template::partial(
    'base_links',
    [
        'links' => $content['base_links'],
    ]
);
