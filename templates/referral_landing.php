<meta property="og:image" content="<?= DOMAIN_FULL ?>/images/referral/banner-hello.jpg"/>
<div id="referral-invited">
    <img src="<?= DOMAIN_FULL ?>/images/referral/banner-hello.jpg" class="hide-image">
    <div class="banner-box">
        <div class="bg-image"></div>

        <h1>Скидка 20% у вас<br>в кармане</h1>

        <p>
            Ваш друг поделился с вами скидкой.
            <br>
            Теперь добавьте понравившийся товар в корзину &mdash;
            <br>
            скидка рассчитается автоматически.
            <br>
            Приятных покупок!
        </p>

        <a href="/" class="go-link">Начать шопинг-тур</a>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <hr>
            <p>
                Скидки не суммируются. Действует правило большей скидки.
            </p>
        </div>
    </div>
</div>
