<?php
/**
 * @var array $order
 * @var string $formID
 * @var string $label
 */
if (!isset($label)) {
    $label = 'Оплатить';
}
?>
<form method="GET" action="/payment/sberbank/<?= $order['site_order_number'] ?>" target="_blank" class="payment-form">
    <input type="submit" value="<?= $label ?>" class="submit-pay">
</form>
