<?php
/**
 * @var array $content
 * @var string $found
 */
?>
<div itemscope itemtype="http://schema.org/WebPage">
    <div class="row">
        <div class="col-xs-7 crumbs">
            <div class="crumb_lvl_1_only"><a itemprop="breadcrumb" href="/sets">Все&nbsp;наборы</a></div>
        </div>
        <div class="col-xs-5 cat_art_link"></div>
    </div>
    <?php
    if ($content['products']) {
        Template::partial(
            'product_filter',
            [
                'filters' => $content['filters'],
                'name' => $content['set']['name'],
                'found' => $content['found'],
                'content' => $content,
            ]
        );
        ?>
        <div class="col-xs-12 products">
            <div class="ua-product-list product-list" data-list="<?= $content['set']['name'] ?>">
                <?php
                Template::partial(
                    'product_list',
                    [
                        'products' => $content['products'],
                    ]
                );
                ?>
            </div>
        </div>
        <div class="row prods_title_sort">
            <div class="col-xs-<?= $content['set']['src'] && $content['set']['id'] >= 68 ? '8' : '12' ?>">
                <?php if ($content['set']['desc']) { ?>
                    <div itemprop="description"><?= $content['set']['desc'] ?></div>
                <?php } ?>
            </div>
            <?php if ($content['set']['src'] && $content['set']['id'] >= 68) { ?>
                <div class="col-xs-4">
                    <img src="/images/sets_photo/<?= $content['set']['src'] ?>" width="300" height="200"
                         alt="<?= $content['set']['name'] ?>">
                </div>
            <?php } ?>
        </div>
    <?php } else { ?>
        <h1 class="no_items">В наборе нет продуктов.</h1>
    <?php } ?>
</div>
