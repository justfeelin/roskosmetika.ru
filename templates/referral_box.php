<?php
/**
 * @var int $referral_discount
 * @var string $url
 * @var string $code
 */
?>
<div class="box">
    <a href="mailto:?subject=Скидка 20% на всю продукцию на Роскосметике!&body=Привет! На www.roskosmetika.ru можно получить скидку 20% на весь заказ! Просто перейди по ссылке и покупай со скидкой <?= $url ?>!" class="mail-share" title="Отправить по электронной почте"></a>

    <div class="social-buttons yashare-auto-init" data-yashareType="none"
         data-yashareTitle="Только для моих друзей скидка <?= $referral_discount ?>% на любой заказ в интернет-магазине Роскосметика.ру!"
         data-yashareLink="<?= $url ?>"
         data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,gplus,linkedin"
         data-image="<?= DOMAIN_FULL ?>/images/referral/banner-hello.jpg"
         data-image:odnoklassniki="<?= DOMAIN_FULL ?>/images/referral/banner-hello.jpg"
         data-image:vkontakte="<?= DOMAIN_FULL ?>/images/referral/banner-hello.jpg"
         data-image:facebook="<?= DOMAIN_FULL ?>/images/referral/banner-hello.jpg">
    </div>

    <div class="copy-link">
        <input type="text" value="<?= $url ?>" id="referral-code">
        <input type="submit" value="Копировать ссылку" data-clipboard-text="<?= $code ?>" id="copy-code">
    </div>
</div>
