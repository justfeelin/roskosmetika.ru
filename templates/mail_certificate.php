<?php
/**
 * @var array $content
 */
?>
<h3>Ваш сертификат на сумму <?= count_morphology((int)abs($content['discount']) . ' рублей|рубль|рубля') ?></h3>

<?php for ($i = 0, $n = count($content['certificates']); $i < $n; ++$i) { ?>
    <p>
        <img src="<?= DOMAIN_FULL . '/' . promo_codes::CERTIFICATE_DIR . '/' . rawurlencode($content['certificates'][$i]) ?>">
    </p>
<?php
}
