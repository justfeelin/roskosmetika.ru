<?php
/**
 * @var string $name
 * @var string $categories_html
 * @var bool $multicol
 * @var string $schema_scope
 * @var string $schema_name
 * @var string $subname
 * @var string $img
 * @var string $imgAlt
 * @var string $imgTitle
 * @var array $filters
 * @var string $found
 * @var bool $relevance
 */
$image = !empty($img);
?>
<div class="row products-header filters-box">
    <?php if (!empty($name) || !empty($categories_html)) { ?>
        <div class="col-xs-12 products-header-top<?= (empty($categories_html) ? ' slim' : '') . ((empty($multicol)) ? ' single-col' : '') . ($image ? ' w-img' : '') ?>">
            <?php if (!empty($name)) { ?>
                <div class="prods_title_sort" itemscope itemtype="http://schema.org/<?= !empty($schema_scope) ? $schema_scope : 'WebPage' ?>">
                    <h1 itemprop="<?= !empty($schema_name) ? $schema_name : 'headline' ?>"><?= $name ?></h1>
                    <?php if (!empty($subname)) { ?>
                        <h2><?= $subname ?></h2>
                    <?php } ?>
                </div>
            <?php
            }

            if (!empty($categories_html)) {
            ?>
                <div class="products-header-top-nav">
                    <?= $categories_html ?>
                </div>
            <?php
            }

            if ($image) {
            ?>
                <img class="top-img" src="<?= $img ?>"
                    <?= (!empty($imgAlt) ? 'alt="' . $imgAlt . '"' : '') . (isset($imgTitle) ? 'title="' . $imgTitle . '"' : '') ?>
                >
            <?php } ?>
        </div>
    <?php } ?>
    <div class="col-xs-12 products-header-filters filters">
        <form method="GET" id="filter-form">
            <?php
            if (!empty($filters)) {
                foreach ($filters as $filter_key => $filter) {
                    $n = count($filter);
            ?>
                    <div class="lvl_3 expandable dropdown-gray-style">
                        <div class="lvl_3_group filter-group">
                            <h3 class="lvl_3_group_title"><?= $filter_key ?> <i class="arrow clear-group"></i></h3>
                            <div class="lvl_3_block c<?= $n ?> lvl_3_last filters-pad">
                                <?php for ($i = 0; $i < $n; ++$i) { ?>
                                    <ul class="filter">
                                        <?php for ($y = 0, $m = count($filter[$i]); $y < $m; ++$y) { ?>
                                            <li><p class="fi f<?= $filter[$i][$y]['id'] ?> available"><span></span><?= $filter[$i][$y]['name'] ?><input class="fin" type="hidden" data-name="f[]" value="<?= $filter[$i][$y]['id'] ?>"></p></li>
                                        <?php } ?>
                                    </ul>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
            <?php
                }
            }

            if (!empty($content['filter_tms'])) {
                $n = count($content['filter_tms'])
            ?>
                <div class="lvl_3 expandable dropdown-gray-style">
                    <div class="lvl_3_group filter-group tm-filter">
                        <h3 class="lvl_3_group_title">Торговые марки <i class="arrow clear-group"></i></h3>
                        <div class="lvl_3_block c<?= $n ?> lvl_3_last filters-pad">
                            <?php for ($i = 0; $i < $n; ++$i) { ?>
                                <ul class="filter">
                                    <?php for ($y = 0, $m = count($content['filter_tms'][$i]); $y < $m; ++$y) { ?>
                                        <li>
                                            <p class="fi tm tm<?= $content['filter_tms'][$i][$y]['id'] ?> available">
                                                <span></span>
                                                <?= $content['filter_tms'][$i][$y]['name'] ?>
                                                <input class="fin" type="hidden" data-name="t[]" value="<?= $content['filter_tms'][$i][$y]['id'] ?>">
                                            </p>
                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php
            }

            if (!empty($content['filter_categories'])) {
                $n = count($content['filter_categories'])
            ?>
                <div class="lvl_3 expandable dropdown-gray-style">
                    <div class="lvl_3_group filter-group tm-filter">
                        <h3 class="lvl_3_group_title">Тип продукции <i class="arrow clear-group"></i></h3>
                        <div class="lvl_3_block c<?= $n ?> lvl_3_last filters-pad">
                            <?php for ($i = 0; $i < $n; ++$i) { ?>
                                <ul class="filter">
                                    <?php for ($y = 0, $m = count($content['filter_categories'][$i]); $y < $m; ++$y) { ?>
                                        <li>
                                            <p class="fi cat cat<?= $content['filter_categories'][$i][$y]['id'] ?> available">
                                                <span></span>
                                                    <?= $content['filter_categories'][$i][$y]['name'] ?>
                                                <input class="fin" type="hidden" data-name="c[]" value="<?= $content['filter_categories'][$i][$y]['id'] ?>">
                                            </p>
                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php
            }

            if (!empty($content['filter_prices'])) {
            ?>
                <div class="lvl_3 expandable dropdown-gray-style">
                    <div class="lvl_3_group" id="price-range-box">
                        <h3 class="lvl_3_group_title">Цена <i class="arrow clear-group"></i></h3>
                        <div class="lvl_3_block c2 lvl_3_last filters-pad">
                            <div id="price-range" data-from="<?= $content['filter_prices'][0] ?>" data-to="<?= $content['filter_prices'][1] ?>"></div>
                            <input class="range-label" type="text" data-name="pr[0]" id="price-from">
                            <input class="range-label" type="text" data-name="pr[1]" id="price-to">
                        </div>
                    </div>
                </div>
            <?php
            }

            if (!empty($content['filter_countries'])) {
                $n = count($content['filter_countries']);
            ?>
            <div class="lvl_3 expandable dropdown-gray-style">
                <div class="lvl_3_group filter-group">
                    <h3 class="lvl_3_group_title">Страна <i class="arrow clear-group"></i></h3>
                    <div class="lvl_3_block c<?= $n ?> lvl_3_last filters-pad">
                        <?php for ($i = 0; $i < $n; ++$i) { ?>
                            <ul class="filter">
                                <?php for ($y = 0, $m = count($content['filter_countries'][$i]); $y < $m; ++$y) { ?>
                                    <li>
                                        <p class="fi cid cid<?= $content['filter_countries'][$i][$y]['id'] ?> available">
                                            <span></span>
                                            <?= $content['filter_countries'][$i][$y]['name'] ?>
                                            <input class="fin" type="hidden" data-name="cid[]" value="<?= $content['filter_countries'][$i][$y]['id'] ?>">
                                        </p>
                                    </li>
                                <?php } ?>
                            </ul>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php } ?>
        </form>
    </div>
    <div class="products-header-bottom col-xs-12 products-loading">
        <div class="products-header-amount">
            <div id="amountGoods"><span id="foundCount"><?= !empty($found) ? $found: '' ?></span>&nbsp;</div>
        </div>
        <div class="products-header-sort">
            <div class="row sort">
                <div class="sort__lable">сортировать по:</div>
                <ul class="sort__menu">
                    <?php if (isset($relevance)) { ?>
                        <li class="sort-link default-sort active-sort up" data-sort="">
                            <a href="#">совпадению</a>
                        </li>
                    <?php } ?>
                    <li class="sort-link" data-sort="price">
                        <a href="#">цене
                            <i class="down-i glyphicon glyphicon-sort-by-attributes-alt"></i>
                            <i class="up-i glyphicon glyphicon-sort-by-attributes"></i>
                        </a>
                    </li>
                    <li class="sort-link up" data-sort="discount">
                        <a href="#">скидке
                            <i class="down-i glyphicon glyphicon-sort-by-attributes-alt"></i>
                            <i class="up-i glyphicon glyphicon-sort-by-attributes"></i>
                        </a>
                    </li>
                    <li class="sort-link <?= !isset($relevance) && empty($content['sort_popularity']) ? 'default-sort active-sort up' : '' ?>" data-sort="name">
                        <a href="#">названию
                            <i class="down-i glyphicon glyphicon glyphicon-sort-by-alphabet-alt"></i>
                            <i class="up-i glyphicon glyphicon glyphicon-sort-by-alphabet"></i>
                        </a>
                    </li>
                    <?php if (!empty($content['sort_popularity'])) { ?>
                        <li class="sort-link <?= !isset($relevance) ? ' default-sort active-sort up' : '' ?>" data-sort="popular">
                            <a href="#">популярности
                                <i class="down-i glyphicon glyphicon-sort-by-attributes"></i>
                                <i class="up-i glyphicon glyphicon-sort-by-attributes-alt"></i>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</div>
