<?php
/**
 * @var string $header
 * @var array $content
 */
if (!empty($crumb)) {
    Template::partial(
        'breadcrumbs',
        [
            'crumbs' => [
                [
                    'name' => $crumb,
                ],
            ],
        ]
    );
}
?>
<div class="row all_articles">
    <h1 class="page_h1"><?= $header ?></h1>
    <?php for ($i = 0, $n = count($content['articles']); $i < $n; ++$i) { ?>
        <div class="articles col-xs-12" itemscope itemtype="http://schema.org/Article">
            <p class="articles_part"><span itemprop="articleSection"><?= $content['articles'][$i]['name'] ?></span></p>

            <div class="articles_block">
                <?php for ($y = 0, $m = count($content['articles'][$i]['articles']); $y < $m; ++$y) { ?>
                    <p><a class="article_link" href="/articles/<?= $content['articles'][$i]['articles'][$y]['url'] ?>" itemprop="url"><span
                                itemprop="name"><?= $content['articles'][$i]['articles'][$y]['name'] ?></span></a></p>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
</div>
<?php
Template::partial('mind_box', [
                'mb_is_auth' => $is_auth,
                'mb_top_view' => $mb_top_view,
            ]);
