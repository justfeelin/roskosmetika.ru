 <!-- VK retargeting -->
<script type="text/javascript" src="https://vk.com/js/api/openapi.js?160" async="true"></script>
<noscript><img src="https://vk.com/rtrg?p=VK-RTRG-352543-4tMLM" style="position:fixed; left:-999px;" alt=""/></noscript>
<!-- /VK retargeting -->

<!-- Google UA Main -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-10838324-2', 'auto', {'name': 'MainUATracker', 'allowLinker': true});
  ga('MainUATracker.require', 'linker');
  ga('MainUATracker.require', 'ec');
  ga('MainUATracker.set', '&cu', 'RUB');
  ga('MainUATracker.linker:autoLink', ['m.roskosmetika.ru'] );
  ga('MainUATracker.set', 'page', window.location.pathname + window.location.hash);

</script>
<!-- /Google UA Main -->

<!-- Google UA -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-10838324-7', 'auto', {'name': 'UniversalAnalytics', 'allowLinker': true});
    ga('UniversalAnalytics.require', 'linker');
    ga('UniversalAnalytics.require', 'ec');
    ga('UniversalAnalytics.set', '&cu', 'RUB');
    ga('UniversalAnalytics.linker:autoLink', ['m.roskosmetika.ru'] );
    ga('UniversalAnalytics.set', 'page', window.location.pathname + window.location.hash);
</script>
<!-- /Google UA -->

<!-- Google Remarketing -->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 944077293;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/944077293/?value=0&amp;guid=ON&amp;script=0"/>
    </div>
</noscript>
<!-- /Google Remarketing -->


<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter482478 = new Ya.Metrika({
                    id: 482478,
                    webvisor: true,
                    clickmap: true,
                    trackLinks: true,
                    ecommerce: "dataLayer", 
                    accurateTrackBounce: true, params: window.yaParams || {}
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n);
            };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>
    <div><img src="//mc.yandex.ru/watch/482478" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->

<!-- Sendpulse Push -->
<script charset="UTF-8" src="//cdn.sendpulse.com/js/push/4198de50888d4f0e9e113d605c0b1f19_1.js" async></script>
<!-- /Sendpulse Push -->

<!-- Admitad 2018 -->
<script src="https://www.artfut.com/static/tagtag.min.js?campaign_code=254166d1c0" async onerror='var self = this;window.ADMITAD=window.ADMITAD||{},ADMITAD.Helpers=ADMITAD.Helpers||{},ADMITAD.Helpers.generateDomains=function(){for(var e=new Date,n=Math.floor(new Date(2020,e.getMonth(),e.getDate()).setUTCHours(0,0,0,0)/1e3),t=parseInt(1e12*(Math.sin(n)+1)).toString(30),i=["de"],o=[],a=0;a<i.length;++a)o.push({domain:t+"."+i[a],name:t});return o},ADMITAD.Helpers.findTodaysDomain=function(e){function n(){var o=new XMLHttpRequest,a=i[t].domain,D="https://"+a+"/";o.open("HEAD",D,!0),o.onload=function(){setTimeout(e,0,i[t])},o.onerror=function(){++t<i.length?setTimeout(n,0):setTimeout(e,0,void 0)},o.send()}var t=0,i=ADMITAD.Helpers.generateDomains();n()},window.ADMITAD=window.ADMITAD||{},ADMITAD.Helpers.findTodaysDomain(function(e){if(window.ADMITAD.dynamic=e,window.ADMITAD.dynamic){var n=function(){return function(){return self.src?self:""}}(),t=n(),i=(/campaign_code=([^&]+)/.exec(t.src)||[])[1]||"";t.parentNode.removeChild(t);var o=document.getElementsByTagName("head")[0],a=document.createElement("script");a.src="https://www."+window.ADMITAD.dynamic.domain+"/static/"+window.ADMITAD.dynamic.name.slice(1)+window.ADMITAD.dynamic.name.slice(0,1)+".min.js?campaign_code="+i,o.appendChild(a)}});'>
</script>
<!-- /Admitad 2018 -->

<!-- Rating Mail.ru counter for mytarget -->
<script type="text/javascript">
var _tmr = window._tmr || (window._tmr = []);
_tmr.push({id: "3142018", type: "pageView", start: (new Date()).getTime(), pid: "USER_ID"});
(function (d, w, id) {
  if (d.getElementById(id)) return;
  var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
  ts.src = "https://top-fwz1.mail.ru/js/code.js";
  var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
  if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
})(document, window, "topmailru-code");
</script>
<noscript>
  <div>
    <img src="https://top-fwz1.mail.ru/counter?id=3142018;js=na" style="border:0;position:absolute;left:-9999px;" alt="Top.Mail.Ru" />
  </div>
</noscript>
<!-- //Rating Mail.ru counter -->