<?php
/**
 * @var array $content
 */
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <?= $content['text'] ?>

            <?php
            Template::partial(
                'mail_cart_items',
                [
                    'items' => $content['products'],
                    'discount' => $content['discount'],
                    'discount_type' => $content['discount_type'],
                    'promo_discount' => $content['promo_discount'],
                    'shipping_name' => $content['shipping_name'],
                    'shipping_price' => $content['shipping_price'],
                    'final_sum' => $content['final_sum'],
                    'duration' => $content['duration'],
                    'shipping_info' => $content['shipping_info'],
                    'products_title' => 'Товар в заказе',
                ]
            );
            ?>

            <?php if ($content['promo_code']) { ?>
                <h2 style="font-family: Tahoma, Geneva, sans-serif;font-size: 12px;color: #1B1B1B;">Не забудьте использовать промо-код!</h2>

                <p style="font-size: 14px; font-family: Tahoma, Geneva, sans-serif;">
                    При использовании промо-кода
                    <b><?= $content['promo_code']['code'] ?></b>
                    дополнительная скидка составит
                    <span style="color:red"><?= (int)($content['sum_with_discount'] - ($content['sum_with_discount'] * (100 - $content['promo_code']['discount']) / 100)) ?> р.</span>!
                </p>
            <?php } ?>
        </td>
    </tr>

    <?= $content['neighbours_html'] ?>
</table>
