<?php
/**
 * @var string $header
 * @var array $msg
 * @var array $values
 * @var bool $is_auth
 * @var array $main_spec
 */
if (!empty($crumb)) {
    Template::partial(
        'breadcrumbs',
        [
            'crumbs' => [
                [
                    'name' => $crumb,
                ],
            ],
        ]
    );
}
?>
<div class="row feedback">
    <div class="col-xs-8 col-xs-offset-2">
        <h1><?= $header ?></h1>

        <p>Мы оперативно ответим на любые Ваши вопросы по работе магазина и обработке Вашего заказа.</p>

        <p>Также мы будем рады получить Ваши пожелания по работе интернет-магазина!</p>

        <form action="/feedback" method="post">
            <div class="form-group <?= isset($msg['contacts']) ? 'has-error' : '' ?>">
                <p>Ваши контакты (телефон или E-mail)<span class="red">*<span></p>
                <input name="contacts" class="form-control fb_contacts"
                       <?= isset($values['contacts']) ? 'value="' . h($values['contacts']) . '"' : '' ?>>
                <?= isset($msg['contacts']) ? '<p class="form_error">' . $msg['contacts'] . '</p>' : '' ?>
            </div>
            <div class="form-group <?= isset($msg['message']) ? 'has-error' : '' ?>">
                <p>Ваш вопрос, идеи или пожелания<span class="red">*<span></p>
                <textarea class="form-control" rows="5"
                          name="message"><?= isset($values['message']) ? h($values['message']) : '' ?></textarea>
                <?= isset($msg['message']) ? '<p class="form_error">' . $msg['message'] . '</p>' : '' ?>
            </div>
            <p><span class="red">*</span>&nbsp;-&nbsp;Обязательные&nbsp;поля</p>
            <button type="submit" name="submit">Отправить</button>
            <p class="hurry_notice_info">Нажимая на кнопку «Отправить», вы соглашаетесь на обработку персональных данных в соответствии с <a class="oferta_link" href="/page/uslovija-obrabotki-personalynih-dannyh" target="_blank">условиями</a>.</p>
        </form>
    </div>
</div>
<?php
Template::partial(
    'main_special',
    [
        'is_auth' => $is_auth,
        'main_spec' => $main_spec,
    ]
);
