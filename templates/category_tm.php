<?php
/**
 * @var array $breadcrumbs
 * @var array $content
 * @var array $Pagination
 * @var string $categories_html
 * @var string $found
 * @var string $category_url
 */
$crumbs = ''; 

if ($breadcrumbs) {

    $crumbs = $breadcrumbs[0]['bc_name'] . $content['tm']['name'];

    Template::partial(
        'breadcrumbs',
        [
            'crumbs' => $breadcrumbs,
        ]
    );
}

if ($content['products']) {
    $paginationHTML = Template::get_tpl('pagination_wrap', null, [
        'Pagination' => !empty($Pagination) ? $Pagination : null,
    ]);

    if ($content['category']['name_on_products'] > 0) {
        $hasH1 = !!$content['category']['h1'];

        $hasAltNumber = !!($hasH1 ? $content['category']['h1_alt_number'] : $content['category']['name_alt_number']);

        $categoryName = $hasAltNumber
            ? helpers::numeric_name(
                $hasH1 ? $content['category']['h1'] : $content['category']['name'],
                $hasH1 ? $content['category']['h1_alt_number'] : $content['category']['name_alt_number'],
                $hasH1 ? $content['category']['h1_plural'] : $content['category']['name_plural'],
                false
            )
            : (
                $hasH1 ? $content['category']['h1'] : $content['category']['name']
            );
    } else {
        $categoryName = null;
    }
?>
    <div>
        <?php
        Template::partial(
            'product_filter',
            [
                'fitlers' => $content['filters'],
                'categories_html' => $categories_html,
                'name' => $content['h1'],
                'found' => $found,
                'schema_scope' => 'Brand',
                'schema_name' => 'name',
                'content' => $content,
            ]
        );
        ?>
        <div class="row">
            <div class="col-xs-12">
                <?= $paginationHTML ?>
            </div>
        </div>
        <div class="ua-product-list products product-list" data-list="Категория-бренд «<?= $crumbs ?>»">
            <?php
            Template::partial(
                'product_list',
                [
                    'products' => $content['products'],
                    'product_type' => $categoryName,
                    'product_type_n' => $content['category']['name_on_products'],
                ]
            );
            ?>
        </div>
        <?php
        echo
            $paginationHTML;

        if (!empty($content['popular_products'])) {
            ?>
            <h3>Популярные товары</h3>
            <?php
            Template::partial(
                'product_list',
                [
                    'products' => $content['popular_products'],
                    'no_description' => true,
                ]
            );
        }

        if ($content['tms']) {
        ?>
            <div class="row bottom-tms tms">
                <div class="col-xs-12">
                    <?php
                    Template::partial(
                        'category_tms',
                        [
                            'tms' => $content['tms'],
                            'category_url' => $category_url,
                            'small' => true,
                        ]
                    );
                    ?>
                </div>
            </div>
        <?php
        }

        if ($content['reviews']) {
        ?>
            <a class="info_title_link" href="/otzyvy/category-tm/<?= $content['category']['_full_url'] . '/' . $content['tm']['url'] ?>">Отзывы <?= $content['reviews_title'] ?></a>
            <div class="row">
                <?php
                Template::partial(
                    'product_reviews',
                    [
                        'reviews' => $content['reviews'],
                    ]
                );
                ?>
            </div>
        <?php } ?>
    </div>
    <?php if ($content['category'][$content['category']['level'] == 1 ? 'id' : ($content['category']['level'] == 2 ? 'parent_1' : 'parent_2')] == Categories::PARFUMERY_ID) { ?>
        <div class="clearfix"></div>
        <noindex>
            <div id="prepay-only">*Товары из раздела &laquo;Парфюмерия&raquo; доступны только по предоплате</div>
        </noindex>
<?php
    }
} else {
?>
    <h1><?= $content['h1'] ?></h1>
    <div class="no_items">К сожалению, в данный момент товары в этой категории не представлены</div>
<?php
    Template::personalized_products();
}

if ($content['desc']) {
?>
    <div class="row prods_title_sort" itemscope itemtype="<?= !empty($content['cur_cat']['id']) ? 'http://schema.org/WebPage' : 'http://schema.org/Brand' ?>">
        <div class="col-xs-12">
            <div class="read_more_desc" itemprop="description" data-size="145">
                <?= $content['desc'] ?>
            </div>
        </div>
    </div>
<?php
}

Template::partial(
    'base_links',
    [
        'links' => $content['base_links'],
    ]
);
