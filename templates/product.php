<?php
/**
 * @var array $content
 * @var bool $is_auth
 * @var array $reviews
 * @var array $month
 */
$packsN = count($content['packs']);

$parfumery = false;

if (isset($content['categories'])) {
    Template::partial(
        'breadcrumbs',
        [
            'crumbs' => $content['categories']['crumbs'],
        ]
    );
}
?>
    <div itemscope itemtype="http://schema.org/Product" id="product-page">
        <div class="row product">
            <div class="col-xs-4 img-box">
                <?php if ($packsN > 0) { ?>
                    <img
                            width="300"
                            height="300"
                            src="<?= IMAGES_DOMAIN_FULL ?>/images/prod_photo/<?= Product::img_url($content['pic_quant'] > 0 ? $content['id'] . '_1.jpg' : $content['src'], $content['url']) ?>"
                            alt="<?= h($content['alt'] ?: $content['name']) ?>"
                            title="<?= h($content['alt'] ?: $content['name']) ?>"
                            itemprop="image"
                            class="main_img"
                            id="zoom_img"
                    >
                <?php } ?>
                <div class="prod_tag_boxes">

                    <?php

                    if ($content['available'] && ($content['discount_end'] || $content['pack_discount_end'])) {

                        ?>
                        <a
                                href="#"
                                rel="nofollow"
                                data-link="<?= Template::hidelink('/products/sales') ?>"
                                id="product-discount-end"
                                class="hidden_link"
                            <?= !$content['available'] || !$content['visible'] || !$content['active'] || !$content['discount_end'] ? 'style="display: none"' : '' ?>
                        >
                            <span class="spec_price_tag_1">Акция</span>
                            <span class="small_spec_price_box">-<b
                                        id="discount-value-sales"><?= $content['discount'] ?></b>%</span>
                            <span class="spec_price_tag_2">осталось</span>
                            <span class="spec_price_tag_3"><b
                                        id="discount-end-value"><?= $content['discount_end'] ?></b>&nbsp;<span
                                        id="discount-end-day-value"><?= helpers::num_ending($content['discount_end']) ?></span></span>
                        </a>

                        <?php
                    }

                    if ($content['available'] && $content['max_discount']) {

                        ?>

                        <a
                                href="#"
                                rel="nofollow"
                                data-link="<?= Template::hidelink('/products/hurry') ?>"
                                id="product-discount"
                                class="hidden_link"
                            <?= $content['discount_end'] || !$content['available'] || !$content['visible'] || !$content['active'] || !$content['max_discount'] ? 'style="display: none"' : '' ?>
                        >
                            <span class="hurry_tag_1">Распродажа</span>
                            <span class="small_hurry_box">-<b id="discount-value-hurry"><?= $content['discount'] ?></b>%</span>
                        </a>

                        <?php
                    }

                    if ($content['hit']) {

                        ?>
                        <a
                                href="#"
                                rel="nofollow"
                                data-link="<?= Template::hidelink('/products/hit') ?>"
                                class="hidden_link small_hit_box">Хит<br>продаж</a>

                        <?php
                    }

                    if (($content['new'] || $content['pack_new']) && !$content['soon']) {

                        ?>
                        <a
                                href="#"
                                rel="nofollow"
                                data-link="<?= Template::hidelink('/products/new') ?>"
                                class="hidden_link small_new_box">Новинка</a>

                        <?php
                    }

                    if ($content['soon']) {

                        ?>
                        <span class="small_action_box">Скоро<br>в продаже</span>
                        <?php
                    }
                    ?>
                </div>
                <div id="gallery">
                    <?php
                    for ($i = 0; $i < $packsN; ++$i) {
                        if ($content['packs'][$i]['pic_quant'] > 0) {
                            ?>
                            <div id="gallery-<?= $content['packs'][$i]['id'] ?>"
                                 class="product-gallery<?= $i !== 0 ? ' none' : '' ?>">
                                <?php for ($y = 1, $n = $content['packs'][$i]['pic_quant'] + 1; $y < $n; ++$y) { ?>
                                    <a
                                            href="#"
                                            data-image="<?= IMAGES_DOMAIN_FULL ?>/images/prod_photo/<?= Product::img_url($content['packs'][$i]['id'] . '_' . $y . '.jpg', $content['url']) ?>"
                                            data-zoom-image="<?= IMAGES_DOMAIN_FULL ?>/images/prod_photo/<?= Product::img_url($content['packs'][$i]['id'] . '_' . $y . '_max.jpg', $content['url']) ?>"
                                            class="preview-link <?= $i === 0 && $y === 1 ? ' gallery_active' : '' ?>">
                                        <img
                                                src="<?= IMAGES_DOMAIN_FULL ?>/images/prod_photo/<?= Product::img_url($content['packs'][$i]['id'] . '_' . $y . '_pre.jpg', $content['url']) ?>"
                                                width="57" height="57">
                                    </a>
                                <?php } ?>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
                <div id="real-look">Фактический вид товара может отличаться от изображения на сайте</div>
                <?php if ($is_auth) { ?>
                    <div class="row yet_and_favorites">
                        <a class="favorites_icom add_to_fav" href="#" rel="nofollow"
                           data-link="<?= Template::hidelink('/room/add_favorites/' . $content['id']) ?>"></a>
                        <a class="yet_and_favorites_link add_to_fav" href="#" rel="nofollow"
                           data-link="<?= Template::hidelink('/room/add_favorites/' . $content['id']) ?>"><span
                                    class="underline_dahsed">В&nbsp;избранное</span></a>
                    </div>
                <?php } ?>
                <div class="talk_to_all_tag">Рассказать&nbsp;друзьям:</div>
                <div class="talk_to_all_btns yashare-auto-init" data-yashareType="none"
                     data-yashareQuickServices="facebook,|,vkontakte,|,twitter,|,odnoklassniki,|,gplus">
                </div>
            </div>
            <div class="col-xs-5 prod_discr">
                <div class="descr-box">
                    <h1 itemprop="name" data-id="<?= $content['id'] ?>">
                        <div id="product-name"><?= $content['h1'] ?></div>
                        <?php if ($content['eng_name']) { ?>
                            <span class="eng_name" itemprop="alternateName"><?= $content['eng_name'] ?></span>
                        <?php } ?>
                    </h1>
                    <?php if ($content['tm_visible']) { ?>
                        <span itemprop="brand" itemscope itemtype="http://schema.org/Brand">
                        <a class="h2_tm" id="product-tm" itemprop="url" href="/tm/<?= $content['tm_url'] ?>"><span
                                    itemprop="name"><?= $content['tm'] ?></span>&nbsp;(<?= $content['country'] ?>)</a>
                    </span>
                    <?php } else { ?>
                        <span class="hidden" id="product-tm" href="/tm/<?= $content['tm_url'] ?>"><?= $content['tm'] ?>
                            (<?= $content['country'] ?>)</span>
                    <?php } ?>
                    <div class="prod_f_desc" itemprop="description">
                        <?= $content['description'] ?>
                        <div>
                            <?= $content['description_synth'] ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-3 product_pack_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                <div class="row hot_icons">
                    <div class="col-xs-4 hot_icon" data-i="1" title="Удобная доставка"><a target="blank"
                                                                                          class="hidden_link icon-link"
                                                                                          href="#"
                                                                                          rel="nofollow"
                                                                                          data-link="<?= Template::hidelink('/page/delivery') ?>"><span
                                    class="hot_delivery"></span></a>
                        <div class="text hidden">
                            Ближайшая доставка по Москве - завтра.
                            <br>
                            МО и другие регионы России - от 1 до 5 дней<sup>*</sup>.
                            <br>
                            <br>
                            Доставка бесплатно при заказе на сумму от 5 500.<sup>**</sup>
                            <br>
                            <br>
                            <a href="/page/delivery">Подробнее о доставке >></a>
                        </div>
                    </div>

                    <div class="col-xs-4 hot_icon edge" data-i="2" title="Гибкая система скидок"><a target="blank"
                                                                                                    class="hidden_link icon-link"
                                                                                                    href="#"
                                                                                                    rel="nofollow"
                                                                                                    data-link="<?= Template::hidelink('/page/sales') ?>"><span
                                    class="hot_sels"></span></a>
                        <div class="text hidden">
                            <ul>
                                <li>
                                    Скидка 2 % — при регистрации на сайте
                                    <br>
                                    и последующем оформлении заказа через корзину.
                                </li>
                                <li>Скидка 5 % — уже с 3-го заказа.</li>
                                <li>Скидка 10 % — для специалистов, от 50 000 <span class="rub">p</span></li>
                                <li>
                                    А также, накопительные скидки
                                    <br>
                                    и скидки в зависимости от суммы заказа.
                                    <br>
                                    Действует правило большей скидки!
                                </li>
                            </ul>
                            <a href="/page/sales">Подробнее о скидках >></a>
                        </div>
                    </div>

                    <div class="col-xs-4 hot_icon edge" data-i="3" title="Оплата любым способом"><a target="blank"
                                                                                                    class="hidden_link icon-link"
                                                                                                    href="#"
                                                                                                    rel="nofollow"
                                                                                                    data-link="<?= Template::hidelink('/page/delivery#pay') ?>"><span
                                    class="hot_pay"><span class="rub">p</span></span></a>
                        <div class="text hidden">
                            Выберите любой удобный для Вас
                            <br>
                            способ оплаты заказа:
                            <ul>
                                <li>наличные курьеру;</li>
                                <li>наложенный платеж (комиссия - до 4 %);</li>
                                <li>банковская карта Visa, MasterCard и Maestro;</li>
                                <li>банковский перевод;</li>
                                <li>Яндекс.Деньги;</li>
                                <li>баланс мобильного телефона;</li>
                                <li>терминалы и салоны связи;</li>
                                <li>WebMoney.</li>
                            </ul>
                            <a href="/page/delivery">Подробнее о способах оплаты >></a>
                        </div>
                    </div>
                </div>
                <?php
                $availablePacks = $hasPacksInStock = null;

                for ($i = 0; $i < $packsN; ++$i) {
                    if (!$content['packs'][$i]['visible']) {
                        continue;
                    }

                    $packAvailable = $content['packs'][$i]['available'] && !$content['packs'][$i]['proff_price'];

                    $availablePacks = $availablePacks !== null ? $availablePacks : ($packAvailable ? $i : null);
                    $hasPacksInStock = $hasPacksInStock !== null ? $hasPacksInStock : ($availablePacks !== null && $content['packs'][$i]['visible'] && $content['packs'][$i]['active'] ? $i : null);

                    if ($i !== 0) {
                        ?>
                        <div class="pack_line"></div>
                    <?php } ?>
                    <div class="row one_pack<?= ($content['packs'][$i]['available'] ? ' in-stock' : '') . ($packAvailable ? ' available-pack' : '') . ($content['packs'][$i]['main_id'] == 0 ? ' main_id_pack' : '') . ($i === 0 ? ' active_pack' : '') ?>"
                         data-quant="<?= $content['packs'][$i]['pic_quant'] ?>"
                         data-pack-id="<?= $content['packs'][$i]['id'] ?>"
                         data-discount="<?= $content['packs'][$i]['discount'] ?>"
                         data-discount-end="<?= $content['packs'][$i]['discount_end'] ?>"
                         data-discount-end-day="<?= helpers::num_ending($content['packs'][$i]['discount_end']) ?>">
                        <div class="col-xs-4">
                            <p class="pack"><?= $content['packs'][$i]['pack'] ?></p>

                            <p class="main_id_tag">код:</p>

                            <p class="main_id"><?= $content['packs'][$i]['id'] ?></p>
                        </div>
                        <div class="col-xs-8">
                            <?php
                            if ($content['packs'][$i]['active']) {
                                if ($content['packs'][$i]['proff_price'] && !REAL_PROF) {
                                    ?>
                                    <div class="col-xs-12">&nbsp;</div>
                                    <?php
                                } else {
                                    if ($content['packs'][$i]['available'] && (int)$content['packs'][$i]['special_price']) {
                                        ?>
                                        <div class="price2_and_counter"
                                            <?php if ($i === 0) { ?>
                                                itemscope
                                                itemtype="http://schema.org/AggregateOffer"
                                            <?php } ?>>
                                            <p class="col-xs-12 prod_old_price"
                                                <?php if ($i === 0) { ?>
                                                    itemprop="highPrice"
                                                <?php } ?>><span
                                                        class="price"><?= formatPrice($content['packs'][$i]['price']) ?></span>
                                                <span class="rub_h">руб</span>
                                                <span class="rub gray">p</span></p>

                                            <p class="col-xs-6 prod_new_price"
                                                <?php if ($i === 0) { ?>
                                                    itemprop="lowPrice"
                                                <?php } ?>>
                                                <span class="new_price p-price"><?= formatPrice($content['packs'][$i]['special_price']) ?></span>
                                                <span class="rub_h">руб</span>
                                                <span class="rub">p</span>
                                            </p>
                                            <span class="stock-label">(В наличии)</span>
                                        </div>
                                    <?php } else { ?>
                                        <div class="col-xs-12 prod_price"
                                            <?php if ($i === 0) { ?>
                                                itemprop="price"
                                            <?php } ?>>
                                            <span class="p-price"><?= formatPrice($content['packs'][$i]['price']) ?></span>
                                            <span class="rub_h">руб</span>
                                            <span class="rub">p</span>
                                            <?php if ($content['packs'][$i]['available']) { ?>
                                                <span class="stock-label">(В наличии)</span>
                                            <?php } ?>
                                        </div>
                                        <?php
                                    }
                                }

                                if ($i === 0) {
                                    ?>
                                    <meta itemprop="priceCurrency" content="RUB"/>
                                <?php } ?>
                                <div class="row">
                                    <?php if ($content['packs'][$i]['proff_price'] && !REAL_PROF) { ?>
                                        <a class="col-xs-12 link for_proff"
                                           title="Стоимость и условия продажи продукции профессионального объема"
                                           href="#"
                                           data-prod-name="<?= $content['name'] . ' ' . $content['pack'] ?>"
                                           data-prod-id="<?= $content['id'] ?>"
                                           data-prod-url="<?= $content['url'] ?>"
                                            <?php if ($content['packs'][$i]['id'] != $content['id']) { ?>
                                                data-pack-id="<?= $content['packs'][$i]['id'] ?>"
                                            <?php } ?>
                                        >Для&nbsp;профессионалов</a>
                                        <?php
                                    } else {
                                        if ($i === 0) {
                                            if ($content['packs'][$i]['available']) {
                                                ?>
                                                <meta itemprop="availability" href="http://schema.org/InStock">
                                            <?php } else { ?>
                                                <meta itemprop="availability" href="http://schema.org/OutOfStock">
                                                <?php
                                            }
                                        }

                                        if ($content['packs'][$i]['available']) {
                                            ?>
                                            <p class="col-xs-4 prod_cart"
                                               ng-click="cartProduct(<?= $content['packs'][$i]['id'] ?>, 1, $event)">
                                                Купить<span
                                                        class="prod_pointer_cart"></span></p>
                                        <?php } else { ?>
                                            <div class="col-xs-4 prod_waiting pre_order_btn"
                                                 data-id="<?= $content['packs'][$i]['id'] ?>"
                                                 data-prod-url="<?= $content['url'] ?>"
                                                 data-name="<?= $content['name'] . ' ' . $content['packs'][$i]['pack'] ?>">
                                                <p>Сообщить</p>
                                                <p> о поступлении</p></div>
                                            <div class="pre_cart_title pre_order_prod">Ожидается<span
                                                        class="pre_cart_icon">i</span>
                                            </div>
                                        <?php
                                        }
                                    }
                                    ?>
                                </div>
                                <?php
                            } else {
                                if (!$content['soon']) {
                                    ?>
                                    <meta itemprop="availability" href="http://schema.org/OutOfStock">
                                    <p class="end_sell align_right">Снят с продаж</p>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <?php
                }

                if (false && $hasPacksInStock !== null) {
                    ?>
                    <a id="one-click-buy" href="#" class="prod_cart">Купить в один клик</a>
                    <?php
                }

                if ($parfumery) {
                    ?>
                    <noindex>
                        <div id="prepay-only" class="row">*Данный товар доступен только по предоплате</div>
                    </noindex>
                    <?php
                }

                if ($hasPacksInStock !== null && $content['id'] != Product::CERTIFICATE_ID && !$content['soon']) {
                    ?>
                    <div id="found-cheaper">
                        <a href="#">
                            <div class="lbl"></div>
                            <div>Видели дешевле?</div>
                            <br>
                            <div>Снизим цену!</div>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </div>
        <span class="row"></span>
        <div class="row spec_prod_info">
            <div class="col-xs-12" id="product-tabs">
                <div>
                    <?php if ($content['ingredients']) { ?>
                        <h2 class="tab-btn vis" data-tab="0"><span>Активные&nbsp;компоненты</span></h2>
                        <?php
                    }

                    if ($content['use']) {
                        ?>
                        <h2 class="tab-btn<?= $content['use'] && !$content['ingredients'] ? ' vis' : '' ?>"
                            data-tab="1">
                            <span>Способ&nbsp;применения</span>
                        </h2>
                    <?php } ?>
                    <h2 class="tab-btn<?= !$content['ingredients'] && !$content['use'] ? ' vis' : '' ?>" data-tab="2">
                        <span>Отзывы</span>
                    </h2>
                </div>
                <div>
                    <p class="use_made_underine"></p>
                    <?php if ($content['ingredients']) { ?>
                        <div class="tab-content tab0"><?= $content['ingredients'] ?></div>
                        <?php
                    }

                    if ($content['use']) {
                        ?>
                        <div class="tab-content tab1<?= $content['use'] && $content['ingredients'] ? ' hidden' : '' ?>"><?= $content['use'] ?></div>
                    <?php } ?>
                    <div class="tab-content tab2<?= $content['use'] || $content['ingredients'] ? ' hidden' : '' ?>">
                        <div class="row">
                            <div class="col-xs-12">
                                <a
                                        href="#"
                                        class="col-xs-2 reviw_link_tag"
                                        data-name="<?= h($content['name']) ?>"
                                        data-id="<?= $content['id'] ?>"
                                        title="Оставить отзыв о товаре">Отзыв о товаре</a>
                            </div>
                        </div>
                        <?php if ($reviews) { ?>
                            <div class="row">
                                <?php Template::partial('product_reviews', [
                                    'reviews' => $reviews,
                                    'product_name' => $content['name'],
                                    'month' => $month,
                                ]) ?>
                            </div>
                            <?php if (count($reviews) > 1) { ?>
                                <div class="prod_see_all">
                                    <a class="hidden_link" href="#" rel="nofollow"
                                       data-link="<?= Template::hidelink('/prod_review/' . $content['id']) ?>"
                                       target="_blank">Все&nbsp;отзывы</a>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="found-lower-modal" ng-controller="found-lower-price">
        <div class="bg"></div>
        <div class="modal">
            <div class="holder">
                <a href="#" class="close" title="Закрыть">×</a>
                <div class="row">
                    <div class="col-xs-4">
                        <img width="250" height="250" ng-src="{{product.src}}">

                        <div class="badges">
                            <div class="bdg" data-i="1">
                                <a class="hidden_link" data-link="<?= Template::hidelink('/page/delivery') ?>" href="#">
                                    <div class="icn i1"></div>
                                    Всегда поможем<br>с выбором
                                </a>
                            </div>
                            <div class="bdg" data-i="3">
                                <a class="hidden_link" data-link="<?= Template::hidelink('/page/delivery#pay') ?>"
                                   href="#">
                                    <div class="icn i2"></div>
                                    Оплата любым удобным способом
                                </a>
                            </div>
                            <div class="bdg bdg3" data-i="2">
                                <a class="hidden_link" data-link="<?= Template::hidelink('/page/sales') ?>" href="#">
                                    <div class="icn i3"></div>
                                    Гибкая система скидок
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-8">
                        <div class="header2">
                        <span class="price-box">
                            <strong ng-bind-html="product.price"></strong>
                            <span class="rub_h">руб</span>
                            <span class="rub">p</span>
                        </span>
                            <div class="product-tm" ng-bind-html="product.tm"></div>
                            <span class="product-name" ng-bind-html="product.name"></span>
                        </div>

                        <h3>Нашли этот товар дешевле?</h3>

                        <p>
                            Пришлите нам ссылку на этот товар в другом магазине<br>и мы сформируем для Вас
                            индивидуальное предложение!
                        </p>

                        <form>
                            <div class="row i i-name">
                                <div class="col-xs-4">
                                    <label for="found-lower-name">Имя</label>
                                </div>
                                <div class="col-xs-8">
                                    <input id="found-lower-name" type="text" name="name" class="l-name"
                                           required="required">
                                    <div class="error">Пожалуйста, представьтесь</div>
                                </div>
                            </div>
                            <div class="row i i-email">
                                <div class="col-xs-4">
                                    <label for="found-lower-email">E-mail</label>
                                </div>
                                <div class="col-xs-8">
                                    <input id="found-lower-email" type="email" name="email" class="l-email"
                                           required="required">
                                    <div class="error">Введите правильный E-mail</div>
                                </div>
                            </div>
                            <div class="row i i-phone">
                                <div class="col-xs-4">
                                    <label for="found-lower-phone">Телефон</label>
                                </div>
                                <div class="col-xs-8">
                                    <input id="found-lower-phone" type="tel" name="phone" class="l-phone"
                                           required="required">
                                    <div class="error">Введите правильный телефон</div>
                                </div>
                            </div>
                            <div class="row i i-url">
                                <div class="col-xs-4">
                                    <label for="found-lower-url">Ссылка на товар<br>в другом магазине</label>

                                    <div class="oferta-label">* Предложение не является публичной офертой</div>
                                </div>
                                <div class="col-xs-8">
                                    <input id="found-lower-url" type="text" name="url" required="required">
                                    <div class="error">Введите правильный адрес</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-8 col-xs-push-4">
                                    <button type="submit">ОТПРАВИТЬ</button>
                                </div>
                            </div>
                            <input type="hidden" name="lower-price" ng-value="product.id">
                        </form>
                    </div>
                </div>
                <div class="logo-bg"></div>
            </div>
        </div>
    </div>

<?php if ($content['buy_with']['products']) { ?>
    <div class="row recomended_products">
        <div class="product-h">
            <h3>С этим товаром покупают</h3>
        </div>

        <a href="#" id="recommended-prev" class="arrow inactive">&lt;</a>
        <a href="#" id="recommended-next" class="arrow next">&gt;</a>
    </div>
    <div id="recommended-products" class="row ua-product-list" data-list="С этим товаром покупают">
        <div class="recommended-holder">
            <?php
            for ($i = 0, $n = count($content['buy_with']['products']); $i < $n; ++$i) {
                Template::partial('product_in_list', [
                    'is_auth' => $is_auth,
                    'product' => $content['buy_with']['products'][$i],
                ]);
            }
            ?>
        </div>
    </div>
<?php } else { ?>
    <div class="product-h">
        <?php Template::personalized_products('С этим товаром покупают'); ?>
    </div>
    <?php
}

if (!empty($content['similar'])) {
    ?>
    <div class="row active_spec_prod ua-product-list" data-list="Похожие товары">
        <div class="product-h">
            <h3>Похожие товары</h3>
        </div>
        <?php
        for ($i = 0, $n = count($content['similar']); $i < $n; ++$i) {
            Template::partial('product_in_list', [
                'is_auth' => $is_auth,
                'product' => $content['similar'][$i],
            ]);
        }
        ?>
    </div>
    <?php
}

if (!empty($content['line']['products'])) {
    ?>
    <div class="row active_spec_prod ua-product-list" data-list="Для комплексного ухода">
        <div class="product-h">
            <h3>Для комплексного ухода</h3>
        </div>
        <?php for ($i = 0, $n = count($content['line']['products']); $i < $n; ++$i) {
            Template::partial('product_in_list', [
                'is_auth' => $is_auth,
                'product' => $content['line']['products'][$i],
            ]);
        }

        if (!empty($content['line']['info']['name'])) {
            ?>
            <div class="row">
                <div class="col-xs-12 prod_see_all"><a
                            href="/tm/<?= $content['line']['info']['tm_url'] . '/' . $content['line']['info']['url'] ?>"><?= $content['line']['info']['name'] ?></a>
                </div>
            </div>
        <?php } ?>
    </div>
    <?php
}

if ($content['type_page_tms']) {
    ?>
    <div class="row">
        <div class="col-xs-12">
            Категории:
            <?php for ($i = 0, $n = count($content['type_page_tms']); $i < $n; ++$i) { ?>
                <a
                href="/kosmetika/<?= $content['type_page_tms'][$i]['url'] ?>"><?= $content['type_page_tms'][$i]['name'] ?></a><?= $i !== $n - 1 ? ', ' : '' ?>
                <?php
            }
            ?>
        </div>
    </div>
    <?php
}

Template::partial(
    'base_links',
    [
        'links' => $content['base_links'],
    ]
);

Template::partial('product_add_review_box');
