<?php
/**
 * @var array $content
 */
$crumbs = '';

for ($i = 0, $n = count($content['categories']); $i < $n; ++$i) {
    $m = count($content['categories'][$i]['cats']);
?>
    <div class="lvl_3 expandable dropdown-gray-style" itemscope itemtype="http://schema.org/SiteNavigationElement">
        <div class="lvl_3_group <?= $i === $n - 1 ? 'lvl_3_last' : '' ?>">
            <h3 class="lvl_3_group_title"><?= $content['categories'][$i]['sub_name'] ?> <i class="arrow"></i></h3>
            <div
                class="lvl_3_block c<?= $m ?> <?= $i === $n - 1 ? 'lvl_3_last' : '' ?> filters-pad">
                <?php for ($y = 0; $y < $m; ++$y) { ?>
                    <ul>
                        <?php
                        for ($z = 0, $o = count($content['categories'][$i]['cats'][$y]); $z < $o; ++$z) {
                            if ($content['categories'][$i]['cats'][$y][$z]['id'] == $content['category']['id']) {
                                $crumbs .= $content['categories'][$i]['sub_name'] . ' / ' . $content['categories'][$i]['cats'][$y][$z]['name'] . ' / ';
                            }
                        ?>
                            <li <?= $content['categories'][$i]['cats'][$y][$z]['id'] == $content['category']['id'] ? 'class="lvl_3_active"' : '' ?>>
                                <a href="/category-tm/<?= $content['subcategory_url'] . '/' . $content['categories'][$i]['cats'][$y][$z]['url'] . '/' . $content['tm']['url'] ?>">
                                    <?= $content['categories'][$i]['cats'][$y][$z]['name'] ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                <?php } ?>
            </div>
        </div>
    </div>
<?php
}
