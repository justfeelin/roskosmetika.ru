<div class="modal fade" id="pre-order-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="true">
    <div class="modal-dialog modal__frame">
        <div class="modal-content modal__content">
            <form class="form-horizontal" role="form">
                <div class="modal-header modal__header">
                    <button type="button" class="close modal__close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <p class="modal-title modal__title pre_order_font" id="myModalLabel">
                        Заявка на информирование о наличии товара
                    </p>
                </div>
                <div class="modal-body modal__body">
                    <div class="form-group modal__row">
                        <div class="col-xs-9 pre_p_name"></div>
                        <div class="col-xs-3 pre_p_img"><img width="85" height="85" src=""></div>
                    </div>
                    <div class="form-group modal__row">
                        <label for="fh_name" class="col-xs-4 control-label modal__label">Как Вас зовут? -<span class="form-required">*</span></label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control l-name form-name" id="fh_name">
                        </div>
                    </div>
                    <div class="form-group modal__row">
                        <label for="fh_email" class="col-xs-4 control-label modal__label">Email:<span class="form-required">*</span></label>
                        <div class="col-xs-8">
                            <input type="email" class="form-control l-email form-email" id="fh_email">
                        </div>
                    </div>
                    <div class="form-group modal__row">
                        <label for="fh_phone" class="col-xs-4 control-label modal__label">Номер телефона:<span class="form-required">*</span></label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control l-phone form-phone" id="fh_phone">
                        </div>
                    </div>
                    <input type="hidden" name="id" id="p_id">
                    <div class="row modal__row">
                        <div class="col-xs-12  modal__notes">Мы сообщим Вам о поступлении товара в продажу по электронной почте или смс.<br><span class="form-required">*</span> - обязательные поля</div>
                    </div>
                    <div class="form-group modal__row pre_order_center form-check">
                        <div class="col-xs-12  modal__notes">Как Вам сообщить о наличии товара?</div>
                        <div class="col-xs-4 col-xs-offset-2">
                            <input type="checkbox" class="form-check-email" id="fh_check_email" checked>
                            <label for="fh_check_email" class="modal__label"> по email</label>
                        </div>
                        <div class="col-xs-4">
                            <input type="checkbox" class="form-check-sms" id="fh_check_sms">
                            <label for="fh_check_sms" class="modal__label"> по СМС</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer modal__footer pre_order_center">
                    <button type="submit" class="btn btn-primary button-common" name="submit">Сообщить о поступлении</button>
                    <p class="hurry_notice_info">Нажимая на кнопку «Задать вопрос», вы соглашаетесь на обработку персональных данных в соответствии с <a class="oferta_link" href="/page/uslovija-obrabotki-personalynih-dannyh" target="_blank">условиями</a>.</p>
                </div>
            </form>
        </div>
    </div>
</div>
