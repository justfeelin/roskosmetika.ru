<?php
/**
 * @var string $page
 * @var string $incartlink
 */
?>
<div class="footer_background">
    <div class="container footer" itemscope itemtype="http://schema.org/WPFooter">
        <div class="row">
            <div class="col-xs-2 foter_links">
                <div class="footer_title">О&nbsp;нас</div>
                <div><a href="/page/contacts">Контакты</a></div>
                <div><a href="/review">Отзывы&nbsp;о&nbsp;магазине</a></div>
                <div><a href="/feedback">Обратная&nbsp;связь</a></div>
                <div><a href="/page/requisites">Реквизиты</a></div>
            </div>
            <div class="col-xs-3 foter_links">
                <div class="footer_title">Оформление&nbsp;заказа</div>
                <div><a href="/page/order">Как сделать&nbsp;заказ</a></div>
                <div><a href="/page/delivery">Способы&nbsp;оплаты</a></div>
                <div><a href="/page/delivery">Доставка&nbsp;и&nbsp;самовывоз</a></div>
                <div><a href="/page/vozvrat-tovara">Возврат&nbsp;товара</a></div>
            </div>
            <div class="col-xs-2 foter_links">
                <div class="footer_title">Информация</div>
                <div><a href="/articles">Статьи</a></div>
                <div><a href="/sitemap">Карта сайта</a></div>
                <div><a href="/master_classes">Мастер-классы</a></div>
                <div><a href="/question">Вопросы&nbsp;косметологу</a></div>
                <div><a href="/country">Страны&nbsp;-&nbsp;производители</a></div>
                <div><a href="/page/uslovija-obrabotki-personalynih-dannyh">Обработка&nbsp;персональных&nbsp;данных</a></div>
                <div><a href="/landing/kak-umenshit-objem-talii">Как&nbsp;уменьшить&nbsp;талию?</a></div>
                <div>
                    <a href="/kosmetika">Все типы косметики</a>
                </div>
            </div>
            <div class="col-xs-5">
                <div class="footer_soc">
                        <a href="https://www.youtube.com/channel/UCj_A0d-kX5x9KyQJHaBje5A/videos" rel="nofollow"
                           class="footer_yt" target="_blank"></a>
                        <a href="https://www.facebook.com/roskosmetika/" rel="nofollow"
                           class="footer_fb" target="_blank"></a>
                        <a href="https://twitter.com/ROSKOSMETIKAru" rel="nofollow" class="footer_tw" target="_blank"></a>
                        <a href="https://www.instagram.com/roskosmetika_official/" rel="nofollow"
                           class="footer_inst" target="_blank"></a>
                        <a href="https://ok.ru/group/53145173885166" rel="nofollow" class="footer_ok"
                           target="_blank"></a>
                        <a href="https://vk.com/club21757533" rel="nofollow" class="footer_vk" target="_blank"></a>
                        <a href="http://www.linkedin.com/pub/%D1%80%D0%BE%D1%81%D0%BA%D0%BE%D1%81%D0%BC%D0%B5%D1%82%D0%B8%D0%BA%D0%B0-%D0%BA%D0%BE%D1%81%D0%BC%D0%B5%D1%82%D0%B8%D0%BA%D0%B0/99/a41/975" rel="nofollow"
                           class="footer_li" target="_blank"></a>
                        <a href="https://plus.google.com/u/0/111185151723588230627/posts" rel="nofollow" class="footer_goo"
                           target="_blank"></a>
                </div>
                <div class="ya_market">
                    <a <?= $incartlink ?> href="https://clck.yandex.ru/redir/dtype=stred/pid=47/cid=2508/*https://market.yandex.ru/shop/50173/reviews" rel="nofollow"
                       target="_blank"
                       class="hidden_link"><img
                            src="https://clck.yandex.ru/redir/dtype=stred/pid=47/cid=2505/*https://grade.market.yandex.ru/?id=50173&action=image&size=0"
                            border="0" width="88" height="31"
                            alt="Читайте отзывы покупателей и оценивайте качество магазина на Яндекс.Маркете"/></a>
                </div>
            </div>
        </div>
        <div class="row" itemprop="creator" itemscope itemtype="http://schema.org/Organization">
            <div class="col-xs-6 copyright">
                <div><?= date('Y') ?>&nbsp;&#169;&nbsp;&laquo;Интернет магазин косметики <span itemprop="name">Роскосметика</span>&raquo;. Все&nbsp;права&nbsp;защищены.
                </div>
                <div>При использовании материалов с сайта активная ссылка на первоисточник обязательна.</div>
                <div>Вся представленная на сайте информация не является публичной офертой.</div>
            </div>
            <div class="col-xs-3">
                <a href="tel:+7(495)662-58-44" class="second_phone">+7 (495) 662-58-44 Москва</a>
                <a href="tel:+7(812)385-55-81" class="second_phone">+7 (812) 385-55-81 Питер</a>
            </div>
            <div class="col-xs-3 footer_phone">
                <a href="tel:8-800-775-54-83" class="phone_number" itemprop="telephone">8&nbsp;800&nbsp;775-54-83</a>
                <div class="footer_phone_tag">Бесплатно&nbsp;по&nbsp;России</div>
                <meta itemprop="address" content="Москва ул. Василия Петушкова д.8"/>
            </div>
        </div>
    </div>
</div>
