<?php
/**
 * @var string $header
 * @var int $active_aside
 * @var array $content
 */
?>
<section class="room">
    <div class="row">
        <h1 class="col-xs-6 col-xs-offset-3"><?= $header ?></h1>
    </div>
    <div class="row">
        <?php
        Template::partial(
            'room_left_menu',
            [
                'active_aside' => $active_aside,
            ]);
        ?>
        <div class="col-xs-8">
            <?php
            if (!empty($content['orders'])) {
                for ($i = 0, $n = count($content['orders']); $i < $n; ++$i) {
            ?>
                    <div class="histiry_order">
                        <div class="row order_info">
                            <div class="col-xs-7">
                                <div class="carrier_info">
                                    <p>
                                        Заказ № 
                                        <b>Р-<?= $content['orders'][$i]['site_order_number'] ?></b>
                                    </p>

                                    <?php
                                    if ($content['orders'][$i]['carrier_name'] || $content['orders'][$i]['shipping_id']) {
                                    ?><p>
                                            Доставка:
                                            <b><?php       
                                        if ($content['orders'][$i]['carrier_name']) {
                                            echo $content['orders'][$i]['carrier_name'];        
                                        } elseif ($content['orders'][$i]['shipping_id']) {
                                    ?>
                                        &laquo;<?= Orders::$shippings[$content['orders'][$i]['shipping_id']] ?>&raquo;
                                        <?php } ?>
                                        </b> 
                                    </p>

                                    <?php 
                                        if  ($content['orders'][$i]['carrier_number'] != '') { ?>
                                        <p>Трек-код:
                                            <b><?php echo $content['orders'][$i]['carrier_number']; ?></b>

                                            <?php
                                            switch ((int)$content['orders'][$i]['carrier_id']) {
                                                case 1:
                                            ?>
                                                    <a href="http://www.dellin.ru/tracker/orders/<?= $content['orders'][$i]['carrier_number'] ?>/" target="_blank">Отследить</a>
                                            <?php
                                                    break;

                                                case 2:
                                            ?>
                                                    <a href="https://cabinet.jde.ru/index.html" target="_blank">Отследить</a>
                                            <?php
                                                    break;

                                                case 3:
                                                case 4:
                                            ?>
                                                    <a href="https://www.pochta.ru/tracking#<?= $content['orders'][$i]['carrier_number'] ?>" target="_blank">Отследить</a>
                                            <?php
                                                    break;

                                                case 6:
                                            ?>
                                                    <a href="https://www.baikalsr.ru/tools/tracking/?number=<?= rawurlencode($content['orders'][$i]['carrier_number']) ?>" target="_blank">Отследить</a>
                                            <?php
                                                    break;

                                                case 7:
                                            ?>
                                                    <a href="https://kabinet.pecom.ru/status" target="_blank">Отследить</a>
                                            <?php
                                                    break;

                                                case 8:
                                            ?>
                                                    <a href="http://www.rateksib.ru/mestonahozhdenie_gruza" target="_blank">Отследить</a>
                                            <?php
                                                    break;

                                                case 9:
                                                    ?>
                                                    <a href="http://tk-kit.ru" target="_blank">Отследить</a>
                                            <?php
                                                    break;

                                                case 14:
                                            ?>
                                                <a href="http://www.edostavka.ru/track.html?order_id=<?= $content['orders'][$i]['carrier_number'] ?>#track-form" target="_blank">Отследить</a>
                                            <?php
                                                    break;

                                                case 15:
                                            ?>
                                                <a href="https://pickpoint.ru/monitoring/" target="_blank">Отследить</a>
                                                <?php

                                                    break;
                                                ?>
                                            <?php } ?>
                                        </p>    
                                    <?php
                                        }
                                    
                                        if (!is_null($content['orders'][$i]['delivery_date'])) {
                                    ?>
                                            <p>Дата отгрузки:
                                                <b><?= Template::date('d.m.Y', $content['orders'][$i]['delivery_date']) ?></b>
                                            </p>
                                        <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col-xs-5">
                                <p class="align_left">
                                    Дата&nbsp;заказа:&nbsp;<b><?= Template::date('d.m.Y', $content['orders'][$i]['added_date']) ?></b>
                                </p>
                                <?php
                                $statuses = [
                                    Orders::STATUS_ID_NEW => [
                                        'label' => 'Новый',
                                        'class' => 'font_warning',
                                    ],
                                    Orders::STATUS_ID_CURRENT => [
                                        'label' => 'Исполняется',
                                        'class' => 'font_warning',
                                    ],
                                    Orders::STATUS_ID_DONE => [
                                        'label' => 'Выполнен',
                                        'class' => 'font_ok',
                                    ],
                                    Orders::STATUS_ID_RETURN => [
                                        'label' => 'Возврат',
                                        'class' => 'font_accent',
                                    ],
                                    Orders::STATUS_ID_CANCEL => [
                                        'label' => 'Отменен',
                                        'class' => 'font_accent',
                                    ],
                                    Orders::STATUS_ID_ON_STOCK => [
                                        'label' => 'Собирается',
                                        'class' => 'font_warning',
                                    ],
                                    Orders::STATUS_ID_STOCK_DONE => [
                                        'label' => 'Собран',
                                        'class' => 'font_ok',
                                    ],
                                    Orders::STATUS_ID_DELIVERY => [
                                        'label' => 'Доставляется',
                                        'class' => 'font_warning',
                                    ],
                                    Orders::STATUS_ID_CORRUPT => [
                                        'label' => 'Уточняется',
                                        'class' => 'font_warning',
                                    ],
                                    Orders::STATUS_ID_WAITING => [
                                        'label' => 'Уточняется',
                                        'class' => 'font_warning',
                                    ],
                                ];

                                $statuses[Orders::STATUS_ID_CANCEL_CLIENT] = $statuses[Orders::STATUS_ID_CANCEL];

                                if (isset($statuses[$content['orders'][$i]['status_id']])) {
                                ?>
                                    <p class="align_left">Статус&nbsp;заказа:&nbsp;<span
                                            class="<?= $statuses[$content['orders'][$i]['status_id']]['class'] ?>"><?= $statuses[$content['orders'][$i]['status_id']]['label'] ?></span>
                                    </p>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="room_header">
                            <div class="row">
                                <div class="col-xs-6 ">Товар</div>
                                <div class="col-xs-2 align_center">Цена</div>
                                <div class="col-xs-2 align_center">Кол-во</div>
                                <div class="col-xs-2 align_center">Итого</div>
                            </div>
                        </div>
                        <?php for ($y = 0, $m = count($content['orders'][$i]['items']); $y < $m; ++$y) { ?>
                            <div class="row cart_item">
                                <a href="/product/<?= $content['orders'][$i]['items'][$y]['url'] ?: $content['orders'][$i]['items'][$y]['id']?>"
                                   class="col-xs-2 cart_photo"><img width="85" height="85"
                                                                    src="<?= IMAGES_DOMAIN_FULL ?>/images/prod_photo/<?= Product::img_url('min_' . $content['orders'][$i]['items'][$y]['src'], $content['orders'][$i]['items'][$y]['url']) ?>"></a>

                                <div class="col-xs-6 cart_desc">
                                    <a href="/product/<?= $content['orders'][$i]['items'][$y]['url'] ?: $content['orders'][$i]['items'][$y]['id'] ?>"
                                       class="link"><?= $content['orders'][$i]['items'][$y]['name'] ?></a>

                                    <p class="cart_pack"><?= $content['orders'][$i]['items'][$y]['pack'] ?></p>
                                </div>
                                <div class="col-xs-2 cart_quan">
                                    <?php if ($content['orders'][$i]['items'][$y]['old_price'] > $content['orders'][$i]['items'][$y]['price']) { ?>
                                        <div class="old-price"><?= formatPrice($content['orders'][$i]['items'][$y]['old_price']) ?>
                                            <span class="rub">p</span></div>
                                    <?php } ?>
                                    <p class="<?= $content['orders'][$i]['items'][$y]['old_price'] > $content['orders'][$i]['items'][$y]['price'] ? 'special-price' : '' ?>"><?= formatPrice($content['orders'][$i]['items'][$y]['price']) ?>
                                        <span class="rub">p</span>
                                        <?php if ($content['orders'][$i]['items'][$y]['old_price'] > $content['orders'][$i]['items'][$y]['price']) { ?>
                                            (-<?= 100 - (int)(((float)$content['orders'][$i]['items'][$y]['price'] / (float)$content['orders'][$i]['items'][$y]['old_price']) * 100) ?>%)
                                        <?php } ?>
                                    </p>
                                </div>
                                <div class="col-xs-2 cart_quan"><?= $content['orders'][$i]['items'][$y]['quantity'] ?></div>
                                <div class="col-xs-2 cart_quan">
                                    <p>
                                        <?= formatPrice($content['orders'][$i]['items'][$y]['sum']) ?>
                                        <span class="rub">p</span></p>
                                </div>
                            </div>
                            <?php
                        }

                        if ($content['orders'][$i]['promo_discount']) {
                        ?>
                            <div class="row sum">
                                <div class="col-xs-2 col-xs-offset-8">
                                    Промо-код
                                </div>
                                <div class="col-xs-2">
                                    <?= $content['orders'][$i]['promo_discount'] . ($content['orders'][$i]['promo_discount'] < 0 ? ' <span class="rub">p</span>' : '%') ?>
                                </div>
                            </div>
                        <?php
                        }

                        if ($content['orders'][$i]['delivery'] !== null) {
                        ?>
                            <div class="row sum">
                                <div class="col-xs-2 col-xs-offset-8">
                                    Доставка
                                </div>
                                <div class="col-xs-2">
                                    <?php if ($content['orders'][$i]['delivery']) { ?>
                                        <?= formatPrice($content['orders'][$i]['delivery']) ?> <span class="rub">p</span>
                                    <?php } else { ?>
                                        <b>Бесплатно</b>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="row sum">
                            <div class="col-xs-6">
                                <?php
                                    if ($content['orders'][$i]['certificates']) {
                                        for ($y = 0, $m = count($content['orders'][$i]['certificates']); $y < $m; ++$y) {
                                ?>
                                            <a target="_blank" href="/<?= promo_codes::CERTIFICATE_DIR . '/' . rawurlencode($content['orders'][$i]['certificates'][$y]['code'] . '.jpg') ?>">Скачать сертификат<?= $m === 1 ? '' : ' (' . ($y + 1) . ')' ?></a>
                                <?php
                                        }
                                }
                                ?>
                            </div>
                            <div class="col-xs-2 col-xs-offset-2">
                                Итого
                            </div>
                            <div class="col-xs-2">
                                <?= formatPrice($content['orders'][$i]['cost']) ?> <span class="rub">p</span>
                            </div>
                        </div>
                        <?php if ($content['orders'][$i]['show_payment'] && $content['orders'][$i]['cost'] != $content['orders'][$i]['to_pay']) { ?>
                            <div class="row sum">
                                <div class="col-xs-2 col-xs-offset-8">
                                    Оплачено
                                </div>
                                <div class="col-xs-2">
                                    <?= formatPrice($content['orders'][$i]['cost'] - $content['orders'][$i]['to_pay']) ?> <span class="rub">p</span>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="row bottom-links">
                            <div class="col-xs-3">
                                <?php if ($content['orders'][$i]['status_id'] != Orders::STATUS_ID_NEW && $content['orders'][$i]['status_id'] != Orders::STATUS_ID_CURRENT) { ?>
                                    <a href="/room/repeat/<?= $content['orders'][$i]['order_number'] ?>" class="order_repeat">повторить<span
                                            class="pointer_cart"></span></a>
                                <?php } ?>
                            </div>
                            <div class="col-xs-9 align_right">
                                <?php if ($content['orders'][$i]['status_id'] == Orders::STATUS_ID_NEW) { ?>
                                    <a href="/room/cancel/<?= $content['orders'][$i]['order_number'] ?>" class="order_cancel">отменить<span
                                            class="pointer_cart"></span></a>
                                <?php } ?>

                                <?php
                                if ($content['orders'][$i]['show_load_bill']) {
                                ?>
                                        <a href="/room/load_bill/<?= $content['orders'][$i]['order_number'] ?>" class="load_bill"
                                           target="_blank">Скачать&nbsp;счет<span
                                                class="pointer_cart"></span></a>
                                <?php
                                }

                                if ($content['orders'][$i]['show_payment']) {
                                    Template::partial(
                                        'sberbank_form',
                                        [
                                            'order' => array_merge(
                                                $content['orders'][$i],
                                                [
                                                    'cost' => $content['orders'][$i]['to_pay'],
                                                ]
                                            ),
                                            'label' => $content['orders'][$i]['cost'] != $content['orders'][$i]['to_pay'] ? 'Доплатить' : null,
                                        ]
                                    );
                                }
                                //Change
                                if ((!is_null ($content['orders'][$i]['fact_paid'])) && $content['orders'][$i]['status_id'] == Orders::STATUS_ID_CURRENT) {
                                ?>
                                    <span class="order-paid">Оплачено <?= formatPrice((int)$content['orders'][$i]['fact_paid']) ?> <span class="rub">p</span></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
            <?php
                }
            } else {
            ?>
                <div class="no_items">У&nbsp;Вас&nbsp;не&nbsp;было&nbsp;заказов.</div>
            <?php } ?>
        </div>
        <div class="col-xs-1">
        </div>
    </div>
    <?php Template::partial('mind_box', [
                    'mb_is_auth' => $is_auth,
                    'mb_top_view' => $mb_top_view,
                ]); ?>
</section>
