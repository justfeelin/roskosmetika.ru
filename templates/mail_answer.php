<?php
/**
 * @var array $content
 */
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <h1 style="font-family: Tahoma, Geneva, sans-serif;font-size: 16px;color: #1B1B1B;margin-top: 10px;margin-bottom: 20px;">
                Здравствуйте<?= $content['name'] ? ', ' . $content['name'] : '' ?></h1>

            <h2 style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 0;margin-bottom: 10px;">
                Вы задавали вопрос на сайте <a target="_blank"
                                               href="<?= DOMAIN_FULL ?>/"><?= DOMAIN ?></a>:
            </h2>

            <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #A6A6A6;margin-top: 6px;margin-bottom: 0;font-style: italic;"><?= $content['question'] ?></p>


            <h2 style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 30px;margin-bottom: 20px;">
                Ответ косметолога Светланы Юдиной:</h2>

            <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 0;margin-bottom: 0;"><?= $content['answer'] ?></p>

            <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #575757;margin-top: 30px;margin-bottom: 12px;">
                <a target="_blank" href="<?= DOMAIN_FULL ?>/question">Все вопросы
                    косметологу</a>
            </p>
        </td>
    </tr>
</table>
