<?php
/**
 * @var string $products_title
 * @var array $items
 * @var int $discount
 * @var string $discount_type
 * @var int $promo_discount
 * @var int $shipping_price
 * @var string $shipping_name
 * @var int $final_sum
 */
$inActions = false;
?>
<table width="600" border="0" cellspacing="0" cellpadding="0">
    <tr height="28">
        <td width="380" bgcolor="#8dc655"
            style="padding-left:10px; font-family: Tahoma, Geneva, sans-serif;  font-size: 13px; color: #FFF; text-align: left; text-decoration: none; vertical-align: middle;">
            <?= !empty($products_title) ? $products_title : 'Приобретённый товар' ?>
        </td>
        <td width="80" bgcolor="#8dc655"
            style="text-align: center;font-family: Tahoma, Geneva, sans-serif;  font-size: 13px; color: #FFF; text-decoration: none; vertical-align: middle;">
            Цена
        </td>
        <td width="60" bgcolor="#8dc655"
            style="text-align: center;font-family: Tahoma, Geneva, sans-serif;  font-size: 13px; color: #FFF; text-decoration: none; vertical-align: middle;">
            Кол-во
        </td>
        <td width="80" bgcolor="#8dc655"
            style="text-align: center;font-family: Tahoma, Geneva, sans-serif; font-size: 13px; color: #FFF; text-decoration: none; vertical-align: middle;">
            Итого
        </td>
    </tr>
    <?php
    for ($i = 0, $n = count($items); $i < $n; ++$i) {
        if ($items[$i]['special'] && !$inActions) {
            $inActions = true;

            if ($i > 0) {
    ?>
                <tr>
                    <td colspan="4">
                        <h3 style="color: #000000; font-size: 18px; font-weight: normal; margin: 15px 0; text-align: center;">
                            Товары по акции</h3>
                    </td>
                </tr>
    <?php
            }
        }
    ?>
        <tr>
            <td width="380" style="padding-left:10px; padding-right:10px">
                <table width="380" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="80"><p
                                style="margin-top: 12px;margin-bottom: 6px;"><a
                                    href="<?= DOMAIN_FULL ?>/product/<?= apply_utm($items[$i]['url'], 'product-logo') ?>"
                                    target="_blank"><img
                                        src="<?= IMAGES_DOMAIN_FULL ?>/images/prod_photo/<?= Product::img_url('min_' . $items[$i]['src'], $items[$i]['url']) ?>"
                                        width="85" height="85"
                                        alt="<?= $items[$i]['name'] ?>"/></a></p></td>
                        <td width="300"><p
                                style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 6px;">
                                <a href="<?= DOMAIN_FULL ?>/product/<?= apply_utm($items[$i]['url'], 'product-name') ?>"
                                   style="color: #9b1d97;"
                                   target="_blank"><?= $items[$i]['name'] ?></a>,&nbsp;<?= $items[$i]['pack'] ?>
                            </p>

                            <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 10px;color: #1B1B1B;margin-top: 6px;margin-bottom: 6px;">
                                Артикул:&nbsp;<?= $items[$i]['id'] ?>
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="80" style="text-align: center;">
                <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 6px;">
                    <?php if ($items[$i]['oldPrice'] !== $items[$i]['price']) { ?>
                        <span style="font-size: 12px; text-decoration: line-through"><?= formatPrice($items[$i]['oldPrice']) ?>&nbsp;р.</span><br>
                    <?php } ?>
                    <span style="font-size: 12px;<?= $items[$i]['oldPrice'] !== $items[$i]['price'] ? 'color: red' : '' ?>"><?= formatPrice($items[$i]['price']) ?>&nbsp;р.</span></p>
            </td>
            <td width="60" style="text-align: center;">
                <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 6px;"><?= $items[$i]['quantity'] ?></p>
            </td>
            <td width="80" style="text-align: center;">
                <div style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 6px;"><?= formatPrice($items[$i]['sum']) ?>&nbsp;р.
                    <?php if ($items[$i]['oldPrice'] !== $items[$i]['price']) { ?>
                        <div style="font-family: Tahoma, Geneva, sans-serif;font-size: 12px;margin-top: 2px;height: 11px;line-height: 11px; font-style: italic; color:red">-<?= (int)(100.0 - $items[$i]['price'] / $items[$i]['oldPrice'] * 100.0) ?>%</div>
                    <?php } ?>
                </div>
            </td>
        </tr>
    <?php
    }

    if ($discount > 0) {
    ?>
        <tr>
            <td colspan="4">
                <p style="font-family: Tahoma, Geneva, sans-serif; font-size: 12px; color: #404040; margin-top: 10px; margin-bottom: 2px; text-align: left;">
                    Скидка до <b><?= $discount ?>%</b>: <?= $discount_type ?>
                </p>
            </td>
        </tr>
    <?php
    }

    if ($promo_discount) {
    ?>
        <tr>
            <td style="padding-right:10px">
                <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 0px;margin-bottom: 2px; text-align: right">Купон:</p>
            </td>
            <td colspan="3">
                <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 0px;margin-bottom: 2px; text-align: right">
                    Скидка до <b><?= $promo_discount . ($promo_discount > 0 ? '%' : ' руб.') ?></b></p>
            </td>
        </tr>
    <?php
    }

    if ($shipping_price !== null) {
    ?>
        <tr>
            <td style="padding-right:10px">
                <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 0;margin-bottom: 2px; text-align: right">
                    Доставка:</p>
            </td>
            <td colspan="3">
                <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 0;margin-bottom: 2px; text-align: right; white-space: nowrap"><?= $shipping_name . ($shipping_name && $shipping_price !== null ? ', ' : '') . ($shipping_price !== null ? '<b>' . ($shipping_price ? formatPrice($shipping_price) . ' р.' : 'Бесплатно') . '</b>' : '') ?>
                </p>
            </td>
        </tr>
    <?php } ?>
    <tr>
        <td style="padding-right:10px">
            <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 8px; text-align: right">
                <?= $shipping_price === null ? 'Итого' : 'Стоимость заказа' ?>:</p>
        </td>
        <td colspan="3">
            <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 8px; text-align: right">
                <b><?= formatPrice($final_sum) ?>&nbsp;р.</b></p>
        </td>
    </tr>

    <?php if ($shipping_info === null) { ?>
    <tr>
        <td style="padding-right:10px">
            <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 11px;color: #1B1B1B;margin-top: 10px;margin-bottom: 8px; text-align: right">
                <b>Срок сборки и доставки:</b></p>
        </td>
        <td colspan="3">
            <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 11px;color: #1B1B1B;margin-top: 10px;margin-bottom: 8px; text-align: right">
            <?= $duration ?>
            </p>
        </td>
    </tr>
    <?php } else { ?>
    <tr>
        <td colspan="4" style="padding-right:10px">
            <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 11px;color: #1B1B1B;margin-top: 10px;margin-bottom: 8px; text-align: left">
                <b>Информация о доставке: </b><?= $shipping_info ?></p>
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <p style="font-family: Tahoma, Geneva, sans-serif;font-size: 11px;color: #1B1B1B;margin-top: 6px;margin-bottom: 6px; font-style: italic;">
                Подробную информацию о стоимости доставки Вы можете посмотреть в разделе  <a href="https://www.roskosmetika.ru/page/delivery" target="_blank">"Оплата и доставка"</a>.
            </p>
        </td>
    </tr>
    <?php } ?>
</table>