<?php
/**
 * @var array $content
 */
?>
<a class="btn btn-primary" href="/admin/seo/type_pages/new">Создать</a>
<?php if ($content['items']) { ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>URL</th>
                <th>Название</th>
                <th>H1</th>
            </tr>
        </thead>
        <tbody>
            <?php for ($i = 0, $n = count($content['items']); $i < $n; ++$i) { ?>
                <tr>
                    <td>
                        <a href="/admin/seo/type_pages/<?= $content['items'][$i]['id'] ?>"><?= $content['items'][$i]['id'] ?></a>
                    </td>
                    <td>
                        <?= h($content['items'][$i]['url']) ?>
                    </td>
                    <td>
                        <?= h($content['items'][$i]['name']) ?>
                    </td>
                    <td>
                        <?= h($content['items'][$i]['h1']) ?>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
<?php
}
