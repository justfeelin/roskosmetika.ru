<?php
/**
 * @var string $header
 * @var array $Pagination
 * @var array $all_comments
 * @var string $bringing_date
 */
$paginationHTML = Template::get_tpl(
    'pagination',
    [
        'Pagination' => $Pagination,
    ]
);
?>
<h1><?= $header ?></h1>

<?= $paginationHTML ?>

<table class="table table-striped">
    <thead>
    <tr>
        <th><p><b>Дата</b></p></th>
        <th><p><b>Автор</b></p></th>
        <th><p><b>Город</b></p></th>
        <th><p><b>Комментарий</b></p></th>
        <th><p><b>Ответ</b></p></th>
        <th><p><b>Видимость</b></p></th>
        <th><p><b>Удаление</b></p></th>
    </tr>
    </thead>
    <tbody>
    <?php for ($i = 0, $n = count($all_comments); $i < $n; ++$i) { ?>
        <tr>
            <td><?= Template::date($bringing_date, $all_comments[$i]['time_message']) ?></td>
            <td><a href="/admin/users/<?= $all_comments[$i]['client_id'] ?>"><?= $all_comments[$i]['surname'] . ' ' . $all_comments[$i]['name'] . ' ' . $all_comments[$i]['patronymic'] ?></a></td>
            <td>
                <a href="/admin/review/edit_city/<?= $all_comments[$i]['id'] ?>"><?= $all_comments[$i]['city'] ?: $all_comments[$i]['region_name'] ?></a>
            </td>
            <td><a href="/admin/review/edit/<?= $all_comments[$i]['id'] ?>"><?= $all_comments[$i]['comment'] ?></a></td>
            <td><a href="/admin/review/edit_answer/<?= $all_comments[$i]['id'] ?>"><?= $all_comments[$i]['answer'] ?: 'Написать' ?></a>
            </td>
            <td><a href="/admin/review/change_visible/<?= $all_comments[$i]['id'] ?>"><?= $all_comments[$i]['visible'] ? 'скрыть' : 'отобразить' ?></a>
            </td>
            <td><a href="/admin/review/delete_comment/<?= $all_comments[$i]['id'] ?>">Удалить</a></td>
        </tr>
    <?php } ?>
    </tbody>
</table>

<?php
echo $paginationHTML;
