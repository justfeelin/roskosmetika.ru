<?php
/**
 * @var string $msg
 * @var array $content
 */
if (isset($msg)) {
?>
<div class="alert alert-block"><?= $msg ?></div>
<?php } ?>
<form action="/admin/export/categories" method="POST">
    <div class="row">
        <div class="form-group">
            <label for="categoryID">Категория</label>
            <select id="categoryID" name="categoryID" class="form-control">
                <option value="">--</option>
                <?php foreach ($content['categories']['lvl_1'] as $lvl1) { ?>
                    <option value="<?= $lvl1['id'] ?>"><?= $lvl1['name'] ?></option>
                    <?php
                    if (!empty($content['categories']['lvl_2'][$lvl1['id']])) {
                        foreach ($content['categories']['lvl_2'][$lvl1['id']] as $lvl2Group) {
                    ?>
                            <option disabled="disabled"><?= str_repeat('&nbsp;', 3) . $lvl2Group['name'] ?></option>
                            <?php foreach ($lvl2Group['cats'] as $lvl2) { ?>
                                <option value="<?= $lvl2['id'] ?>"><?= str_repeat('&nbsp;', 6) . $lvl2['name'] ?></option>
                                <?php
                                if (!empty($content['categories']['lvl_3'][$lvl2['id']])) {
                                    foreach ($content['categories']['lvl_3'][$lvl2['id']] as $lvl3Group) {
                                ?>
                                        <option disabled="disabled"><?= str_repeat('&nbsp;', 9) . $lvl3Group['name'] ?></option>
                                        <?php foreach ($lvl3Group['cats'] as $lvl3) { ?>
                                            <option
                                                value="<?= $lvl3['id'] ?>"><?= str_repeat('&nbsp;', 12) . $lvl3['name'] ?></option>
                                            <?php
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                ?>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-3">
            <span class="help-block"><strong>Описания категорий</strong></span>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='desc'> без описания
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='synonym'> без синонимов
                </label>
            </div>
        </div>

        <div class="col-xs-3">
            <span class="help-block"><strong>Описания страниц</strong></span>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='title'> без &lt;title&gt;
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='seo_desc'> без &lt;description&gt;
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='keywords'> без &lt;keywords&gt;
                </label>
            </div>
        </div>

        <div class="col-xs-3">
            <span class="help-block"><strong>Связи</strong></span>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='filters'> без фильтров
                </label>
            </div>
        </div>
    </div>
    <br>
    <br>

    <div class="row">
        <button type="submit" class="btn btn-primary" name="choise">Загрузить выборку</button>
        <button type="submit" class="btn btn-primary" name="all">Загрузить все</button>
    </div>
</form>
<script>
    $(function () {
        $('#categoryID')
            .select2();
    });
</script>
