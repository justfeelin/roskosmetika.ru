<?php
/**
 * @var array $content
 * @var array $Pagination
 */
?>
<form method="GET">
    <div class="form-group">
        <div class="radio">
            <label>
                <input type="radio" name="read" value="1"<?= $content['search']['read'] === true ? 'checked="checked"' : '' ?>>
                Просмотренные
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="read" value="0"<?= $content['search']['read'] === false ? 'checked="checked"' : '' ?>>
                Непросмотренные
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="read" value=""<?= $content['search']['read'] === null ? 'checked="checked"' : '' ?>>
                -Все-
            </label>
        </div>
    </div>

    <div class="form-group">
        <div class="radio">
            <label>
                <input type="radio" name="answered"
                       value="1"<?= $content['search']['answered'] === true ? 'checked="checked"' : '' ?>>
                Отвеченные
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="answered"
                       value="0"<?= $content['search']['answered'] === false ? 'checked="checked"' : '' ?>>
                Неотвеченные
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="answered"
                       value=""<?= $content['search']['answered'] === null ? 'checked="checked"' : '' ?>>
                -Все-
            </label>
        </div>
    </div>
    <input type="submit" class="btn btn-primary" value="Искать">
</form>

<p>Найдено: <?= $Pagination['count'] ?></p>

<?php
$paginationHTML = Template::get_tpl('pagination', [
    'Pagination' => $Pagination,
]);

echo $paginationHTML;

for ($i = 0, $n = count($content['questions']); $i < $n; ++$i) {
?>
    <div>
        <a href="/admin/questions/<?= $content['questions'][$i]['id'] . $content['back_url_param'] ?>"><?= $content['questions'][$i]['date'] ?></a>
        <?= $content['questions'][$i]['name'] . ', ' . $content['questions'][$i]['city'] . ' [' . $content['questions'][$i]['email'] . ', ' . $content['questions'][$i]['phone'] . ']' ?>
        <p>
            <?= $content['questions'][$i]['question'] ?>
        </p>
        <?= $content['questions'][$i]['answer'] ? '<strong>Есть ответ</strong>' : '<em>Нет ответа</em>' ?>
        <hr>
    </div>
<?php
}

echo $paginationHTML;
