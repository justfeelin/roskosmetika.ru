<?php
/**
 * @var array $content
 */
if ($content['products']) {
    ?>
    <form method="POST" class="products-box">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Название</th>
                <th>Описание</th>
                <?php if ($content['showIngredients']) { ?>
                    <th>
                        Состав
                    </th>
                <?php
                }

                if ($content['certificates']) {
                ?>
                    <th>Сертификаты</th>
                <?php } ?>
                <th>
                    <input type="checkbox" class="check-all" checked="checked">
                </th>
            </tr>
            </thead>
            <tbody>
            <?php for ($i = 0, $n = count($content['products']); $i < $n; ++$i) { ?>
                <tr>
                    <td>
                        <?= $content['products'][$i]['id'] ?>
                    </td>
                    <td>
                        <label for="product-chb-<?= $content['products'][$i]['id'] ?>">
                            <?= $content['products'][$i]['name'] ?>
                        </label>
                    </td>
                    <td>
                        <?= $content['products'][$i]['short_description'] ?>
                    </td>
                    <?php if ($content['showIngredients']) { ?>
                        <td>
                            <?= $content['products'][$i]['ingredients'] ?>
                        </td>
                    <?php
                    }

                    if ($content['certificates']) {
                    ?>
                        <td>
                            <?php
                            if ($content['products'][$i]['certificates']) {
                                for ($y = 0, $m = count($content['products'][$i]['certificates']); $y < $m; ++$y) {
                            ?>
                                <a href="/admin/prod_cert/edit/<?= $content['products'][$i]['certificates'][$y]['id'] ?>">
                                    <?= $content['products'][$i]['certificates'][$y]['folder_url'] ?>
                                </a>
                            <?php
                                }
                            }
                            ?>
                        </td>
                    <?php } ?>
                    <td>
                        <input
                            type="checkbox"
                            name="add_products[]"
                            class="product-chb"
                            id="product-chb-<?= $content['products'][$i]['id'] ?>"
                            value="<?= $content['products'][$i]['id'] ?>"
                            checked="checked">
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Добавить выбранные товары">
        </div>
    </form>
<?php } else { ?>
    - Не найдено -
<?php
}
