<?php
/**
 * @var string $header
 * @var string $content
 */
?>
<h1><?= $header ?></h1>

<?php if (!empty($content) && is_string($content)) { ?>
    <p><?= $content ?></p>
<?php
}
