<?php
/**
 * @var array $content
 * @var array $Pagination
 */

$actionTypes = [
    promo_codes::ACTION_TYPE_PRODUCTS => 'Список товаров',
    promo_codes::ACTION_TYPE_TMS => 'Список торговых марок',
    promo_codes::ACTION_TYPE_CATEGORIES => 'Список категорий',
    promo_codes::ACTION_TYPE_CHEAPEST_PRODUCT_DISCOUNT => 'Самый дешёвый товар со скидкой',
];
?>
<div class="row">
    <div class="col-xs-6"><a href="/admin/promo_codes/add_by_query">Добавить через SQL запрос</a></div>
    <div class="col-xs-6"></div>
</div>
<div class="row">
    <div class="col-xs-6"><a href="/admin/promo_codes/generete_in_excel">Добавить и выгрузить в EXCEL</a></div>
    <div class="col-xs-6"></div>
</div>

<p>
    <a href="/admin/promo_codes/edit" class="btn btn-success">Создать промо-код</a>
</p>

<h3>Поиск</h3>

<form method="GET">
    <div class="form-group">
        <label for="promo-code">Промо-код</label>

        <input id="promo-code" type="text" class="form-control" name="code" value="<?= $content['search']['code'] ? h($content['search']['code']) : '' ?>">
    </div>

    <h5>Фильтрация товаров</h5>

    <div class="radio">
        <label>
            <input type="radio" name="action_type_id" value=""<?= $content['search']['action_type_id'] === null ? 'checked="checked"' : '' ?>>
            -Все-
        </label>
    </div>

    <?php foreach ($actionTypes as $typeID => $typeName) { ?>
        <div class="radio">
            <label>
                <input type="radio" name="action_type_id" value="<?= $typeID ?>"<?= $content['search']['action_type_id'] === $typeID ? 'checked="checked"' : '' ?>>
                <?= $typeName ?>
            </label>
        </div>
    <?php } ?>

    <input type="submit" class="btn btn-primary" value="Искать">
</form>

<p>Найдено: <?= $Pagination['count'] ?></p>

<?php
$paginationHTML = Template::get_tpl('pagination', [
    'Pagination' => $Pagination,
]);

echo $paginationHTML;
?>
<table class="table table-striped">
    <thead>
        <tr>
            <th>Промо-код</th>
            <th>Тип</th>
            <th>Скидка</th>
            <th>Сертификат</th>
            <th>Тип скидки</th>
            <th>Дата создания</th>
            <th>Комментарий</th>
            <th>Фильтрация товаров</th>
            <th>Использован</th>
        </tr>
    </thead>
    <tbody>
    <?php for ($i = 0, $n = count($content['promo_codes']); $i < $n; ++$i) { ?>
        <tr>
            <td>
                <a href="/admin/promo_codes/edit/<?= $content['promo_codes'][$i]['id'] . $content['back_url_param'] ?>"><?= highlight_query($content['promo_codes'][$i]['code'], $content['search']['code']) ?></a>
            </td>
            <td>
                <?= $content['promo_codes'][$i]['type'] === 'uniq' ? 'Уникальный' : 'Повторяемый' ?>
            </td>
            <td>
                <?= $content['promo_codes'][$i]['discount'] . ($content['promo_codes'][$i]['discount'] < 0 ? ' р.' : '%') ?>
            </td>
            <td>
                <?= $content['promo_codes'][$i]['certificate'] ? 'Да' : 'Нет' ?>
            </td>
            <td>
                <?= $content['promo_codes'][$i]['discount_type'] == 1 ? 'Правило большей скидки' : 'Обычный купон' ?>
            </td>
            <td>
                <?= $content['promo_codes'][$i]['create_date'] ?>
            </td>
            <td>
                <?= $content['promo_codes'][$i]['comment'] ? $content['promo_codes'][$i]['comment'] : '' ?>
            </td>
            <td>
                <?= isset($actionTypes[(int)$content['promo_codes'][$i]['action_type_id']]) ? $actionTypes[(int)$content['promo_codes'][$i]['action_type_id']] : 'Просто купон' ?>
            </td>
            <td>
                <?= $content['promo_codes'][$i]['use'] ? 'Да' : 'Нет' ?>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>
<?php
echo $paginationHTML;
