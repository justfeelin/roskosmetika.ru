<ul class="nav">
    <li><a href="/admin/export/categories">Категории</a></li>
    <li><a href="/admin/export/groups">Группы категорий</a></li>
    <li><a href="/admin/export/products">Товары</a></li>
    <li><a href="/admin/export/lines">Линейки</a></li>
    <li><a href="/admin/export/sets">Наборы</a></li>
    <li><a href="/admin/export/filters">Фильтры</a></li>
    <li><a href="/admin/export/tm">Торговые марки</a></li>
    <li><a href="/admin/export/tm_in_cat">Торговые марки в категориях</a></li>
    <li><a href="/admin/export/prods_no_photos">Товары без фотографий</a></li>
    <li><a href="/admin/export/wrong_photo">Фотографии ошибки</a></li>
    <li><a href="/admin/export/main_products">Главные фасовки</a></li>
    <li><a href="/admin/export/admitad_orders">Заказы Admitad</a></li>
    <li><a href="/admin/export/clients_call">Списки на обзвон</a></li>
</ul>
