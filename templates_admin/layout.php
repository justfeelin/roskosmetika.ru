<?php
/**
 * @var string $title
 * @var string $header
 * @var array $css
 * @var string $charset
 * @var array $scripts
 * @var string $page
 */
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <title><?= !empty($title) ? $title : $header ?></title>

    <?php for ($i = 0, $n = count($css); $i < $n; ++$i) { ?>
        <link rel="stylesheet" type="text/css" href="/css/<?= $css[$i] ?>">
    <?php } ?>

    <meta http-equiv="Content-type" content="text/html; charset=<?= $charset ?>">

    <?php for ($i = 0, $n = count($scripts); $i < $n; ++$i) { ?><!-- {{{ --><script type="text/javascript" src="/js/<?= $scripts[$i] ?>"></script><!-- }}} --><?php } ?>

    <script type="text/javascript">
        $(document).ready(function () {
            add_counter('.count_it');
        });

        function add_counter(el) {
            $(el).each(function () {
                $(this).css('resize', 'none').after('<div class="counter" style="width:' + $(this).innerWidth() + '; text-align:right;font-size: 10pt;font-family:Tahoma;color:#AAA">' + $(this).val().length + '</div>');
            });

            $(el).bind('keyup', function () {
                $(this).next('.counter').html($(this).val().length);
            });
        }
    </script>
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container-fluid">
            <div class="navbar-header">
                <?php if (admin::check_access(admin::RIGHTS_COMMON)) { ?>
                    <a class="navbar-brand" href="/admin">Roskosmetika 2.0</a>
                <?php } ?>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-left">
                    <?php if (admin::check_access(admin::RIGHTS_COMMON)) { ?>
                        <li class="active"><a href="/admin">Главная</a></li>
                    <?php } ?>
                    <li><a target="_blank" href="<?= DOMAIN_FULL ?>/">На сайт</a></li>
                    <?php if (admin::check_access(admin::RIGHTS_COMMON)) { ?>
                        <li><a target="_blank" href="https://trello.com">Задания</a></li>
                    <?php } ?>
                </ul>
                <p class="navbar-text pull-right">
                    Администрирование сайта
                    |
                    <a href="<?= DOMAIN_FULL ?>/admin/logout">Выход</a>
                </p>
            </div>
        </div>
    </div>
</div>

<hr>
<hr>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 sidebar">
            <div class="well sidebar-nav">
                <?php Template::partial('menu'); ?>
            </div>
        </div>
        <div class="col-xs-9">
            <div class="page-header">
                <h1>
                    <small><?= $header ?></small>
                </h1>
            </div>
            <?php Template::partial($page, true) ?>
        </div>
    </div>
</div>

<?= DB::output_queries() ?>

</body>
</html>
