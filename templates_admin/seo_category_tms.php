<?php
/**
 * @var array $content
 */
$q = isset($_GET['q']) ? $_GET['q'] : null;
?>
<form method="GET">
    <div class="form-group">
        <label for="category-q">Поиск</label>
        <input type="text" name="q" class="form-control" placeholder="Название, URL категории, бренда, связки..." id="category-q" value="<?= $q ? h($q) : '' ?>">
    </div>
    <input type="submit" class="btn btn-primary" value="Искать">
</form>
<?php
if ($content['items']) {
    $n = count($content['items']);

    if ($n === 100) {
?>
        <br>
        <p class="bg-info">
            <br>
            &nbsp;&nbsp;Показаны первые 100 совпадений
            <br>
            <br>
        </p>
    <?php } ?>
    <div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Категория (ID)</th>
                    <th>Бренд (ID)</th>
                    <th>URL связки</th>
                    <th>&lt;h1&gt;</th>
                    <th>&lt;title&gt;</th>
                    <th>&lt;meta name="description"&gt;</th>
                </tr>
            </thead>
            <tbody>
                <?php for ($i = 0; $i < $n; ++$i) { ?>
                    <tr>
                        <td>
                            <a href="/admin/seo/categories/<?= $content['items'][$i]['cat_id'] ?>">
                                <?= highlight_query($content['items'][$i]['cat_name'], $q) . ' (' . $content['items'][$i]['cat_id'] . ')' ?>
                            </a>
                        </td>
                        <td>
                            <a href="/admin/seo/tm/<?= $content['items'][$i]['tm_id'] ?>">
                                <?= highlight_query($content['items'][$i]['tm_name'], $q) . ' (' . $content['items'][$i]['tm_id'] . ')' ?>
                            </a>
                        </td>
                        <td>
                            <a href="/admin/seo/category-tm/<?= $content['items'][$i]['cat_id'] . '/' . $content['items'][$i]['tm_id'] ?>"><?= highlight_query(h($content['items'][$i]['url']), $q) ?></a>
                        </td>
                        <td>
                            <?= highlight_query(h($content['items'][$i]['h1']), $q) ?>
                        </td>
                        <td>
                            <?= h($content['items'][$i]['title']) ?>
                        </td>
                        <td>
                            <?= h($content['items'][$i]['description']) ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
<?php
}
