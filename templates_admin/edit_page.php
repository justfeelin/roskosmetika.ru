<?php
/**
 * @var string $header
 * @var array $values
 */
?>
<div>
    <form action="/admin/pages/<?= !empty($values['id']) ? 'edit' : 'add' ?>/" method="post">
        <h1><?= $header ?></h1>

        <div class="form-group">
            <label for="page-name">Имя(Url)</label>
            <input type="text" id="page-name" class="form-control" name="name" value="<?= !empty($values['id']) ? $values['name'] : '' ?>" size="60">
        </div>

        <div class="form-group">
            <label for="page-header">Header</label>
            <input type="text" id="page-header" class="form-control" name="header" value="<?= !empty($values['id']) ? $values['header'] : '' ?>" size="60">
        </div>

        <div class="form-group">
            <label for="page-type">Тип</label>
            <select class="choise form-control" id="page-type" name="type">
                <option <?= !empty($values['id']) && $values['type'] === 'standart' ? 'selected="selected"' : '' ?> value="standart">Постоянная</option>
                <option <?= !empty($values['id']) && $values['type'] === 'spec' ? 'selected="selected"' : '' ?> value="spec">Специальная</option>
                <option <?= !empty($values['id']) && $values['type'] === 'limit' ? 'selected="selecetd"' : '' ?> value="limit">Временная</option>
            </select>
        </div>

        <div class="limit_time date-inputs">
            <div class="form-group">
                <label for="page-begin-day">Начало</label>
                <input type="text" id="page-begin-day" name="begin_day" class="form-control" value="<?= !empty($values['id']) ? $values['begin_day'] : '' ?>" maxlength="2" size="2">
                . <input type="text" name="begin_month" class="form-control" value="<?= !empty($values['id']) ? $values['begin_month'] : '' ?>" maxlength="2" size="2">
                . <input type="text" name="begin_year" class="form-control" value="<?= !empty($values['id']) ? $values['begin_year'] : '' ?>" maxlength="4" size="4">
            </div>

            <div class="form-group">
                <label for="page-end-day">Окончание</label>
                <input type="text" id="page-end-day" name="end_day" class="form-control" value="<?= !empty($values['id']) ? $values['end_day'] : '' ?>" maxlength="2" size="2">
                . <input type="text" name="end_month" class="form-control" value="<?= !empty($values['id']) ? $values['end_month'] : '' ?>" maxlength="2" size="2">
                . <input type="text" name="end_year" class="form-control" value="<?= !empty($values['id']) ? $values['end_year'] : '' ?>" maxlength="4" size="4"></p>
            </div>
        </div>

        <div class="no_spec">
            <div class="form-group">
                <label for="page-title">Title</label>
                <textarea name="title" id="page-title" class="form-control"><?= !empty($values['id']) ? $values['title'] : '' ?></textarea>
            </div>

            <div class="form-group">
                <label for="page-keywords">Keywords</label>
                <textarea name="keywords" id="page-keywords" class="form-control"><?= !empty($values['id']) ? $values['keywords'] : '' ?></textarea>
            </div>

            <div class="form-group">
                <label for="page-seo-desc">Description</label>
                <textarea name="seo_desc" id="page-seo-desc" class="form-control"><?= !empty($values['id']) ? $values['description'] : '' ?></textarea>
            </div>
        </div>

        <div class="form-group">
            <label for="page-text">Контент</label>
            <textarea name="text" id="editor" id="page-text" class="form-control"><?= !empty($values['id']) ? $values['content'] : '' ?></textarea>
        </div>

        <?php if (!empty($values['id'])) { ?>
            <input type="hidden" name="id" value="<?= $values['id'] ?>">
        <?php } ?>

        <button type="submit" name="save" class="btn btn-primary">Сохранить</button>
    </form>
</div>
