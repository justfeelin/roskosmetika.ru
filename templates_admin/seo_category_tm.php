<?php
/**
 * @var array $content
 */
?>
<h2><?= $content['item']['cat_name'] . ' ' . $content['item']['tm_name'] ?></h2>

<a target="_blank" href="<?= DOMAIN_FULL . '/category-tm/' . $content['item']['url'] ?>"><?= DOMAIN_FULL . '/category-tm/' . $content['item']['url'] ?></a>

<form method="POST" action="/admin/seo/category-tm/<?= $content['item']['cat_id'] . '/' . $content['item']['tm_id'] ?>">
    <div class="form-group">
        <label for="ct-h1">&lt;h1&gt;</label>
        <input type="text" class="form-control" name="ct[h1]" id="ct-h1" value="<?= h($content['item']['h1']) ?>">
    </div>

    <div class="form-group">
        <label for="ct-title">&lt;title&gt;</label>
        <textarea class="form-control" name="ct[title]" id="ct-title" rows="5"><?= h($content['item']['title']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="ct-description">&lt;meta name="description"&gt;</label>
        <textarea class="form-control" name="ct[description]" id="ct-description" rows="5"><?= h($content['item']['description']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="ct-keywords">&lt;meta name="keywords"&gt;</label>
        <textarea class="form-control" name="ct[keywords]" id="ct-keywords" rows="5"><?= h($content['item']['keywords']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="ct-full-desc">Текст на странице</label>
        <textarea class="form-control" name="ct[full_desc]" id="ct-full-desc" rows="5"><?= h($content['item']['full_desc']) ?></textarea>
    </div>

    <input type="submit" class="btn btn-primary" value="Сохранить">
</form>
