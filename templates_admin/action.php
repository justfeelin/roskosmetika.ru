<?php
/**
 * @var array $content
 */
?>
<p style="margin-bottom: 20px; font-weight: bold "><a href="/admin/action/add">Добавить картинки для акции</a></p>
<?php if ($content) { ?>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Имя</th>
            <th>Активность</th>
        </tr>
        </thead>
        <tbody>
        <?php for ($i = 0, $n = count($content['actions']); $i < $n; ++$i) { ?>
            <tr>
                <td><a href="/admin/action/edit/<?= $content['actions'][$i]['id'] ?>"><?= $content['actions'][$i]['name'] ?></a></td>
                <td><a href="/admin/action/active/<?= $content['actions'][$i]['id'] ?>"><?= $content['actions'][$i]['active'] ? 'скрыть' : 'отобразить' ?></a></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php
}