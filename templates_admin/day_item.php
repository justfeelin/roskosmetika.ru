<?php
/**
 * @var array $content
 */
?>
<form class="admin_forms" action="/admin/day_item/save" method="post">
    <div class="row">
        <p class="col-xs-3 text-left"><label>Начало:</label></p>

        <p class="col-xs-3 text-left"><label>Конец:</label></p>

        <p class="col-xs-2 text-left"><label>Id продукта:</label></p>

        <p class="col-xs-1 text-left"><label>Скидка:</label></p>

        <p class="col-xs-2 text-left"><label>Метка:</label></p>

        <p class="col-xs-1 text-left"></p>
    </div>
    <?php for ($i = 0, $n = count($content['items']); $i < $n; ++$i) { ?>
    <div class="row da_row">
        <p class="form-group col-xs-3">
            <input class="da_begin form-control"
                type="text"
                value="<?= Template::date('Y.m.d A', $content['items'][$i]['begin']) ?>"
                real-value="<?= $content['items'][$i]['begin'] ?>"
                readonly="readonly">
        </p>
        <p class="form-group col-xs-3">
            <input class="da_end form-control"
                type="text"
                value="<?= Template::date('Y.m.d A', $content['items'][$i]['end']) ?>"
                real-value="<?= $content['items'][$i]['end'] ?>"
                readonly="readonly">
        </p>

        <p class="form-group col-xs-2">
            <input class="da_prod_id form-control"
                type="text"
                value="<?= $content['items'][$i]['prod_id'] ?>">
        </p>
        <p class="form-group col-xs-1">
            <input class="da_sale form-control" type="text" value="<?= $content['items'][$i]['sale'] ?>">
        </p>
        <p class="form-group col-xs-2">
            <select class="form-control da_tag">
                <option value="Товар<br>дня" <?= $content['items'][$i]['tag'] === 'Товар<br>дня' ? 'selected="selected"' : '' ?>>Товар дня</option>
                <option value="Успей<br>купить" <?= $content['items'][$i]['tag'] === 'Успей<br>купить' ? 'selected="selected"' : '' ?>>Успей купить
                </option>
                <option value="Cупер<br>цена" <?= $content['items'][$i]['tag'] === 'Cупер<br>цена' ? 'selected="selected"' : '' ?>>Cупер цена</option>
                <option value="Только<br>сегодня" <?= $content['items'][$i]['tag'] === 'Только<br>сегодня' ? 'selected="selected"' : '' ?>>Только
                сегодня
                </option>
                <option value="Шок<br>цена" <?= $content['items'][$i]['tag'] === 'Шок<br>цена' ? 'selected="selected"' : '' ?>>Шок цена</option>
                <option value="Супер<br>скидка" <?= $content['items'][$i]['tag'] === 'Супер<br>скидка' ? 'selected="selected"' : '' ?>>Супер скидка
                </option>
            </select>
        </p>
        <p class="form-group col-xs-1">
            <button type="button" class="da_btn btn btn-primary">Save</button>
        </p>
    </div>
    <?php } ?>
</form>
