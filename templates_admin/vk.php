<?php
/**
 * @var array $content
 * @var bool $get_token_error
 * @var int $new_products
 * @var int $update_products
 * @var int $not_categorized
 */
?>
<div>
    <?php if (!empty($get_token_error)) { ?>
        <p class="bg-danger">Не удалось получить токен<?= $get_token_error !== true ? ' (' . $get_token_error . ')' : '' ?></p>
    <?php
    }

    if (!$content['tokenOK']) {
    ?>
        <p>
            <?php if (!$content['token']) { ?>
                Рабочего токена нет.
            <?php } else { ?>
                Необходимо обновить токен, т.к. он
                <?php if ($content['token']['expires'] > $_SERVER['REQUEST_TIME']) { ?>
                    истекает через <?= $content['token']['expires'] - $_SERVER['REQUEST_TIME'] ?> секунд.
                <?php } else { ?>
                    истёк.
            <?php
                }
            }
            ?>
        </p>

        <pre>Для обновления токена необходимо быть авторизованным на vk.com в качестве <strong>vk.com/roskosmetika_info</strong></pre>

        <a href="/admin/vk/get_token" class="btn btn-success">Обновить токен</a>
    <?php } else { ?>
        <p>
            Токен
            <?php if ($content['token']['expires']) { ?>
                истекает <?= date('Y-m-d H:i:s', $content['token']['expires']) ?>.
            <?php } else { ?>
                не ограничен по времени.
            <?php } ?>
            <a href="/admin/vk/get_token">Обновить принудительно</a>
        </p>

        <div>
            <?php if ($new_products) { ?>
                <p>
                    <a href="/admin/vk/insert_products" class="btn btn-success">Экспортировать 100 новых товаров на
                        vk.com (Всего к экспорту: <?= $new_products ?>)</a>
                </p>
            <?php
            }

            if ($update_products) {
            ?>
                <p>
                    <a href="/admin/vk/update_products" class="btn btn-primary">Обновить 100 ранее добавленных
                        товаров на vk.com (Всего к обновлению: <?= $update_products ?>)</a>
                </p>
            <?php
            }

            if ($not_categorized) {
            ?>
                <p>
                    <a href="/admin/vk/categorize_products" class="btn btn-primary">Добавить 100 товаров в категории на vk.com (Всего без категорий: <?= $not_categorized ?>)</a>
                </p>
            <?php
            }
            ?>
        </div>
    <?php } ?>
</div>
