<?php
/**
 * @var array $content
 */
?>
<form method="GET">
    <div class="form-group">
        <label>
            <input type="checkbox" value="1" name="invisible" <?= $content['invisible'] ? ' checked="checked"' : '' ?>>
            Выключенные (Страница не найдена)
        </label>
    </div>
    <div class="form-group">
        <label>
            <input type="checkbox" value="1" name="not_active" <?= $content['not_active'] ? ' checked="checked"' : '' ?>>
            Сняты с продаж
        </label>
    </div>
    <input type="submit" class="btn btn-primary" value="Искать">
    <a href="../update_packs"><input type="button" class="btn btn-primary" value="Исправить автоматически"></a>
</form>


<?php
if ($content['products'] !== null) {
    if ($content['products']) {
?>
        <div>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Название</th>
                        <th>URL</th>
                        <th>Выключен</th>
                        <th>Снят с продаж</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    for ($i = 0, $n = count($content['products']); $i < $n; ++$i) {
                        $url = $content['products'][$i]['url'] ?: $content['products'][$i]['id'];
                    ?>
                        <tr>
                            <td>
                                <?= $content['products'][$i]['id'] ?>
                            </td>
                            <td>
                                <?= $content['products'][$i]['name'] ?>
                            </td>
                            <td>
                                <a href="/product/<?= h($url) ?>" target="_blank"><?= $url ?></a>
                            </td>
                            <td>
                                <input type="checkbox" disabled="disabled"<?= !$content['products'][$i]['visible'] ? ' checked="checked"' : ''?>>
                            </td>
                            <td>
                                <input type="checkbox" disabled="disabled"<?= !$content['products'][$i]['active'] ? ' checked="checked"' : ''?>>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
<?php } else { ?>
    - Не найдено -
<?php
    }
}
