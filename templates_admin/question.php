<?php
/**
 * @var array $content
 */
?>
<p>
    <a href="/admin/questions<?= $content['back_url_param'] ?>">Все вопросы</a>
</p>

<p><?= $content['question']['date'] ?></p>

<form method="POST" action="<?= $_SERVER['REQUEST_URI'] ?>">
    <p>
        <?= $content['question']['city'] ?>
        <br>
        <?= $content['question']['email'] ?>
        <br>
        <?= $content['question']['phone'] ?>
    </p>

    <div class="form-group">
        <label for="name">Имя автора вопроса</label>
        <input class="form-control" type="text" id="name" name="name" value="<?= $content['question']['name'] ?>">
    </div>

    <div class="form-group">
        <label for="questionText">Текст вопроса</label>
        <textarea class="form-control" rows="10" id="questionText" required="required"
                  name="question"><?= $content['question']['question'] ?></textarea>
    </div>

    <h3>Ответить</h3>
    <?php if ($content['question']['answer']) { ?>
        <strong>Ответ уже есть</strong>
    <?php
    }

    if ($content['answerError']) {
    ?>
        <p class="bg-danger">Проверьте правильность заполнения формы</p>
    <?php } ?>

    <div class="form-group">
        <label for="answerText">Текст ответа</label>
        <textarea class="form-control" rows="10" id="answerText" required="required"
                  name="answer"><?= $content['question']['answer'] ?></textarea>
    </div>

    <div class="checkbox">
        <label>
            <input type="checkbox" value="1" name="visible"<?= $content['question']['visible'] ? ' checked="checked"' : '' ?>>
            Разместить на сайте
        </label>
    </div>

    <div class="checkbox">
        <label>
            <input type="checkbox" value="1" name="email"<?= !empty($_POST['email']) ? ' checked="checked"' : '' ?>>
            Отправить письмо клиенту
        </label>
    </div>

    <input type="submit" class="btn btn-primary" name="submitAnswer" value="Ответить">
</form>
