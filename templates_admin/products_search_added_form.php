<?php
/**
 * @var array $content
 */
if ($content['products']) {
    $n = count($content['products'])
?>
    <h4>Всего добавлено: <?= $n ?></h4>

    <form method="POST" class="products-box remove-added-form">
        <input type="hidden" name="remove_products[]">

        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Название</th>
                <th>Описание</th>
                <?php if (isset($content['products'][0]['ingredients'])) { ?>
                    <th>
                        Состав
                    </th>
                <?php } ?>
                <th>
                    <input type="checkbox" class="check-all">
                </th>
            </tr>
            </thead>
            <tbody>
            <?php for ($i = 0; $i < $n; ++$i) { ?>
                <tr>
                    <td>
                        <?= $content['products'][$i]['id'] ?>
                    </td>
                    <td>
                        <label for="product-chb-<?= $content['products'][$i]['id'] ?>">
                            <?= $content['products'][$i]['name'] . ', ' . $content['products'][$i]['pack'] ?>
                        </label>
                    </td>
                    <td>
                        <?= $content['products'][$i]['short_description'] ?>
                    </td>
                    <?php if (isset($content['products'][$i]['ingredients'])) { ?>
                        <td>
                            <?= $content['products'][$i]['ingredients'] ?>
                        </td>
                    <?php } ?>
                    <td>
                        <input
                            type="checkbox"
                            name="remove_products[]"
                            class="product-chb"
                            id="product-chb-<?= $content['products'][$i]['id'] ?>"
                            value="<?= $content['products'][$i]['id'] ?>"
                        >
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <input type="submit" class="btn btn-danger" value="Удалить выбранные товары">
    </form>
<?php } else { ?>
    - Нет добавленных товаров -
    <?php
}
