<?php
/**
 * @var array $content
 */
?>
<form enctype="multipart/form-data" action="/admin/competition/save" method="post">

    <div class="form-group">
        <label>Название</label>
        <input name="name" class="form-control" value="<?= !empty($content['id']) ? $content['name'] : '' ?>" size="100">
    </div>

    <div class="form-group">
        <label>Ссылка</label>
        <input type="text" class="form-control" name="link" value="<?= !empty($content['id']) ? $content['url'] : '' ?>">
    </div>

    <div class="form-group">
        <label>Ссылка для объявления победителей</label>
        <input type="text" class="form-control" name="win_link" value="<?= !empty($content['id']) ? $content['win_link'] : '' ?>">
    </div>

    <div class="form-group">
        <label>Описание</label>
        <textarea name="text" id="editor" class="form-control"><?= !empty($content['id']) ? $content['description'] : '' ?></textarea>
    </div>

    <h4>Даты акции</h4>

    <div id="dates-box">

        <div class="row banner-date">
            <div class="col-xs-4 col-md-3 col-lg-2">
                <div class="form-group">
                    <label>Начало</label>
                    <input type="text" class="form-control dt-input" name="dates_begin" value="<?= !empty($content['id'])? h($content['begin']):date('Y-m-d')  ?>">
                </div>
            </div>
            <div class="col-xs-4 col-md-3 col-lg-2">
                <div class="form-group">
                    <label>Окончание</label>
                    <input type="text" class="form-control dt-input" name="dates_end" value="<?= !empty($content['id'])? h($content['end']):h(date('Y-m-d') ) ?>">
                </div>
            </div>
        </div>

    </div>

    <div class="form-group">
        <label>Имя файла картинки</label>
        <input type="text" class="form-control" name="file_name" value="<?= !empty($content['id']) ?$content['file_name'].'.'.$content['type']: '' ?>" size="80">
    </div>

    <div class="form-group">
        <label>Альтернатива(если картинка не отобразилась)</label>
        <input type="text" class="form-control" name="alt" value="<?= !empty($content['id']) ?$content['alt']: '' ?>" size="80">
    </div>

    <?php if (!empty($content['file_name'])) { ?>
        <label>Текущая картинка:</label>

        <p id="banner"><img src="/images/competition/<?= $content['file_name'] ?>.<?= $content['type'] ?>" alt="<?= ($content['alt']=='')? $content['alt']:$content['name'] ?>"></p>
    <?php } ?>

    <div class="form-group">
        <label><?= !empty($content['file_name']) ? 'Сменить' : 'Добавить' ?> картинку</label>

        <p><input name="pic" type="file"></p>
    </div>

    <div class="checkbox">
        <label>
            <input type="checkbox" name="visible"<?= !isset($content['id']) || $content['visible'] ? ' checked="checked"' : '' ?>> отображать
        </label>
    </div>

    <?php if (!empty($content['id'])) { ?>
        <input type="hidden" name="id" value="<?= $content['id'] ?>">
    <?php } ?>

    <p><input type="submit" name="submit" value="Cохранить" class="btn btn-primary"></p>
</form>
