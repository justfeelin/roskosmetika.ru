<?php
/**
 * @var array $content
 * @var array $products_search
 */
?>
    <p>
        <a href="<?= DOMAIN_FULL . '/kosmetika/' . $content['type_page']['url'] ?>"><?= $content['type_page']['name'] ?></a> (ID <?= $content['type_page']['id'] ?>)
    </p>

    <h3>Добавить товары</h3>

<?php
Template::partial('products_search_form', [
    'products_search' => $products_search,
]);
