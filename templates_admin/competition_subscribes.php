<?php
/**
 * @var array $subscribes
 * @var array $competition
 */
if ($subscribes) {
    ?>
    <p><a href="/admin/competition/upload/<?= $competition['id'] ?>">Выгрузить в файл</a></p>
    <p>&nbsp;</p>
    <p><?= $competition['name']." до ".$competition['end']?></p>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Имя</th>
            <th>Телефон</th>
            <th>Email</th>
            <th>Дата окончания акции</th>
            <th>Статус</th>
        </tr>
        </thead>
        <tbody>
        <?php for ($i = 0, $n = count($subscribes); $i < $n; ++$i) { ?>
            <tr>
                <td><?= $subscribes[$i]['name'] ?></td>
                <td><?= $subscribes[$i]['phone'] ?></td>
                <td><?= $subscribes[$i]['email'] ?></td>
                <td><?= $subscribes[$i]['competitions_date'] ?></td>
                <td>
                    <a href="/admin/competition/change_win/<?= $subscribes[$i]['id'] ?>"><?= $subscribes[$i]['winner'] ? 'победитель' : 'лузер' ?></a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php } else { ?>
    <p>Нет данных.</p>
    <?php
}
