<?php
/**
 * @var array $Pagination
 */

if (!empty($Pagination) && $Pagination['pages'] > 1) {
?>
    <nav>
        <ul class="pagination">
            <?php if ($Pagination['showAll']) { ?>
                <li>
                    <a href="<?= $Pagination['baseUrl'] ?>" data-page="1">По страницам</a>
                </li>
            <?php
            } else {
                if ($Pagination['showPrev']) {
            ?>
                    <li>
                        <a href="<?= $Pagination['page'] > 2 ? $Pagination['url'] . 'p=' . ($Pagination['page'] - 1) : $Pagination['baseUrl'] ?>">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                <?php
                }

                if ($Pagination['showFirst']) {
                    ?>
                    <li>
                        <a href="<?= $Pagination['baseUrl'] ?>">1</a>
                    </li>
                    <?php
                }

                if ($Pagination['showPrevDots']) {
                ?>
                    <li>
                        <span>...</span>
                    </li>
                <?php
                }

                for ($i = $Pagination['start']; $i < $Pagination['end'] + 1; ++$i) {
                ?>
                    <li <?= $i === $Pagination['page'] ? 'class="active"' : '' ?>>
                        <a href="<?= $i === $Pagination['page'] ? 'javascript:void(0)' : ($i > 1 ? $Pagination['url'] . 'p=' . $i : $Pagination['baseUrl']) ?>"><?= $i ?></a>
                    </li>
                <?php
                }

                if ($Pagination['showNextDots']) {
                ?>
                    <li>
                        <span>...</span>
                    </li>
                <?php
                }

                if ($Pagination['showLast']) {
                ?>
                    <li>
                        <a href="<?= $Pagination['url'] . 'p=' . $Pagination['pages'] ?>"><?= $Pagination['pages'] ?></a>
                    </li>
                <?php
                }

                if ($Pagination['showNext']) {
                ?>
                    <li>
                        <a href="<?= $Pagination['url'] . 'p=' . ($Pagination['page'] + 1) ?>">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                <?php } ?>
                <li>
                    <a href="<?= $Pagination['url'] ?>showAll">Показать все</a>
                </li>
            <?php } ?>
        </ul>
    </nav>
<?php
}
