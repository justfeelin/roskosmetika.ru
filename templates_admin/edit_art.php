<?php
/**
 * @var array $content
 */
?>
<form class="admin_forms" action="/admin/articles/<?= !empty($content['id']) ? 'edit/' . $content['id'] : ('add/' . (isset($content) ? $content : '0')) ?>" method="post">
    <?php
    if (!empty($content['id'])) {
        if ($content['parent_id']) {
    ?>
        <h1>Редактировать статью</h1>
    <?php } else { ?>
        <h1>Редактировать раздел</h1>
    <?php
        }
    } else {
        if (isset($content)) {
    ?>
            <h1>Добавить статью</h1>
        <?php } else { ?>
            <h1>Добавить раздел</h1>
    <?php
        }
    }
    ?>

    <div class="form-group">
        <label>URL</label>
        <input type="text" class="form-control" name="url" value="<?= !empty($content['id']) ? $content['url'] : ''?>">
    </div>

    <div class="form-group">
        <label>Название</label>
        <input type="text" class="form-control" name="name" value="<?= !empty($content['id']) ? $content['name'] : '' ?>">
    </div>

    <div class="form-group">
        <label>Title</label>
        <input type="text" class="form-control count_it" name="title" value="<?= !empty($content['id']) ? $content['title'] : '' ?>">
    </div>

    <div class="form-group">
        <label>Description</label>
        <input type="text" class="form-control count_it" name="description" value="<?= !empty($content['id']) ? $content['description'] : ''?>">
    </div>

    <div class="form-group">
        <label>Keywords</label>
        <input type="text" class="form-control count_it" name="keywords" value="<?= !empty($content['id']) ? $content['keywords'] : '' ?>">
    </div>

    <div class="form-group">
        <label>Синонимы</label>
        <textarea name="synonym" class="form-control"><?= !empty($content['id']) ? $content['synonym'] : '' ?></textarea>
    </div>

    <?php if (!empty($content['id']) ? $content['parent_id'] : isset($content)) { ?>
        <div class="form-group">
            <label>Статья</label>
            <textarea name="text"  id="editor" class="form-control"><?= !empty($content['id']) ? $content['text'] : '' ?></textarea>
        </div>
    <?php
    }

    if (!empty($content['id'])) {
    ?>
        <input type="hidden" name="id" value="<?= $content['id'] ?>">
    <?php } else { ?>
        <input type="hidden" name="parent_id" value="<?= isset($content) ? $content : '0' ?>">
    <?php } ?>

    <p>
        <button type="submit" name="save" class="btn btn-primary">Сохранить изменения</button>
    </p>
</form>
