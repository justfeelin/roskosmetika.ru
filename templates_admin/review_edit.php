<?php
/**
 * @var array $values
 * @var array $content
 * @var array $city
 */
if (isset($values)) {
    ?>
    <form action="/admin/review/edit/" method="post">
        <h1>Редактировать отзыв</h1>

        <div class="form-group">
            <label>Отзыв</label>
            <textarea name="comment" class="form-control" cols="70" rows="30"><?= $values['comment'] ?></textarea>
        </div>

        <input type="hidden" name="id" value="<?= $values['id'] ?>">

        <p>
            <button type="submit" name="save" class="btn btn-primary">Сохранить изменения</button>
        </p>
    </form>
<?php
} else {
    if (isset($content)) {
?>
        <form action="/admin/review/edit_answer/" method="post">
            <h1>Редактировать ответ</h1>

            <div class="form-group">
                <label>Ответ</label>
                <textarea name="answer" class="form-control" cols="70" rows="30"><?= $content['answer'] ?></textarea>
            </div>

            <input type="hidden" name="id" value="<?= $content['id'] ?>">

            <p>
                <button type="submit" name="save" class="btn btn-primary">Сохранить изменения</button>
            </p>
        </form>
    <?php } else { ?>
        <form action="/admin/review/edit_city/" method="post">
            <h1>Изменить город</h1>

            <p>Город в базе:
                <b><?= $city['city'] ?></b>
            </p>

            <div class="form-group">
                <label>Новый город</label>
                <input type="text" class="form-control" name="city" value="<?= $city['city'] ?>">
            </div>

            <input type="hidden" name="id" value="<?= $city['id'] ?>">

            <p>
                <button type="submit" name="save" class="btn btn-primary">Сохранить изменения</button>
            </p>
        </form>
<?php
    }
}