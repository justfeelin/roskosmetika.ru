<?php
/**
 * @var array $content
 */
?>
<h2><?= $content['tm']['name'] ?></h2>

<a target="_blank" href="<?= DOMAIN_FULL . '/tm/' . $content['tm']['url'] ?>"><?= DOMAIN_FULL . '/tm/' . $content['tm']['url'] ?></a>

<form method="POST" action="/admin/seo/tm/<?= $content['tm']['id'] ?>">
    <div class="form-group">
        <label for="tm-alt_name">Альтернативное название</label>
        <input type="text" class="form-control" name="tm[alt_name]" id="tm-alt_name" value="<?= h($content['tm']['alt_name']) ?>">
    </div>

    <div class="form-group">
        <label for="tm-title">&lt;title&gt;</label>
        <textarea class="form-control" name="tm[title]" id="tm-title" rows="5"><?= h($content['tm']['title']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="tm-description">&lt;meta name="description"&gt;</label>
        <textarea class="form-control" name="tm[description]" id="tm-description" rows="5"><?= h($content['tm']['description']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="tm-keywords">&lt;meta name="keywords"&gt;</label>
        <textarea class="form-control" name="tm[keywords]" id="tm-keywords" rows="5"><?= h($content['tm']['keywords']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="tm-desc">Текст на странице</label>
        <textarea class="form-control" name="tm[desc]" id="tm-keywords" rows="5"><?= h($content['tm']['desc']) ?></textarea>
    </div>

    <input type="submit" class="btn btn-primary" value="Сохранить">
</form>
