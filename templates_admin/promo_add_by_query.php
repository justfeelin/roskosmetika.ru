<form class="form-horizontal" action="/admin/promo_codes/add_by_query" method="post">
    <div class="form-group">
        <div class="col-xs-12">
            <label class="control-label">Текст запроса:</label>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-8">
            <textarea class="form-control" rows="20" name="query"></textarea>
        </div>
        <div class="col-xs-4"></div>
    </div>

    <div class="form-group">
        <div class="col-xs-12">
            <label class="control-label">Размер скидки:</label>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-1">
            <input class="form-control" name="discount"></input>
        </div>
        <div class="col-xs-11"></div>
    </div>

    <div class="form-group">
        <div class="col-xs-8">
            <button type="submit" class="btn btn-primary" name="save">Назначить коды</button>
        </div>
        <div class="col-xs-4"></div>
    </div>
</form>
