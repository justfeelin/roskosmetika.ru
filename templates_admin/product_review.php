<?php
/**
 * @var array $content
 */
?>
<p>
    <a href="/admin/product_reviews<?= $content['back_url_param'] ?>">Все отзывы</a>
</p>

<p>
    Товар: <?= $content['review']['product_id'] ? $content['review']['product_name'] . ' (' . $content['review']['product_id'] . ')' : '-Товар не найден-' ?>
</p>

<?php if ($content['saveError']) { ?>
    <p class="bg-danger">Проверьте правильность заполнения формы</p>
<?php } ?>

<form method="POST" action="<?= $_SERVER['REQUEST_URI'] ?>">
    <div class="form-group">
        <label for="reviewName">Имя</label>
        <input type="text" class="form-control" id="reviewName" required="required" name="name"
               value="<?= $content['review']['name'] ?>">
    </div>

    <div class="form-group">
        <label for="reviewCity">Город</label>
        <input type="text" class="form-control" id="reviewCity" required="required" name="city"
               value="<?= $content['review']['city'] ?>">
    </div>

    <div class="form-group">
        <label for="reviewDate">Дата</label>
        <input type="text" class="form-control" id="reviewDate" required="required" name="date"
               value="<?= $content['review']['date'] ?>">
    </div>

    <div class="form-group">
        <label for="reviewComment">Текст отзыва</label>
        <textarea class="form-control" rows="10" id="reviewComment" required="required"
                  name="comment"><?= $content['review']['comment'] ?></textarea>
    </div>

    <div class="checkbox">
        <label>
            <input type="checkbox" value="1" name="negative"<?= $content['review']['negative'] ? ' checked="checked"' : '' ?>>
            Негативный
        </label>
    </div>

    <div class="checkbox">
        <label>
            <input type="checkbox" value="1" name="visible"<?= $content['review']['visible'] ? ' checked="checked"' : '' ?>>
            Разместить на сайте
        </label>
    </div>

    <input type="submit" class="btn btn-primary" name="submitSave" value="Сохранить">
</form>
