<?php
/**
 * @var array $content
 */
?>
<div>
    <a href="/admin/partner/settings" class="btn btn-default">
        Настройки
    </a>
    <a href="/admin/partner/add" class="btn btn-primary">
        Добавить
    </a>
</div>
<?php if ($content) { ?>
    <table class="pages table table-striped">
        <thead>
        <tr class="head">
            <th>Партнерская программа</th>
            <th>Видимость</th>
        </tr>
        </thead>
        <tbody>
        <?php for ($i = 0, $n = count($content['partners']); $i < $n; ++$i) { ?>
            <tr <?= !$content['partners'][$i]['active'] ? 'id="deactive"' : '' ?>>
                <td align="left"><a
                        href="/admin/partner/edit/<?= $content['partners'][$i]['id'] ?>"><?= $content['partners'][$i]['name'] ?></a>
                </td>
                <td>
                    <a href="/admin/partner/show/<?= $content['partners'][$i]['id'] ?>"><?= !$content['partners'][$i]['active'] ? 'показать' : 'скрыть' ?></a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php
}
