<?php
/**
 * @var array $content
 * @var array $Pagination
 */
?>
<form method="GET">
    <div class="form-group">
        <div class="radio">
            <label>
                <input type="radio" name="read" value="1"<?= $content['search']['read'] === true ? ' checked="checked"' : '' ?>>
                Просмотренные
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="read" value="0"<?= $content['search']['read'] === false ? ' checked="checked"' : '' ?>>
                Непросмотренные
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="read" value=""<?= $content['search']['read'] === null ? ' checked="checked"' : '' ?>>
                -Все-
            </label>
        </div>
    </div>

    <div class="form-group">
        <div class="radio">
            <label>
                <input type="radio" name="visible"
                       value="1"<?= $content['search']['visible'] === true ? ' checked="checked"' : '' ?>>
                Видимые
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="visible"
                       value="0"<?= $content['search']['visible'] === false ? ' checked="checked"' : '' ?>>
                Невидимые
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="visible" value=""<?= $content['search']['visible'] === null ? 'checked="checked"' : '' ?>>
                -Все-
            </label>
        </div>
    </div>

    <div class="form-group">
        <div class="radio">
            <label>
                <input type="radio" name="negative" value="1"<?= $content['search']['negative'] === true ? ' checked="checked"' : '' ?>>
                Негативные
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="negative" value="0"<?= $content['search']['negative'] === false ? ' checked="checked"' : '' ?>>
                Не негативные
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="negative" value=""<?= $content['search']['negative'] === null ? ' checked="checked"' : '' ?>>
                -Все-
            </label>
        </div>
    </div>

    <input type="submit" class="btn btn-primary" value="Искать">
</form>

<p>Найдено: <?= $Pagination['count'] ?></p>

<?php

$paginationHTML = Template::get_tpl('pagination', [
    'Pagination' => $Pagination,
]);

echo $paginationHTML;

for ($i = 0, $n = count($content['reviews']); $i < $n; ++$i) {
?>
    <div>
        <a href="/admin/product_reviews/<?= $content['reviews'][$i]['id'] . $content['back_url_param'] ?>"><?= $content['reviews'][$i]['date'] ?></a>
        <?= $content['reviews'][$i]['name'] . ', ' . $content['reviews'][$i]['city'] . ' [' . $content['reviews'][$i]['email'] . ']' ?>
        <p>
            Товар:
            <a href="/product/<?= $content['reviews'][$i]['product_url'] ?>#prod-reviews" target="_blank">
                <?= $content['reviews'][$i]['product_id'] ? $content['reviews'][$i]['product_name'] . ' (' . $content['reviews'][$i]['product_id'] . ')' : '-Товар не найден-' ?>
            </a>
            <a href="/prod_review/<?= $content['reviews'][$i]['product_url'] ?>" target="_blank">[Все отзывы на товар]</a>
            <br>
            Отзыв: <?= $content['reviews'][$i]['comment'] ?>
        </p>
        <?php if ($content['reviews'][$i]['dublicates']) { ?>
            <p>
                <strong class="red">Есть дубликаты отзыва! Количество: <?= $content['reviews'][$i]['dublicates'] ?></strong>
            </p>
        <?php } ?>
        <a href="/admin/product_reviews/toggle_negative/<?= $content['reviews'][$i]['id'] ?>">
            <?= $content['reviews'][$i]['negative'] ? 'Снять пометку "негативный"' : 'Отметить как негативный' ?>
        </a>
        <br>
        <?= $content['reviews'][$i]['visible'] ? '<strong>Отображается</strong>' : '<em>Не отображается</em>' ?>
        <hr>
    </div>
<?php
}

echo $paginationHTML;
