<?php
/**
 * @var array $content
 */
?>
<a href="/admin/pages/add" class="btn btn-primary">
    Новая страница
</a>

<table class="pages table table-striped">
    <thead>
    <tr>
        <th>Название</th>
        <th>Начало</th>
        <th>Окончание</th>
        <th>Тип</th>
        <th>Видимость</th>
    </tr>
    </thead>
    <tbody>
        <?php for ($i = 0, $n = count($content['pages']); $i < $n; ++$i) { ?>
            <tr
                <?= !$content['pages'][$i]['visible'] || ($content['pages'][$i]['end'] <= $_SERVER['REQUEST_TIME'] && $content['pages'][$i]['end']) || ($content['pages'][$i]['begin'] >= $_SERVER['REQUEST_TIME']) ? 'id="deactive"' : '' ?>>
                <td align="left"><a href="/admin/pages/edit/<?= $content['pages'][$i]['id'] ?>"><?= $content['pages'][$i]['header'] ?></a></td>
                <td><?= $content['pages'][$i]['begin'] ? $content['pages'][$i]['begin_day'] . '-' . $content['pages'][$i]['begin_month'] . '-' . $content['pages'][$i]['begin_year'] : '-' ?></td>
                <td><?= $content['pages'][$i]['end'] ? $content['pages'][$i]['end_day'] . '-' . $content['pages'][$i]['end_month'] . '-' . $content['pages'][$i]['end_year'] : '-' ?></td>
                <td><?= $content['pages'][$i]['type'] === 'limit' ? 'временная' : ($content['pages'][$i]['type'] === 'standart' ? 'постоянная' : 'спец.(невидима)') ?></td>
                <td><a href="/admin/pages/show/<?= $content['pages'][$i]['id'] ?>"><?= !$content['pages'][$i]['visible'] ? 'показать' : 'скрыть' ?></a></td>
            </tr>
        <?php } ?>
    </tbody>
</table>
