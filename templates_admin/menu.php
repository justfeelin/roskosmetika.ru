<?php

$menu = admin::menu();

ob_start();

for ($i = 0, $n = count($menu); $i < $n; ++$i) {
    $groupAccess = !empty($menu[$i]['access']) ? $menu[$i]['access'] : null;

    ob_start();

    for ($y = 0, $m = count($menu[$i]['items']); $y < $m; ++$y) {
        if ((!empty($menu[$i]['items'][$y]['access']) || $groupAccess !== null) && !admin::check_access(!empty($menu[$i]['items'][$y]['access']) ? $menu[$i]['items'][$y]['access'] : $groupAccess)) {
            continue;
        }
?>
        <li><a href="<?= $menu[$i]['items'][$y]['url'] ?>"><?= $menu[$i]['items'][$y]['name'] ?></a></li>
<?php
    }

    $groupHTML = ob_get_contents();

    ob_end_clean();

    if ($groupHTML) {
    ?>
        <li class="nav-header"><?= $menu[$i]['name'] ?>:</li>
        <?= $groupHTML ?>
    <?php
    }
}

$menuHTML = ob_get_contents();

ob_end_clean();

if ($menuHTML) {
?>
    <ul class="nav nav-sidebar">
        <?= $menuHTML ?>
    </ul>
<?php
}
