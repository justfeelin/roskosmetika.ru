<?php
/**
 * @var array $msg
 * @var array $content
 */

if (isset($msg)) {
    foreach ($msg as $item) {
?>
    <div class="alert alert-block"><?= $item ?></div>
<?php
    }
}
?>
<form action="/admin/export/tm" method="POST">
    <div class="row">
        <div class="col-xs-6">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="tms">&nbsp;Торговая&nbsp;марка
                </label>
            </div>
            <div class="form-group">
                <select name="tm" class="form-control">
                    <?php
                    if (!empty($content['tm'])) {
                        for ($i = 0, $n = count($content['tm']); $i < $n; ++$i) { ?>
                            <option value="<?= $content['tm'][$i]['id'] ?>"><?= $content['tm'][$i]['name'] ?></option>
                    <?php
                        }
                    } else {
                    ?>
                        <option value="none">Нет торговых марок</option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <span class="col-xs-6"></span>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-primary" name="choise">Загрузить выборку</button>
            <button type="submit" class="btn btn-primary" name="all">Загрузить все</button>
            <button type="submit" class="btn btn-primary" name="blank">Загрузить бланк</button>
        </div>
    </div>
</form>
