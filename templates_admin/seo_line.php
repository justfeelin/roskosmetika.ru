<?php
/**
 * @var array $content
 */
?>
<h2><?= $content['line']['name'] ?></h2>

<a target="_blank" href="<?= DOMAIN_FULL . '/tm/' . $content['line']['tm_url'] . '/' . $content['line']['url'] ?>"><?= DOMAIN_FULL . '/tm/' . $content['line']['tm_url'] . '/' . $content['line']['url'] ?></a>

<form method="POST" action="/admin/seo/lines/<?= $content['line']['id'] ?>">
    <div class="form-group">
        <label for="line-h1">&lt;H1&gt;</label>
        <textarea class="form-control" name="line[h1]" id="line-h1" rows="5"><?= h($content['line']['h1']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="line-title">&lt;title&gt;</label>
        <textarea class="form-control" name="line[title]" id="line-title" rows="5"><?= h($content['line']['title']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="line-description">&lt;meta name="description"&gt;</label>
        <textarea class="form-control" name="line[description]" id="line-description" rows="5"><?= h($content['line']['description']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="line-keywords">&lt;meta name="keywords"&gt;</label>
        <textarea class="form-control" name="line[keywords]" id="line-keywords" rows="5"><?= h($content['line']['keywords']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="line-desc">Описание на странице коллекции</label>
        <textarea class="form-control" name="line[desc]" id="line-desc" rows="5"><?= h($content['line']['desc']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="line-short-desc">Краткое описание</label>
        <textarea class="form-control" name="line[short_desc]" id="line-short-desc" rows="5"><?= h($content['line']['short_desc']) ?></textarea>
    </div>

    <input type="submit" class="btn btn-primary" value="Сохранить">
</form>
