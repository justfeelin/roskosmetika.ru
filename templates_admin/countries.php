<?php
/**
 * @var array $content
 */
?>
<?php if ($content['items']) { ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>URL</th>
                <th>Название</th>
                <th>H1</th>
                <th>Видимость</th>
            </tr>
        </thead>
        <tbody>
            <?php for ($i = 0, $n = count($content['items']); $i < $n; ++$i) { ?>
                <tr>
                    <td>
                        <a href="/admin/country/<?= $content['items'][$i]['id'] ?>"><?= $content['items'][$i]['id'] ?></a>
                    </td>
                    <td>
                        <?= h($content['items'][$i]['url']) ?>
                    </td>
                    <td>
                        <?= h($content['items'][$i]['name']) ?>
                    </td>
                    <td>
                        <?= h($content['items'][$i]['h1']) ?>
                    </td>
                    <td>
                        <a href="/admin/country/<?= $content['items'][$i]['id'] ?>/<?= $content['items'][$i]['visible']?>"><?= $content['items'][$i]['visible'] ? 'скрыть' : 'отобразить' ?></a>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
<?php
}
