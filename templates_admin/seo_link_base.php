<?php
/**
 * @var array $content
 */

if ($content['info']) { ?>
    <div class="alert alert-block bg-success"><?= $content['info'] ?></div>
<?php
}

if ($content['error']) { ?>
    <div class="alert alert-block bg-danger"><?= $content['error'] ?></div>
<?php } ?>

<form enctype="multipart/form-data" action="/admin/seo/link_base" method="POST">
    <div class="form-group">
        <input type="file" name="file" style="display:none;" class="input-file"/>
    </div>

    <div class="input-append form-group" onclick="$('.input-file').click();">
        <input type="text" name="subfile" class="input-medium subfile form-control" placeholder="Выбор файла .xls, .xlsx">
        <a class="btn btn-primary">Обзор</a>
    </div>

    <p>
        <button type="submit" name="submit" class="btn btn-primary">Загрузить файл</button>
    </p>
</form>
