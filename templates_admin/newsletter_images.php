<?php
/**
 * @var string $date
 * @var bool $success
 * @var array $errors
 * @var array $uploaded_images
 * @var string $images_dir
 * @var string $upload_dir_error
 */
if (!empty($success)) {
?>
    <div class="alert alert-block bg-success">Изображения загружены</div>
<?php
}

if (!empty($uploaded_images)) {
?>
    <h4>Загруженные изображения</h4>
<?php
    for ($i = 0, $n = count($uploaded_images); $i < $n; ++$i) {
        if ($uploaded_images[$i] === '.' || $uploaded_images[$i] === '..') {
            continue;
        }
?>
        <p>
            <a target="_blank" href="<?= DOMAIN_FULL . '/images/mail/' . $images_dir . '/' . $uploaded_images[$i] ?>"><?= DOMAIN_FULL . '/images/mail/' . $images_dir . '/' . $uploaded_images[$i] ?></a>
        </p>
<?php
    }
}

if (!empty($upload_dir_error)) {
?>
    <div class="alert alert-block bg-warning">Папка <strong><?= $upload_dir_error ?></strong> не существует!</div>
<?php } ?>
<form method="POST" enctype="multipart/form-data">
    <div class="form-group<?= !empty($errors['date']) ? ' has-error' : '' ?>">
        <label class="control-label" for="uploadDate">Дата рассылки</label>
        <input type="text" name="date" class="form-control" id="uploadDate" value="<?= h($date) ?>" placeholder="2016-10-25" required="required">
        <?php if (!empty($errors['date'])) { ?>
            <div class="form-error"><?= $errors['date'] ?></div>
        <?php } ?>
    </div>
    <div class="form-group<?= !empty($errors['images']) ? ' has-error' : '' ?>">
        <label class="control-label" for="uploadFiles">Загрузить изображения</label>
        <input type="file" name="images[]" id="uploadFiles" multiple>
        <?php if (!empty($errors['images'])) { ?>
            <div class="form-error"><?= $errors['images'] ?></div>
        <?php } ?>
    </div>
    <button type="submit" class="btn btn-primary">Загрузить / Посмотреть загруженные</button>
</form>
