<?php
/**
 * @var array $content
 */
?>
<form method="POST">
    <h4>Компенсации за доставку</h4>

    <div id="compensations">
        <?php
        for ($i = 0, $n = count($content['compensations']); $i < $n; ++$i) {
            Template::partial('region_shipping_compensation', $content['compensations'][$i]);
        }
        ?>
    </div>

    <div class="form-group">
        <button id="add-compensation" class="btn btn-sm btn-success">Добавить</button>
    </div>

    <div class="form-group">
        <input type="submit" class="btn btn-primary" value="Сохранить">
    </div>
</form>

<div id="compensation-tmpl">
<?php
Template::partial('region_shipping_compensation');
?>
</div>
