<?php
/**
 * @var array $content
 */
?>
<a href="/admin/prod_cert/edit" class="btn btn-success">
    Создать сертификат
</a>

<?php
if ($content['certificates']) {
    $paginationHTML = !empty($Pagination) ? Template::get_tpl('pagination', [
        'Pagination' => $Pagination,
    ]) : '';

    $sort = isset($_GET['sort']) ? $_GET['sort'] : null;
    $sortDir = isset($_GET['sort_dir']) ? $_GET['sort_dir'] : null;

    echo $paginationHTML;
?>
    <table class="pages table table-striped">
        <thead>
            <tr class="head">
                <th>ID</th>
                <th>Название</th>
                <th>
                    <a href="?sort=files&sort_dir=<?= !$sort || $sort !== 'files' || $sort === 'files' && ($sortDir === 'desc' || !$sortDir) ? 'asc' : 'desc' ?>">
                        Количество файлов
                        <?php
                        if ($sort === 'files') {
                            echo $sortDir === 'asc' ? '&#8593;' : '&#8595;';
                        }
                        ?>
                    </a>
                </th>
                <th>

                    <a href="?sort=products&sort_dir=<?= !$sort || $sort !== 'products' || $sort === 'products' && ($sortDir === 'desc' || !$sortDir) ? 'asc' : 'desc' ?>">
                        Количество товаров
                        <?php
                        if ($sort === 'products') {
                            echo $sortDir === 'asc' ? '&#8593;' : '&#8595;';
                        }
                        ?>
                    </a>
                </th>
                <th>
                    <a href="?sort=end_date&sort_dir=<?= !$sort || $sort !== 'end_date' || $sort === 'end_date' && ($sortDir === 'desc' || !$sortDir) ? 'asc' : 'desc' ?>">
                        Дата окончания
                        <?php
                        if ($sort === 'end_date') {
                            echo $sortDir === 'asc' ? '&#8593;' : '&#8595;';
                        }
                        ?>
                    </a>
                </th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
        <?php for ($i = 0, $n = count($content['certificates']); $i < $n; ++$i) { ?>
            <tr>
                <td>
                    <?= $content['certificates'][$i]['id'] ?>
                </td>
                <td>
                    <a href="/admin/prod_cert/edit/<?= $content['certificates'][$i]['id'] ?>">
                        <?= $content['certificates'][$i]['name'] ?>
                    </a>
                </td>
                <td>
                    <?= $content['certificates'][$i]['files'] ?>
                </td>
                <td>
                    <?= $content['certificates'][$i]['products'] ?>
                </td>
                <td>
                    <?= $content['certificates'][$i]['end_date'] ?: '-нет-' ?>
                </td>
                <td>
                    <a href="/admin/prod_cert/delete/<?= $content['certificates'][$i]['id'] ?>" class="btn btn-sm btn-danger btn-confirm" data-confirm="Удалить сертификат и все его файлы?">
                        Удалить
                    </a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php
    echo $paginationHTML;
}
