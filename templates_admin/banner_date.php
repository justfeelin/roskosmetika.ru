<?php
/**
 * @var array $date
 */
if (!isset($date)) {
    $date = [
        'begin' => '',
        'end' => '',
    ];
}
?>
<div class="row banner-date">
    <div class="col-xs-4 col-md-3 col-lg-2">
        <div class="form-group">
            <label>Начало</label>
            <input type="text" class="form-control dt-input" name="dates_begin[]" value="<?= h($date['begin']) ?>">
        </div>
    </div>
    <div class="col-xs-4 col-md-3 col-lg-2">
        <div class="form-group">
            <label>Окончание</label>
            <input type="text" class="form-control dt-input" name="dates_end[]" value="<?= h($date['end']) ?>">
        </div>
    </div>
    <div class="col-xs-4">
        <br>
        <button class="btn btn-sm btn-danger delete-date-btn" title="Удалить">&times;</button>
    </div>
</div>
