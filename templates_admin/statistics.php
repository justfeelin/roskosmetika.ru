<?php
/**
 * @var array $content
 */
$dtAddedFrom = isset($_GET['date_added_from']) ? $_GET['date_added_from'] : date('Y-m-01');
$dtAddedTo = isset($_GET['date_added_to']) ? $_GET['date_added_to'] : date('Y-m-d');

$dtDeliveryFrom = isset($_GET['date_delivery_from']) ? $_GET['date_delivery_from'] : date('Y-m-01');
$dtDeliveryTo = isset($_GET['date_delivery_to']) ? $_GET['date_delivery_to'] : date('Y-m-d');

$type = isset($_GET['stat_type']) ? $_GET['stat_type'] : 'orders';
?>
<form method="GET" id="stat-form">
    <div id="by-date-filter">
        <div class="radio">
            <label>
                <input type="radio" class="by-date-chb added" name="by_delivery_date" value=""<?= !isset($_GET['by_delivery_date']) || !$_GET['by_delivery_date'] ? ' checked="checked"' : '' ?>> По дате создания
            </label>
        </div>

        <div class="radio">
            <label>
                <input type="radio" class="by-date-chb delivery" name="by_delivery_date" value="1"<?= !empty($_GET['by_delivery_date']) ? ' checked="checked"' : '' ?>> По дате доставки
            </label>
        </div>
    </div>

    <div class="date-filter added">
        <h3>Дата создания</h3>

        <div class="form-inline dates-box">
            <div class="form-group">
                <label for="date-added-from"></label>
                <input type="text" name="date_added_from" class="form-control date-input" placeholder="С" id="date-added-from" value="<?= h($dtAddedFrom) ?>">
            </div>

            <div class="form-group" id="day_to">
                <label for="date-added-to"></label>
                <input type="text" name="date_added_to" class="form-control date-input" placeholder="До" id="date-added-to" value="<?= h($dtAddedTo) ?>">
            </div>

            <div class="form-group">
                <a href="#" class="btn btn-danger clear-date-btn" title="Очистить">×</a>
            </div>
        </div>
    </div>

    <div class="date-filter delivery">
        <div id="delivery-box">
            <h3>Дата доставки</h3>

            <div class="form-inline dates-box">
                <div class="form-group">
                    <label for="date-delivery-from"></label>
                    <input type="text" name="date_delivery_from" class="form-control date-input" placeholder="С" id="date-delivery-from" value="<?= h($dtDeliveryFrom) ?>">
                </div>

                <div class="form-group">
                    <label for="date-delivery-to"></label>
                    <input type="text" name="date_delivery_to" class="form-control date-input" placeholder="До" id="date-delivery-to" value="<?= h($dtDeliveryTo) ?>">
                </div>

                <div class="form-group">
                    <a href="#" class="btn btn-danger clear-date-btn" title="Очистить">×</a>
                </div>
            </div>
        </div>
    </div>

    <h3>Тип отчёта</h3>

    <div class="radio">
        <label>
            <input type="radio" name="stat_type" value="orders"<?= $type === 'orders' ? ' checked="checked"' : '' ?>>
            Заказы
        </label>
    </div>

    <div class="radio">
        <label>
            <input type="radio" name="stat_type" value="promo" class="no-delivery"<?= $type === 'promo' ? ' checked="checked"' : '' ?>>
            Промо-коды
        </label>
    </div>

    <div class="radio">
        <label>
            <input type="radio" name="stat_type" value="sku_sales" class="no-delivery"<?= $type === 'sku_sales' ? ' checked="checked"' : '' ?>>
            Продажи все SKU за период. Не более 30 дней.
        </label>
    </div>

    <div class="radio">
        <label>
            <input type="radio" name="stat_type" value="cart_stat" class="no-delivery"<?= $type === 'cart_stat' ? ' checked="checked"' : '' ?>>
            Корзины
        </label>
    </div>

    <div class="radio">
        <label>
            <input type="radio" name="stat_type" value="trademarks"<?= $type === 'trademarks' ? ' checked="checked"' : '' ?> id="type-tm">
            Торговые марки
        </label>
    </div>

    <div class="radio">
        <label>
            <input type="radio" name="stat_type" value="clients"<?= $type === 'clients' ? ' checked="checked"' : '' ?>>
            Клиенты
        </label>
    </div>

    <div class="radio">
        <label>
            <input type="radio" name="stat_type" class="no-delivery" value="days"<?= $type === 'days' ? ' checked="checked"' : '' ?>>
            По дням
        </label>
    </div>

    <div class="checkbox">
        <label>
            <input type="hidden" name="to_excel" value="">
            <input type="checkbox" name="to_excel" value="1"<?= !isset($_GET['to_excel']) || $_GET['to_excel'] ? ' checked="checked"' : '' ?>>
            Экспорт в Excel
        </label>
    </div>

    <input type="submit" class="btn btn-primary" value="OK">
</form>

<?php
if ($content['stats']) {
    $fieldsN = count($content['fields']);
?>
    <div>
        <br>
        <a class="btn btn-success" href="<?= $_SERVER['REQUEST_URI'] . '&to_excel=1' ?>">Скачать отчёт в Excel</a>
    </div>
    <div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <?php for ($i = 0; $i < $fieldsN; ++$i) { ?>
                        <th><?= $content['fields'][$i]['name'] ?></th>
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
                <?php for ($i = 0, $n = count($content['stats']); $i < $n; ++$i) { ?>
                    <tr>
                        <?php for ($y = 0; $y < $fieldsN; ++$y) { ?>
                            <td>
                                <?= h($content['stats'][$i][$content['fields'][$y]['field']]) ?>
                            </td>
                        <?php } ?>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
<?php
} else {
    if ($content['stats'] !== null) {
?>
        <h4>- Ничего не найдено -</h4>
<?php
    }
}
