<?php
/**
 * @var array $content
 */
?>
<div>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Название</th>
            <th>URL</th>
            <th>&lt;title&gt;</th>
        </tr>
        </thead>
        <tbody>
        <?php for ($i = 0, $n = count($content['tms']); $i < $n; ++$i) { ?>
            <tr>
                <td>
                    <?= $content['tms'][$i]['id'] ?>
                </td>
                <td>
                    <a href="/admin/seo/tm/<?= $content['tms'][$i]['id'] ?>"><?= h($content['tms'][$i]['name']) ?></a>
                </td>
                <td>
                    <?= h($content['tms'][$i]['url']) ?>
                </td>
                <td>
                    <?= h($content['tms'][$i]['title']) ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
