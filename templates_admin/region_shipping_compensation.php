<?php
/**
 * @var int $id
 * @var int $minimal_sum
 * @var int $compensation
 * @var int $compensation_1
 * @var int $compensation_2
 * @var int $compensation_3
 * @var int $compensation_4
 * @var int $compensation_6
 */
$new = !isset($id);

if ($new) {
    $id = $minimal_sum = $compensation = $compensation_1 = $compensation_2 = $compensation_3 = $compensation_4 = $compensation_6 = '';
}
?>
<div class="form-group compensation-holder"<?= $new ? ' style="display: none"' : '' ?>>
    <div class="row">
        <input type="hidden" name="data[ids][]" value="<?= h($id) ?>" class="compensation-id">

        <div class="col-xs-4">
            <label>Минимальная сумма заказа</label>
            <input type="text" class="form-control" name="data[sums][]" value="<?= h($minimal_sum) ?>">
        </div>
        <div class="col-xs-4">
            <br>
            <button class="btn btn-sm btn-danger delete-compensation" data-id="<?= h($id) ?>">Удалить</button>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h4>Компенсации по типам доставки</h4>
        </div>
    </div>
    <div class="row custom-compensations">
        <div class="col-xs-2 compensation-box">
            <label>Курьер</label>
            <input type="text" class="form-control compensation-input" name="data[compensations_1][]" value="<?= h($compensation_1) ?>"<?= $compensation_1 === null ? ' readonly="readonly"' : '' ?>>

            <div class="checkbox">
                <label>
                    <input type="checkbox" class="free-shipping-chb" value="1"<?= $compensation_1 === null ? ' checked="checked"' : '' ?>> Бесплатная доставка
                </label>
            </div>
        </div>
        <div class="col-xs-2 compensation-box">
            <label>Почта</label>
            <input type="text" class="form-control compensation-input" name="data[compensations_2][]" value="<?= h($compensation_2) ?>"<?= $compensation_2 === null ? ' readonly="readonly"' : '' ?>>

            <div class="checkbox">
                <label>
                    <input type="checkbox" class="free-shipping-chb" value="1"<?= $compensation_2 === null ? ' checked="checked"' : '' ?>> Бесплатная доставка
                </label>
            </div>
        </div>
        <div class="col-xs-2 compensation-box">
                <label>Транспортная компания</label>
            <input type="text" class="form-control compensation-input" name="data[compensations_3][]" value="<?= h($compensation_3) ?>"<?= $compensation_3 === null ? ' readonly="readonly"' : '' ?>>

            <div class="checkbox">
                <label>
                    <input type="checkbox" class="free-shipping-chb" value="1"<?= $compensation_3 === null ? ' checked="checked"' : '' ?>> Бесплатная доставка
                </label>
            </div>
        </div>
        <div class="col-xs-2 compensation-box">
            <label>PickPoint, СДЭК</label>
            <input type="text" class="form-control compensation-input" name="data[compensations_4][]" value="<?= h($compensation_4) ?>"<?= $compensation_4 === null ? ' readonly="readonly"' : '' ?>>

            <div class="checkbox">
                <label>
                    <input type="checkbox" class="free-shipping-chb" value="1"<?= $compensation_4 === null ? ' checked="checked"' : '' ?>> Бесплатная доставка
                </label>
            </div>
        </div>
        <div class="col-xs-2 compensation-box">
            <label>Самовывоз</label>
            <input type="text" class="form-control compensation-input" name="data[compensations_6][]" value="<?= h($compensation_6) ?>"<?= $compensation_6 === null ? ' readonly="readonly"' : '' ?>>

            <div class="checkbox">
                <label>
                    <input type="checkbox" class="free-shipping-chb" value="1"<?= $compensation_6 === null ? ' checked="checked"' : '' ?>> Бесплатная доставка
                </label>
            </div>
        </div>
    </div>
</div>
