<div class="row">
    <p>Для получения или обновления токена API YandeX маркета - нажмите кнопку. После вывода на экран токена, работа с API будет доступна.</p>
</div>
<div class="row">
    <p><a href="/admin/yamarket/token"><button type="button" class="btn btn-primary btn-lg">Получить токен</button></a></p>
</div>