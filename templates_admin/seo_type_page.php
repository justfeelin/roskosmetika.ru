<?php
/**
 * @var array $content
 */
$newItem = !isset($content['item']['id']);

if (!$newItem) {
?>
    <h2><?= $content['item']['name'] ?></h2>

    <a target="_blank" href="<?= DOMAIN_FULL . '/kosmetika/' . $content['item']['url'] ?>"><?= DOMAIN_FULL . '/kosmetika/' . $content['item']['url'] ?></a>
<?php } ?>

<form method="POST" action="/admin/seo/type_pages/<?= $newItem ? 'new' : $content['item']['id'] ?>" enctype="multipart/form-data">
    <div class="form-group">
        <label for="item-url">URL</label>
        <input type="text" class="form-control" name="item[url]" id="item-url" value="<?= h($content['item']['url']) ?>" required="required">
    </div>

    <div class="form-group">
        <label for="item-name">Название</label>
        <input type="text" class="form-control" name="item[name]" id="item-name" value="<?= h($content['item']['name']) ?>" required="required">
    </div>

    <div class="form-group">
        <label for="item-h1">&lt;h1&gt;</label>
        <input type="text" class="form-control" name="item[h1]" id="item-h1" value="<?= h($content['item']['h1']) ?>">
    </div>

    <div class="form-group">
        <label for="item-title">&lt;title&gt;</label>
        <textarea class="form-control" name="item[title]" id="item-title" rows="5"><?= h($content['item']['title']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="item-description">&lt;meta name="description"&gt;</label>
        <textarea class="form-control" name="item[description]" id="item-description" rows="5"><?= h($content['item']['description']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="item-text">Текст на странице</label>
        <textarea class="form-control" name="item[text]" id="item-text" rows="5"><?= h($content['item']['text']) ?></textarea>
    </div>

    <?php if ($content['item']['image']) { ?>
        <img src="/images/kosmetika/<?= $content['item']['image'] ?>">
    <?php } ?>

    <div class="form-group">
        <label for="item-image">Изображение</label>
        <input type="file" id="item-image" name="image">
        <p class="help-block">Загрузить изображение (50x50)</p>
    </div>

    <div class="form-group">
        <label for="item-name-on-products">Отображать название раздела для первых N товаров</label>
        <select name="item[name_on_products]" class="form-control" id="item-name-on-products">
            <?php for ($i = 0; $i <= 40; $i += 10) { ?>
                <option value="<?= $i ?>"<?= $content['item']['name_on_products'] == $i ? ' selected="selected"' : '' ?>><?= $i ?></option>
            <?php } ?>
        </select>
    </div>

    <input type="submit" class="btn btn-primary" value="Сохранить">
</form>
