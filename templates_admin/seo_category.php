<?php
/**
 * @var array $content
 */

$url = DOMAIN_FULL . ($content['category']['type_page_id'] ? '/kosmetika/' . $content['category']['type_page_url'] . '/' : '/category/') . $content['category']['_full_url'];
?>
<h2><?= $content['category']['name'] ?></h2>

<a target="_blank" href="<?= $url ?>"><?= $url ?></a>

<form method="POST" action="/admin/seo/categories/<?= $content['category']['id'] ?>">
    <div class="form-group">
        <label for="category-h1">&lt;h1&gt;</label>
        <input type="text" class="form-control" name="category[h1]" id="category-h1" value="<?= h($content['category']['h1']) ?>">
    </div>

    <?php if ($content['category']['is_tag']) { ?>
        <div class="form-group">
            <label for="category-tag-name">Название тега</label>
            <input class="form-control" name="category[tag_name]" id="category-tag-name" type="text" value="<?= h($content['category']['tag_name']) ?>">
        </div>

        <div class="form-group">
            <label for="category-anchor">Анкор для тега</label>
            <input class="form-control" name="category[anchor]" id="category-anchor" type="text" value="<?= h($content['category']['name']) ?>">
        </div>

        <div class="form-group">
            <strong>
                Связки «Тег + бренд» (/category-tm/)
            </strong>
            <?php for ($i = 0, $n = count($content['tms']); $i < $n; ++$i) { ?>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="category[tm_ids][]" value="<?= $content['tms'][$i]['id'] ?>"<?= in_array($content['tms'][$i]['id'], $content['selected_tms']) ? ' checked="checked"' : '' ?>> <?= $content['tms'][$i]['name'] ?>
                    </label>
                </div>
            <?php } ?>
        </div>
    <?php } ?>

    <div class="form-group">
        <label for="category-title">&lt;title&gt;</label>
        <textarea class="form-control" name="category[title]" id="category-title" rows="5"><?= h($content['category']['title']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="category-description">&lt;meta name="description"&gt;</label>
        <textarea class="form-control" name="category[description]" id="category-description" rows="5"><?= h($content['category']['description']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="category-keywords">&lt;meta name="keywords"&gt;</label>
        <textarea class="form-control" name="category[keywords]" id="category-keywords" rows="5"><?= h($content['category']['keywords']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="category-desc">Текст на странице</label>
        <textarea class="form-control" name="category[desc]" id="category-desc" rows="5"><?= h($content['category']['desc']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="category-name-on-products">Отображать название раздела для первых N товаров</label>
        <select name="category[name_on_products]" class="form-control" id="category-name-on-products">
            <?php for ($i = 0; $i <= 40; $i += 10) { ?>
                <option value="<?= $i ?>"<?= $content['category']['name_on_products'] == $i ? ' selected="selected"' : '' ?>><?= $i ?></option>
            <?php } ?>
        </select>
    </div>

    <input type="submit" class="btn btn-primary" value="Сохранить">
</form>
