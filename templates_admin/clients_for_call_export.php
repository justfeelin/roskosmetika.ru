<?php
/**
 * @var string $msg
 * @var array $content
 */

if  (isset($msg)) { ?>
    <div class="alert alert-block"><?= $msg ?></div>
<?php } ?>
<form action="/admin/export/clients_call" method="POST">

    <div class="row">
        <div class="col-xs-4">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Количество клиентов" name="c_limit">
            </div>
        </div>
        <div class="col-xs-4">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Количество менеджеров" name="managers">
            </div>
        </div>
        <div class="col-xs-4">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Дней без заказов" name="days">
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-xs-12">
            <span class="help-block"><strong>Дополнительно</strong></span>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='org'>&nbsp;Только&nbsp;юр.&nbsp;лица
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='info_all'>&nbsp;Все&nbsp;столбцы
                </label>
            </div>
        </div>
    </div>
    <br>
    <br>

    <div class="row">
        <button type="submit" class="btn btn-primary" name="choise">Загрузить выборку</button>
    </div>
</form>