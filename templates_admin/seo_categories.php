<?php
/**
 * @var array $content
 * @var array $Pagination
 */

$q = isset($_GET['q']) ? $_GET['q'] : null;
?>
<form method="GET">
    <div class="form-group">
        <label for="category-q">Поиск</label>
        <input type="text" name="q" class="form-control" placeholder="Название, URL" id="category-q" value="<?= $q ? h($q) : '' ?>">
    </div>
    <div class="form-group">
        <label>
            <input type="checkbox" value="1" name="tag" <?= $content['tags_only'] ? ' checked="checked"' : '' ?>>
            Искать только среди тегов
        </label>
    </div>
    <input type="submit" class="btn btn-primary" value="Искать">
</form>
<?php
$paginationHTML = Template::get_tpl('pagination', [
    'Pagination' => $Pagination,
]);

echo $paginationHTML;
?>
<div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Название</th>
                <th>URL</th>
                <th>Тег</th>
                <th class="text-nowrap">Название тега</th>
                <th>&lt;title&gt;</th>
                <th>&lt;meta name="description"&gt;</th>
            </tr>
        </thead>
        <tbody>
            <?php for ($i = 0, $n = count($content['categories']); $i < $n; ++$i) { ?>
                <tr>
                    <td>
                        <?= $content['categories'][$i]['id'] ?>
                    </td>
                    <td>
                        <a href="/admin/seo/categories/<?= $content['categories'][$i]['id'] ?>"><?= highlight_query(h($content['categories'][$i]['name']), $q) ?></a>
                    </td>
                    <td>
                        <?= highlight_query(h($content['categories'][$i]['_full_url']), $q) ?>
                    </td>
                    <td>
                        <input type="checkbox" disabled="disabled"<?= $content['categories'][$i]['is_tag'] ? ' checked="checked"' : ''?>>
                    </td>
                    <td>
                        <?= highlight_query(h($content['categories'][$i]['tag_name']), $q) ?>
                    </td>
                    <td>
                        <?= h($content['categories'][$i]['title']) ?>
                    </td>
                    <td>
                        <?= h($content['categories'][$i]['description']) ?>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<?php
echo $paginationHTML;
