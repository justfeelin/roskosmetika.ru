<?php
/**
 * @var array $content
 */
?>
<table class="pages table table-striped">
    <thead>
        <tr class="head">
            <th>ID</th>
            <th>Регион</th>
        </tr>
    </thead>
    <tbody>
        <?php for ($i = 0, $n = count($content['regions']); $i < $n; ++$i) { ?>
            <tr>
                <td><?= $content['regions'][$i]['id'] ?></td>
                <td>
                    <a href="/admin/delivery_compensation/<?= $content['regions'][$i]['id'] ?>">
                        <?= $content['regions'][$i]['name'] ?>
                    </a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
