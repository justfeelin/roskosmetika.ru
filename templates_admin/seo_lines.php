<?php
/**
 * @var array $content
 * @var array $Pagination
 */
$q = isset($_GET['q']) ? $_GET['q'] : null;
?>
    <form method="GET">
        <div class="form-group">
            <label for="lines-tm">Бренд</label>
            <select name="tm_id" class="form-control" id="lines-tm">
                <?php for ($i = 0, $n = count($content['tms']); $i < $n; ++$i) { ?>
                    <option value="<?= $content['tms'][$i]['id'] ?>"<?= $content['tmID'] == $content['tms'][$i]['id'] ? ' selected="selected"' : '' ?>><?= $content['tms'][$i]['name'] ?></option>
                <?php } ?>
            </select>
        </div>
        <input type="submit" class="btn btn-primary" value="Выбрать бренд">
    </form>
<?php
if ($content['lines'] !== null) {
    if ($content['lines']) {
        ?>
        <div>
            <h2>Коллекции</h2>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Название</th>
                    <th>URL</th>
                    <th>&lt;title&gt;</th>
                    <th>&lt;meta name="description"&gt;</th>
                </tr>
                </thead>
                <tbody>
                <?php for ($i = 0, $n = count($content['lines']); $i < $n; ++$i) { ?>
                    <tr>
                        <td>
                            <?= $content['lines'][$i]['id'] ?>
                        </td>
                        <td>
                            <a href="/admin/seo/lines/<?= $content['lines'][$i]['id'] ?>"><?= highlight_query(h($content['lines'][$i]['name']), $q) ?></a>
                        </td>
                        <td>
                            <?= highlight_query(h($content['lines'][$i]['url']), $q) ?>
                        </td>
                        <td>
                            <?= h($content['lines'][$i]['h1']) ?>
                        </td>
                        <td>
                            <?= h($content['lines'][$i]['title']) ?>
                        </td>
                        <td>
                            <?= h($content['lines'][$i]['description']) ?>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
<?php
    } else {
?>
    <h4>Коллекции не найдены</h4>
<?php
    }
}
