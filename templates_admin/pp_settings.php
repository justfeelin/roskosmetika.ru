<?php
/**
 * @var string $header
 * @var array $content
 */
?>
<div>
    <form action="/admin/partner/settings/" method="post">
        <h1><?= $header ?></h1>

        <div class="form-group">
            <label for="partner-name">Название компании:</label>
            <input type="text" id="partner-name" class="form-control" name="name" value="<?= $content['name'] ?>">
        </div>

        <div class="form-group">
            <label for="partner-url">URL сайта компани:</label>
            <input type="text" id="partner-url" class="form-control" name="url" value="<?= $content['file_name'] ?>">
        </div>

        <div class="form-group">
            <label for="partner-shop-name">Название магазина:</label>
            <input type="text" id="partner-shop-name" class="form-control" name="shop_name" value="<?= $content['catalog'] ?>">
        </div>

        <div class="form-group">
            <label for="partner-phone">Телефон:</label>
            <input type="text" id="partner-phone" class="form-control" name="phone" value="<?= $content['utm_medium'] ?>">
        </div>

        <div class="form-group">
            <label for="partner-currency">Валюта:</label>
            <input type="text" id="partner-currency" class="form-control" name="currency" value="<?= $content['utm_source'] ?>">
        </div>

        <div class="form-group">
            <label for="partner-delivery">Цена доставки:</label>
            <input type="text" id="partner-delivery" class="form-control" name="delivery" value="<?= $content['catalog'] ?>">
        </div>

        <div class="form-group">
            <label for="partner-sales-notes">Общие примечания к заказам:</label>
            <textarea name="sales_notes" id="partner-sales-notes" class="form-control"><?= $content['utm_campaign'] ?></textarea>
        </div>

        <button type="submit" name="save" class="btn btn-primary">Сохранить</button>
    </form>
</div>
