<?php
/**
 * @var string $msg
 */
if (isset($msg)) { ?>
    <div class="alert alert-block"><?= $msg ?></div>
<?php } ?>
<form action="/admin/export/lines" method="POST">
    <div class="row">
        <div class="col-xs-3">
            <span class="help-block"><strong>Описания&nbsp;линеек</strong></span>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='synonym'>&nbsp;без&nbsp;синонимов
                </label>
            </div>
        </div>

        <div class="col-xs-3">
            <span class="help-block"><strong>Описания страниц</strong></span>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='title'>&nbsp;без&nbsp;&lt;title&gt;
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='seo_desc'>&nbsp;без&nbsp;&lt;description&gt;
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='keywords'>&nbsp;без&nbsp;&lt;keywords&gt;
                </label>
            </div>
        </div>

        <div class="col-xs-3">
            <span class="help-block"><strong>Логотип</strong></span>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='src'>&nbsp;название&nbsp;по&nbsp;умолчанию
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='alt'>&nbsp;без&nbsp;alt
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='photo_title'>&nbsp;без&nbsp;title
                </label>
            </div>
        </div>
    </div>
    <br>
    <br>

    <div class="row">
        <button type="submit" class="btn btn-primary" name="choise">Загрузить выборку</button>
        <button type="submit" class="btn btn-primary" name="all">Загрузить все</button>
        <button type="submit" class="btn btn-primary" name="blank">Загрузить бланк</button>
    </div>
</form>
