<?php
/**
 * @var array $content
 */
$backParams = '?bu=' . rawurlencode($_SERVER['REQUEST_URI']);
?>
<table class="table table-striped">
    <thead>
        <tr>
            <th>ID</th>
            <th>
                <a href="/admin/tags">Уровень</a>
            </th>
            <th>Название</th>
            <th>URL</th>
            <th>
                <a href="?sort_products=<?= isset($_GET['sort_products']) && $_GET['sort_products'] === 'asc' ? 'desc' : 'asc' ?>">Количество товаров</a>
            </th>
            <th>&nbsp;</th>
            <th>Видимость</th>
        </tr>
    </thead>
    <tbody>
        <?php for ($i = 0, $n = count($content['tags']); $i < $n; ++$i) { ?>
            <tr>
                <td>
                    <a href="/admin/tags/<?= $content['tags'][$i]['id'] ?>"><?= $content['tags'][$i]['id'] ?></a>
                </td>
                <td>
                    <?= $content['tags'][$i]['level'] ?>
                </td>
                <td>
                    <?= $content['tags'][$i]['name'] ?>
                </td>
                <td>
                    <a href="<?= DOMAIN_FULL . '/category/' . $content['tags'][$i]['_full_url'] ?>"><?= DOMAIN_FULL . '/category/' . $content['tags'][$i]['_full_url'] ?></a>
                </td>
                <td>
                    <?= $content['tags'][$i]['products'] ?>
                </td>
                <td>
                    <a href="/admin/tags/clear_products/<?= $content['tags'][$i]['id'] . $backParams ?>" class="btn btn-danger clear-products-btn">Очистить</a>
                </td>
                <td>
                    <a href="/admin/tags/toggle_visibility/<?= $content['tags'][$i]['id'] . $backParams ?>"><?= $content['tags'][$i]['visible'] ? 'Скрыть' : 'Показать' ?></a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
