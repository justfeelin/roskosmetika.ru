<ul class="nav">
    <li class="nav-header">Роскосметика</li>
    <li><a href="/admin/import/categories">Категории</a></li>
    <li><a href="/admin/import/groups">Группы категорий</a></li>
    <li><a href="/admin/import/products">Товары</a></li>
    <li><a href="/admin/import/lines">Линейки</a></li>
    <li><a href="/admin/import/sets">Наборы</a></li>
    <li><a href="/admin/import/filters">Фильтры</a></li>
    <li><a href="/admin/import/tm">Торговые марки</a></li>
    <li><a href="/admin/import/tm_in_cat">Торговые марки в категориях</a></li>
    <li><a href="/admin/import/delivery">Стоимость доставки</a></li>
    <li><a href="/admin/article_prod/">Связи статья-продукт</a></li>
    <li><a href="/admin/yandex_catalog/">Связка Яндекс каталога</a></li>
</ul>
