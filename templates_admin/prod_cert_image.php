<?php
/**
 * @var int $id
 * @var string $file_name
 * @var string $folder_url
 */
$id = isset($id) ? $id : null;
$file_name = isset($file_name) ? $file_name : null;
?>
<div class="row form-group cert-image"<?= !$id ? ' style="display: none"' : '' ?>>
    <div class="col-xs-1">
        <input type="hidden" name="data[images_ids][]" value="<?= h($id) ?>">

        <?php if ($id) { ?>
            <img class="img-file" src="<?= ProdCert::FOLDERS_URL . '/' . $folder_url . '/' . ($file_name ? $file_name . '?r=' . microtime(true) * 10000 : 'empty') ?>" style="max-width: 100%">
        <?php } ?>
    </div>
    <div class="col-xs-2 col-md-3">
        Загрузить изображение:
        <input type="file" name="images_files[]">
    </div>
    <div class="col-xs-3">
        <a href="#" class="btn btn-sm btn-danger btn-delete" data-id="<?= h($id) ?>">Удалить</a>
    </div>
</div>
