<?php
/**
 * @var array $content
 */
$newItem = !isset($content['item']['id']);
if (!$newItem) {
?>
    <h2><?= $content['item']['name'] ?></h2>

    <a target="_blank" href="<?= DOMAIN_FULL . '/country/' . $content['item']['url'] ?>"><?= DOMAIN_FULL . '/country/' . $content['item']['url'] ?></a>

<?php } ?>

<form method="POST" action="/admin/country/<?= $newItem ? 'new' : $content['item']['id'] ?>" enctype="multipart/form-data">
    <div class="form-group">
        <label for="item-url">URL</label>
        <input type="text" class="form-control" name="item[url]" id="item-url" value="<?= h($content['item']['url']) ?>" required="required">
    </div>

    <div class="checkbox">
        <label>
            <input type="checkbox" name="item[visible]" id="item-visible" <?= !empty($content['item']['visible']) ? 'checked="checked"' : '' ?>> Отобразить
        </label>
    </div>

    <div class="form-group">
        <label for="item-name">Название</label>
        <input type="text" class="form-control" name="item[name]" id="item-name" value="<?= h($content['item']['name']) ?>" required="required">
    </div>

    <div class="form-group">
        <label for="item-h1">h1</label>
        <input type="text" class="form-control" name="item[h1]" id="item-h1" value="<?= h($content['item']['h1']) ?>">
    </div>

    <div class="form-group">
        <label for="item-title">title</label>
        <textarea class="form-control" name="item[title]" id="item-title" rows="5"><?= h($content['item']['title']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="item-description">description</label>
        <textarea class="form-control" name="item[description]" id="item-description" rows="5"><?= h($content['item']['description']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="item-keywords">keywords</label>
        <textarea class="form-control" name="item[keywords]" id="item-keywords" rows="5"><?= h($content['item']['keywords']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="item-text">Текст на странице</label>
        <textarea class="form-control" name="item[text]" id="item-text" rows="5"><?= h($content['item']['text']) ?></textarea>
    </div>

    <?php if ($content['item']['image']) { ?>
        <img src="/images/country/<?= $content['item']['image'] ?>">
    <?php } ?>

    <div class="form-group">
        <label for="item-image">Изображение</label>
        <input type="file" id="item-image" name="image">
        <p class="help-block">Загрузить изображение (50x50)</p>
    </div>

    <input type="submit" class="btn btn-primary" value="Сохранить">
</form>
