<?php
/**
 * @var array $Pagination
 * @var array $content
 */
$paginationHTML = Template::get_tpl('pagination', [
    'Pagination' => $Pagination,
]);

echo $paginationHTML;
?>
<table class="table table-striped table-hover">
    <thead>
    <tr>
        <th width="70">Номер</th>
        <th width="100">ФИО</th>
        <th width="100">Регион</th>
        <th width="100">Телефон</th>
        <th width="180">E-mail</th>
    </tr>
    </thead>
    <tbody>
        <?php for ($i = 0, $n = count($content['users']); $i < $n; ++$i) { ?>
            <tr>
                <td class="al_c"><?= $content['users'][$i]['id'] ?>-R</td>
                <td><a href="/admin/users/<?= $content['users'][$i]['id'] ?>"><?= $content['users'][$i]['surname'] . '&nbsp;' . $content['users'][$i]['name'] . '&nbsp;' . $content['users'][$i]['patronymic'] ?></a></td>
                <td><?= $content['users'][$i]['region'] ?></td>
                <td><?= $content['users'][$i]['phone'] ?></td>
                <td><?= $content['users'][$i]['email'] ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<?php
echo $paginationHTML;
