<?php
/**
 * @var array $content
 */
?>
<div id="categories">
    <div class="row">
        <div class="level_1 col-xs-12 parent" data-id="0" data-level="1">
            <div class="row">
                <?php for ($i = 0, $n = count($content['lvl_1']); $i < $n; ++$i) { ?>
                    <div class="col-xs-1 level_1_item" data-id="<?= $content['lvl_1'][$i]['id'] ?>">
                        <a href="#" data-id="<?= $content['lvl_1'][$i]['id'] ?>" class="cat_link"><?= $content['lvl_1'][$i]['name'] ?></a>
                        <br>
                        <button
                            class="download fa fa-cloud-download"
                            title="Экспортировать категорию 1 уровня в Excel"
                            value="/admin/export/cat_structure/<?= $content['lvl_1'][$i]['id'] ?>/cat"></button>
                    </div>
                <?php } ?>

                <div class="col-xs-1">
                    <button class="new_cat fa fa-plus" title="Добавить категорию 1 уровня"></button>

                    <form id="add-form-0" class="add-form" action="/admin/categories/add_category">
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" placeholder="Название категории" required="required">
                        </div>
                        <input type="hidden" value="0">
                        <input type="submit" class="btn btn-primary" value="Добавить">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <?php for ($i = 0; $i < $n; ++$i) { ?>
        <div class="level_2 col-xs-12 sublevel parent" data-id="<?= $content['lvl_1'][$i]['id'] ?>" data-level="2">
            <div class="row">
                <?php
                if (isset($content['lvl_2'][$content['lvl_1'][$i]['id']])) {
                    for ($y = 0, $m = count($content['lvl_2'][$content['lvl_1'][$i]['id']]); $y < $m; ++$y) {
                        ?>
                        <div
                            class="sub_<?= $content['lvl_2'][$content['lvl_1'][$i]['id']][$y]['id'] ?> col-xs-2 level_2_item">
                            <h5><?= $content['lvl_2'][$content['lvl_1'][$i]['id']][$y]['name'] ?></h5>
                            <ul>
                                <?php foreach ($content['lvl_2'][$content['lvl_1'][$i]['id']][$y]['cats'] as $cat) { ?>
                                    <li>
                                        <a class="cat_link category-item<?= $cat['is_tag'] ? ' is-tag' : '' ?>"
                                           data-id="<?= $cat['id'] ?>"
                                           href="#"
                                            <?= $cat['is_tag'] ? 'title="Тег"' : '' ?>><?= $cat['name'] ?></a>
                                    </li>
                                <?php } ?>
                                <li>
                                    <button
                                        class="download fa fa-cloud-download"
                                        title="Экспортировать категорию 2 уровня в Excel"
                                        value="/admin/export/cat_structure/<?= $content['lvl_2'][$content['lvl_1'][$i]['id']][$y]['id'] ?>/sub_cat"></button>
                                    <button
                                        class="new_cat fa fa-plus"
                                        data-id="<?= $content['lvl_2'][$content['lvl_1'][$i]['id']][$y]['id'] ?>"
                                        title="Добавить категорию 2 уровня"></button>
                                </li>
                            </ul>
                        </div>
                <?php
                    }
                }
                ?>
                <div class="col-xs-1">
                    <button class="new_cat fa fa-plus" title="Добавить тип 2 уровня"></button>
                </div>
                <form id="add-form-<?= $content['lvl_1'][$i]['id'] ?>" class="add-form">
                    <?php if (isset($content['lvl_2'][$content['lvl_1'][$i]['id']])) { ?>
                        <div class="form-group">
                            <label for="subcat-id-2-<?= $content['lvl_1'][$i]['id'] ?>">Тип</label>
                            <select name="subcat_id" class="form-control subcat-id" id="subcat-id-2-<?= $content['lvl_1'][$i]['id'] ?>">
                                <?php foreach ($content['lvl_2'][$content['lvl_1'][$i]['id']] as $sub) { ?>
                                    <option value="<?= $sub['id'] ?>"><?= $sub['name'] ?></option>
                                <?php } ?>
                                <option value="0">- Новый тип -</option>
                            </select>
                        </div>
                    <?php } ?>

                    <div class="form-group new-subcat" <?= !isset($content['lvl_2'][$content['lvl_1'][$i]['id']]) ? 'style="display: block"' : '' ?>>
                    <input type="text" name="subcat" class="form-control" placeholder="Название нового типа">
                </div>

                <div class="form-group">
                    <input type="text" name="name" class="form-control" placeholder="Название категории" required="required">
                </div>

                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="is_tag" value="1"> Тег
                    </label>
                </div>

                <input type="hidden" name="parent_id" value="<?= $content['lvl_1'][$i]['id'] ?>">
                <input type="submit" class="btn btn-primary" value="Добавить">
                </form>
            </div>
        </div>
        <?php } ?>
    </div>

    <div class="row">
        <?php
        foreach ($content['lvl_2'] as $level2Subs) {
            for ($i = 0, $n = count($level2Subs); $i < $n; ++$i) {
                for ($y = 0, $m = count($level2Subs[$i]['cats']); $y < $m; ++$y) {
        ?>
        <div class="level_3 col-xs-12 sublevel parent" data-id="<?= $level2Subs[$i]['cats'][$y]['id'] ?>" data-level="3">
            <?php
            if (isset($content['lvl_3'][$level2Subs[$i]['cats'][$y]['id']])) {
                foreach ($content['lvl_3'][$level2Subs[$i]['cats'][$y]['id']] as $sub) {
            ?>
                    <div class="sub_<?= $sub['id'] ?> col-xs-2 level_3_item">
                        <h5><?= $sub['name'] ?></h5>
                        <ul>
                            <?php foreach ($sub['cats'] as $cat) { ?>
                                <li class="category-item<?= $cat['is_tag'] ? ' is-tag' : '' ?>"
                                    data-id="<?= $cat['id'] ?>" <?= $cat['is_tag'] ? 'title="Тег"' : '' ?>><?= $cat['name'] ?></li>
                            <?php } ?>
                            <li>
                                <button
                                    class="download  fa fa-cloud-download"
                                    title="Экспортировать категорию 3 уровня в Excel"
                                    data-id="<?= $sub['id'] ?>"
                                    value="/admin/export/cat_structure/<?= $sub['id'] ?>/sub_cat"></button>
                                <button class="new_cat fa fa-plus" title="Добавить категорию 3 уровня"
                                        data-id="<?= $sub['id'] ?>"></button>
                            </li>
                        </ul>
                    </div>
            <?php
                }
            }
            ?>
            <div class="col-xs-1">
                <button class="new_cat fa fa-plus" title="Добавить тип 3 уровня"></button>
            </div>
            <form id="add-form-<?= $level2Subs[$i]['cats'][$y]['id'] ?>" class="add-form">
                <?php if (isset($content['lvl_3'][$level2Subs[$i]['cats'][$y]['id']])) { ?>
                    <div class="form-group">
                        <label for="subcat-id-3-<?= $level2Subs[$i]['cats'][$y]['id'] ?>">Тип</label>
                        <select name="subcat_id" class="form-control subcat-id" id="subcat-id-3-<?= $level2Subs[$i]['cats'][$y]['id'] ?>">
                            <?php foreach ($content['lvl_3'][$level2Subs[$i]['cats'][$y]['id']] as $sub) { ?>
                                <option value="<?= $sub['id'] ?>"><?= $sub['name'] ?></option>
                            <?php } ?>
                            <option value="0">- Новый тип -</option>
                        </select>
                    </div>
                <?php } ?>

                <div class="form-group new-subcat" <?= !isset($content['lvl_3'][$level2Subs[$i]['cats'][$y]['id']]) ? 'style="display: block"' : '' ?>>
                    <input type="text" name="subcat" class="form-control" placeholder="Название нового типа">
                </div>

                <div class="form-group">
                    <input type="text" name="name" class="form-control" placeholder="Название категории" required="required">
                </div>

                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="is_tag" value="1"> Тег
                    </label>
                </div>

                <input type="hidden" name="parent_id" value="<?= $level2Subs[$i]['cats'][$y]['id'] ?>">
                <input type="submit" class="btn btn-primary" value="Добавить">
            </form>
        </div>
        <?php
                }
            }
        }
        ?>
    </div>
</div>
