<?php
/**
 * @var array $Pagination
 * @var array $content
 */
$paginationHTML = Template::get_tpl('pagination', [
    'Pagination' => $Pagination,
]);

echo $paginationHTML;
?>
<table class="table table-striped">
    <thead>
    <tr>
        <th>Дата</th>
        <th>Имя</th>
        <th>Контакты</th>
        <th>Сообщение</th>
        <th>Ресурс</th>
    </tr>
    </thead>
    <tbody>
    <?php for ($i = 0, $n = count($content['items']); $i < $n; ++$i) { ?>
        <tr>
            <td><?= Template::date('d.m.Y', $content['items'][$i]['date']) ?></td>
            <td><?= $content['items'][$i]['name'] ?></td>
            <td><?= $content['items'][$i]['contacts'] ?></td>
            <td><?= h(mb_substr($content['items'][$i]['question'], 0, 100)) ?></td>
            <td><?= $content['items'][$i]['resource'] ?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>
<?php
echo $paginationHTML;
