<?php
/**
 * @var array $content
 * @var string $header
 */
?>
<form action="/admin/partner/<?= !empty($content['id']) ? 'edit' : 'add' ?>" method="post">
    <h1><?= $header ?></h1>

    <div class="form-group">
        <label for="partner-name">Имя партнерской программы</label>
        <input type="text" id="partner-name" class="form-control" name="name" size="60" value="<?= !empty($content['id']) ? $content['name'] : '' ?>">
    </div>

    <div class="form-group">
        <label for="partner-file-name">Имя файла</label>
        <input type="text" id="partner-file-name" class="form-control" name="file_name" value="<?= !empty($content['id']) ? $content['file_name'] : '' ?>">
    </div>

    <div class="checkbox">
        <label>
            <input type="checkbox" name="equip" value="1" <?= !isset($content['id']) || $content['equip'] ? 'checked="checked"' : '' ?>> Без оборудования и БАД
        </label>
    </div>

    <div class="form-group">
        <label for="partner-utm-source">UTM source</label>
        <input type="text" id="partner-utm-source" class="form-control" name="utm_source" value="<?= !empty($content['id']) ? $content['utm_source'] : '' ?>">
    </div>

    <div class="form-group">
        <label for="partner-utm-medium">UTM medium</label>
        <input type="text" id="partner-utm-medium" class="form-control" name="utm_medium" size="30" value="<?= !empty($content['id']) ? $content['utm_medium'] : '' ?>">
    </div>

    <div class="form-group">
        <label for="partner-utm-campaign">UTM campaign</label>
        <input type="text" id="partner-utm-campaign" class="form-control" name="utm_campaign" value="<?= !empty($content['id']) ? $content['utm_campaign'] : '' ?>">
    </div>

    <fieldset>
        <legend>Основной тег (yml_catalog, torg_price):</legend>
        <div class="form-group">
            <input type="text" class="form-control" name="catalog" value="<?= !empty($content['id']) ? $content['catalog'] : '' ?>">
        </div>
        <fieldset style="position: relative;">
            <legend>&LT;shop&GT;</legend>

            <div class="form-group">
                <label for="partner-shop-name">Тег названия магазина&LT;name, shopname&GT;</label>
                <input type="text" id="partner-shop-name" class="form-control" name="shop_name" value="<?= !empty($content['id']) ? $content['shop_name'] : '' ?>">
            </div>

            <div class="form-group">
                <label for="partner-company">Тег&nbsp;названия&nbsp;компании&LT;company&GT;</label>
                <input type="text" id="partner-company" class="form-control" name="company" value="<?= !empty($content['id']) ? $content['company'] : '' ?>">
            </div>
            <div class="form-group">
                <label for="partner-shop-url">URL&nbsp;магазина&LT;url&GT;</label>
                <input type="text" id="partner-shop-url" class="form-control" name="shop_url" value="<?= !empty($content['id']) ? $content['shop_url'] : '' ?>">
            </div>

            <div class="form-group">
                <label for="partner-currency">Валюта&LT;currency&GT;</label>
                <input type="text" id="partner-currency" class="form-control" name="currency" value="<?= !empty($content['id']) ? $content['currency'] : '' ?>">
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="delivery" value="1" <?= !isset($content['id']) || $content['delivery'] ? 'checked="checked"' : '' ?>> Доставка&LT;delivery&GT;
                </label>
            </div>

            <div class="form-group">
                <label for="partner-local-delivery-cost">Стоимость доставки&LT;local_delivery_cost&GT;</label>
                <input type="text" id="partner-local-delivery-cost" class="form-control" name="local_delivery_cost" value="<?= !empty($content['id']) ? $content['local_delivery_cost'] : '' ?>">
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="categories" value="1" <?= !isset($content['id']) || $content['categories'] ? 'checked="checked"' : '' ?>> Категории&LT;categories&GT;
                </label>
            </div>

            <fieldset>
                <legend>Продукты&LT;offers&GT;</legend>
                <fieldset style="position: relative;">
                    <legend>Продукт&LT;offer&GT;</legend>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="url" value="1" <?= !isset($content['id']) || $content['url'] ? 'checked="checked"' : '' ?>> &LT;url&GT;
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="price" value="1" <?= !isset($content['id']) || $content['price'] ? 'checked="checked"' : '' ?>> &LT;price&GT;
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="picture" value="1" <?= !isset($content['id']) || $content['picture'] ? 'checked="checked"' : '' ?>> &LT;picture&GT;
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="store" value="1" <?= !isset($content['id']) || $content['store'] ? 'checked="checked"' : '' ?>> &LT;store&GT;
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="pickup" value="1" <?= !isset($content['id']) || $content['pickup'] ? 'checked="checked"' : '' ?>> &LT;pickup&GT;
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="vendor" value="1" <?= !isset($content['id']) || $content['vendor'] ? 'checked="checked"' : '' ?>> &LT;vendor&GT;
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="description" value="1" <?= !isset($content['id']) || $content['description'] ? 'checked="checked"' : '' ?>> &LT;description&GT;
                        </label>
                    </div>

                    <div class="form-group">
                        <label for="partner-sales-notes">&LT;sales_notes&GT;</label>
                        <input type="text" id="partner-sales-notes" class=form-control name="sales_notes" value="<?= !empty($content['id']) ? $content['sales_notes'] : '' ?>">
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="country_of_origin" value="1" <?= !isset($content['id']) || $content['country_of_origin'] ? 'checked="checked"' : '' ?>> &LT;country_of_origin&GT;
                        </label>
                    </div>
                </fieldset>
            </fieldset>
        </fieldset>
    </fieldset>

    <?php if (!empty($content['id'])) { ?>
        <input type="hidden" name="id" value="<?= $content['id'] ?>">
    <?php } ?>

    <button type="submit" class="btn btn-primary" name="save">Сохранить</button>
</form>
