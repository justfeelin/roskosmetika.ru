<?php
/**
 * @var array $content
 * @var array $Pagination
 */
?>
<h3>Поиск</h3>
<form id="logs-form" method="GET" action="/admin/search_logs/">
    <h4>По фразе</h4>
    <div class="form-group">
        <input type="text" class="form-control" id="logsQuery" name="query" placeholder="Фраза поиска" value="<?= h($content['search']['query']) ?>">
    </div>

    <h4>По дате</h4>
    <div class="form-group">
        <label for="logsDateFrom">От</label>
        <input type="text" class="form-control logs-date" id="logsDateFrom" name="date_from" placeholder="Дата" value="<?= $content['search']['date_from'] ?>">
    </div>
    <div class="form-group">
        <label for="logsDateTo">До</label>
        <input type="text" class="form-control logs-date" id="logsDateTo" name="date_to" placeholder="Дата" value="<?= $content['search']['date_to'] ?>">
    </div>

    <input type="reset" id="reset-btn" class="btn btn-default" value="Сбросить">
    <input type="submit" class="btn btn-primary" value="Искать">
</form>

<?php if (!empty($content['logs'])) { ?>
    <h5>Выгрузить</h5>
    <p><a href="/admin/search_logs/upload/<?= $content['search_url'] ?>&excel">В формате Excel</a></p>
    <p><a href="/admin/search_logs/upload/<?= $content['search_url'] ?>">В текстовом формате</a></p>
    <p>&nbsp;</p>
    <?php
    if ($content['logs']) {
        $paginationHTML = Template::get_tpl('pagination', [
            'Pagination' => $Pagination,
        ]);

        echo $paginationHTML;
    ?>
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Фраза</th>
                <th>Дата</th>
            </tr>
            </thead>
            <tbody>
            <?php for ($i = 0, $n = count($content['logs']); $i < $n; ++$i) { ?>
                <tr>
                    <td><?= highlight_query(h($content['logs'][$i]['search']), $content['search']['query']) ?></td>
                    <td><?= $content['logs'][$i]['dt'] ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>

<?php
        echo $paginationHTML;
    }
} else {
?>
    <p>Нет логов</p>
<?php
}
