<?php
/**
 * @var array $content
 */
if ($content['error']) {
?>
    <h4 class="red"><?= $content['error'] ?></h4>
<?php } ?>
<form method="POST" id="seo-tag-form">
    <h3>Родительская страница</h3>

    <div class="radio">
        <label>
            <input type="radio" name="tag[parent_type_page]" class="parent-type-radio" value=""<?= empty($content['data']['parent_type_page']) ? ' checked="checked"' : '' ?>> Категория / тег
        </label>
    </div>

    <div class="radio">
        <label>
            <input type="radio" name="tag[parent_type_page]" class="parent-type-radio" value="1"<?= !empty($content['data']['parent_type_page']) ? ' checked="checked"' : '' ?>> Типовая страница
        </label>
    </div>

    <div class="form-group"<?= !empty($content['data']['parent_type_page']) ? ' style="display: none"' : '' ?> id="parent-tag-select">
        <label for="categories-list">Родительская категория / тег</label>
        <select class="form-control" id="categories-list" name="tag[parent_id]">
            <option value="0">Нет (Тег первого уровня)</option>
            <?php foreach ($content['categories']['lvl_1'] as $category1) { ?>
                <option value="<?= $category1['id'] ?>" class="first-level"<?= $content['data']['parent_id'] === $category1['id'] ? ' selected="selected"' : '' ?>><?= $category1['name']  . ' (' . $category1['_full_url'] . ')' ?></option>
                <?php
                if (!empty($content['categories']['lvl_2'][$category1['id']])) {
                    foreach ($content['categories']['lvl_2'][$category1['id']] as $group2) {
                ?>
                        <optgroup label="<?= $group2['name'] ?>" class="second-level">
                            <?php foreach ($group2['cats'] as $category2) { ?>
                                <option value="<?= $category2['id'] ?>"<?= $content['data']['parent_id'] === $category2['id'] ? ' selected="selected"' : '' ?>><?= $category1['name'] . ' / ' . $category2['name']  . ' (' . $category2['_full_url'] . ')' ?></option>
                            <?php } ?>
                        </optgroup>
            <?php
                    }
                }
            }
            ?>
        </select>
    </div>

    <div class="form-group"<?= empty($content['data']['parent_type_page']) ? ' style="display: none"' : '' ?> id="parent-type-page-select">
        <label for="type-pages-list">Родительская типовая страница</label>
        <select class="form-control" id="type-pages-list" name="tag[type_page_id]">
            <?php foreach ($content['type_pages'] as $typePage) { ?>
                <option value="<?= $typePage['id'] ?>" <?= $content['data']['type_page_id'] === $typePage['id'] ? ' selected="selected"' : '' ?>><?= $typePage['name'] ?></option>
            <?php } ?>
        </select>
    </div>

    <div class="form-group">
        <label for="tag-anchor">Анкор на странице родительской категории</label>
        <input type="text" class="form-control" id="tag-anchor" name="tag[anchor]" required="required" value="<?= h($content['data']['anchor']) ?>">
    </div>

    <div class="form-group">
        <label for="tag-url">URL</label>
        <input type="text" class="form-control" id="tag-url" name="tag[url]" required="required" value="<?= h($content['data']['url']) ?>">
    </div>

    <div class="form-group">
        <label for="tag-name">Название (Автоматически из анкора, если пустое)</label>
        <input type="text" class="form-control" id="tag-tag-name" name="tag[tag_name]" value="<?= h($content['data']['tag_name']) ?>">
    </div>

    <input type="submit" class="btn btn-primary" value="Создать">
</form>
