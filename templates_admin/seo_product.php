<?php
/**
 * @var array $content
 */
?>
<h2><?= $content['product']['name'] ?></h2>

<a target="_blank" href="<?= DOMAIN_FULL . '/product/' . $content['product']['url'] ?>"><?= DOMAIN_FULL . '/product/' . $content['product']['url'] ?></a>

<form method="POST" action="/admin/seo/products/<?= $content['product']['id'] ?>">
    <div class="form-group">
        <label for="product-title">&lt;title&gt;</label>
        <textarea class="form-control" name="product[title]" id="product-title" rows="5"><?= h($content['product']['title']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="product-description">&lt;meta name="description"&gt;</label>
        <textarea class="form-control" name="product[description]" id="product-description" rows="5"><?= h($content['product']['description']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="product-keywords">&lt;meta name="keywords"&gt;</label>
        <textarea class="form-control" name="product[keywords]" id="product-keywords" rows="5"><?= h($content['product']['keywords']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="product-page-description">Текст на странице</label>
        <textarea class="form-control" name="product[page_description]" id="product-page-description" rows="5"><?= h($content['product']['page_description']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="product-short-description">Краткое описание</label>
        <textarea class="form-control" name="product[short_description]" id="product-short-description" rows="5"><?= h($content['product']['short_description']) ?></textarea>
    </div>

    <input type="submit" class="btn btn-primary" value="Сохранить">
</form>
