<?php
/**
 * @var array $content
 */
?>
<form enctype="multipart/form-data" action="/admin/master_classes/save" method="post">
    <div class="checkbox">
        <label>
            <input type="checkbox" name="new_address"<?= !isset($content['id']) || $content['new_address'] ? ' checked="checked"' : '' ?>> Новый адрес
        </label>
    </div>

    <div class="checkbox">
        <label>
            <input type="checkbox" name="webinar"<?= !isset($content['id']) || $content['webinar'] ? ' checked="checked"' : '' ?>> Вебинар?
        </label>
    </div>

    <div class="form-group">
        <label>Название</label>
        <input name="name" class="form-control" value="<?= !empty($content['id']) ? $content['name'] : '' ?>" size="100">
    </div>

    <div class="form-group">
        <label>Серия</label>
        <input name="series" class="form-control" value="<?= !empty($content['id']) ? $content['series'] : '' ?>" size="100">
    </div>

    <div class="form-group">
        <label>Краткое описание</label>
        <textarea name="short_desc" class="form-control"><?= !empty($content['id']) ? $content['short_desc'] : '' ?></textarea>
    </div>

    <div class="form-group">
        <label>Описание</label>
        <textarea name="text" id="editor" class="form-control"><?= !empty($content['id']) ? $content['text'] : '' ?></textarea>
    </div>

    <div class="form-group date-inputs">
        <label>Дата</label>
        <input type="text" class="form-control" name="day" value="<?= !empty($content['id']) ? $content['day'] : '' ?>" maxlength="2" size="2">
        . <input type="text" class="form-control" name="month" value="<?= !empty($content['id']) ? $content['month'] : '' ?>" maxlength="2">
        . <input type="text" class="form-control" name="year" value="<?= !empty($content['id']) ? $content['year'] : '' ?>" maxlength="4">
    </div>

    <div class="form-group date-inputs">
        <label>Время</label>
        <input type="text" class="form-control" name="hour" value="<?= !empty($content['id']) ? $content['hour'] : '' ?>" maxlength="2" size="2">
        : <input type="text" class="form-control" name="minute" value="<?= !empty($content['id']) ? $content['minute'] : '' ?>" maxlength="2" size="2">
    </div>

    <div class="form-group">
        <label>Продолжительность</label>
        <input name="duration" class="form-control" value="<?= !empty($content['id']) ? $content['duration'] : '' ?>" size="5">
    </div>

    <div class="form-group">
        <label>Имя файла картинки</label>
        <input type="text" class="form-control" name="file_name" value="<?= !empty($content['id']) ?$content['img'] : '' ?>" size="80">
    </div>

    <?php if (!empty($content['img'])) { ?>
        <label>Текущая картинка:</label>

        <p id="banner"><img src="/images/mc/<?= $content['img'] ?>" alt="<?= $content['name'] ?>"></p>
    <?php } ?>

    <div class="form-group">
        <label><?= !empty($content['img']) ? 'Сменить' : 'Добавить' ?> картинку</label>

        <p><input name="pic" type="file"></p>
    </div>

    <div class="checkbox">
        <label>
            <input type="checkbox" name="visibility"<?= !isset($content['id']) || $content['visibility'] ? ' checked="checked"' : '' ?>> отображать
        </label>
    </div>

    <?php if (!empty($content['id'])) { ?>
        <input type="hidden" name="id" value="<?= $content['id'] ?>">
    <?php } ?>

    <p><input type="submit" name="submit" value="Cохранить" class="btn btn-primary"></p>
</form>
