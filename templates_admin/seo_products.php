<?php
/**
 * @var array $content
 * @var array $Pagination
 */

$q = isset($_GET['q']) ? $_GET['q'] : null;
?>
    <form method="GET">
        <div class="form-group">
            <label for="product-q">Поиск</label>
            <input type="text" name="q" class="form-control" placeholder="Название, URL" id="product-q" value="<?= $q ? h($q) : '' ?>">
        </div>
        <input type="submit" class="btn btn-primary" value="Искать">
    </form>
<?php
$paginationHTML = Template::get_tpl('pagination', [
    'Pagination' => $Pagination,
]);

echo $paginationHTML;
?>
    <div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Название</th>
                <th>URL</th>
                <th>&lt;title&gt;</th>
                <th>&lt;meta name="description"&gt;</th>
            </tr>
            </thead>
            <tbody>
                <?php for ($i = 0, $n = count($content['products']); $i < $n; ++$i) { ?>
                    <tr>
                        <td>
                            <?= $content['products'][$i]['id'] ?>
                        </td>
                        <td>
                            <a href="/admin/seo/products/<?= $content['products'][$i]['id'] ?>"><?= highlight_query(h($content['products'][$i]['name']), $q) ?></a>
                        </td>
                        <td>
                            <?= highlight_query(h($content['products'][$i]['url']), $q) ?>
                        </td>
                        <td>
                            <?= h($content['products'][$i]['title']) ?>
                        </td>
                        <td>
                            <?= h($content['products'][$i]['description']) ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
<?php
echo $paginationHTML;
