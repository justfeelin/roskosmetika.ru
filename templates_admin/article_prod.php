<?php
/**
 * @var array $msg
 * @var string $content
 */
foreach ($msg as $item) {
?>
    <div class="alert alert-block"><?= $item ?></div>
<?php } ?>

<form enctype="multipart/form-data" action="/admin/article_prod" method="POST">
    <input type="file" name="file_<?= $content ?>" style="display:none;" class="input-file"/>

    <div class="input-append form-group">
        <input type="text" name="subfile" class="input-medium subfile" placeholder="Выбор файла">
        <a class="btn" onclick="$('.input-file').click();">Обзор</a>
    </div>
    <p>
        <button type="submit" name="submit" class="btn btn-primary">Отправить</button>
    </p>
</form>
