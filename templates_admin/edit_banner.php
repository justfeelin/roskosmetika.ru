<?php
/**
 * @var array $content
 * @var string $header
 */
?>
<div>
    <form enctype="multipart/form-data" action="/admin/banners/<?= !empty($content['banner']['id']) ? 'edit' : 'add' ?>/" method="post">
        <h1><?= $header ?></h1>

        <div class="form-group">
            <label>Имя</label>
            <input type="text" class="form-control" name="name" value="<?= !empty($content['banner']['id']) ? $content['banner']['name'] : '' ?>">
        </div>

        <div class="form-group">
            <label>Имя файла</label>
            <input type="text" class="form-control" name="file_name" value="<?= !empty($content['banner']['id']) ? $content['banner']['file_name'] : '' ?>">
        </div>

        <div class="form-group">
            <label>Ссылка(/контроллер)</label>
            <input type="text" class="form-control" name="link" value="<?= !empty($content['banner']['id']) ? $content['banner']['link'] : '' ?>">
        </div>

        <div class="form-group">
            <label>Alt/title</label>
            <textarea name="alt" class="form-control" rows="2"><?= !empty($content['banner']['id']) ? $content['banner']['alt'] : '' ?></textarea>

        </div>

        <div class="form-group">
            <label>Текст в меню</label>
            <textarea name="menu_tag" class="form-control"><?= !empty($content['banner']['id']) ? $content['banner']['menu_tag'] : '' ?></textarea>
        </div>

        <h4>Даты показа баннера</h4>

        <div id="dates-box">
            <?php
            if (!empty($content['dates'])) {
                for ($i = 0, $n = count($content['dates']); $i < $n; ++$i) {
                    Template::partial('banner_date', [
                        'date' => $content['dates'][$i],
                    ]);
                }
            }
            ?>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <button class="btn btn-success" id="add-date-btn">Добавить дату</button>
                <br>
                <br>
                <br>
            </div>
        </div>

        <div class="form-group">
            <label>Позиция</label>
            <input type="text" class="form-control" name="position" value="<?= !empty($content['banner']['id']) ? $content['banner']['position'] : '' ?>" maxlength="3">
        </div>

        <div class="form-group">
            <label>Разметка &lt;area&gt; (Если есть ссылки внутри баннера)</label>
            <textarea class="form-control" name="map_area" rows="10"><?= !empty($content['banner']['id']) ? h($content['banner']['map_area']) : '' ?></textarea>
        </div>

        <?php if (!empty($content['banner']['id'])) { ?>
            <h4>Текущий баннер</h4>

            <div id="banner">
                <img src="/images/banners/<?= $content['banner']['file_name'] . '.' . $content['banner']['type'] ?>" alt="<?= $content['banner']['name'] ?>">
                <?php
                if ($content['banner']['type'] !== 'swf') {
                    $coords = $content['banner']['copy_area'] ? explode(',', $content['banner']['copy_area']) : [0, 0, 200, 100];
                ?>
                    <div id="banner-copy-area" style="display: <?= $content['banner']['copy_area'] ? 'block' : 'none' ?>; left: <?= $coords[0] ?>px; top: <?= $coords[1] ?>px; width: <?= $coords[2] ?>px; height: <?= $coords[3] ?>px;"></div>
                <?php } ?>
            </div>

            <?php if ($content['banner']['type'] !== 'swf') { ?>
                <div class="form-group">
                    <label>
                        Блок для копирования промо-кода
                        <input type="checkbox" class="checkbox" name="copy_area_enabled" value="1" <?= $content['banner']['copy_area'] ? 'checked="checked"' : '' ?> id="copy-area-enabled">
                    </label>

                    <input type="hidden" name="copy_area" id="copy-area" value="<?= h($content['banner']['copy_area']) ?>">
                </div>
                <div class="form-group" style="display: <?= $content['banner']['copy_area'] ? 'block' : 'none' ?>" id="copy-text-box">
                    <label for="copy-area-text">Текст для копирования</label>
                    <input type="text" id="copy-area-text" class="form-control" name="copy_area_text" value="<?= h($content['banner']['copy_area_text']) ?>">
                </div>
            <?php } ?>

            <label>Сменить баннер</label>

            <?php if ($content['banner']['type'] === 'swf') { ?>
                <div>
                    <p>Текущая подложка:</p>

                    <p><img src="/flash/banners/<?= $content['banner']['file_name'] ?>_under.jpg" alt="<?= $content['banner']['name'] ?>"></p>

                    <p>Сменить подложку(только .jpg):</p>

                    <p><input name="under_pic" type="file"></p>
                </div>
            <?php } ?>

            <input type="hidden" name="id" value="<?= $content['banner']['id'] ?>">
        <?php } else { ?>
            <label>Баннер</label>
        <?php } ?>
        <p><input name="pic" type="file"></p>

        <p>
            <button type="submit" name="save" class="btn btn-primary">Сохранить</button>
        </p>
    </form>
    <div id="date-tmpl-box" class="hidden">
        <?php Template::partial('banner_date'); ?>
    </div>
</div>
