<?php
/**
 * @var array $content
 */

$p2 = str_repeat('&nbsp;', 4);
$p3 = str_repeat('&nbsp;', 8);

if ($content['error']) {
?>
    <h4 class="red"><?= $content['error'] ?></h4>
<?php } ?>
<form method="POST" enctype="application/x-www-form-urlencoded">
    <div class="row">
        <div class="col-xs-3">
            <div class="form-group">
                <label for="promo-code-code">Код</label>
                <input type="text" id="promo-code-code" class="form-control" name="promo_code[code]" value="<?= !empty($content['promo_code']['code']) ? h($content['promo_code']['code']) : '' ?>" required="required">
            </div>
        </div>
        <?php if (!$content['promo_code']['id']) { ?>
            <div class="col-xs-6 col-md-3 col-xs-offset-1">
                <div class="form-group">
                    <label for="generate-length">Сгенерировать код</label>
                    <input type="number" id="generate-promo-length" class="form-control" value="6" placeholder="Количество символов" size="1">
                    <button id="generate-promo-btn" class="btn btn-success">Сгенерировать</button>
                </div>
            </div>
        <?php } ?>
    </div>

    <div class="form-group">
        <label>Комментарий</label>
        <textarea id="promo-code-comment" class="form-control" name="promo_code[comment]" required="required"><?= !empty($content['promo_code']['comment']) ? $content['promo_code']['comment'] : '' ?></textarea>
    </div>

    <div id="common-promo-box"<?= !empty($content['promo_code']['action_type_id']) && $content['promo_code']['action_type_id'] == promo_codes::ACTION_TYPE_CHEAPEST_PRODUCT_DISCOUNT ? ' style="display: none"' : '' ?>>
        <div class="form-group">
            <label for="promo-code-discount">Размер скидки</label>
            <input type="text" id="promo-code-discount" class="form-control" name="promo_code[discount]" value="<?= !empty($content['promo_code']['discount']) ? h(abs($content['promo_code']['discount'])) : '' ?>"<?= empty($content['promo_code']['action_type_id']) || $content['promo_code']['action_type_id'] != promo_codes::ACTION_TYPE_CHEAPEST_PRODUCT_DISCOUNT ? ' required="required"' : '' ?>>
        </div>

        <h4>Тип скидки</h4>

        <div class="radio">
            <label>
                <input class="rouble-radio" type="radio" name="promo_code[discount_roubles]" value=""<?= empty($content['promo_code']['discount_roubles']) ? ' checked="checked"' : '' ?>>
                В процентах
            </label>
        </div>

        <div class="radio">
            <label>
                <input class="rouble-radio" type="radio" name="promo_code[discount_roubles]" value="1"<?= !empty($content['promo_code']['discount_roubles']) ? ' checked="checked"' : '' ?>>
                В рублях
            </label>
        </div>

        <div class="checkbox" id="certificate-box"<?= empty($content['promo_code']['discount_roubles']) ? ' style="display: none"' : '' ?>>
            <label>
                <input type="hidden" name="promo_code[certificate]" value="0">
                <input id="certificate-input" type="checkbox" name="promo_code[certificate]" value="1"<?= !empty($content['promo_code']['certificate']) ? ' checked="checked"' : '' ?>>
                Сертификат
            </label>
        </div>

        <?php if (!empty($content['promo_code']['id']) && !empty($content['promo_code']['certificate'])) { ?>
            <div id="generate-certificate-box">
                <a href="?generate_certificate">Сгенерировать изображение сертификата</a>
            </div>
        <?php } ?>

        <h4>Тип применения скидки</h4>

        <div class="radio">
            <label>
                <input type="radio" name="promo_code[discount_type]" value="0"<?= !isset($content['promo_code']['discount_type']) || !$content['promo_code']['discount_type'] ? ' checked="checked"' : '' ?>>
                Обычный купон
            </label>
        </div>

        <div class="radio">
            <label>
                <input type="radio" name="promo_code[discount_type]" value="1"<?= !empty($content['promo_code']['discount_type']) ? ' checked="checked"' : '' ?>>
                Правило большей скидки
            </label>
        </div>
    </div>

    <div class="form-group">
        <label for="promo-code-min-sum">Минимальная сумма заказа</label>
        <input type="text" id="promo-code-min-sum" class="form-control" name="promo_code[min_sum]" value="<?= !empty($content['promo_code']['min_sum']) ? h($content['promo_code']['min_sum']) : '' ?>">
    </div>

    <div class="form-group">
        <label for="promo-code-end-date">Дата окончания</label>
        <input type="text" id="promo-code-end-date" class="form-control" name="promo_code[end_date]" value="<?= !empty($content['promo_code']['end_date']) ? h($content['promo_code']['end_date']) : '' ?>">
    </div>

    <h4>Уникальность промо-кода</h4>

    <div class="radio">
        <label>
            <input type="radio" name="promo_code[type]" value="uniq"<?= !isset($content['promo_code']['type']) || $content['promo_code']['type'] === 'uniq' ? ' checked="checked"' : '' ?>>
            Уникальный
        </label>
    </div>

    <div class="radio">
        <label>
            <input type="radio" name="promo_code[type]" value="repeat"<?= !empty($content['promo_code']['type']) && $content['promo_code']['type'] !== 'uniq' ? ' checked="checked"' : '' ?>>
            Повторяемый
        </label>
    </div>

    <div class="checkbox">
        <label>
            <input type="checkbox" name="promo_code[use]"<?= !empty($content['promo_code']['use']) ? ' checked="checked"' : '' ?>>
            Использован
        </label>
    </div>

    <div id="filters-box"<?= !empty($content['promo_code']['certificate']) ? ' style="display: none"' : '' ?>>
        <h4>Фильтрация товаров</h4>

        <input type="hidden" name="promo_code[action_type_id]" id="action-type-id" value="<?= !empty($content['promo_code']['action_type_id']) ? (int)$content['promo_code']['action_type_id'] : 0 ?>">

        <div class="form-group">
            <ul class="nav nav-tabs" role="tablist" id="action-tabs">
                <li role="presentation" class="<?= empty($content['promo_code']['action_type_id']) ? 'active' : '' ?>">
                    <a href="#f-none" aria-controls="f-none" role="tab" data-toggle="tab" data-action-type-id="0">-Нет-</a>
                </li>
                <li role="presentation" class="<?= !empty($content['promo_code']['action_type_id']) && $content['promo_code']['action_type_id'] == promo_codes::ACTION_TYPE_PRODUCTS ? 'active' : '' ?>">
                    <a href="#f-products" aria-controls="f-products" role="tab" data-toggle="tab" data-action-type-id="<?= promo_codes::ACTION_TYPE_PRODUCTS ?>">Список товаров</a>
                </li>
                <li role="presentation" class="<?= !empty($content['promo_code']['action_type_id']) && $content['promo_code']['action_type_id'] == promo_codes::ACTION_TYPE_TMS ? 'active' : '' ?>">
                    <a href="#f-tms" aria-controls="f-tms" role="tab" data-toggle="tab" data-action-type-id="<?= promo_codes::ACTION_TYPE_TMS ?>">По торговым маркам</a>
                </li>
                <li role="presentation" class="<?= !empty($content['promo_code']['action_type_id']) && $content['promo_code']['action_type_id'] == promo_codes::ACTION_TYPE_CATEGORIES ? 'active' : '' ?>">
                    <a href="#f-categories" aria-controls="f-categories" role="tab" data-toggle="tab" data-action-type-id="<?= promo_codes::ACTION_TYPE_CATEGORIES ?>">По категориям</a>
                </li>
                <li role="presentation" class="<?= !empty($content['promo_code']['action_type_id']) && $content['promo_code']['action_type_id'] == promo_codes::ACTION_TYPE_CHEAPEST_PRODUCT_DISCOUNT ? 'active' : '' ?>">
                    <a href="#f-cheapest-discount" aria-controls="f-cheapest-discount" role="tab" data-toggle="tab" data-action-type-id="<?= promo_codes::ACTION_TYPE_CHEAPEST_PRODUCT_DISCOUNT ?>" data-cheapest-discount="1">Самый дешёвый товар со скидкой</a>
                </li>
            </ul>

            <div class="tab-content">
                <div role="tabpanel" class="tab-pane<?= empty($content['promo_code']['action_type_id']) ? ' active' : '' ?>" id="f-none"></div>
                <div role="tabpanel" class="tab-pane<?= !empty($content['promo_code']['action_type_id']) && $content['promo_code']['action_type_id'] == promo_codes::ACTION_TYPE_PRODUCTS ? ' active' : '' ?>" id="f-products">
                    <div class="form-group">
                        <label for="action-products">ID товаров</label>
                        <textarea name="action[product_ids]" id="action-products" class="form-control" rows="20"><?= implode("\n", $content['actionProducts']) ?></textarea>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane<?= !empty($content['promo_code']['action_type_id']) && $content['promo_code']['action_type_id'] == promo_codes::ACTION_TYPE_TMS ? ' active' : '' ?>" id="f-tms">
                    <div class="form-group">
                        <label for="action-tms">Торговые марки</label>
                        <select name="action[tm_ids][]" multiple="multiple" size="30" class="form-control">
                            <?php for ($i = 0, $n = count($content['tms']); $i < $n; ++$i) { ?>
                                <option value="<?= $content['tms'][$i]['id'] ?>"<?= in_array($content['tms'][$i]['id'], $content['actionTrademarks']) ? ' selected="selected"' : '' ?>><?= $content['tms'][$i]['name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane<?= !empty($content['promo_code']['action_type_id']) && $content['promo_code']['action_type_id'] == promo_codes::ACTION_TYPE_CATEGORIES ? ' active' : '' ?>" id="f-categories">
                    <div class="form-group">
                        <label for="action-categories">Категория</label>
                        <select class="form-control" id="action-categories" name="action[category_ids][]" multiple="multiple" size="50">
                            <?php foreach ($content['categories']['lvl_1'] as $category1) { ?>
                                <option value="<?= $category1['id'] ?>" class="first-level"<?= in_array($category1['id'], $content['actionCategories']) ? ' selected="selected"' : '' ?>><?= $category1['name'] ?></option>
                                <?php
                                if (!empty($content['categories']['lvl_2'][$category1['id']])) {
                                    foreach ($content['categories']['lvl_2'][$category1['id']] as $group2) {
                                        ?>
                                        <optgroup label="<?= $group2['name'] ?>" class="second-level">
                                            <?php foreach ($group2['cats'] as $category2) { ?>
                                                <option value="<?= $category2['id'] ?>"<?= in_array($category2['id'], $content['actionCategories']) ? ' selected="selected"' : '' ?>><?= $category2['name'] ?></option>
                                                <?php
                                                if (!empty($content['categories']['lvl_3'][$category2['id']])) {
                                                    foreach ($content['categories']['lvl_3'][$category2['id']] as $group3) { ?>
                                                        <option disabled="disabled"><?= $p2 . $group3['name'] ?></option>
                                                        <?php foreach ($group3['cats'] as $category3) { ?>
                                                            <option value="<?= $category3['id'] ?>"<?= in_array($category3['id'], $content['actionCategories']) ? ' selected="selected"' : '' ?>><?= $p3 . $category3['name'] ?></option>
                                            <?php
                                                        }
                                                    }
                                                }
                                            }
                                            ?>
                                        </optgroup>
                            <?php
                                    }
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane<?= !empty($content['promo_code']['action_type_id']) && $content['promo_code']['action_type_id'] == promo_codes::ACTION_TYPE_CHEAPEST_PRODUCT_DISCOUNT ? ' active' : '' ?>" id="f-cheapest-discount">
                    <h4>Участвующие товары</h4>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                Весь ассортимент
                                <input type="checkbox" name="action[cheapest_product_discount_all]" value="1"<?= !$content['actionCheapestDiscountProducts'] ? ' checked="checked"' : '' ?> id="cheapest-discount-all-chb">
                            </label>
                        </div>
                    </div>
                    <div class="form-group" id="cheapest-discount-product-ids"<?= !$content['actionCheapestDiscountProducts'] ? 'style="display: none"' : '' ?>>
                        <label for="action-cheapest-discount-ids">ID товаров</label>
                        <textarea name="action[cheapest_product_discount_ids]" id="action-cheapest-discount-ids" class="form-control" rows="20"><?= implode("\n", $content['actionCheapestDiscountProducts']) ?></textarea>
                    </div>

                    <div class="form-group">
                        <label for="action-cheapest-discount-discount">Размер скидки, %</label>
                        <input type="number" id="action-cheapest-discount-discount" name="promo_code[cheapest_discount]" class="form-control" value="<?= !empty($content['promo_code']['discount']) ? (int)$content['promo_code']['discount'] : '' ?>">
                    </div>

                    <div class="form-group">
                        <label for="action-cheapest-discount-min">Минимально необходимое число товаров в корзине</label>
                        <input type="number" id="action-cheapest-discount-min" name="promo_code[min_action_products_count]" class="form-control" value="<?= !empty($content['promo_code']['min_action_products_count']) ? (int)$content['promo_code']['min_action_products_count'] : '' ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="submit" class="btn btn-primary" value="Сохранить">
</form>
