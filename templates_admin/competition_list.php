<?php
/**
 * @var array $content
 * @var array $Pagination
 */
?>
<p><a href="/admin/competition/add_competition/">Добавить акцию</a></p>
<p><a href="/admin/competition/upload/">Выгрузить всех участников</a></p>
<br>
<?php
if ($content) {
    $paginationHTML = Template::get_tpl(
        'pagination',
        [
            'Pagination' => $Pagination,
        ]
    );

    echo $paginationHTML;
    ?>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Название</th>
            <th width="90">Ссылка</th>
            <th width="90">ID</th>
            <th width="90">Дата</th>
            <th width="90">Статус</th>
            <th width="90">Видимость</th>
            <th width="90">Участники</th>
        </tr>
        </thead>
        <tbody>
        <?php for ($i = 0, $n = count($content['comp']); $i < $n; ++$i) { ?>
            <tr class="al_c">
                <td class="al_l"><a href="/admin/competition/edit/<?= $content['comp'][$i]['id'] ?>"><?= $content['comp'][$i]['name'] ?></a></td>
                <td>
                    <a href="/competition/<?= $content['comp'][$i]['url'] ?>" target="_blank">перейти</a>
                </td>
                <td><?= $content['comp'][$i]['id'] ?></td>
                <td><?= Template::date('d.m.Y', $content['comp'][$i]['end']) ?></td>
                <td><?= $content['comp'][$i]['status']?'идет' : 'завершено' ?></td>
                <td>
                    <a href="/admin/competition/change_visibility/<?= $content['comp'][$i]['id'] ?>"><?= $content['comp'][$i]['visible'] ? 'скрыть' : 'отобразить' ?></a>
                </td>
                <td>
                    <?php if ($content['comp'][$i]['count']) { ?>
                        <a href="/admin/competition/subscribes/<?= $content['comp'][$i]['id'] ?>"><?= $content['comp'][$i]['count'] ?></a>
                    <?php } else { ?>
                        0
                    <?php } ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <?php
    echo $paginationHTML;
} else {
    ?>
    <p>Нет акций.</p>
<?php } ?>

