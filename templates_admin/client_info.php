<?php
/**
 * @var array $content
 */
?>
<dl>
    <dt>ID</dt>
    <dd><?= $content['id'] ?></dd>
</dl>
<dl>
    <dt>ФИО</dt>
    <dd><?= $content['surname'] . ' ' . $content['name'] . ' ' . $content['patronymic'] ?></dd>
</dl>
<dl>
    <dt>E-mail</dt>
    <dd><?= $content['email'] ?></dd>
</dl>
<dl>
    <dt>Телефон</dt>
    <dd><?= $content['phone'] ?: '<span class="red">Не указан</span>' ?></dd>
</dl>
<?php if ($content['region_name']) { ?>
    <dl>
        <dt>Регион</dt>
        <dd><?= $content['region_name'] ?></dd>
    </dl>
<?php } ?>
<dl>
    <dt>Адрес</dt>
    <dd><?= $content['address'] ?: '<span class="red">Не указан</span>' ?></dd>
</dl>
