<?php
/**
 * @var array $content
 */
?>
<form class="form-horizontal" action="/admin/promo_codes/continue_add_by_query" method="post">

    <div class="form-group">
        <div class="col-xs-8">
            <input type="hidden" class="form-control" name="query" value="<?= $content['query'] ?>">
        </div>
        <div class="col-xs-4"></div>
    </div>

    <div class="form-group">
        <div class="col-xs-1">
            <input type="hidden" class="form-control" name="discount" value="<?= $content['discount'] ?>">
            <input type="hidden" name="key_start" value="<?= $content['key_start'] ?>">
        </div>
        <div class="col-xs-11"></div>
    </div>

    <div class="form-group">
        <div class="col-xs-8">
            <button type="submit" class="btn btn-primary" name="save">Продолжить назначение кодов >></button>
        </div>
        <div class="col-xs-4"></div>
    </div>
</form>
