<?php
/**
 * @var array $content
 */
?>
<form method="POST" action="/admin/seo/home">
    <div class="form-group">
        <label for="page-title">&lt;title&gt;</label>
        <textarea class="form-control" name="page[title]" id="page-title" rows="5"><?= h($content['page']['title']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="page-description">&lt;meta name="description"&gt;</label>
        <textarea class="form-control" name="page[description]" id="page-description" rows="5"><?= h($content['page']['description']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="page-keywords">&lt;meta name="keywords"&gt;</label>
        <textarea class="form-control" name="page[keywords]" id="page-keywords" rows="5"><?= h($content['page']['keywords']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="page-header">&lt;h1&gt;</label>
        <textarea class="form-control" name="page[header]" id="page-header" rows="5"><?= h($content['page']['header']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="page-content">Текст на странице</label>
        <textarea class="form-control" name="page[content]" id="page-content" rows="10"><?= h($content['page']['content']) ?></textarea>
    </div>

    <input type="submit" class="btn btn-primary" value="Сохранить">
</form>
