<?php
/**
 * @var string $msg
 * @var array $content
 */

/**
 * Inserts nbsp characters sequence
 *
 * @param int $n Multiplier
 *
 * @return string
 */
function nbsp($n)
{
    return str_repeat('&nbsp;', $n * 3);
}

if  (isset($msg)) { ?>
    <div class="alert alert-block"><?= $msg ?></div>
<?php } ?>
<form action="/admin/export/products" method="POST">
    <div class="row">
        <div class="col-xs-4">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="ids">&nbsp;ID&nbsp;товара
                </label>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Введите id товаров" name="id_line">
            </div>
        </div>
        <div class="col-xs-3">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="tms">&nbsp;Торговая&nbsp;марка
                </label>
            </div>
            <div class="form-group">
                <select name="tm" class="form-control">
                    <?php
                    if ($content['tm']) {
                        for ($i = 0, $n = count($content['tm']); $i < $n; ++$i) { ?>
                            <option value="<?= $content['tm'][$i]['id'] ?>"><?= $content['tm'][$i]['name'] ?></option>
                    <?php
                        }
                    }else { ?>
                        <option value="none">Нет торговых марок</option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="form-group">
                <label for="export-categories">Категории</label>
                <select name="categories[]" class="form-control" id="export-categories" multiple="multiple" size="20">
                    <?php foreach ($content['categories']['lvl_1'] as $category1) { ?>
                        <option value="<?= $category1['id'] ?>"><?= ($category1['is_tag'] ? '[Тег] ' : '') . $category1['name'] ?></option>
                        <?php

                        if (!empty($content['categories']['lvl_2'][$category1['id']])) {
                            foreach ($content['categories']['lvl_2'][$category1['id']] as $category2Group) {
                        ?>
                                <option disabled="disabled"><?= nbsp(1) . $category2Group['name'] ?></option>
                                <?php foreach ($category2Group['cats'] as $category2) { ?>
                                    <option value="<?= $category2['id'] ?>"><?= nbsp(2) . $category2['name'] ?></option>
                                    <?php
                                    if (!empty($content['categories']['lvl_3'][$category2['id']])) {
                                        foreach ($content['categories']['lvl_3'][$category2['id']] as $category3Group) {
                                    ?>
                                            <option disabled="disabled"><?= nbsp(3) . $category3Group['name'] ?></option>
                                            <?php foreach ($category3Group['cats'] as $category3) { ?>
                                                <option value="<?= $category3['id'] ?>"><?= nbsp(4) . $category3['name'] ?></option>
                    <?php
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-xs-3">
            <span class="help-block"><strong>Описания товаров</strong></span>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='eng'>&nbsp;без&nbsp;английского&nbsp;названия
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='use'>&nbsp;без&nbsp;применения
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='made_of'>&nbsp;без&nbsp;состава
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='synonym'>&nbsp;без&nbsp;синонимов
                </label>
            </div>
        </div>

        <div class="col-xs-3">
            <span class="help-block"><strong>Описания страниц</strong></span>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='title'>&nbsp;без &lt;title&gt;
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='seo_desc'>&nbsp;без &lt;description&gt;
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='keywords'>&nbsp;без &lt;keywords&gt;
                </label>
            </div>
        </div>

        <div class="col-xs-3">
            <span class="help-block"><strong>Фотографии</strong></span>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='alt'>&nbsp;без&nbsp;alt
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='photo_title'>&nbsp;без&nbsp;title
                </label>
            </div>
        </div>

        <div class="col-xs-3">
            <span class="help-block"><strong>Каталог</strong></span>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='no_cat'>&nbsp;без&nbsp;категорий
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='no_filters'>&nbsp;без&nbsp;фильтров
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='no_lines'>&nbsp;без&nbsp;линеек
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='no_tm'>&nbsp;без&nbsp;торговой&nbsp;марки
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='news'>&nbsp;новинки
                </label>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-xs-3">
            <span class="help-block"><strong>Дополнительно</strong></span>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name='no_pack'>&nbsp;Без&nbsp;фасовок
                </label>
            </div>
        </div>
    </div>
    <br>
    <br>

    <div class="row">
        <button type="submit" class="btn btn-primary" name="choise">Загрузить выборку</button>
        <button type="submit" class="btn btn-primary" name="all">Загрузить все</button>
        <button type="submit" class="btn btn-primary" name="blank">Загрузить бланк</button>
    </div>
</form>
