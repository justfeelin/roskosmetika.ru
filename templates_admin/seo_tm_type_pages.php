<?php
/**
 * @var array $content
 */
$typePageID = isset($_GET['type_page_id']) ? $_GET['type_page_id'] : null;
?>
<form method="GET" class="form">
    <div class="form-group">
        <label for="type-page-input">Тип косметики</label>
        <select name="type_page_id" class="form-control" id="type-page-input">
            <option>- Не выбрано -</option>
            <?php for ($i = 0, $n = count($content['type_pages']); $i < $n; ++$i) { ?>
                <option value="<?= $content['type_pages'][$i]['id'] ?>"<?= $typePageID == $content['type_pages'][$i]['id'] ? ' selected="selected"' : '' ?>><?= h($content['type_pages'][$i]['name']) ?></option>
            <?php } ?>
        </select>
    </div>
    <input type="submit" class="btn btn-primary" value="Искать">
</form>
<?php if ($content['items']) { ?>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>URL</th>
            <th>Торговая марка</th>
            <th>H1</th>
            <th>title</th>
            <th>description</th>
        </tr>
        </thead>
        <tbody>
        <?php for ($i = 0, $n = count($content['items']); $i < $n; ++$i) { ?>
            <tr>
                <td>
                    <a href="/admin/seo/tm_type_pages/<?= $content['items'][$i]['type_page_id'] . '/' . $content['items'][$i]['tm_id'] ?>">
                        <?= h($content['items'][$i]['type_page_url']) . '/' . h($content['items'][$i]['tm_url']) ?>
                    </a>
                </td>
                <td>
                    <?= $content['items'][$i]['tm_name'] ?>
                </td>
                <td>
                    <?= h($content['items'][$i]['h1']) ?>
                </td>
                <td>
                    <?= h($content['items'][$i]['title']) ?>
                </td>
                <td>
                    <?= h($content['items'][$i]['description']) ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <?php
}

