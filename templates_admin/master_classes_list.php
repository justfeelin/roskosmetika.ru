<?php
/**
 * @var array $content
 * @var array $Pagination
 */
?>
<p><a href="/admin/master_classes/add_master_class/">Добавить мастер-класс</a></p>
<p><a href="/admin/master_classes/upload/">Выгрузить всех подписавшихся</a></p>
<br>
<?php
if ($content) {
    $paginationHTML = Template::get_tpl(
        'pagination',
        [
            'Pagination' => $Pagination,
        ]
    );

    echo $paginationHTML;
?>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Название</th>
            <th width="90">Дата</th>
            <th width="90">Время</th>
            <th width="90">Видимость</th>
            <th width="90">Записавшихся</th>
            <th width="90">Напоминания</th>
        </tr>
        </thead>
        <tbody>
        <?php for ($i = 0, $n = count($content['mcs']); $i < $n; ++$i) { ?>
            <tr class="al_c">
                <td class="al_l"><a href="/admin/master_classes/edit/<?= $content['mcs'][$i]['id'] ?>"><?= $content['mcs'][$i]['name'] ?></a></td>
                <td><?= Template::date('d.m.Y', $content['mcs'][$i]['date']) ?></td>
                <td><?= Template::date('H:i', $content['mcs'][$i]['date']) ?></td>
                <td>
                    <a href="/admin/master_classes/change_vibibility/<?= $content['mcs'][$i]['id'] ?>"><?= $content['mcs'][$i]['visibility'] ? 'скрыть' : 'отобразить' ?></a>
                </td>
                <td>
                    <?php if ($content['mcs'][$i]['count']) { ?>
                        <a href="/admin/master_classes/subscribes/<?= $content['mcs'][$i]['id'] ?>"><?= $content['mcs'][$i]['count'] ?></a>
                    <?php } else { ?>
                        0
                    <?php } ?>
                </td>
                <td><a href="/admin/master_classes/mc_mail/{$item.id}">Напомнить</a></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php
    echo $paginationHTML;
} else {
?>
    <p>Нет мастер-классов.</p>
<?php } ?>

