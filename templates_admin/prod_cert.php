<?php
/**
 * @var array $content
 * @var string $error
 * @var array $products_search
 */
if (isset($error)) {
?>
    <h4 class="red"><?= $error ?></h4>
<?php } ?>
<form method="POST" enctype="multipart/form-data">
    <div class="form-group">
        <label for="data-name">Название</label>
        <input id="data-name" name="data[name]" class="form-control" type="text" required="required" value="<?= h($content['data']['name']) ?>">
    </div>

    <div class="form-group">
        <label for="data-folder-url">Папка для изображений</label>
        <input id="data-folder-url" name="data[folder_url]" class="form-control" type="text" required="required" value="<?= h($content['data']['folder_url']) ?>">
    </div>

    <div class="form-group">
        <label for="data-end-date">Дата окончания</label>
        <div class="row">
            <div class="col-xs-5 col-md-2">
                <input id="data-end-date" name="data[end_date]" class="form-control" type="text" value="<?= h($content['data']['end_date']) ?>">
            </div>
            <div class="col-xs-3">
                <button id="clear-end-date" class="btn btn-sm btn-danger">Очистить</button>
            </div>
        </div>
    </div>

    <?php if ($content['data']['id']) { ?>
        <h4>Изображения</h4>

        <div id="cert-images">
            <?php
            if($content['data']['images']) {
                for ($i = 0, $n = count($content['data']['images']); $i < $n; ++$i) {
                    Template::partial('prod_cert_image', $content['data']['images'][$i] + [
                        'folder_url' => $content['data']['folder_url'],
                    ]);
                }
            }
            ?>
        </div>

        <div class="form-group">
            <button class="btn btn-success" id="add-image">Добавить изображение</button>
        </div>

    <?php } ?>

    <input type="submit" class="btn btn-primary" value="Сохранить">
</form>

<?php if ($content['data']['id']) { ?>
    <h4>Товары</h4>
    <?php
    Template::partial('products_search_form', [
        'products_search' => $products_search,
    ]);
}
?>

<div id="image-template">
    <?php
    Template::partial('prod_cert_image', [
        'folder_url' => $content['data']['folder_url'],
    ]);
    ?>
</div>
