<?php
/**
 * @var array $content
 */
?>
<div>
    <form enctype="multipart/form-data" action="/admin/action/edit/" method="post">
        <div class="form-group">
            <label for="action-name">Имя</label>
            <input type="text" name="name" id="action-name" class="form-control" value="<?= !empty($content['name']) ? h($content['name']) : '' ?>">
        </div>

        <div class="form-group">
            <label for="action-products">Список id товаров(пр:26, 40, 60)</label>
            <input type="text" name="prods" id="action-products" class="form-control" value="<?= !empty($content['prods']) ? h($content['prods']) : '' ?>">
        </div>

        <div class="form-group">
            <label for="action-url">Ссылка(/контроллер/....)</label>
            <input type="text" name="link" id="action-url" class="form-control" value="<?= !empty($content['link']) ? h($content['link']) : '' ?>">
        </div>

        <div class="checkbox">
            <label>
                <input type="checkbox" name="active" value="1" <?= !empty($content['active']) ? 'checked="checked"' : '' ?>> Активность
            </label>
        </div>

        <?php if (!empty($content['id'])) { ?>
            <input type="hidden" name="id" value="<?= $content['id'] ?>">
        <?php } ?>

        <button type="submit" class="btn btn-primary" name="save">Сохранить изменения</button>
    </form>
</div>