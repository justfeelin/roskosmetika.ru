<?php
/**
 * @var array $Pagination
 * @var array $content
 */
if ($content['items']) {
    $paginationHTML = Template::get_tpl('pagination', [
        'Pagination' => $Pagination,
    ]);

    echo $paginationHTML;
?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Дата</th>
                <th>Email</th>
                <th width="1">Текст письма</th>
                <th>Сайт</th>
            </tr>
        </thead>
        <tbody>
            <?php for ($i = 0, $n = count($content['items']); $i < $n; ++$i) { ?>
                <tr>
                    <td><?= Template::date('H:i d.m.Y', $content['items'][$i]['date']) ?></td>
                    <td><?= $content['items'][$i]['email'] ?></td>
                    <td width="1"><?= h(mb_substr($content['items'][$i]['message'], 0, 100)) ?></td>
                    <td><?= $content['items'][$i]['source'] ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
<?php
} else {
    echo $paginationHTML;
?>
    <p>Нет писем.</p>
<?php
}
