<?php
/**
 * @var array $msg
 * @var array $content
 */

if (isset($msg)) {
    foreach ($msg as $item) {
?>
    <div class="alert alert-block"><?= $item ?></div>
<?php
    }
}
?>
<form class="form-horizontal" action="/admin/export/tm_in_cat" method="POST">
    <div class="form-group">
        <div class="col-xs-7">
            <select class="form-control" name="tm">
                <?php
                if (!empty($content['items'])) {
                    for ($i = 0, $n = count($content['items']); $i < $n; ++$i) { ?>
                        <option value="<?= $content['items'][$i]['id'] ?>"><?= $content['items'][$i]['name'] ?></option>
                <?php
                    }
                } else {
                ?>
                    <option value="none">Нет торговых марок</option>
                <?php } ?>
            </select>
        </div>
        <span class="col-xs-5"></span>
    </div>
    <div lass="form-group">
        <button type="submit" class="btn btn-primary" name="choise">Загрузить выборку</button>
    </div>
</form>
