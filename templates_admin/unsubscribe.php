<?php
/**
 * @var bool $no_checkbox
 * @var string $msg
 */
if (isset($no_checkbox)) {
?>
    <p class="red">Введите параметры выборки.</p>
<?php
}

if (isset($msg)) {
?>
    <p class="red"><?= $msg ?></p>
<?php } ?>
<p><strong>Исключение заблокированных адресов</strong></p>
<form enctype="multipart/form-data" action="/admin/unsubscribe/block_emails" method="POST">
    <p><input name="file_block" type="file"></p>

    <p>
        <input type="submit" name="submit_block" class="btn btn-primary" value="Импортировать">

    </p>
</form>
