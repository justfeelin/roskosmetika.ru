<?php
/**
 * @var array $content
 */
$backParams = '?bu=' . rawurlencode($_SERVER['REQUEST_URI']);
?>
<table class="table table-striped">
    <thead>
    <tr>
        <th>ID</th>
        <th>
            <a href="/admin/kosmetika">Название</a>
        </th>
        <th>URL</th>
        <th>
            <a href="?sort_products=<?= isset($_GET['sort_products']) && $_GET['sort_products'] === 'asc' ? 'desc' : 'asc' ?>">Количество товаров</a>
        </th>
        <th>&nbsp;</th>
        <th>Видимость</th>
    </tr>
    </thead>
    <tbody>
    <?php for ($i = 0, $n = count($content['type_pages']); $i < $n; ++$i) { ?>
        <tr>
            <td>
                <a href="/admin/kosmetika/<?= $content['type_pages'][$i]['id'] ?>"><?= $content['type_pages'][$i]['id'] ?></a>
            </td>
            <td>
                <?= $content['type_pages'][$i]['name'] ?>
            </td>
            <td>
                <a href="<?= DOMAIN_FULL . '/kosmetika/' . $content['type_pages'][$i]['url'] ?>"><?= DOMAIN_FULL . '/kosmetika/' . $content['type_pages'][$i]['url'] ?></a>
            </td>
            <td>
                <?= $content['type_pages'][$i]['products'] ?>
            </td>
            <td>
                <a href="/admin/kosmetika/clear_products/<?= $content['type_pages'][$i]['id'] . $backParams ?>" class="btn btn-danger clear-products-btn">Очистить</a>
            </td>
            <td>
                <a href="/admin/kosmetika/toggle_visibility/<?= $content['type_pages'][$i]['id'] . $backParams ?>"><?= $content['type_pages'][$i]['visible'] ? 'Скрыть' : 'Показать' ?></a>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>
