<?php
/**
 * @var array $content
 * @var array $products_search
 */
?>
<p>
    <a href="<?= DOMAIN_FULL . '/category/' . $content['tag']['_full_url'] ?>"><?= $content['tag']['name'] ?></a> (ID <?= $content['tag']['id'] ?>)
</p>

<h3>Добавить товары</h3>

<?php
Template::partial('products_search_form', [
    'products_search' => $products_search,
]);
