<?php
/**
 * @var array $comment
 * @var string $number
 */
?>
<form action="/admin/unsubscribe/edit_comment/" method="post">
    <h1>Редактировать комментарий</h1>

    <p>Комментарий:</p>

    <p><textarea name="comment" cols="70" rows="30"><?= $comment['comment'] ?></textarea></p>
    <input type="hidden" name="number" value="<?= $number ?>">

    <p>
        <button type="submit" name="save">Сохранить изменения</button>
    </p>
</form>
