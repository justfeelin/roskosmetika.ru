<?php
/**
 * @var array $products_search
 */
$nbsp2 = str_repeat('&nbsp;', 4);
$nbsp3 = str_repeat('&nbsp;', 8);
?>
<div id="products-search">
    <form class="search-form">
        <div class="row">
            <?php if (in_array('search', $products_search['filter_types'])) { ?>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="products-search-name">Поиск по товарам</label>
                        <input type="text" id="products-search-name" class="form-control" name="products_search[name]">
                    </div>
                </div>
            <?php
            }

            if (in_array('tags', $products_search['filter_types'])) {
            ?>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="products-search-name-tag">&lt;- Подставить из тега</label>
                        <select id="products-search-name-tag" class="form-control">
                            <option></option>
                            <?php foreach ($products_search['tags'] as $tag1) { ?>
                                <option<?= !$tag1['is_tag'] ? ' disabled="disabled"' : '' ?>><?= $tag1['name'] ?></option>
                                <?php foreach ($tag1['children'] as $tag2) { ?>
                                    <option<?= !$tag2['is_tag'] ? ' disabled="disabled"' : '' ?>><?= $nbsp2 . $tag2['name'] ?></option>
                                    <?php foreach ($tag2['children'] as $tag3) { ?>
                                        <option><?= $nbsp3 . $tag3['name'] ?></option>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="row">
            <?php if (in_array('categories', $products_search['filter_types'])) { ?>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="products-search-categories">Категории</label>
                        <select id="products-search-categories" name="products_search[categories][]" class="form-control" multiple="multiple" size="25">
                            <?php for ($i = 0, $n = count($products_search['categories']); $i < $n; ++$i) { ?>
                                <option value="<?= $products_search['categories'][$i]['id'] ?>">
                                    <?= str_repeat('&nbsp;', (int)$products_search['categories'][$i]['level'] * 4) . $products_search['categories'][$i]['name'] ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            <?php
            }

            if (in_array('filters', $products_search['filter_types'])) {
            ?>
            <div class="col-xs-6">
                <div class="form-group">
                    <label for="products-search-filters">Фильтры</label>
                    <select id="products-search-filters" name="products_search[filters][]" class="form-control" multiple="multiple" size="25">
                        <?php for ($i = 0, $n = count($products_search['filters']); $i < $n; ++$i) { ?>
                            <option disabled="disabled"><?= $products_search['filters'][$i]['name'] ?></option>
                                <?php for ($y = 0, $m = count($products_search['filters'][$i]['filters']); $y < $m; ++$y) { ?>
                                    <option value="<?= $products_search['filters'][$i]['filters'][$y]['id'] ?>">
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <?= $products_search['filters'][$i]['filters'][$y]['name'] ?>
                                    </option>
                                <?php } ?>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <?php } ?>
        </div>
        <div class="row">
            <?php if (in_array('tms', $products_search['filter_types'])) { ?>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="products-search-tms">Торговые марки</label>
                        <select id="products-search-tms" name="products_search[tms][]" class="form-control"
                                multiple="multiple" size="25">
                            <?php for ($i = 0, $n = count($products_search['tms']); $i < $n; ++$i) { ?>
                                <option value="<?= $products_search['tms'][$i]['id'] ?>"><?= $products_search['tms'][$i]['name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <?php
            }

            if (in_array('lines', $products_search['filter_types'])) {
            ?>
            <div class="col-xs-6">
                <div class="form-group">
                    <label for="products-search-lines">Линейки</label>
                    <select id="products-search-lines" name="products_search[lines][]" class="form-control" multiple="multiple" size="25">
                        <?php for ($i = 0, $n = count($products_search['lines']); $i < $n; ++$i) { ?>
                            <option value="<?= $products_search['lines'][$i]['id'] ?>"><?= '[' . $products_search['lines'][$i]['tm_name'] . '] ' . $products_search['lines'][$i]['name'] ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <?php } ?>
        </div>
        <?php if (in_array('tags', $products_search['filter_types'])) { ?>
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="products-search-copy-tag">Копировать товары из тега</label>
                        <select id="products-search-copy-tag" name="copy_tags[]" class="form-control" multiple="multiple" size="20">
                            <?php foreach ($products_search['tags'] as $tag1) { ?>
                                <option<?= !$tag1['is_tag'] ? ' disabled="disabled"' : ' value="' . $tag1['id'] . '"' ?>><?= ($tag1['is_tag'] ? $tag1['id'] . ': ' : '') . $tag1['name'] ?></option>
                                <?php foreach ($tag1['children'] as $tag2) { ?>
                                    <option<?= !$tag2['is_tag'] ? ' disabled="disabled"' : ' value="' . $tag2['id'] . '"' ?>><?= $nbsp2 . ($tag2['is_tag'] ? $tag2['id'] . ': ' : '') . $tag2['name'] ?></option>
                                    <?php foreach ($tag2['children'] as $tag3) { ?>
                                        <option value="<?= $tag3['id'] ?>"><?= $nbsp3 . ($tag3['is_tag'] ? $tag3['id'] . ': ' : '') . $tag3['name'] ?></option>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-xs-6">
                    <br>
                    <input type="hidden" id="do-copy-tags" name="do_copy_tags" value="">
                    <input id="copy-from-tags-btn" type="submit" class="btn btn-danger" value="Скопировать товары из выбранных тегов">
                </div>
            </div>
        <?php } ?>
        <div class="form-group">
            <label for="products-search-custom-ids">Ручной список ID товаров (Через запятую)</label>
            <input type="text" id="products-search-custom-ids" class="form-control" name="products_search[custom_ids]">
        </div>
        <div class="form-group">
            <h4>Совместное применение условий</h4>
            <label class="radio-inline">
                <input type="radio" name="products_search[and]" value="1" checked="checked"> [И]
            </label>
            <label class="radio-inline">
                <input type="radio" name="products_search[and]"> [ИЛИ]
            </label>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Искать товары">
        </div>
    </form>
    <?php if (in_array('added', $products_search['filter_types'])) { ?>
        <div class="form-group">
            <a href="#" class="btn btn-info" id="products-search-show-added">Показать ранее добавленные товары</a>
        </div>
    <?php } ?>
    <div class="results-box"></div>
</div>
