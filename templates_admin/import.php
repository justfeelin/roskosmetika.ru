<?php
/**
 * @var array $msg
 * @var string $content
 */
?>
<?php foreach ($msg as $item) { ?>
    <div class="alert alert-block"><?= $item ?></div>
<?php } ?>

<form enctype="multipart/form-data" action="/admin/<?= ($content !== 'canonical' ? 'import/' : '') . $content ?>" method="POST">
    <?php if ($content !== 'canonical') { ?>
        <div class="form-group">
            <textarea rows="2" name="remark" class="form-control" placeholder="Примечание"></textarea>
        </div>
    <?php } ?>

    <div class="form-group">
        <input type="file" name="file_<?= $content ?>" style="display:none;" class="input-file"/>
    </div>

    <div class="input-append form-group">
        <input type="text" name="subfile" class="input-medium subfile form-control" placeholder="Выбор файла">
        <a class="btn" onclick="$('.input-file').click();">Обзор</a>
    </div>
    <p>
        <button type="submit" name="submit" class="btn btn-primary">Отправить</button>
    </p>
</form>
