<?php
/**
 * @var array $subscribes
 * @var array $master_class
 */
if ($subscribes) {
?>
    <p><a href="/admin/master_classes/upload/<?= $master_class['id'] ?>">Выгрузить в файл</a></p>
    <p>&nbsp;</p>
    <p><?= $master_class['day'] . '.' . $master_class['month'] . '.' . $master_class['year'] . '"' . $master_class['name'] . '"' ?></p>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Дата</th>
            <th>ФИО</th>
            <th>Телефон</th>
            <th>Email</th>
            <th>Цена</th>
            <th>Сайт</th>
        </tr>
        </thead>
        <tbody>
        <?php for ($i = 0, $n = count($subscribes); $i < $n; ++$i) { ?>
            <tr>
                <td><?= $subscribes[$i]['date'] ?></td>
                <td><?= $subscribes[$i]['surname'] . ' ' . $subscribes[$i]['name'] . ' ' . $subscribes[$i]['patronymic'] ?></td>
                <td><?= $subscribes[$i]['phone'] ?></td>
                <td><?= $subscribes[$i]['email'] ?></td>
                <td><?= $subscribes[$i]['price'] ?></td>
                <td><?= $subscribes[$i]['source'] ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php } else { ?>
    <p>Нет данных.</p>
<?php
}
