<form class="form-horizontal" action="/admin/promo_codes/generete_in_excel" method="post">
    <div class="form-group">
        <div class="col-xs-12">
            <label class="control-label">Количество кодов</label>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-2">
            <input class="form-control" name="code_quan"></input>
        </div>
        <div class="col-xs-10"></div>
    </div>

    <div class="form-group">
        <div class="col-xs-12">
            <label class="control-label">Размер скидки:</label>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-1">
            <input class="form-control" name="discount"></input>
        </div>
        <div class="col-xs-11"></div>
    </div>

    <div class="form-group">
        <div class="col-xs-8">
            <button type="submit" class="btn btn-primary" name="save">Создать и выгрузить</button>
        </div>
        <div class="col-xs-4"></div>
    </div>
</form>
