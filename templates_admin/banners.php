<?php
/**
 * @var array $Pagination
 * @var array $content
 */
$paginationHTML = !empty($Pagination) ? Template::get_tpl('pagination', [
    'Pagination' => $Pagination,
]) : '';

echo $paginationHTML;
?>
<a href="/admin/banners/add" class="btn btn-primary">
    Новый баннер
</a>

<?php if ($content['banners']) { ?>
    <table class="pages table table-striped">
        <thead>
        <tr class="head">
            <th align="left">Название</th>
            <th>Начало</th>
            <th>Окончание</th>
            <th>Формат</th>
            <th>Видимость</th>
            <th>Позиция</th>
            <th>Промо-код</th>
        </tr>
        </thead>
        <tbody>
        <?php for ($i = 0, $n = count($content['banners']); $i < $n; ++$i) { ?>
            <tr
                <?= !$content['banners'][$i]['visible'] || ($content['banners'][$i]['end'] <= $_SERVER['REQUEST_TIME'] && $content['banners'][$i]['end']) || ($content['banners'][$i]['begin'] >= $_SERVER['REQUEST_TIME']) ? 'id="deactive"' : '' ?>>
                <td align="left"><a
                        href="/admin/banners/edit/<?= $content['banners'][$i]['id'] ?>"><?= $content['banners'][$i]['name'] ?></a>
                </td>
                <td><?= $content['banners'][$i]['begin'] ?: '-' ?></td>
                <td><?= $content['banners'][$i]['end'] ?: '-' ?></td>
                <td><?= $content['banners'][$i]['type'] ?></td>
                <td>
                    <a href="/admin/banners/show/<?= $content['banners'][$i]['id'] ?>"><?= !$content['banners'][$i]['visible'] ? 'показать' : 'скрыть' ?></a>
                </td>
                <td><?= $content['banners'][$i]['position'] ?></td>
                <td><input type="checkbox" disabled="disabled"<?= $content['banners'][$i]['copy_area'] ? ' checked = "checked" ' : '' ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php
}

echo $paginationHTML;
