<?php
/**
 * @var array $content
 */
?>
<p><a href="/admin/articles/add/" class="btn btn-primary">Добавить раздел</a></p>
<?php if (isset($content['articles'])) { ?>
<table class="table table-striped">
    <tbody>
        <?php for ($i = 0, $n = count($content['articles']); $i < $n; ++$i) { ?>
            <tr>
                <th class="al_l"><a
                        href="/admin/articles/edit/<?= $content['articles'][$i]['id'] ?>"><?= $content['articles'][$i]['name'] ?></a>
                </th>
                <th width="100"><a href="/admin/articles/add/<?= $content['articles'][$i]['id'] ?>"
                                   title="добавить статью в раздел">+</a></th>
                <th><a
                        href="/admin/articles/show/<?= $content['articles'][$i]['id'] ?>"><?= $content['articles'][$i]['visible'] ? 'скрыть' : 'показать' ?></a>
                </th>
            </tr>
            <?php

            if ($content['articles'][$i]['articles']) {
                for ($y = 0, $m = count($content['articles'][$i]['articles']); $y < $m; ++$y) {
                    ?>
                    <tr>
                        <td>
                            <a href="/admin/articles/edit/<?= $content['articles'][$i]['articles'][$y]['id'] ?>"><?= $content['articles'][$i]['articles'][$y]['name'] ?></a>
                        </td>
                        <th colspan="2"><a
                                href="/admin/articles/show/<?= $content['articles'][$i]['articles'][$y]['id'] ?>"><?= $content['articles'][$i]['articles'][$y]['visible'] ? 'скрыть' : 'показать' ?></a>
                        </th>
                    </tr>
        <?php
                }
            }
        }
        ?>
    </tbody>
</table>
<?php } else { ?>
    <p>Нет статей.</p>
<?php
}
