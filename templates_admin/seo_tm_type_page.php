<?php
/**
 * @var array $content
 */
?>
<h2><?= h($content['item']['type_page_name']) . ' ' . $content['item']['tm_name'] ?></h2>

<a target="_blank" href="<?= DOMAIN_FULL . '/kosmetika/' . $content['item']['type_page_url'] . '/' . $content['item']['tm_url'] ?>"><?= DOMAIN_FULL . '/kosmetika/' . $content['item']['type_page_url'] . '/' . $content['item']['tm_url'] ?></a>

<form method="POST" action="/admin/seo/tm_type_pages/<?= $content['item']['type_page_id'] . '/' . $content['item']['tm_id'] ?>">
    <div class="form-group">
        <label for="item-h1">&lt;h1&gt;</label>
        <input type="text" class="form-control" name="item[h1]" id="item-h1" value="<?= h($content['item']['h1']) ?>">
    </div>

    <div class="form-group">
        <label for="item-title">&lt;title&gt;</label>
        <textarea class="form-control" name="item[title]" id="item-title" rows="5"><?= h($content['item']['title']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="item-description">&lt;meta name="description"&gt;</label>
        <textarea class="form-control" name="item[description]" id="item-description" rows="5"><?= h($content['item']['description']) ?></textarea>
    </div>

    <div class="form-group">
        <label for="item-text">Текст на странице</label>
        <textarea class="form-control" name="item[text]" id="item-text" rows="5"><?= h($content['item']['text']) ?></textarea>
    </div>

    <input type="submit" class="btn btn-primary" value="Сохранить">
</form>
