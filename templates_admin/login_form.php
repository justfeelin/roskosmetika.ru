<?php
/**
 * @var string $title
 * @var string $header
 * @var array $css
 * @var string $charset
 * @var array $scripts
 * @var string $page
 */
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <title>Панель управления</title>

    <?php for ($i = 0, $n = count($css); $i < $n; ++$i) { ?>
        <link rel="stylesheet" type="text/css" href="/css/<?= $css[$i] ?>">
    <?php } ?>

    <meta http-equiv="Content-type" content="text/html; charset=UTF-8">
</head>
<body>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <form method="POST" id="login-form" action="<?= $_SERVER['REQUEST_URI'] ?>">
                <div class="form-group">
                    <label for="loginUsername">Логин</label>
                    <input id="loginUsername" type="text" name="username" class="form-control">
                </div>
                <div class="form-group">
                    <label for="loginPassword">Пароль</label>
                    <input id="loginPassword" type="password" name="password" class="form-control">
                </div>
                <button type="submit" class="btn btn-primary">Вход</button>
            </form>
        </div>
    </div>
</div>

</body>
</html>
