<?php

abstract class email_approve
{
    /**
     * Email
     */
    const APPROVE_URL = '/email_approve/';

    /**
     * Returns row by E-mail
     *
     * @param string $email E-mail
     *
     * @return array
     */
    public static function get_by_email($email)
    {
        return DB::get_row('SELECT e.* FROM rk_email_approve AS e WHERE e.email = LOWER("' . DB::escape($email) . '") LIMIT 1');
    }

    /**
     * Creates new record
     *
     * @param string $email E-mail
     * @param string $password Password
     * @param string $name User name
     *
     * @return string Approve URL
     */
    public static function add($email, $password, $name = null)
    {
        $token = md5(uniqid($email));

        DB::query('INSERT INTO rk_email_approve (email, password, token, name, approved, client_id, added_date, approved_date) VALUES (LOWER("' . DB::escape($email) . '"), "' . DB::escape($password) . '", "' . $token . '", "' . ($name ? DB::escape($name) : '') . '", 0, 0, NOW(), NULL)');

        return self::build_url($token);
    }

    /**
     * Returns row by token
     *
     * @param string $token Token
     *
     * @return array
     */
    public static function get_by_token($token)
    {
        return DB::get_row('SELECT a.* FROM rk_email_approve AS a WHERE a.token = "' . DB::escape($token) . '" LIMIT 1');
    }

    /**
     * Approves email
     *
     * @param int $id Row ID
     * @param int $clientID Client ID to set
     */
    public static function approve($id, $clientID)
    {
        DB::query('UPDATE rk_email_approve SET client_id = ' . ((int)$clientID) . ', approved = 1, approved_date = NOW() WHERE id = ' . (int)$id);
    }

    /**
     * Returns approve URL with given token
     *
     * @param string $token Token
     *
     * @return string
     */
    public static function build_url($token)
    {
        return DOMAIN_FULL . self::APPROVE_URL . $token;
    }
}
