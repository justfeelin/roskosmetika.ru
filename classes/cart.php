<?php

/**
 *  Cart control
 */
class Cart
{
    /**
     * Own referral discount type code
     */
    const OWN_REFERRAL_CODE = 'own-referral';

    /**
     * Use current prices (Not from database e.g. for old orders)
     *
     * @var bool
     */
    public static $currentPrices = true;

    /**
     * Order status is new (Used for delivery calculation)
     *
     * @var bool
     */
    public static $statusNew = false;

    /**
     * @var array Shared cart data
     */
    public static $data = [];

    /**
     * Discount nature type
     *
     * @var string
     */
    public static $discountType = null;

    /**
     * Discount (0 - 100), maximum of summary and personal discount
     *
     * @var integer
     */
    public static $discount = null;

    /**
     * @var int Shipping region ID
     */
    private static $_region = null;

    /**
     * @var int Shipping type
     */
    private static $_shippingType = null;

    /**
     * @var int Shipping cost
     */
    private static $_shippingPrice = null;

    /**
     * @var int Order ID value
     */
    private static $_orderID = null;

    /**
     * Returns order cost information
     *
     * @param array $products Array of order products. Each product must contain 'price', 'special_price', 'quantity', 'apply_discount' fields
     * @param int $clientAutoDiscount Automatically calculated client discount
     * @param int $clientManualDiscount Client discount set ba manager
     * @param int $promoID Promo code ID
     * @param bool $admitadPromo Admitad promo code
     * @param int $shippingCost Shipping cost
     * @param int $shippingTypeID Shipping type ID
     * @param int $regionID Region ID to calculate shipping cost & compensation
     * @param bool $extendedProducts Return extended products info
     * @param int $referralDiscount Referral discount
     * @param int $ownReferralsDiscount Discount from own referrals
     * @param bool $calculateDiscount Calculate discounts
     *
     * @return array <pre>[
     *      'products' => [
     *          [
     *              'id' => `Product ID`,
     *              'quantity' => `Product quantity in cart`,
     *              'oldPrice' => 'Product original price`,
     *              'price' => `Product final price`,
     *              'sum' => `Product summary cost`,
     *              'special' => `Whether product has 'special_price'`,
     *              ['name' => `Product name`,]
     *              ['pack' => `Product pack name`,]
     *              ['url' => `Product URL`,]
     *              ['src' => `Product image name`,]
     *              ['alt' => `Product image alt`,]
     *              ['photo_title' => `Product image title`,]
     *              ['brand_url' => `Product trademark URL`,]
     *          ],
     *          ...
     *      ],
     *      'discount' => `Order discount (Percents)`,
     *      'discountCode' => `Order discount code ('sum', 'individual', 'referral', 'own-referral', 'promo')`,
     *      'discountName' => `Human-read discount name`,
     *      'promoDiscount' => `Discount from regular promo code (Value in roubles)`,
     *      'promoDiscountPercent' => `Discount from promo code (In percents)`,
     *      'certificate' => `Promo code is used as certificate`,
     *      'discounts' => [
     *          'sum' => `Discount for order sum`,
     *          'clientAuto' => `Automatically calculated client discount`,
     *          'clientManual' => `Manually assigned client discount`,
     *          'promo' => `Promo code discount (For NOT regular promo code only)`,
     *      ],
     *      'present' => `false if no present, gained sum for present otherwise`,
     *      'oldShipping' => `Passed shipping value`,
     *      'shipping` => `Calculated shipping`,
     *      'shippingCompensation' => `Shipping compensation amount`,
     *      'shippingCompensationSum' => `Minimal order sum needed for shipping compensation`,
     *      'rawSum' => `Products raw sum (Without discounts, but with respect of special prices)`,
     *      'sum' => `Products summary cost`,
     * ]</pre>
     */
    public static function orderCost($products, $clientAutoDiscount, $clientManualDiscount, $promoID = 0, $admitadPromo = false, $shippingCost = null, $shippingTypeID = null, $regionID = null, $extendedProducts = false, $referralDiscount = 0, $ownReferralsDiscount = 0, $calculateDiscount = true)
    {
        $discount = 0;

        $discountName = $discountCode = null;

        $productsN = count($products);

        $promoProductIDs = $promoTmIDs = $promoCategoryIDs = [];

        $promoCheapestDiscountIDs = null;

        $rawSum = 0;

        for ($i = 0; $i < $productsN; ++$i) {
            $products[$i]['id'] = (int)$products[$i]['id'];

            $products[$i]['special_price'] = roundPrice($products[$i]['special_price']);
            $products[$i]['price'] = roundPrice($products[$i]['price']);

            $products[$i]['quantity'] = (int)$products[$i]['quantity'];

            $rawSum += ($products[$i]['special_price'] ?: $products[$i]['price']) * $products[$i]['quantity'];
        }

        $promo = null;

        $promoSumOk = $simplePromo = $promoInRoubles = $promoCertificate = $promo_end_days = false;

        $promoDiscount = 0;

        if ($productsN) {
            $promo = $promoID ? promo_codes::get_by_id($promoID, $admitadPromo) : null;

            if ($promo) {
                $simplePromo = (int)$promo['action_type_id'] === 0 || $promo['discount'] < 0;

                $promoInRoubles = $promo && $promo['discount'] < 0;

                $promoCertificate = $promoInRoubles && $promo['certificate'];

                $promoSumOk = !$promo['min_sum'] || $promo['min_sum'] <= $rawSum;

                $promoDiscount = $promoInRoubles ? (int)$promo['discount'] * -1 : (float)$promo['discount'];

                $promo_end_days = promo_codes::get_end_day_info($promoID); 
            }
        }

        if ($productsN && $calculateDiscount) {
            if ($promoSumOk && !$simplePromo) {
                switch ((int)$promo['action_type_id']) {
                    case promo_codes::ACTION_TYPE_PRODUCTS:
                        $promoProductIDs = promo_codes::get_action_product_ids($promoID);

                        break;

                    case promo_codes::ACTION_TYPE_TMS:
                        $promoTmIDs = promo_codes::get_action_tm_ids($promoID);

                        break;

                    case promo_codes::ACTION_TYPE_CATEGORIES:
                        $promoCategoryIDs = promo_codes::get_action_category_ids($promoID);

                        break;

                    case promo_codes::ACTION_TYPE_CHEAPEST_PRODUCT_DISCOUNT:
                        $promoCheapestDiscountIDs = promo_codes::get_action_cheapest_discount_product_ids($promoID);

                        break;
                }
            }

            $promoType = $promo ? (int)$promo['discount_type'] : 0;

            $sumDiscounts = [
                [
                    100000, 20,
                ],
                [
                    50000, 10,
                ],
                [
                    20000, 5,
                ],
            ];

            $sumDiscount = $sumGreater = 0;

            for ($i = 0, $n = count($sumDiscounts); $i < $n; ++$i) {
                if ($rawSum >= $sumDiscounts[$i][0]) {
                    $sumDiscount = $sumDiscounts[$i][1];
                    $sumGreater = $sumDiscounts[$i][0];

                    break;
                }
            }

            if ($sumDiscount !== 0) {
                $discount = $sumDiscount;
                $discountName = 'Сумма заказа от ' . formatPrice($sumGreater) . ' рублей';
                $discountCode = 'sum';
            }

            $userDiscount = max($clientAutoDiscount, $clientManualDiscount);

            if ($userDiscount > $discount) {
                $discount = $userDiscount;
                $discountName = 'Индивидуальная скидка';
                $discountCode = 'individual';
            }

            if ($referralDiscount > $discount) {
                $discount = $referralDiscount;
                $discountName = 'Приглашение от друга';
                $discountCode = 'referral';
            } else {
                if ($ownReferralsDiscount > $discount) {
                    $discount = $ownReferralsDiscount;
                    $discountName = 'Приведённый друг';
                    $discountCode = self::OWN_REFERRAL_CODE;
                }
            }

            if (
                $promoSumOk
                && !$promoInRoubles
                && $promoDiscount > $discount
                && $promoType === 1
                && $simplePromo
            ) {
                $discount = $promoDiscount;
                $discountName = 'Промо-код';
                $discountCode = 'promo';
            }
        } else {
            $sumDiscount = 0;

            $promoType = 0;
        }

        $productsResult = [];

        $sum = 0;

        $appliedProductPromo = 0;

        $promoCheapestDiscountIs = [];

        $cheapestProductI = null;

        $addedPromoProducts = 0;

        if ($promoCheapestDiscountIDs !== null) {
            for ($i = 0; $i < $productsN; ++$i) {
                if (!$promoCheapestDiscountIDs || in_array($products[$i]['id'], $promoCheapestDiscountIDs)) {
                    $promoCheapestDiscountIs[] = $i;

                    $addedPromoProducts += $products[$i]['quantity'];
                }
            }

            if ($addedPromoProducts > 0 && ((int)$promo['min_action_products_count'] === 0 || $addedPromoProducts >= $promo['min_action_products_count'])) {
                $minPrice = null;

                for ($i = 0, $n = count($promoCheapestDiscountIs); $i < $n; ++$i) {
                    if ($minPrice === null || $products[$promoCheapestDiscountIs[$i]]['price'] > 0 && $products[$promoCheapestDiscountIs[$i]]['price'] < $minPrice) {
                        $minPrice = $products[$promoCheapestDiscountIs[$i]]['price'];
                        $cheapestProductI = $promoCheapestDiscountIs[$i];
                    }
                }
            }
        }

        for ($i = 0; $i < $productsN; ++$i) {
            $productsResult[$i] = [
                'id' => $products[$i]['id'],
                'quantity' => $products[$i]['quantity'],
                'oldPrice' => $products[$i]['price'],
                'specialPrice' => $products[$i]['special_price'],
            ];

            $productDiscount = $specialDiscount = 0;

            $cheapestProductDiscount = $cheapestProductI === $i;

            if ($cheapestProductDiscount) {
                $price = roundPrice($productsResult[$i]['oldPrice'] * (100 - $promoDiscount) / 100.0);

                $appliedProductPromo += ($productsResult[$i]['oldPrice'] - $price);

                $productsResult[$i]['price'] = $price;

                $productsResult[$i]['sum'] = ($productsResult[$i]['quantity'] > 1 ? $productsResult[$i]['oldPrice'] * ($productsResult[$i]['quantity'] - 1) : 0) + $price;

                $productsResult[$i]['cheapestDiscount'] = true;

                $productsResult[$i]['special'] = false;
            } else {
                $productInfo = DB::get_row('SELECT p.purchase_price, p.tm_id FROM products AS p WHERE p.id = ' . $productsResult[$i]['id']);

                $specialDiscount = $productsResult[$i]['specialPrice'] && $productsResult[$i]['oldPrice'] ? 100 - (int)(((float)$productsResult[$i]['specialPrice'] / (float)$productsResult[$i]['oldPrice']) * 100) : 0;

                $productsResult[$i]['special'] = $specialDiscount > 0;

                if ($products[$i]['apply_discount']) {
                    $productDiscount = $discount;

                    if (
                        !$promoInRoubles && $promoDiscount > $discount && $promoSumOk && !$simplePromo && $cheapestProductI === null
                        && (
                            $promoProductIDs && in_array($productsResult[$i]['id'], $promoProductIDs)
                            || $promoTmIDs && in_array((int)$productInfo['tm_id'], $promoTmIDs)
                            || $promoCategoryIDs && array_intersect(Product::get_categories($products[$i]['id']), $promoCategoryIDs)
                        )
                    ) {
                        $productDiscount = $promoDiscount;

                        $appliedProductPromo += $productsResult[$i]['quantity'] * ($productsResult[$i]['oldPrice'] - (int)($productsResult[$i]['oldPrice'] * (100.0 - $productDiscount) / 100.0));
                    }
                }

                $productsResult[$i]['price'] = $specialDiscount > $productDiscount
                    ? $productsResult[$i]['specialPrice']
                    : (
                        $productDiscount !== 0
                            ? roundPrice((float)$products[$i]['price'] * (100.0 - $productDiscount) / 100.0)
                            : $productsResult[$i]['oldPrice']
                    );

                $minPossiblePrice = roundPrice(((float)$productInfo['purchase_price']) * 1.2);

                if ($minPossiblePrice && ($productsResult[$i]['price'] < $minPossiblePrice)) {
                    $productsResult[$i]['price'] = $minPossiblePrice;
                }

                $productsResult[$i]['sum'] = $productsResult[$i]['price'] * $productsResult[$i]['quantity'];
            }

            $sum += $productsResult[$i]['sum'];

            if ($extendedProducts) {
                $product = DB::get_row(
                    'SELECT IF(p.main_id = 0, p.name, pr.name) AS name, p.pack AS pack, IF(p.main_id = 0, IF(p.url != "", p.url, p.id), IF(pr.url != "", pr.url, pr.id)) AS url,
                        pp.src, pp.alt, pp.title AS photo_title,
                        IF(t.url != "", t.url, t.id) AS brand_url, t.name AS brand,
                        (SELECT rc.name FROM rk_prod_cat_lvl_1 pc_1, rk_categories rc WHERE pc_1.prod_id = p.id AND pc_1.cat_id = rc.id AND rc.visible = 1 LIMIT 1) AS category
                        FROM products AS p
                        INNER JOIN rk_prod_desc AS pd ON pd.id = IF(p.main_id = 0, p.id, p.main_id)
                        INNER JOIN tm AS t ON t.id = IF(p.main_id = 0, p.tm_id, (SELECT pm.tm_id FROM products AS pm WHERE pm.id = p.main_id LIMIT 1))
                        LEFT JOIN rk_prod_photo AS pp ON pp.id = pd.id
                        LEFT JOIN products AS pr ON p.main_id != 0 AND pr.id = p.main_id
                        WHERE p.id = ' . $productsResult[$i]['id']
                );

                $product['ua_name'] = export::anti_typograf($product['name'] . ' ' . $product['brand']);
                $product['ua_pack'] = export::anti_typograf($product['pack']);
                $product['category'] = is_null($product['category']) ? 'no-category' : export::anti_typograf($product['category']);

                $productsResult[$i] += $product;

                $productsResult[$i]['apply_discount'] = !!$products[$i]['apply_discount'];
            }
        }

        $mainPromoDiscount = $promoDiscount !== 0
            && $promoSumOk
            && $simplePromo
            && (
                $calculateDiscount
                && (
                    $promoType === 0 || $promoInRoubles
                )
                || $promoCertificate
            )
                ? (
                    $promoInRoubles
                        ? min($promoDiscount, $sum)
                        : ceil((float)$sum * (float)$promoDiscount / 100.0)
                )
                : 0;

        $promoSum = $mainPromoDiscount ? $sum - $mainPromoDiscount : $sum;

        $shippingCompensation = $shippingCompensationSum = null;

        if ($regionID) {
            $regionID = (int)$regionID;

            if ($shippingTypeID !== null && $shippingTypeID != Orders::CUSTOM_SHIPPING) {
                if ($shippingCost === null) {
                    $shippingCost = DB::get_field('price', 'SELECT sr.price_' . ((int)$shippingTypeID) . ' AS price FROM shipping_regions AS sr WHERE sr.id = ' . $regionID);
                }

                $row = DB::get_row(
                    'SELECT src.minimal_sum, src.compensation_1, src.compensation_2, src.compensation_3, src.compensation_4, src.compensation_6
                    FROM shipping_regions_compensations AS src
                    WHERE src.region_id = ' . $regionID . ' AND src.minimal_sum <= ' . $promoSum . '
                    ORDER BY src.minimal_sum DESC LIMIT 1'
                );

                if ($row) {
                    $key = null;

                    switch ((int)$shippingTypeID) {
                        case Orders::DELIVERY_COURIER_TYPE_ID:
                            $key = 'compensation_1';

                            break;

                        case Orders::DELIVERY_POST_TYPE_ID:
                            $key = 'compensation_2';

                            break;

                        case Orders::DELIVERY_COMPANY_TYPE_ID:
                            $key = 'compensation_3';

                            break;

                        case Orders::DELIVERY_PICKPOINT_TYPE_ID:
                            $key = 'compensation_4';

                            break;

                        case Orders::DELIVERY_PICKUP_TYPE_ID:
                            $key = 'compensation_6';

                            break;
                    }

                    if ($key !== null) {
                        $shippingCompensation = $row[$key] === null ? true : (int)$row[$key];
                        $shippingCompensationSum = (int)$row['minimal_sum'];
                    }
                }
            }
        }

        $shippingCostOriginal = $shippingCost !== null ? (int)$shippingCost : null;

        if ($shippingCompensation === null) {
            $shippingCompensation = $promoSum >= Orders::SHIPPING_COMPENSATION_SUM ? Orders::SHIPPING_COMPENSATION : null;

            if ($shippingCompensation) {
                $shippingCompensationSum = Orders::SHIPPING_COMPENSATION_SUM;
            }
        }

        $shippingCost = $shippingCostOriginal !== null
            ? (
                $shippingCompensation
                    ? (
                        $shippingCompensation === true
                            ? 0
                            : max($shippingCostOriginal - $shippingCompensation, 0)
                    )
                    : $shippingCostOriginal
            )
            : null;

        return [
            'products' => $productsResult,
            'discount' => (int)$discount,
            'discountCode' => $discountCode,
            'discountName' => $discountName,
            'promo_end_days' => $promo_end_days,
            'promoDiscount' => $promoType === 0 && $simplePromo || ($promoInRoubles || $promoCertificate) ? ($mainPromoDiscount ?: $appliedProductPromo) : 0,
            'promoDiscountPercent' => $promo && $simplePromo ? $promoDiscount : 0,
            'certificate' => $promoCertificate,
            'promo_in_roubles' => $promoInRoubles,
            'discounts' => [
                'sum' => $sumDiscount,
                'clientAuto' => $clientAutoDiscount,
                'clientManual' => $clientManualDiscount,
                'promo' => $promoDiscount !== 0 && $promoSumOk && $promoType === 1 ? $promoDiscount : 0,
            ],
            'present' => $promoSum > Orders::PRESENT_SUM ? Orders::PRESENT_SUM : false,
            'oldShipping' => $shippingCostOriginal,
            'shipping' => $shippingCost,
            'shippingCompensation' => $shippingCompensation === true ? true : ($shippingCompensation ?: 0),
            'shippingCompensationSum' => $shippingCompensationSum,
            'rawSum' => $rawSum,
            'sum' => $promoSum,
        ];
    }

    /**
     * Returns order id
     *
     * @param int|bool $id ID to use. If true - new order will be created if no order found. If not passed - returns ID from database
     *
     * @return int
     */
    public static function orderID($id = null)
    {
        static $check;

        if ($id !== null && $id !== true) {
            self::$_orderID = $id;

            $check = true;

            self::$_shippingType = self::$_shippingPrice = null;

            if (!CONSOLE) {
                $_SESSION['user']['order_id'] = $id;
            }

            return $id;
        }

        if ($check === null || self::$_orderID === null || $id) {
            self::$_orderID = !CONSOLE ? $_SESSION['user']['order_id'] : 0;

            self::$_shippingType = self::$_shippingPrice = null;

            if (!self::$_orderID && $id === true) {
                $clientID = Auth::is_auth();

                list(self::$_orderID, $hash) = self::new_order($clientID);

                self::$_orderID = DB::get_last_id();

                Users::update_session($clientID, $hash);

                if (!$clientID && !CONSOLE) {
                    $_SESSION['user']['order_id'] = self::$_orderID;
                }
            }

            $check = true;
        }

        return self::$_orderID;
    }

    /**
     * Returns cart info
     *
     * @param Bool $setData Set self::$data to the returned value. If false - values will be returned
     *
     * @return bool|array If $setData === true: boolean whether contains products, cart info otherwise
     */
    public static function setInfo($setData = true)
    {
        $data = [
            'user' => Users::get_user_info(),
        ];

        if (!self::orderID()) {
            $data['info'] = [
                'quantity' => 0,
                'discount' => 0,
                'discount_code' => null,
                'discount_type' => null,
                'promo_end_days' => false,
                'promo_applied' => false,
                'raw_sum' => 0,
                'sum_with_discount' => 0,
                'promo_discount' => 0,
                'promo_sum' => 0,
                'promo_diff' => 0,
                'promo_min_sum' => 0,
                'certificate' => false,
                'promo_in_roubles' => false,
                'discounts' => [
                    'sum' => 0,
                    'clientAuto' => 0,
                    'clientManual' => 0,
                    'promo' => 0,
                ],
                'shipping' => null,
                'shipping_name' => null,
                'shipping_compensation' => false,
                'shipping_price' => null,
                'shipping_price_original' => null,
                'region' => null,
                'present' => false,
                'hasParfumery' => false,
                'final_sum' => 0,
            ];

            $data['products'] = [];
        } else {
            $user_id = Auth::is_auth() ?: (self::orderID() !== null ? (int)DB::get_field('client_id', 'SELECT o.client_id FROM rk_orders AS o WHERE o.id = ' . self::$_orderID) : 0);

            if ($user_id) {
                $discount = Users::get_discount($user_id);
            } else {
                $discount = [
                    'main_discount' => 0,
                    'auto_discount' => 0,
                ];
            }

            $promo = DB::get_row(
                'SELECT IF(o.admitad_promo = 1, ap.id, p.id) AS id, IF(o.admitad_promo = 1, 0, p.min_sum) AS min_sum, o.admitad_promo, p.discount, p.discount_type
                FROM rk_orders AS o
                LEFT JOIN rk_promo_codes AS p ON p.id = o.promo_code_id AND o.admitad_promo = 0
                LEFT JOIN rk_admitad_promo_codes AS ap ON ap.id = o.promo_code_id AND o.admitad_promo = 1
                WHERE o.id = ' . self::$_orderID . 
                ' AND o.promo_code_id > 0 AND IF(p.end_date IS NOT NULL, p.end_date >= NOW(), 1)'
            );

            if (!$promo['id']) {
                $promo = null;
            } 

            $referralDiscount = (int)DB::get_field('referral_discount', 'SELECT r.referral_discount FROM rk_orders AS o INNER JOIN rk_referrals AS r ON r.id = o.referral_id WHERE o.id = ' . self::$_orderID);

            $ownReferralDiscount = 0;

            if (!$referralDiscount) {
                $ownReferralDiscount = referral::getOwnDiscount($user_id);

                if ($ownReferralDiscount) {
                    $ownReferralDiscount = $ownReferralDiscount['discount'];
                }
            }

            list(self::$_region, self::$_shippingType) = self::getOrderShipping(self::$_orderID);

            if (self::$_region && self::$_shippingType) {
                self::_setShippingInfo();
            }

            $orderInfo = self::orderCost(
                self::get_products(),
                $discount['auto_discount'],
                $discount['main_discount'],
                $promo ? $promo['id'] : 0,
                $promo && $promo['admitad_promo'],
                self::$_shippingPrice !== null && (self::$currentPrices || self::$statusNew) ? self::$_shippingPrice : null,
                self::$_shippingType,
                self::$_region,
                true,
                $referralDiscount,
                $ownReferralDiscount
            );

            $sumWithDiscount = $orderInfo['promoDiscount'] !== 0 ? $orderInfo['sum'] + $orderInfo['promoDiscount'] : $orderInfo['sum'];

            $productIDs = [];

            for ($i = 0, $n = count($orderInfo['products']); $i < $n; ++$i) {
                $productIDs[] = $orderInfo['products'][$i]['id'];
            }

            $data['info'] = [
                'quantity' => count($orderInfo['products']),
                'discount' => $orderInfo['discount'],
                'discount_code' => $orderInfo['discountCode'],
                'discount_type' => $orderInfo['discountName'],
                'promo_end_days' => $orderInfo['promo_end_days'],
                'promo_applied' => !!$promo,
                'raw_sum' => $orderInfo['rawSum'],
                'sum_with_discount' => $sumWithDiscount,
                'promo_discount' => $promo && $orderInfo['promoDiscountPercent'] ? ($promo['discount_type'] == 0 ? (int)$promo['discount'] : 0) : 0,
                'promo_sum' => $orderInfo['sum'],
                'promo_diff' => $orderInfo['promoDiscount'],
                'promo_min_sum' => $promo ? (float)$promo['min_sum'] : 0,
                'certificate' => $orderInfo['certificate'],
                'promo_in_roubles' => $orderInfo['promo_in_roubles'],
                'discounts' => $orderInfo['discounts'],
                'shipping' => self::$_shippingType,
                'shipping_name' => self::$_shippingType !== null ? Orders::$shippings[self::$_shippingType] : null,
                'shipping_compensation' => $orderInfo['shippingCompensation'],
                'shipping_compensation_sum' => $orderInfo['shippingCompensationSum'],
                'shipping_price' => $orderInfo['shipping'],
                'shipping_price_original' => $orderInfo['oldShipping'],
                'region' => self::$_region,
                'present' => $orderInfo['present'],
                'hasParfumery' => Product::has_parfumery_products($productIDs),
                'final_sum' => $orderInfo['sum'] !== 0 ? $orderInfo['sum'] + ($orderInfo['shipping'] ? $orderInfo['shipping'] : 0) : 0,
            ];

            $data['products'] = $orderInfo['products'];
        }

        if ($setData) {
            self::$data = $data;

            return !!self::$data['products'];
        } else {
            return $data;
        }
    }

    /**
     * Updates certificate amount price in current cart
     *
     * @param int $price Certificate price to set
     *
     * @return bool Whether update was done
     */
    public static function setCertificatePrice($price)
    {
        if ($id = self::orderID()) {
            DB::query('UPDATE rk_orders_items SET price = ' . ((int)$price) . ' WHERE product_id = ' . Product::CERTIFICATE_ID . ' AND rk_order_id = ' . $id);

            return !!DB::last_query_info();
        }

        return false;
    }

    /**
     * Sets self::$_shippingPrice
     *
     * @param array $regionRow Region row data
     * @param string $shippingTypeCode Shipping type code
     * @param bool $updateOrder Update orders table with new values
     */
    public static function setShipping($regionRow, $shippingTypeCode, $updateOrder = false)
    {
        self::$_region = (int)$regionRow['id'];
        self::$_shippingType = $shippingTypeCode ? (int)$shippingTypeCode : null;

        self::$_shippingPrice = !self::$currentPrices && !self::$statusNew && self::orderID()
            ? (($price = DB::get_field('delivery', 'SELECT o.delivery FROM rk_orders AS o WHERE o.id = ' . self::$_orderID)) !== null ? (int)$price : null)
            : (
                self::$_shippingType !== null && isset($regionRow['price_' . $shippingTypeCode])
                    ? (int)$regionRow['price_' . $shippingTypeCode]
                    : null
            );

        if ($updateOrder) {
            DB::query('UPDATE rk_orders SET region_id = ' . self::$_region . ', shipping_id = ' . (self::$_shippingType === null ? Orders::CUSTOM_SHIPPING : self::$_shippingType) . ', delivery = ' . (self::$_shippingPrice === null ? 'NULL' : self::$_shippingPrice) . ' WHERE id = "' . self::orderID() . '"');
        }
    }

    /**
     * Returns order region ID and shipping type ID
     *
     * @param int $id Order ID
     *
     * @return array [regionID, shippingTypeID]
     */
    public static function getOrderShipping($id)
    {
        $order = DB::get_row('SELECT o.region_id, o.shipping_id FROM rk_orders AS o WHERE o.id = ' . (int)$id);

        return $order ? [
            $order['region_id'] !== null ? (int)$order['region_id'] : null,
            $order['shipping_id'] !== null ? (int)$order['shipping_id'] : null
        ] : [
            null,
            null,
        ];
    }

    /**
     * Add product to cart
     * @param integer $prod_id product Id
     * @param integer $quantity quantity of added products
     * @return boolean
     */
    static function add_to_cart($prod_id, $quantity = 1)
    {
        $prod_id = (int)$prod_id;
        $quantity = (int)$quantity;

        // Check product for available
        if (!product::check_prod_existence($prod_id, true, true)) {
            return 0;
        } else {
            $order_id = self::orderID(true);

            // Check product existence in cart
            $query = "SELECT quantity
                      FROM   rk_orders_items
                      WHERE  product_id = $prod_id AND
                             rk_order_id = $order_id";
            // change product quantity
            if (DB::get_num_rows($query)) {

                $query = "UPDATE rk_orders_items
                          SET    quantity = quantity + $quantity
                          WHERE  product_id = $prod_id AND
                                 rk_order_id = $order_id";

                return DB::query($query);
            } // Add new product to cart
            else {
                $query = "INSERT INTO rk_orders_items (product_id, quantity, rk_order_id, price, special_price, apply_discount)
                          VALUES ($prod_id,
                                  $quantity,
                                  $order_id,
                                  (SELECT CEIL(price) FROM products WHERE id = $prod_id), (SELECT CEIL(special_price) FROM products WHERE id = $prod_id), (SELECT apply_discount FROM products WHERE id = $prod_id))";

                return DB::query($query);
            }
        }
    }

    /**
     * Return products from cart
     *
     * @return array|null
     */
    static function get_products()
    {
        return self::orderID()
            ? DB::get_rows('SELECT c.product_id AS id, IF(c.product_id = ' . Product::CERTIFICATE_ID . ', c.price, p.price) AS price, c.quantity, IF(c.product_id = ' . Product::CERTIFICATE_ID . ', 0, p.special_price) AS special_price, p.apply_discount FROM rk_orders_items AS c INNER JOIN products AS p ON p.id = c.product_id WHERE c.rk_order_id = ' . self::$_orderID . ' ORDER BY IF(p.special_price > 0, 1, 0) ASC, p.name')
            : [];
    }

    /**
     * Delete product from cart
     * @param integer $prod_id product Id
     * @return  boolean
     */
    static function del_prod($prod_id)
    {
        $orderID = self::orderID();

        if (!$orderID) {
            return false;
        }

        $prod_id = (int)$prod_id;

        $query = "DELETE FROM rk_orders_items
                  WHERE product_id = $prod_id
                  AND   rk_order_id = " . $orderID;

        return !!DB::query($query);
    }

    /**
     * -1 from cart
     * @param integer $prod_id product Id.
     * @param integer $quantity product quantity.
     * @return boolean
     */
    static function subtract($prod_id, $quantity = 1)
    {
        $orderID = self::orderID();

        if (!$orderID) {
            return false;
        }

        $prod_id = (int)$prod_id;
        $quantity = (int)$quantity;

        $query = "SELECT quantity
                  FROM  rk_orders_items
                  WHERE product_id = $prod_id
                  AND   rk_order_id = " . $orderID;
        $quantityInCart = (int)DB::get_field('quantity', $query);

        if ($quantityInCart === 0) {
            return false;
        }

        if ($quantityInCart > $quantity) {
            $query = "UPDATE rk_orders_items
                      SET   quantity = quantity - $quantity
                      WHERE product_id = $prod_id
                      AND   rk_order_id = " . $orderID;
            return !!DB::query($query);
        } else {
            return self::del_prod($prod_id);
        }
    }

    /**
     * Delete all products from cart (clear)
     */
    static function clear_cart()
    {
        if (self::orderID()) {
            DB::query('DELETE FROM rk_orders_items WHERE rk_order_id = ' . self::$_orderID);
        }

        self::$_orderID = null;
    }

    /**
     * Get item from cart by ID for uncomplited order.
     * @param int $item_id Product ID.
     * @return array array('id', 'name', 'pack', 'price', 'quantity', 'cost')
     */
    static function get_item($item_id)
    {
        $item_id = (int)$item_id;

        $query = "SELECT p.id, p.main_id, p.name, IF( p.url =  '', p.id, p.url ) url, p.pack,
                      t.name AS tm,
                      IF(c.price IS NULL, 0, c.price) price,
                      IF(c.quantity IS NULL, 0, c.quantity) quantity,
                      pp.src, pp.alt, pp.title photo_title
                  FROM products AS p
                      LEFT JOIN tm AS t ON t.id = p.tm_id
                      LEFT JOIN rk_orders_items c ON c.product_id = p.id
                      LEFT JOIN rk_prod_photo pp  ON ( pp.id = p.id )
                  WHERE  c.rk_order_id = " . self::orderID() . "
                      AND p.id = $item_id";

        $rows = DB::get_rows($query);

        if ($rows) {
            $descr_id = $rows[0]['main_id'] ? (int)$rows[0]['main_id'] : null;

            $return = products::get_pack_info($rows);
            $return = $return[0];

            $return['id'] = (int)$return['id'];
            $return['price'] = (int)$return['price'];
            $return['quantity'] = (int)$return['quantity'];
            $return['special'] = self::spec_links($item_id);

            if ($descr_id !== null) {
                $parent = DB::get_row('SELECT IF(p.url = "", p.id, p.url) AS url,
                    t.name AS tm
                    FROM products AS p
                    LEFT JOIN tm AS t ON t.id = p.tm_id
                    WHERE p.id = ' . $descr_id);

                $return = array_merge($return, $parent);
            }
        } else {
            $return = null;
        }

        return $return;
    }

    /**
     * Return links on line and article
     * @param  int $item_id product ID
     * @return array  article and line
     */
    protected static function spec_links($item_id)
    {
        //  check descr_id
        $item_id = product::check_descr_id($item_id);

        $query = "SELECT a.id, IF( a.url =  '', a.id, a.url ) url, a.name
                  FROM   rk_articles a, rk_prodarticles pa
                  WHERE  pa.prod_id = $item_id
                  AND    pa.article_id = a.id
                  AND    a.parent_id != 0
                  LIMIT 1";

        $article = db::get_row($query);

        $query = "SELECT l.id, CONCAT('/tm/', IF(t.url = '', t.id, t.url), '/', IF(l.url = '', l.id, l.url)) AS url, l.name
                  FROM rk_lines AS l
                  INNER JOIN rk_prod_lines AS pl ON pl.line_id = l.id AND pl.prod_id = $item_id
                  INNER JOIN tm AS t ON t.id = l.tm_id
                  LIMIT 1";

        $line = db::get_row($query);

        return array('article' => $article, 'line' => $line);
    }

    /**
     * Sets shipping fields
     */
    private static function _setShippingInfo()
    {
        if (self::$_orderID === null || (self::$_region !== null && self::$_shippingType !== null && self::$_shippingPrice !== null)) {
            return;
        }

        $shipping = DB::get_row('SELECT o.region_id, o.shipping_id, CASE o.shipping_id
                WHEN 1 THEN r.price_1
                WHEN 2 THEN r.price_2
                WHEN 3 THEN r.price_3
                WHEN 4 THEN r.price_4
                WHEN 6 THEN r.price_6
            END AS shipping_price
            FROM rk_orders AS o
            INNER JOIN shipping_regions AS r ON r.id = o.region_id
            WHERE o.id = ' . self::$_orderID . ' AND o.shipping_id != 0');

        if (!$shipping) {
            self::$_region = self::$_shippingType = self::$_shippingPrice = null;

            return;
        }

        self::$_region = (int)$shipping['region_id'];
        self::$_shippingType = (int)$shipping['shipping_id'];
        self::$_shippingPrice = $shipping['shipping_price'] !== null ? (int)$shipping['shipping_price'] : null;
    }

    public static function delete_order($orderID)
    {
        $orderID = (int)$orderID;

        DB::query('DELETE FROM rk_orders_items WHERE rk_order_id = ' . $orderID);
        DB::query('DELETE FROM rk_orders WHERE id = ' . $orderID);
    }

    /**
     * Sets order client_id, resets secure_cid
     *
     * @param int $orderID Order ID
     * @param int $clientID Client ID to set
     */
    public static function change_client_id($orderID, $clientID)
    {
        DB::query('UPDATE rk_orders SET client_id = ' . ((int)$clientID) . ', secure_cid = "" WHERE id = ' . ((int)$orderID));
    }

    /**
     * Copies items from order to new order. Existing in new order products won't be copied
     *
     * @param int $oldOrderID Order ID to copy from
     * @param int $newOrderID Order ID to copy to
     */
    public static function merge_items($oldOrderID, $newOrderID)
    {
        $oldOrderID = (int)$oldOrderID;
        $newOrderID = (int)$newOrderID;

        DB::query('INSERT INTO rk_orders_items (rk_order_id, product_id, quantity, price, special_price) SELECT ' . $newOrderID  . ', o.product_id, o.quantity, o.price, o.special_price FROM rk_orders_items AS o WHERE o.rk_order_id = ' . $oldOrderID . ' AND o.product_id NOT IN (SELECT n.product_id FROM rk_orders_items AS n WHERE n.rk_order_id = ' . $newOrderID . ')');
    }

    /**
     * Returns RK order ID by client ID
     *
     * @param int $clientID Client ID
     *
     * @return int
     */
    public static function get_order_id_by_client_id($clientID)
    {
        return (int)DB::get_field('id', 'SELECT o.id FROM rk_orders AS o WHERE o.client_id = ' . ((int)$clientID) . ' AND o.order_number = 0 LIMIT 1');
    }

    /**
     * Returns RK order ID by client ID
     *
     * @param string $hash Order hash
     *
     * @return int
     */
    public static function get_order_by_hash($hash)
    {
        return (int)DB::get_field('id', 'SELECT o.id FROM rk_orders AS o WHERE o.secure_cid= "' . DB::mysql_secure_string($hash) . '" AND o.order_number = 0 LIMIT 1');
    }

    /**
     * Creates new order
     *
     * @param int $clientID Client ID to bind order to
     *
     * @return array [`newOrderID`, `orderHash`]
     */
    public static function new_order($clientID = 0)
    {
        $hash = !$clientID ? Users::generate_hash() : '';

        $clientID = ((int)$clientID === 0 ? helpers::search_user_id() : $clientID);

        DB::query('INSERT INTO rk_orders (secure_cid, client_id, agent_id, referral_id, start_date) VALUES ("' . $hash . '", ' . ((int)$clientID) . ', '. (isset($_SESSION['agent_id']) ? $_SESSION['agent_id'] : Orders::AGENT_ID_RK) . ', ' . (!empty($_SESSION[referral::REFERRAL_KEY]) ? $_SESSION[referral::REFERRAL_KEY]['id'] : 0) . ', NOW()) ');

        return [DB::get_last_id(), $hash];
    }

    /**
     * Creates new order based on other
     *
     * @param int $orderID Order ID to clone from
     *
     * @return array [`New order ID`, `New hash`]
     */
    public static function clone_order($orderID)
    {
        list($newOrderID, $newHash) = self::new_order($_SESSION['user']['id']);

        self::merge_items($orderID, $newOrderID);

        return [
            $newOrderID,
            $newHash,
        ];
    }

    /**
     * Set promo discount by code for order
     *
     * @param int $promo_code_id Promo code ID
     * @param bool $admitad_promo Promo code if admitad
     *
     * @return bool
     */
    public static function set_promo_discount($promo_code_id, $admitad_promo = false)
    {
        $query = 'UPDATE rk_orders
                  SET promo_code_id = ' . ((int)$promo_code_id) . ', admitad_promo = ' . ($admitad_promo ? 1 : 0) . '
                  WHERE id = ' . self::orderID(true);

        return !!DB::query($query);
    }

    /**
     * Returns current cart promo code information
     *
     * @return array
     */
    public static function get_promo()
    {
        return self::orderID()
            && ($order = DB::get_row('SELECT ro.promo_code_id, ro.admitad_promo FROM rk_orders AS ro WHERE ro.id = ' . self::$_orderID))
            && $order['promo_code_id']
                ? DB::get_row('SELECT p.*, ' . ($order['admitad_promo'] ? 1 : 0) . ' AS admitad' . ($order['admitad_promo'] ? ', 0 AS min_sum, 0 AS certificate, 0 AS action_type_id, 0 AS min_action_products_count' : '') . ' FROM ' . ($order['admitad_promo'] ? 'rk_admitad_promo_codes' : 'rk_promo_codes') . ' AS p WHERE p.id = ' . $order['promo_code_id'])
                : null;
    }

    /**
     * Sets own referral discount value for current uncompleted order
     *
     * @param int $discount Discount value
     */
    public static function set_own_referral_discount($discount)
    {
        DB::query('UPDATE rk_orders SET own_referral_discount = ' . ((int)$discount) . ' WHERE id = ' . self::orderID(true));
    }

    /**
     * Updates agent ID for current order
     *
     * @param int $agentID Agent ID value
     */
    public static function setAgentID($agentID)
    {
        DB::query('UPDATE rk_orders SET agent_id = ' . ((int)$agentID) . ' WHERE id = ' . self::orderID(true));
    }
}
