<?php

/*
 * Make and update sitemap.xml
 */

class sitemap
{

    static function update_map()
    {
        //Удаление старых файлов

        array_map("unlink", glob(SITE_PATH . '/www/sitemap*.*'));

        //Часть файла
        $part = 1;

        //Абсолютный максимум
        $absMax = 40000;

        //Текущее динамическое значение максимума
        $max = $absMax;

        //  Date of change for non-lastmod info
        $const_date = '2014-06-02';

        $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');

        $file_begin = '<?xml version="1.0" encoding="UTF-8"?>
        <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        $file_begin_part = '<url><loc>' . DOMAIN_FULL . '/products/new</loc><lastmod>' . $const_date . '</lastmod><changefreq>weekly</changefreq><priority>0.8</priority></url>
        <url><loc>' . DOMAIN_FULL . '/products/sales</loc><lastmod>' . $const_date . '</lastmod><changefreq>weekly</changefreq><priority>0.8</priority></url>
        <url><loc>' . DOMAIN_FULL . '/products/hurry</loc><lastmod>' . $const_date . '</lastmod><changefreq>weekly</changefreq><priority>0.7</priority></url>
        <url><loc>' . DOMAIN_FULL . '/kosmetika</loc><lastmod>' . $const_date . '</lastmod><changefreq>weekly</changefreq><priority>0.6</priority></url>
        <url><loc>' . DOMAIN_FULL . '/otzyvy</loc><lastmod>' . $const_date . '</lastmod><changefreq>weekly</changefreq><priority>0.5</priority></url>
        <url><loc>' . DOMAIN_FULL . '/master_classes</loc><lastmod>' . $const_date . '</lastmod><changefreq>weekly</changefreq><priority>0.5</priority></url>
        <url><loc>' . DOMAIN_FULL . '/products/hit</loc><lastmod>' . $const_date . '</lastmod><changefreq>weekly</changefreq><priority>0.8</priority></url>
         <url><loc>' . DOMAIN_FULL . '/review</loc><lastmod>' . $const_date . '</lastmod><changefreq>weekly</changefreq><priority>0.8</priority></url>
        <url><loc>' . DOMAIN_FULL . '/question</loc><lastmod>' . $const_date . '</lastmod><changefreq>weekly</changefreq><priority>0.6</priority></url>
        <url><loc>' . DOMAIN_FULL . '/articles</loc><lastmod>' . $const_date . '</lastmod><changefreq>weekly</changefreq><priority>0.5</priority></url>
        <url><loc>' . DOMAIN_FULL . '/tm</loc><lastmod>' . $const_date . '</lastmod><changefreq>weekly</changefreq><priority>0.6</priority></url>
        <url><loc>' . DOMAIN_FULL . '/landing/kak-umenshit-objem-talii</loc><lastmod>2015-09-12</lastmod><changefreq>weekly</changefreq><priority>0.9</priority></url>
        <url><loc>' . DOMAIN_FULL . '/country</loc><lastmod>' . $const_date . '</lastmod><changefreq>weekly</changefreq><priority>0.7</priority></url>
        <url><loc>' . DOMAIN_FULL . '/sets</loc><lastmod>' . $const_date . '</lastmod><changefreq>weekly</changefreq><priority>0.7</priority></url>';

        $file_end = '</urlset>';

        //Вычитаем первые 14 ссылок
        $max -= 14;

        $data_array = array();
        $file_content = '';

        //Пишем базовые url
        @fwrite($file, $file_begin . $file_begin_part);

        //Пишем url страниц

        $data_array = Pages::get_list();

        if (count($data_array) > 0) {
            if (($max - count($data_array)) >= 0) {
                $max -= count($data_array);

                write_sitemap($file, 0, count($data_array), $data_array, 'page', 1, '', $type = 2);
            } else {
                //Количество url, превышающие максимум
                $steplength = count($data_array) - $max;

                write_sitemap($file, 0, count($data_array) - $steplength, $data_array, 'page', 1, '', $type = 2);
                @fwrite($file, $file_end);
                fclose($file);
                $part++;
                $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
                @fwrite($file, $file_begin);

                write_sitemap($file, count($data_array) - $steplength, count($data_array), $data_array, 'page', 1, '', $type = 2);
                $max = $absMax - $steplength;
            }
        }

        //Если достигнут максимум открываем новый файли заполняем максимум
        if ($max === 0) {
            @fwrite($file, $file_end);
            fclose($file);
            $part++;
            $max = $absMax;
            $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
            @fwrite($file, $file_begin);
        }

        //Пишем типы косметики

        $data_array = type_page::get_pages(true, false, null);

        if (count($data_array) > 0) {
            if (($max - count($data_array)) >= 0) {
                $max -= count($data_array);

                write_sitemap($file, 0, count($data_array), $data_array, 'kosmetika', 0.5, 'url', 1, 'daily');
            } else {
                //Количество url, превышающие максимум
                $steplength = count($data_array) - $max;

                write_sitemap($file, 0, count($data_array) - $steplength, $data_array, 'kosmetika', 0.5, 'url', 1, 'daily');
                @fwrite($file, $file_end);
                fclose($file);
                $part++;
                $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
                @fwrite($file, $file_begin);

                write_sitemap($file, count($data_array) - $steplength, count($data_array), $data_array, 'kosmetika', 0.5, 'url', 1, 'daily');
                $max = $absMax - $steplength;
            }
        }

        //Если достигнут максимум открываем новый файли заполняем максимум
        if ($max === 0) {
            @fwrite($file, $file_end);
            fclose($file);
            $part++;
            $max = $absMax;
            $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
            @fwrite($file, $file_begin);
        }

        //Пишем отзывы для типов косметики

        $data_array = type_page::get_pages(true, false, null);

        if (count($data_array) > 0) {
            if (($max - count($data_array)) >= 0) {
                $max -= count($data_array);

                write_sitemap($file, 0, count($data_array), $data_array, 'otzyvy/kosmetika', 0.6, 'url', 1, 'daily');
            } else {
                //Количество url, превышающие максимум
                $steplength = count($data_array) - $max;

                write_sitemap($file, 0, count($data_array) - $steplength, $data_array, 'otzyvy/kosmetika', 0.6, 'url', 1, 'daily');
                @fwrite($file, $file_end);
                fclose($file);
                $part++;
                $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
                @fwrite($file, $file_begin);

                write_sitemap($file, count($data_array) - $steplength, count($data_array), $data_array, 'otzyvy/kosmetika', 0.6, 'url', 1, 'daily');
                $max = $absMax - $steplength;
            }
        }

        //Если достигнут максимум открываем новый файли заполняем максимум
        if ($max === 0) {
            @fwrite($file, $file_end);
            fclose($file);
            $part++;
            $max = $absMax;
            $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
            @fwrite($file, $file_begin);
        }


        $prod_cat = ['rk_prod_cat', 'rk_prod_cat_lvl_1', 'rk_prod_cat_lvl_2'];

        foreach ($prod_cat as $table) {
            //Пишем отзывы для категорий
            $data_array = category($table);

            if (count($data_array) > 0) {
                if (($max - count($data_array)) >= 0) {
                    $max -= count($data_array);

                    write_sitemap($file, 0, count($data_array), $data_array, 'otzyvy/category', 0.6, 'url');
                } else {
                    //Количество url, превышающие максимум
                    $steplength = count($data_array) - $max;

                    write_sitemap($file, 0, count($data_array) - $steplength, $data_array, 'otzyvy/category', 0.6, 'url');
                    @fwrite($file, $file_end);
                    fclose($file);
                    $part++;
                    $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
                    @fwrite($file, $file_begin);

                    write_sitemap($file, count($data_array) - $steplength, count($data_array), $data_array, 'otzyvy/category', 0.6, 'url');
                    $max = $absMax - $steplength;
                }
            }

            //Пишем отзывы для категорий и торговых марок
            $data_array = category_tm($table);

            if (count($data_array) > 0) {
                if (($max - count($data_array)) >= 0) {
                    $max -= count($data_array);

                    write_sitemap($file, 0, count($data_array), $data_array, 'otzyvy/category-tm', 0.6, 'url', 1, 'weekly', 'tm_url');
                } else {
                    //Количество url, превышающие максимум
                    $steplength = count($data_array) - $max;

                    write_sitemap($file, 0, count($data_array) - $steplength, $data_array, 'otzyvy/category-tm', 0.6, 'url', 1, 'weekly', 'tm_url');
                    @fwrite($file, $file_end);
                    fclose($file);
                    $part++;
                    $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
                    @fwrite($file, $file_begin);

                    write_sitemap($file, count($data_array) - $steplength, count($data_array), $data_array, 'otzyvy/category-tm', 0.6, 'url', 1, 'weekly', 'tm_url');
                    $max = $absMax - $steplength;
                }
            }

        }

        //Если достигнут максимум открываем новый файли заполняем максимум
        if ($max === 0) {
            @fwrite($file, $file_end);
            fclose($file);
            $part++;
            $max = $absMax;
            $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
            @fwrite($file, $file_begin);
        }

        //Пишем url отзывов о торговых марках

        $data_array = tm::get_list(false, false, false, true);

        if (count($data_array) > 0) {

            for ($i = 0; $i < count($data_array); $i++) {

                $file_content = '<url><loc>' . DOMAIN_FULL . '/otzyvy/tm/' . $data_array[$i]['url'] . "</loc><lastmod>{$data_array[$i]['lastmod']}</lastmod><changefreq>weekly</changefreq><priority>0.6</priority></url>\n";
                @fwrite($file, $file_content);
                $max -= 1;
                if ($max === 0) {
                    @fwrite($file, $file_end);
                    fclose($file);
                    $part++;
                    $max = $absMax;
                    $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
                    @fwrite($file, $file_begin);
                }

                $lines_in_tm = lines::get_list($data_array[$i]['id']);

                if (count($lines_in_tm) > 0) {

                    if (($max - count($lines_in_tm)) >= 0) {
                        $max -= count($lines_in_tm);

                        write_sitemap($file, 0, count($lines_in_tm), $lines_in_tm, 'tm/' . $data_array[$i]['url'], 0.6, 'url');
                    } else {
                        //Количество url, превышающие максимум
                        $steplength = count($lines_in_tm) - $max;

                        write_sitemap($file, 0, count($lines_in_tm) - $steplength, $lines_in_tm, 'tm/' . $data_array[$i]['url'], 0.6, 'url');
                        @fwrite($file, $file_end);
                        fclose($file);
                        $part++;
                        $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
                        @fwrite($file, $file_begin);

                        write_sitemap($file, count($lines_in_tm) - $steplength, count($lines_in_tm), $lines_in_tm, 'otzyvy/tm/' . $data_array[$i]['url'], 0.6, 'url');
                        $max = $absMax - $steplength;
                    }
                }
            }
        }

        //Если достигнут максимум открываем новый файли заполняем максимум
        if ($max === 0) {
            @fwrite($file, $file_end);
            fclose($file);
            $part++;
            $max = $absMax;
            $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
            @fwrite($file, $file_begin);
        }

        //Пишем url торговых марок

        $data_array = tm::get_list(false, false, false, true);

        if (count($data_array) > 0) {

            for ($i = 0; $i < count($data_array); $i++) {
                $file_content = '<url><loc>' . DOMAIN_FULL . '/tm/' . $data_array[$i]['url'] . "</loc><lastmod>{$data_array[$i]['lastmod']}</lastmod><changefreq>weekly</changefreq><priority>0.6</priority></url>\n";
                @fwrite($file, $file_content);
                $max -= 1;
                if ($max === 0) {
                    @fwrite($file, $file_end);
                    fclose($file);
                    $part++;
                    $max = $absMax;
                    $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
                    @fwrite($file, $file_begin);
                }

                $lines_in_tm = lines::get_list($data_array[$i]['id']);

                if (count($lines_in_tm) > 0) {
                    if (($max - count($lines_in_tm)) >= 0) {
                        $max -= count($lines_in_tm);

                        write_sitemap($file, 0, count($lines_in_tm), $lines_in_tm, 'tm/' . $data_array[$i]['url'], 0.6, 'url');
                    } else {
                        //Количество url, превышающие максимум
                        $steplength = count($lines_in_tm) - $max;

                        write_sitemap($file, 0, count($lines_in_tm) - $steplength, $lines_in_tm, 'tm/' . $data_array[$i]['url'], 0.6, 'url');
                        @fwrite($file, $file_end);
                        fclose($file);
                        $part++;
                        $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
                        @fwrite($file, $file_begin);

                        write_sitemap($file, count($lines_in_tm) - $steplength, count($lines_in_tm), $lines_in_tm, 'tm/' . $data_array[$i]['url'], 0.6, 'url');
                        $max = $absMax - $steplength;
                    }
                }
            }
        }

        //Если достигнут максимум открываем новый файли заполняем максимум
        if ($max === 0) {
            @fwrite($file, $file_end);
            fclose($file);
            $part++;
            $max = $absMax;
            $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
            @fwrite($file, $file_begin);
        }

        //Пишем url торговых категорий

        $data_array = categories::get_list();

        if (count($data_array) > 0) {
            if (($max - count($data_array)) >= 0) {
                $max -= count($data_array);

                write_sitemap($file, 0, count($data_array), $data_array, 'category', 0.8, '_full_url', 1, 'daily');
            } else {
                //Количество url, превышающие максимум
                $steplength = count($data_array) - $max;

                write_sitemap($file, 0, count($data_array) - $steplength, $data_array, 'category', 0.8, '_full_url', 1, 'daily');
                @fwrite($file, $file_end);
                fclose($file);
                $part++;
                $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
                @fwrite($file, $file_begin);

                write_sitemap($file, count($data_array) - $steplength, count($data_array), $data_array, 'category', 0.8, '_full_url', 1, 'daily');
                $max = $absMax - $steplength;
            }
        }

        //Если достигнут максимум открываем новый файли заполняем максимум

        if ($max === 0) {
            @fwrite($file, $file_end);
            fclose($file);
            $part++;
            $max = $absMax;
            $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
            @fwrite($file, $file_begin);
        }


        //*******************************
        //Пишем url стран

        $data_array = country::get_countries();

        if (count($data_array) > 0) {
            if (($max - count($data_array)) >= 0) {
                $max -= count($data_array);

                write_sitemap($file, 0, count($data_array), $data_array, 'country', 0.7, 'url');
            } else {
                //Количество url, превышающие максимум
                $steplength = count($data_array) - $max;

                write_sitemap($file, 0, count($data_array) - $steplength, $data_array, 'country', 0.7, 'url');
                @fwrite($file, $file_end);
                fclose($file);
                $part++;
                $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
                @fwrite($file, $file_begin);

                write_sitemap($file, count($data_array) - $steplength, count($data_array), $data_array, 'country', 0.7, 'url');
                $max = $absMax - $steplength;
            }
        }

        //Если достигнут максимум открываем новый файли заполняем максимум

        if ($max === 0) {
            @fwrite($file, $file_end);
            fclose($file);
            $part++;
            $max = $absMax;
            $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
            @fwrite($file, $file_begin);
        }
        //*******************************

        //Пишем url торговых продуктов

        $data_array = products::get_list();
        if (count($data_array) > 0) {
            if (($max - count($data_array)) >= 0) {
                $max -= count($data_array);

                write_sitemap($file, 0, count($data_array), $data_array, 'product', 0.8, 'url');
            } else {

                write_sitemap($file, 0, (int)$max, $data_array, 'product', 0.8, 'url');

                @fwrite($file, $file_end);
                fclose($file);
                $part++;
                $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
                @fwrite($file, $file_begin);
                $start = (int)$max;

                $steps = (count($data_array) - (int)$max) / $absMax;

                for ($j = 1; $j <= (int)$steps; $j++) {

                    $stop = $start + $absMax * $j;

                    write_sitemap($file, $start, $stop, $data_array, 'product', 0.8, 'url');
                    $start = $stop;
                    @fwrite($file, $file_end);
                    fclose($file);
                    $part++;
                    $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
                    @fwrite($file, $file_begin);
                }

                write_sitemap($file, $start, count($data_array), $data_array, 'product', 0.8, 'url');
                $max = $absMax - (count($data_array) - $start);
            }
        }

        //Если достигнут максимум открываем новый файли заполняем максимум

        if ($max === 0) {
            @fwrite($file, $file_end);
            fclose($file);
            $part++;
            $max = $absMax;
            $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
            @fwrite($file, $file_begin);
        }

        //Пишем url торговых артиклов

        $data_array = Articles::get_list();
        if (count($data_array) > 0) {
            if (($max - count($data_array)) >= 0) {
                $max -= count($data_array);

                write_sitemap($file, 0, count($data_array), $data_array, 'articles', 0.5, 'url');
            } else {
                //Количество url, превышающие максимум
                $steplength = count($data_array) - $max;

                write_sitemap($file, 0, count($data_array) - $steplength, $data_array, 'articles', 0.5, 'url');
                @fwrite($file, $file_end);
                fclose($file);
                $part++;
                $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
                @fwrite($file, $file_begin);

                write_sitemap($file, count($data_array) - $steplength, count($data_array), $data_array, 'articles', 0.5, 'url');
                $max = $absMax - $steplength;
            }
        }

        //Если достигнут максимум открываем новый файли заполняем максимум

        if ($max === 0) {
            @fwrite($file, $file_end);
            fclose($file);
            $part++;
            $max = $absMax;
            $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
            @fwrite($file, $file_begin);
        }

        //Пишем url сетов

        $data_array = sets::get_list();

        if (count($data_array) > 0) {
            if (($max - count($data_array)) >= 0) {
                $max -= count($data_array);

                write_sitemap($file, 0, count($data_array), $data_array, 'sets', 0.8, 'url');
            } else {
                //Количество url, превышающие максимум
                $steplength = count($data_array) - $max;

                write_sitemap($file, 0, count($data_array) - $steplength, $data_array, 'sets', 0.8, 'url');
                @fwrite($file, $file_end);
                fclose($file);
                $part++;
                $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
                @fwrite($file, $file_begin);

                write_sitemap($file, count($data_array) - $steplength, count($data_array), $data_array, 'sets', 0.8, 'url');
                $max = $absMax - $steplength;
            }
        }

        //Если достигнут максимум открываем новый файли заполняем максимум

        if ($max === 0) {
            @fwrite($file, $file_end);
            fclose($file);
            $part++;
            $max = $absMax;
            $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
            @fwrite($file, $file_begin);
        }

        //Пишем url отзывов

        $data_array = prod_review::sm_list();
        if (count($data_array) > 0) {
            if (($max - count($data_array)) >= 0) {
                $max -= count($data_array);

                write_sitemap($file, 0, count($data_array), $data_array, 'prod_review', 0.7, 'url');
            } else {
                //Количество url, превышающие максимум
                $steplength = count($data_array) - $max;

                write_sitemap($file, 0, count($data_array) - $steplength, $data_array, 'prod_review', 0.7, 'url');
                @fwrite($file, $file_end);
                fclose($file);
                $part++;
                $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
                @fwrite($file, $file_begin);

                write_sitemap($file, count($data_array) - $steplength, count($data_array), $data_array, 'prod_review', 0.7, 'url');
                $max = $absMax - $steplength;
            }
        }

        //Если достигнут максимум открываем новый файли заполняем максимум

        if ($max === 0) {
            @fwrite($file, $file_end);
            fclose($file);
            $part++;
            $max = $absMax;
            $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
            @fwrite($file, $file_begin);
        }

        //Пишем url вопросов

        $question_quantity = ceil(count(questions_to_cosmetolog::get_questions()) / 15);


        if ($question_quantity > 1) {

            if (($max - ($question_quantity - 1)) >= 0) {
                $max -= ($question_quantity - 1);

                write_sitemap($file, 2, $question_quantity, [], 'question', 0.6, '', 3);
            } else {
                //Количество url, превышающие максимум
                $steplength = ($question_quantity - 1) - $max;

                write_sitemap($file, 2, ($question_quantity - 1) - $steplength, [], 'question', 0.6, '', 3);
                @fwrite($file, $file_end);
                fclose($file);
                $part++;
                $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
                @fwrite($file, $file_begin);

                write_sitemap($file, ($question_quantity - 1) - $steplength, $question_quantity, [], 'question', 0.6, '', 3);
                $max = $absMax - $steplength;
            }
        }

        //Если достигнут максимум открываем новый файли заполняем максимум

        if ($max === 0) {
            @fwrite($file, $file_end);
            fclose($file);
            $part++;
            $max = $absMax;
            $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
            @fwrite($file, $file_begin);
        }

        //Пишем url отзывов на сайт

        $review_quantity = ceil(count(review::get_list(1, 0)) / 15);

        if ($review_quantity > 1) {
            if (($max - ($review_quantity - 1)) >= 0) {
                $max -= ($review_quantity - 1);

                write_sitemap($file, 2, $review_quantity, [], 'review', 0.6, '', 3);
            } else {
                //Количество url, превышающие максимум
                $steplength = ($review_quantity - 1) - $max;

                write_sitemap($file, 2, ($review_quantity - 1) - $steplength, [], 'review', 0.6, '', 3);
                @fwrite($file, $file_end);
                fclose($file);
                $part++;
                $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
                @fwrite($file, $file_begin);

                write_sitemap($file, ($review_quantity - 1) - $steplength, $review_quantity, [], 'review', 0.6, '', 3);
                $max = $absMax - $steplength;
            }
        }

        //Если достигнут максимум открываем новый файли заполняем максимум

        if ($max === 0) {
            @fwrite($file, $file_end);
            fclose($file);
            $part++;
            $max = $absMax;
            $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
            @fwrite($file, $file_begin);
        }

        //Пишем url связок торговых марок с категориями

        $data_array = tm_in_cat::get_urls();

        if (count($data_array) > 0) {

            if (((int)$max - count($data_array)) >= 0) {

                write_sitemap($file, 0, count($data_array), $data_array, 'category-tm', 0.9, '', 4);
            } else {

                write_sitemap($file, 0, (int)$max, $data_array, 'category-tm', 0.9, '', 4);

                @fwrite($file, $file_end);
                fclose($file);
                $part++;
                $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
                @fwrite($file, $file_begin);
                $start = (int)$max;

                $steps = (count($data_array) - (int)$max) / $absMax;

                for ($j = 1; $j <= (int)$steps; $j++) {

                    $stop = $start + $absMax * $j;

                    write_sitemap($file, $start, $stop, $data_array, 'category-tm', 0.9, '', 4);
                    $start = $stop;
                    @fwrite($file, $file_end);
                    fclose($file);
                    $part++;
                    $file = @fopen(SITE_PATH . '/www/sitemap-' . $part . '.xml', 'w+b');
                    @fwrite($file, $file_begin);
                }

                write_sitemap($file, $start, count($data_array), $data_array, 'category-tm', 0.9, '', 4);
            }
        }

        unset($data_array);
        @fwrite($file, $file_end);
        fclose($file);

        //Надо ли сжимать файлы?

        $sizeFlag = false;
        for ($i = 1; $i <= $part; $i++) {
            if (filesize(SITE_PATH . '/www/sitemap-' . $i . '.xml') >= 10000) {
                $sizeFlag = true;
            }
        }

        //Сжимаем, если надо

        if ($sizeFlag) {
            for ($i = 1; $i <= $part; $i++) {
                archivation(SITE_PATH . '/www/sitemap-' . $i . '.xml');
            }
        }

        //Формирование файла индекса

        $file = @fopen(SITE_PATH . '/www/sitemap.xml', 'w+b');

        $file_begin = '<?xml version="1.0" encoding="UTF-8"?>
        <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . "\n";
        @fwrite($file, $file_begin);
        $file_end = '</sitemapindex>';
        $arh = $sizeFlag ? '.gz' : '';
        for ($i = 1; $i <= $part; $i++) {
            $file_content = '<sitemap><loc>' . DOMAIN_FULL . '/sitemap-' . $i . '.xml' . $arh . '</loc><lastmod>' . date("Y-m-d") . "</lastmod></sitemap>\n";
            @fwrite($file, $file_content);
        }
        @fwrite($file, $file_end);
        fclose($file);

        //Удаление незаархивированных файлов

        if ($sizeFlag) {
            array_map("unlink", glob(SITE_PATH . '/www/sitemap-*.xml'));
        }
    }

    /**
     * Prepare  array for output for sitemap page
     * @param  array  $array     Input array
     * @param  array  $find_keys keys for find
     * @param  array  $set_keys  keys for change
     * @param  array  $pre_set   Pre set concat for values
     * @return array             array for output
     */
    static function prepare_array($array, $find_keys = array(0 => 'url', 1 => 'name'), $set_keys = array(0 => 'url', 1 => 'name'), $pre_set = array(0 => '', 1 => '')) {

        $return = FALSE;

        for ($i=0; $i < count($array); $i++) { 
           
            for ($j=0; $j < count($find_keys); $j++) { 

                $return[$i]["$set_keys[$j]"] = $pre_set[$j] . $array[$i]["$find_keys[$j]"];
            }

        }

        return $return;
    }
}

/**Write sitemap in file
 * @param resource $file
 * @param int $begin
 * @param int $end
 * @param [] $data_array
 * @param string $path
 * @param float $priority
 * @param string $key
 * @param int $type
 * @param string $changefreq
 */
function write_sitemap($file, $begin, $end, $data_array, $path, $priority, $key = '', $type = 1, $changefreq = 'weekly', $url_tm = '')
{
    if ($type === 1) {
        $priority = str_replace(',', '.', (string)$priority);
        for ($i = $begin; $i < $end; $i++) {
            $url_tms = $url_tm !== '' ? '/' . $data_array[$i][$url_tm] : '';
            $lastmod = array_key_exists('lastmod', $data_array[$i]) ? $data_array[$i]['lastmod'] : date("Y-m-d");
            $file_content = '<url><loc>' . DOMAIN_FULL . '/' . $path . '/' . $data_array[$i][$key] . $url_tms . "</loc><lastmod>{$lastmod}</lastmod><changefreq>{$changefreq}</changefreq><priority>{$priority}</priority></url>\n";
            @fwrite($file, $file_content);

        }

    } elseif ($type === 2) {

        for ($i = $begin; $i < $end; $i++) {

            $name = $data_array[$i]['name'];

            if ($name == 'index') {
                $file_content = "<url><loc>" . DOMAIN_FULL . "</loc><lastmod>{$data_array[$i]['lastmod']}</lastmod><changefreq>{$changefreq}</changefreq><priority>1</priority></url>\n";
            } else {
                $file_content = '<url><loc>' . DOMAIN_FULL . '/' . $path . '/' . $name . "</loc><lastmod>{$data_array[$i]['lastmod']}</lastmod><changefreq>{$changefreq}</changefreq><priority>0.6</priority></url>\n";
            }

            @fwrite($file, $file_content);
        }
    } elseif ($type === 3) {
        $priority = str_replace(',', '.', (string)$priority);
        for ($i = 2; $i <= $end; $i++) {
            $file_content = '<url><loc>' . DOMAIN_FULL . '/' . $path . '/' . $i . "</loc><changefreq>{$changefreq}</changefreq><priority>{$priority}</priority></url>\n";
            @fwrite($file, $file_content);
        }
    } elseif ($type === 4) {
        $priority = str_replace(',', '.', (string)$priority);
        for ($i = $begin; $i < $end; $i++) {

            $file_content = '<url><loc>' . DOMAIN_FULL . '/' . $path . '/' . $data_array[$i]['cat_url'] . '/' . $data_array[$i]['tm_url'] . "</loc><lastmod>{$data_array[$i]['lastmod']}</lastmod><changefreq>{$changefreq}</changefreq><priority>{$priority}</priority></url>\n";
            @fwrite($file, $file_content);

        }
    }

}


/**
 * @param string $fileName
 */
function archivation($fileName)
{
    $gzfile = $fileName . ".gz";

    $fp = gzopen($gzfile, 'w9'); // w9 - сильное сжатие

    gzwrite($fp, file_get_contents($fileName));

    gzclose($fp);
}

/**
 * Get category urls
 * @param string $table
 * @return array
 */
function category($table)
{

    $query = "SELECT rc.cat_id id, rcat._full_url url
      FROM rk_prod_review pr
      LEFT JOIN {$table} rc ON pr.prod_id = rc.prod_id
      LEFT JOIN rk_categories rcat ON rcat.id = rc.cat_id
      WHERE pr.visible =1
      AND pr.negative = 0
      GROUP BY rc.cat_id";

    return DB::get_rows($query);
}


/**Get category-tm urls
 * @param string $table
 * @return array
 */
function category_tm($table)
{

    $query = "SELECT rc.cat_id id, rcat._full_url url, tm.url tm_url
      FROM rk_prod_review pr
      LEFT JOIN {$table} rc ON pr.prod_id = rc.prod_id
      LEFT JOIN rk_categories rcat ON rcat.id = rc.cat_id
      LEFT JOIN products p ON p.id = pr.prod_id
      LEFT JOIN tm ON p.tm_id = tm.id
      WHERE pr.visible =1
      AND pr.negative = 0
      AND rcat.is_tag = 0
      GROUP BY rcat._full_url,tm.url";

    return DB::get_rows($query);
}