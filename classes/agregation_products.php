<?php
/**
 * Individual products list
 */

class agregation_products
{

    /**
     * Select personal products list
     * @param int   $id     client ID
     * @param int   $num    count products in list
     * @param string    $email client email
     * @param string    $method method type
     * @param string    $statuses list completed status
     * @return array
     */
    static function set_personal_recommendation_product_list($id, $num, $email, $method, $statuses)
    {
        $product_list=[];
        $order_ids_list=[];
        $list = 0;
        switch ($method){
            case 'orders':
                $order_ids = db::get_rows("SELECT order_number FROM orders WHERE client_id = {$id} AND status_id IN({$statuses})");
                foreach ($order_ids as $order_id){
                    $order_ids_list[]=$order_id['order_number'];
                }
                if (count($order_ids_list)>0){
                    $list = implode(',', $order_ids_list);
                }
                $query = "SELECT p.id
FROM products AS p
LEFT JOIN orders_items AS oi ON p.id = oi.product_id
WHERE p.available = 1 AND p.active = 1 AND p.visible = 1 AND oi.order_number IN({$list})
GROUP BY p.id ORDER BY p.price DESC LIMIT $num";
                break;
            case 'rk_orders_items':
                $order_ids = db::get_rows("SELECT id FROM rk_orders WHERE client_id = {$id} AND order_number = 0");
                foreach ($order_ids as $order_id){
                    $order_ids_list[]=$order_id['id'];
                }
                if (count($order_ids_list)>0){
                    $list = implode(',', $order_ids_list);
                }
                $query = "SELECT p.id
FROM products AS p
LEFT JOIN rk_orders_items AS roi ON p.id = roi.product_id
WHERE p.available = 1 AND p.active = 1 AND roi.rk_order_id IN({$list})
GROUP BY p.id ORDER BY p.price DESC LIMIT $num";
                break;
            case 'credentials':
                $order_ids = db::get_rows("SELECT order_id FROM rk_credentials WHERE email = '{$email}'");
                foreach ($order_ids as $order_id){
                    $order_ids_list[]=$order_id['order_id'];
                }
                if (count($order_ids_list)>0){
                    $list = implode(',', $order_ids_list);
                }
                $query = "SELECT p.id
FROM products AS p
LEFT JOIN rk_orders_items AS roi ON p.id = roi.product_id
WHERE p.available = 1 AND p.active = 1 AND roi.rk_order_id IN({$list})
GROUP BY p.id ORDER BY p.price DESC LIMIT {$num}";
                break;
        }
        //echo $query."\n";
        $products = db::get_rows($query);
        if(count($products)) {
            foreach ($products as $product){
                $products_list[] = $product['id'];

            }

            $id_list = implode(',', $products_list);

            $query = "SELECT product_id FROM cdb_product_ballance WHERE product_id IN ($id_list) ORDER BY sold DESC";

            $products = db::get_rows($query);

            foreach ($products as $product){
                $product_list[$product['product_id']] = $num;
                $num--;
            }
        }

        return count($product_list)? $product_list:[];
    }


    /**
     * Replace product list in __personal_choices
     * @param int $id client ID
     * @param array $products_list
     */
    static function replace_personal_choices_product_list($id, $products_list){


        $products_list = json_encode($products_list);
        db::query("INSERT INTO __personal_choices (id, products_list, date) VALUES ({$id}, '{$products_list}', NOW()) ON DUPLICATE KEY UPDATE products_list='{$products_list}', date=NOW()");

    }


    /**
     *  Get personal choices
     * @param int $id clients ID
     * @param bool $flag
     * @return array|mixed
     */
    static function get_personal_choice($id,$flag=false){
        $query = "SELECT products_list FROM __personal_choices WHERE id = $id";
        if($flag){
            $choices_products_list = db::get_row($query);
            return json_decode($choices_products_list['products_list']);
        }else {
            return db::get_row($query);
        }
    }


    /**
     * Splice personal recommendation
     * @param int $id clients ID
     */
    static function splice_personal_recommendation($id){

        $choices_products_list = self::get_personal_choice($id, true);

        if(count($choices_products_list)>0) {

            $num = 0;
            $not_in_id = '';
            $neighbours_products_list = [];
            foreach ($choices_products_list as $key => $product) {

                $limit = 20 - count($neighbours_products_list);

                $query = "SELECT neighbour_id FROM __cart_neighbours WHERE product_id = {$key} AND product_id NOT IN('{$not_in_id}') GROUP BY neighbour_id ORDER BY cnt DESC LIMIT {$limit}";

                $neighbours_list = db::get_rows($query);

                $ids = [];
                if (count($neighbours_list)>0) {

                    foreach ($neighbours_list as $neighbour) {
                        $neighbours_products_list[$neighbour['neighbour_id']] = 20-$num;
                        $num++;
                        $ids[] = $neighbour['neighbour_id'];
                    }

                }
                $not_in_id .= implode(',', $ids);

                if ($num >= 20) {
                    break;
                }
            }

            if (count($neighbours_products_list) < 20) {
                $num = 20 - count($neighbours_products_list);
                foreach ($choices_products_list as $key => $product) {
                    $neighbours_products_list[$key] = $num;
                    $num--;
                    if ($num < 1) {
                        break;
                    }
                }
            }

            if (count($neighbours_products_list) > 0) {

                $products_list = json_encode($neighbours_products_list);

                db::query("INSERT INTO __personal_recommendation (id, products_list, date) VALUES ({$id}, '{$products_list}', NOW()) ON DUPLICATE KEY UPDATE products_list='{$products_list}', date=NOW()");
            }
        }
    }

    /**
     * Get personal recommendation/choices product list
     * @param int $id clients ID
     * @param int $type
     * @param int $num
     * @param string $sort
     * @return array|mixed
     */
    static function get_personal_product_list($id, $type = 1, $num = 0, $sort=''){
        $id_list=[];

        $query = ($type==1? "SELECT * FROM __personal_recommendation WHERE id={$id}":"SELECT * FROM __personal_choices WHERE id={$id}");
        $result = db::get_row($query);

        if($result) {
            $products_list = (array)json_decode($result['products_list']);

            if (count($products_list) > 0) {

                foreach ($products_list as $key=>$product){
                    $id_list[] = $key;
                }

                $id_list = helpers::random_list($id_list,$num);
                
                return $id_list;
            }

        }else{

            $id_list = agregation_products::get_random_product_list($num);
        }

        return $id_list;
    }

    /**
     * Get random recommendation/choices product list
     * @param int $num
     * @return array
     */
    static function get_random_product_list($num = 4){

        //select hits

        $product_ids = db::get_rows("SELECT p.id FROM products AS p INNER JOIN tm ON tm.id = p.tm_id WHERE p.hit = 1 AND p.main_id = 0 AND p.visible = 1 AND (p.active = 1 OR p.price = 0 AND p.new = 1) AND p.for_proff = 0".(TM_FOR_PROF && 'hit' != 'hurry' ? ' AND tm.prof = 1 ' : ''));

        $select_ids = [];

        if(count($product_ids)>0){
            $ids=[];
            foreach ($product_ids as $product_id){
                $ids[] = $product_id['id'];

            }
            $select_ids = helpers::random_list($ids,$num);
        }

        return $select_ids;
    }
}