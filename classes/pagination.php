<?php

class Pagination
{
    /**
     * @var int Number of page numbers to show on each side of current page
     */
    public static $sidePages = 5;

    /**
     * @var string Page base url (Without pagination params)
     */
    private static $_baseUrl = '';

    /**
     * @var string Pagination url
     */
    private static $_url = '?';

    /**
     * @var array Pagination values
     */
    private static $_values = null;

    /**
     * @var array Available on page values
     */
    private static $_onPages = [
        40,
        80,
        160,
    ];

    /**
     * @var bool Whether manual on page count was provided
     */
    private static $_manualOnPage = false;

    /**
     * Returns page base url
     *
     * @return string
     */
    public static function getBaseUrl()
    {
        return self::$_baseUrl;
    }

    /**
     * Returns url (Based on self::$baseUrl) to be used as getUrl() . 'p=2'
     *
     * @return string
     */
    public static function getUrl()
    {
        return self::$_url;
    }

    /**
     * Returns pagination values
     *
     * @param string $key Return single value with given key
     *
     * @return array|string|int
     */
    public static function getValues($key = null)
    {
        return $key === null ? self::$_values : (isset(self::$_values[$key]) ? self::$_values[$key] : null);
    }

    /**
     * Returns formatted count text
     * 
     * @param string $variants Variants text used in count_morphology()
     * 
     * @return string
     */
    public static function getCount($variants = null)
    {
        return isset(self::$_values['count']) ? ($variants !== null ? count_morphology(self::$_values['count'] . ' ' . $variants) : Product::count_string(self::$_values['count'])) : '';
    }

    /**
     * Returns meta tags for pagination
     *
     * @param string $domain Site domain value
     *
     * @return string
     */
    public static function meta($domain = '')
    {
        if (!self::$_values || self::$_values['pages'] < 2) {
            return '';
        }

        return "\n" . (self::$_values['page'] > 1 ? '<link rel="prev" href="' . $domain. (self::$_values['page'] > 2 ? self::$_url . 'p=' . (self::$_values['page'] - 1) : self::$_baseUrl) . '">' : '') .
            "\n" . '<meta name="robots" content="' . (self::$_values['page'] === 1 ? 'index' : 'noindex') . ', follow"/>' ."\n" .
            (self::$_values['page'] < self::$_values['pages'] ? '<link rel="next" href="' . $domain . self::$_url . 'p=' . (self::$_values['page'] + 1) . '">' : '');
    }

    /**
     * Sets page base url (Without pagination params)
     *
     * @param string $url Url
     *
     * @throws Exception On second call
     */
    public static function setBaseUrl($url)
    {
        static $set;

        if ($set) {
            throw new Exception('Url was already set');
        }

        self::$_baseUrl = $url;

        $set = true;

        self::$_url = $url . (strpos($url, '?') !== false ? '&' : '?');
    }

    /**
     * Sets template variable 'Pagination' with value of pagination settings. Result equals self::$_values + `array(
     *      'baseUrl' =>        Page base url,
     *      'url' =>            Pagination url,
     *      'start' =>          Page numbers starting page,
     *      'end' =>            Page numbers ending page,
     *      'showAll' =>        Show all items, no pagination
     *      'showPrev' =>       Show link to previous page
     *      'showNext' =>       Show link to next page
     *      'showFirst' =>      Show link to first page
     *      'showLast' =>       Show link to last page
     *      'showPrevDots' =>   Show dots after link to first page
     *      'showNextDots' =>   Show dots before link to last page
     *      'onPage' =>         Selected on page value
     *      'onPages' =>        Array of available on page values
     * )`
     *
     * @throws Exception If no values were set
     */
    public static function setTemplateSettings()
    {
        if (!self::$_values) {
            throw new Exception('Values not set');
        }

        $start = self::$_values['page'] - self::$sidePages;

        $end = self::$_values['page'] + self::$sidePages;

        if ($start < 1) {
            $start = 1;
        }

        if ($end > self::$_values['pages']) {
            $end = self::$_values['pages'];
        }

        Template::set('Pagination', array_merge(self::$_values, array(
            'baseUrl' => self::getBaseUrl(),
            'url' => self::getUrl(),
            'start' => $start,
            'end' => $end,
            'count' => self::$_values['count'],
            'showAll' => self::$_values['showAll'],
            'showPrev' => self::$_values['page'] > 1,
            'showNext' => self::$_values['page'] < self::$_values['pages'],
            'showFirst' => self::$_values['page'] > self::$sidePages + 1,
            'showLast' => self::$_values['page'] < self::$_values['pages'] - self::$sidePages,
            'showPrevDots' => self::$_values['page'] > self::$sidePages + 2,
            'showNextDots' => self::$_values['page'] < self::$_values['pages'] - self::$sidePages - 1,
            'onPage' => self::$_values['onPage'],
            'onPages' => !self::$_manualOnPage ? self::$_onPages : null,
        )));
    }

    /**
     * Sets pagination values and returns them
     *
     * @param int $count All rows count
     * @param int $onPage Count of rows on page. If not passed - $_GET parameter will be checked
     *
     * @return array ['count' => {$count value}, 'pages' => {Pages count}, 'page' => {Current page}, 'onPage' => {$onPage value}, 'showAll' => {Whether showing all, not per page}]   //Current page starts from 1
     *
     * @throws Exception On second call
     */
    public static function setValues($count, $onPage = null)
    {
        if (self::$_values) {
            throw new Exception('Pagination already initialized');
        }

        $showAll = isset($_GET['showAll']);

        self::$_manualOnPage = !!$onPage;

        $onPage = $onPage !== null ? $onPage : (isset($_GET['onp']) && in_array((int)$_GET['onp'], self::$_onPages, true) ? (int)$_GET['onp'] : self::$_onPages[0]);

        $pages = ceil($count / $onPage);

        static $page;

        if ($page === null) {
            $page = !$showAll && (isset($_GET['p']) && $_GET['p'] > 0 && $_GET['p'] <= $pages) ? (int)$_GET['p'] : 1;
        }

        self::$_values = array(
            'count' => $count,
            'pages' => $pages,
            'page' => $page,
            'onPage' => $onPage,
            'showAll' => $showAll,
        );

        return self::$_values;
    }

    /**
     * Returns SQL `LIMIT {OFFSET}, {COUNT}` statement based on set pagination values
     *
     * @return string
     */
    public static function sqlLimit()
    {
        return self::$_values && !self::$_values['showAll']? (' LIMIT ' . (self::$_values['page'] > 1 ? ((self::$_values['page'] - 1) * self::$_values['onPage']) : 0) . ', ' . self::$_values['onPage'] . ' ') : '';
    }

    /**
     * Returns pagination title part
     *
     * @return string
     */
    public static function title()
    {
        return self::$_values && self::$_values['pages'] > 1 ? ' – ' . self::$_values['page'] . ' страница' : '';
    }
}
