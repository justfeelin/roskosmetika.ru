<?php

/**
 * Search.
 */
class Search
{
    const REMOTE_URL = 'http://185.87.50.29/site-search.php';
    const REMOTE_KEY = 'jacT-aI8cAos[uR1Eui:0Wm]Zlpfo/{q>Pq9nsMJf`;z`nk85R';

    /**
     * Search in products.
     * @param array $words array of words for search.
     * @return array  array of products
     */
    static function in_products($words)
    {

        $return['quantity'] = 0;
        $return['products'] = null;

        if ($words) {
            $query = "SELECT DISTINCT p.id, p.name, IF( p.url =  '', p.id, p.url ) url, p.pack, p.price,
                             pp.src, pp.alt, pp.title photo_title
                      FROM products AS p LEFT JOIN rk_prod_desc pd   ON ( p.id = pd.id )
                                      LEFT JOIN rk_prod_photo pp  ON ( pp.id = p.id )
                      WHERE p.visible = 1 AND p.active = 1 AND p.main_id = 0 ";

            $i = 1;

            $name = '';
            $other = '';
            $tms = '';

            foreach ($words as $word) {

                //  length > 3
                $numeric = is_numeric($word);

                if (mb_strlen($word) >= 3 || $numeric) {
                    if (!get_magic_quotes_gpc()) {
                        $word = DB::mysql_secure_string($word);
                    }

                    $tm = (int)DB::get_field('id', "SELECT id FROM tm WHERE LOWER(name) LIKE '%$word%'");

                    if ($tm) {
                        if ($tms == '') {
                            $tms .= ' p.tm_id = ' . $tm;
                        } else {
                            $tms .= ' OR p.tm_id = ' . $tm;
                        }
                    } else {
                        if ($name != '' && $other != '') {
                            $name .= ' AND ';
                            $other .= ' OR ';
                        }

                        $name .= " LOWER(p.name) LIKE '%$word%' ";

                        $other .= " LOWER(pd.synonym) LIKE '%$word%'
                                  OR LOWER(pd.eng_name) LIKE '%$word%' ";

                        if ($numeric) $other .= " OR p.id LIKE '%$word%' ";
                    }
                }
            }

            $query .= ($name == '' ? '' : " AND $name ") . ($tms == '' ? '' : " AND ($tms) ") . ($other == '' ? '' : " OR ($other)") . ' ORDER BY p.name';

            $result = DB::get_rows($query);
            $return['quantity'] = count($result);
            $return['products'] = null;

            if ($result) {
                $return['products'] = array_chunk($result, 2);
            } else {

                $query = "SELECT DISTINCT p.id, p.name, IF( p.url =  '', p.id, p.url ) url, p.pack, p.price,
                                 pp.src, pp.alt, pp.title photo_title
                          FROM tm, products AS p LEFT JOIN rk_prod_desc pd   ON ( p.id = pd.id )
                                      LEFT JOIN rk_prod_photo pp  ON ( pp.id = p.id )
                          WHERE p.visible = 1 AND p.active = 1 AND p.main_id = 0 AND p.tm_id = tm.id AND (";

                $i = 1;

                foreach ($words as $word) {
                    //  length > 3
                    $numeric = is_numeric($word);
                    if (mb_strlen($word) >= 3 || $numeric) {
                        if ($i++ != 1) $query .= ' OR ';

                        if (!get_magic_quotes_gpc()) {
                            $word = DB::mysql_secure_string($word);
                        }
                        $query .= "LOWER(p.name) LIKE '%$word%'
                                   OR LOWER(pd.synonym) LIKE '%$word%'
                                   OR LOWER(tm.name) LIKE '%$word%'
                                   OR LOWER(pd.eng_name) LIKE '%$word%'
                                   OR LOWER(ingredients)  LIKE '%$word%' ";

                        if ($numeric) $query .= "OR p.id LIKE '%$word%'";
                    }
                }

                $query .= ') ORDER BY p.name';

                $result = DB::get_rows($query);
                $return['quantity'] = count($result);
                $return['products'] = null;
            }

            return $return;
        } // no words
        else {
            return $return;
        }
    }

    /**
     * Search in categories.
     * @param array $words array of words for search.
     * @return array  array of categories
     */
    static function in_categories($words)
    {
        if ($words) {
            $query = "SELECT c.id, c.name, c._full_url, h1
                      FROM rk_categories c LEFT JOIN rk_cat_desc cd   ON ( cd.id = c.id  )
                      WHERE c.visible = 1 AND c.canonical IS NULL AND ";
            $i = 1;

            $name = '';
            $other = '';
            $desc = '';

            foreach ($words as $word) {

                if ($i++ != 1) {
                    $name .= ' AND ';
                    $desc .= ' AND ';
                    $other .= ' OR ';
                }

                if (!get_magic_quotes_gpc()) {
                    $word = DB::mysql_secure_string($word);
                }

                $name .= " LOWER(c.name) LIKE '%$word%' ";
                $desc .= " LOWER(cd.desc) LIKE '%$word%' ";
                $other .= " LOWER(cd.synonym) LIKE '%$word%' ";
            }


            $query .= $name . " OR ($desc)" . " OR ($other)" . ' ORDER BY c.name';

            return DB::get_rows($query);
        } // there is no words.
        else {
            return null;
        }
    }

    /**
     * Search in trade marks.
     * @param array $words array of words for search.
     * @return array  array of tm's
     */
    static function in_tm($words)
    {
        if ($words) {
            $query = "SELECT t.id, t.name, IF( t.url =  '', t.id, t.url ) url
                      FROM tm t LEFT JOIN rk_tm_desc td   ON ( t.id = td.id )
                      WHERE ";
            $i = 1;
            foreach ($words as $word) {
                if ($i++ != 1) {
                    $query .= ' OR ';
                }
                if (!get_magic_quotes_gpc()) {
                    $word = DB::mysql_secure_string($word);
                }

                $query .= "LOWER(t.name) LIKE '%$word%'
                           OR LOWER(t.desc) LIKE '%$word%'
                           OR LOWER(td.synonym) LIKE '%$word%'
                           OR LOWER(td.country) LIKE '%$word%'";
            }

            return DB::get_rows($query);
        } // there is no words.
        else {
            return null;
        }
    }

    /**
     * Prepare search string
     * @param  string $search search string
     * @param  boolen $log log
     * @return array          array of words in search string
     */
    static function prepare_string($search, $log = TRUE)
    {
        $search = htmlspecialchars($search);

        if (!get_magic_quotes_gpc()) {
            $search = DB::mysql_secure_string($search);
        }

        $search = mb_strtolower($search);

        $return['search_string'] = $search;
        $return['words'] = FALSE;

        $words = mb_split(' ', $search);

        $stop_words = array('для', 'под', 'или', 'после', 'бля', 'хуй');
        $ok_words = array('бад', 'спа');

        foreach ($words as $key => $value) {
            $value = mb_strtolower($value);

            if (is_numeric($value) && mb_strlen($value, 'utf-8') >= 2) {
                $return['words'][$key] = (int)$value;
            } elseif (!in_array($value, $stop_words) && (mb_strlen($value, 'utf-8') >= 3 || in_array($value, $ok_words))) {
                $word_len = mb_strlen($value);

                if ($word_len >= 7) {
                    $return['words'][$key] = mb_substr($value, 0, mb_strlen($value) - 2);
                } elseif ($word_len >= 5) {
                    $return['words'][$key] = mb_substr($value, 0, mb_strlen($value) - 1);
                } else {
                    $return['words'][$key] = $value;
                }
            }
        }

        if ($log) {
            DB::query('INSERT INTO search_log (query, datetime)  VALUES ("' . DB::mysql_secure_string($search) . '", NOW())');
        }

        return $return;
    }

    /**
     * Makes call to remote search
     *
     * @param string $query Search query
     * @param string $index Index to search in
     * @param int|array $limit Limit rows in result. Can be associative array representing limit for each index
     * @param int $page Page for pagination (Starting from 1
     * @param bool $log Append search query to search log
     * @param bool $includeCount Include all items count. If true, first item in each array will be count, not item ID
     *
     * @return array An array of IDs for provided index. If no index provided - associative array of `index` => `IDs`
     */
    static function remote_search($query, $index = null, $limit = null, $page = null, $log = true, $includeCount = false)
    {
        $result = remote_request(self::REMOTE_URL . '?q=' . urlencode($query) . '&k=' . self::REMOTE_KEY .
            ($index ? '&i=' . $index : '') .
            ($limit ? '&' . (is_array($limit) ? http_build_query(array('l' => $limit)) : 'l=' . $limit) : '') .
            ($page ? '&p=' . $page : '') .
            ($log || $includeCount ? '&c' : ''),
            [],
            true,
            true,
            2
        );

        if ($result && ($log || $includeCount)) {
            $count = $result['products'][0];

            if (!$includeCount) {
                array_shift($result['products']);
                array_shift($result['categories']);
                array_shift($result['trademarks']);
            }
        } else {
            $count = 'NULL';
        }

        if ($log) {
            DB::query('INSERT INTO rk_search_logs (search, cnt, dt)  VALUES ("' . DB::mysql_secure_string($query) . '", ' . $count . ', NOW())');
        }

        return $result !== null
            ? $result
            : (
                $index !== null
                    ? array()
                    : array(
                        'products' => array(),
                        'categories' => array(),
                        'trademarks' => array(),
                    )
            );
    }

    /**
     * Performs products search based on different conditions
     *
     * @param array $params Conditions. Possible keys: "categories" (IDs of categories), "filters" (IDs of filters), "tms" (IDs of trademarks), "lines" (IDs of lines), "name" (Product name query), "and" (Boolean whether "AND" or "OR" logic to be applied, default false)
     *
     * @return array Array of product IDs
     */
    public static function search_by_params($params)
    {
        $allIDs = [];

        $and = !empty($params['and']);

        if (!empty($params['categories'])) {
            $categoriesSQL = implode(', ', $params['categories']);

            if (preg_match('/^\d+(?:, \d+)*$/', $categoriesSQL)) {
                DB::get_rows(
                    'SELECT pc3.prod_id
                    FROM rk_prod_cat AS pc3
                    WHERE pc3.cat_id IN (' . $categoriesSQL . ')

                    UNION

                    SELECT pc2.prod_id
                    FROM rk_prod_cat_lvl_2 AS pc2
                    WHERE pc2.cat_id IN (' . $categoriesSQL . ')

                    UNION

                    SELECT pc1.prod_id
                    FROM rk_prod_cat_lvl_1 AS pc1
                    WHERE pc1.cat_id IN (' . $categoriesSQL . ')',
                    function ($pair) use (&$allIDs) {
                        $allIDs[] = (int)$pair['prod_id'];
                    }
                );
            }
        }

        if (!empty($params['filters'])) {
            $filtersSQL = implode(', ', $params['filters']);

            if (preg_match('/^\d+(?:, \d+)*$/', $filtersSQL)) {
                $pFilterIDs = [];

                DB::get_rows(
                    'SELECT pf.prod_id
                    FROM rk_prod_filter AS pf
                    WHERE pf.filter_id IN (' . $filtersSQL . ')',
                    function ($pair) use (&$pFilterIDs) {
                        $pFilterIDs[] = (int)$pair['prod_id'];
                    }
                );

                $allIDs = $allIDs ? ($and ? array_intersect($allIDs, $pFilterIDs) : array_merge($allIDs, $pFilterIDs)) : $pFilterIDs;
            }
        }

        if (!empty($params['tms'])) {
            $tmsSQL = implode(', ', $params['tms']);

            if (preg_match('/^\d+(?:, \d+)*$/', $tmsSQL)) {
                $pTmIDs = [];

                DB::get_rows(
                    'SELECT p.id
                    FROM products AS p
                    WHERE p.tm_id IN (' . $tmsSQL . ')',
                    function ($product) use (&$pTmIDs) {
                        $pTmIDs[] = (int)$product['id'];
                    }
                );

                $allIDs = $allIDs ? ($and ? array_intersect($allIDs, $pTmIDs) : array_merge($allIDs, $pTmIDs)) : $pTmIDs;
            }
        }

        if (!empty($params['lines'])) {
            $linesSQL = implode(', ', $params['lines']);

            if (preg_match('/^\d+(?:, \d+)*$/', $linesSQL)) {
                $pLineIDs = [];

                DB::get_rows(
                    'SELECT pl.prod_id
                    FROM rk_prod_lines AS pl
                    WHERE pl.line_id IN (' . $linesSQL . ')',
                    function ($pair) use (&$pLineIDs) {
                        $pLineIDs[] = (int)$pair['prod_id'];
                    }
                );

                $allIDs = $allIDs ? ($and ? array_intersect($allIDs, $pLineIDs) : array_merge($allIDs, $pLineIDs)) : $pLineIDs;
            }
        }

        if (!empty($params['name'])) {
            $pSearchIDs = self::remote_search($params['name'], 'products', ['products' => 50], null, false);

            $allIDs = $allIDs ? ($and ? array_intersect($allIDs, $pSearchIDs) : array_merge($allIDs, $pSearchIDs)) : $pSearchIDs;
        }

        if (
            !empty($params['custom_ids'])
            && preg_match('/^\d+(?:, ?\d+)*$/', $params['custom_ids'])
        ) {
            $customIDs = explode(',', preg_replace('/, +/', ',', $params['custom_ids']));

            $allIDs = $allIDs ? ($and ? array_intersect($allIDs, $customIDs) : array_merge($allIDs, $customIDs)) : $customIDs;
        }

        return array_unique($allIDs);
    }
    /**
     * Get list of products
     * @param  array $products Array of results 
     * @return string          string of pID
     */
    public static function get_ids_string($products){

        $preID = array();

        for ($i=0; $i < count($products); $i++) { 
             $preID[] = $products[$i]['id'];
         }

         return implode(",", $preID);
    }
}
