<?php

class Cached
{
    /**
     * Returns index master classes html
     */
    public static function index_mcs()
    {
        return cache_item('index_mcs', function () {
            return Template::get_tpl('main_mc', null, array(
                'mcs' => mc::get_master_classes_list(),
                'month' => check::month(),
            ));
        }, 10800);//3 hours
    }

    /**
     * Returns day item html
     *
     * @return string
     */
    public static function day_item()
    {
        return cache_item('index_day_item', function () {
            return Template::get_tpl('day_item', null, day_items::get_day_item());
        },
        strtotime(date('Y-m-d 00:00:00', $_SERVER['REQUEST_TIME'] + 24 * 60 * 60)) - $_SERVER['REQUEST_TIME']);// till day end
    }

    /**
     * Return best hurry html
     */
    public static function index_hurry()
    {
        return cache_item('index_hurry', function () {
            return Template::get_tpl('main_hurry', null, products::hurry_sale_product());
        }, 3600);//1 hour
    }

    /**
     * Returns /tm page HTML
     *
     * @return string
     */
    public static function brands_list_page()
    {
        return cache_item((TM_FOR_PROF ? 'p_brands_list_page' : 'brands_list_page'), function () {
            list($letters) = Cached::brands_list();
            $letters['_'] = DB::get_rows('SELECT t.name, IF(t.url != "", t.url, t.id) AS url, t.country_id, c.name AS country
                FROM tm AS t
                INNER JOIN cdb_country AS c ON c.id = t.country_id AND (c.name = "Россия" OR t.name REGEXP "[А-Яа-я]")
                WHERE t.visible = 1 AND t._has_products = 1');

            usort($letters['_'], function ($a, $b) {
                mb_eregi('^[^a-z' . helpers::RUS_LETTERS . ']*([a-z' . helpers::RUS_LETTERS . '])', html_entity_decode($a['name']), $matchesA);
                mb_eregi('^[^a-z' . helpers::RUS_LETTERS . ']*([a-z' . helpers::RUS_LETTERS . '])', html_entity_decode($b['name']), $matchesB);

                $rusA = mb_strpos(helpers::RUS_LETTERS, mb_strtolower($matchesA[1])) !== false;
                $rusB = mb_strpos(helpers::RUS_LETTERS, mb_strtolower($matchesB[1])) !== false;

                return $rusA === $rusB ? ($a['name'] < $b['name'] ? -1 : 1) : ($rusA ? -1 : 1);
            });

            return Template::get_tpl('brands_list_page', null, [
                'tm' => $letters,
            ]);
        }, 1200);//20 minutes
    }

    /**
     * Returns brands list grouped by first letter
     *
     * @param bool $withRussians Include russian brands
     *
     * @return array [`letters`, `lastLetter`]
     */
    public static function brands_list($withRussians = true)
    {
        return cache_item((TM_FOR_PROF ? 'p_brands_list_' : 'brands_list_') . ($withRussians ? '1' : '0'), function () use ($withRussians) {
            $letters = [];

            DB::get_rows(
                'SELECT t.id, t.name, t.url, t.country_id, c.name AS country, rcp.url AS country_url, rcp.h1 AS country_name, rcp.visible AS country_visible
                FROM tm AS t
                INNER JOIN cdb_country AS c ON c.id = t.country_id 
                INNER JOIN rk_country_page AS rcp ON rcp.country_id = t.country_id ' .
                ($withRussians ? '' : 'INNER JOIN rk_tm_desc AS td ON td.id = t.id AND td.country != "Россия" ') .
                'WHERE t.visible = 1 AND t._has_products = 1 ' . (TM_FOR_PROF ? 'AND t.prof = 1' : ''),
                function ($brand) use (&$letters, $withRussians) {
                    mb_eregi('^[^a-z' . ($withRussians ? helpers::RUS_LETTERS : '') . ']*([a-z' . ($withRussians ? helpers::RUS_LETTERS : '') . '])', html_entity_decode($brand['name']), $matches);

                    if ($matches) {
                        if (!$brand['url']) {
                            $brand['url'] = 'id';
                        }

                        unset($brand['id']);

                        $letters[mb_strtoupper($matches[1])][] = $brand;
                    }
                }
            );

            ksort($letters);

            $lastLetter = null;

            foreach ($letters as $letter => &$brands) {
                usort($brands, function ($brand1, $brand2) {
                    return mb_convert_case($brand1['name'], MB_CASE_LOWER) < mb_convert_case($brand2['name'], MB_CASE_LOWER) ? -1 : 1;
                });
            }

            if (!empty($letter)) {
                $lastLetter = $letter;
            }

            return [
                $letters,
                $lastLetter,
            ];
        }, 1200);//20 minutes
    }

    /**
     * Returns alphabetical brands list html
     */
    public static function brands_list_html()
    {
        return cache_item((TM_FOR_PROF ? 'p_brands_list_html' : 'brands_list_html'), function () {
            list($letters, $lastLetter) = self::brands_list();

            return Template::get_tpl('2016/tm_list', null, [
                'letters' => $letters,
                'lastLetter' => $lastLetter,
            ]);
        }, 1200);//20 minutes
    }

    /**
     * Returns popular product IDs
     *
     * @return array
     */
    public static function popular_product_ids()
    {
        return cache_item(
            'popular_products_ids',
            function () {
                return DB::get_rows(
                    'SELECT p.id
                    FROM (
                        SELECT pr.id,
                        (
                            SELECT SUM(c.quantity)
                            FROM rk_orders_items AS c
                            WHERE c.product_id = pr.id
                        ) AS ordered
                        FROM products AS pr
                        INNER JOIN rk_prod_photo AS pp ON pp.id = pr.id
                        WHERE pr.visible = 1
                            AND pr.active = 1
                            AND pr.main_id = 0
                            AND pr.available = 1
                            AND pr.id NOT IN (' . implode(', ', [Orders::PRESENT_PRODUCT_ID, Orders::CONSULTATION_PRODUCT_ID, Orders::HELP_PRODUCT_ID, Orders::CONSULTANT_HELP_PRODUCT_ID]) . ')
                    ) AS p
                    ORDER BY p.ordered DESC
                    LIMIT 1000',
                    function ($product) {
                        return (int)$product['id'];
                    }
                );
            },
            10800// 3 hours
        );
    }
}
