<?php

abstract class Search_Logs
{
    /**
     * Returns count of logs rows
     *
     * @param string $where SQL conditions (Without `WHERE`)
     *
     * @return int
     */
    public static function get_count($where = '')
    {
        return intval(DB::get_field('cnt', 'SELECT COUNT(id) AS cnt FROM rk_search_logs ' . ($where ? ' WHERE ' . $where : '')));
    }

    /**
     * Returns logs rows
     *
     * @param int $page Current page (Starting from 1). If null - all rows returned
     * @param int $onpage Logs on page
     * @param string $where SQL conditions (Without `WHERE`)
     *
     * @return array
     */
    public static function get_rows($page = 1, $onpage = 50, $where = '')
    {
        return DB::get_rows('SELECT search, dt FROM rk_search_logs ' . ($where ? ' WHERE ' . $where : '') . ' ORDER BY id DESC ' . ($page !== null ? 'LIMIT ' . ($page > 1 ? $onpage * ($page - 1) : 0) . ', ' . $onpage : ''));
    }
}
