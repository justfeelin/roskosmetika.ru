<?php

/**
 * Абстрактый контроллер.
 */
abstract class Controller_Base
{
    /**
     * Конструктор.
     */
    function __construct()
    {
        //Вывод на ошибку при количестве переменных в сторке, превышающее использование контроллером. + Admitad  
        if (strpos($_SERVER['SCRIPT_FILENAME'], '/www/admin/') === false) {
            ab_test::init();

            // Admitad uid
            $set_uid = TRUE;

            if (isset($_SERVER['HTTP_REFERER']) && stristr($_SERVER['HTTP_REFERER'], 'roskosmetika.ru')) {
              $set_uid = FALSE;  
            }

            if (isset($_GET['admitad_uid'])) {

                $admitad_uid = trim($_GET['admitad_uid']);

                if (check::sid($admitad_uid) && $set_uid) {
                    setcookie('admitad_uid', $admitad_uid, time()+60*60*24*30, '/', $_SERVER['HTTP_HOST'], false, true);   
                }
            }

            if (!isset($_SESSION[referral::REFERRAL_KEY])) {
                $code = !empty($_GET[referral::REFERRAL_KEY]) ? $_GET[referral::REFERRAL_KEY] : (!empty($_COOKIE[referral::REFERRAL_KEY]) ? $_COOKIE[referral::REFERRAL_KEY] : null);

                if ($code) {
                    $referral = referral::checkCode($code);

                    if ($referral && Auth::is_auth()) {
                        if (Orders::check_referral_order($referral['id'], Auth::get_user_id())) {
                            $referral = null;
                        }
                    }

                    $refID = $referral ? (int)$referral['id'] : 0;
                } else {
                    $refID = 0;
                }

                setcookie(referral::REFERRAL_KEY, $refID ? $code : '', $_SERVER['REQUEST_TIME'] + ($refID ? 604800 : -9999999), '/');//week

                if ($refID || !isset($_SESSION[referral::REFERRAL_KEY])) {
                    $_SESSION[referral::REFERRAL_KEY] = $refID ? [
                        'id' => $refID,
                        'discount' => (int)$referral['referral_discount'],
                    ] : 0;

                    if ($refID) {
                        $GLOBALS['referral_popup'] = true;

                        $_SESSION['agent_id'] = Orders::AGENT_ID_REFERRAL;
                    }
                }
            }

            if (!isset($_SESSION['agent_id'])) {
                $agentID = Orders::AGENT_ID_RK;

                if (isset($_GET['utm_source'])) {
                    switch ($_GET['utm_source']) {
                        case'newsletter':
                            $agentID = Orders::AGENT_ID_NEWSLETTER;

                            break;

                        case 'admitad':
                            $agentID = Orders::AGENT_ID_ADMITAD;

                            break;

                        case 'criteo':
                            $agentID = Orders::AGENT_ID_CRITEO;

                            break;

                        case 'fb_leadza':
                            $agentID = Orders::AGENT_ID_FB_LEADZA;

                            break;

                        case 'site':
                        case 'server':
                            if (isset($_GET['utm_medium']) && $_GET['utm_medium'] === 'newsletter') {
                            $agentID = Orders::AGENT_ID_NEWSLETTER;
                        }

                        break;
                    }
                }

                $_SESSION['agent_id'] = $agentID;
            }

            if (isset($_GET['route'])) {

                $controllers = array(
                    'articles' => 1,
                    'cart' => 3,
                    'category' => 4,
                    'error' => 0,
                    'feedback' => 0,
                    'forgot' => 0,
                    'index' => 0,
                    'lo' => 0,
                    'login' => 2,
                    'master_classes' => 1,
                    'order' => 3,
                    'page' => 1,
                    'product' => 1,
                    'registration' => 2,
                    'rewiew' => 1,
                    'room' => 2,
                    'search' => 0,
                    'sitemap' => 0,
                    'unsubscribe' => 3,
                    'tm' => 2,
                    'sets' => 1,
                    'prod_review' => 1,
                    'category-tm' => 4,
                    'kosmetika' => 2,
                    'payment' => 3,
                    'country' => 1,
                );

                $do = 'no';
                foreach ($controllers as $key => $value) {
                    if ($key == substr($_GET['route'], 0, strpos($_GET['route'], '/'))) {
                        $do = $value;
                        break;
                    }
                }

                if ($do !== 'no') {
                    if ((int)mb_substr_count($_GET['route'], '/') > $do) {
                        header("HTTP/1.0 404 Not Found");
                        header('Location: ' . DOMAIN_FULL . '/404');
                        exit();
                    }

                }
            }
        } else {
            if (empty($_SESSION['admin'])) {
                if ($rights = admin::check_login()) {
                    $_SESSION['admin'] = $rights[0];

                    setcookie(admin::ACCESS_TOKEN, $rights[1], $_SERVER['REQUEST_TIME'] + 60 * 60 * 24 * 30, '/');

                    header('Location: ' . $_SERVER['REQUEST_URI']);
                } else {
                    header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden');

                    Template::output('login_form');
                }

                die();
            }

            if (!admin::check_access($this->admin_access())) {
                $adminPages = admin::menu();

                for ($i = 0, $n = count($adminPages); $i < $n; ++$i) {
                    $groupAccess = isset($adminPages[$i]['access']) ? $adminPages[$i]['access'] : null;

                    for ($y = 0, $m = count($adminPages[$i]['items']); $y < $m; ++$y) {
                        if (
                            empty($adminPages[$i]['items'][$y]['access'])
                            && $groupAccess === null
                            || admin::check_access(!empty($adminPages[$i]['items'][$y]['access']) ? $adminPages[$i]['items'][$y]['access'] : $groupAccess)
                        ) {
                            redirect($adminPages[$i]['items'][$y]['url']);
                        }
                    }
                }

                header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden');

                echo '<h1>Forbidden</h1>';

                die();
            }
        }
    }


    /**
     * index
     */
    abstract function index($arg);

    /**
     * Выводит страницу 404 ошибки.
     *
     * @param bool $ajax Whether is ajax request. If true - no output will be provided
     * @param bool $exitOnAjax exit(0) on AJAX request
     */
    protected function nopage($ajax = false, $exitOnAjax = true)
    {
        header("HTTP/1.0 404 Not Found");

        if (!$ajax) {
            $id_list =agregation_products::get_personal_product_list(helpers::search_user_id(),2,4);

            Template::set('mb_top_view', product::get_by_id($id_list/*mindbox::get_recomendation('PoProsmotram4', FALSE, 4)*/, true, true, false, false, false));
            Template::set_page('404', 'Ошибка 404', [
                'main_spec' => lines::main_special(),
            ]);
        } else {
            if ($exitOnAjax) {
                exit(0);
            }
        }
    }


    /**
     * Возврат на предыдущею страницу.
     */
    protected function go_back()
    {
        redirect(isset($_SERVER ['HTTP_REFERER']) ? $_SERVER ['HTTP_REFERER'] : '/');
    }

    /**
     * Returns pagination parameters
     *
     * @param int $count Total rows count
     * @param int $onPage Rows on page count
     * @param array $getParams Associative array of GET parameters on page
     *
     * @return array <pre>
     *      array(
     *          'count' => {Total rows count},
     *          'pages' => {Pages count},
     *          'page' => {Current page},
     *          'queryString' => {Additional query string for pages link, starting with '&' (If not empty), based on provided GET parameters},
     *      )
     * </pre>
     */
    protected function pagination($count, $onPage, $getParams = null)
    {
        if ($getParams) {
            $queryString = [];

            foreach ($getParams as $key => $value) {
                if ($value !== null) {
                    $queryString[] = h($key) . '=' . (is_bool($value) ? ($value ? 1 : 0) : h($value));
                }
            }

            $queryString = $queryString ? join('&', $queryString) : '';
        } else {
            $queryString = '';
        }

        $pagination = Pagination::setValues($count, $onPage);

        Pagination::setBaseUrl((($qN = strpos($_SERVER['REQUEST_URI'], '?')) !== false ? substr($_SERVER['REQUEST_URI'], 0, $qN) : $_SERVER['REQUEST_URI']) . ($queryString ? '?' : '') . $queryString);

        $pagination['queryString'] = $queryString;

        Pagination::setTemplateSettings();

        return $pagination;
    }

    /**
     * Outputs JSON data
     *
     * @param mixed $data Data to encode & output
     * @param bool $exit Exit script on call
     */
    protected function jsonResponse($data, $exit = true)
    {
        json_headers();

        print json_encode($data);

        if ($exit) {
            exit;
        }
    }

    /**
     * Returns admin access rights to check for request
     *
     * @return int
     */
    protected function admin_access()
    {
        return admin::RIGHTS_COMMON;
    }
}
