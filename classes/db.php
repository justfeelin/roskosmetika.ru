<?php

/**
 * Working with data base
 *
 * @author CheS (nov 2013)
 */
class DB
{
    /**
     * @var DbConnection Active connect
     */
    private static $con;

    /**
     * @var bool Debug flag
     */
    private static $_debug;

    /**
     * Connecting to data base.
     * @param string $host
     * @param string $db
     * @param string $user
     * @param string $password
     */
    static function connect($host, $db, $user, $password)
    {
        self::$con = new DbConnection( $host, $user, $password, $db);

        self::$_debug = DEBUG;

        self::$con->debug = self::$_debug;
    }

    /**
     * Query to DB
     *
     * @param string $query query string
     *
     * @return mysqli_result
     */
    static function query($query)
    {
        return self::$con->query($query);
    }

    /**
     * Returns all values of query
     *
     * @param string $query query string
     * @param Callable $callback Callback to be executed for each row before appending to result. If return null - row won't be appended
     *
     * @return array
     */
    static function get_rows($query, $callback = null)
    {
        return self::$con->getRows($query, $callback);
    }

    /**
     * Returns one string of query
     *
     * @param string $query query string
     *
     * @return array
     */
    static function get_row($query)
    {
        return self::$con->getRow($query);
    }

    /**
     * Returns quantity of values(strings) if query ($query != null) or pre query.
     *
     * @param string $query query string.
     *
     * @return integer
     */
    static function get_num_rows($query)
    {
        $res = self::$con->query($query);

        $numRows = $res->num_rows;

        $res->close();

        return $numRows;
    }

    /**
     * Returns field of value from query.
     *
     * @param string $field field
     * @param string $query query string
     *
     * @return string
     */
    static function get_field($field, $query)
    {
        $row = self::get_row($query);

        return $row && isset($row[$field]) ? $row[$field] : null;
    }

    /**
     * Возвращает последний id insert-запроса.
     * @return integer
     */
    static function get_last_id()
    {
        return self::$con->insert_id;
    }

    /**
     * return last query info
     * @return integer
     */
    static function last_query_info()
    {
        return self::$con->affected_rows;
    }

    /**
     * Return secure string
     * @param  string $string Incoming string
     * @return string         secure string
     */
    static function mysql_secure_string($string)
    {
        return self::$con->escape($string);
    }

    /**
     * mysql_secure_string() alias
     *
     * @param string $string String to escape
     *
     * @return string
     */
    public static function escape($string)
    {
        return self::mysql_secure_string($string);
    }

    /**
     * Calls DbConnection::outputQueries()
     *
     * @return string
     */
    public static function output_queries()
    {
        return self::$_debug ? self::$con->outputQueries() : '';
    }
}
