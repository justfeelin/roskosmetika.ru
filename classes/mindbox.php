<?php

class mindbox
{
    /**
     * Enabled flag
     */
    const ENABLED = true;

    /**
     * Authentication ticket based on identity
     */
    const TICKET_TYPE_IDENTITY = 1;

    /**
     * Authentication ticket based on email
     */
    const TICKET_TYPE_EMAIL = 2;

    /**
     * Authentication ticket based on phone number
     */
    const TICKET_TYPE_PHONE = 3;

    /**
     * Data to be applied
     *
     * @var array
     */
    private static $_actions = [];

    /**
     * Outputs initial javascript
     */
    public static function output_init_js()
    {
        if (self::ENABLED) {
            print "<script>mindbox=window.mindbox||function(){mindbox.queue.push(arguments)};mindbox.queue = mindbox.queue || [];mindbox('create',{projectSystemName:'Roskosmetika',brandSystemName:'Roskosmetika',pointOfContactSystemName:'www.roskosmetika.ru',projectDomain: 'roskosmetika-services.directcrm.ru'})</script> <script src=\"https://api.mindbox.ru/scripts/v1/tracker.js\" async></script>";

            if (self::$_actions) {
                print '<script>';

                for ($i = 0, $n = count(self::$_actions); $i < $n; ++$i) {
                    print 'mindbox("' . self::$_actions[$i][0] . '", ' . json_encode(
                        [
                        'operation' => self::$_actions[$i][1]
                        ] + (
                            isset(self::$_actions[$i][2])
                                ? self::$_actions[$i][2]
                                : []
                        )
                    ) . ');';
                }

                print '</script>';
            }
        }
    }

    /**
     * Adds action to be performed
     *
     * @param string $name Action name
     * @param string $operation Operation name
     * @param array $data Action data
     */
    public static function add_action($name, $operation, $data)
    {
        self::$_actions[] = [
            $name,
            $operation,
            $data,
        ];
    }

    /**
     * Returns authentication ticket value
     *
     * @param int $type Ticket type. 1 - external identity, 2 - E-mail, 3 - phone
     * @param string $data Data to be hashed
     *
     * @return string
     */
    public static function auth_ticket($type, $data)
    {
        switch ($type) {
            case self::TICKET_TYPE_IDENTITY:
                $prefix = 'ExternalIdentityAuthentication';

                $data = env('MINDBOX_CUSTOMER_FIELD') . '|' . $data;

                break;

            case self::TICKET_TYPE_EMAIL:
                $prefix = 'EmailAuthenticationHex';

                break;

            case self::TICKET_TYPE_PHONE:
                $prefix = 'MobilePhoneAuthenticationHex';

                break;

            default:
                return null;

                break;
        }

        $original = $prefix . '|' . $data . '|' . date('Y-m-d H:i:s');

        $hash = strtoupper(hash_hmac('sha512', $original, env('MINDBOX_HASH_KEY')));

        return self::_string_hex($original) . '|' . self::_string_hex($hash);
    }

    /**
     * Sends updated order info to mindbox
     *
     * @param int $orderNumber Order number
     *
     * @return string Error string
     */
    public static function update_order($orderNumber)
    {
        $order = Orders::get_by_id($orderNumber);

        if (!$order) {
            return null;
        }

        $products = Orders::get_order_products($orderNumber);

        if (!$products) {
            return null;
        }

        $identity = env('MINDBOX_IDENTITY');

        $data = [
            'order' => [
                'ids' => [
                    $identity => 'Р-'. $order['site_order_number'],
                ],
                'pointOfContact' => $identity,
                'updatedDateTimeUtc' => date('Y-m-d H:i:s', strtotime("-3 hours", strtotime(($order['date_of_change'] ?: $order['added_date'])))),
                'lines' => [
                    'line' => [],
                ],
            ],
        ];

        $total = 0;

        for ($i = 0, $n = count($products); $i < $n; ++$i) {
            if ($products[$i]['price'] < 0 || $products[$i]['quantity'] < 1) {
                continue;
            }

            $data['order']['lines']['line'][] = [
                'sku' => [
                    'productId' => $products[$i]['id'],
                    'basePricePerItem' => roundPrice($products[$i]['price']),
                ],
                'quantity' => $products[$i]['quantity'],
                'status' => $order['status_key'],
            ];

            $total += roundPrice($products[$i]['price']) * (int)$products[$i]['quantity'];
        }

        if ($total === 0) {
            return null;
        }

        $data['order']['totalPrice'] = in_array((int)$order['status_id'], [Orders::STATUS_ID_RETURN, Orders::STATUS_ID_CANCEL, Orders::STATUS_ID_CANCEL_CLIENT, Orders::STATUS_ID_CORRUPT]) ? 0 : $total;

        if ($data['order']['totalPrice'] > 0 && $order['delivery'] > 0) {

            $data['order']['deliveryCost'] = roundPrice($order['delivery']);
            $data['order']['totalPrice'] = $data['order']['totalPrice'] + $data['order']['deliveryCost'];

        }

        if ($order['client_id']) {
            $data['order']['customer'] = [
                'ids' => [
                    'RoskosmetikaWebSiteId' => $order['client_id'],
                ],
                'email' => $order['email'],
                'mobilePhone' => $order['phone'] ? self::prepare_phone($order['phone']) : '',
                'firstName' => self::prepare_name($order['name']),
                'lastName' => self::prepare_name($order['surname']),
            ];
        }

        $xml = self::_array_to_xml($data);

        $file = SITE_PATH . 'tmp/' . uniqid('mindbox_export_order_') . '.xml';

        $xml->asXML($file);

        $ch = curl_init('https://roskosmetika-services.directcrm.ru/v2.1/orders/update-order?operation=Roskosmetika.Order.Status');

        curl_setopt_array(
            $ch,
            [
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 60,
                CURLOPT_FAILONERROR => false,
                CURLOPT_POSTFIELDS => file_get_contents($file),
                CURLOPT_HTTPHEADER => [
                    'Accept: application/xml',
                    'Authorization: DirectCrm key="' . env('MINDBOX_HASH_KEY') . '"',
                    'Content-Type: application/xml',
                    'Content-Length' => filesize($file),
                ],
            ]
        );

        $response = curl_exec($ch);

        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $return = null;

        if (($errno = curl_errno($ch)) || $code >= 300) {
            $return = 'Curl error: ' . curl_error($ch) . ' (Error number: ' . $errno . ', response code: ' . $code . ")\nRequest data:\n" . print_r($data, true) . "\n";

            if ($response) {
                $return .= "\nCurl response:\n" . $response . "\n";
            }
        }

        curl_close($ch);

        unlink($file);

        return $return;
    }

    /**
     * Prepares name for export to mindbox
     *
     * @param string $name Original name
     *
     * @return string
     */
    public static function prepare_name($name)
    {
        return mb_eregi_replace('[^а-я ]', '', $name, 'i');
    }

    /**
     * Prepares phone to be used for mindbox export
     *
     * @param string $phone Original phone
     *
     * @return string
     */
    public static function prepare_phone($phone)
    {
        if ($phone !== '') {
            $phone = explode(',', $phone);

            $phone = preg_replace('/[^\d]/', '', trim($phone[0]));

            if ($phone) {
                if ($phone[0] === '8') {
                    $phone[0] = '7';
                }

                if (preg_match('/^789(\d{9})$/', $phone, $m)) {
                    $phone = '79' . $m[1];
                } else {
                    if (!preg_match('/^7\d{10}$/', $phone)) {
                        $phone = '';
                    }
                }
            }
        }

        return $phone;
    }

    /**
     * Returns hexadecimal string representation
     *
     * @param string $string Original string
     *
     * @return string
     */
    private static function _string_hex($string)
    {
        return strtoupper(bin2hex($string));
    }

    /**
     * Converts array to XML
     *
     * @param array $data Array data
     * @param SimpleXMLElement $xml SimpleXMLElement element to append children
     *
     * @return SimpleXMLElement
     */
    private static function _array_to_xml($data, &$xml = null) {
        $returnXml = $xml === null;

        if ($returnXml) {
            $keys = array_keys($data);

            $xml = new SimpleXMLElement('<?xml version="1.0"?><' . $keys[0] . '></' . $keys[0] . '>');

            $data = $data[$keys[0]];
        }

        foreach($data as $key => $value) {
            if(is_array($value)) {
                if (self::_is_indexed($value)) {
                    for ($i = 0, $n = count($value); $i < $n; ++$i) {
                        $subnode = $xml->addChild($key);

                        self::_array_to_xml($value[$i], $subnode);
                    }
                } else {
                    $subnode = $xml->addChild($key);

                    self::_array_to_xml($value, $subnode);
                }
            } else {
                $xml->addChild((string)$key, htmlspecialchars((string)$value));
            }
         }

         return $returnXml ? $xml : null;
    }

    /**
     * Returns whether array is indexed
     *
     * @param array $array Array to check
     *
     * @return bool
     */
    private static function _is_indexed($array) {
        foreach ($array as $key => $value) {
            if (!is_numeric($key) || (string)$key !== (string)((int)$key)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get product recomendations 
     * @param  sting  $operation type of recomended mechanics
     * @param  string $p_id      Product ID
     * @param  int    $limit     IDs quantity
     * @param  int    $uid       User ID
     * @return array            array of pIDs 
     */
    public static function get_recomendation($operation, $p_id, $limit, $uid = FALSE)
    {
        $return_IDs = array();

        $url = 'https://roskosmetika-services.directcrm.ru/v2/recommendation-mechanics/get-product-recommendations?operation=' . $operation . ($p_id ? '&products=' . $p_id : '') . ($uid ? '&customerIdentity=' . $uid : '') .'&limit=' . $limit;
        $products = remote_request($url, [], false, true, 3, true);
        
        if (!empty($products) & isset($products['recommendations']['products'])) {

            $products = $products['recommendations']['products'];
            
            for ($i=0; $i < count($products); $i++) { 
                $return_IDs[] = $products[$i]['externalId'];
            }
        }

        return $return_IDs;

    }
}
