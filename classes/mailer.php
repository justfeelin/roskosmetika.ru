<?php

/**
 * Контроллер отправки почты.
 */
class mailer
{

    //настройки
    static $transport;
    //тип
    static $content_type = 'text/html';
    //кодировка
    static $charset = 'utf-8';
    //прикрепление файлов
    public static $attachments = [];

    /**
     * Устанавливает настройки письма
     * @param string $my_charset - кодировка письма
     * @param string $my_content_type - тип контента
     */
    static function settings($my_charset, $my_content_type)
    {

        self::$content_type = $my_content_type;
        self::$charset = $my_charset;
    }

    /**
     * Устанавливает настройки прикрепляемого файла
     * @param string $path - путь для
     * @param string $typee - тип контента
     */
    static function attachment($path, $type)
    {
        self::$attachments[] = [$path, $type];
    }

    /**
     * Подключение библиотеки и отправка письма
     * @param string $theme - тема письма
     * @param string $from - отправитель
     * @param string $to - получатель
     * @param string $body - текст письма
     *
     * @throws Exception
     *
     * @return bool
     */
    static function mail($theme, $from, $to, $body)
    {
        //подключение нужной библиотеки
        require_once 'lib/swift_required.php';

        //Настройки smtp 
        self::$transport = Swift_SmtpTransport::newInstance(env('SMTP_HOST'), env('SMTP_PORT'))
            ->setUsername(env('SMTP_USER'))
            ->setPassword(env('SMTP_PASSWORD'));

        $mailer = Swift_Mailer::newInstance(self::$transport);
        $message = Swift_Message::newInstance($theme)
            ->setFrom($from)
            ->setTo($to)
            ->setBody($body, self::$content_type, self::$charset);

        if (self::$attachments) {
            for ($i = 0, $n = count(self::$attachments); $i < $n; ++$i) {
                $attachment = Swift_Attachment::fromPath(self::$attachments[$i][0], self::$attachments[$i][1]);
                $message->attach($attachment);
            }
        }

        if (!$mailer->send($message)) {
            throw new Exception('Невозможно отправить письмо.');
        } else {
            return TRUE;
        }
    }
}
