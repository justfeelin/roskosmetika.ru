<?php

/**
 * Working with product reviews
 */
class prod_review
{

    /**
     * Get review for product by id
     * @param  int $prod_id product id
     * @return array   array of product review info
     */
    static function get_list($prod_id)
    {

        $query = "SELECT name, `date` , comment, city, DAY (`date`) day, MONTH(`date`) month
                  FROM   rk_prod_review
                  WHERE  prod_id = $prod_id
                  AND    visible = 1
                  AND    negative = 0
                  LIMIT 2";

        return DB::get_rows($query);
    }

    /**
     * Get reviews for all products
     * @return array array of urls
     */
    static function sm_list()
    {

        $query = "SELECT p.id, IF( p.url =  '', p.id, p.url ) url,
      (SELECT pr2.date FROM rk_prod_review pr2 WHERE pr2.prod_id = p.id ORDER BY pr2.date DESC LIMIT 1) lastmod
      FROM products p, rk_prod_review pr
      WHERE pr.prod_id = p.id
      AND pr.visible =1
      AND pr.negative = 0
      AND p.visible =1 AND p.active = 1
      GROUP BY p.id";

        return DB::get_rows($query);
    }


    /**
     * Add comment for product
     * @param int $prod_id product id
     * @param string $email users email
     * @param string $name users name
     * @param string $comment comment text
     * @param string $city users city
     */
    static function add_comment($prod_id, $email, $name, $comment, $city)
    {

        $prod_id = (int)$prod_id;

        $query = "INSERT INTO rk_prod_review (prod_id, name, email, date, comment, city, visible)
                  VALUE ($prod_id, '" . DB::escape($name) . "', '" . DB::escape($email) . "', NOW(), '" . DB::escape($comment) . "', '" . DB::escape($city) . "', 0)";


        $return = DB::query($query);

        $fio = check::separate_fio($name);
        unsubscribe::add_self($email, 'self', 'roskosmetika.ru', $fio['name'], $fio['surname'], $fio['patronymic']);

        return $return;

    }

    /**
     * Updates review row
     *
     * @param int $id Review ID
     * @param string $name Review name
     * @param string $city Review city
     * @param string $date Review date (`YYYY-MM-DD` format)
     * @param string $comment Review comment
     * @param bool $visible Review visible flag
     * @param bool $negative Negative visible flag
     */
    public static function update($id, $name, $city, $date, $comment, $visible, $negative)
    {
        $name = DB::mysql_secure_string($name);
        $city = DB::mysql_secure_string($city);
        $date = DB::mysql_secure_string($date);
        $comment = DB::mysql_secure_string($comment);

        DB::query('UPDATE rk_prod_review SET name = "' . $name . '", city = "' . $city . '", date = "' . $date . '", comment = "' . $comment . '", visible = ' . ($visible ? 1 : 0) . ', negative = ' . ($negative ? 1 : 0) . ' WHERE id = ' . intval($id));
    }

    /**
     * Get all reviews by product id
     * @param  int $prod_id product id
     * @return array|null   array of products
     */
    static function get_all($prod_id)
    {

        $return = FALSE;

        $query = "SELECT name, `date` , comment, city, DAY (`date`) day, MONTH(`date`) month
                  FROM   rk_prod_review
                  WHERE  prod_id = $prod_id
                  AND    visible = 1
                  AND    negative = 0
                  ORDER BY `date` ";

        $return['reviews'] = DB::get_rows($query);

        $query = "SELECT p.id, p.name, IF(p.url = '', p.id, p.url) AS url,
                      t.name AS tm_name, td.alt_name AS tm_alt_name
                  FROM products AS p
                  INNER JOIN tm AS t ON t.id = p.tm_id
                  INNER JOIN rk_tm_desc AS td ON td.id = t.id
                  WHERE p.id = $prod_id
                  ";

        $return['product'] = DB::get_row($query);

        return $return;
    }

    /**
     * Returns single review
     *
     * @param int $id Review ID
     *
     * @return array
     */
    public static function one($id)
    {
        return DB::get_row('SELECT r.*, p.id AS product_id, p.name AS product_name FROM rk_prod_review AS r ' .
            'LEFT JOIN products AS p ON p.id = r.prod_id ' .
            'WHERE r.id = ' . intval($id));
    }

    /**
     * Returns total count of product reviews
     *
     * @param bool $read `read` filter (0 or 1). null to ignore
     * @param bool $visible `visible` filter (0 or 1). null to ignore
     * @param bool $negative `negative` filter (0 or 1). null to ignore
     *
     * @return int
     */
    public static function count($read, $visible, $negative)
    {
        return intval(DB::get_field('cnt', 'SELECT count(r.id) AS cnt FROM rk_prod_review AS r ' . self::_searchCondition($read, $visible, $negative)));
    }

    /**
     * Returns product reviews filtered
     *
     * @param bool $read `read` filter (0 or 1). null to ignore
     * @param bool $visible `visible` filter (0 or 1). null to ignore
     * @param bool $negative `negative` filter (0 or 1). null to ignore
     * @param int $page Current page (Starting from 1)
     * @param int $onPage Reviews on page
     *
     * @return array
     */
    public static function search($read, $visible, $negative, $page, $onPage)
    {
        return DB::get_rows('SELECT r.*, p.id AS product_id, p.name AS product_name, IF(p.url != "", p.url, p.id) AS product_url
            FROM rk_prod_review AS r
            LEFT JOIN products AS p ON p.id = r.prod_id ' .
            self::_searchCondition($read, $visible, $negative) . ' LIMIT ' . $onPage * ($page - 1) . ', ' . $onPage,
            function ($review) {
                $cnt = (int)DB::get_field('cnt', 'SELECT COUNT(r.id) AS cnt FROM rk_prod_review AS r WHERE LTRIM(RTRIM(r.comment)) = LTRIM(RTRIM("' . DB::escape($review['comment']) . '"))');

                $review['dublicates'] = $cnt > 1 ? $cnt - 1 : false;

                return $review;
            }
        );
    }

    /**
     * Toggles review's negative flag
     *
     * @param int $reviewID Review ID
     *
     * @return bool
     */
    public static function toggle_negative($reviewID)
    {
        return !!DB::query('UPDATE rk_prod_review SET negative = IF(negative, 0, 1) WHERE id = ' . (int)$reviewID);
    }

    /**
     * Returns SQL conditions for reviews fetch
     *
     * @param bool $read `read` filter (0 or 1). null to ignore
     * @param bool $visible `visible` filter (0 or 1). null to ignore
     * @param bool $negative `negative` filter (0 or 1). null to ignore
     *
     * @return string 'WHERE {condition}' or empty string. `r` must be used for table alias!!!
     */
    private static function _searchCondition($read, $visible, $negative)
    {
        $where = array();

        if ($read !== null) {
            $where[] = 'r.read = ' . ($read ? 1 : 0);
        }

        if ($visible !== null) {
            $where[] = 'r.visible = ' . ($visible ? 1 : 0);
        }

        if ($negative !== null) {
            $where[] = 'r.negative = ' . ($negative ? 1 : 0);
        }

        return $where ? ' WHERE ' . implode(' AND ', $where) : '';
    }
}
