<?php

/**
 * Работа с клиентами.
 */
class Users
{
    /**
     * Masseur client type ID
     */
    const CLIENT_TYPE_MASSEUR = 1;

    /**
     * Company client type ID
     */
    const CLIENT_TYPE_COMPANY = 2;

    /**
     * Individual client type ID
     */
    const CLIENT_TYPE_INDIVIDUAL = 3;

    /**
     * Key used to store cookie hash
     */
    const HASH_KEY = 'uid';

    /**
     * Возвращает список всех зарегистрированных клиентов.
     * @return array
     */
    static function get_users($onPage = null)
    {
        if ($onPage !== null) {
            $count = (int)DB::get_field('cnt', 'SELECT COUNT(u.id) AS cnt FROM clients AS u');

            Pagination::setValues($count, $onPage);

            Pagination::setTemplateSettings();
        }

        $query = 'SELECT u.id,
                         u.name,
                         u.surname,
                         u.patronymic,
                         u.phone,
                         u.email,
                         sr.name as region
                  FROM clients AS u
                  LEFT JOIN shipping_regions AS sr ON sr.id = u.region_id
                  ORDER BY u.surname,
                           u.name,
                           u.patronymic,
                           u.email ' .
            ($onPage !== null ? Pagination::sqlLimit() : '');

        return DB::get_rows($query);
    }

    /**
     * Return client info by client ID.
     * @param int $id Client ID.
     * @return array Array of clients info fields.
     */
    static function get_client_by_id($id)
    {
        $id = (int)$id;

        $query = 'SELECT u.id,
                         u.name,
                         surname,
                         patronymic,
                         email,
                         phone,
                         region_id,
                         address,
                         inn,
                         type_id,
                         sr.name AS region_name
                  FROM clients AS u
                  LEFT JOIN shipping_regions AS sr ON sr.id = u.region_id
                  WHERE u.id = ' . $id;

        return DB::get_row($query);
    }

    /**
     * Возвращает информацию о клиенте по SID
     * @return array
     */
    static function get_user_info()
    {
        return !empty($_SESSION['user']['id']) ? DB::get_row('SELECT id, name, surname, patronymic, email, phone, address, region_id, main_discount, auto_discount FROM clients WHERE id = ' . $_SESSION['user']['id']) : [];
    }

    /**
     * Add new client.
     * @param string $email
     * @param string $password
     * @param int $vkID Vk.com user ID
     * @param int $fbID Facebook user ID
     * @param int $okID Odnoklassniki user ID
     *
     * @return int
     */
    static function insert_user($email, $password, $vkID = 0, $fbID = 0, $okID = 0)
    {
        $hash = self::generate_hash();

        $query = "INSERT INTO clients (password, email, secure_cid, auto_discount, vk_id, fb_id, ok_id)
                  VALUES ('" . DB::escape($password) . "', LOWER('" . DB::escape($email) . "'), '" . DB::escape($hash) . "', 2, " . ((int)$vkID) . ", " . ((int)$fbID) . ", " . ((int)$okID) . ")";

        unsubscribe::add_self($email, 'self', 'roskosmetika.ru');

        return DB::query($query) ? DB::get_last_id() : 0;
    }

    /**
     * Updates vk.com ID
     *
     * @param int $clientID Client ID
     * @param string $networkKey Social network key ('vk', 'fb', 'ok')
     * @param int $socialID Social network user ID
     */
    public static function set_social_id($clientID, $networkKey, $socialID)
    {
        DB::query('UPDATE clients SET ' . ($networkKey === 'vk' ? 'vk_id' : ($networkKey === 'fb' ? 'fb_id' : 'ok_id')) . ' = ' . ((int)$socialID) . ' WHERE id = ' . (int)$clientID);
    }

    /**
     * Проверяет есть ли клиент с $email и возвращает id клиента если есть email
     * @param string $email
     * @return integer Id клиента.
     */
    static function check_email($email)
    {
        $email = DB::mysql_secure_string($email);

        $query = "SELECT id
                  FROM clients
                  WHERE email = LOWER('$email')";

        return (int)DB::get_field('id', $query);
    }

    /**
     * Выход авторизованного клиента.
     *
     * @return bool
     */
    static function logout()
    {
        $orderID = Cart::orderID();

        Users::update_session();

        if ($orderID) {
            list(, $newHash) = Cart::clone_order($orderID);
            
            Users::update_session(0, $newHash);
        }
    }

    /**
     * Сохраняет информацию о клиенте.
     * @param integer $user_id
     * @param string $name
     * @param string $surname
     * @param string $patronymic
     * @return boolean
     */
    static function update_info($user_id, $name, $surname, $patronymic = null)
    {
        $user_id = (int)$user_id;

        $query = 'UPDATE clients
                  SET name = "' . DB::escape($name) . '",
                      surname = "' . DB::escape($surname) . '"' .
                      ($patronymic !== null ? ', patronymic = "' . DB::escape($patronymic) . '"' : '') .
                  ' WHERE id = ' . (int)$user_id;

        unsubscribe::add_self(DB::get_field('email', "SELECT email FROM clients WHERE id = $user_id"), 'self', 'roskosmetika.ru', $name, $surname, $patronymic);

        return DB::query($query);
    }

    /**
     * Сохраняет информацию о доставки клиента.
     * @param integer $user_id
     * @param string $phone
     * @param string $region
     * @param string $address
     * @return boolean
     */
    static function update_delivery($user_id, $phone, $region, $address)
    {
        $user_id = (int)$user_id;

        if (!get_magic_quotes_gpc()) {
            $phone = DB::mysql_secure_string($phone);
            $region = DB::mysql_secure_string($region);
            $address = DB::mysql_secure_string($address);
        }

        $query = "UPDATE clients
                  SET phone = '$phone',
                      address = '$address',
                      region_id = '$region'
                  WHERE id = $user_id";

        return DB::query($query);
    }

    /**
     * Возвращате пароль пользователя.
     * @param integer $user_id
     * @return string
     */
    static function get_password($user_id)
    {
        $user_id = (int)$user_id;

        $query = "SELECT password
                  FROM clients
                  WHERE id = $user_id";

        return DB::get_field('password', $query);
    }

    /**
     * Сохраняет пароль клиента.
     * @param integer $user_id
     * @param string $password
     * @return boolean
     */
    static function update_password($user_id, $password)
    {
        $user_id = (int)$user_id;

        if (get_magic_quotes_gpc()) {
            $password = DB::mysql_secure_string($password);
        }

        $query = "UPDATE clients
                  SET password = '$password'
                  WHERE id = $user_id";

        return DB::query($query);
    }

    /**
     * Получение email клиента по id.
     * @param integer $user_id
     */
    static function get_email($user_id)
    {
        $user_id = (int)$user_id;


        $query = "SELECT email
                  FROM clients
                  WHERE id = $user_id";

        return DB::get_field('email', $query);
    }


    /**
     * Получение информации о скидках клиента по id.
     * @param integer $user_id
     * @return array массив скидок клиента
     */
    static function get_discount($user_id)
    {
        $user_id = (int)$user_id;

        $query = "SELECT main_discount, auto_discount
                  FROM clients
                  WHERE id = $user_id";

        return DB::get_row($query);
    }

    /**
     * Return user's email 
     * @return string return md5 or empty string
     */
    static function session_email() {

        $return = FALSE;

        $id = $_SESSION['user']['id'] ?: null;

        if ($id === null) {
            return '';
        }

        // find in base
        $email = DB::get_field('email', "SELECT email FROM clients WHERE id = " . $id);

        if (!$email) {
            $email = DB::get_row('SELECT c.email, c.client_id FROM rk_credentials AS c WHERE c.client_id = ' . $id);

            if ($email['email'] == '' || empty($email['email'])) {
                if ($email['client_id'] > 0) {
                    $email = self::get_email($email['client_id']);

                    if ($email) $return = $email;
                }
            } else {
                $return = $email['email'];
            }
        } else {
           $return = $email; 
        }

        // find in SESSION
        if (!$return && isset($_SESSION['email'])) $return = $_SESSION['email'];

        return ($return ? md5(mb_strtolower($return)) : '');
    }

    /**
     * Updates user credentials
     *
     * @param string $key Field name
     * @param string $value New value
     * @param int $orderID Order ID
     * @param string $sessionUserID User ID stored in session
     */
    public static function updateCredentials($key, $value, $orderID, $sessionUserID)
    {
        $existingUserID = DB::get_field('client_id', 'SELECT c.client_id FROM rk_credentials AS c WHERE c.order_id = ' . $orderID);

        if ($existingUserID === null) {
            DB::query('INSERT INTO rk_credentials (client_id, order_id, `name`, email, phone) VALUES (0, ' . $orderID . ', "", "", "")');

            $existingUserID = 0;
        }

        $userID = (int)($sessionUserID ?: $existingUserID);

        DB::query('UPDATE rk_credentials SET client_id = ' . $userID . ', `' . $key . '` = "' . DB::mysql_secure_string($value) . '" WHERE order_id = ' . $orderID);
    }

    /**
     * Validates user hash
     *
     * @param string $hash Hash value
     *
     * @return bool
     */
    public static function validate_hash($hash)
    {
        return !!preg_match('/^[a-zA-Z0-9]{35}$/', $hash);
    }

    /**
     * Returns client row based on secure hash
     *
     * @param string $hash Client secret hash
     *
     * @return array|null
     */
    public static function get_by_hash($hash)
    {
        return self::validate_hash($hash) ? DB::get_row('SELECT * FROM clients AS c WHERE c.secure_cid ="' . DB::mysql_secure_string($hash) . '" LIMIT 1') : null;
    }

    /**
     * Generates unique user secure hash
     *
     * @param bool $checkOrders Check orders too
     *
     * @return string
     */
    public static function generate_hash($checkOrders = true)
    {
        do {
            $hash = random_string(35);
        } while (
            !!DB::get_row('SELECT c.id FROM clients AS c WHERE c.secure_cid = "' . $hash . '" LIMIT 1')
            || $checkOrders && !!DB::get_row('SELECT o.id FROM rk_orders AS o WHERE o.secure_cid = "' . $hash . '" LIMIT 1')
        );

        return $hash;
    }

    /**
     * Updates user session variables ($_SESSION['user'])
     *
     * @param int $userID User ID. If empty - resets user ID
     * @param string $hash User hash
     */
    public static function update_session($userID = null, $hash = null)
    {
        if ($userID === null) {
            $userID = 0;
            $orderID = 0;
            $hash = '';
        } else {
            $userID = (int)$userID;

            if ($userID) {
                $hash = DB::get_field('secure_cid', 'SELECT c.secure_cid FROM clients AS c WHERE c.id = ' . $userID . ' LIMIT 1');
            } else {
                $hash = $hash && self::validate_hash($hash) ? $hash : '';
            }

            $orderID = $userID ? Cart::get_order_id_by_client_id($userID) : ($hash ? Cart::get_order_by_hash($hash) : 0);
        }

        $_SESSION['user'] = [
            'id' => $userID,
            'order_id' => $orderID,
        ];

        setcookie(Users::HASH_KEY, $hash, $_SERVER['REQUEST_TIME'] + ($hash ? 99999999 : -100000), '/');

        $_COOKIE[Users::HASH_KEY] = $hash;
    }

    /**
     * Returns client by email
     *
     * @param string $email User email
     *
     * @return array
     */
    public static function get_by_email($email)
    {
        return DB::get_row('SELECT * FROM clients AS c WHERE c.email = "' . DB::mysql_secure_string($email) . '" LIMIT 1');
    }

    /**
     * Returns client by email
     *
     * @param string $phone User phone number
     *
     * @return array
     */
    public static function get_by_phone($phone)
    {
        return DB::get_row('SELECT * FROM clients AS c WHERE c.email = "' . DB::mysql_secure_string($phone) . '" LIMIT 1');
    }

    /**
     * Creates new client
     *
     * @param string $phone Client phone number
     * @param string $name Client name
     * @param string $surname Client surname
     * @param string $patronymic Client patronymic
     *
     * @return int Created client ID
     */
    public static function create($phone, $name, $surname, $patronymic)
    {
        $hash = self::generate_hash();

        DB::query('INSERT INTO clients (phone, name, surname, patronymic, secure_cid) VALUES ("' . DB::mysql_secure_string($phone) . '", "' . DB::mysql_secure_string($name) . '", "' . DB::mysql_secure_string($surname) . '", "' . DB::mysql_secure_string($patronymic) . '", "' . $hash . '")');

        return DB::get_last_id();
    }

    /**
     * Check and change flag TMs only for professional
     *
     * @param int $user_id User ID from session
     * 
     * @return array true/false for two parameters
     */
    public static function tm_prof_backend($user_id) {

        //  return parametrs
        $return = array('tm_for_prof' => FALSE, 
                        'real_prof'   => !empty($_COOKIE['prof']));


        if(isset($_COOKIE['prof_manual'])) {

            // 1 for prof, 2 for all
            $return['tm_for_prof'] = ($_COOKIE['prof_manual'] == 1 ? TRUE : FALSE);

        } else {

            if(isset($_COOKIE['prof'])) {

                // 1 for prof, 0 for all
                $return['tm_for_prof'] = ($_COOKIE['prof'] == 1 ? TRUE : FALSE);

            } else {

                $current_value = 0;

                if (!$user_id && isset($_COOKIE[Users::HASH_KEY]) && check::sid($_COOKIE[Users::HASH_KEY])){

                    $user_id = (int)DB::get_field('id', "SELECT id FROM clients WHERE secure_cid = '" . DB::mysql_secure_string($_COOKIE[Users::HASH_KEY]) . "' LIMIT 1" );

                    if (!$user_id) {

                        $user_id = (int)DB::get_field('id', "SELECT c.id, c.type_id
                                                             FROM  rk_orders ro, orders o, clients c
                                                             WHERE ro.order_number = o.order_number
                                                             AND o.client_id = c.id
                                                             AND  ro.secure_cid = '" . DB::mysql_secure_string($_COOKIE[Users::HASH_KEY]) . "' LIMIT 1" );
                    } 
                }


                if ($user_id) {

                    $user = self::get_client_by_id($user_id);
                    $current_value = ($user['type_id'] == self::CLIENT_TYPE_INDIVIDUAL ? 0 : 1);
                    setcookie('prof', $current_value, time()+60*60*24*180, '/', $_SERVER['HTTP_HOST'], false, true);

                } 

                $return['tm_for_prof'] = ($current_value == 1 ? TRUE : FALSE);
            }
        }

        return $return;
    }

}
