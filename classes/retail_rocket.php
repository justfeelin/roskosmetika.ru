<?php

/**
 * Work with retail_rocket
 */
class retail_rocket
{
    /**
     * js rocket code for product page
     * @param  array $product product info
     * @return string  js code in string
     */
    static function product_page($product)
    {

        return " rrApiOnReady.push(function() {try{ rrApi.view($product); } catch(e) {} })";
    }

    /**
     * js rocket code for category page
     * @param  int $cat_id_1 ID category lvl 1
     * @param  int $cat_id_2 ID category lvl 2
     * @param  int $cat_id_3 ID category lvl 3
     * @return string  js code in string
     */
    static function category_page($cat_id_1, $cat_id_2, $cat_id_3)
    {
        $cat_id = $cat_id_1;

        if ($cat_id_2) $cat_id = $cat_id_2;
        if ($cat_id_3) $cat_id = $cat_id_3;

        return " rrApiOnReady.push(function() {try { rrApi.categoryView($cat_id); } catch(e) {} })";
    }

    /**
     * retail rocket for search
     * @param  string $serch_string Search string
     * @return string  js code in string
     */
    static function search_result($serch_string)
    {
        return ("rrApiOnReady.push(function() {try { rrApi.search(\"$serch_string\"); }catch(e) {}});");    
    }

    /**
     * Generate rocket tracking code for order
     * @param  int    $order_number Order number
     * @param  array  $order_items  array of order items
     * @return string               JS code 
     */
    static function ok_page($order_number, $order_items) 
    {

        $return = "rrApiOnReady.push(function() {try{ rrApi.order({transaction: $order_number, items: [";

        $quan = count($order_items);  

        for ($i=0; $i < $quan; $i++) { 
            $return .= "{ id: {$order_items[$i]['id']}, qnt: {$order_items[$i]['quantity']}, price: {$order_items[$i]['price']} }";
            $return .= (($quan - 1) == $i) ? '' : ',';
          }  

        $return .= ']});} catch(e) {}})';
      
        return $return;
    }
}

?>