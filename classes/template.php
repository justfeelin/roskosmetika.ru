<?php

/**
 * Templates.
 * @author Vadim
 * @todo Make catch managment.
 * @todo Make reserved var verification.
 * @todo Adding inline scripts to template.
 */
class Template
{
    /**
     * @var string Template files extension
     */
    public static $ext = 'php';
    /**
     * Path to css location.
     * @var string
     */
    static $css_path = '/css/';

    /**
     * Path to javascripts location
     * @var string
     */
    static $javascripts_path = '/js/';

    /**
     * Path to bower
     */
    static $bower_path = "/bower_components/";

    /**
     * Path to images.
     * @var string
     */
    static $images_path = '/images/';

    /**
     * Default page charset.
     * @var string
     */
    static $defaul_page_charset = 'utf-8';

    /**
     * Reserved variables names.
     * @var array
     */
    static $reserved_vars_name = array('charset', 'javascripts_path', 'css_path', 'images_path', 'bower_path');

    /**
     * @var string UTM source name
     */
    public static $utm_source = null;

    /**
     * @var string UTM medium name
     */
    public static $utm_medium = null;

    /**
     * @var string UTM campaign name
     */
    public static $utm_campaign = null;

    /**
     * @var string UTM term name
     */
    public static $utm_term = null;

    /**
     * @var array Array of events to be passed to `yaCounter{number}.reachGoal()`
     */
    private static $_yandexEvents = array();

    /**
     * @var array Array of events to be passed to `_gaq.push()`
     */
    private static $_googleEvents = '';

    /**
     * @var string html code to inset into layout
     */
    private static $_html_code = '';

    /**
     * @var string js admitad code (ReTag and counter)
     */
    private static $_admitad_code = '';

    /**
     * @var string js my target code (ReTag and counter)
     */
    private static $_mytarget_code = '';

    /**
     * @var string js Retail Rocket code
     */
    private static $_rocket_code = '';

    /**
     * @var string js Criteo code
     */
    private static $_criteo_code = '';

     /**
     * @var string js VK-retargeting code
     */
    private static $_vk_code = '';

    /**
     * @var string Directory name containing template files
     */
    private static $_templates_dirname = '';

    /**
     * @var string Full directory containing template files
     */
    private static $_templates_dir = '';

    /**
     * @var array View variables
     */
    private static $_variables = [];

    /**
     * Initialization and adding constants to template.
     * @var string Google tag manager code
     */
    private static $_gtm_code = '';

    /**
     * Code for inboxer OK page
     * @var order information
     */
    private static $_inboxer_code = '';

    /**
     * Smarty initialization and adding constants to template.
     *
     * @param string $dir Templates dir
     */
    static function init($dir = 'templates')
    {
        self::set('css', []);
        self::set('scripts', []);

        self::setPaths($dir);

        self::set('javascripts_path', self::$javascripts_path);
        self::set('bower_path', self::$bower_path);
        self::set('css_path', self::$css_path);
        self::set('images_path', self::$images_path);
        self::set('charset', self::$defaul_page_charset);

        self::set('incartlink', !empty($_SERVER['REQUEST_URI']) && substr($_SERVER['REQUEST_URI'], 0, 5) === '/cart' ? ' target="_blank" ' : '');
    }

    /**
     * Sets templates dir & compile prefix
     *
     * @param string $dir Templates dir name
     */
    public static function setPaths($dir)
    {
        self::$_templates_dirname = $dir;
        self::$_templates_dir = SITE_PATH . $dir . DIRECTORY_SEPARATOR;
    }

    /**
     * Передача переменной в шаблон
     * @param string $name Название переменной.
     * @param mixed $value Значение переменной.
     */
    static function set($name, $value)
    {
        self::$_variables[$name] = $value;
    }

    /**
     * Returns view variable if set
     *
     * @param string $name Variable name
     *
     * @return mixed|null
     */
    public static function get($name)
    {
        return isset(self::$_variables[$name]) ? self::$_variables[$name] : null;
    }

    /**
     * Removes view variable
     *
     * @param string $name Variable name
     */
    static function clear($name)
    {
        unset(self::$_variables[$name]);
    }

    /**
     * Adds value to view variable of type array
     *
     * @param string $name Array variable name
     * @param mixed $value New value to add
     */
    static function append($name, $value)
    {
        self::$_variables[$name] = isset(self::$_variables[$name]) ? array_merge((array)self::$_variables[$name], [$value]) : [$value];
    }

    /**
     * Задает шаблон страницы, заголовок, контент.
     * @param string $page
     * @param string $header
     * @param mixed $content
     */
    static function set_page($page, $header = null, $content = null)
    {
        self::set('page', $page);

        if (!empty($header)) {
            self::set('header', $header);
        }

        if (!empty($content)) {
            self::set('content', $content);
        }
    }

    /**
     * Задает описание страницы.
     * @param string $title
     * @param string $description
     * @param string $keywords
     * @param string $page_descr
     * @param array  $open_graph Open Graph data array
     */
    static function set_description($title, $description, $keywords, $page_descr = null, $open_graph = null)
    {
        self::set('title', $title);
        self::set('description', $description);
        self::set('keywords', $keywords);
        self::set('page_descr', $page_descr);
        self::set('open_graph', $open_graph);
    }

    /**
     * Передает параметры формы.
     * @param array $values Значение полей.
     * @param array $msg Сообщения.
     */
    static function set_form($values = null, $msg = null)
    {
        self::set('values', $values);
        self::set('msg', $msg);
    }

    /**
     * Setting page charset.
     * @param string $charset Charset name.
     */
    static function set_charset($charset)
    {
        self::set('charset', $charset);
    }

    /**
     * Adding JavaScript to template.
     * @param string $file_name JavaScript file name.
     */
    static function add_script($file_name)
    {
        self::append('scripts', $file_name);
    }

    /**
     * Adding CSS file to template.
     *
     * @param string $file_name CSS file name.
     */
    static function add_css($file_name)
    {
        self::append('css', $file_name);
    }

    /**
     * Выводти шаблон.
     * @param string $layout Название выводимого шаблона.
     */
    static function output($layout)
    {
        self::partial($layout, true);
    }

    /**
     * Возвращает шаблон в переменную
     *
     * @param string $templateFile
     * @param string $header
     * @param mixed $content
     *
     * @return string
     */
    static function get_tpl($templateFile, $header = null, $content = null)
    {
        if ($header !== null) {
            self::set('header', $header);
        }

        if ($content !== null) {
            $oldContent = self::get('content');

            self::set('content', $content);
        }

        unset($header, $content);

        ob_start();

        self::partial($templateFile, true);

        $output = ob_get_contents();

        ob_end_clean();

        isset($oldContent) && self::set('content', $oldContent);

        return $output;
    }

    /**
     * Adds event to yandex events list
     *
     * @param string $name Event code name
     * @param mixed $data Event data (`yaParams`)
     */
    public static function addYandexEvent($name, $data = null)
    {
        self::$_yandexEvents[] = array($name, $data);
    }

    /**
     * Adds event to google events list
     *
     * @param mixed $data Event data (Will be passed to `_gaq.push()`)
     */
    public static function addGoogleEvent($data, $last = FALSE)
    {
        self::$_googleEvents .= 'ga(' . $data . ')' . ($last ? '' : ';');
    }

    /**
     * Add or update admitad js code in string
     * @param string $data js code in string to add or update
     */
    public static function add_admitad_code($data)
    {
        self::$_admitad_code .= $data;
    }

    /**
     * Add or update addigital js code in string
     * @param string $data js code in string to add or update
     */
    public static function add_mytarget_code($data)
    {
        self::$_mytarget_code .= $data;
    }

    /**
     * Add or update retail rocket js code in string
     * @param string $data js code in string to add or update
     */
    public static function add_rocket_code($data)
    {
        self::$_rocket_code .= $data;
    }

    /**
     * Add or update criteo js code in string
     * @param string $data js code in string to add or update
     */
    public static function add_criteo_code($data)
    {
        self::$_criteo_code .= $data;
    }

    /**
     * Add or update vk js code in string
     * @param string $data js code in string to add or update
     */
    public static function add_vk_code($data)
    {
        self::$_vk_code .= $data;
    }

    /**
     * Generate OK-page inboxer code
     * @param string $data js code in string to add or update
     */
    public static function add_inboxer_code($data)
    {
        self::$_inboxer_code = $data;
    }

    /**
     * Add or update html code in string
     * @param string $data html code in string
     */
    public static function add_html_code($data)
    {
        self::$_html_code .= $data;
    }

    /**
     * Adds google tag manager ecommerce code
     *
     * @param string $type Event type ('product', 'cart', 'purchase')
     * @param int $total Total amount
     * @param int|array $productIDs Product ID or array of product IDs (For 'product' and 'cart' events)
     */
    public static function add_gtm_code($type, $total, $productIDs = null)
    {
        $data = [
            'ecomm_pagetype' => $type,
            'ecomm_totalvalue' => (int)$total,

        ];

        if ($productIDs !== null) {
            $data['ecomm_prodid'] = $productIDs;
        }

        self::$_gtm_code .= '(window.dataLayer || (window.dataLayer = [])).push(' . json_encode($data) . ');';
    }

    /**
     * Sets `trackingEventsCode` view variable with javascript code for window.yandexEvents and window.googleEvents objects
     */
    public static function setTrackingEvents()
    {
        $codes = array();

        if (self::$_yandexEvents) {
            $codes[] = 'window.yandexEvents = ' . json_encode(self::$_yandexEvents);
        }

        if (self::$_inboxer_code) {
            $codes[] = '(window["gazeAPIready"]=window["gazeAPIready"] || []).push(function(){try{gazeApi.order(' . json_encode(self::$_inboxer_code) . ');} catch(e) {}})';
        }

        if (self::$_googleEvents) {
            $codes[] = self::$_googleEvents;
        }

        if (!empty(self::$_admitad_code)) {
            $codes[] = self::$_admitad_code;
        }

        if (!empty(self::$_mytarget_code)) {
            $codes[] = self::$_mytarget_code;
        }
        
        if (!empty(self::$_rocket_code)) {
            $codes[] = self::$_rocket_code;
        }

        if (!empty(self::$_criteo_code)) {
            $codes[] = self::$_criteo_code;
        }

        if (!empty(self::$_vk_code)) {
            $codes[] = self::$_vk_code;
        }

        if (!empty(self::$_gtm_code)) {
            $codes[] = self::$_gtm_code;
        }

        $codes = ($codes ? implode(";", $codes) : '');

        self::set('trackingEventsCode', $codes);

        if (!empty(self::$_html_code)) {
            self::set('html_code', self::$_html_code);
        }
    }

    /**
     * Returns rendered cart neighbours for using inside mail template
     *
     * @param array $neighbours Neighbours data, product::cart_neighbours() result
     *
     * @return string
     */
    public static function mail_neighbours($neighbours)
    {
        $n = count($neighbours);

        if ($n > 0) {
            $width = 600 / $n;

            $names = $images = $descriptions = '';

            for ($i = 0; $i < $n; ++$i) {
                $names .= '<td width="' . $width . '">' .
                    '<div style="margin: 10px 10px 0">' .
                    '<a style="font-family: Tahoma, Geneva, sans-serif;font-size: 12px;color: #1B1B1B;margin-top: 6px;margin-bottom: 6px;" href="' . DOMAIN_FULL . '/product/' . $neighbours[$i]['url'] . apply_utm(null, 'neighbours-product-name') . '">' . $neighbours[$i]['name'] . '</a>' .
                    '<br>' .
                    '<a style="text-decoration: none;font-family: Tahoma, Geneva, sans-serif;font-size: 12px;color: #ADADAD" href="' . DOMAIN_FULL . '/tm/' . $neighbours[$i]['tm_url'] . apply_utm(null, 'neighbours-brand') . '">' . $neighbours[$i]['tm'] . '</a>' .
                    '</div>' .
                    '</td>';

                $images .= '<td width="' . $width . '">' .
                    '<div style="margin: 6px 10px 0">' .
                    '<a href="' . DOMAIN_FULL . '/product/' . $neighbours[$i]['url'] . apply_utm(null, 'neighbours-product-logo') . '"><img src="' . IMAGES_DOMAIN_FULL . '/images/prod_photo/' . Product::img_url('min_' . $neighbours[$i]['id'] . '.jpg', $neighbours[$i]['url']) . '" width="85" height="85"></a>' .
                    '</div>' .
                    '</td>';

                $descriptions .= '<td width="' . $width . '">' .
                    '<div style="margin: 0 10px">' .
                    '<span style="font-size: 11px;color: #5F5F5F;">' . $neighbours[$i]['pack'] . '</span>' .
                    '<div style="font-size: 13px;color: #1d1d1d;">' .
                    ((int)$neighbours[$i]['special_price'] ? '<span style="text-decoration: line-through">' . formatPrice($neighbours[$i]['price']) . ' р.</span>&nbsp;&nbsp;&nbsp;' : '') .
                    '<span style="' . ((int)$neighbours[$i]['special_price'] ? 'color: #f63e3c;' : '') . '">' . formatPrice($neighbours[$i][(int)$neighbours[$i]['special_price'] ? 'special_price' : 'price']) . ' р.</span>' .
                    '</div>' .
                    '</div>' .
                    '</td>';
            }

            return '<tr><td align="center">' .
                '<h2 style="color: #5A5A5A; font-size: 18px; font-weight: normal; margin: 20px 0 3px; text-align: center;">Рекомендуем посмотреть</h2>' .
                '<table border="0" cellpadding="0" cellspacing="0" width="100%">' .
                '<tr>' .
                $names .
                '</tr>' .
                '<tr>' .
                $images .
                '</tr>' .
                '<tr>' .
                $descriptions .
                '</tr>' .
                '</table>' .
                '</td></tr>';
        }

        return '';
    }

    /**
     * Returns rendered mail template
     *
     * @param string $template Template name to render inside mail layout
     * @param string $header Mail header
     * @param array $content Content
     * @param string $unsubscribeEmail Add unsubscribe link for given E-mail
     *
     * @return string
     */
    public static function mail_template($template, $header, $content = null, $unsubscribeEmail = null)
    {
        $oldPath = self::$_templates_dirname;

        self::setPaths('templates');

        $email = Template::get_tpl('mail_layout', $header, [
            'template' => $template,
            'content' => $content,
            'unsubscribeLink' => $unsubscribeEmail && ($hash = unsubscribe::get_email_hash($unsubscribeEmail))
                ? [
                    'email' => $unsubscribeEmail,
                    'hash' => $hash,
                ]
                : null,
        ]);

        self::setPaths($oldPath);

        return $email;
    }

    /**
     * Returns walletone - compatible form fields HTML
     *
     * @param array $order Order array
     *
     * @return string
     */
    public static function walletoneFormFields($order)
    {
        $cost = floatraw($order['cost']);

        if (!isset($order['ts'])) {
            $order['ts'] = $_SERVER['REQUEST_TIME'];
        }

        $fields = [
            'WMI_CURRENCY_ID' => '643',
            'WMI_DESCRIPTION' => 'BASE64:' . base64_encode('Оплата заказа № Р-' . $order['site_order_number']),
            'WMI_EXPIRED_DATE' => date('c', substr($_SERVER['REQUEST_TIME'] + 30 * 24 * 60 * 60, 0, 19)),
            'WMI_FAIL_URL' => DOMAIN_FULL . '/room/history',
            'WMI_MERCHANT_ID' => env('WALLETONE_MERCHANT_ID'),
            'WMI_PAYMENT_AMOUNT' => $cost,
            'WMI_PAYMENT_NO' => $order['site_order_number'] . '-' . $cost . '-' . $order['ts'],
            'WMI_SUCCESS_URL' => DOMAIN_FULL . '/room/history',
        ];

        $fields['WMI_SIGNATURE'] = base64_encode(pack('H*', md5(implode('', $fields) . env('WALLETONE_KEY'))));

        $fieldsHtml = [];

        foreach ($fields as $key => $val) {
            $fieldsHtml[] = '<input type="hidden" name="' . $key . '" value="' . $val . '"/>';
        }

        return implode("\n", $fieldsHtml);
    }

    /**
     * LinkHider template alias
     *
     * @param string $url Original link
     *
     * @return string
     */
    public static function hidelink($url)
    {
        static $urls = [];

        if (!isset($urls[$url])) {
            $urls[$url] = LinkHider::encode($url);
        }

        return $urls[$url];
    }

    /**
     * Renders partial view
     *
     * @param string $file View filename
     * @param bool|array $variables View variables. If true - appends global view variables, provided variables otherwise
     * @param bool $appendGlobal Append global view variables to provided
     */
    public static function partial($file, $variables = null, $appendGlobal = false)
    {
        if ($variables !== null) {
            extract($variables === true ? self::$_variables : ($appendGlobal ? array_merge($variables, self::$_variables) : $variables));
        }

        include self::$_templates_dir . $file . '.' . self::$ext;
    }

    /**
     * Returns formatted date from date in other format
     *
     * @param string $format Format to be passed to date()
     * @param string|int $date Date in one of date formats or UNIX time
     *
     * @return bool|string
     */
    public static function date($format, $date)
    {
        return date($format, is_numeric($date) ? (int)$date : (strtotime($date) ?: null));
    }

    // /**
    //  * Renders RetailRocket personalized products block with 2 minutes cache
    //  *
    //  * @param string $header Block header value
    //  *
    //  * @return string
    //  */
    // public static function personalized_products($header = null)
    // {
    //     if (!empty($_COOKIE['rcuid'])) {
    //         if (!isset($_SESSION['pers_prods']) || $_SESSION['pers_prods_exp'] <= $_SERVER['REQUEST_TIME']) {
    //             $products = remote_request('https://api.retailrocket.ru/api/2.0/recommendation/personalized/popular/55cdc1e268e9a65c24215c3d?session=' . $_COOKIE['rcuid'], [], false, true);

    //             $pIDs = [];

    //             if (isset($products[0])) {
    //                 $count = 0;

    //                 for ($i = 0, $n = count($products); $i < $n; ++$i) {
    //                     if (Product::get_by_id($products[$i]['ItemId'], true)) {
    //                         $pIDs[] = $products[$i]['ItemId'];

    //                         if (++$count === 40) {
    //                             break;
    //                         }
    //                     }
    //                 }
    //             } else {
    //                 $n = 0;
    //             }

    //             $_SESSION['pers_prods'] = $pIDs;
    //             $_SESSION['pers_prods_exp'] = DEBUG || $n === 0 ? 0 : $_SERVER['REQUEST_TIME'] + 120;
    //         }

    //         if ($_SESSION['pers_prods']) {
    //             $pIDs = $_SESSION['pers_prods'];

    //             if ($pIDs) {
    //                 shuffle($pIDs);

    //                 $products = Product::get_by_id(array_slice($pIDs, 0, 4), true, true, false, false, false);

    //                 Template::partial('personalized_products', [
    //                     'header' => $header,
    //                     'products' => $products,
    //                 ]);
    //             }
    //         }
    //     }
    // }

    /**
     * Renders MindBox personalized products block with 2 minutes cache
     *
     * @param string $header Block header value
     *
     * @return string
     */
    public static function personalized_products($header = null)
    {

        if ((!isset($_SESSION['pers_prods']) || !isset($_SESSION['pers_prods_exp'])) || ($_SESSION['pers_prods_exp'] <= $_SERVER['REQUEST_TIME'])) {

            $id_list_order = agregation_products::get_personal_product_list(helpers::search_user_id(), 2, 6);
            $id_list_viz = agregation_products::get_personal_product_list(helpers::search_user_id(), 1, 6);

            //$pIDs = array_merge(mindbox::get_recomendation('PoZakazam6', FALSE, 6), mindbox::get_recomendation('PoProsmotram6', FALSE, 6));
            $pIDs = array_merge($id_list_order, $id_list_viz);

            $n = count($pIDs);

            if (empty($pIDs)) {
                $n = 0;
            }

            $_SESSION['pers_prods'] = $pIDs;
            $_SESSION['pers_prods_exp'] = DEBUG || $n === 0 ? 0 : $_SERVER['REQUEST_TIME'] + 120;
        }

        if ($_SESSION['pers_prods']) {
            $pIDs = $_SESSION['pers_prods'];

            if ($pIDs) {
                shuffle($pIDs);

                $products = Product::get_by_id(array_slice($pIDs, 0, 4), true, true, false, false, false);

                Template::partial('personalized_products', [
                    'header' => $header,
                    'products' => $products,
                ]);
            }
        }

    }
}