<?php

/**
 * Working with day items.
 */
class day_items
{

    /**
     * Return day_item.
     * @return array array of dqy item info
     */
    static function get_day_item()
    {

        $query = "SELECT p.id, p.main_id, p.price, p.special_price, p.new, p.available, p.pack, di.tag, p._discount AS sale, UNIX_TIMESTAMP(di.end) end,
                         pp.src, pp.alt, pp.title photo_title   
                  FROM rk_day_item AS di, products AS p
                  LEFT JOIN rk_prod_photo pp  ON ( pp.id = p.id )
                  WHERE di.begin >= DATE_FORMAT(NOW(), '%Y-%m-%d')
                  AND di.end > NOW()
                  AND di.prod_id = p.id
                  AND p.visible = 1
                  AND p.active = 1
                  AND p.available = 1
                  ORDER BY di.begin ASC
                  LIMIT 1";

        $day_item = DB::get_row($query);

        if (!$day_item) {
            $day_item = DB::get_row(
                'SELECT p.id, p.main_id, p.price, p.special_price, p.new, p.available, p.pack, "Товар<br>дня" AS tag, p._discount AS sale, UNIX_TIMESTAMP(NOW() + INTERVAL 1 DAY) end,
                    pp.src, pp.alt, pp.title photo_title
                FROM products AS p
                INNER JOIN rk_prod_photo AS pp ON pp.id = p.id AND pp.src != "none.jpg"
                WHERE p.main_id = 0 AND p.available = 1 AND p.visible = 1 AND p.active = 1 AND p.special_price > 0 AND p.main_stock = 1
                ORDER BY RAND()
                LIMIT 1'
            );
        }

        if (!$day_item) {

            $query = "SELECT p.id, p.main_id, p.price, p.special_price, p.new, p.available, p.pack, 'Товар<br>дня' AS tag, p._discount AS sale, UNIX_TIMESTAMP( NOW() + INTERVAL 1 DAY) end,
                             pp.src, pp.alt, pp.title photo_title   
                      FROM  products AS p
                      LEFT JOIN rk_prod_photo pp  ON ( pp.id = p.id )
                      WHERE p.id = 182
                      AND p.visible = 1
                      AND p.active = 1
                      LIMIT 1";

            $day_item = DB::get_row($query);
        }

        $main_id = $day_item['id'];

        if ($day_item['main_id']) $main_id = $day_item['main_id'];

        $spec_info = DB::get_row("SELECT p.name, p.short_description, IF( p.url =  '', p.id, p.url ) url,  tm.name tm, IF(tm.url =  '', tm.id, tm.url ) tm_url
                                  FROM tm, products AS p
                                  WHERE p.id = $main_id
                                  AND   p.visible = 1
                                  AND   p.active = 1
                                  AND   p.tm_id = tm.id
                                  LIMIT 1");

        $day_item['name'] = $spec_info['name'];
        $day_item['short_description'] = $spec_info['short_description'];
        $day_item['url'] = $spec_info['url'];
        $day_item['tm'] = $spec_info['tm'];
        $day_item['tm_url'] = $spec_info['tm_url'];

        return $day_item;
    }

    /**
     * Show last items and new for add
     * @param  integer $days interval of new items
     * @return array         array of items
     */
    static function items_to_add($days = 21)
    {

        $query = 'SELECT *
                FROM (SELECT * FROM rk_day_item AS di WHERE di.prod_id > 0 AND di.sale > 0 AND di.begin >= DATE_FORMAT(NOW(), "%Y-%m-%d")) AS result
                ORDER BY end';

        $exist_items = DB::get_rows($query);

        $max_unix_end = DB::get_field('max_end', 'SELECT UNIX_TIMESTAMP(MAX(end)) AS max_end FROM rk_day_item');

        for ($i = 0, $n = count($exist_items); $i < $days; $i++) {

            $begin = date("Y-m-d", $max_unix_end + 3600 * 24 * $i) . ' 00:00:00';
            $end = date("Y-m-d", $max_unix_end + 3600 * 24 * ($i + 1)) . ' 00:00:00';

            $exist_items[$n + $i]['id'] = '';
            $exist_items[$n + $i]['prod_id'] = '';
            $exist_items[$n + $i]['name'] = '';
            $exist_items[$n + $i]['tag'] = '';
            $exist_items[$n + $i]['sale'] = '';
            $exist_items[$n + $i]['begin'] = $begin;
            $exist_items[$n + $i]['end'] = $end;
        }

        return $exist_items;
    }

    /**
     * [add_day_item description]
     * @param date $begin Date of begin
     * @param date $end Date of end
     * @param int $prod_id Id of sale product
     * @param int $sale Sale percent
     * @param string $tag Sale tag
     */
    static function add_day_item($begin, $end, $prod_id, $sale, $tag)
    {

        $exist = (int)DB::get_field('id', "SELECT id FROM rk_day_item WHERE begin = '$begin' AND end = '$end'");

        if ($exist) {

            $query = "UPDATE rk_day_item
                    SET prod_id = $prod_id,
                        name = DATE_FORMAT(NOW(),'%Y.%m.%d %W'),
                        sale = '$sale',
                        tag = '$tag'
                    WHERE id = $exist";

            return DB::query($query);

        } else {

            $query = "INSERT INTO rk_day_item (id, prod_id, name, tag, sale, begin, end)
                    VALUES (0, $prod_id, DATE_FORMAT(NOW(),'%Y.%m.%d %W'), '$tag', $sale, '$begin', '$end')";

            return DB::query($query);
        }

    }

}

?>