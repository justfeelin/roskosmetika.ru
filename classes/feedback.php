<?php

/**
 * Работа с обратной связью
 */
class feedback
{

    /**
     * Добавляет новый запрос.
     * @param string $email Обратный email.
     * @param string $msg Текст сообщения.
     */
    static function add($email, $msg)
    {
        $query = "INSERT INTO rk_feedback (email, message, date, source)
                  VALUE ('" . DB::escape($email) . "', '" . DB::escape($msg) . "', NOW(), 'roskosmetika.ru')";

        unsubscribe::add_self($email, 'self', 'roskosmetika.ru');

        return DB::query($query);
    }

    /**
     * Выборка списка feedback-ов
     *
     * @param int $onPage
     *
     * @return array
     */
    static function get_list($onPage = null)
    {
        if ($onPage !== null) {
            $count = (int)DB::get_field('cnt', 'SELECT COUNT(f.id) AS cnt FROM rk_feedback AS f ');

            Pagination::setValues($count, $onPage);

            Pagination::setTemplateSettings();
        }

        $query = "SELECT id,
                         email,
                         message,
                         date,
                         source
                  FROM rk_feedback
                  ORDER BY date " .
            ($onPage !== null ? Pagination::sqlLimit() : '');
        return DB::get_rows($query);
    }

}
