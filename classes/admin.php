<?php

class admin
{
    /**
     * Root access
     */
    const RIGHTS_ALL = 1;

    /**
     * Common admin pages
     */
    const RIGHTS_COMMON = 2;

    /**
     * Upload newsletter images
     */
    const RIGHTS_NEWSLETTER_IMAGES = 4;

    /**
     * SEO pages
     */
    const RIGHTS_SEO = 8;

    /**
     * Tags products pages
     */
    const RIGHTS_STATISTICS = 16;

    /**
     * Promo codes pages
     */
    const RIGHTS_PROMO_CODES = 32;

    /**
     * Product certificates pages
     */
    const RIGHTS_PRODUCT_CERTIFICATES = 64;

    /**
     * Product certificates pages
     */
    const RIGHTS_TAGS_EDITOR = 128;


    /**
     * Access token cookie key
     */
    const ACCESS_TOKEN = '_aatkn';

    /**
     * @var string Password hashing salt
     */
    private static $_passwordSalt = 'rk2.0admin';

    /**
     * Checks for login request by username & password or access token
     *
     * @return array [`Rights mask`, `Access token`]
     */
    public static function check_login()
    {
        $rights = 0;

        $accessToken = !empty($_COOKIE[self::ACCESS_TOKEN]) ? $_COOKIE[self::ACCESS_TOKEN] : null;

        if (
            $accessToken
            || (
                !empty($_POST['username'])
                && !empty($_POST['password'])
            )
        ) {
            $row = DB::get_row('SELECT a.rights, a.access_token FROM rk_admin AS a WHERE ' . ($accessToken ? 'a.access_token = "' . DB::escape($accessToken) . '"': 'a.username = "' . DB::escape($_POST['username']) . '" AND a.password = "' . sha1(self::$_passwordSalt . $_POST['password']) . '"'));

            $rights = (int)$row['rights'];
            $accessToken = $row['access_token'];
        }

        return $rights ? [
            $rights,
            $accessToken,
        ] : null;
    }

    /**
     * Returns whether user has access to certain pages
     *
     * @param int $rights Access mask to check
     *
     * @return bool
     */
    public static function check_access($rights)
    {
        return !empty($_SESSION['admin']) ? ($_SESSION['admin'] === self::RIGHTS_ALL || $_SESSION['admin'] & $rights) : false;
    }

    /**
     * Products search logic
     *
     * @param callable $addCallback Callback to be called on products add submit
     * @param callable $showAdded Function to be called to return previously added products rows
     * @param callable $removeProducts Function to be called to remove product links
     * @param callable $copyFromTags Function to be called to copy products from tags. Will receive tag IDs array
     * @param array $types Array of filter types to show. Values: "search", "categories", "tags", "filters", "tms", "lines", "added", "certificates"
     * @param bool $showIngredients Show products ingredients
     */
    public static function products_search_form($addCallback, $showAdded, $removeProducts, $copyFromTags = null, $types = null, $showIngredients = false)
    {
        if ($types === null) {
            $types = [
                'search',
                'categories',
                'tags',
                'filters',
                'tms',
                'lines',
                'added',
            ];
        }

        if ($copyFromTags) {
            if (!empty($_POST['do_copy_tags'])) {
                if (!empty($_POST['copy_tags']) && is_array($_POST['copy_tags'])) {
                    $copyFromTags($_POST['copy_tags']);

                    $copied = true;
                } else {
                    $copied = false;
                }

                json_headers();

                print json_encode([
                    'copied' => $copied,
                ]);

                exit(0);
            }
        }

        if (in_array('search', $types)) {
            if (!empty($_POST['products_search'])) {
                $productIDs = search::search_by_params($_POST['products_search']);

                $products = $productIDs ? Product::get_by_id($productIDs, false, false, false, $_POST['products_search'], false) : [];

                $certificates = in_array('certificates', $types);

                if ($products && $certificates) {
                    for ($i = 0, $n = count($products); $i < $n; ++$i) {
                        $products[$i]['certificates'] = DB::get_rows(
                            'SELECT c.id, c.folder_url
                            FROM cdb_cert_prod AS cp
                            INNER JOIN cdb_certificates AS c ON c.id = cp.cert_id
                            WHERE cp.prod_id = ' . (int)$products[$i]['id']
                        );
                    }
                }

                json_headers();

                print json_encode([
                    'html' => Template::get_tpl('products_search_products', '', [
                        'products' => $products,
                        'showIngredients' => $showIngredients,
                        'certificates' => $certificates,
                    ]),
                ]);

                exit(0);
            }
        }

        if ($showAdded) {
            if (!empty($_POST['show_added'])) {
                json_headers();

                print json_encode([
                    'html' => Template::get_tpl('products_search_added_form', '', [
                        'products' => $showAdded(),
                    ]),
                ]);

                exit(0);
            }
        }

        if ($removeProducts) {
            if (isset($_POST['remove_products'])) {
                $removeProducts($_POST['remove_products']);

                redirect($_SERVER['REQUEST_URI']);
            }
        }

        if (isset($_POST['add_products'])) {
            $addCallback($_POST['add_products']);

            redirect($_SERVER['REQUEST_URI']);
        }

        Template::add_script('admin/products_search.js');

        Template::add_css('../assets/jquery-select2/select2.css');
        Template::add_css('../assets/jquery-select2/select2-bootstrap.css');
        Template::add_script('../assets/jquery-select2/select2.min.js');

        if (!$copyFromTags) {
            $n = array_search('tags', $types);

            if ($n !== false) {
                array_splice($types, $n, 1);
            }
        }

        if (!$showAdded || !$removeProducts) {
            $n = array_search('added', $types);

            if ($n !== false) {
                array_splice($types, $n, 1);
            }
        }

        Template::set(
            'products_search',
            cache_item(
                'admin_products_search_params',
                function () {
                    return [
                        'categories' => categories::get_tree(false, false, false),
                        'tags' => categories::get_tags_tree(false),
                        'filters' => filters::get_filters('prod'),
                        'tms' => tm::get_list(false, false, false, false),
                        'lines' => DB::get_rows(
                            'SELECT l.id, l.name,
                            t.name AS tm_name
                        FROM rk_lines AS l
                        INNER JOIN tm AS t ON t.id = l.tm_id
                        ORDER BY t.name'),
                    ];
                },
                300// 5 minutes
            ) + [
                'filter_types' => $types,
            ]
        );
    }

    /**
     * Returns admin menu items
     *
     * @return array
     */
    public static function menu()
    {
        return [
            [
                'name' => 'Импорт/экпорт',
                'access' => admin::RIGHTS_COMMON,
                'items' => [
                    [
                        'url' => '/admin/import/',
                        'name' => 'Импорт файлов',
                    ],
                    [
                        'url' => '/admin/export/',
                        'name' => 'Экспорт файлов',
                    ],
                    [
                        'url' => '/admin/unsubscribe/',
                        'name' => 'Рассылка',
                    ],
                    [
                        'url' => '/admin/day_item/',
                        'name' => 'Товар дня',
                    ],
                    [
                        'url' => '/admin/promo_codes/',
                        'name' => 'Промо-коды',
                        'access' => self::RIGHTS_PROMO_CODES,
                    ],
                    // [
                    //     'url' => '/admin/partner/',
                    //     'name' => 'Партнёрские программы',
                    // ],
                    [
                        'url' => '/admin/canonical/',
                        'name' => 'Канонические ссылки',
                    ],
                ],
            ],
            [
                'name' => 'Редактировать',
                'access' => admin::RIGHTS_COMMON,
                'items' => [
                    [
                        'url' => '/admin/categories/',
                        'name' => 'Категории',
                    ],
                    [
                        'url' => '/admin/pages/',
                        'name' => 'Страницы',
                    ],
                    [
                        'url' => '/admin/articles/',
                        'name' => 'Статьи',
                    ],
                    [
                        'url' => '/admin/master_classes/',
                        'name' => 'Расписания мастер-классов',
                    ],
                    [
                        'url' => '/admin/competition/',
                        'name' => 'Акции',
                    ],
                    [
                        'url' => '/admin/review/',
                        'name' => 'Отзывы',
                    ],
                    [
                        'url' => '/admin/banners/',
                        'name' => 'Баннеры',
                    ],
                    // [
                    //     'url' => '/admin/action/',
                    //     'name' => 'Акции',
                    // ],
                    [
                        'url' => '/admin/tags/',
                        'name' => 'Теги',
                        'access' => self::RIGHTS_TAGS_EDITOR,
                    ],
                    [
                        'url' => '/admin/kosmetika/',
                        'name' => 'Типы косметики',
                    ],
                    [
                        'url' => '/admin/prod_cert/',
                        'name' => 'Сертификаты',
                        'access' => self::RIGHTS_PRODUCT_CERTIFICATES,
                    ],
                    [
                        'url' => '/admin/delivery_compensation/',
                        'name' => 'Компенсация доставки',
                        'access' => self::RIGHTS_STATISTICS,
                    ],
                    [
                        'url' => '/admin/country',
                        'name' => 'Косметика по странам',
                    ],
                ],
            ],
            [
                'name' => 'Отчёты',
                'access' => self::RIGHTS_STATISTICS,
                'items' => [
                    [
                        'url' => '/admin/search_logs/',
                        'name' => 'История поиска',
                        'access' => self::RIGHTS_STATISTICS,
                    ],
                    [
                        'url' => '/admin/feedback/',
                        'name' => 'Обратная связь',
                        'access' => self::RIGHTS_STATISTICS,
                    ],
                    [
                        'url' => '/admin/live_operator/',
                        'name' => 'Live operator',
                        'access' => self::RIGHTS_STATISTICS,
                    ],
                    [
                        'url' => '/admin/statistics/',
                        'name' => 'Статистика',
                        'access' => self::RIGHTS_STATISTICS,
                    ],
                ],
            ],
            [
                'name' => 'Дополнительно',
                'access' => admin::RIGHTS_COMMON,
                'items' => [
                    [
                        'url' => '/admin/questions/',
                        'name' => 'Вопросы косметологу',
                        'access' => self::RIGHTS_STATISTICS,
                    ],
                    [
                        'url' => '/admin/product_reviews/',
                        'name' => 'Отзывы о товарах',
                        'access' => self::RIGHTS_STATISTICS,
                    ],
                    [
                        'url' => '/admin/newsletter_images/',
                        'name' => 'Картинки рассылок',
                        'access' => admin::RIGHTS_NEWSLETTER_IMAGES,
                    ],
                    [
                        'url' => '/admin/vk/',
                        'name' => 'Интеграция vk.com',
                    ],
                    [
                        'url' => '/admin/yamarket/',
                        'name' => 'Yandex API',
                    ],
                    [
                        'url' => '/admin/compress_images/',
                        'name' => 'Оптимизация изображений',
                    ],
                ],
            ],
            [
                'name' => 'SEO',
                'access' => admin::RIGHTS_SEO,
                'items' => [
                    [
                        'url' => '/admin/seo/home',
                        'name' => 'Главная страница',
                    ],
                    [
                        'url' => '/admin/seo/categories',
                        'name' => 'Категории',
                    ],
                    [
                        'url' => '/admin/seo/tm',
                        'name' => 'Бренды',
                    ],
                    [
                        'url' => '/admin/seo/lines',
                        'name' => 'Бренд + коллекция',
                    ],
                    [
                        'url' => '/admin/seo/category-tm',
                        'name' => 'Категория + бренд',
                    ],
                    [
                        'url' => '/admin/seo/products',
                        'name' => 'Товары',
                    ],
                    [
                        'url' => '/admin/seo/tag_page',
                        'name' => 'Создание теговой странцы',
                    ],
                    [
                        'url' => '/admin/seo/link_base',
                        'name' => 'Перелинковка',
                    ],
                    [
                        'url' => '/admin/seo/type_pages',
                        'name' => 'Страницы типов косметики',
                    ],
                    [
                        'url' => '/admin/seo/tm_type_pages',
                        'name' => 'Страница типа косметики + бренд',
                    ],
                ],
            ],
        ];
    }
}
