<?php

/**
 * Work with promo codes
 */
class promo_codes
{
    /**
     * Promo code for certain product IDs
     */
    const ACTION_TYPE_PRODUCTS = 1;

    /**
     * Promo code for certain trademark IDs
     */
    const ACTION_TYPE_TMS = 2;

    /**
     * Promo code for certain category IDs
     */
    const ACTION_TYPE_CATEGORIES = 3;

    /**
     * Promo code giving cheapest product with discount if certain products are bought together
     */
    const ACTION_TYPE_CHEAPEST_PRODUCT_DISCOUNT = 4;

    /**
     * Pagination on page value
     */
    const ON_PAGE = 30;

    /**
     * Certificate directory inside document root
     */
    const CERTIFICATE_DIR = 'images/certificates';

    /**
     * Get promo code info
     * @param   string $code users code
     * @param bool $notUsed Find not used and active promo code only
     * @return  array  Code info
     */
    static function get_by_code($code, $notUsed = false)
    {

        $query = 'SELECT id, code, type, create_date, end_date, use_date, discount, `use`, discount_type, IF(end_date IS NULL OR end_date > NOW(),1,0) AS `time`
                  FROM rk_promo_codes
                  WHERE code = "' . DB::mysql_secure_string($code) . '"' .
                  ($notUsed ? ' AND `use` = 0 AND (end_date IS NULL OR end_date > NOW())' : '');

        return DB::get_row($query);
    }

    /**
     * Set code is use, and set use date
     * @param string $code $code Users code
     * @param int $id Users code id
     */
    static function set_is_use($code, $id)
    {

        $query = "UPDATE rk_promo_codes 
                  SET use_date = NOW(), `use` = 1
                  WHERE code = '$code'
                  AND   id = $id";

        return DB::query($query);
    }

    /**
     * Generate and check promo codes
     *
     * @param int $discount Value of discount
     * @param int $discountType Discount type value
     * @param int $day_end Days before lock promo-code
     * @param bool $certificate Certificate
     *
     * @return string Code
     */
    public static function generate_promo_code($discount, $discountType = 0, $certificate = false, $day_end = FALSE)
    {
        do {
            $code = optimization::make_code(8);
        } while (DB::get_row("SELECT id FROM rk_promo_codes WHERE code = '$code'"));

        $query = "INSERT INTO rk_promo_codes( code,
                                           type, 
                                           create_date," .
                                           ($day_end ? ' end_date,' : '') .
                                           "use_date,
                                           discount,
                                           discount_type,
                                           certificate,
                                           `use`) 
                         VALUES ('$code', 
                                 'uniq', 
                                 NOW(), " .
                                 ($day_end ? ' DATE_ADD(NOW(), INTERVAL '. $day_end .' DAY),' : '') .
                                 "'0000-00-00 00:00:00',
                                 $discount,
                                 " . ((int)$discountType) . ",
                                 " . ($certificate ? 1 : 0) . ",
                                 0)";

        DB::query($query);

        return $code;
    }

    /**
     * Returns promo code
     *
     * @param int $promoID Promo code ID
     * @param bool $admitad Admitad promo code
     *
     * @return array
     */
    public static function get_by_id($promoID, $admitad = false)
    {
        return DB::get_row('SELECT p.*' . ($admitad ? ', 0 AS certificate, 0 AS min_sum, 0 AS action_type_id, 0 AS min_action_products_count' : '') . ' FROM ' . ($admitad ? 'rk_admitad_promo_codes' : 'rk_promo_codes') . ' AS p WHERE p.id = ' . ((int)$promoID));
    }

    /**
     * Returns promo code action products
     *
     * @param int $promoID Promo code ID
     *
     * @return array
     */
    public static function get_action_product_ids($promoID)
    {
        return DB::get_rows(
            'SELECT pp.product_id FROM rk_promo_codes_products AS pp WHERE pp.promo_code_id = ' . ((int)$promoID),
            function ($product) {
                return (int)$product['product_id'];
            }
        );
    }

    /**
     * Returns promo code action trademarks
     *
     * @param int $promoID Promo code ID
     *
     * @return array
     */
    public static function get_action_tm_ids($promoID)
    {
        return DB::get_rows(
            'SELECT pt.tm_id FROM rk_promo_codes_tms AS pt WHERE pt.promo_code_id = ' . ((int)$promoID),
            function ($tm) {
                return (int)$tm['tm_id'];
            }
        );
    }

    /**
     * Returns promo code action categories
     *
     * @param int $promoID Promo code ID
     *
     * @return array
     */
    public static function get_action_category_ids($promoID)
    {
        return DB::get_rows(
            'SELECT pt.category_id FROM rk_promo_codes_categories AS pt WHERE pt.promo_code_id = ' . ((int)$promoID),
            function ($category) {
                return (int)$category['category_id'];
            }
        );
    }

    /**
     * Returns product IDs used as a set of products when one with minimal price is being sold with discount
     *
     * @param int $promoID Promo code ID
     *
     * @return array
     */
    public static function get_action_cheapest_discount_product_ids($promoID)
    {
        return DB::get_rows(
            'SELECT pt.product_id FROM rk_promo_codes_cheapest_discount_products AS pt WHERE pt.promo_code_id = ' . ((int)$promoID),
            function ($product) {
                return (int)$product['product_id'];
            }
        );
    }

    /**
     * Returns paginated promo codes
     *
     * @param int $page Page number. If null - count will be returned
     * @param string $query Partial promo code value
     * @param int $actionTypeID Promo code action type ID
     *
     * @return array|int
     */
    public static function get_promo_codes($page, $query = null, $actionTypeID = null)
    {
        $returnCount = $page === null;

        $query = 'SELECT ' . ($returnCount ? 'COUNT(p.id) AS cnt' : 'p.*') . '
                  FROM rk_promo_codes AS p
                  WHERE 1 = 1' .
            ($query ? ' AND code LIKE "%' . DB::escape($query) . '%"' : '') .
            ($actionTypeID ? ' AND p.action_type_id = ' . (int)$actionTypeID : '') .
            ($page ? ' LIMIT ' . ($page == 1 ? 0 : self::ON_PAGE * ($page - 1)) . ', ' . self::ON_PAGE : '');

        return $returnCount ? (int)DB::get_field('cnt', $query) : DB::get_rows($query);
    }

    /**
     * Updates promo code
     *
     * @param int $id Promo code ID. null if new promo code must be created
     * @param string $code Code
     * @param int $discount Discount value
     * @param bool $uniq Whether unique or repeatable client
     * @param string $endDate End date value
     * @param int $discountType Discount type. 0 - regular promo code, 1 - simple discount
     * @param int $minSum Minimal order sum
     * @param bool $used Mark promo code as used
     * @param int $actionTypeID Promo code action type ID
     * @param int $minActionProductsCount Minimal products count needed for discount for cheapest product
     * @param bool $isCertificate Is certificate
     *
     * @return bool|int Promo code ID on success, false on failure
     */
    public static function update($id, $code, $discount, $uniq, $endDate, $discountType, $minSum, $used, $actionTypeID, $minActionProductsCount, $comment, $isCertificate)
    {
        $id = (int)$id;

        if (!!DB::get_field('id', 'SELECT p.id FROM rk_promo_codes AS p WHERE p.code = "' . DB::escape($code) . '"' . ($id ? ' AND p.id != ' . $id : '') . ' LIMIT 1')) {
            return false;
        }

        $now = date('Y-m-d H:i:s');

        $data = $id ? DB::get_row('SELECT p.create_date, p.use_date FROM rk_promo_codes AS p WHERE p.id = ' . $id) : [
            'create_date' => $now,
            'use_date' => $now,
        ];

        DB::query(
            'REPLACE INTO rk_promo_codes (' . ($id ? 'id, ' : '') . '`code`, type, create_date, end_date, use_date, discount, certificate, discount_type, `use`, min_sum, action_type_id, min_action_products_count, comment)
            VALUES (
                ' . ($id ? $id . ', ' : '') . '
                "' . DB::escape($code) . '",
                "' . ($uniq ? 'uniq' : 'repeat') . '",
                "' . $data['create_date'] . '",
                ' . ($endDate ? '"' . date('Y-m-d H:i:s', strtotime($endDate)) . '"' : 'NULL') . ',
                "' . ($used ? $data['use_date'] : '0000-00-00 00:00:00') . '",
                ' . ($discount > 0 ? ($discount <= 100 ? floatraw($discount) : 100) : floatraw($discount)) . ',
                ' . ($isCertificate ? 1 : 0) . ',
                ' . ((int)$discountType) . ',
                ' . ($used ? 1 : 0) . ',
                ' . ((int)$minSum) . ',
                ' . ((int)$actionTypeID) . ',
                ' . ((int)$minActionProductsCount) . ',
                "' . ($comment ? $comment : '') . '"
            )'
        );

        if (!$id) {
            $id = DB::get_last_id();
        }

        return $id;
    }

    /**
     * Updates action products for promo code
     *
     * @param int $promoCodeID Promo code ID
     * @param array $productIDs Product IDs
     */
    public static function update_action_products($promoCodeID, $productIDs = null)
    {
        $promoCodeID = (int)$promoCodeID;

        DB::query('DELETE FROM rk_promo_codes_products WHERE promo_code_id = ' . $promoCodeID);

        if ($productIDs) {
            for ($i = 0, $n = count($productIDs); $i < $n; ++$i) {
                DB::query('INSERT INTO rk_promo_codes_products (promo_code_id, product_id) VALUES (' . $promoCodeID . ', ' . ((int)$productIDs[$i]) . ')');
            }
        }
    }

    /**
     * Updates action trademarks for promo code
     *
     * @param int $promoCodeID Promo code ID
     * @param array $tmIDs Trademark IDs
     */
    public static function update_action_tms($promoCodeID, $tmIDs = null)
    {
        $promoCodeID = (int)$promoCodeID;

        DB::query('DELETE FROM rk_promo_codes_tms WHERE promo_code_id = ' . $promoCodeID);

        if ($tmIDs) {
            for ($i = 0, $n = count($tmIDs); $i < $n; ++$i) {
                DB::query('INSERT INTO rk_promo_codes_tms (promo_code_id, tm_id) VALUES (' . $promoCodeID . ', ' . ((int)$tmIDs[$i]) . ')');
            }
        }
    }

    /**
     * Updates action categories for promo code
     *
     * @param int $promoCodeID Promo code ID
     * @param array $categoryIDs Category IDs
     */
    public static function update_action_categories($promoCodeID, $categoryIDs = null)
    {
        $promoCodeID = (int)$promoCodeID;

        DB::query('DELETE FROM rk_promo_codes_categories WHERE promo_code_id = ' . $promoCodeID);

        if ($categoryIDs) {
            for ($i = 0, $n = count($categoryIDs); $i < $n; ++$i) {
                DB::query('INSERT INTO rk_promo_codes_categories (promo_code_id, category_id) VALUES (' . $promoCodeID . ', ' . ((int)$categoryIDs[$i]) . ')');
            }
        }
    }

    /**
     * Updates action of type discount for cheapest product for promo code
     *
     * @param int $promoCodeID Promo code ID
     * @param array $productIDs Product IDs
     */
    public static function update_action_cheapest_discount_products($promoCodeID, $productIDs = null)
    {
        $promoCodeID = (int)$promoCodeID;

        DB::query('DELETE FROM rk_promo_codes_cheapest_discount_products WHERE promo_code_id = ' . $promoCodeID);

        if ($productIDs) {
            for ($i = 0, $n = count($productIDs); $i < $n; ++$i) {
                DB::query('INSERT INTO rk_promo_codes_cheapest_discount_products (promo_code_id, product_id) VALUES (' . $promoCodeID . ', ' . ((int)$productIDs[$i]) . ')');
            }
        }
    }

    /**
     * Generates certificate & starts download
     *
     * @param array $promoCode Promo code ID
     * @param bool $outputAttachment Output file content to browser
     *
     * @return string Saved filename (If $outputAttachment false)
     */
    public static function generate_certificate($promoCode, $outputAttachment = true)
    {
        if ($promoCode && $promoCode['discount'] < 0) {
            $saveFile = SITE_PATH . 'www/' . self::CERTIFICATE_DIR . '/' . $promoCode['code'] . '.jpg';

            $im = imagecreatefromjpeg(SITE_PATH . 'www/images/promo_certificate_template.jpg');

            $purple = imagecolorallocate($im, 147, 122, 172);

            $amount = abs((int)$promoCode['discount']);

            imagettftext($im, 45, 0, 180, 155, $purple, SITE_PATH . 'www/fonts/dinpro-bold-webfont.ttf', $promoCode['code']);

            imagettftext($im, 21, 0, 260, 322, $purple, SITE_PATH . 'www/fonts/dinpro-medium-webfont.ttf', 'НА ' . $amount);

            imagettftext($im, 21, 0, 310 + strlen($amount) * 15, 322, $purple, SITE_PATH . 'www/fonts/rouble/rouble.ttf', 'p');

            imagejpeg($im, $saveFile, 100);

            imagedestroy($im);

            unset($im);

            if ($outputAttachment) {
                header('Content-type: image/jpeg');
                header('Content-Disposition: attachment; filename=' . $promoCode['code'] . '.jpg');

                print file_get_contents($saveFile);

                exit(0);
            } else {
                return $saveFile;
            }
        } else {
            return null;
        }
    }

    /**
     * Returns order certificates, creates them if needed
     *
     * @param int $orderNumber Order number
     *
     * @return array File names
     */
    public static function check_order_certificates($orderNumber)
    {
        $products = Orders::get_order_by_number($orderNumber, true);

        $certificates = [];

        for ($i = 0, $n = count($products); $i < $n; ++$i) {
            if ($products[$i]['id'] == Product::CERTIFICATE_ID) {
                $discount = abs((int)$products[$i]['price']);

                $existingCertificates = self::order_certificates($orderNumber, $discount);

                for ($y = 0, $m = (int)$products[$i]['quantity']; $y < $m; ++$y) {
                    if (!isset($existingCertificates[$y])) {
                        $code = self::generate_promo_code(-1 * $discount, 0, true);

                        $promoCode = self::get_by_code($code);

                        $certificateFile = basename(self::generate_certificate($promoCode, false));

                        DB::query('INSERT INTO orders_certificates (promo_code_id, order_number) VALUES (' . ((int)$promoCode['id']) . ', ' . ((int)$orderNumber) . ') ON DUPLICATE KEY UPDATE promo_code_id = promo_code_id');
                    } else {
                        $certificateFile = $existingCertificates[$y]['code'] . '.jpg';
                    }

                    $certificates[] = $certificateFile;
                }

                break;
            }
        }

        return $certificates;
    }

    /**
     * Returns order certificates
     *
     * @param int $orderNumber Order number
     * @param float|int $amount Certificate amount value to search for
     *
     * @return array
     */
    public static function order_certificates($orderNumber, $amount = null)
    {
        return DB::get_rows(
            'SELECT oc.order_number, oc.promo_code_id,
                ABS(p.discount) AS discount, p.code
            FROM orders_certificates AS oc
            INNER JOIN rk_promo_codes AS p ON p.id = oc.promo_code_id' . ($amount ? ' AND p.discount = -' . abs((int)$amount) : '') . ' AND p.certificate = 1
            WHERE oc.order_number = ' . ((int)$orderNumber)
        );
    }

    /**
     * Cancels cart's promo code
     *
     * @param int $cartID Cart order ID
     * @param bool $markUnused Mark used in cart order promo code as unused
     */
    public static function cart_cancel($cartID, $markUnused = false)
    {
        $cartID = (int)$cartID;

        if ($markUnused) {
            $promo = DB::get_row('SELECT ro.promo_code_id, ro.admitad_promo FROM rk_orders AS ro WHERE ro.id = ' . $cartID);

            if ($promo) {
                DB::query('UPDATE ' . ($promo['admitad_promo'] ? 'rk_admitad_promo_codes' : 'rk_promo_codes') . ' SET `use` = 0, use_date = "0000-00-00 00:00:00" WHERE id = ' . $promo['promo_code_id']);
            } else {
                return;
            }
        }

        DB::query('UPDATE rk_orders SET promo_code_id = 0, admitad_promo = 0 WHERE id = ' . $cartID);
    }

    /**
     * Get information about promo-code end day
     * @param  int $promo_id promo-code ID
     * @return array         data about end date
     */
    public static function get_end_day_info ($promo_id)
    {
        $return = DB::get_row('SELECT rpc.end_date, IF(DATEDIFF(rpc.end_date, CURDATE()) = 0, 1, DATEDIFF(rpc.end_date, CURDATE())) AS end_days
                               FROM rk_promo_codes rpc
                               WHERE rpc.end_date IS NOT NULL
                               AND  rpc.end_date >=  NOW()
                               AND  rpc.id = ' . (int)$promo_id); 

        if ($return) 
        {
            $return['end_days_num'] = helpers::num_ending($return['end_days']);

        } else {
            $return = FALSE;
        }

        return $return;
    }
}
