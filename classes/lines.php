<?php

/**
 * Working with lines
 */
class lines
{

    /**
     * return all lines
     * @param  boolean $desc include description
     * @param  boolean $seo include seo
     * @param  boolean $photo include photo description
     * @return array          array of lines
     */
    static function get_lines($desc = TRUE, $seo = TRUE, $photo = TRUE)
    {
        $query = 'SELECT rk_lines.id, rk_lines.tm_id, rk_lines.name, rk_lines.url, rk_lines.short_desc, rk_lines.desc ' .
            ($desc ? ' ,ld.synonym, ld.alt_name' : '') .
            ($seo ? ' ,ls.h1, ls.title, ls.description seo_desc, ls.keywords' : '') .
            ($photo ? ' ,lp.src, lp.alt, lp.title photo_title ' : '') .
            'FROM rk_lines  ' .
            ($desc ? 'LEFT JOIN rk_lines_desc ld  ON ( ld.id = rk_lines.id ) ' : '') .
            ($seo ? 'LEFT JOIN rk_lines_seo ls   ON ( ls.id = rk_lines.id ) ' : '') .
            ($photo ? 'LEFT JOIN rk_lines_photo lp ON ( lp.id = rk_lines.id ) ' : '') .
            'ORDER BY rk_lines.name';

        return DB::get_rows($query);
    }


    /**
     * return lines for export
     * @param  array $param export parameters
     * @return array         array of lines
     */
    static function get_export_lines($param)
    {

        //include check for NULL and ''
        if (!empty($param)) {
            $sort = '';

            foreach ($param as $field) {
                $sort .= " ($field = '')  ($field IS NULL) ";
                if ($field == 'lp.src') $sort .= " ($field = 'none.jpg') ";
            }

            $param = str_replace('  ', ' OR ', trim($sort));
            $param = " WHERE ($param) ";
        }

        $query = 'SELECT rk_lines.id, rk_lines.tm_id, rk_lines.name, rk_lines.url, rk_lines.short_desc, rk_lines.desc,
                         ld.synonym, ld.alt_name, 
                         ls.h1, ls.title, ls.description seo_desc, ls.keywords,
                         lp.src, lp.alt, lp.title photo_title
                  FROM `rk_lines` 
                  LEFT JOIN `rk_lines_desc`  ld ON ( ld.id = rk_lines.id ) 
                  LEFT JOIN `rk_lines_seo`   ls ON ( ls.id = rk_lines.id ) 
                  LEFT JOIN `rk_lines_photo` lp ON ( lp.id = rk_lines.id )' . $param .
            'ORDER BY rk_lines.id';

        return DB::get_rows($query);

    }


    /**
     * return lines and products for special main
     * @return array array of special
     */
    public static function main_special()
    {
        $lines = cache_item('main_lines', function () {
            return DB::get_rows(
                'SELECT l.id, l.name, IF( l.url =  "", l.id, l.url ) url, IF( tm.url =  "", tm.id, tm.url ) tm_url FROM `rk_lines` l, tm  WHERE l.tm_id = tm.id AND tm._has_products = 1 AND l.main_page = 1 ORDER BY RAND() LIMIT 20',
                function ($line) {
                    $line['ids'] = DB::get_rows(
                        'SELECT p.id
                        FROM  rk_prod_lines pl,  products AS p
                        WHERE pl.line_id = ' . $line['id'] . '
                        AND   pl.prod_id = p.id
                        AND   p.visible = 1
                        AND   p.active = 1
                        AND   p.available = 1
                        AND   p.main_id = 0
                        GROUP BY p.id
                        ORDER BY RAND()
                        LIMIT 4',
                        function ($product) {
                            return (int)$product['id'];
                        }
                    );

                    return $line['ids'] && count($line['ids']) > 3  ? $line : null;
                }
            );
        }, 3600);//1 hour

        $line = $lines[rand(0, count($lines) - 1)];

        unset($lines);

        $sets = cache_item('main_sets', function () {
            return DB::get_rows(
                'SELECT id, name, IF( url =  "", id, url ) url FROM rk_sets WHERE main_page = 1 ORDER BY RAND() LIMIT 20',
                function ($set) {
                    $set['ids'] = DB::get_rows(
                        'SELECT p.id
                        FROM rk_prod_sets AS ps, products AS p
                        WHERE ps.set_id = ' . $set['id'] . '
                        AND   ps.prod_id = p.id
                        AND   p.visible = 1
                        AND   p.active = 1
                        AND   p.available = 1
                        AND   p.main_id = 0
                        GROUP BY p.id
                        ORDER BY RAND()
                        LIMIT 4',
                        function ($product) {
                            return (int)$product['id'];
                        }
                    );

                    return $set['ids'] && count($set['ids']) > 3 ? $set : null;
                }
            );
        }, 3600);//1 hour

        $set = $sets[rand(0, count($sets) - 1)];

        unset($sets);

        $tabs = [
            [
                'name' => 'новинки',
                'url' => '/products/new',
                'url_name' => 'Все&nbsp;новинки',
                'sql' => [
                    'select' => 'SELECT p.id, p.name, p.short_description, IF( p.url =  "", p.id, p.url ) url, p.price, p.new, p.hit, p.available, p.special_price, p.pack, tm.name tm, IF(tm.url =  "", tm.id, tm.url ) tm_url, 0 AS day_item, p.for_proff, 
                        p._max_discount AS discount, p._max_discount_days  AS is_sales,
                        pp.src, pp.alt, pp.title photo_title,
                        (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.hit = 1 LIMIT 1) AS pack_hit,
                        (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.new = 1 LIMIT 1) AS pack_new,
                        0 AS soon',
                    'query' => 'FROM tm, products AS p
                        LEFT JOIN rk_prod_photo pp ON (pp.id = p.id)
                        WHERE p.new = 1
                        AND   p.tm_id = tm.id
                        AND   p.visible = 1
                        AND   p.active = 1
                        AND   p.available = 1
                        AND   p.main_id = 0',
                    'end' => 'LIMIT 4',
                ],
            ],
            [
                'name' => 'со скидкой',
                'url' => '/products/sales',
                'url_name' => 'Все&nbsp;товары&nbsp;со&nbsp;скидкой',
                'sql' => [
                    'select' => 'SELECT p.id, p.name, p.short_description, IF(p.url = "", p.id, p.url) url, p.price, p.new, p.hit, p.available, p.special_price, p.pack, p.for_proff,
                        p._max_discount AS discount, p._max_discount_days  AS is_sales,
                        tm.name tm, IF(tm.url = "", tm.id, tm.url) tm_url,
                        0 AS day_item,
                        pp.src, pp.alt, pp.title photo_title,
                        (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.hit = 1 LIMIT 1) AS pack_hit,
                        (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.new = 1  LIMIT 1) AS pack_new,
                        0 AS soon',
                    'query' => 'FROM tm, products AS p
                        LEFT JOIN rk_prod_photo pp ON (pp.id = p.id)
                        WHERE p.special_price > 0
                        AND   p.tm_id = tm.id
                        AND   p.available = 1
                        AND   p.active = 1
                        AND   p.visible = 1
                        AND   p.main_id = 0',
                    'end' => 'GROUP BY p.id
                        LIMIT 4',
                ],
            ],
            [
                'name' => mb_strlen($line['name']) > 30 ? mb_substr($line['name'], 0, 30) . '...' : $line['name'],
                'url' => '/tm/' . $line['tm_url'] . '/' . $line['url'],
                'url_name' => 'Все&nbsp;товары&nbsp;этой&nbsp;линейки',
                'sql' => [
                    'select' => 'SELECT p.id, p.name, p.short_description, IF( p.url =  "", p.id, p.url ) url, p.price, p.new, p.hit, p.available, p.special_price, p.pack, tm.name tm, IF(tm.url =  "", tm.id, tm.url ) tm_url, 0 AS day_item, p.for_proff, 
                         p._max_discount AS discount, p._max_discount_days  AS is_sales,
                         pp.src, pp.alt, pp.title photo_title,
                         (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.hit = 1 LIMIT 1) AS pack_hit,
                         (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.new = 1 LIMIT 1) AS pack_new,
                         0 AS soon',
                    'ids' => $line['ids'],
                    'query' => 'FROM tm,  products AS p
                        LEFT JOIN rk_prod_photo pp ON (pp.id = p.id)
                        WHERE tm.id = p.tm_id
                        AND p.visible = 1
                        AND p.active = 1
                        AND p.available = 1
                        AND p.main_id = 0
                        ',
                    'end' => 'LIMIT 4',
                ],
            ],
            [
                'name' => mb_strlen($set['name']) > 30 ? mb_substr($set['name'], 0, 30) . '...' : $set['name'],
                'url' => '/sets/' . $set['url'],
                'url_name' => 'Все&nbsp;товары&nbsp;этого&nbsp;набора',
                'sql' => [
                    'select' => 'SELECT p.id, p.name, p.short_description, IF( p.url =  "", p.id, p.url ) url, p.price, p.new, p.hit, p.available, p.special_price, p.pack, tm.name tm, IF(tm.url =  "", tm.id, tm.url ) tm_url, 0 AS day_item, p.for_proff,
                         p._max_discount AS discount, p._max_discount_days  AS is_sales,
                         pp.src, pp.alt, pp.title photo_title,
                         (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.hit = 1 LIMIT 1) AS pack_hit,
                         (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.new = 1 LIMIT 1) AS pack_new,
                         0 AS soon',
                    'ids' => $set['ids'],
                    'query' => 'FROM tm, products AS p
                        LEFT JOIN rk_prod_photo pp ON (pp.id = p.id)
                        WHERE tm.id = p.tm_id
                        AND p.visible = 1
                        AND p.active = 1
                        AND p.available = 1
                        AND   p.main_id = 0',
                    'end' => 'LIMIT 4',
                ],
            ],
        ];


        if (DEBUG || empty($_SESSION['index_rr_hits']) || $_SESSION['index_rr_hits_exp'] < $_SERVER['REQUEST_TIME']) {

            $u_id = ((isset($_SESSION['user']['id']) && (int)$_SESSION['user']['id'] > 0) ? (int)$_SESSION['user']['id'] : 0);

            $pIDs =  products::get_hits(15);
            // $pIDs = helpers::get_rr_product_ids('https://api.retailrocket.ru/api/2.0/recommendation/personalized/popular/55cdc1e268e9a65c24215c3d?session=' . $_COOKIE['rcuid'], 2);
            $_SESSION['index_rr_hits'] = $pIDs;
            $_SESSION['index_rr_hits_exp'] = $_SERVER['REQUEST_TIME'] + 60;
        } else {
            $pIDs = $_SESSION['index_rr_hits'];
        }

        if ($pIDs) {
            array_unshift(
                $tabs,
                [
                    'name' => 'Хиты',
                    'url_name' => '',
                    'no_cache' => true,
                    'sql' => [
                        'select' => 'SELECT p.id, p.name, p.short_description, IF( p.url =  "", p.id, p.url ) url, p.price, p.new, p.hit, p.available, p.special_price, p.pack, tm.name tm, IF(tm.url =  "", tm.id, tm.url ) tm_url, 0 AS day_item, p.for_proff,
                         p._max_discount AS discount, p._max_discount_days  AS is_sales,
                         pp.src, pp.alt, pp.title photo_title,
                         (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.hit = 1 LIMIT 1) AS pack_hit,
                         (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.new = 1 LIMIT 1) AS pack_new,
                         0 AS soon',
                        'query' => 'FROM tm, products AS pack_prod
                            INNER JOIN products AS p
                            LEFT JOIN rk_prod_photo pp ON (pp.id = p.id)
                            WHERE p.main_id = 0
                            AND   p.visible = 1
                            AND   p.active = 1
                            AND   p.available = 1
                            AND   p.tm_id = tm.id',
                        'ids' => $pIDs,
                        'end' => 'LIMIT 4',
                    ],
                ]
            );
        }


        $data = [
            'groups' => [],
            'lines' => [],
        ];

        for ($i = 0, $n = count($tabs); $i < $n; ++$i) {
            $predefined = isset($tabs[$i]['sql']['ids']);

            $noCache = !empty($tabs[$i]['no_cache']);

            if (!$predefined && !$noCache) {
                //cache all IDs to prevent SQL database overload
                $allIDs = cache_item('main_tab_' . $i, function () use ($tabs, $i) {
                    return DB::get_rows(
                       'SELECT DISTINCT(p.id) ' . $tabs[$i]['sql']['query'] . ' ORDER BY RAND() LIMIT 60',                      
                        function ($product) {
                            return (int)$product['id'];
                        }
                    );
                }, 600);//10 minutes

                shuffle($allIDs);
            } else { 
                $allIDs = $tabs[$i]['sql']['ids'];
            }

            $products = $allIDs
                ? filters::set_products_info(DB::get_rows($tabs[$i]['sql']['select'] . ' ' . $tabs[$i]['sql']['query'] . ' AND p.id IN (' . implode(', ', $predefined ? $allIDs : array_slice($allIDs, 0, 4)) . ') ' . $tabs[$i]['sql']['end']))
                : []; 

            if ($data['groups'] || $products) {
                $data['groups'][] = $tabs[$i]['name'];
                $data['lines'][] = [
                    'line_url' => isset($tabs[$i]['url']) ? $tabs[$i]['url'] : null,
                    'tag_url' => $tabs[$i]['url_name'],
                    'products' => $products,
                ];
            }
        }

        if ($data['groups'][0] === 'Хиты') {
            if (!isset($_SESSION['premium_index_product_id'])) {
                $premiumIDs = [2021, 1943, 2092, 1938, 2061, 1940, 1959, 1952, 1942, 2091, 1937, 1885];

                shuffle($premiumIDs);

                $product = null;

                for ($i = 0, $n = count($premiumIDs); $i < $n; ++$i) {
                    $product = Product::get_by_id($premiumIDs[$i], true, true);

                    if ($product && $product['available']) {
                        break;
                    } else {
                        $product = null;
                    }
                }

                if ($product) {
                    $_SESSION['premium_index_product_id'] = $product['id'];

                    $data['lines'][0]['products'][0] = $product;
                }
            } else {
                if ($_SESSION['premium_index_product_id']) {
                    $data['lines'][0]['products'][0] = Product::get_by_id($_SESSION['premium_index_product_id'], true, true);
                }
            }
        }

        return $data;
    }

    /**
     * Get by ID  URL for redirect line
     * @param  int $line_id id of line
     * @param  int $tm_id id of trademark
     * @return string       URL of line
     */
    static function redirect($line_id, $tm_id)
    {
        return DB::get_field('url', 'SELECT url FROM rk_lines WHERE id = ' . $line_id . ' AND tm_id = ' . $tm_id);
    }

    /**
     * Get by URL id to get lines and products
     * @param  string $url url of lines
     * @param  int    $tm_id ID of tm
     * @return int          ID of line
     */
    static function get_id_by_url($url, $tm_id)
    {

        return (int)DB::get_field('id', "SELECT id FROM rk_lines WHERE url = '$url' AND tm_id = $tm_id");

    }


    /**
     * Output products, filters  for line
     * @param  int $line_id line ID
     * @param  bool $flag flag for start work - TRUE, if FALSE - stop work and return FALSE
     * @param bool $ajax Ajax request, return only products
     *
     * @return array         products of line with filters
     */
    static function line_output($id, $flag, $ajax = false)
    {

        //  return array begin value
        $data = FALSE;

        if ($flag) {

            if (!$ajax) {
                // line
                $query = "SELECT rk_lines.id, rk_lines.name, rk_lines.url, rk_lines.short_desc, rk_lines.desc,
                               ld.synonym,
                               ls.h1, ls.title, ls.description seo_desc, ls.keywords,
                               lp.src, lp.alt, lp.title photo_title
                      FROM rk_lines  
                      LEFT JOIN rk_lines_desc ld  ON ( ld.id = rk_lines.id ) 
                      LEFT JOIN rk_lines_seo ls   ON ( ls.id = rk_lines.id ) 
                      LEFT JOIN rk_lines_photo lp ON ( lp.id = rk_lines.id )
                      WHERE rk_lines.id = $id 
                      LIMIT 1";

                $data['line'] = DB::get_row($query);

                //  filters
                $data['filters'] = FALSE;
                $query = "SELECT id, name FROM rk_filters WHERE parent_id = 0 AND visible = 1 ORDER BY name";

                $main_filters = DB::get_rows($query);

                foreach ($main_filters as $key => $main_filter) {
                    $query = "SELECT  f.id, f.name
                          FROM   rk_prod_lines AS pl, rk_prod_filter AS pf, rk_filters AS f                                                 
                          WHERE  pl.line_id = $id
                          AND    pl.prod_id = pf.prod_id
                          AND    pf.filter_id = f.id
                          AND    f.visible = 1
                          AND    f.parent_id = {$main_filter['id']}
                          GROUP BY f.id 
                          ORDER BY f.name";
                    $filters = db::get_rows($query);
                    if ($filters && (count($filters) > 1)) $data['filters'][$main_filter['name']] = $filters;
                }
            }

            $query = "SELECT p.id, p.name, p.short_description, IF( p.url =  '', p.id, p.url ) url, p.price, p.new, p.hit, p.available, p.special_price, p.pack, tm.name tm, IF(tm.url =  '', tm.id, tm.url ) tm_url, 0 AS day_item,  p.for_proff, 
                               p._max_discount AS discount, p._max_discount_days  AS is_sales,
                               pp.src, pp.alt, pp.title photo_title,
                               (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.hit = 1 LIMIT 1) AS pack_hit,
                               (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.new = 1 LIMIT 1) AS pack_new
              FROM  rk_prod_lines pl, tm,  products AS p
              LEFT JOIN rk_prod_photo pp ON ( pp.id = p.id )
              WHERE  pl.line_id = $id
              AND    pl.prod_id = p.id
              AND    p.tm_id = tm.id
              AND    p.visible = 1
              AND    p.active = 1
              AND    p.main_id = 0" .
              ($ajax ? Product::filter_condition('p') : '') . "
              ORDER BY " . product::filter_order('p');

            $products = DB::get_rows($query);
            $data['products'] = $products ? filters::set_products_info($products) : array();

        }

        return $data ? ($ajax ? $data['products'] : $data) : false;
    }

    /**
     * Get list of lines
     *
     * @param  int     $tm_id   ID of line in tm
     * @param  boolean $visible True - only visible, False - all
     * @param bool $withSEO Include SEO info
     *
     * @return array            array of lines
     */
    public static function get_list($tm_id, $visible = TRUE, $withSEO = false)
    {

        $query = 'SELECT l.id, l.name, IF(l.url = "", l.id, l.url) AS url, DATE_FORMAT(l.lastmod ,"%Y-%m-%d") AS lastmod' .
                 ($withSEO ? ', ls.h1, ls.title, ls.description, ls.keywords' : '') .
                 ' FROM  rk_lines AS l' .
                 ($withSEO ? ' INNER JOIN rk_lines_seo AS ls ON ls.id = l.id' : '') .
                 ' WHERE l.tm_id = ' . ((int)$tm_id) . ($visible ? ' AND l.visible = 1' : '') .
            ' ORDER BY l.name';

        return DB::get_rows($query);
    }

    /**
     * Returns line by ID
     *
     * @param int $lineID Line ID
     *
     * @return array
     */
    public static function get_by_id($lineID)
    {
        return DB::get_row(
            'SELECT l.id, l.name, IF(l.url = "", l.id, l.url) AS url, l.desc, l.short_desc, DATE_FORMAT(l.lastmod ,"%Y-%m-%d") AS lastmod,
                    ls.h1, ls.title, ls.description, ls.keywords,
                    t.name AS tm_name, IF(t.url != "", t.url, t.id) AS tm_url
            FROM rk_lines AS l
            INNER JOIN rk_lines_seo AS ls ON ls.id = l.id
            INNER JOIN tm AS t ON t.id = l.tm_id
            WHERE l.id = ' . (int)$lineID
        );
    }


    /**
     * Updates line title, description, keywords, desc, short_desc
     *
     * @param int $lineID Line ID
     * @param string $h1 h1 value
     * @param string $title Title value
     * @param string $description Description value
     * @param string $keywords Keywords value
     * @param string $desc `desc` value
     * @param string $shortDesc `short_desc` value
     *
     * @return bool
     */
    public static function update_seo($lineID, $h1, $title, $description, $keywords, $desc, $shortDesc)
    {
        return !!DB::query('UPDATE rk_lines SET `desc` = "' . DB::escape($desc) . '", `short_desc` = "' . DB::escape($shortDesc) . '" WHERE id = ' . ((int)$lineID)) && !!DB::query('UPDATE rk_lines_seo SET h1 ="' . DB::escape($h1) . '",  title = "' . DB::escape($title) . '", description = "' . DB::escape($description) . '", keywords = "' . DB::escape($keywords) . '" WHERE id = ' . ((int)$lineID));
    }
}
