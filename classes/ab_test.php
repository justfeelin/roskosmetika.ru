<?php

abstract class ab_test
{
    /**
     * Key used to store session variable
     */
    const SESSION_KEY = 'abt';
    /**
     * Key used to store session variable as JSON string
     */
    const SESSION_STRING_KEY = 'abt-string';

    /**
     * Key used to store database session row ID
     */
    const SESSION_DB_HASH_KEY = 'abt-id';

    /**
     * Key used to store cookie session ID
     */
    const COOKIE_KEY = 'abt';

    /**
     * Salt value for hashin cookie session
     */
    const COOKIE_SALT = 'rk-abtests';

    /**
     * @var bool Store AB test state in database & cookie
     */
    private static $_storeInDb = false;

    /**
     * Initiates AB-test cases
     */
    public static function init()
    {
        if (!isset($_SESSION[self::SESSION_KEY])) {
            list($rowID, $session, $data) = self::$_storeInDb ? self::_dbData() : [null, null, null];

            if ($data === null) {
                $data = [];

                DB::get_rows(
                    'SELECT t.code, t.analytics_id, t.coverage, (SELECT c.code FROM rk_ab_test_case AS c WHERE c.test_id = t.id AND c.enabled = 1 ORDER BY RAND() LIMIT 1) AS test_case FROM rk_ab_test AS t WHERE t.enabled = 1',
                    function ($test) use (&$data) {
                        $data[$test['code']] = [
                            'analytics' => $test['analytics_id'],
                            'value' => $test['coverage'] === '100' || rand(0, 100) <= (int)$test['coverage'] ? ($test['test_case'] ?: true) : false,
                        ];
                    }
                );

                $stringData = json_encode($data);

                if (self::$_storeInDb) {
                    self::_setDbData($rowID, $stringData);
                }
            } else {
                $stringData = $data;
                $data = json_decode($data, true);
            }

            $_SESSION[self::SESSION_KEY] = $data;
            $_SESSION[self::SESSION_STRING_KEY] = $stringData;

            if (self::$_storeInDb) {
                $_SESSION[self::SESSION_DB_HASH_KEY] = $session;
            }
        } else {
            $stringData = $_SESSION[self::SESSION_STRING_KEY];

            if (self::$_storeInDb && (!isset($_COOKIE[self::COOKIE_KEY]) || $_COOKIE[self::COOKIE_KEY] !== $_SESSION[self::SESSION_DB_HASH_KEY])) {
                self::_setCookie($_SESSION[self::SESSION_DB_HASH_KEY]);
            }
        }

        Template::set('ABTests', $stringData);
    }

    /**
     * Returns specified AB-test use case
     *
     * @param string $testCode AB-test code value
     * @param string $neededCase Check for provided test case
     *
     * @return string|bool|null Used test case code, true if boolean AB-test (Or provided case matched), false if test is not being performed or provided case didn't match or null if specified test is not used for current session
     */
    public static function check($testCode, $neededCase = null)
    {
        return isset($_SESSION[self::SESSION_KEY][$testCode]) ? ($neededCase !== null ? $_SESSION[self::SESSION_KEY][$testCode]['value'] === $neededCase : $_SESSION[self::SESSION_KEY][$testCode]['value']) : null;
    }

    /**
     * Returns database tests values
     *
     * @return array [`Row session hash value`, `Row ID`, `String representation of tests data`]
     */
    private static function _dbData()
    {
        if (!isset($_COOKIE[self::COOKIE_KEY])) {
            list($id, $session) = self::_createDbRow();

            return [
                $id,
                $session,
                null,
            ];
        } else {
            $data = DB::get_row('SELECT s.id, s.session, s.data FROM rk_ab_test_session AS s WHERE s.session = "' . DB::mysql_secure_string($_COOKIE[self::COOKIE_KEY]) . '"');

            if (!$data) {
                list($id, $session) = self::_createDbRow();

                $data = [
                    'id' => $id,
                    'session' => $session,
                    'data' => null,
                ];
            }

            return [
                (int)$data['id'],
                $data['session'],
                $data['data'],
            ];
        }
    }

    /**
     * Updates tests data in database
     *
     * @param int $rowID Database row ID
     * @param string $data JSON tests data
     */
    private static function _setDbData($rowID, $data)
    {
        DB::query('UPDATE rk_ab_test_session SET data = "' . DB::mysql_secure_string($data) . '" WHERE id = ' . $rowID);
    }

    /**
     * Creates new cookie session key, sets cookie value
     *
     * @return array [`New row ID`, `Session hash value`]
     */
    private static function _createDbRow()
    {
        do {
            $session = sha1(self::COOKIE_SALT . '|' . microtime(true) . '|' . rand());
        } while (DB::get_row('SELECT s.id FROM rk_ab_test_session AS s WHERE s.session = "' . $session . '"'));

        DB::query('INSERT INTO rk_ab_test_session (session) VALUES ("' . $session . '")');

        self::_setCookie($session);

        return [
            DB::get_last_id(),
            $session,
        ];
    }

    /**
     * Sets cookie value
     *
     * @param string $session Session hash value
     */
    private static function _setCookie($session)
    {
        setcookie(self::COOKIE_KEY, $session, $_SERVER['REQUEST_TIME'] + 730 * 24 * 60 * 60, '/', DOMAIN, false, true);
    }
}
