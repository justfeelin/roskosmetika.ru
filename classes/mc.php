<?php

/**
 * Management master-class.
 * @author CheS
 */
class mc
{

    /**
     * Return mc by id
     * @param int $id
     * @return array
     */
    static function get_master_class($id)
    {
        $id = (int)$id;

        $query = "SELECT id,
                         name,
                         webinar,
                         new_address,
                         series,
                         short_desc,
                         description text,
                         date,
                         duration,
                         img,
                         HOUR(date) hour,
                         MINUTE(date) minute,
                         DAY(date) day,
                         MONTH(date) month,
                         YEAR(date) year,
                         visibility
                  FROM rk_master_classes
                  WHERE id = $id";
        return DB::get_row($query);
    }

    /**
     * Return mc list
     * @param boolean $only_visible Выборка только актуальных мастер-классов.
     * @param int $onPage
     *
     * @return array
     */
    static function get_master_classes_list($only_visible = true, $onPage = null, $dateDescending = false)
    {
        $where = $only_visible ? ' WHERE mc.visibility = 1 AND mc.date > NOW()' : '';

        if ($onPage !== null) {
            $count = (int)DB::get_field('cnt', 'SELECT COUNT(mc.id) AS cnt FROM rk_master_classes AS mc' . $where);

            Pagination::setValues($count, $onPage);

            Pagination::setTemplateSettings();
        }

        $query = 'SELECT mc.id,
                         mc.name,
                         mc.series,
                         mc.short_desc,
                         mc.description,
                         mc.date,
                         MONTH(mc.date) month,
                         DAY(mc.date) day,
                         DATE_ADD(mc.date, INTERVAL mc.duration HOUR) duration,
                         mc.img,
                         mc.visibility,
                         (SELECT COUNT(mcs.id) FROM rk_master_class_subscribes AS mcs WHERE mcs.master_class_id = mc.id) AS `count`
                  FROM rk_master_classes AS mc ' .
            $where .
            ' ORDER BY mc.date' . ($dateDescending ? ' DESC' : '') .
            ($onPage !== null ? Pagination::sqlLimit() : '');

        return DB::get_rows($query);
    }

    /**
     * Возвращает список записавшихся на мастер-классы.
     * @param integer $master_class_id ID мастер-класса.
     * @return array
     */
    static function get_subscribes_list($master_class_id = 0)
    {
        $master_class_id = (int)$master_class_id;

        $query = 'SELECT mcs.id,
                         mcs.name,
                         mcs.surname,
                         mcs.patronymic,
                         mcs.email,
                         mcs.phone,
                         mcs.price,
                         mcs.client_id,
                         mcs.date,
                         mcs.source,
                         mc.date master_class_date,
                         mc.name master_class_name
                  FROM rk_master_class_subscribes mcs,
                       rk_master_classes mc
                  WHERE mc.id = mcs.master_class_id';
        if ($master_class_id) {
            $query .= " AND master_class_id = $master_class_id";
        }
        $query .= ' ORDER BY mc.date, mcs.date';

        return DB::get_rows($query);
    }

    /**
     * Сохраняет изменения в мастер-классе, если id = 0, то добавляет новый.
     * @param <type> $id ID мастер-класса.
     * @param string $name Название.
     * @param string $series series.
     * @param string $short_desc short description.
     * @param string $description Описание.
     * @param string $date Дата и время проведения.
     * @param int $duration Duration of mc
     * @param string $img Image of mc
     * @param boolean $visibility Отображать или нет
     * @param boolean $webinar Webinar or Not
     */
    static function save_master_class($id, $name, $new_address, $series, $short_desc, $description, $date, $duration, $img, $visibility, $webinar)
    {
        $id = (int)$id;
        $visibility = (int)$visibility;
        $duration = (int)$duration;
        $new_address = (int)$new_address;

        if (!get_magic_quotes_gpc()) {
            $name = DB::mysql_secure_string(trim($name));
            $series = DB::mysql_secure_string(trim($series));
            $short_desc = DB::mysql_secure_string(trim($short_desc));
            $description = DB::mysql_secure_string(trim($description));
            $img = DB::mysql_secure_string(trim($img));
            $date = DB::mysql_secure_string(trim($date));
        }
        // Добавляем новый мастер-класс.
        if ($id == 0) {
            $query = "INSERT INTO rk_master_classes (name, new_address, series, short_desc, description, date, duration, pub_date, img, visibility, webinar)
                      VALUES ('$name',  $new_address, '$series', '$short_desc', '$description', '$date', $duration, NOW(), '$img', $visibility, $webinar)";
        } // Сохраняем изменеия в мастер-классе.
        else {
            $query = "UPDATE rk_master_classes
                      SET name = '$name',
                          new_address = $new_address, 
                          series = '$series',
                          short_desc = '$short_desc',
                          description = '$description',
                          date = '$date',
                          duration = $duration,
                          img = '$img',
                          visibility = $visibility,
                          webinar = $webinar
                      WHERE id = $id";
        }
        return DB::query($query);
    }


    /**
     * Добавляет нового подписчика.
     * @param string $name Имя клиента.
     * @param string $surname Фамилия клиента.
     * @param string $patronimic Отчество.
     * @param string $email Email клиента.
     * @param string $phone Телефон клиента.
     * @param string $type Тип клиента.
     * @param integer $master_class_id Id мастер класса.
     * @param integer $client_id ID клинета.
     * @return boolean
     */
    static function add_subscriber($name, $surname, $patronymic, $email, $phone, $price, $master_class_id, $client_id = 0)
    {
        $price = (int)$price;
        $master_class_id = (int)$master_class_id;
        $client_id = (int)$client_id;

        $query = "INSERT INTO rk_master_class_subscribes (name, surname, patronymic, email, phone, price, master_class_id, client_id, date, source)
                  VALUE ('" . DB::escape($name) . "', '" . DB::escape($surname) . "', '" . DB::escape($patronymic) . "', '" . DB::escape($email) . "', '" . DB::escape($phone) . "', $price, $master_class_id, $client_id, NOW(), 'roskosmetika.ru')";

        unsubscribe::add_self($email, 'self', 'roskosmetika.ru', $name, $surname, $patronymic);

        return DB::query($query);
    }

    /**
     * Скрывает/отображает мастер-класс.
     * @param integer $master_class_id ID мастер-класса
     * @return boolean
     */
    static function change_master_class_visibility($master_class_id)
    {
        $master_class_id = (int)$master_class_id;

        $query = "UPDATE rk_master_classes
                  SET visibility = (IF(visibility = 1, 0, 1))
                  WHERE id = $master_class_id";
        return DB::query($query);
    }

    /**
     * Проверяет доступность мастер-класса по id мастер-класса.
     * Т.е. cуществует ли мастер-класс с указанным id и доступен ли он.
     * @param <type> $id
     * @return <type>
     */
    static function check_master_class($id)
    {
        $id = (int)$id;

        $query = "SELECT id
                  FROM rk_master_classes
                  WHERE id = $id AND visibility = 1";
        return DB::get_num_rows($query);
    }

}

?>
