<?php
/**
 * Created by CheS.
 * User: CheS
 * Date: 09.09.2019
 * Time:  21:45
 */

class sendpulse
{

    const EVENT_LC_ID = 'https://events.sendpulse.com/events/id/ce938ab2101e4ed38e43d194cdf52b2a/7198326';
    const EVENT_ORDER_ID = 'https://events.sendpulse.com/events/id/34f9f7668d7d474bd5c8e4c6271892f0/7198326';
    const EVENT_5_DAY = 'https://events.sendpulse.com/events/id/9260a084c947459ca5306684b23319f4/7198326';
    const EVENT_DINAMO = 'https://events.sendpulse.com/events/id/3b6bc024b9774cd2d6aac872c8790955/7198326';
    const PROMO_END = 'https://events.sendpulse.com/events/id/b453265c4f0047b79977ac38b68b76ed/7198326';
    const REACTIVATION_1 = 'https://events.sendpulse.com/events/id/f42e2974d8014137c8c36c8d44a18b91/7198326';

    /**
     * Get lost order
     * @param  int $rk_order rk temp order ID
     * @param  int $limit    products limit
     * @return array   array for send to sendpulse
     */
    private static function get_lost_order($rk_order, $limit)
    {
        $return = array(); 
        $i = 0;  

        $query = "SELECT p.id, 
                         CONCAT(IF(p.main_id = 0, p.name, (SELECT pp.name FROM products pp WHERE pp.id = p.main_id)), ' ', p.pack, ', ', tm.name) AS name,
                         roi.quantity,
                         CAST(roi.price AS SIGNED) AS price,
                         CAST(IF(p.special_price > 0, p.price, 0) AS SIGNED) AS old_price,
                         p.url,
                         IF(rpp.pic_quant > 0, 1, 0) AS img
                  FROM rk_orders_items roi LEFT JOIN products p ON roi.product_id = p.id
                                           LEFT JOIN tm ON p.tm_id = tm.id
                                           LEFT JOIN rk_prod_photo rpp ON roi.product_id = rpp.id
                  WHERE roi.rk_order_id = $rk_order
                  ORDER BY price DESC
                  LIMIT $limit";


        DB::get_rows($query, function($product) use (&$return, &$i){

          $product['name'] = helpers::str_prepare($product['name']);
          $return['products'][$i] = $product;

          $return['ids'][] = $product['id'];
          $i++;


        });           

        $recomended = product::cart_neighbours($return['ids'], FALSE, $limit);

        for ($i=0; $i < count($recomended); $i++) { 

          $return['recomended'][$i]['id'] = $recomended[$i]['id'];
          $return['recomended'][$i]['name'] = helpers::str_prepare($recomended[$i]['name'] . ' ' . $recomended[$i]['packs'][0]['pack'] . ', '. $recomended[$i]['tm']);
          $return['recomended'][$i]['price'] = $recomended[$i]['packs'][0]['special_price'] > 0 ?  (int)$recomended[$i]['packs'][0]['special_price'] : (int)$recomended[$i]['packs'][0]['price'];
          $return['recomended'][$i]['old_price'] = $recomended[$i]['packs'][0]['special_price'] > 0 ?  (int)$recomended[$i]['packs'][0]['price'] : 0;
          $return['recomended'][$i]['url'] = $recomended[$i]['url'];
          $return['recomended'][$i]['img'] = $recomended[$i]['packs'][0]['pic_quant'] > 0 ?  1 : 0;
          
        }  

        unset($return['ids']);    
        
        return $return;          

    }

    /**
     * Send lost cart action to sendpulse
     * @param  array    $user_data client's rk_orders ID, email, phone, name, start_date
     * @param  bool|int $promo_code  add personal promo-code, int - promo code value
     * @return bool true or false to send data
     */
    public static function event_lost_cart($user_data,  $promo_code = FALSE)
    {
        $data = self::get_lost_order($user_data['rk_order'], 3);

        $data['email'] = $user_data['email'];
        $data['phone'] = $user_data['phone'];
        $data['client_name'] = $user_data['name'];
        $data['action_date'] = $user_data['start_date'];


        if ($promo_code) 
        {
          $data['promo_code'] = generate_promo_code($promo_code);
        }
        
        return self::send($data, self::EVENT_LC_ID);
    }

    /**
     * Send cURL POST data
     * @param  array  $data data for send
     * @param  string $url  cURL url
     * @return json   result
     */
    private static function send($data, $url)
    {

        $data = json_encode($data);

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_POST => true,
            CURLOPT_HTTPHEADER => ['Content-Type: application/json',
                                   'Accept: application/json'],
            CURLOPT_POSTFIELDS => $data,                        
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_SSL_VERIFYPEER => FALSE,
            CURLOPT_SSL_VERIFYHOST => FALSE,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_FAILONERROR => FALSE,
        ]);

        $result = curl_exec($curl);

        $result = json_decode($result, true);   

        if ($curl)
        {
            curl_close($curl);
        }

        return $result;
    }

    /**
     * Send order to sendpulse
     * @param  int   $order_number Base order number
     * @param  array $order_items  array of products
     * @param  int   $limit        products limit
     * @return bool                Sucsess of fail sending data
     */
    public static function event_order($order_number, $order_items, $limit)
    {
        $return = FALSE;

        $order = db::get_row("SELECT o.email, o.phone, o.added_date AS order_date, o.order_number, CAST(o.cost AS SIGNED) AS order_sum FROM orders o WHERE o.order_number = $order_number");

        if ($order)
        {
          $return = self::send($order + array_slice($order_items, 0, $limit), self::EVENT_ORDER_ID); 
        }

        return $return;
    }

    /**
     * Send orders data after 5 days  orders done
     * @param  array $data events data
     * @return boolen true or false
     */
    public static function event_5_days($data)
    {
        return self::send($data, self::EVENT_5_DAY);
    }

    /**
     * Send event for 15 days dinamo
     * @param  array $data events data
     * @return boolen true or false
     */
    public static function event_15_days($data)
    {
        return self::send($data, self::EVENT_DINAMO);
    }

    /**
     * Send event promo end sale
     * @param  array $data events data
     * @return boolen true or false
     */
    public static function promo_end_sale($data)
    {
        return self::send($data, self::PROMO_END);
    }

    /**
     * Send event reactivation
     * @param  array $data events data
     * @return boolen true or false
     */
    public static function reactivation_1($data)
    {
        return self::send($data, self::REACTIVATION_1);
    }

}