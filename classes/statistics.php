<?php

abstract class statistics
{

    /**
     * Orders export
     *
     * @param string $dateAddedFrom Starting added date time
     * @param string $dateAddedTo Starting added date time
     * @param string $dateDeliveryFrom Starting delivery date time
     * @param string $dateDeliveryTo Starting delivery date time
     *
     * @return array
     */
    public static function orders($dateAddedFrom = null, $dateAddedTo = null, $dateDeliveryFrom = null, $dateDeliveryTo = null)
    {
        return DB::get_rows(
            'SELECT *, o.cost - o.purchase - o.delivery AS profit
            FROM (
                SELECT o.order_number,
                    o.added_date,
                    IFNULL(o.delivery, 0) AS delivery,
                    IFNULL(co.old_delivery, 0) - IFNULL(o.delivery, 0) AS delivery_compensation,
                    o.cost,
                    IF(o.discount > 0, CONCAT(o.discount, "%"), "-") AS discount,
                    a.name AS agent,
                    co.delivery_date,
                    u.name AS manager,
                    cl.id AS client_id,
                    CONCAT(IF(cl.surname IS NULL, "", cl.surname), " ", IF(cl.name IS NULL, "", cl.name), " ", IF(cl.patronymic IS NULL, "", cl.patronymic)) AS client_name,
                    IF(cl.type_id = 1, "массажист", IF(cl.type_id = 2, "организация", "частное лицо")) AS client_type,
                    s.name AS region,
                    (
                        SELECT SUM(
                            IF(
                                oi.product_id = ' . Orders::PRESENT_PRODUCT_ID . ',
                                120,
                                (
                                    SELECT p.purchase_price
                                    FROM products AS p
                                    WHERE p.id = oi.product_id
                                )
                            ) * oi.quantity
                        )
                        FROM orders_items AS oi
                        WHERE oi.order_number = o.order_number
                    ) AS purchase,
                    IFNULL(c.name, "") AS carrier,
                    IFNULL(cc.name, "") AS courier
                FROM `orders` AS o
                INNER JOIN cdb_orders AS co ON co.order_number = o.order_number AND co.delivery_date IS NOT NULL
                LEFT JOIN agents AS a ON a.id = o.agent_id
                LEFT JOIN clients AS cl ON cl.id = o.client_id
                LEFT JOIN cdb_users AS u ON u.id = co.user_id
                LEFT JOIN address AS adr ON adr.id = o.address_id
                LEFT JOIN shipping_regions AS s ON s.id = adr.region_id
                LEFT JOIN carriers AS c ON c.id = o.carrier_id
                LEFT JOIN cdb_couriers AS cc ON co.courier_id IS NOT NULL AND cc.id = co.courier_id
                WHERE o.status_id NOT IN (2, 3, 4, 5, 10, 11, 12) ' .
            ($dateAddedFrom ? ' AND o.added_date >= "' . DB::escape($dateAddedFrom) . '" ' : '') .
            ($dateAddedTo ? ' AND o.added_date <= "' . DB::escape($dateAddedTo) . '" ' : '') .
            ($dateDeliveryFrom ? ' AND co.delivery_date >= "' . DB::escape($dateDeliveryFrom) . '" ' : '') .
            ($dateDeliveryTo ? ' AND co.delivery_date <= "' . DB::escape($dateDeliveryTo) . '" ' : '') .
            'ORDER BY o.order_number
            ) AS o'
        );
    }

    /**
     * Trademarks export
     *
     * @param bool $filterTypeAdded Filter type. True if by order added date, false if by order delivery date
     * @param string $dateFrom Starting date time
     * @param string $dateTo Starting date time
     *
     * @return array
     */
    public static function trademarks($filterTypeAdded = true, $dateFrom = null, $dateTo = null)
    {
        $conditionSQL = ' AND s.date_type = "' . ($filterTypeAdded ? 'creation' : 'delivery') . '"'
            . ($dateFrom !== null ? ' AND s.dt >= "' . DB::escape($dateFrom) . '"' : '')
            . ($dateTo !== null ? ' AND s.dt <= "' . DB::escape($dateTo) . '"' : '');

        $tms = DB::get_rows(
            'SELECT t.id, t.name,
                (
                    SELECT s.name
                    FROM cdb_suppliers AS s
                    WHERE s.id = (
                        SELECT cp.supplier_id
                        FROM products AS p
                        INNER JOIN cdb_products AS cp ON cp.id = p.id
                        WHERE p.tm_id = t.id
                        LIMIT 1
                    )
                ) AS supplier
            FROM tm AS t',
            function ($tm) use ($conditionSQL) {
                $tm += DB::get_row(
                    'SELECT IFNULL(SUM(s.sm_fl), 0) AS sm_fl, IFNULL(SUM(s.purchase_fl), 0) AS purchase_fl,
                    IFNULL(SUM(s.sm_ul), 0) AS sm_ul, IFNULL(SUM(s.purchase_ul), 0) AS purchase_ul
                    FROM __statistics_tm AS s
                    WHERE s.tm_id = ' . $tm['id'] . $conditionSQL
                );

                $tm['profit_fl'] = $tm['sm_fl'] - $tm['purchase_fl'];
                $tm['profit_ul'] = $tm['sm_ul'] - $tm['purchase_ul'];

                $tm['sm'] = $tm['sm_fl'] + $tm['sm_ul'];

                return $tm;
            }
        );

        usort($tms, function ($a, $b) {
            return $a['sm'] > $b['sm'] ? -1 : 1;
        });

        return $tms;
    }

    /**
     * Clients export
     *
     * @param string $dateAddedFrom Starting date time
     * @param string $dateAddedTo Starting date time
     * @param string $dateDeliveryFrom Starting delivery date time
     * @param string $dateDeliveryTo Starting delivery date time
     *
     * @return array
     */
    public static function clients($dateAddedFrom = null, $dateAddedTo = null, $dateDeliveryFrom = null, $dateDeliveryTo = null)
    {
        return DB::get_rows(
            'SELECT o.*,
                (SELECT CONCAT(IF(surname IS NULL, "", surname), " ", IF(name IS NULL, "", name) , " ", IF(patronymic IS NULL, "", patronymic)) FROM clients WHERE id = o.client_id) AS fio,
                COUNT(*) AS orders_count,
                SUM(o.order_cost) AS cost,
                SUM(o.order_profit) AS profit
            FROM (
                SELECT *, o.order_cost - o.purchase AS order_profit
                FROM (
                    SELECT o.client_id, o.cost AS order_cost,
                        ct.name AS client_type,
                        r.name AS region,
                        (SELECT DATE_FORMAT(co.added_date, "%Y-%m-%d") FROM orders AS co WHERE co.client_id = c.id AND co.status_id NOT IN (2, 3, 4, 5, 10, 11, 12) AND co.added_date IS NOT NULL ORDER BY co.order_number LIMIT 1) AS first_order,
                        (SELECT SUM(IF(oi.product_id = ' . Orders::PRESENT_PRODUCT_ID . ', 120, (SELECT p.purchase_price FROM products AS p WHERE p.id = oi.product_id)) * oi.quantity) FROM orders_items AS oi WHERE oi.order_number = o.order_number) AS purchase
                    FROM `orders` AS o
                    INNER JOIN clients AS c ON c.id = o.client_id
                    LEFT JOIN cdb_orders AS co ON co.order_number = o.order_number
                    LEFT JOIN shipping_regions AS r ON r.id = c.region_id
                    LEFT JOIN cdb_client_types AS ct ON ct.id = c.type_id
                    WHERE o.status_id NOT IN (2, 3, 4, 5, 10, 11, 12)
                        AND o.added_date IS NOT NULL
                        AND o.client_id > 0' .
            ($dateAddedFrom ? ' AND o.added_date >= "' . DB::escape($dateAddedFrom) . '" ' : '') .
            ($dateAddedTo ? ' AND o.added_date <= "' . DB::escape($dateAddedTo) . '" ' : '') .
            ($dateDeliveryFrom ? ' AND co.delivery_date >= "' . DB::escape($dateDeliveryFrom) . '" ' : '') .
            ($dateDeliveryTo ? ' AND co.delivery_date <= "' . DB::escape($dateDeliveryTo) . '" ' : '') .
            ') AS o
            ) AS o
            GROUP BY o.client_id
            ORDER BY SUM(o.order_profit) DESC'
        );
    }

    /**
     * Promo codes info export
     *
     * @param string $dateAddedFrom Starting date time
     * @param string $dateAddedTo End date time
     *
     * @return array
     */
    public static function promo($dateAddedFrom = null, $dateAddedTo = null)
    {

        return DB::get_rows(
            'SELECT DATE_FORMAT(o.added_date , "%d-%m-%Y") AS added_date, 
                    rpc.code, 
                    rpc.discount, 
                    IF(rpc.type = "uniq", "разовый", "мульти") as type,
                    DATE_FORMAT(rpc.create_date , "%d-%m-%Y") AS create_date,
                    DATE_FORMAT(rpc.end_date , "%d-%m-%Y") AS end_date,
                    COUNT(o.order_number) as quantity, 
                    SUM(o.cost) as total_cost, 
                    SUM(IF(c.type_id = 3, 1, 0)) AS fizik, 
                    SUM(IF(c.type_id = 3, o.cost, 0)) AS fizik_cost,
                    SUM(IF(c.type_id = 3, 0, 1)) AS yurik, 
                    SUM(IF(c.type_id = 3, 0, o.cost)) AS yurik_cost
             FROM rk_orders ro, orders o, clients c, rk_promo_codes rpc
             WHERE ro.order_number = o.order_number
             AND ro.promo_code_id    > 0
             AND o.client_id = c.id
             AND ro.promo_code_id = rpc.id
             AND o.auto_order = 0
             AND o.added_date IS NOT NULL' .
            ($dateAddedFrom ? ' AND o.added_date >= "' . DB::escape($dateAddedFrom) . '" ' : '') .
            ($dateAddedTo ? ' AND o.added_date <= "' . DB::escape($dateAddedTo) . '" ' : '') .
            'AND o.status_id IN(SELECT id FROM status WHERE ballance_type = "sold")
             GROUP BY rpc.code, DATE_FORMAT(o.added_date , "%d-%m-%Y")
             ORDER BY o.added_date'
        );
    }


    /**
     * Day orders report
     *
     * @param string $dateAddedFrom Starting date time
     * @param string $dateAddedTo End date time
     *
     * @return array
     */
    public static function sku_sales($dateAddedFrom = '01-01-2005', $dateAddedTo = '01-01-2005')
    {

        $date = date("d-m-Y", strtotime($dateAddedFrom));
        $dateTo = date("d-m-Y", strtotime($dateAddedTo));
        $where = ($date === $dateTo) ? 'WHERE DATE_FORMAT(o.added_date , "%d-%m-%Y") = "' . $date . '"' : 'WHERE DATE_FORMAT(o.added_date , "%d-%m-%Y") BETWEEN "' . $date . '" AND "' . $dateTo . '"';

        return DB::get_rows(
            'SELECT oi.product_id AS id,
                    REPLACE(CONCAT(IF(p.main_id > 0, (SELECT pp.name FROM products pp WHERE pp.id = p.main_id), p.name), " " , p.pack), "&nbsp;", " ") AS name, 
                    (SELECT tm.name FROM tm WHERE tm.id = p.tm_id) AS brand,
                    (SELECT cs.name FROM cdb_suppliers cs WHERE cs.id = cp.supplier_id) AS supplier,
                    SUM(oi.quantity) AS total_quantity,  
                    SUM(oi.final_price * oi.quantity) AS total_final_price, 
                    SUM(oi.price * oi.quantity) AS total_recomended_price,
                    SUM(IF(c.type_id = 3, oi.quantity, 0)) AS quantity_fiziki,
                    SUM(IF(c.type_id = 3, (oi.final_price * oi.quantity), 0)) AS final_price_fiziki,
                    SUM(IF(c.type_id = 3, 0, oi.quantity)) AS quantity_yuriki,
                    SUM(IF(c.type_id = 3, 0,  (oi.final_price * oi.quantity))) AS final_price_yuriki
            FROM orders o, clients c, orders_items oi LEFT JOIN products p ON oi.product_id = p.id
                                                      LEFT JOIN cdb_products cp ON oi.product_id = cp.id ' . $where . '
           
            AND o.status_id NOT IN (SELECT s.id FROM status s WHERE s.ballance_type = "no")
            AND o.auto_order = 0
            AND o.client_id = c.id
            AND oi.order_number = o.order_number 
            GROUP BY oi.product_id
            ORDER BY total_final_price DESC'
        );
    }

    /**
     * Cart report
     *
     * @param string $dateAddedFrom Starting date time
     * @param string $dateAddedTo End date time
     *
     * @return array
     */
    public static function cart_stat($dateAddedFrom = '01-01-2005', $dateAddedTo = '01-01-2005')
    {

        $date = date("Y-m-d", strtotime($dateAddedFrom));
        $dateTo = date("Y-m-d", strtotime($dateAddedTo));
        $dates = ' BETWEEN "' . $date . '" AND "' . $dateTo . '"';

        return DB::get_rows(
            'SELECT
            (SELECT COUNT(o.id)
            FROM rk_orders o
            WHERE o.start_date ' . $dates . ') AS all_rk_carts, 
            (SELECT COUNT(o.id)
            FROM rk_orders o
            WHERE o.start_date ' . $dates . ' AND o.order_number > 0) AS rk_comp_carts, 
            (SELECT COUNT(o.order_number)
            FROM orders o
            WHERE o.added_date ' . $dates . ' AND o.auto_order = 0) AS all_orders,
            (SELECT COUNT(o.order_number)
            FROM orders o
            WHERE o.added_date ' . $dates . ' AND o.status_id IN (5, 2 , 10) AND o.auto_order = 0) AS cancel_orders, 
            (SELECT COUNT(o.order_number)
            FROM orders o
            WHERE o.added_date ' . $dates . ' AND o.status_id IN (1, 8 , 9) AND o.auto_order = 0) AS ok_orders, 
            (SELECT COUNT(o.order_number)
            FROM orders o
            WHERE o.added_date ' . $dates . ' AND o.status_id IN (7, 11 , 12, 13, 4, 3) AND o.auto_order = 0) AS hold_orders'
        );
    }

    /**
     * By days export
     *
     * @param string $dateAddedFrom Starting added date time
     * @param string $dateAddedTo Starting added date time
     *
     * @return array
     */
    public static function days($dateAddedFrom = null, $dateAddedTo = null)
    {
        $range = DB::get_row(
            'SELECT UNIX_TIMESTAMP(DATE_FORMAT(MIN(o.added_date), "%Y-%m-%d 00:00:00")) AS from_day,
                UNIX_TIMESTAMP(DATE_FORMAT((MAX(o.added_date) + INTERVAL 1 DAY), "%Y-%m-%d 00:00:00")) AS to_day
                FROM orders AS o
                WHERE o.added_date IS NOT NULL' .
            ($dateAddedFrom ? ' AND o.added_date >= "' . DB::escape($dateAddedFrom) . '" ' : '') .
            ($dateAddedTo ? ' AND o.added_date <= "' . DB::escape($dateAddedTo) . '" ' : '')
        );

        $statusesSQL = implode(', ', [Orders::STATUS_ID_DONE, Orders::STATUS_ID_STOCK_DONE, Orders::STATUS_ID_DELIVERY]);

        $result = [];

        for ($day = (int)$range['from_day'], $to = (int)$range['to_day']; $day < $to; $day += 3600 * 24) {
            $dt = date('Y-m-d', $day);

            $added = (int)DB::get_field(
                'cnt',
                'SELECT COUNT(o.order_number) AS cnt FROM orders AS o WHERE o.added_date IS NOT NULL AND o.added_date BETWEEN "' . $dt . ' 00:00:00" AND "' . $dt . ' 23:59:59"'
            );

            $delivered = (int)DB::get_field(
                'cnt',
                'SELECT COUNT(o.order_number) AS cnt
                    FROM orders AS o
                    LEFT JOIN cdb_orders AS co ON co.order_number = o.order_number
                    WHERE o.added_date IS NOT NULL
                        AND o.added_date BETWEEN "' . $dt . ' 00:00:00" AND "' . $dt . ' 23:59:59"
                        AND co.delivery_date IS NOT NULL
                        AND o.status_id IN (' . $statusesSQL . ')'
            );

            $result[] = [
                'day' => $dt,
                'added_count' => $added,
                'delivery_count' => $delivered,
                'proc' => ($added ? ((int)($delivered / $added * 100)) : 0) . '%',
            ];
        }

        return $result;
    }

    /**
     * Caches trademarks statistic data
     *
     * @param string $dateFrom Starting date (YYYY-MM-DD)
     * @param string $dateTo Ending date (YYYY-MM-DD)
     */
    public static function cache_tms($dateFrom, $dateTo)
    {
        $dateFromSQL = DB::escape($dateFrom);
        $dateToSQL = DB::escape($dateTo);

        $conditionTypes = [
            'creation' => 'o.added_date',
            'delivery' => 'co.delivery_date',
        ];

        $clientTypes = [
            'ul' => [
                'types' => implode(', ', [
                    Users::CLIENT_TYPE_MASSEUR,
                    Users::CLIENT_TYPE_COMPANY,
                ]),
                'statuses' => implode(', ', [
                    Orders::STATUS_ID_DONE,
                    Orders::STATUS_ID_ON_STOCK,
                    Orders::STATUS_ID_STOCK_DONE,
                    Orders::STATUS_ID_DELIVERY,
                    Orders::STATUS_ID_READY_FOR_ASSEMBLY,
                ]),
            ],
            'fl' => [
                'types' => implode(', ', [
                    Users::CLIENT_TYPE_INDIVIDUAL,
                ]),
                'statuses' => implode(', ', [
                    Orders::STATUS_ID_DONE,
                    Orders::STATUS_ID_ON_STOCK,
                    Orders::STATUS_ID_STOCK_DONE,
                    Orders::STATUS_ID_DELIVERY,
                    Orders::STATUS_ID_READY_FOR_ASSEMBLY,
                    Orders::STATUS_ID_CORRUPT,
                ]),
            ],
        ];

        DB::get_rows(
            'SELECT * FROM
            (
                SELECT ADDDATE("1970-01-01", t4.i * 10000 + t3.i * 1000 + t2.i * 100 + t1.i * 10 + t0.i) AS dt
                FROM
                (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t0,
                (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t1,
                (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t2,
                (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t3,
                (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t4
            ) AS dates
            WHERE dates.dt BETWEEN "' . $dateFromSQL . '" AND "' . $dateToSQL . '"
            ORDER BY dates.dt',
            function ($date) use ($conditionTypes, $clientTypes, $dateFromSQL, $dateToSQL) {
                $tms = [];

                foreach ($conditionTypes as $conditionType => $conditionField) {
                    $tms[$conditionType] = [];

                    foreach ($clientTypes as $clientTypeKey => $clientTypeValues) {
                        DB::get_rows(
                            '
                            SELECT DISTINCT p.tm_id AS id
                            FROM products AS p
                            WHERE p.id IN (
                                SELECT oi.product_id
                                FROM orders_items AS oi
                                INNER JOIN orders AS o ON o.order_number = oi.order_number
                                    AND o.added_date IS NOT NULL
                                    AND o.status_id IN (' . $clientTypeValues['statuses'] . ')
                                INNER JOIN clients AS c ON c.id = o.client_id AND c.type_id IN (' . $clientTypeValues['types'] . ')
                                LEFT JOIN cdb_orders AS co ON co.order_number = o.order_number
                                WHERE ' . $conditionField . ' >= "' . $date['dt'] . ' 00:00:00" AND ' . $conditionField . ' <= "' . $date['dt'] . ' 23:59:59"
                                    AND oi.quantity IS NOT NULL
                                    AND oi.final_price IS NOT NULL
                                    AND oi.quantity > 0
                                    AND oi.final_price > 0
                            )
                            ',
                            function ($tm) use ($date, $clientTypes, $conditionType, $conditionField, $clientTypeKey, $clientTypeValues, &$tms) {
                                $query = 'SELECT ' . $tm['id'] . ' AS id, (
                                        SELECT SUM(oi.final_price * oi.quantity)
                                        FROM orders_items AS oi
                                        WHERE oi.order_number IN (
                                            SELECT o.order_number
                                            FROM orders AS o
                                            INNER JOIN clients AS c ON c.id = o.client_id AND c.type_id IN (' . $clientTypeValues['types'] . ')
                                            LEFT JOIN cdb_orders AS co ON co.order_number = o.order_number
                                            WHERE o.status_id IN (' . $clientTypeValues['statuses'] . ')
                                                AND o.added_date IS NOT NULL
                                                AND ' . $conditionField . ' >= "' . $date['dt'] . ' 00:00:00" AND ' . $conditionField . ' <= "' . $date['dt'] . ' 23:59:59"
                                        )
                                            AND oi.product_id IN (SELECT p.id FROM products AS p WHERE p.tm_id = ' . $tm['id'] . ')
                                            AND oi.quantity IS NOT NULL
                                            AND oi.final_price IS NOT NULL
                                            AND oi.quantity > 0
                                            AND oi.final_price > 0
                                    ) AS sm,
                                    (
                                        SELECT SUM((SELECT p.purchase_price FROM products AS p WHERE p.id = oi.product_id) * oi.quantity)
                                        FROM orders_items AS oi
                                        WHERE oi.order_number IN (
                                            SELECT o.order_number
                                            FROM orders AS o
                                            LEFT JOIN cdb_orders AS co ON co.order_number = o.order_number
                                            INNER JOIN clients AS c ON c.id = o.client_id AND c.type_id IN (' . $clientTypeValues['types'] . ')
                                            WHERE o.status_id IN (' . $clientTypeValues['statuses'] . ')
                                                AND o.added_date IS NOT NULL
                                                AND ' . $conditionField . ' >= "' . $date['dt'] . ' 00:00:00" AND ' . $conditionField . ' <= "' . $date['dt'] . ' 23:59:59"
                                        )
                                            AND oi.product_id IN (SELECT p.id FROM products AS p WHERE p.tm_id = ' . $tm['id'] . ')
                                            AND oi.quantity IS NOT NULL
                                            AND oi.final_price IS NOT NULL
                                            AND oi.quantity > 0
                                            AND oi.final_price > 0
                                    ) AS purchase';

                                $info = DB::get_row($query);

                                $info['sm_' . $clientTypeKey] = (float)$info['sm'];
                                $info['purchase_' . $clientTypeKey] = (float)$info['purchase'];

                                $info['profit_' . $clientTypeKey] = $info['sm'] - $info['purchase'];

                                if (!isset($tms[$conditionType][$tm['id']])) {
                                    $tms[$conditionType][$tm['id']] = $info;
                                } else {
                                    $tms[$conditionType][$tm['id']] += $info;
                                }
                            }
                        );
                    }
                }

                foreach ($tms as $conditionType => $trademarks) {
                    foreach ($trademarks as $tm) {
                        if (!isset($tm['sm_fl'])) {
                            $tm['sm_fl'] = 0;
                            $tm['purchase_fl'] = 0;
                        }

                        if (!isset($tm['sm_ul'])) {
                            $tm['sm_ul'] = 0;
                            $tm['purchase_ul'] = 0;
                        }

                        DB::query(
                            'REPLACE INTO __statistics_tm (dt, tm_id, date_type, sm_fl, purchase_fl, sm_ul, purchase_ul)
                            VALUES ("' . $date['dt'] . '", ' . $tm['id'] . ', "' . $conditionType . '", ' . floatraw($tm['sm_fl']) . ', ' . floatraw($tm['purchase_fl']) . ', ' . floatraw($tm['sm_ul']) . ', ' . floatraw($tm['purchase_ul']) . ')'
                        );
                    }
                }
            }
        );
    }

    /**
     * Returns fields for given statistics type
     *
     * @param string $type Statistics type
     *
     * @return array
     */
    public static function fields($type)
    {
        switch ($type) {
            case 'orders':
                return [
                    [
                        'field' => 'order_number',
                        'name' => 'Номер заказа',
                    ],
                    [
                        'field' => 'added_date',
                        'name' => 'Дата создания',
                    ],
                    [
                        'field' => 'delivery_date',
                        'name' => 'Дата доставки',
                    ],
                    [
                        'field' => 'region',
                        'name' => 'Регион',
                    ],
                    [
                        'field' => 'delivery_date',
                        'name' => 'Дата доставки',
                    ],
                    [
                        'field' => 'delivery',
                        'name' => 'Стоимость доставки',
                    ],
                    [
                        'field' => 'delivery_compensation',
                        'name' => 'Компенсация доставки',
                    ],
                    [
                        'field' => 'discount',
                        'name' => 'Скидка',
                    ],
                    [
                        'field' => 'cost',
                        'name' => 'Стоимость заказа',
                    ],
                    [
                        'field' => 'purchase',
                        'name' => 'Цена закупки',
                    ],
                    [
                        'field' => 'profit',
                        'name' => 'Маржа',
                    ],
                    [
                        'field' => 'manager',
                        'name' => 'Менеджер',
                    ],
                    [
                        'field' => 'client_id',
                        'name' => 'ID клиента',
                    ],
                    [
                        'field' => 'client_name',
                        'name' => 'Клиент',
                    ],
                    [
                        'field' => 'client_type',
                        'name' => 'Тип клиента',
                    ],
                    [
                        'field' => 'agent',
                        'name' => 'Агент',
                    ],
                    [
                        'field' => 'carrier',
                        'name' => 'Тип доставки',
                    ],
                    [
                        'field' => 'courier',
                        'name' => 'Наш курьер',
                    ],
                ];

                break;

            case 'trademarks':
                $types = [
                    'ul' => 'Юр. лица',
                    'fl' => 'Физ. лица',
                ];

                $fields = [
                    'sm' => 'Сумма продаж',
                    'purchase' => 'Цена закупки',
                    'profit' => 'Маржа',
                ];

                $return = [
                    [
                        'field' => 'name',
                        'name' => 'Торговая марка',
                    ],
                    [
                        'field' => 'sm',
                        'name' => 'Сумма продаж',
                    ],
                ];

                foreach ($types as $key => $name) {
                    foreach ($fields as $fieldKey => $fieldName) {
                        $return[] = [
                            'field' => $fieldKey . '_' . $key,
                            'name' => $fieldName . ' (' . $name . ')',
                        ];
                    }
                }

                $return[] = [
                    'field' => 'supplier',
                    'name' => 'Поставщик',
                ];

                return $return;

                break;

            case 'clients':
                return [
                    [
                        'field' => 'client_id',
                        'name' => 'ID клиента',
                    ],
                    [
                        'field' => 'fio',
                        'name' => 'ФИО',
                    ],
                    [
                        'field' => 'client_type',
                        'name' => 'Тип клиента',
                    ],
                    [
                        'field' => 'region',
                        'name' => 'Регион',
                    ],
                    [
                        'field' => 'first_order',
                        'name' => 'Дата первого заказа',
                    ],
                    [
                        'field' => 'orders_count',
                        'name' => 'Количество заказов',
                    ],
                    [
                        'field' => 'cost',
                        'name' => 'Сумма заказов',
                    ],
                    [
                        'field' => 'profit',
                        'name' => 'Маржа',
                    ],
                ];

                break;

            case 'days':
                return [
                    [
                        'field' => 'day',
                        'name' => 'Дата',
                    ],
                    [
                        'field' => 'added_count',
                        'name' => 'Поступило заказов',
                    ],
                    [
                        'field' => 'delivery_count',
                        'name' => 'Доставлено заказов',
                    ],
                    [
                        'field' => 'proc',
                        'name' => 'Поступило / Собрано',
                    ],
                ];

                break;

            case 'promo':
                return [
                    [
                        'field' => 'added_date',
                        'name' => 'Дата',
                    ],
                    [
                        'field' => 'code',
                        'name' => 'Промокод',
                    ],
                    [
                        'field' => 'discount',
                        'name' => 'Скидка %',
                    ],
                    [
                        'field' => 'type',
                        'name' => 'Тип купона',
                    ],
                    [
                        'field' => 'create_date',
                        'name' => 'Дата создания',
                    ],
                    [
                        'field' => 'end_date',
                        'name' => 'Действует ДО',
                    ],
                    [
                        'field' => 'quantity',
                        'name' => 'Количество заказов',
                    ],
                    [
                        'field' => 'total_cost',
                        'name' => 'Сумма заказов',
                    ],
                    [
                        'field' => 'fizik',
                        'name' => 'Колличество ФЛ',
                    ],
                    [
                        'field' => 'fizik_cost',
                        'name' => 'Цена заказов ФЛ',
                    ],
                    [
                        'field' => 'yurik',
                        'name' => 'Колличество ЮЛ',
                    ],
                    [
                        'field' => 'yurik_cost',
                        'name' => 'Цена заказов ЮЛ',
                    ],
                ];

                break;


            case 'sku_sales':
                return [
                    [
                        'field' => 'id',
                        'name' => 'ID',
                    ],
                    [
                        'field' => 'name',
                        'name' => 'Наименование',
                    ],
                    [
                        'field' => 'brand',
                        'name' => 'Торговая марка',
                    ],
                    [
                        'field' => 'supplier',
                        'name' => 'Поставщик',
                    ],
                    [
                        'field' => 'total_recomended_price',
                        'name' => 'Рекомендованная цена',
                    ],
                    [
                        'field' => 'total_quantity',
                        'name' => 'Продано шт.',
                    ],
                    [
                        'field' => 'total_final_price',
                        'name' => 'Фактическая цена',
                    ],
                    [
                        'field' => 'quantity_yuriki',
                        'name' => 'Колличество ЮЛ',
                    ],
                    [
                        'field' => 'final_price_yuriki',
                        'name' => 'Цена ЮЛ',
                    ],
                    [
                        'field' => 'quantity_fiziki',
                        'name' => 'Колличество ФЛ',
                    ],
                    [
                        'field' => 'final_price_fiziki',
                        'name' => 'Цена ФЛ',
                    ],
                ];

                break;

            case 'cart_stat':
                return [
                    [
                        'field' => 'all_rk_carts',
                        'name' => 'Все корзины',
                    ],
                    [
                        'field' => 'rk_comp_carts',
                        'name' => 'Корзины переросшие в заказ',
                    ],
                    [
                        'field' => 'all_orders',
                        'name' => 'Все заказы',
                    ],
                    [
                        'field' => 'cancel_orders',
                        'name' => 'Отмененные заказы',
                    ],
                    [
                        'field' => 'ok_orders',
                        'name' => 'Выполненные заказы',
                    ],
                    [
                        'field' => 'hold_orders',
                        'name' => 'Заказы в работе',
                    ]
                ];

                break;
        }
    }
}
