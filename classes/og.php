<?php

/**
 * Open Graph
 */
class og
{
    /**
     * @var string img url
     */
    private static $img_full_url = IMAGES_DOMAIN_FULL . '/images/prod_photo/';

    /**
     * @var array open graph data
     */
    private static $_og_array = [];

    /**
     * Add elements to open graph array
     * 
     * @param string $property OG $property
     * @param string $content OG $content
     * 
     * @return bool
     */
    private static function add_info($property, $content)
    {
        self::$_og_array[] = array('property' => $property, 'content' => $content); 
    }

    /**
     * Generate data for Open Graph meta tags in product
     * @param  array $product_data product data array
     * @return array array of OG data
     */
    static function product($product_data)
    {

        self::add_info('type', 'website');
        self::add_info('url', DOMAIN_FULL . '/product/' . $product_data['url']);
        self::add_info('locale', 'ru_RU');
        self::add_info('title', export::anti_typograf($product_data['h1']));
        self::add_info('description', $product_data['seo_desc']);

        if($product_data['pic_quant'] > 0) {

            self::add_info('image', self::$img_full_url . Product::img_url($product_data['id'] . '_1.jpg', $product_data['url']));
            self::add_info('image:type', 'image/jpeg');
            self::add_info('image:width', 300);
            self::add_info('image:height', 300);

            self::add_info('image', self::$img_full_url . Product::img_url($product_data['id'] . '_1_max.jpg', $product_data['url']));
            self::add_info('image:type', 'image/jpeg');
            self::add_info('image:width', 1000);
            self::add_info('image:height', 1000);
          

        } else {

            self::add_info('image', self::$img_full_url . Product::img_url($product_data['src'], $product_data['url']));
            self::add_info('image:type', 'image/jpeg');
            self::add_info('image:width', 300);
            self::add_info('image:height', 300);
        }

        return self::$_og_array;
    }

    /**
     * Generate data for Open Graph meta tags in category
     * @param  array $category_data category data array
     * @return array array of OG data
     */
    static function category($category_data)
    {

        self::add_info('type', 'website');
        self::add_info('url', DOMAIN_FULL . '/category/' . $category_data['main']['_full_url']);
        self::add_info('locale', 'ru_RU');
        self::add_info('title', html_entity_decode($category_data['main']['h1']) . ' на выгодных условиях от интернет магазина Роскосметика');

        if (isset($category_data['main']['og_description']))
        {
            self::add_info('description', $category_data['main']['og_description']);

            if(isset($category_data['products'][0]))
            {
                self::add_info('image', self::$img_full_url  . 's_' . Product::img_url($category_data['products'][0]['src'], $category_data['products'][0]['url']));
                self::add_info('image:type', 'image/jpeg');
                self::add_info('image:width', 185);
                self::add_info('image:height', 185);
            }
        }

        return self::$_og_array;
    }

    /**
     * Generate data for Open Graph meta tags in tm/lines
     * @param  array $tm_data tm data array
     * @param  bool  $ctm flag tm in category
     * @return array array of OG data
     */
    static function tm($tm_data, $ctm = FALSE)
    {

        self::add_info('type', 'website');

        if ($ctm) 
        {
            self::add_info('url', DOMAIN_FULL . '/category-tm/' . $tm_data['category']['_full_url'] . '/' . $tm_data['tm']['url']);
        } else 
        {
            self::add_info('url', DOMAIN_FULL . '/tm/' . $tm_data['tm']['tm_url'] . ($tm_data['line'] ? '/' . $tm_data['tm']['url'] : ''));
        }

        self::add_info('locale', 'ru_RU');

         if (isset($tm_data['tm']['og_title']))
        {
            self::add_info('title', $tm_data['tm']['og_title']);
        }

        if (isset($tm_data['tm']['og_description']))
        {
            self::add_info('description', $tm_data['tm']['og_description']);

            if(isset($tm_data['products'][0]))
            {
                self::add_info('image', self::$img_full_url . 's_' . Product::img_url($tm_data['products'][0]['src'], $tm_data['products'][0]['url']));
                self::add_info('image:type', 'image/jpeg');
                self::add_info('image:width', 185);
                self::add_info('image:height', 185);
            }
        }

        return self::$_og_array;
    }

     /**
     * Generate data for Open Graph meta tags in index page
     * @return array array of OG data
     */
    static function index_page()
    {

        self::add_info('type', 'website');
        self::add_info('url', DOMAIN_FULL);
        self::add_info('locale', 'ru_RU');
        self::add_info('title', 'Роскосметика - интернет-магазин профессиональной уходовой косметики');
        self::add_info('description', 'Выбрать и заказать натуральную косметику в интернет-магазине Роскосметика с доставкой по Москве, Санкт-Петербургу и всей России! В нашем каталоге огромный ассортимент, постоянные скидки и отличные цены!');
        self::add_info('image', DOMAIN_FULL . '/images/logo_2018.png');

        return self::$_og_array;
    }

}