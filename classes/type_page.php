<?php

class type_page
{
    /**
     * Returns type pages list
     *
     * @param bool $visibleOnly Return only visible type pages
     * @param bool $productsCount Include products count in "products" field
     * @param bool $sortByProductsDirection Sort by "products" field direction
     *
     * @return array
     */
    public static function get_pages($visibleOnly = true, $productsCount = false, $sortByProductsDirection = null)
    {
        return DB::get_rows(
            'SELECT * FROM (
                SELECT p.*' . ($productsCount ? ', (SELECT COUNT(ptp.product_id) FROM rk_prod_type_page AS ptp WHERE ptp.type_page_id = p.id) AS products' : '') . '
                FROM rk_type_page AS p ' .
                ($visibleOnly ? 'WHERE p.visible = 1' : '') . '
            ) AS p
            ORDER BY ' . ($sortByProductsDirection !== null ? ('p.products ' . ($sortByProductsDirection ? 'ASC' : 'DESC')) : 'p.name')
        );
    }

    /**
     * Returns type page row
     *
     * @param int $id Type page ID
     *
     * @return array
     */
    public static function get_page($id)
    {
        return DB::get_row('SELECT p.* FROM rk_type_page AS p WHERE p.id = ' . ((int)$id));
    }

    /**
     * Returns type page row by url
     *
     * @param string $url Type page URL
     * @param bool $ajax AJAX request
     *
     * @return array
     */
    public static function output($url, $ajax = false)
    {
        $data = DB::get_row('SELECT p.* FROM rk_type_page AS p WHERE p.url = "' . DB::escape($url) . '" AND p.visible = 1');

        if (!$data) {
            return null;
        }

        $data = [
            'type_page' => $data,
        ];

        $filter_condition = $ajax ? Product::filter_condition('p') : '';
        $price_condition = $ajax ? Product::price_condition('p') : '';
        $tm_condition = $ajax ? Product::tm_condition('p') : '';
        $country_condition = $ajax ? Product::country_condition('p') : '';

        $innerJoins = 'INNER JOIN rk_prod_type_page AS ptp ON ptp.type_page_id = ' . $data['type_page']['id'] . ' AND ptp.product_id = p.id
                    INNER JOIN rk_prod_desc pd  ON (p.id = pd.id)
                    INNER JOIN tm ON tm.id = p.tm_id';

        $where = '(p.visible = 1 OR IF((SELECT COUNT(pp.id) FROM products AS pp WHERE pp.visible = 1 AND pp.main_id = p.id), 1, 0))
                    AND p.main_id = 0 AND (p.active = 1 OR p.price = 0 AND p.new = 1) ';

        $whereFull = $where . $filter_condition . $tm_condition . $price_condition . $country_condition;

        Pagination::setValues((int)DB::get_field('cnt', 'SELECT COUNT(p.id) AS cnt FROM products AS p ' . $innerJoins . ' WHERE ' . $whereFull));

        $query = 'SELECT p.id, IF(p.url = "", p.id, p.url ) url, p.main_id, p.name, p.short_description, p.description, p.price, p.special_price, p.pack, p.new, p.hit, p.available, p.for_proff, 
                    p._max_discount AS discount, p._max_discount_days  AS is_sales,
                    pd.eng_name, pd.use, pd.ingredients, pd.synonym, p.tm_id,
                    ps.title, ps.description seo_desc, ps.keywords,
                    pp.src, pp.alt, pp.title photo_title,
                    tm.name tm, IF(tm.url = "", tm.id, tm.url ) tm_url, td.country,
                    (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.hit = 1 LIMIT 1) AS pack_hit,
                    (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.new = 1 LIMIT 1) AS pack_new,
                    IF(p.active = 0 AND p.price = 0 AND p.new = 1 AND p.visible = 1, 1, 0) AS soon
                    FROM products AS p
                    ' . $innerJoins . '
                    LEFT JOIN rk_prod_seo ps    ON (ps.id = p.id)
                    LEFT JOIN rk_prod_photo pp  ON (pp.id = p.id)
                    LEFT JOIN rk_tm_desc td     ON (p.tm_id = td.id)
                    WHERE ' . $whereFull . '
                    ORDER BY ' . Product::filter_order('p', false, []) .
            Pagination::sqlLimit();

        $products = DB::get_rows($query);

        $data['products'] = filters::set_products_info($products);

        if (!$ajax) {
            $data['tms'] = tm::type_page_tms($data['type_page']['id']);

            $data['reviews'] = review::product_reviews_type_page($data['type_page']['id'], 2);

            if ($data['reviews']) {
                $data['reviews_title'] = helpers::grammatical_case($data['type_page']['h1'] ?: $data['type_page']['name'], helpers::CASE_TYPE_PREPOSITIONAL_EXTENDED, true);
            }

            $data['filters'] = [];

            $main_filters = DB::get_rows('SELECT f.id, f.name FROM rk_filters AS f WHERE f.parent_id = 0 AND f.visible = 1 ORDER BY f.name');

            for ($i = 0, $n = count($main_filters); $i < $n; ++$i) {
                $query = 'SELECT f.id, f.name
                      FROM products AS p
                      INNER JOIN rk_prod_filter AS pf ON pf.prod_id = p.id
                      INNER JOIN rk_filters AS f ON f.id = pf.filter_id AND f.visible = 1 AND f.parent_id = ' . $main_filters[$i]['id'] . '
                      INNER JOIN rk_prod_type_page AS ptp ON ptp.product_id = p.id AND ptp.type_page_id = ' . $data['type_page']['id'] . '
                      WHERE p.visible = 1
                          AND p.active = 1
                          AND f.visible = 1
                      GROUP BY f.id
                      ORDER BY f.name';

                $filters = db::get_rows($query);

                if (count($filters) > 1) {
                    $data['filters'][$main_filters[$i]['name']] = helpers::explode_filters($filters);
                }
            }

            $hasH1 = !!$data['type_page']['h1'];

            if (!$hasH1) {
                $data['type_page']['h1'] = $data['type_page']['name'];
            }

            if (!$data['type_page']['title']) {
                $data['type_page']['title'] = 'Купить ' .
                    helpers::grammatical_case(
                        helpers::numeric_name(
                            $hasH1 ? $data['type_page']['h1'] : $data['type_page']['name'],
                            $hasH1 ? $data['type_page']['h1_alt_number'] : $data['type_page']['name_alt_number'],
                            $hasH1 ? $data['type_page']['h1_plural'] : $data['type_page']['name_plural'],
                            false
                        ),
                        helpers::CASE_TYPE_ACCUSATIVE,
                        true
                    ) .
                    ' в интернет-магазине в Москве. Цены на ' .
                    helpers::grammatical_case(
                        helpers::numeric_name(
                            $hasH1 ? $data['type_page']['h1'] : $data['type_page']['name'],
                            $hasH1 ? $data['type_page']['h1_alt_number'] : $data['type_page']['name_alt_number'],
                            $hasH1 ? $data['type_page']['h1_plural'] : $data['type_page']['name_plural'],
                            true
                        ),
                        helpers::CASE_TYPE_ACCUSATIVE,
                        true
                    ) .
                    ' в каталоге Роскосметики';
            }

            if (!$data['type_page']['description']) {
                $data['type_page']['description'] = 'Выбрать и заказать ' . helpers::grammatical_case($data['type_page']['h1'], helpers::CASE_TYPE_ACCUSATIVE, true) . ' в интернет-магазине Роскосметика с доставкой по Москве, Санкт-Петербургу, России! В нашем каталоге огромный ассортимент, скидки и лучшие цены!';
            }

            $data['base_links'] = link_base::get_links(link_base::TYPE_PAGE_TYPE_ID, $data['type_page']['id']);

            if (count($products) <= 12) {
                $data['popular_products'] = Product::popular_products_block(8, null, null, $data['type_page']['id']);
            }
        } else {
            $data['filters'] = $data['products'] ? filters::available_filters($where . $tm_condition . $price_condition . $country_condition, $innerJoins) : null;
        }

        $data['filter_tms'] = $data['products'] ? filters::available_tms($where . $filter_condition . $price_condition . $country_condition, $innerJoins, 'p', $ajax) : null;
        $data['filter_prices'] = $data['products'] ? filters::price_range($where . $filter_condition . $tm_condition . $country_condition, $innerJoins) : null;
        $data['filter_countries'] = $data['products'] ? filters::available_countries($where . $filter_condition . $price_condition . $tm_condition, $innerJoins, 'p', $ajax) : null;

        return $data;
    }

    /**
     * Returns type page by URL
     *
     * @param string $url URL
     *
     * @return array
     */
    public static function get_type_page_by_url($url)
    {
        return DB::get_row('SELECT p.* FROM rk_type_page AS p WHERE p.url = "' . DB::escape($url) . '" LIMIT 1');
    }

    /**
     * Returns visible products count for given type page (And optionally trademark)
     *
     * @param int $typePageID Type page ID
     * @param int $tmID Trademark ID
     *
     * @return int
     */
    public static function products_count($typePageID, $tmID = null)
    {
        return (int)DB::get_field(
            'cnt',
            'SELECT COUNT(p.id) AS cnt
            FROM rk_prod_type_page AS ptp
            INNER JOIN products AS p ON p.id = ptp.product_id AND p.active = 1 AND p.visible = 1
            WHERE ptp.type_page_id = ' . (int)$typePageID .
            ($tmID !== null ? ' AND p.tm_id = ' . $tmID : '')
        );
    }

    /**
     * Returns type page - trademark info by URLs
     *
     * @param string $typePageUrl Type page URL
     * @param string $tmUrl Trademark URL
     *
     * @return array
     */
    public static function get_type_page_tm_by_url($typePageUrl, $tmUrl)
    {
        return DB::get_row(
            'SELECT tpt.h1,
                p.id AS type_page_id, p.name AS type_page_name, p.name_plural AS type_page_name_plural, p.name_alt_number AS type_page_name_alt_number, p.h1 AS type_page_h1, p.h1_plural AS type_page_h1_plural, p.h1_alt_number AS type_page_h1_alt_number, p.name_on_products AS type_page_name_on_products,
                t.id AS tm_id, t.name AS tm_name, td.alt_name AS tm_alt_name
            FROM rk_tm_in_type_page AS tpt
            INNER JOIN rk_type_page AS p ON p.id = tpt.type_page_id AND p.url = "' . DB::escape($typePageUrl) . '"
            INNER JOIN tm AS t ON t.id = tpt.tm_id AND t.url = "' . DB::escape($tmUrl) . '"
            INNER JOIN rk_tm_desc AS td ON td.id = t.id'
        );
    }

    /**
     * Outputs /kosmetika/typePage/tm page
     *
     * @param string $typePageUrl Type page URL
     * @param string $tmUrl Trademark URL
     * @param bool $ajax AJAX request
     *
     * @return array
     */
    public static function output_tm($typePageUrl, $tmUrl, $ajax)
    {
        if (is_numeric($tmUrl)) {
            $tm = tm::get_by_id((int)$tmUrl);

            if (!$tm) {
                return null;
            }

            if ($tm['url']) {
                redirect('/kosmetika/' . $typePageUrl . '/' . $tm['url']);
            } else {
                $tm['url'] = $tm['id'];
            }
        }

        $data = [];

        $data['info'] = DB::get_row(
            'SELECT tp.*,
                p.name AS type_page_name, p.name_plural AS type_page_name_plural, p.name_alt_number AS type_page_name_alt_number, p.h1 AS type_page_h1, p.h1_plural AS type_page_h1_plural, p.h1_alt_number AS type_page_h1_alt_number, p.url AS type_page_url, p.name_on_products AS type_page_name_on_products,
                t.name AS tm_name, td.alt_name AS tm_alt_name, IF(t.url != "", t.url, t.id) AS tm_url
            FROM rk_tm_in_type_page AS tp
            INNER JOIN rk_type_page AS p ON p.id = tp.type_page_id AND p.url = "' . DB::escape($typePageUrl) . '" AND p.visible = 1
            INNER JOIN tm AS t ON t.id = tp.tm_id AND t.url = "' . DB::escape($tmUrl) . '" AND t.visible = 1
            INNER JOIN rk_tm_desc AS td ON td.id = t.id
            WHERE tp._has_products = 1'
        );

        if (!$data['info']) {
            return null;
        }

        $filter_condition = $ajax ? Product::filter_condition('p') : '';
        $price_condition = $ajax ? Product::price_condition('p') : '';
        $tm_condition = $ajax ? Product::tm_condition('p') : '';

        $innerJoins = 'INNER JOIN rk_prod_type_page AS ptp ON ptp.type_page_id = ' . $data['info']['type_page_id'] . ' AND ptp.product_id = p.id
                    INNER JOIN rk_prod_desc pd  ON (p.id = pd.id)
                    INNER JOIN tm ON tm.id = p.tm_id AND tm.id = ' . $data['info']['tm_id'];

        $where = '(p.visible = 1 OR IF((SELECT COUNT(pp.id) FROM products AS pp WHERE pp.visible = 1 AND pp.main_id = p.id), 1, 0))
                    AND p.main_id = 0 AND (p.active = 1 OR p.price = 0 AND p.new = 1) ';

        $whereFull = $where . $filter_condition . $tm_condition . $price_condition;

        Pagination::setValues((int)DB::get_field('cnt', 'SELECT COUNT(p.id) AS cnt FROM products AS p ' . $innerJoins . ' WHERE ' . $whereFull));

        $query = 'SELECT p.id, IF(p.url = "", p.id, p.url ) url, p.main_id, p.name, p.short_description, p.description, p.price, p.special_price, p.pack, p.new, p.hit, p.available, p.for_proff,
                    p._max_discount AS discount, p._max_discount_days  AS is_sales,
                    pd.eng_name, pd.use, pd.ingredients, pd.synonym, p.tm_id,
                    ps.title, ps.description seo_desc, ps.keywords,
                    pp.src, pp.alt, pp.title photo_title,
                    tm.name tm, IF(tm.url = "", tm.id, tm.url ) tm_url, td.country,
                    (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.hit = 1 LIMIT 1) AS pack_hit,
                    (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.new = 1 LIMIT 1) AS pack_new,
                    IF(p.active = 0 AND p.price = 0 AND p.new = 1 AND p.visible = 1, 1, 0) AS soon
                    FROM products AS p
                    ' . $innerJoins . '
                    LEFT JOIN rk_prod_seo ps    ON (ps.id = p.id)
                    LEFT JOIN rk_prod_photo pp  ON (pp.id = p.id)
                    LEFT JOIN rk_tm_desc td     ON (p.tm_id = td.id)
                    WHERE ' . $whereFull . '
                    ORDER BY ' . Product::filter_order('p', false, []) .
            Pagination::sqlLimit();

        $products = DB::get_rows($query);

        $data['products'] = filters::set_products_info($products);

        if (!$ajax) {
            $data['tms'] = tm::type_page_tms($data['info']['type_page_id'], $data['info']['tm_id']);

            $data['reviews'] = review::product_reviews_type_page_tm($data['info']['type_page_id'], $data['info']['tm_id'], 2);

            if ($data['reviews']) {
                $data['reviews_title'] = helpers::grammatical_case($data['info']['h1'] ?: ($data['info']['type_page_h1'] ?: $data['info']['type_page_name']), helpers::CASE_TYPE_PREPOSITIONAL_EXTENDED, true) . ($data['info']['h1'] ? '' : ' ' . $data['info']['tm_name']);
            }

            $data['base_links'] = link_base::get_links(link_base::TYPE_PAGE_TRADEMARK_TYPE_ID, $data['info']['type_page_id'], $data['info']['tm_id']);

            $data['filters'] = [];

            $main_filters = DB::get_rows('SELECT f.id, f.name FROM rk_filters AS f WHERE f.parent_id = 0 AND f.visible = 1 ORDER BY f.name');

            for ($i = 0, $n = count($main_filters); $i < $n; ++$i) {
                $query = 'SELECT f.id, f.name
                      FROM products AS p
                      INNER JOIN rk_prod_filter AS pf ON pf.prod_id = p.id
                      INNER JOIN rk_filters AS f ON f.id = pf.filter_id AND f.visible = 1 AND f.parent_id = ' . $main_filters[$i]['id'] . '
                      INNER JOIN rk_prod_type_page AS ptp ON ptp.product_id = p.id AND ptp.type_page_id = ' . $data['info']['type_page_id'] . '
                      INNER JOIN tm AS t ON t.id = p.tm_id AND t.id = ' . $data['info']['tm_id'] . '
                      WHERE p.visible = 1
                          AND p.active = 1
                          AND f.visible = 1
                      GROUP BY f.id
                      ORDER BY f.name';

                $filters = db::get_rows($query);

                if (count($filters) > 1) {
                    $data['filters'][$main_filters[$i]['name']] = helpers::explode_filters($filters);
                }
            }

            if (!$data['info']['h1']) {
                $data['info']['h1'] = ($data['info']['type_page_h1'] ?: $data['info']['type_page_name']) . ' ' . $data['info']['tm_name'] . ($data['info']['tm_alt_name'] ? ' (' . $data['info']['tm_alt_name'] . ')' : '');
            }

            if (!$data['info']['title']) {
                $hasH1 = !!$data['info']['type_page_h1'];

                $data['info']['title'] = 'Купить ' .
                    helpers::grammatical_case(
                        helpers::numeric_name(
                            $hasH1 ? $data['info']['type_page_h1'] : $data['info']['type_page_name'],
                            $hasH1 ? $data['info']['type_page_h1_alt_number'] : $data['info']['type_page_name_alt_number'],
                            $hasH1 ? $data['info']['type_page_h1_plural'] : $data['info']['type_page_name_plural'],
                            false
                        ),
                        helpers::CASE_TYPE_ACCUSATIVE,
                        true
                    ) .
                    ' ' . ($data['info']['tm_name'] ?: $data['info']['tm_alt_name']) .
                    '  в интернет-магазине в Москве. Цены на ' .
                    helpers::grammatical_case(
                        helpers::numeric_name(
                            $hasH1 ? $data['info']['type_page_h1'] : $data['info']['type_page_name'],
                            $hasH1 ? $data['info']['type_page_h1_alt_number'] : $data['info']['type_page_name_alt_number'],
                            $hasH1 ? $data['info']['type_page_h1_plural'] : $data['info']['type_page_name_plural'],
                            true
                        ),
                        helpers::CASE_TYPE_ACCUSATIVE,
                        true
                    ) .
                    ' ' . ($data['info']['tm_alt_name'] ?: $data['info']['tm_name']) .
                    ' в каталоге Роскосметики';
            }

            if (!$data['info']['description']) {
                $data['info']['description'] = 'Выбрать и заказать ' . helpers::grammatical_case($data['info']['type_page_h1'] ?: $data['info']['type_page_name'], helpers::CASE_TYPE_ACCUSATIVE, true) . ' ' . ($data['info']['tm_alt_name'] ?: $data['info']['tm_name']) . ' в интернет-магазине Роскосметика с доставкой по Москве, Санкт-Петербургу, России! В нашем каталоге огромный ассортимент, скидки и лучшие цены!';
            }

            if (count($products) <= 12) {
                $data['popular_products'] = Product::popular_products_block(8, $data['info']['tm_id'], null, $data['info']['type_page_id']);
            }
        } else {
            $data['filters'] = $data['products'] ? filters::available_filters($where . $tm_condition . $price_condition, $innerJoins) : null;
        }

        $data['filter_prices'] = $data['products'] ? filters::price_range($where . $filter_condition . $tm_condition, $innerJoins) : null;

        return $data;
    }

    /**
     * Outputs /kosmetika/type_page/category page
     *
     * @param array $arg URL arguments
     * @param bool $ajax AJAX request flag
     *
     * @return array
     */
    public static function output_category($arg, $ajax)
    {
        $info = DB::get_row(
            'SELECT t.id, t.url, t.name,
                c.id AS cat_id, c.parent_1 AS cat_parent_1, c.parent_2 AS cat_parent_2, c.level AS cat_level
            FROM rk_type_page AS t
            INNER JOIN rk_categories AS c ON c._full_url = "' . DB::escape(implode('/', array_slice($arg, 1))) . '" AND c.visible = 1
            LEFT JOIN rk_categories AS cp ON cp.type_page_id = t.id AND cp.url = "' . DB::escape($arg[1]) . '" AND cp.id = IF(c.parent_2 = 0, c.parent_1, c.parent_2) AND cp.visible = 1
            WHERE t.url = "' . DB::escape($arg[0]) . '" AND t.visible = 1 AND IF(c.level = "1", cp.id IS NULL, cp.id IS NOT NULL)'
        );

        if (!$info) {
            return null;
        }

        $data = categories::client_output(
            $info['cat_level'] == 1 ? $info['cat_id'] : ($info['cat_level'] == 2 ? $info['cat_parent_1'] : $info['cat_parent_2']),
            $info['cat_level'] == 1 ? 0 : ($info['cat_level'] == 2 ? $info['cat_id'] : $info['cat_parent_1']),
            $info['cat_level'] == 3 ? $info['cat_id'] : 0,
            true,
            $ajax,
            '/kosmetika/' . $arg[0] . '/',
            $info['id']
        ) + [
            'type_page' => $info,
        ];

        return $data;
    }

    /**
     * Updates or creates new row
     *
     * @param int|null $id Type page ID. Empty to create new
     * @param string $url Type page URL
     * @param string $name Type page name
     * @param string $h1 Type page H1 value
     * @param string $title Type page title
     * @param string $description Type page meta description
     * @param string $text Type page text
     * @param string $image Image file name
     * @param int $nameOnProductsN Count of first products to display category name on category page
     *
     * @return bool|int
     */
    public static function update($id, $url, $name, $h1, $title, $description, $text, $image = null, $nameOnProducts = 10)
    {
        if ($id) {
            DB::query('UPDATE rk_type_page SET url = "' . DB::escape($url) . '", `name` = "' . DB::escape($name) . '", h1 = "' . DB::escape($h1) . '", title = "' . DB::escape($title) . '", description = "' . DB::escape($description) . '", `text` = "' . DB::escape($text) . '"' . ($image ? ', `image` = "' . DB::escape($image) . '"' : '') . ', name_on_products = ' . ((int)$nameOnProducts) . ' WHERE id = ' . (int)$id);

            return $id;
        } else {
            DB::query('INSERT INTO rk_type_page (url, `name`, h1, title, description, `text`, name_on_products) VALUES ("' . DB::escape($url) . '", "' . DB::escape($name) . '", "' . DB::escape($h1) . '", "' . DB::escape($title) . '", "' . DB::escape($description) . '", "' . DB::escape($text) . '", ' . ((int)$nameOnProducts) . ')');

            return DB::get_last_id();
        }
    }

    /**
     * Returns type pages + trademarks rows
     *
     * @param int $typePageID Type page ID
     *
     * @return array
     */
    public static function get_type_pages_tms($typePageID)
    {
        return DB::get_rows(
            'SELECT tp.*,
                p.name AS type_page_name, IF(p.url != "", p.url, p.id) AS type_page_url,
                t.name AS tm_name, IF(t.url != "", t.url, t.id) AS tm_url
            FROM rk_tm_in_type_page AS tp
            INNER JOIN rk_type_page AS p ON p.id = tp.type_page_id
            INNER JOIN tm AS t ON t.id = tp.tm_id
            WHERE tp.type_page_id = ' . ((int)$typePageID) . '
            ORDER BY t.name');
    }

    /**
     * Returns type page + trademark row
     *
     * @param int $typePageID Type page ID
     * @param int $tmID Trademark ID
     *
     * @return array
     */
    public static function get_type_page_tm($typePageID, $tmID)
    {
        return DB::get_row(
            'SELECT tp.*,
                p.name AS type_page_name, IF(p.url != "", p.url, p.id) AS type_page_url,
                t.name AS tm_name, IF(t.url != "", t.url, t.id) AS tm_url
            FROM rk_tm_in_type_page AS tp
            INNER JOIN rk_type_page AS p ON p.id = tp.type_page_id
            INNER JOIN tm AS t ON t.id = tp.tm_id
            WHERE tp.type_page_id = ' . ((int)$typePageID) . '
                AND tp.tm_id = ' . (int)$tmID);
    }

    /**
     * Updates type page + trademark row
     *
     * @param int $typePageID Type page ID
     * @param int $tmID Trademark ID
     * @param string $h1 Type page + trademark H1 value
     * @param string $title Type page + trademark title
     * @param string $description Type page + trademark meta description
     * @param string $text Type page + trademark page text
     *
     * @return bool
     */
    public static function update_type_page_tm($typePageID, $tmID, $h1, $title, $description, $text)
    {
        return !!DB::query('UPDATE rk_tm_in_type_page SET h1 = "' . DB::escape($h1) . '", title = "' . DB::escape($title) . '", description = "' . DB::escape($description) . '", `text` = "' . DB::escape($text) . '" WHERE type_page_id = ' . ((int)$typePageID) . ' AND tm_id = ' . ((int)$tmID));
    }

    /**
     * Adds products to type page
     *
     * @param int $typePageID Type page ID
     * @param array $productIDs Product IDs
     */
    public static function add_products($typePageID, $productIDs)
    {
        $typePageID = (int)$typePageID;

        for ($i = 0, $n = count($productIDs); $i < $n; ++$i) {
            DB::query('INSERT INTO rk_prod_type_page (product_id, type_page_id) VALUES (' . ((int)$productIDs[$i]) . ', ' . $typePageID . ') ON DUPLICATE KEY UPDATE product_id = product_id');
        }
    }

    /**
     * Deletes all products from type page
     *
     * @param int $typePageID Type page ID
     */
    public static function clear_products($typePageID)
    {
        DB::query('DELETE FROM rk_prod_type_page WHERE type_page_id = ' . (int)$typePageID);
    }

    /**
     * Toggles "visible" flag for type page
     *
     * @param int $typePageID Type page ID
     */
    public static function toggle_visibility($typePageID)
    {
        DB::query('UPDATE rk_type_page SET visible = IF(visible = 1, 0, 1) WHERE id = ' . (int)$typePageID);
    }

    /**
     * Returns linked to type page products
     *
     * @param int $typePageID Type page ID
     *
     * @return array
     */
    public static function linked_products($typePageID)
    {
        return DB::get_rows(
            'SELECT p.id, p.name, p.pack, p.short_description
            FROM products AS p
            WHERE p.id IN (
              SELECT ptp.product_id
              FROM rk_prod_type_page AS ptp WHERE ptp.type_page_id = ' . ((int)$typePageID) . '
            )'
        );
    }

    /**
     * Removes products from type page
     *
     * @param int $typePageID Type page ID
     * @param array $productIDs Product IDs
     */
    public static function remove_products($typePageID, $productIDs)
    {
        $typePageID = (int)$typePageID;

        for ($i = 0, $n = count($productIDs); $i < $n; ++$i) {
            DB::query('DELETE FROM rk_prod_type_page WHERE type_page_id = ' . $typePageID . ' AND product_id = ' . ((int)$productIDs[$i]));
        }
    }

    /**
     * Copies products from tags
     *
     * @param int $typePageID Type page ID
     * @param array $tagIDs Tag IDs
     */
    public static function copy_from_tags($typePageID, $tagIDs)
    {
        $typePageID = (int)$typePageID;

        for ($i = 0, $n = count($tagIDs); $i < $n; ++$i) {
            DB::query('INSERT INTO rk_prod_type_page (type_page_id, product_id) SELECT ' . $typePageID . ', pc.prod_id FROM rk_prod_cat AS pc WHERE pc.cat_id = ' . ((int)$tagIDs[$i]) . ' ON DUPLICATE KEY UPDATE rk_prod_type_page.type_page_id = rk_prod_type_page.type_page_id');
        }
    }
}
