<?php

/**
 * Banners.
 */
class Banners
{

    /**
     * Get banners
     * @param  boolean $for_client Get banners for client or for administrator (unvisible and end date)
     * @return array               Banners array
     */
    static function get_banners($for_client = TRUE)
    {

        $query = 'SELECT   id,
                             name,
                             file_name,
                             menu_tag,
                             type,
                             link,
                             alt,
                             position,
                             map_area,
                             copy_area,
                             copy_area_text,
                             blank '
            . ($for_client ? '' : ', visible ') .


            'FROM rk_banners
                  WHERE ' . ($for_client ? ' visible = 1 AND id IN (SELECT bd.banner_id FROM rk_banners_dates AS bd WHERE bd.begin_date <= NOW() AND IF(bd.end_date IS NOT NULL, NOW() <= end_date, 1)) ' : ' 1 ') .
            'ORDER BY position ';

        $res = db::query($query);

        $banners = FALSE;

        $alphavit = array(
                            "а" => "a", "б" => "b", "в" => "v", "г" => "g", "д" => "d", "е" => "e",
                            "ё" => "jo", "ж" => "zh", "з" => "z", "и" => "i", "й" => "j", "к" => "k", "л" => "l", "м" => "m",
                            "н" => "n", "о" => "o", "п" => "p", "р" => "r", "с" => "s", "т" => "t",
                            "у" => "u", "ф" => "f", "х" => "h", "ц" => "c", "ч" => "ch", "ш" => "sh", "щ" => "w", "ъ" => "#",
                            "ы" => "y", "ь" => "'", "э" => "je", "ю" => "ju", "я" => "ja",
                            "А" => "A", "Б" => "B", "В" => "V", "Г" => "G", "Д" => "D", "Е" => "E", "Ё" => "Yo",
                            "Ж" => "Zh", "З" => "Z", "И" => "I", "Й" => "J", "К" => "K", "Л" => "L", "М" => "M",
                            "Н" => "N", "О" => "O", "П" => "P", "Р" => "R", "С" => "S", "Т" => "T", "У" => "U",
                            "Ф" => "F", "Х" => "H", "Ц" => "C", "Ч" => "Ch", "Ш" => "Sh", "Щ" => "W",
                            "Ы" => "Y", "Э" => "Je", "Ю" => "Ju", "Я" => "Ja",
                            "Ь" => "'", "Ъ" => "#", " " => "-"
                        );

        while($banner = $res->fetch_assoc()) {
            $file = 'images/banners/' .  $banner['file_name'] . '.' . $banner['type'];

            if (file_exists($file)) {
                $size = getimagesize($file);
                $width = $size[0];
                $height = $size[1]; 

                $opt_label = strtr($banner['name'], $alphavit);

                $banners[] = array( 'menu_tag' => $banner['menu_tag'],
                                    'file' => $file,
                                    'alt' => $banner['alt'],
                                    'link' => $banner['link'],
                                    'width' => $width,
                                    'height' => $height,
                                    'opt_label' => $opt_label,
                                    'map_area' => $banner['map_area'],
                                    'position' => $banner['position'],
                                    'copy_area' => $banner['copy_area'],
                                    'copy_area_text' => $banner['copy_area_text'],
                                    'blank' => $banner['blank']);
            }
        }

        $res -> close();

        return $banners;
    }


    /**
     * Возвращает баннер.
     * @param integer $id Id баннера.
     * @return array
     */
    static function get_banner($id)
    {
        $query = "SELECT id,
                             name,
                             file_name,
                             menu_tag,
                             type,
                             alt,
                             link,
                             copy_area,
                             copy_area_text,
                             visible,
                             position,
                             map_area,
                             blank
                       FROM rk_banners WHERE id = $id";

        $main = DB::get_row($query);

        return $main
            ? [
                'banner' => $main,
                'dates' => DB::get_rows('SELECT bd.begin_date AS begin, bd.end_date AS end FROM rk_banners_dates AS bd WHERE bd.banner_id = ' . $main['id']),
            ]
            : null;
    }

    /**
     * Возвращает список баннеров.
     * @return array
     */
    static function get_list($map = 0, $onPage = null)
    {
        $where = ($map ? "WHERE visible = 1 AND id IN (SELECT bd.banner_id FROM rk_banners_dates AS bd WHERE bd.begin_date <= NOW() AND IF(bd.end_date IS NOT NULL, NOW() <= end_date, 1)" : '');

        if ($onPage !== null) {
            $count = (int)DB::get_field('cnt', 'SELECT COUNT(b.id) AS cnt FROM rk_banners AS b ' . $where);

            Pagination::setValues($count, $onPage);

            Pagination::setTemplateSettings();
        }

        $query = 'SELECT     id,
                             name,
                             file_name,
                             type,
                             link,
                             alt,
                             copy_area,
                             copy_area_text,
                             visible,
                             (SELECT bd.begin_date FROM rk_banners_dates AS bd WHERE bd.banner_id = b.id ORDER BY bd.begin_date LIMIT 1) AS begin,
                             (SELECT bd.end_date FROM rk_banners_dates AS bd WHERE bd.banner_id = b.id AND bd.end_date IS NOT NULL ORDER BY bd.end_date DESC LIMIT 1) AS end,
                             position
                  FROM rk_banners AS b
                  ' . $where .
            ' ORDER BY (SELECT bd.begin_date FROM rk_banners_dates AS bd WHERE bd.banner_id = b.id ORDER BY bd.begin_date DESC LIMIT 1) DESC, position, visible' .
            ($onPage !== null ? Pagination::sqlLimit() : '');

        return DB::get_rows($query);
    }

    /**
     * Добавляет Баннер.
     * @param integer $id
     * @param string $name
     * @param string $file_name
     * @param string $menu_tag
     * @param string $type
     * @param string $alt
     * @param string $link
     * @param int $position - banner number
     * @param string $map_area
     * @param string $copy_area
     * @param string $copy_area_text
     * @param array $dates
     *
     * @return boolean
     */
    static function set_banner($id, $name, $file_name, $menu_tag, $type, $alt, $link, $position, $map_area, $copy_area, $copy_area_text, $dates)
    {
        $id = (int)$id;

        $query = "UPDATE rk_banners
                  SET name = '$name',
                      file_name = '$file_name',
                      menu_tag = '$menu_tag', 
                      type = '$type',
                      alt = '$alt',
                      link = '$link',
                      position = $position,
                      map_area = '$map_area',
                      copy_area = '$copy_area',
                      copy_area_text = '$copy_area_text'
                  WHERE id = $id";

        if (DB::query($query)) {
            DB::query('DELETE FROM rk_banners_dates WHERE banner_id = ' . $id);

            for ($i = 0, $n = count($dates); $i < $n; ++$i) {
                DB::query('INSERT INTO rk_banners_dates (banner_id, begin_date, end_date) VALUES (' . $id . ', "' . DB::escape($dates[$i][0]) . '", ' . ($dates[$i][1] ? '"' . DB::escape($dates[$i][1]) . '"' : 'NULL') . ')');
            }

            return true;
        }

        return false;
    }


    /**
     * Добавляет Баннер.
     *
     * @param string $name
     * @param string $file_name
     * @param string $type
     * @param string $alt
     * @param string $link
     * @param int $position - banner position
     * @param string $map_area
     * @param array $dates
     *
     * @return boolean
     */
    static function add_banner($name, $file_name, $menu_tag, $type, $alt, $link, $position, $map_area, $dates)
    {

        $query = "INSERT INTO rk_banners (id, name, file_name, menu_tag, type, alt, link, visible, position, map_area)
                  VALUES (0, '$name', '$file_name', ' $menu_tag', '$type', '$alt', '$link', 1, $position, '$map_area')";

        if (DB::query($query)) {
            $id = DB::get_last_id();

            for ($i = 0, $n = count($dates); $i < $n; ++$i) {
                DB::query('INSERT INTO rk_banners_dates (banner_id, begin_date, end_date) VALUES (' . $id . ', "' . DB::escape($dates[$i][0]) . '", ' . ($dates[$i][1] ? '"' . DB::escape($dates[$i][1]) . '"' : 'NULL') . ')');
            }
        }
    }


    /**
     * Проверяет file_name на существование.
     * @param char $file_name - имя файла баннера
     * @return array
     */
    static function check_file_name($file_name)
    {

        $query = "SELECT *
                  FROM rk_banners
                  WHERE file_name = '$file_name'";
        $result = array('kol' => DB::get_num_rows($query),
            'id' => DB::get_field('id', $query));

        return $result;
    }

    /**
     * Изменение видимости
     * @param int $id - id баннера
     * @return bool
     */

    static function change_visibile($id)
    {
        $id = (int)$id;

        $query = "UPDATE rk_banners
                  SET visible = (IF(visible = 1, 0, 1))
                  WHERE id = $id";

        return DB::query($query);
    }
}

?>