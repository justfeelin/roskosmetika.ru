<?php

/**
 * Check data
 */
class check
{

    /**
     *  Check existence in data base
     * @param string $table data base table
     * @param string $field field of table
     * @param string|int $value value to check
     * @return array
     */
    static function check_existence($table, $field, $value)
    {

        $result = array();
        $result['number'] = db::get_num_rows("SELECT * FROM $table WHERE $field = '$value'");

        if ($result['number']) {
            $result[$field] = DB::get_field($value, "SELECT * FROM $table WHERE $field = '$value'");
            $result['result'] = TRUE;
        } else {
            $result['result'] = FALSE;
        }

        return $result;
    }


    /**
     * Проверка имени.
     * @param string $name Проверяемое имя.
     * @return boolean
     */
    static function name($name)
    {
        return preg_match('/^[\w\d\sА-Яа-яёЁ-]+$/u', $name);
    }

    /**
     * Returns whether given value not empty
     * 
     * @param string $value Value to check
     * 
     * @return bool
     */
    public static function notEmpty($value)
    {
        return trim($value) !== '';
    }

    /**
     * Check SID for import
     * @param  string $sid Session ID
     * @return boolen     Success of fail check
     */
    static function sid($sid)
    {
        return preg_match('/^[a-z0-9]+$/u', $sid);
    }

    /**
     * Проверяет email.
     * @param string $email Проверяемый email.
     * @return boolean
     */
    static function email($email)
    {

        return preg_match('/^[\._a-zA-Z\d-]+@[\.a-zA-Z\d-]+\.[a-zA-Z]{2,6}$/', $email);
    }

    /**
     * Проверяет телефон.
     * @param string $phone Проверяемый телефон.
     * @return boolean
     */
    static function phone($phone)
    {
        return strlen(preg_replace('/[^\d]/', '', $phone)) > 9;
    }

    /**
     * Проверяет пароль.
     * @param string $password Проверяемый пароль.
     * @return boolean
     */
    static function password($password)
    {

        return preg_match('/^[А-Яа-яёЁ\d\w-_]{6,}$/', $password);
    }

    /*
     *  Исключение слов невозможной длинны. Проверка на наличие тегов, ковычек и мата, их замена
     * @param  string $comment строка комментария
     */

    static function check_lengh_tags($comment)
    {

        $tempcomment[] = strtok($comment, " ");
        foreach ($tempcomment as $key => $value) {

            if (strlen($value) > 60) {
                $message = 'Отзыв содержит слова невозможной длины:';
            }
        }
        $comment = htmlspecialchars($comment, ENT_QUOTES);
        $pattern = array("'&amp;'ui", "'&lt;'ui", "'&gt;'ui",
            "'ху(ё|е|й)'ui", "'пиз(д|ж)'ui", "'бля'ui", "'жоп(а|о)'ui", "'пид(о|е|а)р'ui", "'пидр'ui", "'(е|ё)б(л|а)'ui",
            "'hu(y|i)'ui", "'piz(d|j)'ui", "'bl(y|a)'ui", "'jop(a|a)'ui", "'pid(o|e|a)р'ui", "'pidr'ui", "'eb(l|a)'ui");
        $replace = array('and', '[', ']', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*', '*');
        $comment = preg_replace($pattern, $replace, $comment);
        $comment = trim($comment);
        $value = $comment;
        if (!isset($message)) {
            $message = '';
        }
        if ($message == '') {
            $right = TRUE;
        } else {
            $right = FALSE;
        }

        $result = array('0' => $value, '1' => $message, '2' => $right);
        return $result;
    }

    /*
     *  Проверка заполнения поля сообщения
     *  @param  string $comment строка комментария
     *  @return boolen
     */

    static function check_fullness($comment)
    {

        return empty($comment);
    }

    /**
     * Извлекает из даты название дня недели на русском
     * @param array $date_array результат запроса  содержащий поля дат.
     * @return array.
     */
    static function date()
    {


        for ($i = 1; $i <= 7; $i++) {

            switch ($i) {
                case 1:
                    $name = 'понедельник';
                    break;
                case 2:
                    $name = 'вторник';
                    break;
                case 3:
                    $name = 'среда';
                    break;
                case 4:
                    $name = 'четверг';
                    break;
                case 5:
                    $name = 'пятница';
                    break;
                case 6:
                    $name = 'суббота';
                    break;
                case 7:
                    $name = 'воскресение';
                    break;
            }

            $day_of_week[] = array('eng' => $i, 'rus' => $name);
        }
        return $day_of_week;
    }

    /**
     * Извлекает из даты название месяца  на русском
     * @param array $date_array результат запроса  содержащий поля дат.
     * @return array.
     */
    static function month()
    {


        for ($i = 1; $i <= 12; $i++) {

            switch ($i) {
                case 1:
                    $name = 'января';
                    break;
                case 2:
                    $name = 'февраля';
                    break;
                case 3:
                    $name = 'марта';
                    break;
                case 4:
                    $name = 'апреля';
                    break;
                case 5:
                    $name = 'мая';
                    break;
                case 6:
                    $name = 'июня';
                    break;
                case 7:
                    $name = 'июля';
                    break;
                case 8:
                    $name = 'августа';
                    break;
                case 9:
                    $name = 'сентября';
                    break;
                case 10:
                    $name = 'октября';
                    break;
                case 11:
                    $name = 'ноября';
                    break;
                case 12:
                    $name = 'декабря';
                    break;
            }

            $day_of_week[$i] = $name;
        }
        return $day_of_week;
    }


    /**
     * Separate fio of user for name, surname, patronymic
     * @param  string $fio name, surname, patronymic in one string
     * @return array      array of name, surname, patronymic
     */
    static function separate_fio($fio)
    {

        $fio = trim($fio);
        $return = array('name' => '', 'surname' => '', 'patronymic' => '');

        if (!empty($fio)) {

            $string_separate = explode(' ', $fio, 3);
            $fio_quan = count($string_separate);

            if ($fio_quan == 1) {
                if (isset($string_separate[0])) $return['name'] = trim($string_separate[0]);
            }
            if ($fio_quan == 2) {
                if (isset($string_separate[1])) $return['name'] = trim($string_separate[1]);
                if (isset($string_separate[0])) $return['surname'] = trim($string_separate[0]);
            }
            if ($fio_quan == 3) {
                if (isset($string_separate[1])) $return['name'] = trim($string_separate[1]);
                if (isset($string_separate[0])) $return['surname'] = trim($string_separate[0]);
                if (isset($string_separate[2])) $return['patronymic'] = trim($string_separate[2]);
            }

        }

        return $return;
    }


    /**
     * create canonical link
     * @return string|bool link of false
     */
    static function canonical_url()
    {
        $this_url = strpos($_SERVER['REQUEST_URI'], '?');
        $this_url = ($this_url ? mb_substr($_SERVER['REQUEST_URI'], 0, $this_url) : $_SERVER['REQUEST_URI']);

        return mb_strlen($_SERVER['REQUEST_URI']) > 1 ? mb_strtolower($this_url) : '/';
    }

    /**
     * Chech url for spec chars
     * @param  string $url URL
     * @return string      checked URL
     */
    static function url($url) {

        $url = preg_replace ("/[^a-zA-Z0-9]/",'-',trim(strtolower($url)));
        $url = preg_replace ("/-{1,}/","-",$url);

        return $url;
    }
}
