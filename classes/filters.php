<?php

/**
 * working with filters
 */
class filters
{

    /**
     * return filters
     * @param  string $type тип возвращаемых фильтров
     * @param  bool $visible видимость
     * @return array          Возвращаемая выборка
     */
    static function get_filters($type, $visible = 1)
    {
        $groups = DB::get_rows("SELECT id, name, parent_id, visible FROM rk_filters WHERE parent_id = 0 AND visible = $visible ORDER BY id");

        if ($type == 'prod') {
            foreach ($groups as $key => $group) {
                $groups[$key]['filters'] = DB::get_rows("SELECT id, name, parent_id, visible FROM rk_filters WHERE parent_id = {$group['id']} AND visible = $visible ORDER BY id");
            }
        }
        return $groups;
    }

    /**
     * Sets products info
     *
     * @param array $products products array
     * @param int $tmID Trademark ID of current products list (Used for `for_proff` field retrieval)
     *
     * @return array sorting products
     */
    public static function set_products_info($products, $tmID = null)
    {
        if ($products) {
            for ($i = 0, $n = count($products); $i < $n; ++$i) {
                $products[$i]['packs'] = Product::get_packs($products[$i]['id'], true, $tmID);
            }
        }

        return $products;
    }

    /**
     * Returns array of filter IDs based on fetched products list
     *
     * @param string $where SQL 'WHERE' statement (Without 'WHERE') to filter products
     * @param string $innerJoins SQL 'INNER JOIN' statements required for products fetching
     * @param string $alias 'products' table alias
     * @param array $allowedFilters Allowed parent filter IDs. If null - all filters allowed
     *
     * @return array
     */
    public static function available_filters($where, $innerJoins = null, $alias = 'p', $allowedFilters = null)
    {
        $filterIDs = [];

        $aliasName = $alias ? $alias : null;

        $alias = $alias ? $alias . '.' : '';

        DB::get_rows(
            'SELECT f.id FROM rk_filters AS f WHERE f.parent_id != 0 ' . ($allowedFilters !== null ? 'AND f.parent_id IN (' . implode(', ', $allowedFilters) . ') ' : '') . 'AND f.visible = 1 AND f.id IN (SELECT pd.filter_id FROM rk_prod_filter AS pd WHERE pd.prod_id IN (SELECT ' . $alias . 'id FROM products ' . ($aliasName !== null ? 'AS ' . $aliasName . ' ' : '') . ($innerJoins !== null ? $innerJoins . ' ' : '') . 'WHERE ' . $where . '))',
            function ($filter) use (&$filterIDs) {
                $filterIDs[] = (int)$filter['id'];
            }
        );

        return $filterIDs;
    }

    /**
     * Returns array of trademark IDs or arranged set of trademarks for filters, based on fetched products list
     *
     * @param string $where SQL 'WHERE' statement (Without 'WHERE') to filter products
     * @param string $innerJoins SQL 'INNER JOIN' statements required for products fetching
     * @param string $alias 'products' table alias
     * @param bool $ajax AJAX request (Return tm IDs if true)
     *
     * @return array
     */
    public static function available_tms($where, $innerJoins = null, $alias = 'p', $ajax = true)
    {
        $tmIDs = [];

        $aliasName = $alias ? $alias : null;

        $alias = $alias ? $alias . '.' : '';

        DB::get_rows(
            'SELECT DISTINCT ' . $alias . 'tm_id FROM products ' . ($aliasName !== null ? 'AS ' . $aliasName . ' ' : '') . ($innerJoins !== null ? $innerJoins . ' ' : '') . 'WHERE ' . $where,
            function ($tm) use (&$tmIDs) {
                $tmIDs[] = (int)$tm['tm_id'];
            }
        );

        return $ajax
            ? $tmIDs
            : (
                $tmIDs
                    ? helpers::explode_filters(
                        DB::get_rows(
                            'SELECT tm.id, tm.name FROM tm WHERE tm.id IN (' .
                                implode(', ', $tmIDs) .
                            ') ORDER BY tm.name'
                        )
                    )
                    : []
            );
    }

    /**
     * Returns array of countries IDs or arranged set of countries for filters, based on fetched products list
     *
     * @param string $where SQL 'WHERE' statement (Without 'WHERE') to filter products
     * @param string $innerJoins SQL 'INNER JOIN' statements required for products fetching
     * @param string $alias 'products' table alias
     * @param bool $ajax AJAX request (Return country IDs if true)
     *
     * @return array
     */
    public static function available_countries($where, $innerJoins = null, $alias = 'p', $ajax = true)
    {
        $countriesIDs = [];

        $aliasName = $alias ? $alias : null;

        $alias = $alias ? $alias . '.' : '';

        DB::get_rows(
            'SELECT DISTINCT __t.country_id
            FROM products ' . ($aliasName !== null ? 'AS ' . $aliasName . ' ' : '') .
            'INNER JOIN tm AS __t ON __t.id = ' . $alias . 'tm_id ' .
                ($innerJoins !== null ? $innerJoins . ' ' : '') .
            'WHERE ' . $where,
            function ($country) use (&$countriesIDs) {
                $countriesIDs[] = (int)$country['country_id'];
            }
        );

        return $ajax
            ? $countriesIDs
            : (
                $countriesIDs
                    ? helpers::explode_filters(
                        DB::get_rows(
                            'SELECT c.id, c.name
                            FROM cdb_country AS c
                            WHERE c.id IN (' .
                                implode(', ', $countriesIDs) .
                            ')
                            ORDER BY c.name'
                        )
                    )
                    : []
            );
    }

    /**
     * Returns price range based on fetched products list
     *
     * @param string $where SQL 'WHERE' statement (Without 'WHERE') to filter products
     * @param string $innerJoins SQL 'INNER JOIN' statements required for products fetching
     * @param string $alias 'products' table alias
     *
     * @return array [`Minimal price`, `Maximum price`]
     */
    public static function price_range($where, $innerJoins = null, $alias = 'p')
    {
        $aliasName = $alias ? $alias : null;

        $alias = $alias ? $alias . '.' : '';

        $field = 'IF (' . $alias . 'special_price > 0, ' . $alias . 'special_price, ' . $alias . 'price)';

        $range = DB::get_row('SELECT MIN(' . $field . ') AS min_price, MAX(' . $field . ') AS max_price FROM products ' . ($aliasName !== null ? 'AS ' . $aliasName . ' ' : '') . ($innerJoins !== null ? $innerJoins . ' ' : '') . 'WHERE ' . $where);

        return [
            (int)$range['min_price'],
            (int)$range['max_price'],
        ];
    }

    public static function available_categories($where, $innerJoins = null, $alias = 'p', $ajax = true)
    {
        $result = [];

        $aliasName = $alias ? $alias : null;

        $alias = $alias ? $alias . '.' : '';

        DB::get_rows(
            'SELECT DISTINCT pc.cat_id AS id' . (!$ajax ? ', c.name' : '') .
                ' FROM rk_prod_cat_lvl_1 AS pc ' .
                (!$ajax ? 'INNER JOIN rk_categories AS c ON c.id = pc.cat_id ' : '') .
                'WHERE pc.prod_id IN (
                    SELECT ' . $alias . 'id FROM products ' . ($aliasName !== null ? 'AS ' . $aliasName . ' ' : '') . ($innerJoins !== null ? $innerJoins . ' ' : '') . 'WHERE ' . $where .
            ')',
            function ($category) use (&$result, $ajax) {
                $result[] = $ajax ? (int)$category['id'] : $category;
            }
        );

        return $ajax ? $result : helpers::explode_filters($result, 15);
    }

    /**
     * Returns filters for given products
     *
     * @param array $productIDs Product IDs
     * 
     * @return array
     */
    public static function get_filters_for_products($productIDs)
    {
        $return = [];

        $productIDsSQL = implode(', ', $productIDs);

        $query = "SELECT id, name FROM rk_filters WHERE parent_id = 0 AND visible = 1 ORDER BY name";

        $main_filters = DB::get_rows($query);

        for ($i = 0, $n = count($main_filters); $i < $n; ++$i) {
            $query = "SELECT  f.id, f.name
                          FROM rk_filters AS f
                          INNER JOIN rk_prod_filter AS pf ON pf.filter_id = f.id AND pf.prod_id IN ($productIDsSQL)
                          WHERE f.visible = 1 AND f.parent_id = {$main_filters[$i]['id']}
                          GROUP BY f.id
                          ORDER BY f.name";

            $filters = db::get_rows($query);

            if (!empty($filters)) {
                $return[$main_filters[$i]['name']] = helpers::explode_filters($filters);
            }
        }

        return $return;
    }
}
