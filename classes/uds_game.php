<?php
class uds_game {

    /**
     * UDS Token (API key)
     */
    const UDS_TOKEN = ''; 

    /**
     * Main UDS url for working
     */
    const MAIN_UDS_URL = 'https://udsgame.com/v1/partner/';

    /**
     * Generate UUID v4 for UDS
     * @return string UUID v4 code
     */
    private static function make_UUID() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
          // 32 bits for "time_low"
          mt_rand(0, 0xffff), mt_rand(0, 0xffff),
          // 16 bits for "time_mid"
          mt_rand(0, 0xffff),
          // 16 bits for "time_hi_and_version",
          // four most significant bits holds version number 4
          mt_rand(0, 0x0fff) | 0x4000,
          // 16 bits, 8 bits for "clk_seq_hi_res",
          // 8 bits for "clk_seq_low",
          // two most significant bits holds zero and one for variant DCE1.1
          mt_rand(0, 0x3fff) | 0x8000,
          // 48 bits for "node"
          mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    /**
     * Generate ISO 8601 Date
     * @return date ISO 8601 Date
     */
    private static function get_timestamp() {

        $date = new DateTime();
        return $date->format(DateTime::ATOM);

    }

    /**
     * Request to uds
     * @param  string         $url                request url
     * @param  string         $method             HTTP method
     * @param  boolean|array  $content            content for POST
     * @param  boolean|string $opts_content_type  option for POST method
     * @param  boolean        $opts_ignore_errors option for http
     * @return json                               request result 
     */
    private static function uds_request($url, $method = 'GET', $content = FALSE, $opts_content_type = FALSE, $opts_ignore_errors = FALSE)
    {
        // Create a stream
        $opts = array(
            'http' => array(
                'method' => $method,
                'header' => "Accept: application/json\r\n" .
                            "Accept-Charset: utf-8\r\n" .
                            ($opts_content_type ? "Content-Type: " . $opts_content_type . "\r\n" : '') .
                            "X-Api-Key: " . UDS_TOKEN . "\r\n" .
                            "X-Origin-Request-Id: " . self::make_UUID() . "\r\n" .
                            "X-Timestamp: " . self::get_timestamp()
            )
        );

        if ($content) $opts['http']['content'] = $content;

        if ($opts_ignore_errors) $opts['http']['ignore_errors'] = FALSE;

        return file_get_contents($url, false, stream_context_create($opts)); 
    }

    /**
     * Get customer and his structure by code or customer id (phone)
     * @param  string $id    code or id
     * @param  string $param param for url code|phone|customerId
     * @return json          json customer object
     */
    public static function get_client($id, $param = 'code')
    {

        $url = MAIN_UDS_URL . 'customer?' . $param . '=' . $id;

        return self::uds_request($url);
    }

    /**
     * Get company infomation
     * @return json json company object
     */
    public static function get_company() 
    {

        $url = MAIN_UDS_URL . 'company';

        return self::uds_request($url);

    }

    /**
     * Get payment information hidden behind the promo code
     * @param  string $promo_code promo-code
     * @return json               json action data
     */
    public static function get_payments($promo_code) 
    {
        $url = MAIN_UDS_URL . 'promo-code?code=' . $promo_code;

        return self::uds_request($url);

    }

    /**
     * Perform purchase operation. 
     * On success operation appears in operations list in UDS Game Admin and customer receives push notification about purchase.
     * @param  float|int  $total       total cost
     * @param  float|int  $cash        real cost (no scores)
     * @param  float|int  $scores      users scores
     * @param  string     $promo_code  promo code
     * @param  string     $order       order number
     * @param  inf|boolen $customer_id customer id 
     * @param  int        $cashier_id  manager id
     * @return json                    information about operation in JSON
     */
    public static function payment($total, $cash, $scores, $promo_code, $order, $customer_id = FALSE, $cashier_id = FALSE) 
    {
        $url = MAIN_UDS_URL . 'purchase';

        // Set request body
        $post_data = array( 'total' => (float)$total,
                            'cash' => (float)$cash,
                            'scores'=> (float)$scores,
                            'invoiceNumber' => $order);


        if ($promo_code && !$customer_id) $post_data['code'] = $promo_code;

        if (!$promo_code && $customer_id) $post_data['customerId'] = $customer_id;

        if ($cashier_id) $post_data['cashierExternalId'] = $cashier_id;

        $post_data = json_encode($post_data);


        return self::uds_request($url, 'POST', $post_data, 'application/json', TRUE);
    }

    /**
     * Perform purchase operation on special offer. 
     * On success operation appears in operations list in UDS Game Admin and customer receives push notification about purchase.
     * @param  float|int  $cash        real cost (no scores)
     * @param  string     $promo_code  promo code
     * @param  string     $order       order number
     * @param  string     $offer       offer ID
     * @param  inf|boolen $customer_id customer id 
     * @param  int        $cashier_id  manager id
     * @return json                    information about operation in JSON
     */
    public static function payment_offer($cash, $promo_code, $order, $offer, $customer_id = FALSE, $cashier_id = FALSE)
    {

        $url = MAIN_UDS_URL . 'purchase-offer';

        // Set request body
        $post_data = array( 'cash' => (float)$cash,
                            'invoiceNumber' => $order,
                            'offerExternalId'=> $offer);


        if ($promo_code && !$customer_id) $post_data['code'] = $promo_code;

        if (!$promo_code && $customer_id) $post_data['customerId'] = $customer_id;

        if ($cashier_id) $post_data['cashierExternalId'] = $cashier_id;

        $post_data = json_encode($post_data);


        return self::uds_request($url, 'POST', $post_data, 'application/json', TRUE);
    }

    /**
     * Revert operation
     * @param  string $id revert operation ID
     * @return json       information about revert
     */
    public static function revert($id)
    {
        $url = MAIN_UDS_URL . 'revert/' . $id;

        return self::uds_request($url, 'POST', FALSE, FALSE, TRUE);
    }

    /**
     * Generate nonce for operation. A nonce is an arbitrary number that can only be used once. It should be used in purchase by phone number.
     * @return json  information about nonce
     */
    public static function nonce()
    {
        $url = MAIN_UDS_URL . 'operation-nonce';

        return self::uds_request($url, 'POST');

    }

}