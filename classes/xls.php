<?php

/**
 * Работает с excel-файлами.
 */
class xls
{
    /**
     * Generates master class's subscribers file
     *
     * @param array $subscribes_list Subscribers
     * @param string $header Document header
     *
     * @return PHPExcel
     *
     * @throws Exception
     */
    static function get_master_class_subscribes($subscribes_list, $header = null)
    {
        $excel = export::create_new_file();

        $sheet = $excel->getSheet(0);

        $columns = [
            'A' => '#',
            'B' => 'Дата записи',
            'C' => 'ФИО',
            'D' => 'Телефон',
            'E' => 'Email',
            'F' => 'Цена',
            'G' => 'Дата мастер-класса',
            'H' => 'Сайт',
        ];

        $offset = $header ? 2 : 0;

        if ($header) {
            $sheet->setCellValue('A1', $header);
        }

        foreach ($columns as $column => $title) {
            if ($column !== 'A') {
                $sheet->getColumnDimension($column)->setAutoSize(true);
            }

            $sheet->getStyle($column . (1 + $offset))->getFont()->setBold(true);
            $sheet->setCellValue($column . (1 + $offset), $title);
        }

        for ($i = 0, $n = count($subscribes_list); $i < $n; ++$i) {
            $sheet->setCellValue('A' . ($i + 2 + $offset), $i + 1);
            $sheet->setCellValue('B' . ($i + 2 + $offset), date('d.m.Y', strtotime($subscribes_list[$i]['date'])));
            $sheet->setCellValue('C' . ($i + 2 + $offset), $subscribes_list[$i]['surname'] . ' ' . $subscribes_list[$i]['name'] . ' ' . $subscribes_list[$i]['patronymic']);
            $sheet->setCellValue('D' . ($i + 2 + $offset), $subscribes_list[$i]['phone']);
            $sheet->setCellValue('E' . ($i + 2 + $offset), $subscribes_list[$i]['email']);
            $sheet->setCellValue('F' . ($i + 2 + $offset), $subscribes_list[$i]['price']);
            $sheet->setCellValue('G' . ($i + 2 + $offset), date('d.m.Y', strtotime($subscribes_list[$i]['master_class_date'])));
            $sheet->setCellValue('H' . ($i + 2 + $offset), $subscribes_list[$i]['source']);
        }

        return $excel;
    }

    /**
     * Generates competition subscribers file
     *
     * @param array $subscribes_list Subscribers
     * @param string $header Document header
     *
     * @return PHPExcel
     *
     * @throws Exception
     */
    static function get_competition_subscribes($subscribes_list, $header = null)
    {
        $excel = export::create_new_file();

        $sheet = $excel->getSheet(0);

        $columns = [
            'A' => '#',
            'B' => 'Имя',
            'C' => 'Телефон',
            'D' => 'Email',
            'E' => 'Дата окончания акции',
            'F' => 'Статус',
        ];

        $offset = $header ? 2 : 0;

        if ($header) {
            $sheet->setCellValue('A1', $header);
        }

        foreach ($columns as $column => $title) {
            if ($column !== 'A') {
                $sheet->getColumnDimension($column)->setAutoSize(true);
            }

            $sheet->getStyle($column . (1 + $offset))->getFont()->setBold(true);
            $sheet->setCellValue($column . (1 + $offset), $title);
        }

        for ($i = 0, $n = count($subscribes_list); $i < $n; ++$i) {
            $sheet->setCellValue('A' . ($i + 2 + $offset), $i + 1);
            $sheet->setCellValue('B' . ($i + 2 + $offset), $subscribes_list[$i]['name']);
            $sheet->setCellValue('C' . ($i + 2 + $offset), $subscribes_list[$i]['phone']);
            $sheet->setCellValue('D' . ($i + 2 + $offset), $subscribes_list[$i]['email']);
            $sheet->setCellValue('E' . ($i + 2 + $offset), date('d.m.Y', strtotime($subscribes_list[$i]['competitions_date'])));
            $sheet->setCellValue('F' . ($i + 2 + $offset), $subscribes_list[$i]['winner']? 'Победитель':'Неудачник');
        }

        return $excel;
    }

}
