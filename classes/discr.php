<?php

/**
 * Работа с описанием страниц.
 */
class Discr
{

    /**
     * Возвращает список описаний страниц.
     * @param string $table Таблица.
     * @param string $page_type Тип страницы.
     * @param string $term Ограничения выборки.
     * @return array
     */
    static function get_discr_list($table, $page_type, $term = '')
    {

        if (get_magic_quotes_gpc()) {
            $table = DB::mysql_secure_string($table);
            $page_type = DB::mysql_secure_string($page_type);
            $term = DB::mysql_secure_string($term);
        }

        // Выборка товаров без описания
        $query = "SELECT $table.id,
                         $table.name
                  FROM $table
                  WHERE NOT EXISTS (SELECT *
                                    FROM page_discriptions
                                    WHERE page_discriptions.page_id = $table.id
                                      AND page_discriptions.page_type = '$page_type'
                                      AND (page_discriptions.title != ''
                                      OR page_discriptions.discription != ''
                                      OR page_discriptions.keywords != ''))
                        $term
                  ORDER BY name";

        $content['no_discr'] = DB::get_rows($query);

        // Выводим список товаров с описанием.
        $query = "SELECT $table.id,
                         $table.name,
                         page_discriptions.title,
                         page_discriptions.keywords,
                         page_discriptions.discription
                  FROM $table,
                       page_discriptions
                  WHERE $table.id = page_discriptions.page_id
                    AND page_discriptions.page_type = '$page_type'
                    AND (page_discriptions.title != ''
                    OR page_discriptions.discription != ''
                    OR page_discriptions.keywords != '')
                       $term
                  ORDER BY name";

        $content['with_discr'] = DB::get_rows($query);

        return $content;
    }

    /**
     * Возвращает описание страницы.
     * @param integer $page_id
     * @param string $page_type
     * @return array
     */
    static function get_discr($page_id, $page_type)
    {

        $page_id = (int)$page_id;

        if (get_magic_quotes_gpc()) {
            $page_type = DB::mysql_secure_string($page_type);
        }

        $query = "SELECT *
                  FROM page_discriptions
                  WHERE page_id = $page_id
                    AND page_type = '$page_type'";

        $row = DB::get_row($query);
        $result['title'] = (!empty($row['title']) ? $row['title'] : '');
        $result['discription'] = (!empty($row['discription']) ? $row['discription'] : '');
        $result['keywords'] = (!empty($row['keywords']) ? $row['keywords'] : '');
        $result['page_discr'] = (!empty($row['page_discr']) ? $row['page_discr'] : '');
        $result['page_id'] = $page_id;
        $result['page_type'] = $page_type;

        return $result;
    }

    /**
     * Сохранаяет описание страницы.
     * @param integer $page_id
     * @param string $page_type
     * @param string $title
     * @param <string $discription
     * @param string $keywords
     * @param string $page_discription
     * @return boolean
     */
    static function set_discr($page_id, $page_type, $title, $discription, $keywords, $page_discription = '')
    {

        $page_id = (int)$page_id;

        if (!get_magic_quotes_gpc()) {
            $page_type = DB::mysql_secure_string($page_type);
            $title = DB::mysql_secure_string($title);
            $discription = DB::mysql_secure_string($discription);
            $keywords = DB::mysql_secure_string($keywords);
            $page_discription = DB::mysql_secure_string($page_discription);
        }

        $query = "SELECT id
                  FROM page_discriptions
                  WHERE page_id = $page_id
                    AND page_type = '$page_type'";

        if ($row = DB::get_row($query)) {
            $query = "UPDATE page_discriptions
                      SET title = '$title',
                          discription = '$discription',
                          keywords = '$keywords',
                          page_discr = '$page_discription'
                      WHERE id = {$row['id']}";
        } else {
            $query = "INSERT INTO page_discriptions
                      SET title = '$title',
                          discription = '$discription',
                          keywords = '$keywords',
                          page_discr = '$page_discription',
                          page_id = $page_id,
                          page_type = '$page_type'";
        }

        return DB::query($query);
    }

    /**
     * Возвращает название страницы.
     * @param integer $id Id страницы.
     * @param string $table Название таблицы.
     * @return string
     */
    static function get_name($id, $table)
    {
        $id = (int)$id;

        if (!get_magic_quotes_gpc()) {
            $table = DB::mysql_secure_string($table);
        }

        $query = "SELECT name
                  FROM $table
                  WHERE id = $id";

        return DB::get_field('name', $query);
    }

}

?>