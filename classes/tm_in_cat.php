<?php

/**
 * Work with categories in TM.
 * @author CheS
 */
class tm_in_cat
{
    /**
     * Get all tms in cats from base
     * @param  int $tm_id trade mark ID
     * @return array Array of cats in tm links
     */
    
    static function get_by_tm($tm_id)
    {

        $query = "SELECT tm_id, cat_id, h1, title, description, keywords, full_desc, help
                  FROM rk_tm_in_cat
                  WHERE tm_id = $tm_id
                  ORDER BY cat_id";

        return DB::get_rows($query);
    }
    /**
     * Get all tms in cats from base by category ID
     *
     * @param  int $category_id Category ID
     *
     * @return array Array of cats in tm links
     */

    public static function get_by_category($category_id)
    {
        $query = 'SELECT *
                  FROM rk_tm_in_cat AS tc
                  WHERE tc.cat_id = ' . (int)$category_id;

        return DB::get_rows($query);
    }

    /**
     * Returns all categories linked to trademark
     *
     * @param int $tm_id Trademark ID
     *
     * @return array
     */
    public static function get_categories($tm_id)
    {
        $query = 'SELECT * FROM rk_categories AS c WHERE c.visible = 1 AND c.id IN (SELECT tc.cat_id FROM rk_tm_in_cat AS tc WHERE tc.tm_id = ' . ((int)$tm_id) . ') ORDER BY c.name';

        return DB::get_rows($query);
    }

    /**
     * Returns category-trademark pair
     *
     * @param int $tm_id Trademark ID
     * @param int $cat_id Category ID
     *
     * @return array
     */
    public static function get_pair($tm_id, $cat_id)
    {
        $tm_id = (int)$tm_id;
        $cat_id = (int)$cat_id;

        $query = 'SELECT ct.tm_id, ct.cat_id, ct.title, ct.description, ct.keywords, ct.full_desc,
                  IF(c.h1 != "", c.h1, c.name) AS cat_name, c.parent_1 AS cat_parent_1, c.parent_2 AS cat_parent_2, c.h1_1_organik AS cat_h1_1_organik, c.h1_2_organik AS cat_h1_2_organik,
                  CONCAT(c._full_url, "/", IF(t.url != "", t.url, t.id)) AS url,
                  t.name AS tm_name,
                  td.alt_name AS tm_alt_name,
                  ct.h1,
                  cs.name_alt_number AS cat_name_alt_number, cs.h1_alt_number AS cat_h1_alt_number, cs.name_plural AS cat_name_plural, cs.h1_plural AS cat_h1_plural
                  FROM rk_tm_in_cat AS ct
                  INNER JOIN rk_categories AS c ON c.id = ct.cat_id
                  INNER JOIN tm AS t ON t.id = ct.tm_id
                  INNER JOIN rk_tm_desc AS td ON td.id = t.id
                  INNER JOIN rk_cat_seo AS cs ON cs.id = c.id
                  WHERE tm_id = ' . $tm_id . '
                  AND   cat_id = ' . $cat_id;

        return DB::get_row($query);
    }

    /**
     * Get urls for trade marks in categories
     * @return array 
     */
    public static function get_urls()
    {
        $query = "SELECT IF( tm.url =  '', tm.id, tm.url ) tm_url, c._full_url cat_url, tc.lastmod
                  FROM rk_tm_in_cat tc, rk_categories c, tm
                  WHERE tm.id = tc.tm_id
                  AND   c.id = tc.cat_id 
                  AND c.visible = 1
                  AND tm.visible = 1
                  AND c._full_url != ''
                  ORDER BY tm.id";

        return DB::get_rows($query);
    }

    public static function get_list($query = null)
    {
        $query = $query !== null ? DB::escape($query) : null;

        return DB::get_rows(
            'SELECT tc.cat_id, tc.tm_id, tc.h1, tc.title, tc.description, tc.keywords,
            CONCAT(c._full_url, "/", IF(t.url != "", t.url, t.id)) AS url,
            c.name AS cat_name, c.h1 AS cat_h1,
            t.name AS tm_name
            FROM rk_tm_in_cat AS tc
            INNER JOIN rk_categories AS c ON c.id = tc.cat_id
            INNER JOIN tm AS t ON t.id = tc.tm_id
            ' .
            (
                $query !== null
                    ? ' WHERE (' .
                        implode(
                            ' OR ',
                            [
                                't.name LIKE "%' . $query . '%"',
                                'c.name LIKE "%' . $query . '%"',
                                'c.h1 LIKE "%' . $query . '%"',
                                'tc.h1 LIKE "%' . $query . '%"',
                                'c._full_url LIKE "%' . $query . '%"',
                                't.url LIKE "%' . $query . '%"',
                                '(CONCAT(c._full_url, "/", IF(t.url != "", t.url, t.id)) LIKE "%' . $query . '%")',
                            ]
                        ) .
                        ')'
                    : ''
            ) .
            ' LIMIT 100'
        );
    }

    /**
     * Updates trademark in category h1, title, description, keywords
     *
     * @param int $tmID Trademark ID
     * @param int $catID $catID ID
     * @param string $h1 H1 value
     * @param string $title Title value
     * @param string $description Description value
     * @param string $keywords Keywords value
     * @param string $pageText Page text
     *
     * @return bool
     */
    public static function update_seo($tmID, $catID, $h1, $title, $description, $keywords, $pageText)
    {
        return !!DB::query('UPDATE rk_tm_in_cat SET h1 = "' . DB::escape($h1) . '", title = "' . DB::escape($title) . '", description = "' . DB::escape($description) . '", keywords = "' . DB::escape($keywords) . '", full_desc = "' . DB::escape($pageText) . '" WHERE tm_id = ' . ((int)$tmID) . ' AND cat_id = ' . ((int)$catID));
    }
}
