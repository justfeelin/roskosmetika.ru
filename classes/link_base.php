<?php

abstract class link_base
{
    /**
     * Category type ID
     */
    const CATEGORY_TYPE_ID = 1;

    /**
     * Product type ID
     */
    const PRODUCT_TYPE_ID = 2;

    /**
     * Trademark type ID
     */
    const TRADEMARK_TYPE_ID = 3;

    /**
     * Trademark line type ID
     */
    const TRADEMARK_LINE_TYPE_ID = 4;

    /**
     * Category-trademark type ID
     */
    const CATEGORY_TM_TYPE_ID = 5;

    /**
     * Type page type ID
     */
    const TYPE_PAGE_TYPE_ID = 6;

    /**
     * Type page trademark type ID
     */
    const TYPE_PAGE_TRADEMARK_TYPE_ID = 7;

    /**
     * Type page category type ID
     */
    const TYPE_PAGE_CATEGORY_TYPE_ID = 8;

    /**
     * Parses URL and returns type ID and item(s) ID
     *
     * @param string $url URL to parse
     *
     * @return array [typeID, item_1_id, item_2_id, item_3_id, item_4_id]
     */
    public static function parse_url($url) {
        $types = [
            [
                'id' => self::CATEGORY_TYPE_ID,
                'check' => function ($url) {
                    if (
                        preg_match('#/category/([^/]+)(?:/([^/]+))?(?:/([^/]+))?$#', $url, $m)
                        && ($category = Categories::parse_url([$m[1], isset($m[2]) ? $m[2] : null, isset($m[3]) ? $m[3] : null], null))
                    ) {
                        return [
                            $category['ids'][0],
                            $category['ids'][1],
                            $category['ids'][2],
                        ];
                    }

                    return null;
                },
            ],
            [
                'id' => self::PRODUCT_TYPE_ID,
                'check' => function ($url) {
                    if (
                        preg_match('#/product/([^/]+)$#', $url, $m)
                        && ($productID = Product::get_id_by_url(DB::escape($m[1])))
                    ) {
                        return [
                            $productID,
                        ];
                    }

                    return null;
                }
            ],
            [
                'id' => self::TRADEMARK_TYPE_ID,
                'check' => function ($url) {
                    if (
                        preg_match('#/tm/([^/]+)$#', $url, $m)
                        && ($tmID = tm::get_id_by_url(DB::escape($m[1])))
                    ) {
                        return [
                            $tmID,
                        ];
                    }

                    return null;
                }
            ],
            [
                'id' => self::TRADEMARK_LINE_TYPE_ID,
                'check' => function ($url) {
                    if (
                        preg_match('#/tm/([^/]+)/([^/]+)$#', $url, $m)
                        && ($tmLine = tm::parse_url([$m[1], $m[2]], true))
                    ) {
                        return [
                            $tmLine['ids'][0],
                            $tmLine['ids'][1],
                        ];
                    }

                    return null;
                }
            ],
            [
                'id' => self::CATEGORY_TM_TYPE_ID,
                'check' => function ($url) {
                    if (
                        preg_match('#/category-tm/([^/]+)/([^/]+)(?:/([^/]+))?(?:/([^/]+))?$#', $url, $m)
                        && ($categoryTm = tm::parse_category_tm_url([$m[1], $m[2], isset($m[3]) ? $m[3] : null, isset($m[4]) ? $m[4] : null], null))
                    ) {
                        return array_merge(
                            [
                                $categoryTm['tm_id'],
                            ],
                            $categoryTm['category_ids']
                        );
                    }

                    return null;
                },
            ],
            [
                'id' => self::TYPE_PAGE_TYPE_ID,
                'check' => function ($url) {
                    if (
                        preg_match('#/kosmetika/([^/]+)$#', $url, $m)
                        && ($typePage = type_page::get_type_page_by_url($m[1]))
                    ) {
                        return [
                            $typePage['id'],
                        ];
                    }

                    return null;
                }
            ],
            [
                'id' => self::TYPE_PAGE_TRADEMARK_TYPE_ID,
                'check' => function ($url) {
                    if (
                        preg_match('#/kosmetika/([^/]+)/([^/]+)$#', $url, $m)
                        && ($typePage = type_page::get_type_page_by_url($m[1]))
                        && ($tmID = tm::get_id_by_url(DB::escape($m[2])))
                        && ($typePageTm = type_page::get_type_page_tm($typePage['id'], $tmID))
                    ) {
                        return [
                            $typePage['id'],
                            $tmID,
                        ];
                    }

                    return null;
                }
            ],
            [
                'id' => self::TYPE_PAGE_CATEGORY_TYPE_ID,
                'check' => function ($url) {
                    if (
                        preg_match('#/kosmetika/([^/]+)/([^/]+)(?:/([^/]+))?(?:/([^/]+))?$#', $url, $m)
                        && ($typePage = type_page::get_type_page_by_url($m[1]))
                        && ($category = Categories::parse_url([$m[2], isset($m[3]) ? $m[3] : null, isset($m[4]) ? $m[4] : null], null))
                    ) {
                        return [
                            $typePage['id'],
                            $category['ids'][0],
                            $category['ids'][1],
                            $category['ids'][2],
                        ];
                    }

                    return null;
                },
            ],
        ];

        for ($i = 0, $n = count($types); $i < $n; ++$i) {
            if ($m = $types[$i]['check']($url)) {
                return [
                    $types[$i]['id'],
                    $m[0],
                    !empty($m[1]) ? $m[1] : null,
                    !empty($m[2]) ? $m[2] : null,
                    !empty($m[3]) ? $m[3] : null,
                ];
            }
        }

        return null;
    }

    /**
     * Inserts/updates row
     *
     * @param array $donor Donor info [type_id, item_1_id, item_2_id, item_3_id, item_4_id]
     * @param array $acceptor Acceptor info [type_id, item_1_id, item_2_id, item_3_id, item_4_id]
     * @param string $anchor Anchor value
     */
    public static function update($donor, $acceptor, $anchor)
    {
        $src_type_id = (int)$donor[0];
        $src_1_id = isset($donor[1]) ? (int)$donor[1] : 0;
        $src_2_id = isset($donor[2]) ? (int)$donor[2] : 0;
        $src_3_id = isset($donor[3]) ? (int)$donor[3] : 0;
        $src_4_id = isset($donor[4]) ? (int)$donor[4] : 0;

        $dst_type_id = (int)$acceptor[0];
        $dst_1_id = isset($acceptor[1]) ? (int)$acceptor[1] : 0;
        $dst_2_id = isset($acceptor[2]) ? (int)$acceptor[2] : 0;
        $dst_3_id = isset($acceptor[3]) ? (int)$acceptor[3] : 0;
        $dst_4_id = isset($acceptor[4]) ? (int)$acceptor[4] : 0;

        if (
            !(
                $rowID = DB::get_field(
                    'id',
                    'SELECT l.id
                    FROM rk_link_base AS l
                    WHERE
                        l.src_type_id = ' . $src_type_id . '
                        AND l.src_item_1_id = ' . $src_1_id . '
                        AND l.src_item_2_id = ' . $src_2_id . '
                        AND l.src_item_3_id = ' . $src_3_id . '
                        AND l.src_item_4_id = ' . $src_4_id . '
                        AND l.dst_type_id = ' . $dst_type_id . '
                        AND l.dst_item_1_id = ' . $dst_1_id . '
                        AND l.dst_item_2_id = ' . $dst_2_id . '
                        AND l.dst_item_3_id = ' . $dst_3_id . '
                        AND l.dst_item_4_id = ' . $dst_4_id
                )
            )
        ) {
            DB::query('INSERT INTO rk_link_base
                      (src_type_id, src_item_1_id, src_item_2_id, src_item_3_id, src_item_4_id, dst_type_id, dst_item_1_id, dst_item_2_id, dst_item_3_id, dst_item_4_id, _dst_link, anchor) VALUES
                      (' . $src_type_id . ', ' . $src_1_id . ', ' . $src_2_id . ', ' . $src_3_id . ', ' . $src_4_id . ', ' .
                        $dst_type_id . ', ' . $dst_1_id . ', ' . $dst_2_id . ', ' . $dst_3_id . ', ' . $dst_4_id . ', ' .
                        '"", "' . DB::escape($anchor) . '")'
            );
        } else {
            DB::query('UPDATE rk_link_base SET anchor = "' . DB::escape($anchor). '" WHERE id = ' . $rowID);
        }
    }

    /**
     * Returns array of links connected to given page
     *
     * @param string $typeID Page type ID
     * @param int $item_1_id Item 1 ID
     * @param int $item_2_id Item 2 ID
     * @param int $item_3_id Item 3 ID
     * @param int $item_4_id Item 4 ID
     *
     * @return array Each item: ['url' => '/category/masla', 'anchor' => 'Masla']
     */
    public static function get_links($typeID, $item_1_id, $item_2_id = 0, $item_3_id = 0, $item_4_id = 0)
    {
        return DB::get_rows(
            'SELECT l._dst_link AS url, l.anchor
            FROM rk_link_base AS l
            WHERE l.src_type_id = ' . ((int)$typeID) . '
                AND l.src_item_1_id = ' . ((int)$item_1_id) . '
                AND l.src_item_2_id = ' . ((int)$item_2_id) . '
                AND l.src_item_3_id = ' . ((int)$item_3_id) . '
                AND l.src_item_4_id = ' . ((int)$item_4_id)
        );
    }

    /**
     * Updates _dst_link values
     */
    public static function update_links()
    {
        DB::get_rows(
            'SELECT * FROM rk_link_base',
            function ($link) {
                $url = null;

                switch ((int)$link['dst_type_id']) {
                    case self::CATEGORY_TYPE_ID:
                        $categoryUrls = self::_category_urls($link['dst_item_3_id'] ?: ($link['dst_item_2_id'] ?: $link['dst_item_1_id']));

                        $url = '/category/' . implode('/', $categoryUrls);

                        break;

                    case self::PRODUCT_TYPE_ID:
                        $product = Product::get_by_id($link['dst_item_1_id']);

                        $url = '/product/' . $product['url'];

                        break;

                    case self::TRADEMARK_TYPE_ID:
                        $tm = tm::get_by_id($link['dst_item_1_id']);

                        $url = '/tm/' . $tm['url'];

                        break;

                    case self::TRADEMARK_LINE_TYPE_ID:
                        $line = lines::get_by_id($link['dst_item_2_id']);

                        $url = '/tm/' . $line['tm_url'] . '/' . $line['url'];

                        break;

                    case self::CATEGORY_TM_TYPE_ID:
                        $tm = tm::get_by_id($link['dst_item_1_id']);

                        $categoryUrls = self::_category_urls($link['dst_item_4_id'] ?: ($link['dst_item_3_id'] ?: $link['dst_item_2_id']));

                        $url = '/category-tm/' . implode('/', $categoryUrls) . '/' . $tm['url'];

                        break;

                    case self::TYPE_PAGE_TYPE_ID:
                        $typePage = type_page::get_page($link['dst_item_1_id']);

                        $url = '/kosmetika/' . $typePage['url'];

                        break;

                    case self::TYPE_PAGE_TRADEMARK_TYPE_ID:
                        $typePage = type_page::get_page($link['dst_item_1_id']);
                        $tm = tm::get_by_id($link['dst_item_2_id']);

                        $url = '/kosmetika/' . $typePage['url'] . '/' . $tm['url'];

                        break;

                    case self::TYPE_PAGE_CATEGORY_TYPE_ID:
                        $typePage = type_page::get_page($link['dst_item_1_id']);

                        $categoryUrls = self::_category_urls($link['dst_item_4_id'] ?: ($link['dst_item_3_id'] ?: $link['dst_item_2_id']));

                        $url = '/kosmetika/' . $typePage['url'] . '/' . implode('/', $categoryUrls);

                        break;
                }

                if ($url) {
                    DB::query('UPDATE rk_link_base SET _dst_link = "' . DB::escape($url) . '" WHERE id = ' . $link['id']);
                }
            }
        );
    }

    /**
     *
     * @param $categoryID
     * @return array
     */
    private static function _category_urls($categoryID)
    {
        $category = Categories::get_by_id((int)$categoryID);

        $urls[] = $category['url'];

        if ($category['level'] > 1) {
            $parentCategory = Categories::get_by_id((int)$category['parent_1']);

            $urls[] = $parentCategory['url'];

            if ($category['level'] == 3) {
                $parentLevel1 = Categories::get_by_id((int)$category['parent_2']);

                $urls[] = $parentLevel1['url'];
            }
        }

        return array_reverse($urls);
    }
}
