<?php

/**
 * Working with favourites
 */
class Favorites
{

    /**
     * Layout favourites products for client
     * @param integer $user_id client Id.
     * @return array
     */
    static function show($user_id)
    {
        $user_id = (int)$user_id;

        $query = "SELECT p.id, IF(p.url =  '', p.id, p.url ) url, p.main_id, p.name, p.pack, p.available, p.price, p.special_price, p.visible, 
                         pp.src, pp.alt, pp.title photo_title
                  FROM  rk_favorites f, products p LEFT JOIN rk_prod_photo pp  ON ( pp.id = p.id )
                  WHERE f.client_id = $user_id
                  AND   f.prod_id = p.id
                  AND   p.visible = 1
                  ORDER BY p.name";


        return products::get_pack_info(DB::get_rows($query));

    }

    /**
     * Add product to favourites
     * @param integer $prod_id product Id.
     * @param integer $user_id client Id.
     * @return boolean
     */
    static function add($prod_id, $user_id)
    {
        $prod_id = (int)$prod_id;
        $user_id = (int)$user_id;

        // is isset product
        if (product::check_prod_existence($prod_id)) {
            // check existence
            $query = "SELECT prod_id
                      FROM  rk_favorites
                      WHERE client_id = $user_id
                      AND   prod_id = $prod_id";

            // no in favourites	  		
            if (!DB::get_num_rows($query)) {

                $query = "INSERT INTO rk_favorites
                          SET client_id = $user_id,
                              prod_id = $prod_id";
                DB::query($query);
            }

            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Delete product from favourites
     * @param integer $prod_id product Id.
     * @param integer $user_id client Id.
     * @return boolean
     */
    static function del($prod_id, $user_id)
    {
        $prod_id = (int)$prod_id;
        $user_id = (int)$user_id;

        $query = "DELETE FROM rk_favorites
                  WHERE client_id = $user_id
                  AND   prod_id = $prod_id";

        return DB::query($query);
    }

    /**
     * Return quantity different products in favourites
     * @param integer $user_id Client Id.
     * @return int quantity of products
     */
    static function count_favourites($user_id)
    {

        $query = "SELECT prod_id
                  FROM   rk_favorites
                  WHERE client_id = $user_id";

        return count(DB::get_rows($query));

    }

}
