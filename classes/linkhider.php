<?php

/**
 * Class LinkHider
 *
 * Links encryption mechanism based on Caesar's code
 */
abstract class LinkHider
{
    /**
     * Encodes value
     *
     * @param string $value Unencoded value
     *
     * @return string
     */
    public static function encode($value)
    {
        $offset = rand(100, 700);

        $charcodes = array($offset);

        $i = 0;

        while ($i >= 0) {
            $charcodes[] = self::_ord($value, $i) + $offset;
        }

        return implode('.', $charcodes);
    }

    /**
     * ord() with UTF-8 support
     *
     * @param string $string Original string
     * @param int $offset Offset to return code at
     *
     * @return int -1 if last symbol, next symbol offset otherwise
     */
    private static function _ord($string, &$offset)
    {
        $code = ord(substr($string, $offset, 1));

        if ($code >= 128) {//0xxxxxxx
            if ($code < 224) $bytesnumber = 2;//110xxxxx
            else if ($code < 240) $bytesnumber = 3;//1110xxxx
            else if ($code < 248) $bytesnumber = 4;//11110xxx
            else $bytesnumber = 0;

            $codetemp = $code - 192 - ($bytesnumber > 2 ? 32 : 0) - ($bytesnumber > 3 ? 16 : 0);

            for ($i = 2; $i <= $bytesnumber; $i++) {
                $offset++;
                $code2 = ord(substr($string, $offset, 1)) - 128;        //10xxxxxx
                $codetemp = $codetemp * 64 + $code2;
            }

            $code = $codetemp;
        }

        $offset += 1;

        if ($offset >= strlen($string)) $offset = -1;

        return $code;
    }
}
