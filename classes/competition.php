<?php

class competition
{

    /**
     * Return competition parameters for layout
     * @param  int     $id id of item
     * @param  boolean $win flag winners list
     * @return array|boolean      parameters of product
     */
    static function get_competition($id, $win = FALSE)
    {
        $query = 'SELECT c.id, c.url, c.description, c. name, c.file_name, c.type, c.alt, c.win_link, c.begin, c.end, c.visible
         FROM rk_competition AS c
         WHERE c.id ='. $id;

        $competition = db::get_row($query);

        $query = 'SELECT cc.id, cc.name, cc.client_id, cc.email, cc.comp_id, cc.winner
        FROM rk_competition_clients AS cc
        WHERE cc.comp_id ='.$id;
        $players = db::get_rows($query);

        if (isset($competition['id'])) {

            $competition['players'] = $players;
            $time = strtotime($competition['end'])-strtotime(date('d.m.Y'));
            $competition['day'] = ($time < 0)? -1:$time/(3600*24);

            return $competition;

        } else {
            return FALSE;
        }

    }

    /**
     * Get competition id
     * @param  string $url competition url
     * @return int  id
     */
    static function get_competition_id($url){
        return DB::get_field('id', "SELECT id FROM rk_competition WHERE url='$url'");
    }

    /**
     * Check id for redirect on URL
     * @param  int $id competition id
     * @return string  url for redirect on
     */
    static function redirect($id)
    {

        return DB::get_field('url', "SELECT url FROM rk_competition WHERE id = $id");

    }


    /**
     * Add or rewrite client in rk_competition_clients
     * @param string $name clients name
     * @param string $phone clients phone
     * @param int $client_id clients id
     * @param string $email clients email
     * @param int $comp_id competition id
     */
    static function  add_client($name, $phone, $client_id, $email, $comp_id){
        //check email
        $exist_mail = self::check_existence($email, $comp_id);

        if ($exist_mail) {
            //rewrite client
            self::rewrite_client($name, $phone, $client_id, $email, $comp_id);

        } else {
            //add client
            self::add_new_client($name, $phone, $client_id, $email, $comp_id);

        }

    }

    /**
     * Add new client in rk_competition_clients
     * @param string $name clients name
     * @param string $phone clients phone
     * @param int $client_id clients id
     * @param string $email clients email
     * @param int $comp_id competition id
     * @return mysqli_result
     */
    static function  add_new_client($name, $phone, $client_id, $email, $comp_id)
    {
        $query = "INSERT INTO rk_competition_clients (name, client_id, email, phone, comp_id)
                  VALUE ('" . DB::escape($name) ."', '" . $client_id ."', '" . DB::escape($email) ."', '" . DB::escape($phone) ."', '" . $comp_id . "')";

        return DB::query($query);

    }

    /**
     * Rewrite client in rk_competition_clients
     * @param string $name clients name
     * @param string $phone clients phone
     * @param int $client_id clients id
     * @param string $email clients email
     * @param int $comp_id competition id
     * @return mysqli_result
     */
    static function  rewrite_client($name, $client_id, $email,$phone, $comp_id)
    {
        $query = "UPDATE rk_competition_clients SET
                      name = '" . DB::escape($name) . "',
                      
                      client_id = '" . DB::escape($client_id) . "',
                      email = '" . DB::escape($email) . "',
                      phone = '" . DB::escape($phone) . "',
                      comp_id = '" . DB::escape($comp_id) . "'                  
                  WHERE  email = LOWER('" . DB::escape($email) . "')
                  AND comp_id = '" . DB::escape($comp_id) . "'";

        return DB::query($query);

    }

    /**
     * Check email
     * @return array $exist_email
     */

    static function check_existence($email, $comp_id)
    {

        $query = "SELECT *
                  FROM rk_competition_clients
                  WHERE email = LOWER('" . DB::escape($email) . "') AND comp_id =". $comp_id;

        return DB::get_row($query);
    }
//array
    /**
     * Return competition list
     * @param boolean $only_visible Выборка только актуальных акций.
     * @param int $onPage
     *
     * @return
     */
    static function get_competition_list($only_visible = true, $onPage = null, $dateDescending = false)
    {
        $where = $only_visible ? ' WHERE c.visible = 1 AND c.end > NOW()' : '';
        if ($onPage !== null) {
            $count = (int)DB::get_field('cnt', 'SELECT COUNT(c.id) AS cnt FROM rk_competition AS c' . $where);

            Pagination::setValues($count, $onPage);

            Pagination::setTemplateSettings();
        }

        $query = 'SELECT c.id,
                         c.name,
                         c.end,
                         c.url,
                         IF(c.end > NOW(),1,0) AS status,
                         c.visible,
                         (SELECT COUNT(cc.id) FROM rk_competition_clients AS cc WHERE cc.comp_id = c.id) AS `count`
                  FROM rk_competition AS c ' .
            $where .
            ' ORDER BY c.end' . ($dateDescending ? ' DESC' : '') .
            ($onPage !== null ? Pagination::sqlLimit() : '');

        return DB::get_rows($query);
    }

    /**
     * Скрывает/отображает акцию.
     * @param integer $competition_id ID акция
     * @return boolean
     */
    static function change_competition_visibility($competition_id)
    {
        $competition_id = (int)$competition_id;

        $query = "UPDATE rk_competition
                  SET visible = (IF(visible = 1, 0, 1))
                  WHERE id = $competition_id";
        return DB::query($query);
    }

    /**
     * Меняет признак победитель/лузер.
     * @param integer $clients_id ID акция
     * @return boolean
     */
    static function change_winner($clients_id)
    {
        $clients_id = (int)$clients_id;

        $query = "UPDATE rk_competition_clients
                  SET winner = (IF(winner = 1, 0, 1))
                  WHERE id = $clients_id";
        return DB::query($query);
    }



    /**
     * Возвращает список участников акции.
     * @param integer $competitions_id ID акции.
     * @return array
     */
    static function get_subscribes_list($competitions_id = 0)
    {
        $competitions_id = (int)$competitions_id;

        $query = 'SELECT cc.id,
                         cc.name,                         
                         cc.email,
                         cc.phone,
                         cc.winner,                      
                         c.end AS competitions_date,
                         c.name AS competitions_name
                  FROM rk_competition_clients AS cc,
                       rk_competition AS c
                  WHERE c.id = cc.comp_id';
        if ($competitions_id) {
            $query .= " AND cc.comp_id = $competitions_id";
        }
        $query .= ' ORDER BY c.end';

        return DB::get_rows($query);
    }

    /**
     * Сохраняет изменения в акции, если id = 0, то добавляет новую.
     * @param <type> $id ID акции.
     * @param string $url Адрес.
     * @param string $description Описание.
     * @param string $name Название.
     * @param string $file_name Имя картинки.
     * @param string $type Тип файла картинки.
     * @param string $alt .
     * @param string $win_link Ссылка на победителей
     * @param string $begin Дата начала.
     * @param string $end Дата завершения.
     * @param boolean $visible Отображать или нет
     */
    static function save_competition($id, $url, $description, $name, $file_name, $type, $alt, $win_link, $begin, $end, $visible)
    {
        $id = (int)$id;
        $visible = (int)$visible;


        if (!get_magic_quotes_gpc()) {
            $name = DB::mysql_secure_string(trim($name));
            $url = DB::mysql_secure_string(trim($url));
            $description = DB::mysql_secure_string(trim($description));
            $file_name = DB::mysql_secure_string(trim($file_name));
            $type = DB::mysql_secure_string(trim($type));
            $alt = DB::mysql_secure_string(trim($alt));
            $win_link = DB::mysql_secure_string(trim($win_link));
            $begin = DB::mysql_secure_string(trim($begin));
            $end = DB::mysql_secure_string(trim($end));
        }
        // Добавляем новую акцию.
        if ($id == 0) {
            $query = "INSERT INTO rk_competition (name, url, description, file_name, type, alt, win_link, begin, end, visible)
                      VALUES ('$name',  '$url', '$description', '$file_name', '$type', '$alt', '$win_link', '$begin', '$end', $visible)";
        } // Сохраняем изменеия в акции.
        else {
            $query = "UPDATE rk_competition
                      SET name = '$name',
                          url = '$url',
                          description = '$description',
                          file_name = '$file_name',
                          type = '$type',
                          alt = '$alt',
                          win_link = '$win_link',
                          begin = '$begin',
                          end = '$end',
                          visible = $visible
                      WHERE id = $id";
        }
        return DB::query($query);
    }


}
?>