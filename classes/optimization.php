<?php

/**
 * SQL optimization
 */
class optimization
{

    /**
     * Delete special link tables
     */
    static function del_link_tables()
    {

        DB::query("TRUNCATE rk_prod_cat_lvl_1");
        DB::query("TRUNCATE rk_prod_cat_lvl_2");

    }

    /**
     * Add special link tables
     */
    static function add_link_tables()
    {
        $query = 'INSERT INTO rk_prod_cat_lvl_1 (prod_id, cat_id)
                 SELECT DISTINCT pc.prod_id, c.parent_2 AS cat_id
                 FROM rk_categories AS c, rk_prod_cat AS pc
                 WHERE c.visible = 1
                 AND   c.is_tag = 0
                 AND   c.id = pc.cat_id
                 ON DUPLICATE KEY UPDATE rk_prod_cat_lvl_1.prod_id = rk_prod_cat_lvl_1.prod_id';

        DB::query($query);

        $query = 'INSERT INTO rk_prod_cat_lvl_2 (prod_id, cat_id)
                  SELECT DISTINCT pc.prod_id, c.parent_1 cat_id
                  FROM rk_categories AS c, rk_prod_cat AS pc
                  WHERE c.visible = 1
                  AND   c.is_tag = 0
                  AND   c.id = pc.cat_id
                  ON DUPLICATE KEY UPDATE rk_prod_cat_lvl_2.prod_id = rk_prod_cat_lvl_2.prod_id';

        DB::query($query);

        $joins = Categories::product_joins();

        DB::get_rows(
            'SELECT c.id, c.level FROM rk_categories AS c WHERE c.is_tag = 1 AND c.level != 3',
            function ($tag) use ($joins) {
                DB::query(
                    'INSERT INTO ' . $joins[(int)$tag['level']] . ' (prod_id, cat_id)
                    SELECT DISTINCT c.prod_id, ' . $tag['id'] . '
                    FROM (
                        SELECT pc.prod_id
                        FROM ' . $joins[3] . ' AS pc
                        WHERE pc.cat_id IN (
                            SELECT c.id
                            FROM rk_categories AS c
                            WHERE c.is_tag = 1
                                AND (
                                    c.parent_1 = ' . $tag['id'] . '
                                    OR c.parent_2 = ' . $tag['id'] . '
                                )
                        )
                        UNION
                        SELECT pcj.prod_id
                        FROM ' . $joins[3] . ' AS pcj
                        WHERE pcj.cat_id = ' . $tag['id'] . '
                    ) AS c
                    ON DUPLICATE KEY UPDATE ' . $joins[(int)$tag['level']] . '.prod_id = ' . $joins[(int)$tag['level']] . '.prod_id'
                );
            }
        );

        $joins = Categories::product_joins();

        $exludeSQL = implode(', ', Product::ignore_ids());

        foreach ($joins AS $level => $table) {
            DB::query(
                'UPDATE rk_categories AS c
                SET c._has_products = (
                    SELECT IF(COUNT(cp.prod_id) > 0, 1, 0)
                    FROM ' . $table . ' AS cp
                    INNER JOIN products AS p ON p.id = cp.prod_id
                        AND p.visible = 1
                        AND (p.active = 1 OR p.price = 0 AND p.new = 1)
                    WHERE cp.cat_id = c.id
                        AND cp.prod_id NOT IN (' . $exludeSQL . ')
                )
            WHERE c.level = ' . $level
            );
        }
    }

    /**
     * Updates lines "_has_products" field
     */
    public static function lines_has_products()
    {
        DB::query(
            'UPDATE rk_lines AS l
            SET l._has_products = (
                SELECT IF(COUNT(pl.prod_id) > 0, 1, 0)
                FROM rk_prod_lines AS pl
                INNER JOIN products AS p ON p.id = pl.prod_id AND p.id NOT IN (' . implode(', ', Product::ignore_ids()) . ')
                    AND p.visible = 1
                    AND (p.active = 1 OR p.price = 0 AND p.new = 1)
                    AND p.main_id = 0
                    AND (p.visible = 1 OR IF((SELECT COUNT(pp.id) FROM products AS pp WHERE pp.visible = 1 AND pp.main_id = p.id), 1, 0))
                WHERE pl.line_id = l.id
            )'
        );
    }

    /**
     *  Create article - category link
     */
    static function article_cat()
    {


        //  clear table
        DB::query('TRUNCATE rk_article_cat_2');

        // take articles
        $art_cats = DB::get_rows('SELECT DISTINCT pc2.cat_id, ap.article_id
                                  FROM  rk_articleprods ap, rk_prod_cat_lvl_2 pc2
                                  WHERE ap.prod_id = pc2.prod_id');

        foreach ($art_cats as $art_cat) {
            DB::query("INSERT INTO rk_article_cat_2 (article_id, cat_id) VALUES ({$art_cat['article_id']}, {$art_cat['cat_id']})");
        }

        //  3 level

        // clear vars 
        $art_cats = NULL;
        $art_cat = NULL;

        //  clear table
        DB::query('TRUNCATE rk_article_cat_3');

        // take articles
        $art_cats = DB::get_rows('SELECT DISTINCT pc.cat_id, ap.article_id
                                  FROM  rk_articleprods ap, rk_prod_cat pc
                                  WHERE ap.prod_id = pc.prod_id');

        foreach ($art_cats as $art_cat) {
            DB::query("INSERT INTO rk_article_cat_3 (article_id, cat_id) VALUES ({$art_cat['article_id']}, {$art_cat['cat_id']})");
        }

    }


    /**
     * Generate promo code
     * @param  integer $size Size of promo code
     * @return string        promo code string
     */
    static function make_code($size = 6)
    {

        //   generate promo code
        $arr = array('a', 'b', 'c', 'd', 'e', 'f',
            'g', 'h', 'i', 'j', 'k', 'l',
            'm', 'n', 'o', 'p', 'r', 's',
            't', 'u', 'v', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F',
            'G', 'H', 'I', 'J', 'K', 'L',
            'M', 'N', 'O', 'P', 'R', 'S',
            'T', 'U', 'V', 'X', 'Y', 'Z',
            '1', '2', '3', '4', '5', '6',
            '7', '8', '9', '0');

        $code = '';

        for ($i = 0; $i < $size; $i++) {
            // random array index
            $step_number = rand(0, count($arr) - 1);
            $code .= $arr[$step_number];
        }

        return $code;
    }

    /**
     * Updates tm_in_category table
     */
    public static function category_tm()
    {
        $joins = categories::product_joins();

        DB::get_rows(
            'SELECT c.id, IF( c.h1 =  "", c.name, c.h1 ) AS name, c.h1, c.level, c.parent_1, c.parent_2 FROM rk_categories AS c WHERE c.visible = 1 AND c.is_tag = 0',
            function ($category) use ($joins) {
                $cID = (int)$category['id'];

                $cName = $category['name'];

                $cLevel = (int)$category['level'];

                switch ($cLevel) {
                    case 1:
                        $categoryCondition = 'IN (SELECT cc.id FROM rk_categories AS cc WHERE cc.parent_2 = ' . $cID . ')';
                        break;

                    case 2:
                        $categoryCondition = 'IN (SELECT cc.id FROM rk_categories AS cc WHERE cc.parent_1 = ' . $cID . ')';
                        break;

                    case 3:
                        $categoryCondition = '= ' . $cID;
                        break;

                    default:
                        return;
                }

                $joinTable = $joins[$cLevel];

                $tmIDs = [];

                DB::get_rows(
                    'SELECT t.id, t.name, td.alt_name
                    FROM tm AS t
                    LEFT JOIN rk_tm_desc AS td ON td.id = t.id
                    WHERE t.id IN (
                        SELECT pd.tm_id
                        FROM rk_prod_desc AS pd
                        WHERE pd.id IN (
                            SELECT pc.prod_id FROM rk_prod_cat AS pc WHERE pc.cat_id ' . $categoryCondition . '
                        )
                    )',
                    function ($tm) use ($cID, $cName, $joinTable, &$tmIDs) {
                        $tID = (int)$tm['id'];

                        $tmIDs[] = $tID;

                        $tName = $tm['name'];

                        $tmInCat = DB::get_row('SELECT t.title, t.description FROM rk_tm_in_cat AS t WHERE t.tm_id = ' . $tID . ' AND t.cat_id = ' . $cID);

                        $help = $cName . ' | ' . $tName;

                        $title = $tmInCat ? $tmInCat['title'] : '';

                        $description = $tmInCat ? $tmInCat['description'] : '';

                        DB::query('REPLACE INTO rk_tm_in_cat (tm_id, cat_id, help, title, description, _has_products) VALUES (' . $tID . ', ' . $cID . ', "' . $help . '", "' . ($title ? DB::mysql_secure_string($title) : '') . '", "' . ($description ? DB::mysql_secure_string($description) : '') . '",
                            IF(
                                (
                                    SELECT COUNT(p.id)
                                    FROM products AS p
                                    INNER JOIN ' . $joinTable . ' AS pc ON pc.prod_id = p.id AND pc.cat_id = ' . $cID . '
                                    WHERE p.visible = 1
                                        AND p.main_id = 0
                                        AND (p.active = 1 OR p.price = 0 AND p.new = 1)
                                        AND p.id NOT IN (' . implode(', ', Product::ignore_ids()) . ')
                                        AND p.tm_id = ' . $tID . '
                                ) > 0,
                                1,
                                0
                            )
                        )');
                    }
                );

                DB::query('UPDATE rk_tm_in_cat SET _has_products = 0 WHERE cat_id = ' . $cID . ($tmIDs ? ' AND tm_id NOT IN (' . implode(', ', $tmIDs) . ')' : ''));
            }
        );
    }

    /**
     * Creates type-pages trademarks rows
     */
    public static function type_page_tm()
    {
        DB::get_rows(
            'SELECT p.* FROM rk_type_page AS p',
            function ($typePage) {
                $typePageID = (int)$typePage['id'];

                DB::get_rows(
                    'SELECT t.id
                    FROM tm AS t',
                    function ($tm) use ($typePageID) {
                        $tmID = (int)$tm['id'];

                        $hasProducts = (int)DB::get_field(
                            'cnt',
                            'SELECT COUNT(p.id) AS cnt
                            FROM products AS p
                            INNER JOIN tm AS t ON t.id = p.tm_id AND t.visible = 1 AND t.id = ' . $tmID . '
                            INNER JOIN rk_prod_type_page AS pp ON pp.product_id = p.id AND pp.type_page_id = ' . $typePageID . '
                            WHERE p.visible = 1
                                AND p.main_id = 0
                                AND (p.active = 1 OR p.price = 0 AND p.new = 1)
                                AND p.id NOT IN (' . implode(', ', Product::ignore_ids()) . ')'
                        ) > 0
                            ? 1
                            : 0;

                        $exists = !!DB::get_field('tm_id', 'SELECT p.tm_id FROM rk_tm_in_type_page AS p WHERE tm_id = ' . $tmID . ' AND type_page_id = ' . $typePageID);

                        if (!$exists) {
                            DB::query('INSERT INTO rk_tm_in_type_page (tm_id, type_page_id, h1, title, description, `text`, `_has_products`) VALUES (' . $tmID . ', ' . $typePageID . ', "", "", "", "", ' . $hasProducts . ')');
                        } else {
                            DB::query('UPDATE rk_tm_in_type_page SET `_has_products` = ' . $hasProducts . ' WHERE tm_id = ' . $tmID . ' AND type_page_id = ' . $typePageID);
                        }
                    }
                );
            }
        );

        DB::query('DELETE FROM rk_tm_in_type_page WHERE type_page_id NOT IN (SELECT p.id FROM rk_type_page AS p) OR tm_id NOT IN (SELECT t.id FROM tm AS t)');
    }

    /**
     * Sets `full_desc_synth` for rk_tm_in_cat
     *
     * @param bool $emptyOnly Apply only for pairs with empty synthetic description
     *
     * @return int Count of processed pairs
     */
    public static function category_tm_description($emptyOnly = false)
    {
        $textValues = require SITE_PATH . 'optimization/category_tm_description.php';

        $joins = Categories::product_joins();

        $categories = [];

        $processed = 0;

        try {
            DB::get_rows(
                'SELECT ct.tm_id, ct.cat_id, c.h1 AS category, c.level, t.name AS tm FROM rk_tm_in_cat AS ct ' .
                'INNER JOIN rk_categories AS c ON c.id = ct.cat_id ' .
                'INNER JOIN tm AS t ON t.id = ct.tm_id' .
                (
                    $emptyOnly
                        ? ' WHERE (ct.full_desc_synth = "" OR ct.full_desc_synth IS NULL)'
                        : ''
                ) . ' LIMIT 5',
                function ($cat_in_tm) use ($textValues, $joins, &$categories, &$processed) {
                    $text = '';

                    for ($paragraph = 0; $paragraph < 3; ++$paragraph) {
                        $variant = rand(0, 2);

                        $text .= '<div class="p">';

                        for ($i = 0, $n = count($textValues['texts'][$variant][$paragraph]); $i < $n; ++$i) {
                            $text .= self::_parseCatTmPart($textValues['texts'][$variant][$paragraph][$i]);
                        }

                        $text .= '</div>';
                    }

                    $categoryID = (int)$cat_in_tm['cat_id'];

                    if (!isset($categories[$categoryID])) {
                        $categories[$categoryID] = [
                            'upper' => mb_strtoupper(mb_substr($cat_in_tm['category'], 0, 1)) . mb_substr($cat_in_tm['category'], 1),
                            'lower' => $category = mb_strtolower(mb_substr($cat_in_tm['category'], 0, 1)) . mb_substr($cat_in_tm['category'], 1),
                        ];
                    }

                    $tm = $cat_in_tm['tm'];

                    $text = preg_replace_callback(
                        '/%(r|v)% \$(c|C)ategory([^a-zA-Z])/',
                        function ($matches) use ($categoryID, $tm, &$categories) {
                            $word = $categories[$categoryID][$matches[2] === 'c' ? 'lower' : 'upper'];

                            if (!isset($categories[$categoryID][$matches[1]])) {
                                $categories[$categoryID][$matches[1]] = helpers::morpher($word, $matches[1]);

                                if (!$categories[$categoryID][$matches[1]]) {
                                    throw new Exception();
                                }
                            }

                            return ($categories[$categoryID][$matches[1]] ?: $word) . ' ' . $tm . $matches[3];
                        },
                        $text
                    );

                    $joinsWhere = 'INNER JOIN ' . $joins[$cat_in_tm['level']] . ' AS pc ON pc.cat_id = ' . $categoryID . ' AND pc.prod_id = p.id ' .
                        'WHERE p.visible = 1 AND p.active = 1 AND p.tm_id = ' . (int)$cat_in_tm['tm_id'];

                    $topProduct = DB::get_row('SELECT * FROM (SELECT p.name, IF(p.special_price > 0, p.special_price, p.price) AS price, (SELECT COUNT(c.id) FROM orders_items AS c WHERE c.product_id = p.id) AS cnt FROM products AS p ' . $joinsWhere . ') AS pp ORDER BY pp.cnt DESC LIMIT 1');

                    $prodsInfo = DB::get_row('SELECT COUNT(p.id) AS cnt, MIN(IF(p.special_price > 0, p.special_price, p.price)) AS min_price, MAX(IF(p.special_price > 0, p.special_price, p.price)) AS max_price ' .
                        'FROM products AS p ' .
                        $joinsWhere
                    );

                    $values = [
                        $categories[$categoryID]['lower'] . ' ' . $tm,
                        $categories[$categoryID]['upper'] . ' ' . $tm,
                        (int)$topProduct['price'],
                        mb_strtolower(mb_substr($topProduct['name'], 0, 1)) . mb_substr($topProduct['name'], 1),
                        mb_strtoupper(mb_substr($topProduct['name'], 0, 1)) . mb_substr($topProduct['name'], 1),
                        (int)$prodsInfo['cnt'],
                        (int)$prodsInfo['min_price'],
                        (int)$prodsInfo['max_price'],
                    ];

                    $text = count_morphology(
                        str_replace(
                            $textValues['keys'],
                            $values,
                            optimization::_randomValues(
                                optimization::_randomValues(
                                    $text,
                                    '\$pay_r',
                                    $textValues['pay']
                                ),
                                '\$delivery_r',
                                $textValues['delivery']
                            )
                        )
                    );

                    ++$processed;

                    DB::query('UPDATE rk_tm_in_cat SET full_desc_synth = "' . DB::mysql_secure_string($text) . '" WHERE tm_id = ' . (int)$cat_in_tm['tm_id'] . ' AND cat_id = ' . (int)$cat_in_tm['cat_id']);
                }
            );
        } catch (Exception $error) {
        }

        return $processed;
    }

    /**
     * Updates description_synth for products
     *
     * @param bool $emptyOnly Update only products with empty description_synth
     */
    public static function product_text($emptyOnly = false)
    {
        $textValues = require SITE_PATH . 'optimization/product.php';

        $variantsN = count($textValues['variants']);
        $deliveriesN = count($textValues['deliveries']);

        DB::get_rows(
            'SELECT p.id, p.name, p.price, p.always_available, p.available,
                t.name AS tm_name
            FROM products AS p
            INNER JOIN rk_prod_desc AS pd ON pd.id = p.id
            INNER JOIN tm AS t ON t.id = pd.tm_id
            WHERE p.main_id = 0' . ($emptyOnly ? ' AND (p.description_synth = "" OR p.description_synth IS NULL)' : ''),
            function ($product) use ($textValues, $variantsN, $deliveriesN) {
                $text = $textValues['variants'][rand(0, $variantsN - 1)];

                shuffle($textValues['deliveries']);

                do {
                    $oldText = $text;

                    $text = preg_replace_callback(
                        '/\{([^\{\}]+)}/',
                        function ($matches) {
                            $values = explode('|', $matches[1]);

                            return $values[rand(0, count($values) - 1)];
                        },
                        $text
                    );
                } while ($text !== $oldText);

                $text = str_replace(
                    $textValues['keys'],
                    [
                        $product['name'],
                        mb_strtoupper(mb_substr($product['name'], 0, 1)) . mb_substr($product['name'], 1),
                        $product['tm_name'],
                        mb_strtoupper(mb_substr($product['tm_name'], 0, 1)) . mb_substr($product['tm_name'], 1),
                        implode(
                            ', ',
                            array_slice($textValues['deliveries'], 0, rand(1, $deliveriesN))
                        ),
                    ],
                    $text
                );

                DB::query('UPDATE products SET description_synth = "' . DB::mysql_secure_string($text) . '" WHERE id = ' . $product['id']);
            }
        );
    }

    /**
     * Replaces pattern with list of possible values (Used in self::category_tm_description())
     *
     * @param string $text Original text
     * @param string $pattern Pattern to replace (Escape for use inside regular expression!)
     * @param array $values Array of possible values (Each item is array of words)
     *
     * @return mixed
     */
    private static function _randomValues($text, $pattern, $values)
    {
        $added = [];

        return preg_replace_callback(
            '/' . $pattern . '/',
            function () use (&$added, $values) {
                $return = [];

                shuffle($values);

                for ($i = 0, $n = rand(2, count($values)); $i < $n; ++$i) {
                    shuffle($values[$i]);

                    do {
                        $word = $values[$i][0];
                    } while (in_array($word, $added));

                    $added[] = $word;
                    $return[] = $word;
                }

                return implode(', ', $return);
            },
            $text
        );
    }

    /**
     * Returns parsed category-trademark description
     *
     * @param mixed $part Part to be parsed
     * @param int $level Recursion level
     *
     * @return string
     */
    private static function _parseCatTmPart($part, $level = 0) {
        switch (gettype($part)) {
            case 'string':
                return $part;

                break;

            case 'array':
                if ($level && !(($level + 1) % 2)) {
                    for ($i = 0, $n = count($part), $return = ''; $i < $n; ++$i) {
                        $return .= self::_parseCatTmPart($part[$i], $level + 1);
                    }

                    return $return;
                } else {
                    return self::_parseCatTmPart($part[rand(0, count($part) - 1)], $level + 1);
                }

                break;
        }
    }

    /**
     * Updates `_full_url` field for categories
     */
    public static function category_full_urls()
    {
        $categories = DB::get_rows('SELECT c.id, c.url AS url1, p1.url AS url2, p2.url AS url3
                                    FROM rk_categories AS c
                                    LEFT JOIN rk_categories AS p1 ON p1.id = c.parent_1
                                    LEFT JOIN rk_categories AS p2 ON p2.id = c.parent_2
                                    ');

        for ($i = 0, $n = count($categories); $i < $n; ++$i) {
            DB::query('UPDATE rk_categories SET _full_url = "' . DB::mysql_secure_string(
                    ($categories[$i]['url3'] ? $categories[$i]['url3'] : '') . ($categories[$i]['url2'] ? ($categories[$i]['url3'] ? '/' :'') . $categories[$i]['url2'] : '') . ($categories[$i]['url2'] ? '/' : '') . $categories[$i]['url1']
                ) . '" WHERE id = ' . (int)$categories[$i]['id']);
        }
    }
}
