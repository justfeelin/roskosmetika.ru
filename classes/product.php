<?php

/**
 * Working with product
 */
Class Product
{
    /**
     * Certificate product ID
     */
    const CERTIFICATE_ID = 99999999;

    /**
     * Returns product cart neighbours
     *
     * @param int|array $ids Product ID or array of product IDs
     * @param bool $shuffle Shuffle result. If false - result will be ordered by cart popularity descending
     * @param int $limit Limit result to certain maximum
     *
     * @return array self::get_by_id() result
     */
    public static function cart_neighbours($ids, $shuffle = true, $limit = null)
    {
        if (!$ids) {
            return [];
        }

        $pIDs = [];

        DB::get_rows(
            'SELECT IF(p.main_id > 0, p.main_id, p.id) AS id FROM products AS p WHERE p.id IN (' . implode(', ', !is_array($ids) ? [$ids] : $ids) . ') AND p.visible = 1',
            function ($product) use (&$pIDs) {
                $pIDs[] = (int)$product['id'];
            }
        );

        if (!$pIDs) {
            return [];
        }

        $sqlIDs = implode(', ', $pIDs);

        $nIDs = [];

        DB::get_rows(
            'SELECT DISTINCT(n.neighbour_id) AS id FROM __cart_neighbours AS n, products AS p
             WHERE n.neighbour_id = p.id
             AND p.visible = 1
             AND n.product_id IN (' . $sqlIDs . ')' .
            ' AND (n.neighbour_id NOT IN (' . $sqlIDs . '))' .
            ' ORDER BY n.cnt DESC ' .
            ($limit ? ' LIMIT ' . $limit : ''),
            function ($neighbour) use (&$nIDs) {
                $nIDs[] = (int)$neighbour['id'];
            }
        );

        return $nIDs ? self::get_by_id($nIDs, true, true, false, false, false, true) : [];
    }

    /**
     * Returns all category IDs linked to product
     *
     * @param int $productID Product ID
     *
     * @return array
     */
    public static function get_categories($productID)
    {
        $productID = (int)$productID;

        return DB::get_rows(
            'SELECT DISTINCT c.cat_id
            FROM (
                SELECT pc.cat_id
                FROM rk_prod_cat AS pc
                WHERE pc.prod_id = ' . $productID . '
                UNION
                SELECT pc2.cat_id
                FROM rk_prod_cat_lvl_2 AS pc2
                WHERE pc2.prod_id = ' . $productID . '
                UNION
                SELECT pc1.cat_id
                FROM rk_prod_cat_lvl_1 AS pc1
                WHERE pc1.prod_id = ' . $productID . '
            ) AS c',
            function ($pair) {
                return (int)$pair['cat_id'];
            }
        );
    }

    /**
     * Функция получения продуктов для экспорта
     * @param  array|false $param Параметры выгрузки продуктов
     * @return array              Возвращает продукты по выборке
     */
    static function get_export_products($param)
    {


        $query = 'SELECT p.id, p.main_id,  p.name, p.pack,  p.short_description, p.description, p.url, p.new, p.visible,
                             pd.eng_name, pd.use, pd.ingredients, pd.synonym, p.tm_id,   
                             ps.title, ps.description seo_desc, ps.keywords, 
                             pp.src, pp.alt, pp.title photo_title,  
                            (SELECT GROUP_CONCAT(pf.filter_id) FROM rk_prod_filter pf WHERE pf.prod_id = p.id)  filters, 
                            (SELECT GROUP_CONCAT(pc.cat_id) FROM rk_prod_cat pc, rk_categories rc WHERE pc.cat_id = rc.id AND rc.is_tag = 0 AND pc.prod_id = p.id) categories, 
                            (SELECT GROUP_CONCAT(pl.line_id) FROM rk_prod_lines pl WHERE pl.prod_id = p.id) `lines`, 
                            (SELECT GROUP_CONCAT(ps.set_id) FROM rk_prod_sets ps WHERE ps.prod_id = p.id) sets 
                      FROM products AS p
                      LEFT JOIN rk_prod_desc pd   ON ( pd.id = p.id ) 
                      LEFT JOIN rk_prod_seo ps    ON ( ps.id = p.id ) 
                      LEFT JOIN rk_prod_photo pp  ON ( pp.id = p.id )';

        //включение в выборку параметров
        if ($param) {
            $query .= ' WHERE ';
            $and = FALSE;
            $or = FALSE;
            $begin_group = FALSE;


            //проверка параметров выборки
            if (isset($param[1])) {
                if (isset($param[1]['id_line'])) {

                    $id_line = $param[1]['id_line'];
                    preg_match_all('/\d{1,4}-\d{1,4}/', $id_line, $res);
                    $interval = FALSE;
                    if (!empty($res)) {
                        foreach ($res[0] as $key => $data) {
                            $len = strlen($data);
                            $separator = strpos($data, '-');
                            $interval[$key]['first'] = substr($data, 0, $separator);
                            $interval[$key]['last'] = substr($data, $separator + 1, $len);
                            $id_line = str_replace($data, '', $id_line);
                        }
                    }

                    $id_line = preg_split('/[^\d]+/', $id_line, -1, PREG_SPLIT_NO_EMPTY);

                    //включение интервалов
                    if ($interval) {
                        $pre_query = '';
                        foreach ($interval as $value) {
                            $pre_query .= ($value['last'] > $value['first'] ? " (p.id >= {$value['first']} AND p.id <= {$value['last']}) " : " (p.id >= {$value['last']} AND p.id <= {$value['first']}) ");
                        }

                        $pre_query = str_replace('  ', ' OR ', trim($pre_query));
                        $query .= '(' . $pre_query;
                        $or = ' OR ';
                        $begin_group = ')';
                    }

                    //включение перечислений
                    if ($id_line) {
                        $pre_query = ' p.id IN (' . implode(",", $id_line) . ')';

                        $pre_query = '(' . $pre_query . ')';

                        if ($or) $pre_query = $or . $pre_query;

                        $or = FALSE;

                        $query .= $pre_query;
                    }

                    $and = ' AND ';
                }
                //добавление торговой марки
                if (isset($param[1]['tm'])) {

                    if ($and) $query .= ' AND ';

                    $and = ' AND ';
                    $query .= " p.tm_id = {$param[1]['tm']} ";

                }
                if ($begin_group) $query = $query . $begin_group;

            }

            $pre_query = '';

            if (isset($param[2])) {

                if (isset($param[2]['eng'])) $pre_query .= " (pd.eng_name = '')  (pd.eng_name IS NULL) ";
                if (isset($param[2]['use'])) $pre_query .= " (pd.use = '')  (pd.use IS NULL) ";
                if (isset($param[2]['made_of'])) $pre_query .= " (pd.ingredients = '')  (pd.ingredients IS NULL) ";
                if (isset($param[2]['synonym'])) $pre_query .= " (pd.synonym = '')  (pd.synonym IS NULL) ";

            }

            if (isset($param[3])) {

                if (isset($param[3]['title'])) $pre_query .= " (ps.title = '')  (ps.title IS NULL) ";
                if (isset($param[3]['seo_desc'])) $pre_query .= " (ps.description = '')  (ps.description IS NULL) ";
                if (isset($param[3]['keywords'])) $pre_query .= " (ps.keywords = '')  (ps.keywords IS NULL) ";

            }

            if (isset($param[4])) {

                if (isset($param[4]['alt'])) $pre_query .= " (pp.alt = '')  (pp.alt IS NULL) ";
                if (isset($param[4]['photo_title'])) $pre_query .= " (pp.title = '')  (pp.title IS NULL) ";

            }

            if (isset($param[5])) {

                if (isset($param[5]['no_cat'])) $pre_query .= " ((SELECT GROUP_CONCAT(pc.cat_id) FROM rk_prod_cat pc WHERE pc.prod_id = p.id) = '')  ((SELECT GROUP_CONCAT(pc.cat_id) FROM rk_prod_cat pc WHERE pc.prod_id = p.id) IS NULL) ";
                if (isset($param[5]['no_filters'])) $pre_query .= " ((SELECT GROUP_CONCAT(pf.filter_id) FROM rk_prod_filter pf WHERE pf.prod_id = p.id) = '')  ((SELECT GROUP_CONCAT(pf.filter_id) FROM rk_prod_filter pf WHERE pf.prod_id = p.id) IS NULL) ";
                if (isset($param[5]['no_lines'])) $pre_query .= " ((SELECT GROUP_CONCAT(pl.line_id) FROM rk_prod_lines pl WHERE pl.prod_id = p.id) = '')  ((SELECT GROUP_CONCAT(pl.line_id) FROM rk_prod_lines pl WHERE pl.prod_id = p.id) IS NULL) ";
                if (isset($param[5]['no_tm'])) $pre_query .= " (p.tm_id = 0) ";
                if (isset($param[5]['news'])) $pre_query .= " (p.new = 1) ";

            }


            if ($pre_query != '') {
                if ($and) $query .= ' AND ';

                $and = ' AND ';
                $pre_query = str_replace('  ', ' OR ', trim($pre_query));
                $pre_query = '(' . $pre_query . ')';
                $query .= $pre_query;
            }


            if (isset($param[6]['no_pack'])) {
                if ($and) $query .= ' AND ';
                $query = $query . ' (main_id = 0) ';

            }

            if (!empty($param['categories'])) {
                $categoriesSQL = implode(', ', $param['categories']);

                if (preg_match('/^[0-9]+(, [0-9]+)*$/', $categoriesSQL)) {
                    $query .= ($pre_query ? ' AND ' : '') . ' p.id IN (SELECT c1.prod_id FROM rk_prod_cat_lvl_1 AS c1 WHERE c1.cat_id IN (' . $categoriesSQL . ') UNION SELECT c2.prod_id FROM rk_prod_cat_lvl_2 AS c2 WHERE c2.cat_id IN (' . $categoriesSQL . ') UNION SELECT c3.prod_id FROM rk_prod_cat AS c3 WHERE c3.cat_id IN (' . $categoriesSQL . '))';
                }
            }
        }

        return DB::get_rows($query);
    }


    /**
     * Return product parameters for layout
     * @param  int|string $item url or id of item
     * @param  boolean $url flag id or url
     * @return array      parameters of product
     */
    static function get_product($item, $url = TRUE)
    {

        $field = ($url ? 'url' : 'id');

        $query = 'SELECT p.id id, p.main_id, p.url, p.name, p.short_description, p.description, p.description_synth, p.price, p.special_price, p.pack, p.new, p.hit, p.available, p.for_proff, p.visible, p.active, p._discount AS discount, p._discount_end AS discount_end, p._max_discount AS max_discount, p._max_discount_days  AS is_sales, ' . self::proff_price('p.tm_id') . ',
                           pd.eng_name, pd.use, pd.ingredients, pd.synonym, p.tm_id,
                           ps.title, ps.description seo_desc, ps.keywords,
                           pp.src, pp.alt, pp.title photo_title, pp.pic_quant, 
                           t.name AS tm, td.alt_name AS tm_alt, c.name AS country, IF( t.url = "", t.id, t.url ) tm_url, IF(t.visible AND t._has_products, 1, 0) AS tm_visible,
                           0 AS day_item,
                           (SELECT pl.line_id FROM rk_prod_lines pl WHERE pl.prod_id = p.id LIMIT 1) `rand_line`,
                           (SELECT MAX(pv._discount_end) FROM products pv WHERE pv.main_id = p.id) AS pack_discount_end,
                           (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.hit = 1 LIMIT 1) AS pack_hit,
                           (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.new = 1 AND pv.visible = 1 AND pv.active = 1 LIMIT 1) pack_new,
                           IF(p.active = 0 AND p.price = 0 AND p.new = 1 AND p.visible = 1, 1, 0) AS soon
                    FROM  products AS p
                    LEFT JOIN tm AS t ON t.id = p.tm_id
                    LEFT JOIN rk_prod_desc pd   ON ( p.id = pd.id )
                    LEFT JOIN rk_prod_seo ps    ON ( ps.id = p.id ) 
                    LEFT JOIN rk_prod_photo pp  ON ( pp.id = p.id )
                    LEFT JOIN rk_tm_desc td     ON ( p.tm_id = td.id )
                    LEFT JOIN cdb_country AS c  ON c.id = t.country_id
                    WHERE  p.' . $field . ' = "' . $item . '"
                    AND    p.main_id = 0
                    AND (
                        p.visible = 1
                        OR IF((SELECT COUNT(pp.id) FROM products AS pp WHERE pp.visible = 1 AND pp.main_id = p.id), 1, 0)
                    )';

        $product = db::get_row($query);

        if (isset($product['id'])) {

            $product['buy_with']['products'] = self::cart_neighbours($product['id'], false, 8);

            //  lines
            if ($product['rand_line']) {
                $query = "SELECT p.id, p.name, p.short_description, IF( p.url =  '', p.id, p.url ) url, p.price, p.new, p.hit, p.available, p.special_price, p.pack, tm.name AS tm, td.alt_name AS tm_alt, IF(tm.url =  '', tm.id, tm.url) AS tm_url, 0 AS day_item, p.for_proff, 
                                 p.for_proff, p._max_discount AS discount, p._max_discount_days  AS is_sales,
                                 pp.src, pp.alt, pp.title photo_title,
                                 (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.hit = 1 LIMIT 1) AS pack_hit,
                                 (SELECT pv.id FROM products pv WHERE pv.main_id = p.id AND pv.new = 1 AND pv.visible = 1 AND pv.active = 1 LIMIT 1) pack_new,
                                 0 AS soon
                          FROM  rk_prod_lines pl, tm, rk_tm_desc AS td, products AS p
                          LEFT JOIN rk_prod_photo pp  ON ( pp.id = p.id )
                          WHERE  pl.line_id = {$product['rand_line']} 
                          AND    pl.prod_id = p.id
                          AND    p.tm_id = tm.id
                          AND    p.visible = 1
                          AND    p.active = 1
                          AND    td.id = tm.id
                          LIMIT 3";

                $product['line']['products'] = DB::get_rows($query);
                foreach ($product['line']['products'] as $key => $line_prod) {
                    //  Процент скидки
                    $product['line']['products'][$key]['discount'] = $line_prod['discount'];

                    $product['line']['products'][$key]['packs'] = self::get_packs($line_prod['id']);
                }

                $product['line']['info'] = DB::get_row("SELECT l.id, l.name, IF( l.url =  '', l.id, l.url ) url,  IF( tm.url =  '', tm.id, tm.url ) tm_url FROM rk_lines AS l, tm WHERE l.id = {$product['rand_line']} AND l.tm_id = tm.id");
            }

            // h1
            $product['h1'] = helpers::mb_ucfirst($product['name']) . ' ' . ($product['tm'] ?: $product['tm_alt']);

            // categories crumbs
            $product['categories']['crumbs'] = DB::get_rows("SELECT c.parent_2 AS id, 
                                                                   CONCAT('/category/', IF( c2.url =  '', c2.id, c2.url )) AS url, 
                                                                   c2.name AS name, 1 AS one_level 
                                                             FROM rk_categories AS c, rk_prod_cat AS pc, rk_categories AS c2
                                                             WHERE c.visible = 1
                                                             AND c.is_tag = 0
                                                             AND pc.prod_id = {$product['id']} 
                                                             AND pc.cat_id = c.id
                                                             AND c2.id = c.parent_2
                                                             AND c2.visible = 1
                                                             GROUP BY c.parent_2
                                                             LIMIT 3");


            unset($product['categories']['crumbs'][0]['one_level']);


            $limit = 6;
            
            // if (($lvl1N = count($product['categories']['crumbs'])) > 1) {
            //     $limit = 5;
            // }

            if (count($product['categories']['crumbs']) > 1) {
                $limit = 5;
            }

            $cats_lvl_2 = DB::get_rows("SELECT c.parent_1
                                        FROM rk_categories AS c, rk_prod_cat AS pc, rk_categories AS c1, rk_categories AS c2
                                        WHERE c.visible = 1
                                        AND c.is_tag = 0
                                        AND pc.prod_id = {$product['id']}
                                        AND pc.cat_id = c.id
                                        AND (SELECT cc.visible FROM rk_categories cc WHERE cc.id = c.parent_1) = 1
                                        AND c1.id = c.parent_1
                                        AND c1.visible = 1
                                        AND c2.id = c1.parent_1
                                        AND c2.visible = 1
                                        GROUP BY c.parent_1
                                        LIMIT  $limit");

            // $lvl1Ids = [];

            // for ($i = 0; $i < $lvl1N; ++$i) {
            //     $product['categories']['lvl_1'][] = DB::get_row("SELECT id, IF( url =  '', id, url ) url, name FROM rk_categories WHERE id = {$cats_lvl_1[$i]['parent_2']} AND visible = 1");

            //     $lvl1Ids[] = (int)$product['categories']['lvl_1'][$i]['id'];

            // }

            $similar = array();

            for ($i = 0, $n = count($cats_lvl_2); $i < $n; ++$i) {
                $lvl_2 = DB::get_row("SELECT id, IF( url =  '', id, url ) url, REPLACE(name, '&nbsp;', ' ') AS name, parent_1 FROM rk_categories WHERE id = {$cats_lvl_2[$i]['parent_1']} AND visible = 1");
  
                $lvl_2['url'] = '/category/' . DB::get_field('url', "SELECT IF( url =  '', id, url ) url FROM rk_categories WHERE id = {$lvl_2['parent_1']}") . '/' . $lvl_2['url'];

                if (mb_strlen($lvl_2['name']) >= 22) {
                    $lvl_2['title'] = $lvl_2['name'];
                    $lvl_2['name'] = mb_substr($lvl_2['name'], 0, 22) . '..';
                }
                if($i > 0) $lvl_2['one_level'] = TRUE;
                // $product['categories']['lvl_2'][] = $lvl_2;
                $product['categories']['crumbs'][] = $lvl_2;

                if(count($similar) < 2) {
                  $similar[] = $lvl_2;
                }

            }


            // -- alternatives by cat lvl 2 + recommendations
            if(!empty($similar))
            {
              $productIDs = DB::get_rows(
                  'SELECT DISTINCT(n.product_id)
                      FROM __cart_neighbours AS n
                      INNER JOIN products AS p ON p.id = n.product_id AND p.available = 1 AND p.visible = 1 AND p.active = 1 AND p.id != ' . $product['id'] . '
                      WHERE n.product_id IN (SELECT pc.prod_id FROM rk_prod_cat_lvl_2 AS pc WHERE pc.cat_id IN (' . implode(', ', array_column($similar, 'id')) . '))
                      ORDER BY n.cnt DESC LIMIT 4',
                  function ($product) {
                      return (int)$product['product_id'];
                  }
              );
            } 

            //  $mb_recomended_1 = mindbox::get_recomendation('PoxozhieTovarySajt', $product['id'], 4);
            $mb_recomended_1 = agregation_products::get_personal_product_list(helpers::search_user_id(), 2, 2);

            //  $mb_recomended_2 = mindbox::get_recomendation('SEtimTovaromPokupayut', $product['id'], 4);
            $mb_recomended_2 = agregation_products::get_personal_product_list(helpers::search_user_id(), 1, 2);

            // $union IDS
            $union_recomended = array_merge($mb_recomended_1, $mb_recomended_2);

            if (isset($productIDs) && $productIDs)
            {
               $union_recomended = array_merge($productIDs, $union_recomended);
            }

            $union_recomended = array_unique($union_recomended);
   
            $product['similar'] = self::get_by_id(
                $union_recomended,
                true,
                true,
                false,
                false,
                false,
                true
            );
            // --

            $product['categories']['crumbs'][] = array('name' => $product['h1']);

            // packs
            $product['packs'] = DB::get_rows('SELECT p.id, p.main_id, p.price, p.special_price, p.pack, p.for_proff, p.available, p.visible, p.active,  p.for_proff, p._discount AS discount, p._discount_end AS discount_end, ' . Product::proff_price((int)$product['tm_id']) . ',
                                                           pp.src, pp.alt, pp.title photo_title, pp.pic_quant
                                                    FROM products p LEFT JOIN rk_prod_photo pp  ON ( pp.id = p.id )
                                                    WHERE p.main_id = ' . $product['id'] . '
                                                    AND   p.visible = 1 AND p.active = 1');

            $product['packs'][] = array('id' => $product['id'],
                'main_id' => $product['main_id'],
                'price' => $product['price'],
                'proff_price' => $product['proff_price'],
                'special_price' => $product['special_price'],
                'pack' => $product['pack'],
                'for_proff' => $product['for_proff'],
                'available' => $product['available'],
                'visible' => $product['visible'],
                'active' => $product['active'],
                'src' => $product['src'],
                'alt' => $product['alt'],
                'photo_title' => $product['photo_title'],
                'discount' => $product['discount'],
                'discount_end' => $product['discount_end'],
                'pic_quant' => $product['pic_quant']);

            if (count($product['packs']) > 1) {

                $pack_array = array();

                foreach ($product['packs'] as $key => $value) {
                    $pack_array[$key] = $value['price'];
                }
                array_multisort($pack_array, SORT_NUMERIC, $product['packs']);
            }

            $packsN = count($product['packs']);

            if ($product['name'] && !$product['title'] || $product['description_synth']) {
                $price = (int)(
                    $product['available'] && $product['visible'] && $product['active'] && (int)$product['special_price']
                        ? $product['special_price']
                        : $product['price']
                );

                if ($packsN > 1) {
                    for ($i = 0; $i < $packsN; ++$i) {
                        $pPrice = (int)(
                            $product['packs'][$i]['available'] && $product['packs'][$i]['visible'] && $product['packs'][$i]['active'] && (int)$product['packs'][$i]['special_price']
                                ? $product['packs'][$i]['special_price']
                                : $product['packs'][$i]['price']
                        );

                        if ($pPrice < $price) {
                            $price = $pPrice;
                        }
                    }
                }
            }

            if ($product['name']) {
                if (!$product['title']) {
                    if ($packsN === 1) {
                        $packs = $product['packs'][0]['pack'];
                    } else {
                        $packs = [];

                        for ($i = 0; $i < $packsN; ++$i) {
                            $packs[] = $product['packs'][$i]['pack'];
                        }

                        $packs = '(' . implode(', ', $packs) . ')';
                    }

                    // SEO update 21.11.18
                    // $product['title'] = $product['name'] . ' ' . ($product['tm_alt'] ?: $product['tm']) . ' - купить в интернет-магазине из Москвы. Цена на ' . helpers::grammatical_case($product['name'], helpers::CASE_TYPE_ACCUSATIVE, true) . ' ' . ($product['tm'] ?: $product['tm_alt']) . ' ' . $packs . ' с доставкой из каталога Роскосметика';
                    $product['title'] = export::anti_typograf($product['h1']) . ' купить в Москве: цена, отзывы, характеристики';
                }

                if (!$product['seo_desc']) {

                    // SEO update 21.11.18
                    // $product['seo_desc'] = 'Выбрать и заказать ' . $product['name'] . ' ' . $product['tm'] . ' в интернет-магазине Роскосметика с доставкой по Москве, Санкт-Петербургу, России! В нашем каталоге огромный ассортимент, скидки и лучшие цены!';
                    $product['seo_desc'] = 'Заказать ' . $product['h1'] . ' по отличным ценам в интернет-магазине Роскосметика. Широкий выбор профессиональной косметики, доставка по Москве и всей России. Акции и скидки.';
                }
            }

            if ($product['description_synth']) {
                $product['description_synth'] = str_replace('$price$', $price, $product['description_synth']);
            }

            $product['base_links'] = link_base::get_links(link_base::PRODUCT_TYPE_ID, $product['id']);

            $product['type_page_tms'] = DB::get_rows(
                'SELECT CONCAT(tp.url, "/", IF(t.url != "", t.url, t.id)) AS url, CONCAT(LOWER(tp.name), " ", t.name) AS name
                FROM rk_type_page AS tp
                INNER JOIN rk_tm_in_type_page AS ttp ON ttp.type_page_id = tp.id AND ttp.tm_id = ' . $product['tm_id'] . ' AND ttp._has_products = 1
                INNER JOIN tm AS t ON t.id = ttp.tm_id
                WHERE ttp.type_page_id IN (
                    SELECT ptp.type_page_id
                    FROM rk_prod_type_page AS ptp
                    WHERE ptp.product_id = ' . $product['id'] . '
                )'
            );

            return $product;
        } else {
            return FALSE;
        }
    }

    /**
     * Check id for redirect on URL
     * @param  int $id product id
     * @return string  url for redirect on
     */
    static function redirect($id)
    {

        return DB::get_field('url', "SELECT url FROM products WHERE id = $id");

    }

    /**
     * Get id by url
     * @param  string $url product url
     * @return int  product id
     */
    static function get_id_by_url($url)
    {

        return DB::get_field('id', "SELECT id FROM products WHERE url = '$url'");

    }

    /**
     * Cheeking item for descr_id
     * @param  int $id item_id
     * @return int     product id
     */
    static function check_descr_id($id)
    {

        $main_id = (int)DB::get_field('main_id', "SELECT main_id FROM products WHERE id = $id");
        if ($main_id) return $main_id;
        else {
            return $id;
        }

    }

    /**
     * Get special packs for product
     * @param  int $id item_id
     * @param bool $includeSelf Include self pack
     * @param int $tmID Trademark ID of product
     *
     * @return array   array of packs
     */
    static function get_packs($id, $includeSelf = true, $tmID = null)
    {
        $id = (int)$id;

        return DB::get_rows('SELECT p.id, p.pack, p.price, p.special_price, p.available, p.for_proff, ' . self::proff_price($tmID ?: '(IF(p.main_id = 0, p.tm_id, (SELECT pm_.tm_id FROM products AS pm_ WHERE pm_.id = p.main_id LIMIT 1)))') . ', pp.pic_quant
            FROM products AS p
            LEFT JOIN rk_prod_photo AS pp ON pp.id = p.id
            WHERE (p.main_id = ' . $id . ($includeSelf ? ' OR p.id = ' . $id : '') . ')
            AND p.visible = 1
            AND p.active = 1
            ORDER BY ' . (
                !Auth::get_user_id()
                    ? 'p.for_proff ASC, '
                    : ''
            ) . 'p.available DESC, IF(p.special_price > 0, p.special_price, p.price) ASC');
    }

    /**
     * Check product for existence
     * @param int $prod_id item_id
     * @param bool $visibleOnly Only visible product
     * @param bool $availableOnly Only available product
     *
     * @return bool  true or false
     */
    static function check_prod_existence($prod_id, $visibleOnly = true, $availableOnly = false)
    {
        return DB::get_field('id', 'SELECT p.id FROM products AS p WHERE p.id = ' . $prod_id . ' AND p.visible = 1' . ($visibleOnly ? ' AND p.active = 1' : '') . ($availableOnly ? ' AND p.available = 1' : ''));
    }



    //functions for import catalog r-cosmetics from admin menu of roskosmetika

    /**
     * Получение  id  всех продуктов
     * @param string $type тип таблицы
     * @return array
     */
    static function get_all($type = 'products')
    {

        $query = "SELECT  id, lastmod
                  FROM  $type";

        return DB::get_rows($query);

    }


    /**
     * Добавляет новую информацию о времени продукта для RSS
     * @param int $id - id продукта.
     * @return boolean
     */
    static function add_date($id)
    {

        $query = "INSERT INTO  product_pub_date (id, lastmod)
                  VALUES ($id , NOW())";
        return DB::query($query);
    }


    /**
     * Удаляет информацию о времени продукта для RSS
     * @param int $id - id продукта.
     * @return boolean
     */
    static function del_date($id)
    {

        $query = "DELETE FROM `product_pub_date` WHERE id = $id";
        return DB::query($query);
    }

    /**
     * Fetches product(s) by ID(s)
     *
     * @param int|array $id ID or array of IDs
     * @param bool $active Fetch only with provided value of `active` field (null to fetch all)
     * @param bool $full Return full products description (Including packs etc.)
     * @param bool $applyFilter Apply filter sorting and conditions
     * @param bool $searchQuery Search query from search page (Order by search relevance by default in this case)
     * @param bool $pagination Apply pagination limits
     * @param bool $sort_by_input sort products by input
     *
     * @return array
     */
    static function get_by_id($id, $active = null, $full = false, $applyFilter = false, $searchQuery = false, $pagination = true, $sort_by_input = FALSE)
    {
        if (!$id) {
            return [];
        }

        $multiple = is_array($id);

        if (!$multiple) {
            $pagination = false;
        }

        $sql = $full ? self::full_info_sql('p') : null;

        $filterWhere = $applyFilter ?  self::filter_condition('p') . self::tm_condition('p') . self::price_condition('p') . self::country_condition('p') : '';

        $filterOrder = $applyFilter ? self::filter_order('p', true, $searchQuery ? [
            'search' => $searchQuery,
        ] : []) : '';
        $order = $filterOrder ?: ($searchQuery ? ($multiple ? ('p.available DESC, ' . (!Auth::get_user_id() ? 'p.for_proff ASC, ' : '') . 'FIELD(p.id, ' . implode(', ', array_reverse($id)) . ') DESC') : '') : '');

        if(!$order && $sort_by_input)
        {
          $order = 'FIELD(p.id, ' . implode(', ', $id) . ')' ;
        } 

        $partialQuery = 'FROM products AS p
            INNER JOIN tm AS t ON t.id = p.tm_id
            LEFT JOIN rk_prod_desc pd   ON ( p.id = pd.id )
            LEFT JOIN rk_prod_photo pp  ON ( pp.id = p.id ) ' .
            ($full ? $sql['joins'] : null) .
            ' WHERE p.id ' . ($multiple ? 'IN (' . implode(', ', $id) . ')' : '= ' . intval($id)) . ' AND p.visible = 1' . ($active != null ? ' AND p.active = ' . ($active ? 1 : 0) : '') . $filterWhere;

        if ($pagination) {
            Pagination::setValues((int)DB::get_field('cnt', 'SELECT COUNT(p.id) AS cnt ' . $partialQuery));
        }

        $rows = DB::get_rows('SELECT p.id, IF(p.main_id = 0, p.name, (SELECT name FROM products WHERE id = p.main_id LIMIT 1)) AS name, p.hit, p.new, IF(p.main_id = 0, p.short_description, (SELECT pp.short_description FROM products AS pp WHERE pp.id = p.main_id)) AS short_description, p.available, p.for_proff, IF(p.main_id = 0, IF(p.url != "", p.url, p.id), (SELECT IF(url != "", url, id) FROM products WHERE id = p.main_id LIMIT 1)) AS url, p.pack, p.price, p.special_price, p.apply_discount,
            pp.src, pp.alt, pp.title photo_title,
            IF(p.main_id = 0, pd.ingredients, (SELECT ppd.ingredients FROM rk_prod_desc AS ppd WHERE ppd.id = p.main_id)) AS ingredients,
            IF(t.url = "", t.id, t.url) AS tm_url,
            IF(p.tm_id > 0 AND p.active = 0 AND p.price = 0 AND p.new = 1 AND p.visible = 1, 1, 0) AS soon ' .
            ($full ? $sql['fields'] : '') . $partialQuery . ($order ? ' ORDER BY ' . $order . ' ' : '') . ($pagination ? Pagination::sqlLimit() : ''));

        if ($full) {
            $rows = filters::set_products_info($rows);
        }

        return $multiple ? $rows : (!empty($rows[0]) ? $rows[0] : null);
    }

    /**
     * Returns select condition according to passed filters
     *
     * @param string $alias Products table alias
     * @param bool $addAnd Add ' AND (`condition`)' to result
     *
     * @return string
     */
    public static function filter_condition($alias = null, $addAnd = true)
    {
        static $return;

        if ($return === null) {
            if (empty($_GET['f']) || !is_array($_GET['f'])) {
                return '';
            }

            $alias = $alias ? $alias . '.' : '';

            $filters = array();

            $n = sizeof($_GET['f']);

            for ($i = 0; $i < $n; ++$i) {
                //iterate through end-point filters
                if (!empty($_GET['f'][$i]) && is_numeric($_GET['f'][$i]) && $_GET['f'][$i] >= 1) {
                    $filter_id = (int)$_GET['f'][$i];

                    $parent_id = DB::get_field('parent_id', 'SELECT parent_id FROM rk_filters WHERE parent_id != 0 AND visible = 1 AND id = ' . $filter_id);

                    if (!$parent_id) {
                        continue;
                    }

                    $filters[$parent_id][] = $filter_id;
                }
            }

            if (!$filters) {
                $return = '';
            } else {

                $queries = array();

                foreach ($filters as $filter) {
                    $queries[] = '(' . $alias . 'id IN (SELECT DISTINCT prod_id FROM rk_prod_filter WHERE filter_id IN (' . implode(', ', $filter) . ')))';
                }

                $return = ($addAnd ? ' AND ' : '') . '(' . implode(' AND ', $queries) . ')';
            }
        }

        return $return;
    }

    /**
     * Returns select condition according to passed price range
     *
     * @param string $alias Products table alias
     *
     * @return string
     */
    public static function price_condition($alias = null)
    {
        static $return;

        if ($return === null) {
            $alias = $alias ? $alias . '.' : '';

            $from = !empty($_GET['pr'][0]) && $_GET['pr'][0] > 0 ? (int)$_GET['pr'][0] : null;
            $to = !empty($_GET['pr'][1]) && $_GET['pr'][1] > 0 ? (int)$_GET['pr'][1] : null;

            if (
                !empty($_GET['pr'])
                && is_array($_GET['pr'])
                && (
                    $from !== null
                    || $to !== null
                )
            ) {
                $field = '(IF (' . $alias . 'special_price > 0, ' . $alias . 'special_price, ' . $alias . 'price))';

                $return = $from !== null && $to !== null
                    ? (
                        $from <= $to
                            ? ' AND ' . $field . ' BETWEEN ' . $from . ' AND ' . $to
                            : ''
                    )
                    : (
                        $from !== null
                            ? ' AND ' . $field . ' >= ' . $from
                            : (
                                $to !== null
                                    ? ' AND ' . $field . ' <= ' . $to
                                    : ''
                            )
                    );
            } else {
                $return = '';
            }
        }

        return $return;
    }

    /**
     * Returns select condition according to passed trademarks IDs
     *
     * @param string $alias Products table alias
     *
     * @return string
     */
    public static function tm_condition($alias = null)
    {
        if (empty($_GET['t']) || !is_array($_GET['t'])) {
            return '';
        }

        $tIDs = [];

        for ($i = 0, $n = count($_GET['t']); $i < $n; ++$i) {
            if (!empty($_GET['t'][$i]) && is_numeric($_GET['t'][$i]) && $_GET['t'][$i] >= 1) {
                $tIDs[] = (int)$_GET['t'][$i];
            }
        }

        if (!$tIDs) {
            return '';
        }

        return ' AND ' . ($alias ? $alias . '.' : '') . 'id IN (
            SELECT pd.id
            FROM products AS pd
            INNER JOIN tm ON tm.id = pd.tm_id AND tm.visible = 1
            WHERE pd.tm_id IN (' . implode(', ', $tIDs) .'))';
    }

    /**
     * Returns select condition according to passed country IDs
     *
     * @param string $alias Products table alias
     *
     * @return string
     */
    public static function country_condition($alias = null)
    {
        if (empty($_GET['cid']) || !is_array($_GET['cid'])) {
            return '';
        }

        $countryIDs = [];

        for ($i = 0, $n = count($_GET['cid']); $i < $n; ++$i) {
            if (!empty($_GET['cid'][$i]) && is_numeric($_GET['cid'][$i]) && $_GET['cid'][$i] >= 1) {
                $countryIDs[] = (int)$_GET['cid'][$i];
            }
        }

        if (!$countryIDs) {
            return '';
        }

        return ' AND ' . ($alias ? $alias . '.' : '') . 'id IN (
            SELECT pd.id
            FROM products AS pd
            INNER JOIN tm AS t ON t.id = pd.tm_id AND t.visible = 1 AND t.country_id IN (' . implode(', ', $countryIDs) .')
        )';
    }

    /**
     * Returns select condition according to passed categories IDs
     *
     * @param string $alias Products table alias
     *
     * @return string
     */
    public static function category_condition($alias = null)
    {
        if (empty($_GET['c']) || !is_array($_GET['c'])) {
            return '';
        }

        $cIDs = [];

        for ($i = 0, $n = count($_GET['c']); $i < $n; ++$i) {
            if (!empty($_GET['c'][$i]) && is_numeric($_GET['c'][$i]) && $_GET['c'][$i] >= 1) {
                $cIDs[] = (int)$_GET['c'][$i];
            }
        }

        if (!$cIDs) {
            return '';
        }

        return ' AND ' . ($alias ? $alias . '.' : '') . 'id IN (
            SELECT pc.prod_id
            FROM rk_prod_cat_lvl_1 AS pc
            INNER JOIN rk_categories AS c ON c.id = pc.cat_id AND c.visible = 1
            WHERE pc.cat_id IN (' . implode(', ', $cIDs) .'))';
    }

    /**
     * Returns select order
     *
     * @param string $alias Products table alias
     * @param bool $strict Return sort order only if proper GET parameter passed
     * @param array $popularOptions Popularity sort options. Supported keys: categoryID, tm, new, sales, search
     *
     * @return string
     */
    public static function filter_order($alias = null, $strict = false, $popularOptions = [])
    {
        static $return;

        if ($return === null) {
            $getSort =
                !empty($_GET['s'])
                && in_array(
                    $_GET['s'],
                    [
                        'price',
                        'discount',
                        'name',
                        'popular',
                    ]
                )
                    ? $_GET['s']
                    : null;

            $alias = $alias ? $alias . '.' : '';

            $return = ($getSort !== 'name' ? $alias . 'active DESC, ' . $alias . 'visible ASC, IF(' . $alias . 'price != 0, 0, 1) ASC' : '');

            if (!($strict && !$getSort)) {
                $proffSort = self::proff_price($alias . 'tm_id', null);

                $return .= ($return ? ', ' : '') . ($proffSort ? $proffSort . ' ASC, ' : '') .
                    $alias . 'available DESC';

                $sortDir = isset($_GET['sort-desc']) ? 'DESC' : 'ASC';

                $sortField = $getSort ?: ($popularOptions ? 'popular' : 'name');

                if ($sortField === 'popular') {

                    // *** Retail Rocket Code
                    // 
                    // $url = $useCache = $clientCache = null; 
                    // switch (true) {
                    //     case isset($popularOptions['new']) && !empty($_COOKIE['rcuid']):
                    //         $url = 'https://api.retailrocket.ru/api/2.0/recommendation/personalized/latest/55cdc1e268e9a65c24215c3d?session=' . $_COOKIE['rcuid'];

                    //         $apiV = 2;

                    //         $clientCache = 'n';

                    //         break;

                    //     case isset($popularOptions['sales']):
                    //         $url = 'https://api.retailrocket.ru/api/2.0/recommendation/saleByPopular/55cdc1e268e9a65c24215c3d?categoryIds=0';

                    //         $apiV = 2;

                    //         $useCache = 'sales';

                    //         break;

                    //     case !empty($popularOptions['search']):
                    //         $url = 'https://api.retailrocket.ru/api/2.0/recommendation/search/55cdc1e268e9a65c24215c3d?phrase=' . rawurlencode($popularOptions['search']);

                    //         $apiV = 2;

                    //         $useCache = crc32('search:' . $popularOptions['search']);

                    //         break;

                    //     case !empty($popularOptions['tm']):
                    //         $url = 'https://api.retailrocket.ru/api/2.0/recommendation/popular/55cdc1e268e9a65c24215c3d/?vendor=' . rawurlencode($popularOptions['tm']) . '&categoryIds=' . (!empty($popularOptions['categoryID']) ? $popularOptions['categoryID'] : '0');

                    //         $apiV = 2;

                    //         $useCache = crc32('tm' . $popularOptions['tm'] . '-cat' . (!empty($popularOptions['categoryID']) ? $popularOptions['categoryID'] : '0'));

                    //         break;

                        
                        //         
                        // case !empty($popularOptions['categoryID']) && !empty($_COOKIE['rcuid']):
                        //     $url = 'https://api.retailrocket.ru/api/2.0/recommendation/popular/55cdc1e268e9a65c24215c3d?categoryIds=' . $popularOptions['categoryID'] . '&session=' . $_COOKIE['rcuid'] . '&features=Popular->ItemsInCategory/PropertyInterests';

                        //     $apiV = 2;

                        //     $clientCache = 'c.' . $popularOptions['categoryID'];

                        //     break;

 
                    // }

                    // if ($url !== null) {
                    //     $generate = function () use ($url, $apiV, $clientCache) {
                    //         if (
                    //             $clientCache
                    //             && isset($_SESSION['RR'][$clientCache][1])
                    //             && $_SESSION['RR'][$clientCache][1] > $_SERVER['REQUEST_TIME']
                    //         ) {
                    //             $pIDs = $_SESSION['RR'][$clientCache][0];
                    //         } else {
                    //             $pIDs = helpers::get_rr_product_ids($url, $apiV);

                    //             if ($pIDs) {
                    //                 $pIDs = array_reverse($pIDs);

                    //                 if ($clientCache) {
                    //                     $_SESSION['RR'][$clientCache][0] = $pIDs;
                    //                     $_SESSION['RR'][$clientCache][1] = time() + 30;
                    //                 }
                    //             } else {
                    //                 if ($clientCache && isset($_SESSION['RR'][$clientCache][0])) {
                    //                     $pIDs = $_SESSION['RR'][$clientCache][0];
                    //                     $_SESSION['RR'][$clientCache][1] = time() + 10;
                    //                 }
                    //             }
                    //         }

                    //         return $pIDs ? implode(',', $pIDs) : null;
                    //     };

                    //     $productsSQL = $useCache ? cache_item('rr-sort-' . $useCache, $generate, 1800) /* 30 minutes */ : $generate();

                    //     if (!$productsSQL) {
                    //         $sortField = 'name';
                    //     } else {
                    //         $sortField = 'FIELD(' . $alias . 'id, ' . $productsSQL . ')';

                    //         $sortDir = $sortDir === 'ASC' ? 'DESC' : 'ASC';
                    //     }
                    // } else {
                    //     $sortField = 'name';
                    // }
                    // 
                    // 
            
                // *** Our popular code  
                
                $sort_type = $useCache = $clientCache = null; 
                    switch (true) {
                        case isset($popularOptions['new']):
                            $sort_type = array('type' => 'new', 'id' => 0);
                            $clientCache = 'n';

                            break;

                        case isset($popularOptions['sales']):
                            $sort_type = array('type' => 'sales', 'id' => 0);
                            $useCache = 'sales';

                            break;

                        case !empty($popularOptions['search']):
                            $sort_type = array('type' => 'search', 'id' => $popularOptions['search']);
                            $useCache = crc32('search:' . $popularOptions['search']);

                            break;

                        case !empty($popularOptions['tm']):
                            $sort_type = array('type' => 'tm', 'id' => $popularOptions['tm']);
                            $useCache = crc32('tm' . $popularOptions['tm'] . '-cat' . (!empty($popularOptions['categoryID']) ? $popularOptions['categoryID'] : '0'));
                            break;                 
                                
                        case !empty($popularOptions['categoryID']):
                            $sort_type = array('type' => 'categories', 'id' => $popularOptions['categoryID']);
                            $clientCache = 'c.' . $popularOptions['categoryID'];

                            break;
 
                    }
        
                    if ($sort_type !== null) {
                        $generate = function () use ($sort_type, $clientCache) {
                            if (
                                $clientCache
                                && isset($_SESSION['RR'][$clientCache][1])
                                && $_SESSION['RR'][$clientCache][1] > $_SERVER['REQUEST_TIME']
                            ) {
                                $pIDs = $_SESSION['RR'][$clientCache][0];
                            } else {

                                $pIDs = bestsellers::get_list($sort_type);

                                if ($pIDs) {
                                    $pIDs = array_reverse($pIDs);

                                    if ($clientCache) {
                                        $_SESSION['RR'][$clientCache][0] = $pIDs;
                                        $_SESSION['RR'][$clientCache][1] = time() + 30;
                                    }
                                } else {
                                    if ($clientCache && isset($_SESSION['RR'][$clientCache][0])) {
                                        $pIDs = $_SESSION['RR'][$clientCache][0];
                                        $_SESSION['RR'][$clientCache][1] = time() + 10;
                                    }
                                }
                            }

                            return $pIDs ? implode(',', $pIDs) : null;
                        };

                        $productsSQL = $useCache ? cache_item('rr-sort-' . $useCache, $generate, 1800) /* 30 minutes */ : $generate();

                        if (!$productsSQL) {
                            $sortField = 'name';
                        } else {
                            $sortField = 'FIELD(' . $alias . 'id, ' . $productsSQL . ')';

                            $sortDir = $sortDir === 'ASC' ? 'DESC' : 'ASC';
                        }
                    } else {
                        $sortField = 'name';
                    }


                } else {
                    $sortField = $sortField === 'price'
                        ? 'IF(' . $alias . 'special_price > 0, ' . $alias . 'special_price, IF(' . $alias . 'price > 0, ' . $alias . 'price, ' . ($sortDir === 'ASC' ? 99999999 : 0) . '))'
                        : (
                            $sortField === 'discount'
                                ? 'GREATEST(' . $alias . '_discount, (IF(' . $alias . 'main_id = 0, IFNULL((SELECT MAX(__pp._discount) FROM products AS __pp WHERE __pp.main_id = ' . $alias . 'id AND __pp.active = 1 AND __pp.visible = 1), 0), 0)))'
                            : $alias . $sortField
                        );
                }

                $return .= ', ' . $sortField . ' ' . $sortDir;
            }
        }

        return $return;
    }

    /**
     * Returns popular products
     *
     * @param bool $full Fetch full product info (Including trademark, packs etc.)
     * @param bool $count Return rows max count
     *
     * @return array
     */
    public static function get_popular($full, $count)
    {
        $ids = cached::popular_product_ids();

        $products = cache_item('popular_products_' . ($full ? 1 : 0), function () use ($full, $ids) {
            $sql = $full ? self::full_info_sql('p') : null;

            $products = DB::get_rows(
                'SELECT p.id, p.tm_id, p.available, p.new, p.hit, p.name, p.price, p.special_price, p.pack, p.short_description, p.for_proff, p._discount,
                    0 AS soon,
                    pp.alt, pp.src,
                    IF(p.url = "", p.id, p.url ) AS url ' .
                    ($full ? $sql['fields'] : '') . '
                FROM products AS p
                LEFT JOIN rk_prod_photo pp ON pp.id = p.id ' .
                ($full ? $sql['joins'] . ' ' : '') . '
                WHERE p.id IN (' . implode(', ', $ids) . ')
                ORDER BY FIELD (p.id, ' . implode(', ', array_reverse($ids)) . ') DESC'
            );

            if ($full) {
                $products = filters::set_products_info($products);
            }

            return $products;
        }, 10800);// 3 hours

        return array_slice($products, 0, $count);
    }

    /**
     * Returns shuffled popular products
     *
     * @param int $count Products count to return
     * @param string $key Page key (To store different sets of products on different type of pages)
     * @param int $expireTime Time in seconds to hold cached result
     *
     * @return array
     */
    public static function get_popular_pseudo_random($count, $key = '', $expireTime = 10800)
    {
        return array_slice(
            cache_item(
                'popular_products_random_' . $key,
                function () {
                    $products = Product::get_popular(true, 100);

                    shuffle($products);

                    return $products;
                },
                $expireTime
            ),
            0,
            $count
        );
    }

    /**
     * Returns SQL statements to be used for full product info fetching (Including trademark, packs etc.)
     *
     * @param string $alias Product table alias
     *
     * @return array [fields => `comma separated select fields statement. Starting from comma!`, joins => `JOIN statements`]
     */
    public static function full_info_sql($alias = null)
    {
        $a = $alias ? $alias . '.' : '';

        return array(
            'fields' => ',__t.name AS tm, IF(__t.url =  "", __t.id, __t.url ) AS tm_url,
          (SELECT pv.id FROM products AS pv WHERE pv.main_id = ' . $a . 'id AND pv.new = 1 AND pv.visible = 1 AND pv.active = 1 LIMIT 1) AS pack_new,
          (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.hit = 1 LIMIT 1) AS pack_hit,
          ' . $a . '_max_discount AS discount, _max_discount_days  AS is_sales,
          0 AS day_item ',
            'joins' => '
                LEFT JOIN tm AS __t           ON __t.id = ' . $a . 'tm_id',
        );
    }

    /**
     * Returns query for retrieving whether product price should be hided
     *
     * @param int|string $tmID original product's trademark ID. Or SQL query returning it
     * @param string $as Add ' AS $as'
     * 
     * @return string
     */
    public static function proff_price($tmID = null, $as = 'proff_price')
    {
        $clientTypeID = Auth::organization_type();

        $userProff = $clientTypeID && $clientTypeID !== Users::CLIENT_TYPE_INDIVIDUAL;

        return (
            $userProff
                ? '0'
                : (
                    !Auth::$user_id
                        ? 'for_proff'
                        : (
                            'IF(for_proff = 1 AND ' .
                            (
                                is_int($tmID)
                                    ? (
                                        $tmID === 40 ? '1' : '0'
                                    )
                                    : (
                                        'IF((' . $tmID . ') = 40, 1, 0)'
                                    )
                            ) .
                            ', 1, 0)'
                        )
                )
        ) .
        ($as ? ' AS ' . $as : '');
    }

    /**
     * Returns found products count
     *
     * @param int $count Count of found products
     *
     * @return string
     */
    public static function count_string($count)
    {
        return $count !== 0 ?count_morphology($count . ' товаров|товар|товара') : 'Ничего не найдено';
    }

    /**
     * Returns products count
     *
     * @param int $categoryID Category ID
     * @param int $categoryLevel Category level
     * @param int $tmID Trademark ID
     *
     * @return int
     */
    public static function products_count($categoryID, $categoryLevel, $tmID = null)
    {
        $joins = $categoryLevel ? Categories::product_joins() : null;

        $innerJoins = [];

        if ($categoryLevel) {
            $innerJoins[] = 'INNER JOIN ' . $joins[$categoryLevel] . ' AS pc ON pc.cat_id = ' . $categoryID . ' AND pc.prod_id = p.id';
        }

        return (int)DB::get_field('cnt', 'SELECT COUNT(p.id) AS cnt FROM products AS p ' .
            implode(' ', $innerJoins) .
            ' WHERE p.visible = 1 AND p.active = 1' .
            ($tmID ? ' AND p.tm_id = ' . $tmID : '')
        );
    }

    /**
     * Returns product image URL with added part (E.g. 1.jpg => 1-product-url.jpg)
     *
     * @param string $src Original image URL
     * @param string $add Part to add
     *
     * @return string
     */
    public static function img_url($src, $add)
    {
        return $add && $src !== 'none.jpg' && preg_match('/^(.+)\.([a-zA-Z]+)$/', $src, $m) ? $m[1] . '-' . $add . '.' . $m[2] : $src;
    }

    /**
     * Returns products list
     *
     * @param string $search Search query (Name, url)
     * @param int|bool $onPage On page limit. If true returns categories count
     * @param int $offset Pagination offset
     * @param bool $byIDOnly Search by ID only (If $search is numeric)
     *
     * @return array
     */
    static function search($search = null, $onPage = null, $offset = null, $byIDOnly = false)
    {
        $numeric = is_numeric($search);

        $query = 'SELECT ' . ($onPage === true ? 'COUNT(p.id) AS cnt' : 'p.id, p.name, p.description AS page_description, p.short_description, IF(p.url != "", p.url, p.id) AS url, ps.title, ps.description, ps.keywords') . '
                  FROM products AS p
                  INNER JOIN rk_prod_seo AS ps ON ps.id = p.id
                  WHERE p.main_id = 0 ' .
            (
                $search
                    ? (
                        ' AND (' . ($numeric ? 'p.id = ' . ((int)$search) : '') .
                        (
                            $numeric && $byIDOnly
                                ? ''
                                : (
                                    ($numeric ? ' OR ' : '') .
                                    'p.name LIKE "%' . ($search = DB::escape($search)) . '%" OR p.url LIKE "%' . $search . '%"'
                                )
                        ) .
                        ')'
                    )
                    : ''
            ) .
            (
                $onPage !== null
                    ? ' LIMIT ' . (($offset !== 0 ? ((int)$offset) . ', ' : '') .  (int)$onPage)
                    : ''
            );

        return $onPage === true ? DB::get_field('cnt', $query) : DB::get_rows($query);
    }

    /**
     * Updates category title, description
     *
     * @param int $productID Product ID
     * @param string $title Title value
     * @param string $description Description value
     * @param string $keywords Keywords value
     * @param string $pageDescription On page description text
     * @param string $shortDescription Short description
     *
     * @return bool
     */
    public static function update_seo($productID, $title, $description, $keywords, $pageDescription, $shortDescription)
    {
        return !!DB::query('UPDATE rk_prod_seo SET title = "' . DB::escape($title) . '", description = "' . DB::escape($description) . '", keywords = "' . DB::escape($keywords) . '" WHERE id = ' . (int)$productID) && !!DB::query('UPDATE products SET description = "' . DB::escape($pageDescription) . '", short_description = "' . DB::escape($shortDescription) . '" WHERE id = ' . (int)$productID);
    }

    /**
     * Returns disabled main products which contain child products
     *
     * @param bool $inVisible Include invisible products
     * @param bool $notActive Include not active products
     *
     * @return array
     */
    public static function get_main_disabled($inVisible, $notActive)
    {
        $conditions = [];

        if ($inVisible) {
            $conditions[] = 'p.visible = 0';
        }

        if ($notActive) {
            $conditions[] = 'p.active = 0';
        }

        return DB::get_rows('SELECT p.id, p.name, p.url, p.visible, p.active FROM products AS p WHERE p.main_id = 0 AND p.id IN (SELECT pp.main_id FROM products AS pp WHERE pp.visible = 1 AND pp.active = 1)' . ($conditions ? ' AND (' . implode(' OR ', $conditions) . ')' : ''));
    }

    /**
     * Returns whether any of given products belongs to parfumery
     *
     * @param array $productIDs Product IDs
     *
     * @return bool
     */
    public static function has_parfumery_products($productIDs)
    {
        if (!$productIDs) {
            return false;
        }

        $joins = Categories::product_joins();

        return (int)DB::get_field('cnt', 'SELECT COUNT(pc.prod_id) AS cnt FROM ' . $joins[1] . ' AS pc WHERE pc.cat_id = ' . Categories::PARFUMERY_ID .' AND pc.prod_id IN (' . implode(', ', $productIDs) . ')') > 0;
    }

    /**
     * Returns product IDs to be ignored for statistics (Present, consultation etc.)
     *
     * @return array
     */
    public static function ignore_ids()
    {
        return [
            Orders::PRESENT_PRODUCT_ID,
            Orders::HELP_PRODUCT_ID,
            Orders::CONSULTATION_PRODUCT_ID,
            Orders::CONSULTANT_HELP_PRODUCT_ID,
            Product::CERTIFICATE_ID,
        ];
    }

    /**
     * Returns products for popular products block with 50/50 division (50 for current brand, 50 for parent category)
     *
     * @param int $count Count of products to return
     * @param int $tmID Current trademark ID
     * @param int $categoryID Current category ID
     * @param int $typePageID Current type page ID
     *
     * @return array
     */
    public static function popular_products_block($count, $tmID, $categoryID = null, $typePageID = null)
    {
        $ids = cache_item(
            'popular_' . (
                $typePageID
                    ? 'typepage' . ($tmID || $categoryID ? '-' : '')
                    : ''
            ) . (
                $categoryID
                    ? ('category' . ($tmID ? '-tm' : ''))
                    : (
                        $tmID
                            ? 'tm'
                            : ''
                    )
            ) . '_' . (
                $typePageID
                    ? $typePageID . ($tmID || $categoryID ? '-' . ($tmID ?: $categoryID) : '')
                    : ''
            ) . (
                $categoryID
                    ? ($categoryID . ($tmID ? '-' . $tmID : ''))
                    : (
                        $tmID ?: ''
                    )
            ),
            function () use ($count, $tmID, $categoryID, $typePageID) {
                $productIDs = $parentProductIDs = [];

                $ignoreProductsSQL = implode(', ', Product::ignore_ids());

                $noParent = false;

                if ($tmID) {
                    DB::get_rows(
                        'SELECT p.id
                        FROM (
                            SELECT p.id, (SELECT COUNT(oi.quantity) FROM rk_orders_items AS oi WHERE oi.product_id = p.id) AS cnt
                            FROM products AS p
                            WHERE p.main_id = 0 AND p.available = 1 AND p.visible = 1 AND p.active = 1 AND p.tm_id = ' . $tmID . '
                        ) AS p
                        ORDER BY p.cnt DESC
                        LIMIT ' . $count,
                        function ($product) use (&$productIDs) {
                            $productIDs[] = (int)$product['id'];

                            return null;
                        }
                    );
                } else {
                    $productIDs = Cached::popular_product_ids();

                    shuffle($productIDs);

                    $productIDs = array_slice($productIDs, 0, $count);
                }

                if ($categoryID || $typePageID) {
                    if ($categoryID) {
                        $category = Categories::get_by_id((int)$categoryID);

                        if ($category['level'] != 1 || $tmID) {
                            $joins = Categories::product_joins();

                            DB::get_rows(
                                'SELECT p.id
                                FROM (
                                    SELECT p.id, (SELECT COUNT(oi.quantity) FROM rk_orders_items AS oi WHERE oi.product_id = p.id) AS cnt
                                    FROM products AS p
                                    INNER JOIN ' . $joins[$category['level'] - ($tmID ? 0 : 1)] . ' AS pc ON pc.prod_id = p.id AND pc.cat_id = ' . ((int)($tmID ? $category['id'] : $category['parent_1'])) .
                                    ($typePageID ? 'INNER JOIN rk_prod_type_page AS ptp ON ptp.product_id = p.id AND ptp.type_page_id = ' . $typePageID : '') . '
                                    WHERE p.main_id = 0 AND p.available = 1 AND p.visible = 1 AND p.active = 1 AND p.id NOT IN (' . $ignoreProductsSQL . ') ' .
                                        ($productIDs ? ' AND p.id NOT IN (' . implode(', ', $productIDs) . ')' : '') . ' ' .
                                    (
                                        $category['level'] == 3 || ($tmID && $category['level'] != 1)
                                            ? 'UNION

                                                SELECT p.id, (SELECT COUNT(oi.quantity) FROM rk_orders_items AS oi WHERE oi.product_id = p.id) AS cnt
                                                FROM products AS p
                                                INNER JOIN ' . $joins[$tmID ? $category['level'] - 1 : 1] . ' AS pc ON pc.prod_id = p.id AND pc.cat_id = ' . ((int)$category[$tmID ? 'parent_1' : 'parent_2']) .
                                                ($typePageID ? 'INNER JOIN rk_prod_type_page AS ptp ON ptp.product_id = p.id AND ptp.type_page_id = ' . $typePageID : '') . '
                                                WHERE p.main_id = 0 AND p.available = 1 AND p.visible = 1 AND p.active = 1 AND p.id NOT IN (' . $ignoreProductsSQL . ') ' .
                                                    ($productIDs ? ' AND p.id NOT IN (' . implode(', ', $productIDs) . ')' : '')
                                            : ''
                                    ) . '
                                ) AS p
                                ORDER BY p.cnt DESC
                                LIMIT ' . $count,
                                function ($product) use (&$parentProductIDs) {
                                    $parentProductIDs[] = (int)$product['id'];

                                    return null;
                                }
                            );
                        } else {
                            if (!$tmID && !$typePageID) {
                                $noParent = true;
                            }
                        }
                    }

                    if ($typePageID) {
                        if ($tmID || isset($category) && $category['level'] == 1) {
                            DB::get_rows(
                                'SELECT p.id
                                FROM (
                                    SELECT p.id, (SELECT COUNT(oi.quantity) FROM rk_orders_items AS oi WHERE oi.product_id = p.id) AS cnt
                                    FROM products AS p
                                    INNER JOIN rk_prod_type_page AS ptp ON ptp.product_id = p.id AND ptp.type_page_id = ' . $typePageID . '
                                    WHERE p.main_id = 0 AND p.available = 1 AND p.visible = 1 AND p.active = 1 AND p.id NOT IN (' . $ignoreProductsSQL . ')
                                        ' . ($productIDs ? 'AND p.id NOT IN (' . implode(', ', $productIDs) . ')' : '') . '
                                ) AS p
                                ORDER BY p.cnt DESC
                                LIMIT ' . $count,
                                function ($product) use (&$parentProductIDs) {
                                    $parentProductIDs[] = (int)$product['id'];

                                    return null;
                                }
                            );
                        } else {
                            $noParent = true;
                        }
                    }
                } else {
                    $noParent = true;
                }

                if ($noParent) {
                    $parentProductIDs = Cached::popular_product_ids();

                    if ($productIDs) {
                        $parentProductIDs = array_diff($parentProductIDs, $productIDs);
                    }
                }

                if ($parentProductIDs) {
                    shuffle($parentProductIDs);
                }

                $n = count($productIDs);

                if ($n > 0) {
                    $middle = min($n, (int)($count / 2));

                    $m = count($parentProductIDs);

                    if ($count - $middle > $m) {
                        $middle += ($middle - $m);
                    }

                    $productIDs = array_merge(
                        array_slice($productIDs, 0, $middle),
                        array_slice($parentProductIDs, $middle, $count - $middle)
                    );

                    shuffle($productIDs);
                } else {
                    $productIDs = array_slice($parentProductIDs, 0, $count);
                }

                $n = count($productIDs);

                if ($n < $count) {
                    $popularIDs = Cached::popular_product_ids();

                    $popularIDs = array_diff($popularIDs, $productIDs);

                    shuffle($popularIDs);

                    $productIDs = array_merge($productIDs, array_slice($popularIDs, 0, $count - $n));
                }

                return $productIDs;
            },
            0
        );

        return Product::get_by_id($ids, null, true, false, false, false);
    }
}
