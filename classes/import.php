<?php

/**
 * Класс для работы с импортом
 */
class import
{

    /**
     * Функция проверки  загужаемого файла
     * @param  string $file Имя формы загрузки файла по названию функции
     * @param  string $type Тип MIME excel или word
     * @return srting       Возвращает текст ошибки или 1
     */
    static function check_MIME($file, $type)
    {

        //  Проверяем есть ли файл вообще
        if ($_FILES["file_$file"]['error'] == UPLOAD_ERR_NO_FILE) {
            return 'Не выбран файл.';
        } //  Превышен ли максимально допустимый размер
        elseif ($_FILES["file_$file"]['error'] == UPLOAD_ERR_INI_SIZE) {
            return 'Превышен максимально допустимый размер.';
        } //  Проверяем тип файла
        elseif ((strpos($_FILES["file_$file"]['type'], $type) === FALSE) && ($_FILES["file_$file"]['type'] != 'application/octet-stream')) {

            return 'Неверный тип файла.';

        } //  Ошибок нет - возвращаем единицу
        else {

            return 1;
        }


    }


    /**
     * Проверка необходимых полей
     * @param  array $excel Массив столбцов и строк из файла
     * @param  array $fields Массив необходимых строк
     * @param  int $list Номер листа
     * @param  int $num номер строки-шапки данных
     * @return array||bool         Возвращает массив полей с индексом или FALSE - если полей в файле нет.
     */
    static function check_fields($excel, $fields, $list = 0, $num = 1)
    {

        $field = array();


        $excel->setActiveSheetIndex($list);
        $main = $excel->getActiveSheet();
        $max_column = PHPExcel_Cell::columnIndexFromString($main->getHighestColumn());

        for ($i = 0; $i < $max_column; $i++) {

            if ($main->getCellByColumnAndRow($i, 1)->getValue()) {

                foreach ($fields as $value) {

                    if (mb_strtolower($main->getCellByColumnAndRow($i, 1)->getValue()) == $value) {
                        $field["$value"] = $i;
                    }

                }

            }
        }

        //  Проверяем соответствие количества найденых полей в таблице и количества необходимых полей
        if (count($field) != count($fields)) {
            return FALSE;
        } else {
            return $field;
        }
    }


    /**
     * Функция определения опций импорта
     * @param  array $option_list Масив данных листа опций
     * @param  int $list Номер листа
     * @return array              Массив опций после обработки
     */
    static function check_options($excel, $list)
    {

        $return = array('update_categories' => TRUE,
            'update_desc' => TRUE,
            'update_seo' => TRUE,
            'update_catalog' => TRUE,
            'update_photos' => TRUE,
            'update_lines' => TRUE,
            'update_tm' => TRUE,
            'update_filters' => TRUE,
            'update_packs' => TRUE);


        $excel->setActiveSheetIndex($list);
        $main = $excel->getActiveSheet();
        $max_row = $main->getHighestRow();

        for ($i = 2; $i < $max_row; $i++) {

            switch (mb_strtolower($main->getCellByColumnAndRow(0, $i)->getValue(), 'utf8')) {
                case 'updatecategories' :
                    if (mb_strtolower($main->getCellByColumnAndRow(1, $i)->getValue(), 'utf8') == 0)
                        $return['update_categories'] = FALSE;
                    break;
                case 'updatedescriptions' :
                    if (mb_strtolower($main->getCellByColumnAndRow(1, $i)->getValue(), 'utf8') == 0)
                        $return['update_desc'] = FALSE;
                    break;
                case 'updateseo' :
                    if (mb_strtolower($main->getCellByColumnAndRow(1, $i)->getValue(), 'utf8') == 0)
                        $return['update_seo'] = FALSE;
                    break;
                case 'updatecatalog' :
                    if (mb_strtolower($main->getCellByColumnAndRow(1, $i)->getValue(), 'utf8') == 0)
                        $return['update_catalog'] = FALSE;
                    break;
                case 'updatefotos' :
                    if (mb_strtolower($main->getCellByColumnAndRow(1, $i)->getValue(), 'utf8') == 0)
                        $return['update_photos'] = FALSE;
                    break;
                case 'updatelines' :
                    if (mb_strtolower($main->getCellByColumnAndRow(1, $i)->getValue(), 'utf8') == 0)
                        $return['update_lines'] = FALSE;
                    break;
                case 'updatetm' :
                    if (mb_strtolower($main->getCellByColumnAndRow(1, $i)->getValue(), 'utf8') == 0)
                        $return['update_tm'] = FALSE;
                    break;
                case 'updatefilters' :
                    if (mb_strtolower($main->getCellByColumnAndRow(1, $i)->getValue(), 'utf8') == 0)
                        $return['update_filters'] = FALSE;
                    break;
                case 'updatepacks' :
                    if (mb_strtolower($main->getCellByColumnAndRow(1, $i)->getValue(), 'utf8') == 0)
                        $return['update_packs'] = FALSE;
                    break;
            }

        }
        return $return;
    }


    /**
     * Функция конвертации markdown в html
     * @param  string $text Текст для конвертации
     * @return srting       Возвращаемый  конвертированный текст
     */
    static function markdown_to_html($text)
    {

        //подключаем библиотеку, конвертируем в html   
        $text = str_replace('\n', PHP_EOL, $text);
        $text = str_replace("_x000D_", '', $text);

        require_once SITE_PATH . 'includes/markdown.php';

        $parser = new Markdown;
        $parser->fn_id_prefix = "post22-";
        return $parser->transform($text);

    }


    /**
     * Типограф
     * @param  string $text Текст для типографа
     * @return srting       Возвращаемый текст
     */
    static function typograf($text)
    {

        // add local library 1
        // require_once(SITE_PATH . 'includes/EMT.php');

        // $typograph = new EMTypograph();

        // $options = array(
        //     'Text.paragraphs' => 'off', 
        //     'Text.breakline' => 'off',
        //     'OptAlign.all' => 'off',
        //     'OptAlign.oa_oquote' => 'off',
        //     'OptAlign.oa_obracket_coma' => 'off',
        //     'OptAlign.oa_oquote_extra' => 'off',
        //     'OptAlign.layout' => 'off',
        //     'Text.auto_links' => 'off'
        //   );

        // $typograph->setup($options);

        // // задаём текст для типографирования
        // $typograph->set_text($text);
        // $result = $typograph->apply();

        // return $result;


        // Лебедев Стайл

        //  подключаем библиотеку, типографим получившееся 
        // include_once SITE_PATH . 'includes/remotetypograf.php';

        // $remoteTypograf = new RemoteTypograf('UTF-8');

        // $remoteTypograf->htmlEntities();
        // $remoteTypograf->br (false);
        // $remoteTypograf->p (false);
        // $remoteTypograf->nobr (3);
        // $remoteTypograf->quotA ('laquo raquo');
        // $remoteTypograf->quotB ('bdquo ldquo');

        // $return = $remoteTypograf->processText(html_entity_decode($text, ENT_QUOTES));

        // if ($return == 'Error: unknown action or encodin') $return = $text;

        // return $return;
        
        return $text;
    }


    /**
     * Функция проверяющая URL на дубли
     * @param  int $id id  товара
     * @param  string $url новый url товара
     * @param  string $table Таблица для проверки
     * @return array         'check' - TRUE - url подходит, FALSE - дубль; 'url' = значение url
     */
    static function check_url($id, $url, $table)
    {
        if ($table != 'rk_categories') {
            return array('check' => (DB::get_row("SELECT id FROM `$table` WHERE url = '$url' AND id <> $id") ? FALSE : TRUE),
                'url' => DB::get_field('url', "SELECT url FROM `$table` WHERE  id = $id"));
        } else {
            $parent = DB::get_field('parent_1', "SELECT parent_1 FROM rk_categories WHERE id = $id");
            return array('check' => (DB::get_row("SELECT id FROM rk_categories WHERE url = '$url' AND id <> $id AND parent_1 = $parent") ? FALSE : TRUE),
                'url' => DB::get_field('url', "SELECT url FROM `$table` WHERE  id = $id"));
        }

    }


    /**
     * Создает сообщение на вывод о произведенных действиях или ошибках
     * @param  array $data Массив данных
     * @param  string $type Тип сообщения для генерациии
     * @return string       HTML сообщение на вывод
     */
    static function report_msg($data, $type)
    {

        $msg = '';

        if ($type == 'info') {
            $msg .= '<p>Импорт по полям:</p><div>';
            foreach ($data as $table => $results) {

                $msg .= "<p>$table:</p>
                <ul>
                <li>Новые: {$results['insert']}</li>
                <li>Измененные: {$results['update']}</li>
                </ul>";
            }
            $msg .= '</div>';
        } else {

            //сообщения для основных ошибок
            if (!empty($data['error'])) {

                $msg .= "<div>
                        <p>При выполнении скрипта на строке '{$data['error']['line']}' произошла ошибка.</p>
                        <p>Текст ошибки: {$data['error']['text']}</p>
                        <p>Попробуйте удалить пустые строки и столбцы в файле.</p>
                     </div>";
            }

            if (!empty($data['main_fields'])) {

                $msg .= '<div>
                            <p>Данные со следующих строк не загружены, не заполнены обязятельные поля.</p>
                            <p>Список строк: ' . str_replace(' ', ', ', trim($data['main_fields'])) . '.</p>
                         </div>';
            }

            if (!empty($data['url'])) {

                $msg .= '<div>
                                <p>Данные не загружены. URL в следующих строках не уникален.</p>
                                <p>Список URL и строк: ' . str_replace(' ', ', ', trim($data['url'])) . '.</p>
                             </div>';
            }


            //сообщения для периодичных ошибок
            if (isset($data['lenght'])) {
                if (!empty($data['lenght'])) {

                    $msg .= '<div>
                                        <p>Превышен лимит символов для поля краткого описания или имени(для фильтров) в следующих строках.</p>
                                        <p>Список строк: ' . str_replace(' ', ', ', trim($data['lenght'])) . '.</p>
                                     </div>';
                }
            }

            if (isset($data['no_photo'])) {
                if (!empty($data['no_photo'])) {

                    $msg .= '<div>
                                                        <p>Для следующих позиций отсутствуют фотографии.</p>
                                                        <p>Список строк: ' . str_replace(' ', ', ', trim($data['no_photo'])) . '.</p>
                                                     </div>';
                }
            }


            if (isset($data['no_tm'])) {
                if (!empty($data['no_tm'])) {

                    $msg .= '<div>
                                                            <p>У следующих позиций не прописана торговая марка.</p>
                                                            <p>Список строк: ' . str_replace(' ', ', ', trim($data['no_tm'])) . '.</p>
                                                         </div>';
                }
            }

            if (isset($data['no_cats'])) {
                if (!empty($data['no_cats'])) {

                    $msg .= '<div>
                                                                    <p>У следующих позиций не прописаны Категории.</p>
                                                                    <p>Список строк: ' . str_replace(' ', ', ', trim($data['no_cats'])) . '.</p>
                                                                 </div>';
                }
            }

            if (isset($data['no_filters'])) {
                if (!empty($data['no_filters'])) {

                    $msg .= '<div>
                                                                            <p>У следующих позиций не прописаны фильтры.</p>
                                                                            <p>Список строк: ' . str_replace(' ', ', ', trim($data['no_filters'])) . '.</p>
                                                                         </div>';
                }
            }

            if (isset($data['no_group'])) {
                if (!empty($data['no_group'])) {

                    $msg .= '<div>
                                                                                <p>Нет ни одной группы для импортируемых фильтров.</p>
                                                                                <p>Список строк: ' . str_replace(' ', ', ', trim($data['no_group'])) . '.</p>
                                                                             </div>';
                }
            }


        }

        return $msg;

    }


    /**
     * Анализирует последний запрос, определяет количество добавленных и измененных полей
     * @param  array $info Данные предыдущего анализа
     * @return array       Обновленные данные
     */
    static function count_info($info)
    {

        $count = DB::last_query_info();

        if ($count == 1)
            ++$info['insert'];

        if ($count == 2)
            ++$info['update'];

        return $info;

    }

    /**
     * Добавление логов
     * @param  string $text Текст примечания
     * @param  string $user Пользователь
     * @param  string $log Лог выполнения
     * @param  string $type Тип таблиц
     * @return bool         Прошел ли запрос
     */
    static function new_log($text, $user, $log, $type)
    {

        if ($text == '')
            $text = 'Примечание не указано.';

        $query = "INSERT INTO rk_remarks(`text`, `user`, `log`, `date`, `type`) VALUES ('$text', '$user', '$log', NOW(), '$type')";

        return DB::query($query);

    }


    /**
     * Преобразует строку для импорта в mysql + преобразует символы в html сущности
     * @param  string $text Строка до преобразования
     * @param  string $type Тип преобразования 1 полный 0 только для sql
     * @return string       Строка после преобразования
     */
    static function convert_text($text, $type = 1)
    {

        if ($type)
            return DB::mysql_secure_string(htmlspecialchars(trim($text)));
        else {
            return DB::mysql_secure_string(trim($text));
        }

    }


    /**
     * Функция изменения редиректа в .htaccess если менялся url
     * @param  string $controller Контроллер для которого прописан редирект
     * @param  string $old_url Старый URL
     * @param  string $new_url Новый URL
     * @return int                Возвращает количество изменений
     */
    static function change_redirect($controller, $old_url, $new_url)
    {

        $text = str_replace("/$controller/$old_url [R=301,L]", "/$controller/$new_url [R=301,L]", file_get_contents('../../www/.htaccess'), $count);

        if ($count)
            file_put_contents('../../www/.htaccess', $text);

        return $count;
    }


    /**
     * Отправка служебных писем соответсвующим лицам
     * @param  string $type тип рассылки
     * @param  string $body тело письма
     * @return bool         результат отправки
     */
    static function service_mail($type, $body)
    {

        require_once 'mail_list.php';

        if (isset($mail_list['categories'])) {

            $to = $mail_list['categories'];
            $from = array('sales@roskosmetika.ru' => 'Импорт Роскосметика');
            $theme = "Импорт Роскосметика($type)";
            $body = "<html><h1>Импорт Роскосметика($type)</h1><h2>Информация о новом импорте:</h2>$body</html>";

            return @mailer::mail($theme, $from, $to, $body);
        } else {
            return FALSE;
        }
    }


    /**
     * Функция форматирующая название фотографии. + изменяет название.
     * @param  int $id ID товара
     * @param  string $src Название фото
     * @param  string $type тип данных
     * @return string       возвращает отформатированное название
     */
    static function check_img($id, $src, $type)
    {
        $id = (int)$id;

        $return = '';
        $old_src = DB::get_field('src', 'SELECT src FROM rk_' . $type . ' WHERE id = ' . $id);
        $path = $type;


        if ($old_src) {
            $return = $old_src;
            if (($src != '') && ($old_src != $src)) $return = $src;

        } else {
            if ($src != '') $return = $src;
        }


        if ($return != '') {
            $ext = trim(strstr($return, '.'));
            if ($ext) {
                $return = str_replace($ext, '.jpg', $return);
            } else {
                $return = $return . '.jpg';
            }

            if (file_exists('../../www/images/' . $path . '/' . $old_src) && ($return != $old_src) && ($old_src != 'none.jpg') && !is_null($old_src)) {
                @rename('../../www/images/' . $path . '/' . $old_src, '../../www/images/' . $path . '/' . $return);
            }

            if (!file_exists('../../www/images/' . $path . '/' . $return)) {
                $return = 'none.jpg';
            }

        } else {
            $return = 'none.jpg';
        }

        return $return;

    }


    /**
     * Обработка новинок
     * @param  int $id id проверяемой позиции
     * @param  int $new новое значение
     * @param  string $table таблица для проверки
     * @return string          [description]
     */
    static function check_for_new($id, $new, $table)
    {

        if ((!DB::get_row("SELECT id FROM `$table` WHERE id = $id")) && ($new == 0)) $new = 1;

        return $new;

    }


    /**
     * Подключение библиотек и загрузка файла для чтения
     * @param  [type] $file [description]
     * @return PHPExcel       [description]
     */
    static function load_file($file)
    {

        include(SITE_PATH . 'includes/PHPExcel.php');
        include_once SITE_PATH . 'includes/PHPExcel/IOFactory.php';

        $new_file_type = PHPExcel_IOFactory::identify($file);
        $object = PHPExcel_IOFactory::createReader($new_file_type);
        $object->setReadDataOnly(true);

        return $object->load($file);

    }
}

?>