<?php

/**
 * Categories management.
 */
class Categories
{
    /**
     * Parfumery category ID
     */
    const PARFUMERY_ID = 1332;

    /**
     * Returns join tables for products/categories join
     * @return array
     */
    public static function product_joins()
    {
        return array(
            1 => 'rk_prod_cat_lvl_1',
            2 => 'rk_prod_cat_lvl_2',
            3 => 'rk_prod_cat',
        );
    }

    /**
     * Return category of ever lvl
     * @param integer $id category ID
     * @return array
     */
    static function get_category($id)
    {

        $query = "SELECT * FROM rk_categories WHERE id  = $id";
        return DB::get_row($query);
    }

    /**
     * Return subcategory of ever level
     * @param integer $id subcategory ID
     * @return array
     */
    static function get_subcategory($id)
    {

        $query = "SELECT * FROM rk_subcategories WHERE id  = $id";
        return DB::get_row($query);
    }


    /**
     * Link category - subcategory
     * @param integer $cat_id category ID
     * @param integer $subcat_id subcategory ID
     * @return bool
     */
    static function set_subcat_link($cat_id, $subcat_id)
    {

        $query = "INSERT INTO rk_cat_subcat (cat_id, subcat_id)
                VALUES ($cat_id, $subcat_id)";
        return DB::query($query);
    }

    /**
     * Add new category
     * @param integer $name category name
     * @param integer $level category lvl
     * @param integer $parent_1 first parent
     * @param integer $parent_2 second parent
     * @param string|bool $tagName Tag name (Negative for category) or true if is tag (Without admin name)
     * @param string $url Category URL
     * @param int $tagTypePageID Parent type page ID (For tags)
     *
     * @return int new category ID
     */
    static function add_category($name, $level, $parent_1 = 0, $parent_2 = 0, $tagName = null, $url = '', $tagTypePageID = 0)
    {
        $query = "INSERT INTO rk_categories (name, level, parent_1, parent_2, is_tag, url, type_page_id)
                  VALUES ('$name', '$level', $parent_1, $parent_2, " . ($tagName ? 1 : 0) . ", '$url', " . ((int)$tagTypePageID) . ")";

        DB::query($query);

        $id = DB::get_last_id();

        DB::query('INSERT INTO rk_cat_desc (id, `desc`, synonym, vk_id) VALUES (' . $id . ', "", "", 0)');
        DB::query('INSERT INTO rk_cat_seo (id, title, description, keywords, tag_name) VALUES (' . $id . ', "", "", "", "' . ($tagName && is_string($tagName) ? DB::escape($tagName) : '') . '")');

        return $id;
    }

    /**
     * Add ne subcategory
     * @param integer $level category level
     * @param string $name category name
     * @return bool
     */
    static function add_subcategory($level, $name)
    {
        $query = "INSERT INTO rk_subcategories (level, name)
                  VALUES ('$level', '$name')";

        return DB::query($query);


    }


    /**
     * Info about last aded subcategory
     * @return array
     */
    static function last_subcategory()
    {
        $query = 'SELECT * FROM rk_subcategories ORDER BY id DESC LIMIT 1';

        return DB::get_row($query);
    }


    /**
     * Info about last aded category
     * @return array
     */
    static function last_category()
    {

        $query = 'SELECT * FROM rk_categories WHERE id = (SELECT max(id) FROM rk_categories)';
        return DB::get_row($query);
    }


    /**
     * Get level1 and level2 for main_menu
     *
     * @param bool $includeTms Include trademarks
     * @param bool $visibleOnly Include visible categories only
     * @param bool $includeTags Include tag categories
     *
     * @return array levels array for main menu
     */
    static function get_main_menu($includeTms = true, $visibleOnly = true, $includeTags = false)
    {
        $data = FALSE;

        //  get 1 lvl IDs 
        $level_1 = DB::get_rows(
            'SELECT c.id, c.name, IF(c.url = "", c.id, c.url) AS url, c.is_tag
            FROM rk_categories AS c
            WHERE c.level = 1' .
            ($visibleOnly ? ' AND c.visible = 1 AND c._has_products = 1' : '') .
            (!$includeTags ? ' AND c.is_tag = 0' : '') .
            (TM_FOR_PROF ? ' AND c._prof = 1' : '') .
            ' ORDER BY c.norder'
        );

        if (!empty($level_1)) {
            foreach ($level_1 as $key => $lvl_1_items) {

                $data[$key] = $lvl_1_items;

                $query = "SELECT s.id, s.name
                         FROM rk_subcategories s, rk_categories c, rk_cat_subcat cs
                         WHERE c.id = cs.cat_id
                         AND   cs.subcat_id = s.id
                         AND   c.level = '2'
                         AND   s.level = '2'
                         AND   c.parent_1 = {$lvl_1_items['id']}" .
                        (!$includeTags ? ' AND c.is_tag = 0' : '') . 
                        (TM_FOR_PROF ? ' AND c._prof = 1' : '') . " GROUP BY s.id";

                $subcategories = DB::get_rows($query);

                foreach ($subcategories as $sub) {
                    //  get 2level for subcat
                    $query = "SELECT c.id, c.name, IF(c.url =  '', c.id, c.url ) AS url, c.is_tag
                                   FROM rk_subcategories s, rk_categories c, rk_cat_subcat cs
                                   WHERE   s.id  = {$sub['id']}
                                   AND     c.id = cs.cat_id
                                   AND     cs.subcat_id = s.id                          
                                   AND     c.level = '2'
                                   AND     s.level = '2'
                                   AND     c.parent_1 = {$lvl_1_items['id']}" .
                                   ($visibleOnly ? ' AND c.visible = 1 AND c._has_products = 1' : '') .
                                   (!$includeTags ? ' AND c.is_tag = 0' : '') .
                                   (TM_FOR_PROF ? ' AND c._prof = 1' : '') .
                                   ' ORDER BY c.name';

                    $data[$key]['lvl_2'][$sub['name']]['items'] = DB::get_rows($query);
                }

                if ($includeTms) {
                    //  get tm for 1 level

                    $query = 'SELECT tm.id, tm.name, IF(tm.url =  "", tm.id, tm.url ) url
                            FROM tm
                            WHERE tm.visible = 1 AND tm._has_products = 1 ' . (TM_FOR_PROF ? ' AND tm.prof = 1 ' : '') . '
                            AND tm.id IN (SELECT tm_id FROM rk_tm_in_cat WHERE cat_id = ' . ((int)$lvl_1_items['id']) . ' AND _has_products = 1)';

                    $result = DB::get_rows($query);

                    usort($result, function ($a, $b) {
                        mb_eregi('^[^a-z' . helpers::RUS_LETTERS . ']*([a-z' . helpers::RUS_LETTERS . '])', $a['name'], $matchesA);
                        mb_eregi('^[^a-z' . helpers::RUS_LETTERS . ']*([a-z' . helpers::RUS_LETTERS . '])', $b['name'], $matchesB);

                        $firstRus = mb_eregi('[' . helpers::RUS_LETTERS . ']', $matchesA[1]);
                        $secondRus = mb_eregi('[' . helpers::RUS_LETTERS . ']', $matchesB[1]);

                        return $firstRus && !$secondRus ? -1 : (!$firstRus && $secondRus ? 1 : ($matchesA[1] < $matchesB[1] ? -1 : 1));
                    });

                    //  check for cats in one tm
                    for ($i = 0, $n = count($result); $i < $n; ++$i) {
                        $query = "SELECT COUNT(pc.cat_id) AS cnt
                                      FROM   rk_prod_cat_lvl_1 AS pc, products AS p
                                      WHERE p.tm_id  = " . ((int)$result[$i]['id']) . "
                                      AND   p.id =  pc.prod_id
                                      AND   p.visible = 1
                                      AND   p.active = 1
                                      GROUP BY pc.cat_id";

                        $cats_of_tm = DB::get_field('cnt', $query);

                        $result[$i]['cat_not_one'] = $cats_of_tm > 1;
                    }

                    $data[$key]['lvl_2']['производитель']['items'] = $result;
                    $data[$key]['lvl_2']['производитель']['category-tm'] = true;
                }
            }
        }

        return $data;
    }

    /**
     * Return categories with subcategories in array (admin layout).
     * @param  bool $only_visible get all categories if False and only visible if True
     * @param bool $tags Include tags
     * @return bool
     */
    static function get_categories($only_visible = False, $tags = false)
    {

        $data['lvl_1'] = 0;
        $data['lvl_2'] = array();
        $data['lvl_3'] = array();


        // сбор id по уровням
        $level_1 = DB::get_rows('SELECT * FROM rk_categories WHERE level = 1' . ($only_visible ? ' AND visible = 1' : '') . (!$tags ? ' AND is_tag = 0' : ''));


        $parents_level_2 = DB::get_rows('SELECT parent_1 FROM  `rk_categories` 
                                          WHERE  `level` =2 AND parent_1 !=0'  . ($only_visible ? ' AND visible = 1' : '') . (!$tags ? ' AND is_tag = 0' : '') .
                                          ' GROUP BY  `parent_1` ');

        $parents_level_3 = DB::get_rows('SELECT parent_1 FROM  `rk_categories` 
                                          WHERE  `level` =3 AND parent_1 !=0' . ($only_visible ? ' AND visible = 1' : '') .  (!$tags ? ' AND is_tag = 0' : '') . 
                                          ' GROUP BY  `parent_1` ');


        //данные для первого уровня
        if (!empty($level_1)) {
            $data['lvl_1'] = $level_1;
        }

        unset($level_1);

        if (!empty($parents_level_2)) {

            for ($i=0; $i < count($parents_level_2); $i++) { 

                $subs = DB::get_rows("SELECT s.id, s.name FROM rk_subcategories s, rk_categories c, rk_cat_subcat cs
                                      WHERE c.id = cs.cat_id
                                      AND   cs.subcat_id = s.id
                                      AND   c.level = 2
                                      AND   c.parent_1 = {$parents_level_2[$i]['parent_1']}
                                      AND   c.is_tag = 0
                                      GROUP BY s.id");

                for ($j=0; $j < count($subs); $j++) { 

                    $cats = DB::get_rows("SELECT c.id, c.name, c.is_tag, c._full_url FROM rk_categories c, rk_cat_subcat cs
                                          WHERE c.id = cs.cat_id
                                          AND c.level = 2
                                          AND c.parent_1 = {$parents_level_2[$i]['parent_1']}
                                          AND c.is_tag = 0
                                          AND cs.subcat_id = {$subs[$j]['id']}");

                    $subs[$j]['cats'] = $cats;
                }

                if ($tags) {
                    $cTags = DB::get_rows('SELECT c.id, CONCAT(c.name, " [Тег]") AS name, c.is_tag, c._full_url FROM rk_categories AS c WHERE c.parent_1 = ' . $parents_level_2[$i]['parent_1'] . ' AND c.level = 2 AND c.is_tag = 1' . ($only_visible ? ' AND c.visible = 1' : ''));

                    if ($cTags) {
                        $subs[] = [
                            'id' => 0,
                            'name' => 'Теги',
                            'cats' => $cTags,
                        ];
                    }
                }

                $data['lvl_2']["{$parents_level_2[$i]['parent_1']}"] = $subs;


            }
        }

        unset($parents_level_2);

        if (!empty($parents_level_3)) {

            for ($i=0; $i < count($parents_level_3); $i++) { 

                $subs = DB::get_rows("SELECT s.id, s.name FROM rk_subcategories s, rk_categories c, rk_cat_subcat cs
                                      WHERE c.id = cs.cat_id
                                      AND   cs.subcat_id = s.id
                                      AND   c.level = 3
                                      AND   c.parent_1 = {$parents_level_3[$i]['parent_1']}
                                      AND   c.is_tag = 0
                                      GROUP BY s.id");

                for ($j=0; $j < count($subs); $j++) { 

                    $cats = DB::get_rows("SELECT c.id, c.name, c.is_tag, c._full_url FROM rk_categories c, rk_cat_subcat cs
                                          WHERE c.id = cs.cat_id
                                          AND c.level = 3
                                          AND c.parent_1 = {$parents_level_3[$i]['parent_1']}
                                          AND c.is_tag = 0
                                          AND cs.subcat_id = {$subs[$j]['id']}");

                    $subs[$j]['cats'] = $cats;
                }

                if ($tags) {
                    $cTags = DB::get_rows('SELECT c.id, CONCAT(c.name, " [Тег]") AS name, c.is_tag, c._full_url FROM rk_categories AS c WHERE c.parent_1 = ' . $parents_level_3[$i]['parent_1'] . ' AND c.level = 3 AND c.is_tag = 1' . ($only_visible ? ' AND c.visible = 1' : ''));

                    if ($cTags) {
                        $subs[] = [
                            'id' => 0,
                            'name' => 'Теги',
                            'cats' => $cTags,
                        ];
                    }
                }

                $data['lvl_3']["{$parents_level_3[$i]['parent_1']}"] = $subs;
            }
        }

        return $data;
    }


    /**
     * Return categories with filters for export
     * @param  array $param special parameters
     * @return array  category array
     */
    static function all_with_filters($param)
    {

        //включение проверки на NULL и ''
        if (!empty($param)) {
            $where = array();
            $id = null;

            if (isset($param['id'])) {
                $id = $param['id'];
                unset($param['id']);
            }

            foreach ($param as $field) {
                $where[] = "($field = '')";
                $where[] = "($field IS NULL)";
            }

            $where = implode(' OR ', $where);

            if ($id) {
                $category = self::get_category($id);

                if ($category) {
                    $categoryWhere = '';

                    switch (true) {
                        case $category['level'] == 1:
                            $categoryWhere = 'cat.parent_1 = ' . intval($category['id']);

                            break;

                        case $category['level'] == 2:
                            $categoryWhere = ' cat.parent_2 = ' . intval($category['id']);

                            break;

                        default:
                            $categoryWhere = ' cat.id = ' . intval($category['id']);

                            break;
                    }

                    $where = ($where ? '(' . $where . ') AND ' : '') . $categoryWhere;
                }
            }

            $param = 'WHERE ' . $where;
        }

        $query = 'SELECT cat.id, cat.name, cat.level, cat.parent_1, cat.parent_2, cat.url, cat.h1, c_desc.desc, c_desc.synonym, seo.title, seo.description seo_desc, seo.keywords,
                        (SELECT GROUP_CONCAT(cf.filter_id) FROM rk_cat_filters cf WHERE cf.cat_id = cat.id) filters 
                  FROM rk_categories cat
                  LEFT JOIN rk_cat_desc c_desc ON ( c_desc.id = cat.id ) 
                  LEFT JOIN rk_cat_seo seo ON ( seo.id = cat.id ) ' . $param .
            ' ORDER BY cat.id';

        return DB::get_rows($query);
    }


    /**
     * Get categories for structure
     * @param  int $id category or subcategory ID
     * @param  string $type category or subcategory 'cat' - category
     * @return array        return array of data
     */
    static function cat_by_structure($id, $type)
    {

        //  data array
        $result = array();

        if ($type == 'cat') {

            $query = "SELECT cat.id, cat.name, cat.level, cat.parent_1, cat.parent_2, cat.url, cat.h1, c_desc.desc, c_desc.synonym, seo.title, seo.description seo_desc, seo.keywords,
                   (SELECT GROUP_CONCAT(cf.filter_id) FROM rk_cat_filters cf WHERE cf.cat_id = cat.id) filters 
                    FROM rk_categories cat
                    LEFT JOIN rk_cat_desc c_desc ON ( c_desc.id = cat.id ) 
                    LEFT JOIN rk_cat_seo seo ON ( seo.id = cat.id )
                    WHERE ((cat.id = $id) OR (cat.parent_1 = $id) OR (cat.parent_2 = $id))
                    ORDER BY cat.id";
            $result = DB::get_rows($query);

        } else {
            //  get categories from subcategory
            $categories = DB::get_rows("SELECT cat_id FROM rk_cat_subcat WHERE subcat_id = $id");

            foreach ($categories as $category) {

                $query = "SELECT cat.id, cat.name, cat.level, cat.parent_1, cat.parent_2, cat.url, cat.h1, c_desc.desc, c_desc.synonym, seo.title, seo.description seo_desc, seo.keywords,
                         (SELECT GROUP_CONCAT(cf.filter_id) FROM rk_cat_filters cf WHERE cf.cat_id = cat.id) filters 
                          FROM rk_categories cat
                          LEFT JOIN rk_cat_desc c_desc ON ( c_desc.id = cat.id ) 
                          LEFT JOIN rk_cat_seo seo ON ( seo.id = cat.id )
                          WHERE ((cat.id = {$category['cat_id']}) OR (cat.parent_1 = {$category['cat_id']}) OR (cat.parent_2 = {$category['cat_id']}))
                          ORDER BY cat.id";

                $pre_result = DB::get_rows($query);

                foreach ($pre_result as $key => $pre_cat) {
                    $result[] = $pre_cat;
                }
            }
        }

        return $result;
    }


    /**
     * Get by ID  URL for redirect catrgories
     * @param  int $cat_id id of category
     * @return string       URL of category
     */
    static function redirect($cat_id)
    {

        return DB::get_field('url', "SELECT url FROM rk_categories WHERE id = $cat_id AND visible = 1");

    }


    /**
     * Get by URL id to get catrgories and products
     * @param  string $url url of category
     * @param  int|bool $parent_id id of parent category
     * @param bool $visible Search visible only
     * @param bool $parentIsCategory Parent is category (Not type page)
     *
     * @return int          ID of category
     */
    static function get_id_by_url($url, $parent_id = null, $visible = true, $parentIsCategory = true)
    {
        return (int)DB::get_field('id', "SELECT id
                                          FROM rk_categories 
                                          WHERE url = '$url' 
                                          " . ($visible ? ' AND visible = 1 ' : '')
            . ($parent_id !== null ? " AND " . ($parentIsCategory ? 'parent_1' : 'type_page_id') . ' = ' . ((int)$parent_id) : '')
            . ' LIMIT 1'
        );
    }

    /**
     * Output products, filters and subcategories for levels 1-3
     * @param  int $lvl_1_id category ID fist level
     * @param  int $lvl_2_id category ID second level
     * @param  int $lvl_3_id category ID third level
     * @param  bool $flag flag for start work - TRUE, if FALSE - stop work and return FALSE
     * @param bool $ajax Return only products
     * @param string $crumbsPrefix Breadcrumbs base prefix
     * @param int $typePageID Type page ID (If /kosmetika/ page requested)
     *
     * @return array         products of categories with subcategories and filters
     */
    static function client_output($lvl_1_id, $lvl_2_id, $lvl_3_id, $flag, $ajax, $crumbsPrefix = '/category/', $typePageID = null)
    {
        //  return array begin value
        $data = FALSE;

        if ($flag) {
            $filter_condition = $ajax ? Product::filter_condition('p') : '';
            $price_condition = $ajax ? Product::price_condition('p') : '';
            $tm_condition = $ajax ? Product::tm_condition('p') : '';
            $country_condition = $ajax ? Product::country_condition('p') : '';

            $level = $lvl_3_id ? 3 : ($lvl_2_id ? 2 : 1);
            $lvl_id = $level === 1 ? $lvl_1_id : ($level === 2 ? $lvl_2_id : $lvl_3_id);

            $check = DB::get_row("SELECT c.id, c.name, IF(c.url =  '', c.id, c.url ) url, c._full_url, c.level, c.h1, c.canonical, c.is_tag, c.parent_1, c.parent_2, c.h1_1_organik, c.h1_2_organik,
                               cd.desc,
                               cs.title, cs.description seo_desc, cs.keywords, cs.tag_name, cs.name_plural, cs.h1_plural, cs.name_alt_number, cs.h1_alt_number, cs.name_on_products
                        FROM rk_categories c
                        LEFT JOIN rk_cat_desc cd   ON ( c.id = cd.id )
                        LEFT JOIN rk_cat_seo cs    ON ( cs.id = c.id )
                        WHERE c.id = " . $lvl_id . "
                        AND " . (
                            $level === 1
                                ? 'c.level = 1'
                                : (
                                    'parent_1 = ' . ($level === 2 ? $lvl_1_id : $lvl_2_id) .
                                    ($level === 3 ? ' AND parent_2 = ' . $lvl_1_id : '')
                                )
                        ) . "
                        AND c.visible = 1");

            if ($check) {
                if (!$ajax) {
                    $data['main'] = $check;

                    $data['bread_1'] = $level === 1 ? false : DB::get_row("SELECT id, name, IF(url =  '', id, url ) url FROM rk_categories WHERE id = $lvl_1_id" . (TM_FOR_PROF ? ' AND _prof = 1' : ''));
                    $data['bread_2'] = $level === 1 ? false : DB::get_row("SELECT id, name, IF(url =  '', id, url ) url FROM rk_categories WHERE id = $lvl_2_id" . ($level === 2 ? " AND parent_1 = $lvl_1_id" : '') . (TM_FOR_PROF ? ' AND _prof = 1' : ''));

                    $data['articles'] = DB::get_rows("SELECT a.id, a.name, IF(a.url =  '', a.id, a.url ) url
                                                        FROM rk_articles a, rk_cat_article ca
                                                        WHERE ca.cat_id = " . $lvl_id . "
                                                        AND   a.id = ca.article_id
                                                        AND   a.visible = 1
                                                        ORDER BY a.name");

                    $data['urls']['url_1'] = $level === 1 ? $check : $data['bread_1'];
                    $data['urls']['url_2'] = $level === 1 ? false : ($level === 2 ? $check : $data['bread_2']);

                    list($categories_html, $data['last_crumb']) = self::_subcategories_html(
                        $level === 1 ? $lvl_1_id : $lvl_2_id, $level === 1 ? 2 : 3,
                        $level === 3 ? $lvl_3_id : 0
                    );

                    $data['tms'] = tm::category_tms($lvl_3_id ? $lvl_3_id : ($lvl_2_id ? $lvl_2_id : $lvl_1_id));

                    $data['tags_html'] = $level !== 3 ? self::_tags_html($level === 1 ? $lvl_1_id : $lvl_2_id, $level) : '';

                    $organikCategory = $data['main']['id'] == 1448 || $data['main']['parent_1'] == 1448 || $data['main']['parent_2'] == 1448;

                    $data['reviews'] = review::product_reviews_category($lvl_id, $level, 2);

                    if ($data['reviews']) {
                        $data['reviews_title'] = helpers::grammatical_case(
                            $data['main']['is_tag'] && $data['main']['tag_name']
                                ? $data['main']['tag_name']
                                : (
                                    $organikCategory && $data['main']['h1_2_organik']
                                        ? $data['main']['h1_2_organik']
                                        : $data['main']['h1']
                                ),
                            helpers::CASE_TYPE_PREPOSITIONAL_EXTENDED,
                            true
                        );
                    }

                    $crumbs = self::breadcrumbs($data['main']['id'], $crumbsPrefix);

                    $data['crumbs'] = $crumbs;

                    $crumbs_names = [];

                    for ($i = 0, $n = count($crumbs); $i < $n; ++$i) {
                        $crumbs_names[] = $crumbs[$i]['name'];
                    }

                    $data['crumbs_text'] = implode(' / ', $crumbs_names);
                }

                $joins = self::product_joins();

                $innerJoins = 'INNER JOIN ' . $joins[$level] . ' AS pc_join ON pc_join.cat_id = ' . $lvl_id . ' AND pc_join.prod_id = p.id
                    INNER JOIN rk_prod_desc pd  ON ( p.id = pd.id )
                    INNER JOIN tm ON tm.id = p.tm_id';

                $where = '(p.visible = 1 OR IF((SELECT COUNT(pp.id) FROM products AS pp WHERE pp.visible = 1 AND pp.main_id = p.id), 1, 0))
                    AND p.main_id = 0 AND (p.active = 1 OR p.price = 0 AND p.new = 1)' . (TM_FOR_PROF ? ' AND tm.prof = 1 ' : '');

                $whereFull = $where . $filter_condition . $tm_condition . $price_condition . $country_condition;

                Pagination::setValues((int)DB::get_field('cnt', 'SELECT COUNT(p.id) AS cnt FROM products AS p ' . $innerJoins . ' WHERE ' . $whereFull));

                $query = 'SELECT p.id, IF(p.url = "", p.id, p.url ) url, p.main_id, p.name, p.short_description, p.description, p.price, p.special_price, p.pack, p.new, p.hit, p.available, p.for_proff, 
                             p._max_discount AS discount, p._max_discount_days  AS is_sales,
                             pd.eng_name, pd.use, pd.ingredients, pd.synonym, p.tm_id,
                             ps.title, ps.description seo_desc, ps.keywords,
                             pp.src, pp.alt, pp.title photo_title,
                             tm.name tm, IF(tm.url = "", tm.id, tm.url ) tm_url, td.country,
                             (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.hit = 1 LIMIT 1) AS pack_hit,
                             (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.new = 1 LIMIT 1) AS pack_new,
                             IF(p.active = 0 AND p.price = 0 AND p.new = 1 AND p.visible = 1, 1, 0) AS soon
                            FROM products AS p
                            ' . $innerJoins . '
                            LEFT JOIN rk_prod_seo ps    ON ( ps.id = p.id )
                            LEFT JOIN rk_prod_photo pp  ON ( pp.id = p.id )
                            LEFT JOIN rk_tm_desc td     ON ( p.tm_id = td.id )
                            WHERE ' . $whereFull . ' 
                            ORDER BY ' . Product::filter_order('p', false, [
                                'categoryID' => $lvl_id
                            ]) .
                            Pagination::sqlLimit();

                $products = DB::get_rows($query);

                $data['products'] = filters::set_products_info($products);

                $data['filter_tms'] = $data['products'] ? filters::available_tms($where . $filter_condition . $price_condition . $country_condition, $innerJoins, 'p', $ajax) : null;
                $data['filter_prices'] = $data['products'] ? filters::price_range($where . $filter_condition . $tm_condition . $country_condition, $innerJoins) : null;
                $data['filter_countries'] = $data['products'] ? filters::available_countries($where . $filter_condition . $price_condition . $tm_condition, $innerJoins, 'p', $ajax) : null;
                $data['show_desc'] = Pagination::getValues('page') > 1 ? FALSE : ($check['desc'] ?: FALSE);

                $allowedFilters = [];

                DB::get_rows(
                    'SELECT af.filter_id FROM rk_allowed_category_filters AS af WHERE af.category_id = ' . $lvl_1_id,
                    function ($allowedFilter) use (&$allowedFilters) {
                        $allowedFilters[] = (int)$allowedFilter['filter_id'];
                    }
                );

                if (!$ajax) {
                    if ($data) {
                        if ($data['main']['tag_name'] || $data['main']['name']) {
                            $nameInPlural = $data['main']['h1'] ? $data['main']['h1_plural'] : $data['main']['name_plural'];
                            $altName = $data['main']['h1'] ? $data['main']['h1_alt_number'] : $data['main']['name_alt_number'];

                            if ($organikCategory && $data['main']['h1_2_organik']) {
                                $data['main']['h1'] = $data['main']['h1_2_organik'];
                            }

                            if (!$data['main']['h1']) {
                                $data['main']['h1'] = $data['main']['tag_name'] ?: $data['main']['name'];
                            }

                            if (!$data['main']['title']) {
                                $data['main']['title'] =
                                    helpers::numeric_name($data['main']['h1'], $altName, $nameInPlural, true) .
                                    ', купить ' .
                                    helpers::grammatical_case(
                                        helpers::numeric_name($data['main']['h1'], $altName, $nameInPlural, false),
                                        helpers::CASE_TYPE_ACCUSATIVE,
                                        true
                                    ) .
                                    ' в интернет-магазине с доставкой. Низкие цены, скидки!';
                            }

                            if (!$data['main']['seo_desc']) {
                                $data['main']['seo_desc'] = 'Выбрать и заказать ' . helpers::grammatical_case($data['main']['h1'], helpers::CASE_TYPE_ACCUSATIVE, true) . ' в интернет-магазине Роскосметика с доставкой по Москве, Санкт-Петербургу, России! В нашем каталоге огромный ассортимент, скидки и лучшие цены!';
                            }
                        }

                        $joins = self::product_joins();

                        $info = DB::get_row('SELECT COUNT(p.id) AS cnt, MIN(p.price) AS min_price, MAX(p.price) AS max_price FROM products AS p WHERE p.id IN (SELECT pc.prod_id FROM ' . $joins[$level] .' AS pc WHERE pc.cat_id = ' . $data['main']['id'] . ') AND p.visible = 1 AND p.active = 1 AND p.price > 0');

                        //  data for Open Graph
                        if ($info['cnt'] && $info['min_price']) {

                            $data['main']['og_description'] = html_entity_decode($data['main']['h1']) . ' от интернет магазина Роскосметика – это ' . count_morphology($info['cnt'] . ' товаров|товар|товара') . ' по цене от ' . ((int)$info['min_price']) . ' руб, удобные способы оплаты и оперативная доставка, включая возможность самовывоза!';
                        }

                        $data['filters_html'] = self::_filters_html($lvl_id, $data['main']['h1'], $level, $categories_html, $data['filter_tms'], $data['filter_prices'], $data['filter_countries'],  $allowedFilters ?: null);

                        $data['base_links'] = $typePageID
                            ? link_base::get_links(link_base::TYPE_PAGE_CATEGORY_TYPE_ID, $typePageID, $lvl_1_id, $lvl_2_id, $lvl_3_id)
                            : link_base::get_links(link_base::CATEGORY_TYPE_ID, $lvl_1_id, $lvl_2_id, $lvl_3_id);

                        if ($info['cnt']) {
                            $data['main']['title'] = helpers::replace_seo_values(
                                $data['main']['title'],
                                [
                                    'MIN_PRICE' => (int)$info['min_price'],
                                    'MAX_PRICE' => (int)$info['max_price'],
                                ]
                            );

                            $data['main']['seo_desc'] = helpers::replace_seo_values(
                                $data['main']['seo_desc'],
                                [
                                    'MIN_PRICE' => (int)$info['min_price'],
                                    'MAX_PRICE' => (int)$info['max_price'],
                                ]
                            );

                            $data['main']['desc'] = helpers::replace_seo_values(
                                $data['main']['desc'],
                                [
                                    'MIN_PRICE' => (int)$info['min_price'],
                                    'MAX_PRICE' => (int)$info['max_price'],
                                ]
                            );
                        }

                        if (count($products) <= 12) {
                            $data['popular_products'] = Product::popular_products_block(8, null, $data['main']['id'], $typePageID);
                        }
                    }
                } else {
                    $data['filters'] = $data['products'] ? filters::available_filters($where . $tm_condition . $price_condition . $country_condition, $innerJoins, 'p', $allowedFilters ?: null) : null;
                }
            }

            return $data ?: false;
        }

        return null;
    }

    /**
     * Return categories list
     *
     * @param bool $visible 1 - only visible, 0 - all
     * @param string $search Search query (Name, url)
     * @param int|bool $onPage On page limit. If true returns categories count
     * @param int $offset Pagination offset
     * @param bool $tagsOnly Search for tags categories only
     *
     * @return array            array of categories
     */
    static function get_list($visible = TRUE, $search = null, $onPage = null, $offset = null, $tagsOnly = false)
    {

        $query = 'SELECT ' . ($onPage === true ? 'COUNT(c.id) AS cnt' : 'c.id, c.name, c.level, c._full_url, DATE_FORMAT( c.lastmod ,"%Y-%m-%d") AS lastmod, c.is_tag,
                  cs.title, cs.description, cs.tag_name') . '
                  FROM rk_categories c
                  LEFT JOIN rk_cat_seo AS cs ON cs.id = c.id
                  WHERE c.canonical IS NULL ' .
                    ($visible ? 'AND c.visible = 1 ' : '') .
                    ($search ? ' AND (c.name LIKE "%' . ($search = DB::escape($search)) . '%" OR c.url LIKE "%' . $search . '%" OR cs.tag_name LIKE "%' . $search . '%")' : '') .
                    ($tagsOnly ? ' AND c.is_tag = 1' : '') .
                  ' ORDER BY c.level' . ($onPage !== null ? ' LIMIT ' . (($offset !== 0 ? ((int)$offset) . ', ' : '') .  (int)$onPage) : '');

        return $onPage === true ? DB::get_field('cnt', $query) : DB::get_rows($query);
    }

    /**
     * Return gategories tree
     *
     * @param bool $visible 1 - only visible, 0 - all
     * @param bool $no_cat - Hide special cats
     * @param bool $tags Return tags or regular categories
     *
     * @return array            array of categories
     */
    static function get_tree($visible = TRUE, $no_cat = FALSE, $tags = false)
    {

        $return = FALSE;

        $query = "SELECT id, name, level, IF( url =  '', id, url ) url, parent_1, is_tag
                FROM rk_categories  
                WHERE level = 1 AND is_tag = " . ($tags ? 1 : 0)
            . ($no_cat ? " AND id != 7 AND id != 775 " : '') .
            " AND parent_1 = 0 " . ($visible ? " AND visible = 1 " : '') . ' ORDER BY id ';

        $lvls_1 = DB::get_rows($query);

        foreach ($lvls_1 as $lvl_1) {
            $return[] = $lvl_1;

            $query = "SELECT id, name, level, IF( url =  '', id, url ) url, parent_1, is_tag
                    FROM rk_categories  
                    WHERE level = 2 AND is_tag = " . ($tags ? 1 : 0) . "
                    AND parent_1 =  {$lvl_1['id']}
                    AND parent_2 = 0 " . ($visible ? " AND visible = 1 " : '') . ' ORDER BY id';

            $lvls_2 = DB::get_rows($query);

            foreach ($lvls_2 as $lvl_2) {
                $return[] = $lvl_2;

                $query = "SELECT id, name, level, IF( url =  '', id, url ) url, parent_1, is_tag
              FROM rk_categories  
              WHERE level = 3 AND is_tag = "  . ($tags ? 1 : 0) . "
              AND parent_1 =  {$lvl_2['id']}
              AND parent_2 =  {$lvl_1['id']} " . ($visible ? " AND visible = 1 " : '') . ' ORDER BY id ';

                $lvls_3 = DB::get_rows($query);
                foreach ($lvls_3 as $lvl_3) {
                    $return[] = $lvl_3;
                }
            }
        }

        return $return;
    }

    /**
     * Fetches category(es) by ID(s)
     *
     * @param int|array $id ID or array of IDs
     * @param int $parent_id Add category parent ID to search conditions
     * @param bool $visibleOnly Search for visible only
     *
     * @return array
     */
    public static function get_by_id($id, $parent_id = null, $visibleOnly = false)
    {
        $multiple = is_array($id);

        $rows = DB::get_rows('SELECT c.id, c.name, IF( c.url =  "", c.id, c.url ) url, c.is_tag, c._full_url, c.h1, c.level, c.parent_1, c.parent_2, c.type_page_id, c.h1_1_organik, c.h1_2_organik,
                cd.desc,
                cs.title, cs.description, cs.keywords, cs.tag_name, cs.name_alt_number, cs.h1_alt_number, cs.name_plural, cs.h1_plural, cs.name_on_products,
                t.url AS type_page_url
            FROM rk_categories c
            LEFT JOIN rk_cat_desc cd   ON ( cd.id = c.id  )
            LEFT JOIN rk_cat_seo cs   ON ( cs.id = c.id  )
            LEFT JOIN rk_type_page AS t ON t.id = c.type_page_id
            WHERE c.id ' . ($multiple ? 'IN (' . implode(', ', $id) . ')' : '= ' . intval($id)) .
            ($parent_id ? ' AND c.parent_1 = ' . (int)$parent_id : '') .
            ($visibleOnly ? ' AND c.visible = 1' : '')
        );

        return $multiple ? $rows : (!empty($rows[0]) ? $rows[0] : null);
    }

    /**
     * Returns breadcrumbs for specific end-point category
     *
     * @param int $id Category ID
     * @param string $crumbsPrefix Breadcrumbs base prefix
     * @param array  $add_to_array Data to add to end of array
     * @param string $bc_name      Add breadcrumbs full name to first array element 
     *
     * @return array Each item: [name => 'Category name', url => 'Category url']
     */
    public static function breadcrumbs($id, $crumbsPrefix = '/category/', $add_to_array = FALSE, $bc_name = FALSE) {
        $crumbs = array();

        $category_id = (int)$id;
        $bc_name_result = '';

        do {
            $category = DB::get_row('SELECT name, is_tag, IF(url =  "", id, url) AS url, parent_1 FROM rk_categories WHERE id = ' . $category_id);

            $name = $category['is_tag'] ? helpers::mb_ucfirst($category['name']) : $category['name'];

            $crumbs[] = array(
                'name' => $name,
                'url' => $category['url'],
            );

            $category_id = $category['parent_1'] ? (int)$category['parent_1'] : 0;

            if($bc_name)
            {
                $bc_name_result .= $name . ' / ';
            }

        } while ($category_id);

        $crumbs = array_reverse($crumbs);

        $crumbs[0]['url'] = $crumbsPrefix . $crumbs[0]['url'];

        if($bc_name)
        {
            $crumbs[0]['bc_name'] = $bc_name_result;
        }
        
        for ($i = 1, $n = count($crumbs); $i < $n; ++$i) {
            $crumbs[$i]['url'] = $crumbs[$i - 1]['url'] . '/' . $crumbs[$i]['url'];
        }

        if($add_to_array) {
            for ($i=0; $i < count($add_to_array); $i++) { 
                $crumbs[] = $add_to_array[$i];
            }
        }

        return $crumbs;
    }

    /**
     * Returns subcategories
     *
     * @param int $category_id Parent category ID
     * @param int $level Subcategories level
     * @param int $trademark_id Trademark ID. If not specified - all subcategories will be displayed
     *
     * @return array
     */
    public static function subcategories($category_id, $level, $trademark_id = null)
    {
        $category_id = (int)$category_id;
        $level = (int)$level;

        $unsort_cat = array();

        $query = "SELECT s.id, s.name
                             FROM rk_subcategories s, rk_categories c, rk_cat_subcat cs
                             WHERE c.id = cs.cat_id
                             AND   cs.subcat_id = s.id
                             AND   c.level = '$level'
                             AND   s.level = '$level'
                             AND   c.parent_1 = $category_id
                             AND   c.is_tag = 0
                             AND   c._has_products = 1
                             GROUP BY s.id";

        $subcategories = DB::get_rows($query);

        foreach ($subcategories as $key => $sub) {
            //  get needed level for subcat
            $query = "SELECT c.id, c.name, IF(c.url =  '', c.id, c.url ) url, c._full_url
                                       FROM rk_subcategories s, rk_categories c, rk_cat_subcat cs
                                       WHERE   s.id  = {$sub['id']}
                                       AND     c.id = cs.cat_id
                                       AND     cs.subcat_id = s.id
                                       AND     c.level = '$level'
                                       AND     s.level = '$level'
                                       AND     c.parent_1 = $category_id
                                       AND     c.is_tag = 0
                                       AND     c._has_products = 1
                                       AND     c.visible = 1 " .
                                        ($trademark_id ? "
                                        AND c.id IN (SELECT tc.cat_id FROM rk_tm_in_cat AS tc WHERE tc.tm_id = $trademark_id AND tc._has_products = 1)
                                        " : '') .
                                       "GROUP BY c.id
                                       ORDER BY c.name
                                       ";

            $categories = DB::get_rows($query);

            if ($categories) {
                $unsort_cat[$key] = array(
                    'sub_name' => $sub['name'],
                    'quanity' => count($categories),
                    'cats' => helpers::explode_filters($categories, 15),
                );
            }
        }

        $cat_quan_array = array();

        foreach ($unsort_cat as $key => $value) {
            $cat_quan_array[$key] = $value['quanity'];
        }

        array_multisort($cat_quan_array, SORT_NUMERIC, $unsort_cat);

        return $unsort_cat;
    }

    /**
     * Returns category tags
     *
     * @param int $category_id Parent category ID
     *
     * @return array
     */
    public static function tags($category_id)
    {
        return DB::get_rows("SELECT c.name, c._full_url,
                         cs.tag_name
                     FROM rk_categories AS c
                     LEFT JOIN rk_cat_seo AS cs ON cs.id = c.id
                     WHERE c.parent_1 = $category_id
                     AND   c.visible = 1
                     AND   c._has_products = 1
                     AND   c.is_tag = 1");
    }

    /**
     * Returns subcategories HTML
     *
     * @param int $category_id Parent category ID
     * @param int $level Subcategories level
     * @param int $level_3_id Current 3rd level category ID
     *
     * @return array ['subcategories HTML', '3rd level crumbs (Delimited with /)']
     */
    private static function _subcategories_html($category_id, $level, $level_3_id = 0)
    {
        $generate = function ($category_id, $level, $level_3_id) {
            $subcategories = self::subcategories($category_id, $level);

            $last_crumb = null;

            if ($level_3_id) {
                for ($i = 0, $n = count($subcategories); $i < $n; ++$i) {
                    for ($y = 0, $m = count($subcategories[$i]['cats']); $y < $m; ++$y) {
                        for ($z = 0, $l = count($subcategories[$i]['cats'][$y]); $z < $l; ++$z) {
                            if ($subcategories[$i]['cats'][$y][$z]['id'] == $level_3_id) {
                                $last_crumb = html_entity_decode(' / ' . $subcategories[$i]['sub_name'] . ' / ' . $subcategories[$i]['cats'][$y][$z]['name'], ENT_QUOTES, 'UTF-8');

                                break;
                            }
                        }
                    }
                }
            }

            return [
                Template::get_tpl(
                    'subcategories',
                    null,
                    [
                        'categories' => $subcategories,
                        'category_id' => $category_id,
                        'level_3_id' => $level_3_id,
                    ]
                ),
                $last_crumb,
            ];
        };

        return $level === 2 ? cache_item('subcategories-' . $category_id, function () use ($category_id, $level, $level_3_id, $generate) {
            return $generate($category_id, $level, $level_3_id);
        }, 10800) /* 3 hours */ : $generate($category_id, $level, $level_3_id);
    }

    /**
     * Returns category tags HTML
     *
     * @param int $category_id Category ID
     * @param int $level Category level
     *
     * @return string
     */
    private static function _tags_html($category_id, $level)
    {
        $generate = function ($category_id) {
            return Template::get_tpl('category_tags', null, array(
                'tags' => self::tags($category_id),
            ));
        };

        return $level === 1 ? cache_item('tags-' . $category_id, function () use ($category_id, $level, $generate) {
            return $generate($category_id, $level);
        }, 10800) /* 3 hours */ : $generate($category_id, $level);
    }

    /**
     * Returns category filter's HTML
     *
     * @param int $category_id Category ID
     * @param string $category_name Category name
     * @param int $level Category level
     * @param string $categories_html Subcategories list HTML
     * @param array $filterTms Arranged for filter trademarks list
     * @param array $filterPrices Filter price range
     * @param array $filterCountries Arranged for filter countries list
     * @param array $allowedFilters Allowed parent filter IDs. If null - all filters allowed
     *
     * @return string
     */
    private static function _filters_html($category_id, $category_name, $level, $categories_html, $filterTms, $filterPrices, $filterCountries, $allowedFilters)
    {
        $generate = function () use ($category_id, $category_name, $level, $categories_html, $filterTms, $filterPrices, $filterCountries, $allowedFilters) {
            $filters = [];

            $joins = self::product_joins();

            $query = 'SELECT id, name FROM rk_filters WHERE parent_id = 0 AND visible = 1 ' . ($allowedFilters !== null ? 'AND id IN (' . implode(', ', $allowedFilters) . ') ' : '') . 'ORDER BY name';

            $main_filters = DB::get_rows($query);

            $innerJoins = 'INNER JOIN ' . $joins[$level] . ' AS pc ON pc.cat_id = ' . $category_id;

            for ($i = 0, $n = count($main_filters); $i < $n; ++$i) {
                $query = 'SELECT  f.id, f.name
                            FROM rk_prod_filter AS pf, rk_filters AS f
                            ' . $innerJoins . '
                            WHERE  pf.filter_id = f.id
                            AND    pc.prod_id = pf.prod_id
                            AND    f.visible = 1
                            AND    f.parent_id = ' . $main_filters[$i]['id'] . '
                            GROUP BY f.id
                            ORDER BY f.name';

                $filtersRaw = DB::get_rows($query);

                if ($filtersRaw) {
                    $filters[$main_filters[$i]['name']] = helpers::explode_filters($filtersRaw);
                }
            }

            return Template::get_tpl('product_filter_wrap', null, array(
                'filters' => $filters,
                'filter_tms' => $filterTms,
                'filter_prices' => $filterPrices,
                'filter_countries' => $filterCountries,
                'name' => $category_name,
                'categories_html' => $categories_html,
                'found' => Pagination::getCount(),
                'sort_popularity' => true,
            ));
        };

        return $level === 1 ? cache_item((TM_FOR_PROF ? 'p_filters-' : 'filters-') . $category_id, $generate, 10800) /* 3 hours */ : $generate();
    }

    /**
     * Parses url and retrieves category's information
     *
     * @param array $args Controller's function arguments
     * @param string $redirectBase Redirect base URL (With both leading and trailing slashes)
     * @param bool $returnNames Return category names
     *
     * @return array ['ids' => [id1, id2, id3], 'urls' => [url1, url2, url3], 'names' => [`Level 1 name`, `Level 2 name`, `Level 3 name`]
     */
    public static function parse_url($args, $redirectBase = '/category/', $returnNames = false)
    {
        if (empty($args[0])) {
            return null;
        }

        //  parameters
        $item_1 = $item_2 = $item_3 = $item_4 = $redirect_1 = $redirect_2 = $redirect_3 = $redirect_4 = $id_1 = $id_2 = $id_3 = $id_4 = false;

        $item_1 = $args[0];

        if (is_numeric($item_1)) {
            $id_1 = (int)$item_1;
            $redirect_1 = $id_1 > 0 ? self::redirect($id_1) : false;

            if (!$redirect_1) {
                return null;
            }
        } else {
            if (!get_magic_quotes_gpc()) {
                $item_1 = DB::mysql_secure_string($item_1);
            }

            $id_1 = self::get_id_by_url($item_1, 0);
        }

        if (!$id_1) {
            return null;
        }

        //  item of level 2
        //!!!  Добавить проверку для 2-го уровня страна или категория
        if (isset($args[1])) {
            $item_2 = $args[1];

            if (is_numeric($item_2)) {
                $id_2 = (int)$item_2;
                $redirect_2 = $id_2 > 0 ? self::redirect($id_2) : false;
            } else {
                if (!get_magic_quotes_gpc()) {
                    $item_2 = DB::mysql_secure_string($item_2);
                }

                if ($id_1) {
                    $id_2 = self::get_id_by_url($item_2, $id_1);
                }
            }

            if (!$id_2) {
                return null;
            }
        }

        //  item of level 3
        //!!!  Добавить проверку для 2-го уровня страна или категория
        if (isset($args[2])) {
            $item_3 = $args[2];

            if (is_numeric($item_3)) {
                $id_3 = (int)$item_3;
                $redirect_3 = $id_3 > 0 ? self::redirect($id_3) : false;
            } else {
                if (!get_magic_quotes_gpc()) {
                    $item_3 = DB::mysql_secure_string($item_3);
                }

                if ($id_1 && $id_2) $id_3 = self::get_id_by_url($item_3, $id_2);
            }

            if (!$id_3) {
                return null;
            }
        }

        //  country in listing
        //!!!  Написать процедура подтягивания страны по 4-му параметру

        //  redirect
        if ($redirectBase && ($redirect_1 || $redirect_2 || $redirect_3)) {
            redirect($redirectBase . ($redirect_1 ? $redirect_1 : $item_1) . '/' . ($redirect_2 ? $redirect_2 : $item_2) . '/' . ($redirect_3 ? $redirect_3 : $item_3));
        }

        return [
            'ids' => [
                $id_1,
                $id_2,
                $id_3,
            ],
            'urls' => [
                $item_1,
                $item_2,
                $item_3,
            ],
        ] + (
            $returnNames
                ? [
                    'names' => [
                        DB::get_field('name', 'SELECT IF(c.h1 = "" OR c.h1 IS NULL, c.name, c.h1) AS name FROM rk_categories AS c WHERE c.id = ' . $id_1),
                        $id_2 ? DB::get_field('name', 'SELECT IF(c.h1 = "" OR c.h1 IS NULL, c.name, c.h1) AS name FROM rk_categories AS c WHERE c.id = ' . $id_2) : null,
                        $id_3 ? DB::get_field('name', 'SELECT IF(c.h1 = "" OR c.h1 IS NULL, c.name, c.h1) AS name FROM rk_categories AS c WHERE c.id = ' . $id_3) : null,
                    ],
                ]
                : []
        );
    }

    /**
     * Returns HTML for found categories
     *
     * @param array $categories Categories rows
     *
     * @return string
     */
    public static function search_categories_html($categories)
    {
        return Template::get_tpl(
            '2016/search_categories',
            null,
            [
                'multicol' => true,
                'categories' => helpers::explode_filters($categories, 5),
            ]
        );
    }

    /**
     * Updates category title, description
     *
     * @param int $categoryID Category ID
     * @param string $h1 h1 value
     * @param string $title Title value
     * @param string $description Description value
     * @param string $keywords Keywords value
     * @param string $pageText Page text
     * @param string $tagAnchor Tag anchor value
     * @param string $tagName Tag name
     * @param int $nameOnProductsN Count of first products to display category name on category page
     *
     * @return bool
     */
    public static function update_seo($categoryID, $h1, $title, $description, $keywords, $pageText, $tagAnchor = null, $tagName = null, $nameOnProductsN = 0)
    {
        return !!DB::query('UPDATE rk_categories SET h1 = "' . DB::escape($h1) . '"' . ($tagAnchor !== null ? ', name = "' . DB::escape($tagAnchor) . '"' : '') . ' WHERE id = ' . (int)$categoryID)
        && !!DB::query('UPDATE rk_cat_desc SET `desc` = "' . DB::escape($pageText) . '" WHERE id = ' . (int)$categoryID)
        && !!DB::query('UPDATE rk_cat_seo SET title = "' . DB::escape($title) . '", description = "' . DB::escape($description) . '", keywords = "' . DB::escape($keywords) . '"' . ($tagName !== null ? ', tag_name = "' . DB::escape($tagName) . '"' : '') . ', name_on_products = ' . ((int)$nameOnProductsN) . ' WHERE id = ' . (int)$categoryID);
    }

    /**
     * Creates trademark links to category tag if not created
     *
     * @param int $categoryID Category (Tag) ID
     * @param array $tmIDs Array of trademark IDs to be linked to given category
     */
    public static function update_tag_tm_ids($categoryID, $tmIDs)
    {
        $categoryID = (int)$categoryID;
        $savedTms = [];

        for ($i = 0, $n = count($tmIDs); $i < $n; ++$i) {
            $savedTms[] = (int)$tmIDs[$i];

            if (!DB::get_row('SELECT tm_id FROM rk_tm_in_cat WHERE cat_id = ' . $categoryID . ' AND tm_id = ' . (int)$tmIDs[$i])) {
                DB::query('INSERT INTO rk_tm_in_cat (cat_id, tm_id) VALUES (' . $categoryID . ', ' . ((int)$tmIDs[$i]) . ')');
            }
        }

        $savedIDsSQL = implode(', ', $savedTms);

        DB::query('DELETE FROM rk_tm_in_cat WHERE cat_id = ' . $categoryID . ($savedIDsSQL ? ' AND tm_id NOT IN (' . $savedIDsSQL . ')' : ''));
    }

    /**
     * Returns all tags
     *
     * @param bool $productsCount Include products count in "products" field
     * @param bool $sortByProductsDirection Sort by "products" field direction
     *
     * @return array
     */
    public static function get_tags($productsCount = false, $sortByProductsDirection = null)
    {
        return DB::get_rows(
            'SELECT * FROM (
                SELECT c.id, c.name, c.url, c._full_url, c.level, c.visible' . ($productsCount ? ', (SELECT COUNT(pc.prod_id) FROM rk_prod_cat AS pc WHERE pc.cat_id = c.id) AS products' : '') . '
                FROM rk_categories AS c
                WHERE c.is_tag = 1
            ) AS c
            ORDER BY ' . ($sortByProductsDirection !== null ? ('c.products ' . ($sortByProductsDirection ? 'ASC' : 'DESC')) : 'c.level')
        );
    }

    /**
     * Deletes all products from category
     *
     * @param int $categoryID Category ID
     */
    public static function clear_products($categoryID)
    {
        DB::query('DELETE FROM rk_prod_cat WHERE cat_id = ' . (int)$categoryID);

        if (DB::last_query_info()) {
            optimization::del_link_tables();
            optimization::add_link_tables();
        }
    }

    /**
     * Toggles "visible" flag for category
     *
     * @param int $categoryID Category ID
     */
    public static function toggle_visibility($categoryID)
    {
        DB::query('UPDATE rk_categories SET visible = IF(visible = 1, 0, 1) WHERE id = ' . (int)$categoryID);
    }

    /**
     * Returns tags tree
     *
     * @param bool $productsCount Include products count in 'products' field
     *
     * @return array
     */
    public static function get_tags_tree($productsCount = false)
    {
        $tags = [];

        DB::get_rows(
            'SELECT c.id, c.name, c.url, c._full_url, c.level, c.is_tag' . ($productsCount ? ', (SELECT COUNT(pc.prod_id) FROM rk_prod_cat_lvl_1 AS pc WHERE pc.cat_id = c.id) AS products' : '') . '
            FROM rk_categories AS c
            WHERE (
                c.is_tag = 1
                OR
                (
                    SELECT COUNT(ct.id)
                    FROM rk_categories AS ct
                    WHERE ct.is_tag = 1 AND (
                        ct.parent_1 = c.id
                        OR
                        ct.parent_2 = c.id
                    )
                ) > 0
            )
                AND c.level = "1"',
            function ($tag1) use (&$tags, $productsCount) {
                $tag1['children'] = [];

                DB::get_rows(
                    'SELECT c.id, c.name, c.url, c._full_url, c.level, c.is_tag' . ($productsCount ? ', (SELECT COUNT(pc.prod_id) FROM rk_prod_cat_lvl_2 AS pc WHERE pc.cat_id = c.id) AS products' : '') . '
                    FROM rk_categories AS c
                    WHERE (
                        c.is_tag = 1
                        OR
                        (
                            SELECT COUNT(ct.id)
                            FROM rk_categories AS ct
                            WHERE ct.is_tag = 1 AND ct.parent_1 = c.id
                        ) > 0
                    )
                        AND c.level = "2"
                        AND c.parent_1 = ' . $tag1['id'],
                    function ($tag2) use (&$tag1, $productsCount) {
                        $tag2['children'] = [];

                        $tag2['children'] = DB::get_rows(
                            'SELECT c.id, c.name, c.url, c._full_url, c.level, c.is_tag' . ($productsCount ? ', (SELECT COUNT(pc.prod_id) FROM rk_prod_cat AS pc WHERE pc.cat_id = c.id) AS products' : '') . '
                            FROM rk_categories AS c
                            WHERE c.is_tag = 1
                                AND c.parent_1 = ' . $tag2['id'] . '
                                AND c.level = "3"'
                        );

                        $tag1['children'][] = $tag2;
                    }
                );

                $tags[] = $tag1;
            }
        );

        return $tags;
    }

    /**
     * Adds products to category
     *
     * @param int $categoryID Category ID
     * @param array $productIDs Product IDs
     */
    public static function add_products($categoryID, $productIDs)
    {
        $categoryID = (int)$categoryID;

        for ($i = 0, $n = count($productIDs); $i < $n; ++$i) {
            $pID = (int)$productIDs[$i];

            if ($pID > 0) {
                DB::query('INSERT INTO rk_prod_cat (prod_id, cat_id) VALUES (' . $pID . ', ' . $categoryID . ') ON DUPLICATE KEY UPDATE prod_id = prod_id');
            }
        }
    }

    /**
     * Returns linked to category products
     *
     * @param int $categoryID Category ID
     *
     * @return array
     */
    public static function linked_products($categoryID)
    {
        return DB::get_rows(
            'SELECT p.id, p.name, p.pack, p.short_description, pd.ingredients
            FROM products AS p
            INNER JOIN rk_prod_desc AS pd ON pd.id = p.id
            INNER JOIN rk_prod_cat AS pc ON pc.prod_id = p.id AND pc.cat_id = ' . ((int)$categoryID)
        );
    }

    /**
     * Removes products from category
     *
     * @param int $categoryID Category ID
     * @param array $productIDs Product IDs
     */
    public static function remove_products($categoryID, $productIDs)
    {
        $categoryID = (int)$categoryID;

        for ($i = 0, $n = count($productIDs); $i < $n; ++$i) {
            DB::query('DELETE FROM rk_prod_cat  WHERE cat_id = ' . $categoryID . ' AND prod_id = '. ((int)$productIDs[$i]));
        }
    }

    /**
     * Copies products from tags
     *
     * @param int $categoryID Category ID
     * @param array $tagIDs Tag IDs
     */
    public static function copy_from_tags($categoryID, $tagIDs)
    {
        $categoryID = (int)$categoryID;

        $category = self::get_by_id($categoryID);

        if ($category) {
            $joins = self::product_joins();

            $table = $category['is_tag'] ? $joins[3] : $joins[$category['level']];

            for ($i = 0, $n = count($tagIDs); $i < $n; ++$i) {
                DB::query('INSERT INTO ' . $table . ' (cat_id, prod_id) SELECT ' . $categoryID . ', pc.prod_id FROM rk_prod_cat AS pc WHERE pc.cat_id = ' . ((int)$tagIDs[$i]) . ' ON DUPLICATE KEY UPDATE ' . $table . '.cat_id = ' . $table . '.cat_id');
            }
        }
    }
}
