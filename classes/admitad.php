<?php

/**
 * Work with admitad
 */
class Admitad
{

    /**
     * js admitad code for index page
     * @return string js code in string
     */
    static function index_page()
    {
        return " window._retag = window._retag || [];window._retag.push({code: \"9ce8886d63\", level: 0});(function(){
                var id=\"admitad-retag\";
                if (document.getElementById(id)) {return;}
                var s=document.createElement(\"script\");
                s.async=true; s.id=id;
                var r=(new Date).getDate();
                s.src=(document.location.protocol == \"https:\" ? \"https:\" : \"http:\") + \"//cdn.trmit.com/static/js/retag.min.js?r=\"+r;
                var a = document.getElementsByTagName(\"script\")[0]
                a.parentNode.insertBefore(s, a);
                })()";
    }

    /**
     * js admitad code for category page
     * @param  int $cat_id_1 ID category lvl 1
     * @param  int $cat_id_2 ID category lvl 2
     * @param  int $cat_id_3 ID category lvl 3
     * @return string  js code in string
     */
    static function category_page($cat_id_1, $cat_id_2, $cat_id_3)
    {
        $cat_id = $cat_id_1;

        if($cat_id_2) {
            $cat_id = $cat_id_2;
        }

        if($cat_id_3) {
            $cat_id = $cat_id_3;
        }

        return " window.ad_category=\"$cat_id\";
                window._retag=window._retag || [];
                window._retag.push({code: \"9ce8886ade\", level: 1});
                (function () {
                var id = \"admitad-retag\";
                if (document.getElementById(id)) {return;}
                var s=document.createElement(\"script\");
                s.async = true; s.id = id;
                var r = (new Date).getDate();
                s.src = (document.location.protocol == \"https:\" ? \"https:\" : \"http:\") + \"//cdn.trmit.com/static/js/retag.min.js?r=\"+r;
                var a = document.getElementsByTagName(\"script\")[0]
                a.parentNode.insertBefore(s, a);
                })()";
    }

    /**
     * js admitad code for product page
     * @param  array $product product info
     * @return string  js code in string
     */
    static function product_page($product)
    {
        $category = '';

        if (isset($product['categories']['lvl_2'][0]['id'])) {
            $category = $product['categories']['lvl_2'][0]['id'];
        }

        return " window.ad_product = {
                \"id\": \"{$product['id']}\",
                \"vendor\": \"{$product['tm']}\",
                \"price\": \"" . ((int)$product['special_price'] > 0 ? $product['special_price'] : $product['price']) . "\",
                \"url\": \"" . DOMAIN_FULL . "/product/{$product['url']}\",
                \"picture\": \"" . IMAGES_DOMAIN_FULL . "/images/prod_photo/" . Product::img_url($product['src'], $product['url']) . "\",
                \"name\": \"{$product['name']}\",
                \"category\": \"$category\",};window._retag = window._retag || [];
                window._retag.push({code: \"9ce8886adf\", level: 2});
                (function () {
                var id=\"admitad-retag\";
                if (document.getElementById(id)) {return;}
                var s=document.createElement(\"script\");
                s.async = true; s.id = id;
                var r = (new Date).getDate();
                s.src = (document.location.protocol == \"https:\" ? \"https:\" : \"http:\") + \"//cdn.trmit.com/static/js/retag.min.js?r=\"+r;
                var a = document.getElementsByTagName(\"script\")[0]
                a.parentNode.insertBefore(s, a);})()";
    }

    /**
     * js admitad code for cart page
     * @param  array $cart cart info
     * @return string js code in string
     */
    static function cart_page($cart)
    {
        $return = ' window.ad_products = [';

        foreach ($cart as $product) {
            $return .= "{\"id\": \"{$product['id']}\", \"number\": \"{$product['quantity']}\",},";
        }

        $return .= "];window._retag = window._retag || [];
                    window._retag.push({code: \"9ce8886adc\", level: 3});
                    (function () {
                    var id = \"admitad-retag\";
                    if (document.getElementById(id)) {return;}
                    var s = document.createElement(\"script\");
                    s.async = true; s.id = id;
                    var r = (new Date).getDate();
                    s.src = (document.location.protocol == \"https:\" ? \"https:\" : \"http:\") + \"//cdn.trmit.com/static/js/retag.min.js?r=\"+r;
                    var a = document.getElementsByTagName(\"script\")[0]
                    a.parentNode.insertBefore(s, a);})()";

        return $return;
    }

    /**
     * Generate admitad tracking code for order
     * @param  int    $order_number Order number
     * @param  array  $order_items  array of order items
     * @param  string $order_price  order price
     * @param  string $admitad_uid  Admitad user id
     * @param string $promo_code Promo code
     *
     * @return string               HTML + JS code 
     */
    static function ok_page($order_number, $order_items, $order_price, $admitad_uid, $promo_code = null)
    {

        $action_code = self::choice_tariff($order_number, $admitad_uid);

        $return = array('admitad_code' => '',
        'admitad_counter' => FALSE);

        $position_count = count($order_items);
        $retag = " window.ad_order = \"$order_number\";
            window.ad_amount = \"$order_price\";
            window.ad_products = [";

        $prPixel = !$admitad_uid ? '(function (d, w) {
            w._prPixel = {
            response_type: "img",
            action_code: "'. $action_code . '",
            campaign_code: "254166d1c0"
            };
            w._prPositions = w._prPositions || [];' : '';

        $admitad_counter = '';

        $img_code = '<noscript>';

        foreach ($order_items as $key => $product) {
            $retag .= "{\"id\": \"{$product['id']}\",
            \"number\": \"{$product['quantity']}\",},";

            $position_id = $key + 1;
            $admitad_counter .= "w._admitadPositions.push({uid: '" . ($admitad_uid ?: '') . "', tariff_code: '1', order_id: '$order_number', position_id: '$position_id', currency_code: 'RUB', position_count: '$position_count', price: '{$product['price']}', quantity: '{$product['quantity']}', product_id: '{$product['id']}', payment_type: 'sale'" . (!$admitad_uid ? ", promocode: '" . $promo_code . "'" : '') . "});";
            $img_code .= "<img src=\"//ad.admitad.com/r?campaign_code=254166d1c0&action_code=$action_code&payment_type=sale&response_type=img&uid=$admitad_uid&tariff_code=1&order_id=$order_number&position_id=$position_id&currency_code=RUB&position_count=$position_count&price={$product['price']}&quantity={$product['quantity']}&product_id={$product['id']}" . ($promo_code ? "&promocode=" . $promo_code : '') . "\" width=\"1\" height=\"1\" alt=\"\">";

            if (!$admitad_uid) {
                $prPixel .= " w._prPositions.push({
                    tariff_code: '1',
                    order_id: '{$order_number}',
                    position_id: '{$position_id}',
                    currency_code: 'RUB',
                    position_count: '{$position_count}',
                    price: '{$product['price']}',
                    quantity: '{$product['quantity']}',
                    product_id: '{$product['id']}',
                    payment_type: 'sale',
                    promocode: '{$promo_code}'
                });";
            }
        }

        $retag .= "];window._retag = window._retag || [];
            window._retag.push({code: \"9ce8886add\", level: 4});
            (function () {
            var id = \"admitad-retag\";
            if (document.getElementById(id)) {return;}
            var s = document.createElement(\"script\");
            s.async = true; s.id = id;
            var r = (new Date).getDate();
            s.src = (document.location.protocol == \"https:\" ? \"https:\" : \"http:\") + \"//cdn.trmit.com/static/js/retag.min.js?r=\"+r;
            var a = document.getElementsByTagName(\"script\")[0]
            a.parentNode.insertBefore(s, a);})();";

        $img_code .= '</noscript>';

        if (!$admitad_uid) {
            $prPixel .= " var id = '_promocode-pixel';
            if (d.getElementById(id)) { return; }
            var s = d.createElement('script');
            s.id = id;
            var r = (new Date).getTime();
            var protocol = (d.location.protocol === 'https:' ? 'https:' : 'http:');
            s.src = protocol + '//cdn.bamitdo.com/static/js/prpixel.min.js?r=' + r;
            var head = d.getElementsByTagName('head')[0];
            head.appendChild(s);
            })(document, window);";
        }

        $admitad_counter = "<script type=\"text/javascript\">{$prPixel}(function (d, w){w._admitadPixel={response_type: 'img', action_code: '$action_code', campaign_code: '254166d1c0'};w._admitadPositions = w._admitadPositions || [];" .
            $admitad_counter . "var id='_admitad-pixel';if (d.getElementById(id)){return;}var s=d.createElement('script');s.id=id;var r=(new Date).getTime();var protocol=(d.location.protocol === 'https:' ? 'https:' : 'http:');s.src=protocol+'//cdn.asbmit.com/static/js/pixel.min.js?r=' + r;var head=d.getElementsByTagName('head')[0];head.appendChild(s);})(document, window);</script>" . $img_code;

        $return['admitad_code'] = $retag;

        if ($admitad_uid || $promo_code) {
            $return['admitad_counter'] = $admitad_counter;
        }

        return $return;
    }

    /**
     * Generate admitad tracking code for order
     * @param  int    $order_number Order number
     * @param  array  $order_items  array of order items
     * @param  string $order_price  order price
     * @param  string $admitad_uid  Admitad user id
     * @param  string $promo_code Promo code
     *
     * @return string               HTML + JS code 
     */
    static function ok_page_new($order_number, $order_items, $admitad_uid) {

        $action_code = self::choice_tariff($order_number, $admitad_uid);

        $admitad_counter = "ADMITAD = window.ADMITAD || {};
                            ADMITAD.Invoice = ADMITAD.Invoice || {};
                            ADMITAD.Invoice.broker = 'adm';
                            ADMITAD.Invoice.category = '$action_code';
                            var orderedItem = [];";


        foreach ($order_items as $key => $product) {

            $admitad_counter .= "orderedItem.push({Product: {productID: '{$product['id']}', category: '$action_code', price: '{$product['price']}', priceCurrency: 'RUB',}, orderQuantity: '{$product['quantity']}', additionalType: 'sale'});";

        }



        $admitad_counter .= "ADMITAD.Invoice.referencesOrder = ADMITAD.Invoice.referencesOrder || [];
                             ADMITAD.Invoice.referencesOrder.push({orderNumber: '{$order_number}', orderedItem: orderedItem});";    

        return $admitad_counter;
    }

    /**
     * Add info about admitad orders
     * @param int $order_id    order ID
     * @param string $admitad_uid admitad user ID
     * @param int $promo_code_id Promo code ID
     *
     * @return bool|mysqli_result
     */
    public static function add_admitad_order($order_id, $admitad_uid, $promo_code_id)
    {
        return DB::query("INSERT INTO rk_admitad_orders (order_id, admitad_uid, promo_code_id) VALUES ('$order_id', '$admitad_uid', " . ($admitad_uid ? 0 : $promo_code_id) . ")");
    }

    /**
     * Returns admitad promo code row
     *
     * @param string $code Promo code value
     *
     * @return array
     */
    public static function get_promo_code($code)
    {
        return DB::get_row('SELECT * FROM rk_admitad_promo_codes AS c WHERE c.code = "' . DB::mysql_secure_string($code) . '" LIMIT 1');
    }

    /**
     * Set code is use, and set use date
     *
     * @param string $code $code Users code
     * @param int $id Users code id
     *
     * @return bool|mysqli_result
     */
    public static function set_promo_code_is_use($code, $id)
    {
        $query = "UPDATE rk_admitad_promo_codes
                  SET use_date = NOW(), `use` = 1
                  WHERE code = '$code'
                  AND   id = $id";

        return DB::query($query);
    }

    /**
     * Choice tariff  action code for report
     * @param  int    $order_number Order number
     * @param  string $admitad_uid  Admitad user id
     * @return int                  Number of tariff code
     */
    private static function choice_tariff($order_number, $admitad_uid)
    {
        $order_number = (int)$order_number;

        // new user tariff code
        $action_code = 1;
        $user_id = 0;

        if ((int)DB::get_field('order_id', 'SELECT order_id FROM rk_admitad_orders WHERE admitad_uid = "' . DB::mysql_secure_string($admitad_uid) . '" AND order_id != ' . $order_number) > 0){
            $action_code = 2;
        }  else {
            $user_id = (int)DB::get_field('client_id', "SELECT client_id FROM orders WHERE order_number = $order_number");

            if ($user_id == 0) {
                $user_info = DB::get_row('SELECT p.client_id, p.email, p.phone FROM orders AS o INNER JOIN shipping_persons AS p ON p.id = o.shipping_person_id WHERE o.order_number = ' . $order_number);

                if ((int)$user_info['client_id'] > 0) {
                    $user_id = $user_info['client_id'];
                } else {

                    if (!is_null($user_info['email']) && $user_info['email']!= '') {
                        $user_id = (int)DB::get_field('id', "SELECT id FROM clients WHERE email = '{$user_info['email']}' LIMIT 1");
                    }

                    if($user_id == 0 && !is_null($user_info['phone']) && $user_info['phone'] != '') {
                            $user_id = (int)DB::get_field('id', "SELECT id FROM clients WHERE phone = '{$user_info['phone']}' LIMIT 1");
                    }

                }
          
            }

        }

        if ($user_id > 0 && $action_code == 1) {

            if ((int)DB::get_field('total', "SELECT COUNT(o.order_number) AS total FROM orders o WHERE o.order_number != '$order_number' AND o.client_id = $user_id") > 0) {
                $action_code = 2;
            }
        }

        return $action_code;
    }

    /**
     * Returns order's promo code by order number
     *
     * @param int $order_number Order number
     *
     * @return array
     */
    public static function get_code_by_order_number($order_number)
    {
        return DB::get_row('SELECT p.id, p.code FROM rk_admitad_promo_codes AS p WHERE p.id = (SELECT od.coupon_id FROM orders AS o INNER JOIN order_discounts AS od ON od.order_number = o.order_number WHERE o.order_number = ' . ((int)$order_number) . ' AND od.admitad_coupon = 1)');
    }
}
