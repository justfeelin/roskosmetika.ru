<?php

/**
 * Функции необходимые для редиректа.
 */
class redirect
{

    static function fill_in($args)
    {

        $query = 'SELECT id, name
                  FROM rk_categories
                  WHERE parent_id != 0';

        $data = DB::get_rows($query);

        foreach ($data as $value) {

            $query = "SELECT COUNT(member_id) AS kol
                         FROM cat_url
                         WHERE member_id = {$value['id']}
                         AND type = 'cat'";

            $kol = (int)DB::get_field('kol', $query);

            if ($kol == 0) {
                $name = DB::mysql_secure_string($value['name']);
                $query = "INSERT INTO cat_url (id, member_id, member_name, url, type) VALUES ('', {$value['id']}, '$name', '', 'cat')";
                DB::query($query);
            }
        }


        $query = 'SELECT discr_id, name
                  FROM prod';

        $data = DB::get_rows($query);

        foreach ($data as $value) {

            $query = "SELECT COUNT(member_id) AS kol
                         FROM cat_url
                         WHERE member_id = {$value['discr_id']}
                         AND type = 'prod'";

            $kol = (int)DB::get_field('kol', $query);


            if ($kol == 0) {
                $name = DB::mysql_secure_string($value['name']);
                $query = "INSERT INTO cat_url (id, member_id, member_name, url, type) VALUES ('', {$value['discr_id']}, '$name', '', 'prod')";
                DB::query($query);
            }
        }

    }


}

?>
