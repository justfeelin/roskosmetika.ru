<?php

abstract class helpers
{
    /**
     * Prepositional grammatical case ID
     */
    const CASE_TYPE_PREPOSITIONAL = 1;

    /**
     * Prepositional (With pretext) grammatical case ID
     */
    const CASE_TYPE_PREPOSITIONAL_EXTENDED = 2;

    /**
     * Parental grammatical case ID
     */
    const CASE_TYPE_PARENTAL = 3;

    /**
     * Accusative (Винительный) grammatical case ID
     */
    const CASE_TYPE_ACCUSATIVE = 4;

    /**
     * Nominative grammatical case ID
     */
    const CASE_TYPE_NOMINATIVE = 5;

    /**
     * Cyrillic letters
     */
    const RUS_LETTERS = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя';

    /**
     * Roskosmetika technical user ID
     */
    const RK_USER_ID = 9999;

    private static $_1_2 = [
        1 => 'одна ',
        2 => 'две ',
    ];

    private static $_1_19 = [
        1 => 'один ',
        2 => 'два ',
        3 => 'три ',
        4 => 'четыре ',
        5 => 'пять ',
        6 => 'шесть ',
        7 => 'семь ',
        8 => 'восемь ',
        9 => 'девять ',
        10 => 'десять ',
        11 => 'одиннацать ',
        12 => 'двенадцать ',
        13 => 'тринадцать ',
        14 => 'четырнадцать ',
        15 => 'пятнадцать ',
        16 => 'шестнадцать ',
        17 => 'семнадцать ',
        18 => 'восемнадцать ',
        19 => 'девятнадцать ',
    ];

    private static $_des = [
        2 => 'двадцать ',
        3 => 'тридцать ',
        4 => 'сорок ',
        5 => 'пятьдесят ',
        6 => 'шестьдесят ',
        7 => 'семьдесят ',
        8 => 'восемдесят ',
        9 => 'девяносто ',
    ];

    private static $_hang = [
        1 => 'сто ',
        2 => 'двести ',
        3 => 'триста ',
        4 => 'четыреста ',
        5 => 'пятьсот ',
        6 => 'шестьсот ',
        7 => 'семьсот ',
        8 => 'восемьсот ',
        9 => 'девятьсот ',
    ];

    /**
     * Returns string representation of price
     *
     * @param int|float $price Price
     *
     * @return string
     */
    public static function priceToString($price)
    {
        $namerub = [
            1 => 'рубль ',
            2 => 'рубля ',
            3 => 'рублей ',
        ];

        $nametho = [
            1 => 'тысяча ',
            2 => 'тысячи ',
            3 => 'тысяч ',
        ];

        $namemil = [
            1 => 'миллион ',
            2 => 'миллиона ',
            3 => 'миллионов ',
        ];

        $namemrd = [
            1 => 'миллиард ',
            2 => 'миллиарда ',
            3 => 'миллиардов ',
        ];

        $kopeek = [
            1 => 'копейка ',
            2 => 'копейки ',
            3 => 'копеек ',
        ];

        $s = ' ';
        $s1 = ' ';

        $kop = (int)(($price * 100 - (int)($price) * 100));

        $price = (int)($price);

        if ($price >= 1000000000) {
            $many = 0;

            self::_semantic((int)($price / 1000000000), $s1, $many, 3);

            $s .= $s1 . $namemrd[$many];
            $price %= 1000000000;
        }

        if ($price >= 1000000) {
            $many = 0;

            self::_semantic((int)($price / 1000000), $s1, $many, 2);

            $s .= $s1 . $namemil[$many];
            $price %= 1000000;

            if ($price == 0) {
                $s .= 'рублей ';
            }
        }

        if ($price >= 1000) {
            $many = 0;

            self::_semantic((int)($price / 1000), $s1, $many, 1);

            $s .= $s1 . $nametho[$many];
            $price %= 1000;

            if ($price == 0) {
                $s .= 'рублей ';
            }
        }

        if ($price != 0) {
            $many = 0;

            self::_semantic($price, $s1, $many, 0);

            $s .= $s1 . $namerub[$many];
        }

        if ($kop > 0) {
            $many = 0;

            self::_semantic($kop, $s1, $many, 1);

            $s .= $s1 . $kopeek[$many];
        } else {
            $s .= ' 00 копеек';
        }

        return trim($s);
    }

    /**
     * Used in self::priceToString()
     *
     * @param int $i
     * @param string $words
     * @param int $fem
     * @param int $f
     */
    private static function _semantic($i, &$words, &$fem, $f)
    {
        $words = '';

        if ($i >= 100) {
            $jkl = (int)($i / 100);
            $words .= self::$_hang[$jkl];
            $i %= 100;
        }

        if ($i >= 20) {
            $jkl = intval($i / 10);
            $words .= self::$_des[$jkl];
            $i %= 10;
        }

        switch ($i) {
            case 1:
                $fem = 1;

                break;

            case 2:
            case 3:
            case 4:
                $fem = 2;

                break;

            default:
                $fem = 3;

                break;
        }

        if ($i) {
            if ($i < 3 && $f > 0) {
                if ($f >= 2) {
                    $words .= self::$_1_19[$i];
                } else {
                    $words .= self::$_1_2[$i];
                }
            } else {
                $words .= self::$_1_19[$i];
            }
        }
    }

    /**
     * Explodes product filters to groups
     *
     * @param array $filtersRaw All filters rows
     * @param int $maxInGroup Maximum items in each vertical group
     *
     * @return array
     */
    public static function explode_filters($filtersRaw, $maxInGroup = 3)
    {
        $maxGroups = 4;

        $filtersCount = count($filtersRaw);

        $filters = [];

        $groups = (int)ceil($filtersCount / $maxInGroup);

        if ($groups === 0) {
            $groups = 1;
        }

        if ($groups > $maxGroups) {
            $groups = $maxGroups;
        }

        $inGroup = (int)ceil($filtersCount / $groups);

        $currentGroup = 0;
        $inCurrentGroup = 0;

        for ($y = 0; $y < $filtersCount; ++$y) {
            $filters[$currentGroup][] = $filtersRaw[$y];

            if ($inCurrentGroup === $inGroup - 1) {
                $inCurrentGroup = 0;

                ++$currentGroup;
            } else {
                ++$inCurrentGroup;
            }
        }

        return $filters;
    }

    /**
     * Returns possible addresses by search query
     *
     * @param string $address Search query
     *
     * @return array
     */
    public static function find_address($address)
    {
        $ch = curl_init('https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address');

        curl_setopt_array(
            $ch,
            [
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_HEADER => false,
                CURLOPT_HTTPHEADER => [
                    'Content-Type: application/json',
                    'Accept: application/json',
                    'Authorization: Token ' . env('DADATARU_TOKEN')
                ],
                CURLOPT_POSTFIELDS => json_encode([
                    'query' => $address,
                    'count' => 5,
                ]),
            ]
        );

        $response = curl_exec($ch);

        curl_close($ch);

        return $response !== false && ($return = json_decode($response, true)) && isset($return['suggestions']) ? $return['suggestions'] : null;
    }

    /**
     * Returns name, surname and patronymic from single string
     *
     * @param string $fio Single credentials string
     *
     * @return array [`name`, `surname`, `patronymic`]
     */
    public static function parse_fio($fio)
    {
        list($name, $surname, $patronymic) = explode(' ', $fio, 3) + array('', '', '');

        if ($surname) {
            $sname = $name;

            $name = $surname;

            $surname = $sname;
        }

        return [
            mb_substr(trim($name), 0, 20),
            mb_substr(trim($surname), 0, 20),
            mb_substr(trim($patronymic), 0, 20),
        ];
    }

    /**
     * Returns array of product IDs fetched from RetailRocket API
     *
     * @param string $url API url
     * @param string $apiV API version (1/2)
     *
     * @return array
     */
    public static function get_rr_product_ids($url, $apiV)
    {
        $products = remote_request($url, [], false, true, 3);

        $pIDs = [];

        for ($i = 0, $n = count($products); $i < $n; ++$i) {
            if (!empty($products[$i])) {
                $pIDs[] = (int)($apiV === 1 ? $products[$i] : $products[$i]['ItemId']);
            }
        }

        return $pIDs;
    }


    /**
     * api.morpher.ru caller. Returns morphed text
     *
     * @param string $text Text to morph
     * @param int $caseID Morph form ID (self::CASE_TYPE_* value)
     * @param bool $plural Return plural form
     *
     * @return string
     */
    public static function morpher($text, $caseID, $plural = false)
    {
        $response = remote_request(
            'http://api.morpher.ru/WebService.asmx/GetXml',
            [
                's' => $text,
                'username' => 'Roskosmetika',
                'password' => 'nopasaran123',
            ],
            false,
            false,
            2
        );

        if (!$response) {
            return null;
        }

        $xml = self::_XMLToArray($response);

        if (
            !isset($xml['xml']['Р'])
            || !isset($xml['xml']['В'])
            || !isset($xml['xml']['П'])
            || !isset($xml['xml']['П-о'])
        ) {
            return null;
        }

        $codes = [
            self::CASE_TYPE_NOMINATIVE => 'И',
            self::CASE_TYPE_PARENTAL => 'Р',
            self::CASE_TYPE_ACCUSATIVE => 'В',
            self::CASE_TYPE_PREPOSITIONAL => 'П',
            self::CASE_TYPE_PREPOSITIONAL_EXTENDED => 'П-о',
        ];

        return $plural && isset($xml['xml']['множественное'][$codes[$caseID]]) ? $xml['xml']['множественное'][$codes[$caseID]] : $xml['xml'][$codes[$caseID]];
    }

    /**
     * Converts text to grammatical case
     *
     * @param string $original Original text
     * @param int $caseID Case ID
     * @param bool $firstToLower Convert first letter to lower case
     * @param bool $nullOnFail Return null if morpher failed
     * @param bool $plural Return plural form
     *
     * @return string
     */
    public static function grammatical_case($original, $caseID, $firstToLower = false, $nullOnFail = false, $plural = false)
    {
        if ($firstToLower) {
            $original = mb_strtolower(mb_substr($original, 0, 1)) . mb_substr($original, 1);
        }

        $hash = sha1($original);

        if ($value = DB::get_field('value', 'SELECT c.value FROM rk_grammatical_cases AS c WHERE c.hash = "' . $hash . '" AND c.case_id = ' . ((int)$caseID) . ' AND c.plural = ' . ($plural ? 1 : 0))) {
            return $value;
        }

        $value = self::morpher($original, $caseID, $plural);

        if ($value === null) {
            return $nullOnFail ? null : $original;
        }

        DB::query('INSERT INTO rk_grammatical_cases (hash, case_id, value, plural) VALUES ("' . $hash . '", ' . ((int)$caseID) . ', "' . DB::mysql_secure_string($value) . '", ' . ($plural ? 1 : 0) . ')');

        return $value;
    }

    /**
     * Inserts action log
     *
     * @param int $order_number Record ID
     * @param int $userID User ID
     * @param int $codeID Code ID
     * @param string $comment Comment
     */
    public static function add_action_log($order_number, $userID, $codeID, $comment)
    {
        DB::query('INSERT INTO __logs_orders (order_number, user_id, mod_cod, action_time, comment) VALUES(' . ((int)$order_number) . ', ' . ((int)$userID) . ', ' . ((int)$codeID) . ', NOW(), "' . DB::escape($comment) . '")');
    }

    /**
     * Multibyte lcfirst()
     *
     * @param string $text Original text
     *
     * @return string
     */
    public static function mb_lcfirst($text)
    {
        return mb_convert_case(mb_substr($text, 0, 1), MB_CASE_LOWER) . mb_substr($text, 1);
    }

    /**
     * Multibyte ucfirst()
     *
     * @param string $text Original text
     *
     * @return string
     */
    public static function mb_ucfirst($text)
    {
        return mb_convert_case(mb_substr($text, 0, 1), MB_CASE_UPPER) . mb_substr($text, 1);
    }

    /**
     * Replaces SEO values in values. Each key will be wrapped with "{}"
     *
     * @param string $text Original text
     * @param array $params Key-value parameters
     *
     * @return mixed
     */
    public static function replace_seo_values($text, $params)
    {
        foreach ($params as $key => $value) {
            $text = str_replace('{' . $key . '}', $value, $text);
        }

        return $text;
    }

    /**
     * Returns name in needed numeric form
     *
     * @param string $name Original name
     * @param string $altName Alternate number name
     * @param bool $isPlural Original name in plural
     * @param bool $needPlural Return plural form (Or single if false)
     *
     * @return string
     */
    public static function numeric_name($name, $altName, $isPlural, $needPlural)
    {
        return (bool)$isPlural === (bool)$needPlural ? ($name ?: $altName) : ($altName ?: $name);
    }

    /**
     * Deletes directory recursively
     *
     * @param string $dir Directory path
     *
     * @link http://php.net/manual/ru/function.rmdir.php#110489
     *
     * @return bool
     */
    public static function unlink_recursive($dir)
    {
        $files = array_diff(scandir($dir), ['.', '..']);

        foreach ($files as $file) {
            is_dir($dir . '/' . $file)
                ? self::unlink_recursive($dir . '/' . $file)
                : unlink($dir . '/' . $file);
        }

        return rmdir($dir);
    }

    /**
     * Appends new values to array (Used in self::_XMLToArray())
     *
     * @param array $array Array to append to
     * @param array $stack Stack
     * @param array $value Value
     */
    private static function _setArrayValue(&$array, $stack, $value)
    {
        if ($stack) {
            $key = array_shift($stack);

            self::_setArrayValue($array[$key], $stack, $value);
        } else {
            $array = $value;
        }
    }

    /**
     * Parses XML string and returns as array
     *
     * @param string $xml XML string
     *
     * @return array
     */
    private static function _XMLToArray($xml)
    {
        $parser = xml_parser_create('UTF-8');

        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
        xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
        xml_parse_into_struct($parser, $xml, $values);
        xml_parser_free($parser);

        $return = array();
        $stack = array();

        foreach ($values as $val) {
            if ($val['type'] === "open") {
                array_push($stack, $val['tag']);
            } elseif ($val['type'] === "close") {
                array_pop($stack);
            } elseif ($val['type'] === "complete") {
                array_push($stack, $val['tag']);
                self::_setArrayValue($return, $stack, isset($val['value']) ? $val['value'] : '');
                array_pop($stack);
            }
        }

        return $return;
    }

    /**
     * Return ending for words with num
     * @param  int $num number
     * @param  array $end_array cases, for default array('день', 'дня', 'дней')
     * @return char               word with right case
     */
    public static function num_ending($num, $end_array = array('день', 'дня', 'дней'))
    {

        return $end_array[($num % 10 == 1 && $num % 100 != 11 ? 0 : ($num % 10 >= 2 && $num % 10 <= 4 && ($num % 100 < 10 || $num % 100 >= 20) ? 1 : 2))];

    }

    /**
     * Find user id
     * @return int|null
     */
    public static function search_user_id()
    {

        //user authorized
        $user_id = Auth::is_auth();

        if (($user_id === 0) && (isset($_COOKIE['uid']))) {
            //find user id in table
            $uid = $_COOKIE['uid'];
            $user_id = (int)DB::get_field('id', "SELECT id FROM clients WHERE secure_cid ='$uid'");

            if ($user_id === 0) {
                $user_id = (int)DB::get_field('client_id', "SELECT client_id FROM rk_orders WHERE secure_cid ='$uid'");

                //find user id in $_COOKIE['rk_id']
                if (($user_id === 0) && (isset($_COOKIE['rk_id']))) {
                    $user_id = (int)$_COOKIE['rk_id'];
                    $user_id = is_int($user_id) ? $user_id : 0;
                }
            }
        }

        return $user_id;
    }

    /**
     * Random list
     * @param array $list
     * @param int $num
     * @return array
     */
    public static function random_list($list, $num)
    {

        $select_ids = [];

        if (isset($list) && (count($list) > 0)) {
            if (count($list) != 1) {
                $select_keys = array_rand($list, count($list) > $num ? $num : count($list));

                foreach ($select_keys as $select_key) {
                    $select_ids[] = $list[$select_key];
                }
            } else {
                $select_ids = $list;
            }
        }
        return $select_ids;
    }

    /**
     * Translit
     * @param $s
     * @return mixed|null|string|string[]
     */
    public static function translit($s) {
        $s = (string) $s; // преобразуем в строковое значение
        $s = strip_tags($s); // убираем HTML-теги
        $s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
        $s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
        $s = trim($s); // убираем пробелы в начале и конце строки
        $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
        $s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
        $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
        $s = str_replace(" ", "-", $s); // заменяем пробелы знаком минус
        return $s; // возвращаем результат
    }

    /**
     * Find user IP
     * @return string
     */
    public static function get_user_ip(){

        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ip = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ip = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ip = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ip = $_SERVER['REMOTE_ADDR'];
        else
            $ip = 'NO';

        return $ip;
    }

    /**
     * Create open graph data
     * @param array $data data for meta tags
     * @return string
     */
    public static function open_graph($data)
    {
        $return = '';

        for ($i=0; $i < count($data); $i++) { 
            $return .= '<meta property="og:' . $data[$i]['property'] . '" content="' . $data[$i]['content'] . '" />';
        }

        return $return;
    }


    /**
     * Prepare string
     * @param  string $string string for file
     * @param  string $strip_tags delete tags
     * @return string         formated string
     */
    public static function str_prepare ($string, $strip_tags = FALSE) {

        $string = ($strip_tags ? strip_tags($string) : $string);

        $string = preg_replace(array('/&laquo;/',
                                     '/&quot;/', 
                                     '/quot;/',
                                     '/&raquo;/', 
                                     '/&nbsp;/', 
                                     '/&lt;p&gt;/',
                                     '/&lt;\/p&gt;/',
                                     '/"/',
                                     '/>/',
                                     '/</',
                                     '/&mdash;/', 
                                     '/&times;/',
                                     '/&minus;/',
                                     '/&deg;/',
                                     '/&trade;/',
                                     '/&reg;/',
                                     '/&alpha;/',
                                     '/&rsquo;/',
                                     '/&egrave;/',
                                     '/&amp;/'),
                               array('"', 
                                     '"',
                                     '',
                                     '"',
                                     ' ', 
                                     '',
                                     '',
                                     '"',
                                     '>',
                                     '<',
                                     '-',
                                     '*',
                                     '-',
                                     'o',
                                     'tm',
                                     '',
                                     'Alpha',
                                     '\'',
                                     'E',
                                     '&'),$string);

        return $string;

    }

}