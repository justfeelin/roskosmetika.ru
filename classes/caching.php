<?php

abstract class caching
{
    /**
     * Mysql cache table name
     */
    const TABLE = '__cache';


    /**
     * Sets cache data
     *
     * @param string $key Cache key
     * @param mixed $value Values to set
     * @param int $time Expiration time
     */
    public static function set($key, $value = null, $time = 300)
    {
        $isString = is_string($value);

        DB::query('REPLACE INTO ' . self::TABLE . ' (`name`, `value`, `is_string`, `expires`) VALUES ("' . DB::escape($key) . '","' . ($value ? (DB::escape($isString ? $value : serialize($value))) : '') . '", ' . ($isString ? 1 : 0) . ', ' . ($time ? $_SERVER['REQUEST_TIME'] + (int)$time : 0) . ')');
    }

    /**
     * Returns cached data (If exists)
     *
     * @param string $key Cache key
     *
     * @return mixed
     */
    public static function get($key)
    {
        $row = DB::get_row('SELECT c.value, c.is_string FROM ' . self::TABLE . ' AS c WHERE c.name = "' . DB::escape($key) . '" AND (c.expires = 0 OR c.expires > ' . $_SERVER['REQUEST_TIME'] . ')');

        return $row && $row['value']
            ? (
                $row['is_string']
                    ? $row['value']
                    : unserialize($row['value'])
            )
            : null;
    }

    /**
     * Deletes cached data. "%" symbol for wildcard
     *
     * @param string $key Cache key
     */
    public static function delete($key)
    {
        DB::query('DELETE FROM ' . self::TABLE . ' WHERE `name` LIKE "' . DB::escape($key) . '"');
    }

    /**
     * Erases all cached data
     */
    public static function clean()
    {
        DB::query('TRUNCATE TABLE ' . self::TABLE);
    }

    /**
     * Erases all expired cached data
     */
    public static function autoclean()
    {
        DB::query('DELETE FROM ' . self::TABLE . ' WHERE expires != 0 AND expires <= UNIX_TIMESTAMP()');

        DB::query('OPTIMIZE TABLE ' . self::TABLE);
    }
}
