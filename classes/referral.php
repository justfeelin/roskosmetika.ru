<?php

abstract class referral
{
    /**
     * Referral code key for using in URL, cookies and session
     */
    const REFERRAL_KEY = 'refid';

    /**
     * @var string Prefix used to store referral promo codes
     */
    private static $_couponPrefix = 'referral-promo-';

    /**
     * Increments referral code's activation count
     *
     * @param int $referralID Referral code ID
     *
     * @return bool
     */
    public static function addActivation($referralID)
    {
        return DB::query('UPDATE rk_referrals SET activations = activations + 1 WHERE id = ' . (int)$referralID);
    }

    /**
     * Increments referral code's used count
     *
     * @param int $userID Referral owner ID
     *
     * @return bool
     */
    public static function addUsage($userID)
    {
        return !!DB::query('UPDATE rk_referrals SET used = used + 1 WHERE client_id = ' . (int)$userID);
    }

    /**
     * Returns campaign referrals rows. Or referrals for provided user
     *
     * @param int $campaignID Campaign ID
     * @param int $userID User ID
     *
     * @return array
     */
    public static function get($campaignID, $userID = null)
    {
        $method = $userID ? 'get_row' : 'get_rows';

        return DB::$method('SELECT r.* FROM rk_referrals AS r WHERE r.campaign_id = ' . (int)$campaignID . ($userID ? ' AND r.client_id = ' . (int)$userID : ''));
    }

    public static function campaign($id)
    {
        $campaigns = [
            [
                'id' => 1,
                'name' => 'ПРИВЕДИ ДРУЗЕЙ И ПОЛУЧИ СКИДКУ 20% ЗА КАЖДОГО!',
                'description' => 'Приведи друзей и получи скидку 20% за <b>каждого</b>. Каждый друг, пришедший по ссылке, также получит 20% скидку на заказ!<ol>
                <li>Скопируй ссылку и отправь её Друзьям и Подругам.</li>
                <li>При переходе по ссылке и оформлении заказа Ваши друзья сразу получат скидку 20%</li>
                <li>После получения и оплаты заказа вашим другом мы предоставим вам скидку 20% на следующий заказ. Скидка отразится при оформлении заказа.</li>
                <li>Количество друзей, оформивших заказ по ссылке, будет равно количеству ваших заказов со скидкой.</li></ol>*Скидки не суммируются! Действует правило большей скидки!',
                'user_discount' => 20,
                'referral_discount' => 20,
                'user_percentage_discount' => true,
                'referral_percentage_discount' => true
            ]
        ];

        return isset($campaigns[$id]) ? $campaigns[$id] : null;
    }

    /**
     * Creates new user referral code
     *
     * @param int $campaignID Campaign ID
     * @param int $userID User ID
     * @param int $userDiscount User discount for every successful referral code appliance
     * @param int $referralDiscount Referral discount
     * @param bool $userPercentageDiscount User gets discount as percentage
     * @param bool $referralPercentageDiscount Referral gets discount as percentage
     *
     * @return array
     */
    public static function create($campaignID, $userID, $userDiscount, $referralDiscount, $userPercentageDiscount = true, $referralPercentageDiscount = true)
    {
        do {
            $code = sprintf('%08s', dechex(crc32(rand(10000000, 99999999))));
        } while (DB::get_field('id', 'SELECT r.id FROM rk_referrals AS r WHERE code = "' . DB::mysql_secure_string($code) . '"'));

        DB::query('INSERT INTO rk_referrals (code, campaign_id, client_id, user_discount, referral_discount, user_percentage_discount, referral_percentage_discount)
            VALUES ("' . $code . '", ' . ((int)$campaignID) . ', ' . ((int)$userID) . ', ' . ((int)$userDiscount) . ', ' . ((int)$referralDiscount) . ', ' . ($userPercentageDiscount ? 1 : 0) . ', ' . ($referralPercentageDiscount ? 1 : 0) . ')');

        return [
            'id' => DB::get_last_id(),
            'code' => $code,
            'referral_discount' => $referralDiscount,
        ];
    }

    /**
     * Creates new user referral code or returns already existing
     *
     * @param int $campaignID Campaign ID
     * @param int $userID User ID
     * @param int $userDiscount User discount for every successful referral code appliance
     * @param int $referralDiscount Referral discount
     * @param bool $userPercentageDiscount User gets discount as percentage
     * @param bool $referralPercentageDiscount Referral gets discount as percentage
     *
     * @return array
     */
    public static function getOrCreate($campaignID, $userID, $userDiscount, $referralDiscount, $userPercentageDiscount, $referralPercentageDiscount)
    {
        return self::get($campaignID, $userID) ?: self::create($campaignID, $userID, $userDiscount, $referralDiscount, $userPercentageDiscount, $referralPercentageDiscount);
    }

    /**
     * Checks code from URL. Returns DB row if exists
     *
     * @param string $code Referral code value
     *
     * @return null
     */
    public static function checkCode($code)
    {
        $row = preg_match('/^[a-f0-9]{8}$/', $code) ? DB::get_row('SELECT r.* FROM rk_referrals AS r WHERE r.code = "' . DB::mysql_secure_string($code) . '"') : null;

        if (
            ($userID = auth::is_auth())
            && $userID == $row['client_id']
        ) {
            return null;
        }

        return $row;
    }

    /**
     * Returns single maximum discount available to be used
     *
     * @param int $userID User ID to check. If not passed - user ID from session will be used
     *
     * @return array|null ['id' => `Referral code ID`, 'discount' => `Own discount value`], null if no user ID
     */
    public static function getOwnDiscount($userID = null)
    {
        $userID = $userID ?: Auth::$user_id;

        if (!$userID) {
            return null;
        }

        $row = DB::get_row('SELECT r.id, MAX(r.user_discount) AS discount FROM rk_referrals AS r WHERE r.client_id = ' . $userID . ' AND r.used < r.activations');

        $row['discount'] = (int)$row['discount'];
        $row['id'] = (int)$row['id'];

        return $row;
    }

    /**
     * Returns referral code info for logged in user
     *
     * @param int $campaignID Referral campaign ID
     * @param int $userDiscount User discount
     * @param int $referralDiscount Referral owner discount
     * @param bool $userPercentageDiscount User discount in percents
     * @param bool $referralPercentageDiscount Referral owner discount in percents
     *
     * @return array ['referral_discount' => `Referral owner discount amount`, 'code' => `Referral code hash`, 'url' => `Referral page URL`]
     */
    public static function referralInfo($campaignID, $userDiscount, $referralDiscount, $userPercentageDiscount, $referralPercentageDiscount)
    {
        if ($userID = Auth::is_auth()) {
            Template::add_script('../assets/zeroclipboard/ZeroClipboard.min.js');

            $code = referral::getOrCreate($campaignID, $userID, $userDiscount, $referralDiscount, $userPercentageDiscount, $referralPercentageDiscount);

            $return = [
                'referral_discount' => $code['referral_discount'],
                'code' => $code['code'],
                'url' => DOMAIN_FULL . '/referral/hello?' . referral::REFERRAL_KEY . '=' . $code['code'],
            ];
        } else {
            $return = [
                'referral_discount' => null,
                'code' => null,
                'url' => DOMAIN_FULL . '/',
            ];
        }

        return $return;
    }
}
