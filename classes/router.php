<?php

/**
 * Controllers management.
 */
class Router
{

    /**
     * path to controllers.
     * @var string
     */
    private static $path;
    /**
     * URL parameters.
     * @var string
     */
    private static $args;

    /**
     * Path to root folder
     * @param string $path
     * @return boolean
     */
    static function setPath($path)
    {

        $path = trim($path, '/\\');
        $path .= DIRECTORY_SEPARATOR;

        // no folder
        if (!is_dir($path)) throw new Exception('Неверные путь контроллера "' . $path . '".');

        self::$path = $path;

        return 1;
    }

    /**
     * Delegate control to controller.
     */
    static function delegate()
    {
        // Get file, file name, to do and conroller arguments
        self::getController($file, $contr, $action, $args);

        // if no file-controller...
        if (!is_readable($file)) throw new Exception('Отсутствует файл контроллера "' . basename($file) . '".');

        include_once $file;

        $class = 'Controller_' . $contr;
        $controller = new $class();

        $actionName = str_replace('-', '__', $action);

        // if no method...
        if (!is_callable(array($controller, $actionName))) {
            array_unshift($args, $action);

            $actionName = 'index';
        }

        $controller->$actionName($args);
    }

    /**
     * Define controller
     * @param string $file
     * @param string $controller
     * @param string $action
     * @param string $args
     */
    static private function getController(&$file, &$controller, &$action, &$args)
    {

        // no query
        $route = (empty($_GET['route'])) ? 'index' : $_GET['route'];

        // separate query
        $route = trim($route, '/\\');
        $parts = explode('/', $route);

        // find controller
        $cmd_path = self::$path;

        if (!empty($parts[0])) {
            $parts[0] = str_replace('-', '__', $parts[0]);
        }

        foreach ($parts as $part) {

            $fullpath = $cmd_path . $part;

            if (preg_match('/^[\w\d\s\-_]+$/u', $part) && is_file($fullpath . '.php')) {
                $controller = $part;
                array_shift($parts);
                break;
            }
        }

        // no controller
        if (empty($controller)) {
            $controller = '404';
        }

        // define action
        $action = array_shift($parts);

        if (empty($action)) {
            $action = 'index';
        }

        // file controller
        $file = $cmd_path . $controller . '.php';

        // query params
        $args = $parts;
    }

}
