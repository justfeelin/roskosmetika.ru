<?php

/**
 * Working with pages.
 */
class pages
{

    /**
     * Return page.
     * @param integer|string $page page ID or name .
     * @param bool $for_client if use client - TRUE if admin - FALSE
     * @return array
     */
    static function get_page($page, $for_clients = TRUE)
    {

        if (!get_magic_quotes_gpc()) {
            $page = DB::mysql_secure_string($page);
        }

        $query = 'SELECT id,
                             name,
                             header,
                             content,
                             title,
                             description,
                             keywords,
                             crumb,
                             type,
                             begin,
                             end '
            . ($for_clients ? '' : ', DAY(`begin`) begin_day,
                                         MONTH(`begin`) begin_month,
                                         YEAR(`begin`) begin_year,
                                         DAY(end) end_day,
                                         MONTH(end) end_month,
                                         YEAR(end) end_year,
                                         visible ') .

            'FROM rk_pages WHERE ' . (is_numeric($page) ? ' id = ' : ' name = ') . " '$page' " .
            ($for_clients ? " AND visible = 1 AND type !='spec' AND UNIX_TIMESTAMP(`begin`) <= UNIX_TIMESTAMP(NOW())  AND  (IF (UNIX_TIMESTAMP(end) != 0,  UNIX_TIMESTAMP(NOW()) <= UNIX_TIMESTAMP(end), 1))  " : '');

        return DB::get_row($query);
    }

    /**
     * Return list of pages.
     * @param  bool $for_clients - Get pages for client or for administrator (unvisible and end date)
     * @return array
     */
    static function get_list($for_clients = TRUE)
    {

        $query = "SELECT id,
                         name,
                         header,
                         content,
                         title,
                         description,
                         keywords,
                         type,
                         UNIX_TIMESTAMP(`begin`) `begin`,
                         UNIX_TIMESTAMP(end) end,
                         DAY(`begin`) begin_day,
                         MONTH(`begin`) begin_month,
                         YEAR(`begin`) begin_year,
                         DAY(end) end_day,
                         MONTH(end) end_month,
                         YEAR(end) end_year,
                         visible,
                         DATE_FORMAT( lastmod ,'%Y-%m-%d') lastmod
                  FROM rk_pages
                  WHERE " . ($for_clients ? " visible = 1 AND type !='spec' AND UNIX_TIMESTAMP(`begin`) <= UNIX_TIMESTAMP(NOW())  AND (IF (UNIX_TIMESTAMP(end) != 0,  UNIX_TIMESTAMP(NOW()) <= UNIX_TIMESTAMP(end), 1)) " : '1') .
            ' ORDER BY visible DESC, type';

        return DB::get_rows($query);
    }

    /**
     * Save page changes
     * @param integer $id
     * @param string $url
     * @param string $name
     * @param string $content
     * @param string $title
     * @param string $description
     * @param string $keywords
     * @param string $type - page type
     * @param date $begin - begin view
     * @param date $end - end view
     * @return boolean
     */
    static function set_page($id, $url, $name, $content, $title, $description, $keywords, $type, $begin, $end)
    {

        $id = (int)$id;

        if (!get_magic_quotes_gpc()) {
            $url = DB::mysql_secure_string($url);
            $name = DB::mysql_secure_string($name);
            $content = DB::mysql_secure_string($content);
            $title = DB::mysql_secure_string($title);
            $description = DB::mysql_secure_string($description);
            $keywords = DB::mysql_secure_string($keywords);
            $type = DB::mysql_secure_string($type);
            $begin = DB::mysql_secure_string($end);
            $end = DB::mysql_secure_string($end);
        }

        $query = "UPDATE rk_pages
                  SET name = '$url',
                      header = '$name', 
                      content = '$content',
                      title = '$title',
                      description = '$description',
                      keywords = '$keywords',
                      type = '$type',
                      `begin` = '$begin',
                      end = '$end'
                  WHERE id = $id";

        return DB::query($query);
    }

    /**
     * Add page.
     * @param integer $id
     * @param string $url
     * @param string $name
     * @param string $content
     * @param string $title
     * @param string $discription
     * @param string $keywords
     * @param string $type - page type
     * @param date $begin - begin view
     * @param date $end - end view
     *
     * @return bool|int false on error, new page ID on success
     */
    static function add_page($url, $name, $content, $title, $description, $keywords, $type, $begin, $end)
    {
        //info about repeat fields
        $repeat = check::check_existence('rk_pages', 'name', $url);

        if (!$repeat['result']) {
            $query = "INSERT INTO rk_pages (id, name, header, content, title, description, keywords, type, `begin`, end, visible)
                      VALUES (0, '$url', '$name', '$content', '$title', '$description', '$keywords', '$type', '$begin', '$end', 1)";

            return DB::query($query) ? DB::get_last_id() : false;
        } else {
            return FALSE;
        }
    }


    /**
     * Check name for existence.
     * @param char $url - URL page
     * @return array
     */
    static function check_name($name)
    {

        $query = "SELECT *
                  FROM rk_pages
                  WHERE name = '$name'";
        $result = array('kol' => DB::get_num_rows($query),
            'id' => DB::get_field('id', $query));

        return $result;
    }


    /**
     * Change visible
     * @param int $id - page ID
     * @return bool
     */
    static function change_visibile($id)
    {
        $id = (int)$id;

        $query = "UPDATE rk_pages
                  SET visible = (IF(visible = 1, 0, 1))
                  WHERE id = $id";

        return DB::query($query);
    }

}

?>