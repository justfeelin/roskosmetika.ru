<?php

class Payment
{
    /**
     * Sberbank order ID session key
     */
    const SBERBANK_SESSION_KEY = 'sb_pay';

    /**
     * Log action code ID for payments
     */
    const LOG_CODE_ID = 6;

    /**
     * Creates new payment row
     *
     * @param int $orderID Order ID
     * @param float $amount Payment amount
     * @param int $ts Invoice timestamp
     */
    public static function add($orderID, $amount, $ts)
    {
        DB::query('INSERT INTO payments_done (order_number, amount, ts) VALUES (' . ((int)$orderID) . ', ' . floatraw($amount) . ', ' . ((int)$ts) . ')');
    }

    /**
     * Finds payment
     *
     * @param int $orderID Order number
     * @param float $amount Payment amount
     * @param int $ts Invoice timestamp
     *
     * @return array
     */
    public static function find($orderID, $amount, $ts = 0)
    {
        return DB::get_row('SELECT p.id, p.order_number, p.amount, p.carrier_number, p.comment FROM payments_done AS p WHERE p.order_number = ' . ((int)$orderID) . ' AND p.amount = ' . floatraw($amount) . ($ts ? ' AND p.ts = ' . ((int)$ts) : ''));
    }

    /**
     * Creates payment invoice, returns invoice hash
     *
     * @param int $orderNumber Order number
     * @param float $cost Payment amount
     *
     * @return string
     */
    public static function create_invoice($orderNumber, $cost)
    {
        do {
            $hash = random_string(50);
        } while (!!DB::get_row('SELECT pi.id FROM rk_payment_invoice AS pi WHERE pi.hash = "' . DB::escape($hash) . '"'));

        DB::query('INSERT INTO rk_payment_invoice (hash, order_number, cost, ts) VALUES ("' . DB::escape($hash) . '", ' . ((int)$orderNumber) . ', ' . floatraw($cost) . ', ' . $_SERVER['REQUEST_TIME'] . ')');

        return $hash;
    }

    /**
     * Returns payment invoice hash. Creates if not found for given pair of order number - cost
     *
     * @param int $orderNumber Order number
     * @param float $cost Payment amount
     *
     * @return string
     */
    public static function get_or_create_invoice($orderNumber, $cost)
    {
        return DB::get_field('hash', 'SELECT pi.hash FROM rk_payment_invoice AS pi WHERE pi.order_number = ' . ((int)$orderNumber) . ' AND pi.cost = ' . floatraw($cost) . ' LIMIT 1') ?: self::create_invoice($orderNumber, $cost);
    }

    /**
     * Returns invoice by hash
     *
     * @param string $hash Invoice hash
     *
     * @return array
     */
    public static function get_invoice_by_hash($hash)
    {
        return DB::get_row('SELECT * FROM rk_payment_invoice AS pi WHERE pi.hash = "' . DB::escape($hash) . '"');
    }

    /**
     * Checks whether sberbank invoice was paid. If paid - marks as paid and creates payments_done row
     *
     * @param string $sbrfID
     * @param bool $checkRefund Check refund amount
     *
     * @return bool Invoice paid state
     */
    public static function check_sberbank_paid($sbrfID, $checkRefund = false)
    {
        $sbrfInvoice = DB::get_row('SELECT i.* FROM rk_sberbank_invoice AS i WHERE i.sberbank_id = "' . DB::escape($sbrfID) . '"');

        if (!$sbrfInvoice) {
            return null;
        }

        $info = !$sbrfInvoice['paid'] || $checkRefund
            ? self::_sberbank_request('getOrderStatus', [
                'orderId' => $sbrfID,
            ])
            : null;

        if (!$sbrfInvoice['paid']) {
            $paid =
                $info
                && $info['OrderStatus'] == 2;

            if ($paid) {
                DB::query('INSERT INTO payments_done (order_number, amount, ts, sberbank_invoice_id) VALUES (' . ((int)$sbrfInvoice['order_number']) . ', ' . floatraw($sbrfInvoice['cost']) . ', ' . ((int)$sbrfInvoice['ts']) . ', ' . ((int)$sbrfInvoice['id']) . ')');

                DB::query('UPDATE rk_sberbank_invoice SET paid = 1 WHERE id = ' . ((int)$sbrfInvoice['id']));

                $order = Orders::get_by_id($sbrfInvoice['order_number']);

                if ($order['status_id'] === Orders::STATUS_ID_CANCEL) {
                    Orders::mark_as_current_callback($order['order_number']);
                }

                helpers::add_action_log($sbrfInvoice['order_number'], helpers::RK_USER_ID, Payment::LOG_CODE_ID, 'платёж сбер ' . floatraw($sbrfInvoice['cost']));
            }
        } else {
            $paid = true;
        }

        if ($checkRefund && $info && $info['OrderStatus'] == 4) {
            $refund = ($info['Amount'] - $info['depositAmount']) / 100;

            if ($refund > 0) {
                Payment::set_sberbank_refund($sbrfInvoice['id'], $refund);
            }
        }

        return $paid;
    }

    /**
     * Adds or updates sberbank refund
     *
     * @param int $invoiceID Sberbank invoice ID
     * @param float $amount Refund amount (Positive!)
     */
    public static function set_sberbank_refund($invoiceID, $amount)
    {
        $invoiceID = (int)$invoiceID;

        $id = DB::get_field('id', 'SELECT p.id FROM payments_done AS p WHERE p.sberbank_invoice_id = ' . $invoiceID . ' AND p.amount < 0');

        if ($id) {
            DB::query('UPDATE payments_done SET amount = -' . floatraw($amount) . ' WHERE id = ' . $id);
        } else {
            DB::query('INSERT INTO payments_done (order_number, amount, ts, sberbank_invoice_id, comment) VALUES ((SELECT i.order_number FROM rk_sberbank_invoice AS i WHERE i.id = ' . $invoiceID . '), -' . floatraw($amount) . ', ' . $_SERVER['REQUEST_TIME'] . ', ' . $invoiceID . ', "")');
        }
    }

    /**
     * Create not paid sberbank invoice
     *
     * @param int $orderNumber Order number
     * @param int $siteOrderNumber Site order number
     * @param float $cost Payment amount
     *
     * @return array
     *
     * @throws Exception
     */
    public static function create_sberbank_invoice($orderNumber, $siteOrderNumber, $cost)
    {
        $sbrfOrder = self::_sberbank_request('register', [
            'orderNumber' => $siteOrderNumber . '-' . floatraw($cost) . '-' . $_SERVER['REQUEST_TIME'],
            'returnUrl' => DOMAIN_FULL . '/payment/result',
            'failUrl' => DOMAIN_FULL . '/payment/error',
            'amount' => (int)($cost * 100),
            'description' => 'Оплата заказа Р-' . $siteOrderNumber . ' в интернет-магазине Роскосметика.ру',
            'expirationDate' => date('Y-m-d\TH:i:s', $_SERVER['REQUEST_TIME'] + 30 * 24 * 60 * 60),
        ]);

        if (!$sbrfOrder || !empty($sbrfOrder['errorCode'])) {
            throw new Exception('Sberbank order not created');
        }

        DB::query('INSERT INTO rk_sberbank_invoice (order_number, cost, ts, sberbank_id, form_url) VALUES (' . ((int)$orderNumber) . ', ' . floatraw($cost) . ', ' . $_SERVER['REQUEST_TIME'] . ', "' . DB::escape($sbrfOrder['orderId']) . '", "' . DB::escape($sbrfOrder['formUrl']) . '")');

        return [
            'id' => DB::get_last_id(),
            'order_number' => $orderNumber,
            'cost' => $cost,
            'ts' => $_SERVER['REQUEST_TIME'],
            'sberbank_id' => $sbrfOrder['orderId'],
            'form_url' => $sbrfOrder['formUrl'],
        ];
    }

    /**
     * Returns sberbank invoices for given order
     *
     * @param int $orderNumber Order number
     * @param bool $notPaid Return not paid invoices only
     *
     * @return array
     */
    public static function get_sberbank_invoices_by_order_number($orderNumber, $notPaid = true)
    {
        return DB::get_rows('SELECT i.* FROM rk_sberbank_invoice AS i WHERE i.order_number = ' . ((int)$orderNumber) . ($notPaid ? ' AND i.paid = 0' : ''));
    }

    /**
     * Performs request to sberbank API
     *
     * @param string $method Method name (Without ".do" ending)
     * @param array $params Request params
     *
     * @return array null on error
     */
    private static function _sberbank_request($method, $params)
    {
        $info = remote_request(
            'https://securepayments.sberbank.ru/payment/rest/' . $method . '.do',
            [
                'userName' => env('SBRF_USER'),
                'password' => env('SBRF_PASSWORD'),
            ] + $params,
            true,
            true,
            10
        );

        if (!$info || !empty($sbrfOrder['errorCode'])) {
            if (DEBUG) {
                if ($info) {
                    pr($info);
                }
            }

            return null;
        }

        return $info;
    }

    /**
     * Returns sberbank payment invoice by sberbank order ID
     *
     * @param string $sberbankID Sberbank order ID
     *
     * @return array
     */
    public static function get_sberbank_invoice($sberbankID)
    {
        return DB::get_row('SELECT i.* FROM rk_sberbank_invoice AS i WHERE i.sberbank_id = "' . DB::escape($sberbankID) . '"');
    }

    /**
     * Returns existing sberbank invoice or creates new
     *
     * @param int $orderNumber Order number
     * @param float $cost Invoice cost
     *
     * @return array Invoice row
     */
    public static function get_or_create_sberbank_invoice($orderNumber, $cost)
    {
        $orderNumber = (int)$orderNumber;

        $i = 0;

        do {
            $invoice = DB::get_row('SELECT i.* FROM rk_sberbank_invoice AS i WHERE i.order_number = ' . $orderNumber . ' AND i.paid = 0 AND i.cost = ' . floatraw($cost) . ' ORDER BY i.id ASC LIMIT ' . $i . ', 1');

            if ($invoice) {
                $invoice['paid'] = self::check_sberbank_paid($invoice['sberbank_id']);
            }

            ++$i;
        } while ($invoice && $invoice['paid']);

        return $invoice ?: self::create_sberbank_invoice(
            $orderNumber,
            (int)DB::get_field(
                'site_order_number',
                'SELECT o.site_order_number FROM orders AS o WHERE o.order_number = ' . $orderNumber
            ),
            $cost
        );
    }
}
