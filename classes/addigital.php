<?php

/**
 * Work with addigital
 */
class Addigital
{

    /**
     * js addigital code for index page
     * @return string js code in string
     */
    static function index_page()
    {
        return " var _tmr = _tmr || [];
                _tmr.push({
                    type: \"itemView\",
                    productid: \"\",
                    pagetype: \"home\",
                    list: \"1\",
                    totalvalue: \"\"
                })";
    }

    /**
     * js addigital code for category page
     * @return string  js code in string
     */
    static function category_page()
    {

        return " var _tmr = _tmr || [];
                _tmr.push({
                    type: \"itemView\",
                    productid: \"\",
                    pagetype: \"category\",
                    list: \"1\",
                    totalvalue: \"\"
                })";
    }

    /**
     * js addigital code for product page
     * @param  array $product product info
     * @return string  js code in string
     */
    static function product_page($product)
    {

        return " var _tmr = _tmr || [];
                _tmr.push({
                    type: \"itemView\",
                    productid: \"{$product['id']}\",
                    pagetype: \"product\",
                    list: \"1\",
                    totalvalue: \"" . ((int)$product['special_price'] > 0 ? $product['special_price'] : $product['price']) . "\"
                })";

    }

    /**
     * js addigitalcode for cart page
     * @param  array $cart cart info
     * @return string js code in string
     */
    static function cart_page($cart)
    {
        $return = " var _tmr = _tmr || [];
                    _tmr.push({
                        type: \"itemView\",
                        productid:";

        $total = count($cart);
        $sum = 0;

        if($total == 1) {
            $return .= "\"" .  $cart[0]['id'] . "\",";
            $return .= " pagetype: \"cart\",
                        list: \"1\",
                        totalvalue: \"" . $cart[0]['quantity'] *  $cart[0]['price'] . "\"})";

        } else {
               
            $return .= '[';  
                  
                for ($i=0; $i < $total; $i++) { 
                     $return .= "\"{$cart[$i]['id']}\"" . ($i != $total-1 ? ',' : '');
                     $sum = $sum + ($cart[$i]['quantity'] * $cart[$i]['price']);
                  }  

            $return .= '],';  
            $return .= " pagetype: \"cart\",
                        list: \"1\",
                        totalvalue: \"$sum\"})";               
        }

        return $return;
    }

    /**
     * Generate addigital tracking code for order
     * @param  array  $order_items  array of order items
     * @param  string $order_price  order price
     * @return string               HTML + JS code 
     */
    static function ok_page($order_items, $order_price)
    {

        $return = "var _tmr = _tmr || [];
                _tmr.push({
                    type: \"itemView\",
                    productid: "; 


        $total = count($order_items);

        if($total == 1) {
            $return .= "\"" .  $order_items[0]['id'] . "\",";
            $return .= " pagetype: \"purchase\",
                        list: \"1\",
                        totalvalue: \"$order_price\"})";

        } else {
               
            $return .= '[';  
                  
                for ($i=0; $i < $total; $i++) { 
                     $return .= "\"{$order_items[$i]['id']}\"" . ($i != $total-1 ? ',' : '');
                  }  

            $return .= '],';  
            $return .= " pagetype: \"purchase\",
                        list: \"1\",
                        totalvalue: \"$order_price\"})";               
        }


        return $return;

    }

}