<?php

abstract class vk
{
    /**
     * Key to store token in cache
     */
    const TOKEN_CACHE_KEY = 'vk_token_admin';

    /**
     * Key to store flag that admin was notified of expired token (Used for CRON)
     */
    const ADMIN_TOKEN_MESSAGE_CACHE_KEY = 'vk_token_admin_message';

    /**
     * @var int Last API call time
     */
    private static $_last_api_call_time = null;

    /**
     * Performs vk.com token retrieval
     *
     * @param string $redirectURI URL to redirect to after client authorized on vk.com
     * @param string $scope Rights scope
     * @param bool $returnRedirectURL Return authorization URL instead of redirecting
     *
     * @return array|string
     */
    public static function get_token($redirectURI, $scope, $returnRedirectURL = false)
    {
        $appID = env('VK_APP_ID');
        $appSecret = env('VK_APP_SECRET');

        if (!isset($_GET['code']) && !isset($_GET['error'])) {
            $url = 'https://oauth.vk.com/authorize?client_id=' . $appID . '&scope=' . $scope . '&redirect_uri=' . rawurlencode($redirectURI) . '&response_type=code&v=5.60';

            if ($returnRedirectURL) {
                return $url;
            } else {
                redirect($url);

                return null;
            }
        }

        if (!empty($_GET['error'])) {
            return null;
        }

        return remote_request(
            'https://oauth.vk.com/access_token',
            [
                'client_id' => $appID,
                'client_secret' => $appSecret,
                'code' => $_GET['code'],
                'redirect_uri' => $redirectURI,
            ],
            false,
            true,
            5
        );
    }

    /**
     * Returns count of products which were not exported to vk.com yet
     *
     * @return int
     */
    public static function new_products_count()
    {
        return DB::get_field(
            'cnt',
            'SELECT COUNT(p.id) AS cnt
            FROM products AS p
            INNER JOIN rk_prod_desc AS pd ON pd.id = p.id AND pd.vk_id = 0
            WHERE p.main_id = 0'
        );
    }

    /**
     * Returns count of products which were updated but not exported to vk.com
     *
     * @return int
     */
    public static function not_updated_products_count()
    {
        return (int)DB::get_field(
            'cnt',
            'SELECT COUNT(p.id) AS cnt
            FROM products AS p
            INNER JOIN rk_prod_desc AS pd ON pd.id = p.id AND pd.vk_id != 0 AND pd.vk_updated < p.lastmod
            WHERE p.main_id = 0'
        );
    }

    /**
     * Returns count of not categorized on vk.com products
     *
     * @return int
     */
    public static function not_categorized_products_count()
    {
        return (int)DB::get_field(
            'cnt',
            'SELECT COUNT(p.id) AS cnt
            FROM products AS p
            INNER JOIN rk_prod_desc AS pd ON pd.id = p.id AND pd.vk_id != 0 AND pd.vk_categorized = 0'
        );
    }

    /**
     * Exports previously not exported products to vk.com
     *
     * @param string $token Access token
     * @param int $groupID vk.com group ID
     * @param int $limit Max products count to export
     */
    public static function export_new_products($token, $groupID, $limit = 200)
    {
        $categoryID = 702;

        $processed = $productI = 0;

        do {
            $product = DB::get_row(
                'SELECT p.id, p.name, p.pack, p.short_description, IF(p.special_price > 0, p.special_price, p.price) AS price, IF(p.url != "", p.url, p.id) AS url, IF(
                    pp.id IS NOT NULL,
                    (
                        IF(
                            pp.pic_quant = 0,
                            CONCAT(p.id, ".jpg"),
                            CONCAT(p.id, "_1_max.jpg")
                        )
                    ),
                    (
                        IF(
                            pp_.id IS NOT NULL,
                            IF(
                                pp_.pic_quant = 0,
                                CONCAT(p.main_id, ".jpg"),
                                CONCAT(p.main_id, "_1_max.jpg")
                            ),
                            NULL
                        )
                    )
                ) AS img_src
                FROM products AS p
                INNER JOIN rk_prod_desc AS pd ON pd.id = p.id AND pd.vk_id = 0
                LEFT JOIN rk_prod_photo AS pp ON pp.id = p.id AND pp.src != "none.jpg"
                LEFT JOIN rk_prod_photo AS pp_ ON p.main_id != 0 AND pp_.id = p.main_id AND pp_.src != "none.jpg"
                WHERE p.main_id = 0 AND p.active = 1 AND p.visible = 1 AND p.price > 0
                ORDER BY p.id
                LIMIT ' . $productI . ', 1'
            );

            ++$productI;

            if (!$product) {
                return;
            }

            $photoID = 0;

            if ($product['img_src']) {
                $resizedPath = '/home/u29842/vk_photos/' . $product['img_src'];

                $resizedFile = file_exists($resizedPath);

                if ($resizedFile) {
                    $filePath = $resizedPath;
                } else {
                    $filePath = tempnam(ini_get('upload_tmp_dir'), 'imp');

                    rename($filePath, $filePath . '.jpg');

                    $filePath .= '.jpg';

                    file_put_contents(
                        $filePath,
                        file_get_contents(IMAGES_DOMAIN_FULL . '/images/prod_photo/' . $product['img_src'])
                    );

                    if (!filesize($filePath)) {
                        $filePath = null;
                    }
                }

                if ($filePath) {
                    $size = getimagesize($filePath);

                    if ($size[0] < 400 || $size[1] < 400) {
                        if (
                            !($image = getimagesize($filePath))
                            || $image['mime'] !== 'image/jpeg'
                        ) {
                            if (DEBUG) {
                                print 'Bad image ' . $filePath . (CONSOLE ? "\n" : '<br>');
                            }

                            if (!$resizedFile) {
                                unlink($filePath);
                            }

                            continue;
                        }

                        $im = @imagecreatefromjpeg($filePath);

                        $newimage = imagecreatetruecolor($size[0] > 400 ? $size[0] : 400, $size[1] > 400 ? $size[1] : 400);

                        imagefill($newimage, 0, 0, imagecolorallocate($im, 255, 255, 255));

                        imagecopy($newimage, $im, $size[0] < 400 ? (int)((400 - $size[0]) / 2) : 0, $size[1] < 400 ? (int)((400 - $size[1]) / 2) : 0, 0, 0, $size[0], $size[1]);

                        imagejpeg($newimage, $filePath);

                        imagedestroy($im);
                        imagedestroy($newimage);
                    }

                    $photoID = vk::upload_market_photo($token, $groupID, $filePath);

                    if (!$resizedFile) {
                        unlink($filePath);
                    }
                } else {
                    if (DEBUG) {
                        print 'File doesn\'t exist: ' . $filePath . (CONSOLE ? "\n" : '<br>');
                    }
                }
            }

            if (!$photoID) {
                if (DEBUG) {
                    print 'No photo for product: ' . $product['id'] . (CONSOLE ? "\n" : '<br>');
                }

                continue;
            }

            $name = html_entity_decode($product['name'] . ', ' . $product['pack'], ENT_QUOTES, 'UTF-8');

            if (mb_strlen($name) > 100) {
                $name = mb_substr($name, 0, 97) . '...';
            }

            $created = vk::api_call($token, 'market.add', [
                'owner_id' => $groupID * -1,
                'name' => strip_tags($name),
                'description' => vk::product_description($product['short_description'], $product['url']),
                'category_id' => $categoryID,
                'price' => ceil((float)$product['price']),
                'main_photo_id' => $photoID,
                'deleted' => 0,
            ]);

            if ($created) {
                DB::query('UPDATE rk_prod_desc SET vk_id = ' . $created['market_item_id'] . ', vk_updated = NOW() WHERE id = ' . $product['id']);

                ++$processed;
            } else {
                if (DEBUG) {
                    print 'Product not created: ' . $product['id'] . (CONSOLE ? "\n" : '<br>');
                }
            }
        } while ($processed < $limit);
    }

    /**
     * Updates previously exported products
     *
     * @param string $token Access token
     * @param int $groupID vk.com group ID
     * @param int $limit Max products count to update
     */
    public static function update_products($token, $groupID, $limit = 200)
    {
        $categoryID = 702;

        $ids = [];
        $results = [];

        $processed = $productI = 0;

        do {
            $product = DB::get_row(
                'SELECT p.id, p.name, p.pack, p.price, p.short_description, IF(p.url != "", p.url, p.id) AS url,
                    pd.vk_id
                FROM products AS p
                INNER JOIN rk_prod_desc AS pd ON pd.id = p.id AND pd.vk_id != 0 AND pd.vk_updated < p.lastmod
                ORDER BY p.id
                LIMIT ' . $productI . ', 1'
            );

            ++$productI;

            if (!$product) {
                break;
            }

            $ids[] = (int)$product['id'];

            $name = html_entity_decode($product['name'] . ', ' . $product['pack'], ENT_QUOTES, 'UTF-8');

            if (mb_strlen($name) > 100) {
                $name = mb_substr($name, 0, 97) . '...';
            }

            $request =
                'API.market.edit({
                    "owner_id": ' . ($groupID * -1) . ',
                    "item_id": ' . $product['vk_id'] . ',
                    "name": ' . json_encode($name, JSON_UNESCAPED_UNICODE) . ',
                    "description": ' . json_encode(vk::product_description($product['short_description'], $product['url']), JSON_UNESCAPED_UNICODE) . ',
                    "category_id": ' . $categoryID . ',
                    "price": ' . ((int)ceil((float)$product['price'])) . ',
                    "main_photo_id": API.market.getById(' .
                        json_encode([
                            'item_ids' => (($groupID * -1) . '_' . $product['vk_id']),
                            'extended' => 1,
                        ]) .
                    ').items[0].photos[0].id
                })';

            $result = vk::multiple_api_calls(
                $token,
                $request,
                12// Using 2 requests for 1 product, so use 25 / 2 as maximum requests count
            );

            if ($result !== null) {
                $results = array_merge($results, $result);
            }

            ++$processed;
        } while ($processed < $limit);

        $result = vk::multiple_api_calls($token);

        if ($result !== null) {
            $results = array_merge($results, $result);
        }

        for ($i = 0, $n = count($ids); $i < $n; ++$i) {
            if (!empty($results[$i])) {
                DB::query('UPDATE rk_prod_desc SET vk_updated = NOW() WHERE id = ' . $ids[$i]);
            }
        }
    }

    /**
     * Categorizes exported products on vk.com
     *
     * @param string $token Access token
     * @param int $groupID vk.com group ID
     * @param int $limit Max products count to categorize
     */
    public static function categorize_products($token, $groupID, $limit = 200)
    {
        DB::get_rows(
            'SELECT p.id, pd.vk_id
            FROM products AS p
            INNER JOIN rk_prod_desc AS pd ON pd.id = p.id AND pd.vk_id != 0 AND pd.vk_categorized = 0
            LIMIT ' . $limit,
            function ($product) use ($token, $groupID) {
                $albumIDs = [];

                DB::get_rows(
                    'SELECT cd.vk_id
                    FROM rk_prod_cat_lvl_1 AS pc
                    INNER JOIN rk_cat_desc AS cd ON cd.id = pc.cat_id AND cd.vk_id != 0
                    WHERE pc.prod_id = ' . $product['id'],
                    function ($pc) use (&$albumIDs) {
                        $albumIDs[] = (int)$pc['vk_id'];
                    }
                );

                DB::get_rows(
                    'SELECT cd.vk_id
                    FROM rk_prod_cat_lvl_2 AS pc
                    INNER JOIN rk_cat_desc AS cd ON cd.id = pc.cat_id AND cd.vk_id != 0
                    WHERE pc.prod_id = ' . $product['id'],
                    function ($pc) use (&$albumIDs) {
                        $albumIDs[] = (int)$pc['vk_id'];
                    }
                );

                if ($albumIDs) {
                    vk::multiple_api_calls(
                        $token,
                        'API.market.addToAlbum(' . json_encode([
                            'owner_id' => $groupID * -1,
                            'item_id' => (int)$product['vk_id'],
                            'album_ids' => implode(',', $albumIDs),
                        ]) . ')'
                    );

                    DB::query('UPDATE rk_prod_desc SET vk_categorized = 1 WHERE id = ' . $product['id']);
                }
            }
        );

        self::multiple_api_calls($token);
    }

    /**
     * @param $token
     * @param $groupID
     */
    public static function create_categories_albums($token, $groupID)
    {
        DB::get_rows(
            'SELECT c.id, IF(c.h1 != "", c.h1, c.name) AS name
            FROM rk_categories AS c
            INNER JOIN rk_cat_desc AS cd ON cd.id = c.id AND cd.vk_id = 0
            WHERE   c.level = "2"
                AND c.is_tag = 0
                AND c.visible = 1
                AND c.parent_1 IN (1, 2, 5)
                AND c.id NOT IN (
                    SELECT cs.cat_id
                    FROM rk_cat_subcat AS cs
                    WHERE cs.subcat_id IN (177, 75)
                )',
            function ($category) use ($token, $groupID) {
                $created = vk::api_call($token, 'market.addAlbum', [
                    'owner_id' => $groupID * -1,
                    'title' => html_entity_decode($category['name'], ENT_QUOTES, 'UTF-8'),
                    'main_album' => 0,
                ]);

                if ($created && empty($created['error'])) {
                    DB::query('UPDATE rk_cat_desc SET vk_id = ' . $created['market_album_id'] . ' WHERE id = ' . $category['id']);
                } else {
                    if (DEBUG) {
                        pr('category ' . $category['id'] . ' not created' . ($created ? ' ' . print_r($created, true) : '') . (CONSOLE ? "\n" : '<br>'));
                    }
                }
            }
        );
    }

    /**
     * Uploads product or market album photo
     *
     * @param string $token
     * @param int $groupID vk.com group ID to add to
     * @param string $filePath Source file path
     * @param bool $product Upload product photo (Market album if false)
     *
     * @return int new photo ID
     */
    public static function upload_market_photo($token, $groupID, $filePath, $product = true)
    {
        $uploadInfo = vk::api_call($token, 'photos.' . ($product ? 'getMarketUploadServer' : 'getMarketAlbumUploadServer'), [
            'group_id' => $groupID,
            'main_photo' => 1,
        ]);

        if ($uploadInfo && empty($uploadInfo['error'])) {
            $uploaded = remote_request(
                $uploadInfo['upload_url'],
                [
                    'file' => new CurlFile($filePath, 'image/jpeg'),
                ],
                true,
                true,
                5
            );

            if ($uploaded && empty($uploaded['error'])) {
                $saveData = [
                    'group_id' => $groupID,
                    'photo' => $uploaded['photo'],
                    'server' => $uploaded['server'],
                    'hash' => $uploaded['hash'],
                ];

                if ($product) {
                    $saveData['crop_data'] = $uploaded['crop_data'];
                    $saveData['crop_hash'] = $uploaded['crop_hash'];
                }

                $photoData = vk::api_call($token, 'photos.' . ($product ? 'saveMarketPhoto' : 'saveMarketAlbumPhoto'), $saveData);

                if ($photoData && empty($photoData['error'])) {
                    return $photoData[0]['id'];
                } else {
                    if (DEBUG) {
                        print 'save photo error ' . $filePath . ($photoData ? ' ' . print_r($photoData, true) : '') . (CONSOLE ? "\n" : '<br>');
                    }
                }
            } else {
                if (DEBUG) {
                    print 'not uploaded ' . $filePath . ($uploaded ? ' ' . print_r($uploaded, true) : '') . (CONSOLE ? "\n" : '<br>');
                }
            }
        } else {
            if (DEBUG) {
                print 'no upload info ' . $filePath . ($uploadInfo ? ' ' . print_r($uploadInfo, true) : '') . (CONSOLE ? "\n" : '<br>');
            }
        }

        return null;
    }

    /**
     * Prepares product description for using on vk.com
     *
     * @param string $description Product description
     * @param string $url Product URL (Part after "/product/")
     *
     * @return string
     */
    public static function product_description($description, $url)
    {
        return strip_tags(html_entity_decode($description, ENT_QUOTES, 'UTF-8')) . "\n\n" . DOMAIN_FULL . '/product/' . $url;
    }

    /**
     * Performs API call
     *
     * @param string $token Access token
     * @param string $method Method name
     * @param array $parameters API call parameters
     *
     * @return array
     */
    public static function api_call($token, $method, $parameters)
    {
        if ($token) {
            $parameters['access_token'] = $token;
        }

        $parameters['v'] = '5.52';

        if (self::$_last_api_call_time) {
            $sleep = 0.34 - (microtime(true) - self::$_last_api_call_time);

            if ($sleep > 0) {
                usleep($sleep * 1000000);
            }
        }

        self::$_last_api_call_time = microtime(true);

        $response = remote_request('https://api.vk.com/method/' . $method, $parameters, true, false, 5);

        if ($response) {
            $json = json_decode($response, true);

            if ($json && empty($json['error'])) {
                return $json['response'];
            }
        }

        return null;
    }

    /**
     * Performs batched multiple requests by API "execute" method
     *
     * @param string $token Access token
     * @param string $request Code to pass to "execute" API method. Can be null to immediately execute previously passed code
     * @param int $max Execute when reached this count of requests
     *
     * @return array Array of results for each request
     */
    public static function multiple_api_calls($token, $request = null, $max = 25)
    {
        static $queue;

        if ($queue === null) {
            $queue = [];
        }

        if ($request !== null) {
            $queue[] = $request;
        }

        if ($queue && ($request === null || count($queue) === $max)) {
            $code = "return [\n" . implode(",\n", $queue) . "\n];";

            $queue = [];

            return self::api_call($token, 'execute', [
                'code' => $code,
            ]);
        }

        return null;
    }

    /**
     * Returns stored token value
     *
     * @param bool $nullOnExpired Return null if token expires in less than 10 minutes
     *
     * @return array
     */
    public static  function get_admin_token($nullOnExpired = true)
    {
        $token = caching::get(self::TOKEN_CACHE_KEY);

        return $token && (!$nullOnExpired || (!$token['expires'] || $token['expires'] >= $_SERVER['REQUEST_TIME'] + 600)) ? $token : null;
    }

    /**
     * Saves admin access token
     *
     * @param array $token Token
     */
    public static function set_admin_token($token)
    {
        caching::set(vk::TOKEN_CACHE_KEY, $token);
    }


    /**
     * Set product event params for layout
     * @param array $products       array of product data
     * @param array $p_recomend     array of recomended products for product
     * @param array $recomended     array of recomended 
     * @param array $cat_ids        array of categories
     * @param string $search_string users querry 
     * @param int    $total_price   total price for products group 
     * 
     * @return string  js code in string
     */
    private static function set_event_params($products = FALSE, $p_recomend = FALSE, $recomended = FALSE, $cat_ids = FALSE, $search_string = FALSE, $total_price = FALSE)
    {
        $return = '';

        if($products)
        { 
            if($p_recomend) {
                $p_recomend = implode(',', $p_recomend);
            }


            $return .= 'products : [';  

            $spec_price = isset($products[0]['special_price']) ? 'special_price' : 'specialPrice';

            for ($i=0; $i < count($products); $i++) { 
                $return .= '{id: ' . $products[$i]['id'] . ', ' . 
                            'price: ' .
                            ((isset($products[$i][$spec_price]) && $products[$i][$spec_price] > 0) ? (int)$products[$i][$spec_price] . ', price_old: ' . (int)$products[$i]['price']  : (int)$products[$i]['price'] ) . 
                            ($p_recomend ? ', recommended_ids: "' . $p_recomend . '"' : '') .
                            '}' . ($i+1 != count($products) ? ',' : '');
            }

            $return .= '],';  
        }

        if($recomended)
        {
            $return .= 'products_recommended_ids : "' . implode(',', array_column($recomended, 'id')) . '", ';
        }

        if($cat_ids)
        { 
            if(is_array($cat_ids))
            {
               $count = count($cat_ids);      
               $cat_ids = array_slice($cat_ids, $count - 2, $count - 1); 

               $return .= 'category_ids : "' .  implode(',', $cat_ids) . '", '; 

            } else
            {
                $return .= 'category_ids : "' . $cat_ids . '", ';
            }
        }

        if($search_string)
        {
            $return .= 'search_string : "' . $search_string . '", ';
        }

        if($total_price)
        {
            $return .= 'total_price : ' . (int)$total_price . ', ';
        }

        $return .= 'currency_code : "RUR"';

        return $return;
    } 

    /**
     * js vk ret code for main page
     * @return string  js code in string
     */
    private static function set_event($price_list_id, $event, $event_param)
    {
        return ' window.vkAsyncInit = function() {VK.Retargeting.Init("VK-RTRG-352543-4tMLM"); VK.Retargeting.Hit(); const eventParams = {' . $event_param . 
                '}; VK.Retargeting.ProductEvent(' . $price_list_id . ', "' . $event . '", eventParams);} ';

    }

    /**
     * View home page code
     * @return string js code
     */
    static function view_home()
    {
        $event_param = self::set_event_params(FALSE, FALSE, Product::get_popular(true, 10));

        return  self::set_event(3335, 'view_home', $event_param);
    }

    /**
     * View category page code
     * @param  array  $category Data array
     * @param  boolean $not_cat  another listing
     * @return string js code
     */
    static function view_category($category, $not_cat = FALSE)
    {
        $event_param = self::set_event_params(array_slice($category['products'], 0, 9), FALSE, FALSE, ($not_cat ? FALSE : (int)$category['main']['id']));

        return self::set_event(3335, 'view_category', $event_param);
    }

    /**
     * View product page code
     * @param  array $product Data array
     * @return string js code
     */
    static function view_product($product)
    {
        $main_product[] = $product;

        $packs_id = $line_id = $buy_with = $categories = array();

        if(isset($product['packs'])){
            $key = array_search($product['id'], $product['packs']);
            unset($product['packs'][$key]);
            $packs_id = array_column($product['packs'], 'id');
        } 

        if(isset($product['line']['products'])) {
            $key = array_search($product['id'], $product['line']['products']);
            unset($product['line']['products'][$key]);
            $line_id =  array_column($product['line']['products'], 'id');
        }

        if(isset($product['buy_with']['products'])) {
            $buy_with =  array_column($product['buy_with']['products'], 'id');
        }

        if(isset($product['categories']['crumbs'])) {
            $categories = array_column($product['categories']['crumbs'], 'id');
        }


        $p_recomend = array_merge($packs_id, $line_id, $buy_with);

        $event_param = self::set_event_params($main_product, $p_recomend, FALSE, $categories);

        return self::set_event(3335, 'view_product', $event_param);
    }

    /**
     * View search page code
     * @param  array  $products Data array
     * @param  string $querry   Search string
     * @return string js code
     */
    static function view_search($products, $querry)
    {
        $event_param = self::set_event_params(array_slice($products['products'], 0, 9), FALSE, FALSE, FALSE, $querry);

        return self::set_event(3335, 'view_search', $event_param);    
    }

    /**
     * View cart page code
     * @param  array $products Data array
     * @return string js code
     */
    static function init_checkout($products)
    {
        $recomended = product::cart_neighbours(array_column($products['products'], 'id'), false, 10);

        $event_param = self::set_event_params($products['products'], FALSE, $recomended, FALSE, FALSE, $products['info']['final_sum']);
      
        return self::set_event(3335, 'init_checkout', $event_param);
    }

    /**
     * View ok page code 
     * @param  array $products Data array
     * @return string js code
     */
    static function purchase($products)
    {
        $recomended = product::cart_neighbours(array_column($products, 'id'), false, 10);

        $event_param = self::set_event_params($products, FALSE, $recomended);
      
        return self::set_event(3335, 'purchase', $event_param);
    }
}