<?php

/**
 * Questions to cosmetolog.
 */
class questions_to_cosmetolog
{

    const STEP = 15;

    /**
     * Get quetsion to cosmetolog
     * @param  boolean $to_client TRUE for client FALSE to admin
     * @return array              array of question parameters
     */
    static function get_question($to_client = TRUE)
    {

        $return = FALSE;

        $query = 'SELECT id, question, answer FROM rk_questions_to_cosmetolog WHERE ' .
            ($to_client ? 'visible = 1' : '1') . " AND answer != ''";

        $return = DB::get_rows($query);

        if ($return) {
            shuffle($return);
            $return = $return[0];
            $return['answer'] = strip_tags($return['answer']);

            if (strlen($return['answer']) > 230) $return['answer'] = (mb_substr($return['answer'], 0, 250) . ' ...');
            $return['main_answer'] = DB::get_rows("SELECT answer 
                                                   FROM rk_questions_to_cosmetolog 
                                                   WHERE answer != ''
                                                   AND id != {$return['id']}
                                                   AND visible = 1
                                                   AND type = 'cosmetolog'");
            if ($return['main_answer']) {
                shuffle($return['main_answer']);
                $return['main_answer'] = $return['main_answer'][0]['answer'];
                $return['main_answer'] = strip_tags($return['main_answer']);

                if (strlen($return['main_answer']) > 150) $return['main_answer'] = (mb_substr($return['main_answer'], 0, 150) . ' ...');
            }
        }

        return $return;
    }


    /**
     * add question
     * @return boolean
     */
    static function add_message($name, $phone, $email, $question, $type = 'cosmetolog')
    {
        $query = "INSERT INTO rk_questions_to_cosmetolog (name, 
                                                      `date`,
                                                       phone,
                                                       email,
                                                       question, 
                                                       visible,
                                                       type)
                                               VALUES ('" . DB::escape($name) . "',
                                                        NOW(),
                                                       '" . DB::escape($phone) . "',
                                                       '" . DB::escape($email) . "',
                                                       '" . DB::escape($question) . "',
                                                        0,
                                                        '" . DB::escape($type) . "')";


        $fio = check::separate_fio($name);

        if (Check::email($email)) unsubscribe::add_self($email, 'lo', 'roskosmetika.ru', $fio['name'], $fio['surname'], $fio['patronymic']);

        return DB::query($query);
    }


    /**
     * Get all questions
     * @param  boolean $num_page number of page
     * @param  boolean $to_client only visible
     * @param  boolean $with_answer with not empty answer
     * @param bool $read Only read or not. If null - omitted
     * @return array                array of questions
     */
    static function get_questions($num_page = 0, $to_client = TRUE, $with_answer = TRUE, $read = null)
    {

        $query = "SELECT id, name, city, phone, email, date, question, answer, DAY (`date`) day, MONTH(`date`) month
                  FROM rk_questions_to_cosmetolog 
                  WHERE question != '' 
                  AND type = 'cosmetolog' " .
            ($to_client ? ' AND visible = 1' : '') .
            ($with_answer ? " AND answer !=''" : '') .
            ($read !== null ? " AND `read` = " . ($read ? 1 : 0) : '') .
            ' ORDER BY DATE DESC ' .
            ($num_page ? ' LIMIT ' . ($num_page == 1 ? 0 : self::STEP * ($num_page - 1)) . ', ' . self::STEP : '');

        return DB::get_rows($query);

    }

    /**
     * Returns questions count
     *
     * @param bool $read Whether to count only read questions
     * @param bool $withAnswer Whether to count only answered questions
     *
     * @return int
     */
    public static function get_questions_count($read = null, $withAnswer = null)
    {
        return DB::get_num_rows("SELECT id FROM rk_questions_to_cosmetolog WHERE question != '' AND type = 'cosmetolog'" .
            ($read !== null ? ' AND `read` = ' . ($read ? 1 : 0) : '') .
            ($withAnswer !== null ? ' AND answer ' . ($withAnswer ? '!=' : '=') . ' ""' : ''));
    }

    /**
     * Returns one specific question
     *
     * @param int $id Question ID
     *
     * @return array
     */
    public static function get_one_question($id)
    {
        return DB::get_row('SELECT * FROM rk_questions_to_cosmetolog WHERE id = ' . intval($id));
    }

    /**
     * Posts answer to question & emails it
     *
     * @param int $id Question ID
     * @param string $question Question text
     * @param string $answer Answer to question
     * @param string $name Client name
     * @param bool $visible Whether to mark question & answer as visible
     * @param bool $sendEmail Whether to send email to client
     *
     * @return bool
     *
     * @throws Exception
     */
    public static function post_answer($id, $question, $answer, $name, $visible, $sendEmail)
    {
        $id = intval($id);

        $questionRow = DB::get_row('SELECT email FROM rk_questions_to_cosmetolog WHERE id = ' . $id);

        if (!$questionRow) {
            return false;
        }

        DB::query('UPDATE rk_questions_to_cosmetolog SET question = "' . DB::mysql_secure_string($question) . '", answer = "' . DB::mysql_secure_string($answer) . '", name = "' . DB::mysql_secure_string($name) . '", visible = ' . ($visible ? 1 : 0) . ' WHERE id = ' . intval($id));

        if ($sendEmail) {
            $to = DEBUG ? array(env('ADMIN_EMAIL') => 'Ответ косметолога (DEBUG)') : array($questionRow['email'] => 'Ответ косметолога');

            $body = Template::mail_template('mail_answer', 'Магазин Роскосметика. Ответ косметолога', [
                'name' => $name,
                'question' => $question,
                'answer' => $answer,
            ]);

            @mailer::mail('Ответ косметолога', array('info@roskosmetika.ru' => 'Роскосметика'), $to, $body);
        }

        return true;
    }
}
