<?php

/**
 * Работа с отзывами (запросы SQL)
 */
class review
{

    const STEP = 15;

    /**
     * Вывод на страницу  комментариев
     * @param bool $visible
     * @param int $num_page
     * @return array|NULL
     */
    static function get_list($visible = 1, $num_page = 0)
    {
        $visible = (int)$visible;

        $query = 'SELECT rk_review.id,
                         rk_review.comment,
                         rk_review.time_message,
                         rk_review.visible,
                         rk_review.answer,
                         clients.id AS client_id,
                         clients.name,
                         clients.surname,
                         clients.patronymic,
                         clients.region_id,
                         clients.email,
                         rk_review.city,
                         rk_review.answer_time,
                         IF (sr.id = 0, \'Город не указан\', sr.name ) AS region_name
                  FROM  rk_review,
                        clients
                        LEFT JOIN shipping_regions AS sr ON sr.id = clients.region_id
                  WHERE rk_review.client_id = clients.id ' . ($visible ? 'AND rk_review.visible = TRUE ' : '') . '
                  ORDER BY time_message  DESC '
            . ($num_page ? 'LIMIT ' . ($num_page == 1 ? 0 : self::STEP * ($num_page - 1)) . ', ' . self::STEP : '');


        return DB::get_rows($query);
    }

    /**
     * Удаление  комментария
     * @param int $id
     */
    static function delete($id)
    {
        $id = (int)$id;

        $query = "DELETE FROM rk_review
                  WHERE id =$id";
        DB::query($query);
    }

    /**
     * Редактирование комментария
     * @param int $id
     * @param string $message
     */
    static function edit($id, $comment)
    {
        $id = (int)$id;
        if (!get_magic_quotes_gpc()) {
            $comment = DB::mysql_secure_string($comment);
        }

        $query = "UPDATE rk_review 
                  SET comment = '$comment' 
                  WHERE id = $id";

        return DB::query($query);
    }


    /**
     * Редактирование города
     * @param int $id
     * @param string $city
     */
    static function edit_city($id, $city)
    {
        $id = (int)$id;
        if (!get_magic_quotes_gpc()) {
            $city = DB::mysql_secure_string($city);
        }

        $query = "UPDATE rk_review
                  SET city = '$city'
                  WHERE id = $id";

        return DB::query($query);
    }


    /**
     * Редактирование ответа
     * @param int $id
     * @param string $message
     */
    static function edit_answer($id, $answer)
    {
        $id = (int)$id;
        if (!get_magic_quotes_gpc()) {
            $answer = DB::mysql_secure_string($answer);
        }

        $query = "UPDATE rk_review
                  SET answer = '$answer',
                      answer_time = NOW()
                  WHERE id = $id";

        return DB::query($query);
    }


    /**
     * Скрыть/открыть   комментарий
     * @param int $id
     * @param bool $visible
     */
    static function change_visible($id)
    {
        $id = (int)$id;

        $query = "UPDATE rk_review 
                  SET visible = IF(visible = 1, 0, 1)
                  WHERE id = $id";

        return DB::query($query);
    }

    /**
     * Добавление комментария
     * @param string $comment
     */
    static function add_comment($comment)
    {

        if (!get_magic_quotes_gpc()) {
            $comment = DB::mysql_secure_string($comment);
        }

        if (auth::is_auth()) {
            $user_id = auth::is_auth();
            $query = "INSERT INTO rk_review (
                                        id,
                                        client_id,
                                        comment,
                                        time_message,
                                        visible)
                          VALUES (
                                        NULL,
                                        '$user_id',
                                        '$comment',
                                        NOW(),
                                        0)";

            return DB::query($query);
        }
    }

    /**
     * Получает комментарий по id
     * @param int $review_id номер комментария
     * @return string $comment
     */
    static function get_comment($id)
    {
        $id = (int)$id;

        $query = "SELECT rk_review.id, rk_review.client_id, rk_review.city,  rk_review.time_message, rk_review.comment, rk_review.answer, rk_review.visible, clients.region_id
                  FROM rk_review, clients
                  WHERE rk_review.id = $id
                  AND rk_review.client_id = clients.id";
        $comment = DB::get_row($query);

        return $comment;
    }


    /**
     * Get rand comment
     * @return array comment params
     */
    static function get_rand_comments($limit = 1)
    {

        $query = "SELECT u.name, u.surname, r.comment, r.visible, r.city, u.region_id, sr.name AS region_name
                  FROM rk_review AS r
                  INNER JOIN clients AS u ON u.id = r.client_id
                  INNER JOIN shipping_regions AS sr ON sr.id = u.region_id
                  WHERE r.visible = 1
                  ORDER BY RAND()
                  LIMIT " . ((int)$limit);

        return DB::get_rows($query);
    }

    /**
     * Returns product reviews based on category-tm
     *
     * @param int $categoryID Category ID
     * @param int $level Category level
     * @param int $tmID Trademark ID
     * @param int $limit Limit rows count. If not passed - pagination will be applied
     *
     * @return array [$reviews, $allReviewsCount]
     *
     * @throws Exception
     */
    public static function product_reviews_category_tm($categoryID, $level, $tmID, $limit = null)
    {
        $joins = Categories::product_joins();

        return self::_product_reviews(
            'FROM products
            INNER JOIN ' . $joins[$level] . ' AS pc ON pc.cat_id = ' . $categoryID . ' AND pc.prod_id = products.id
            WHERE products.visible = 1 AND products.active = 1 AND products.tm_id = ' . $tmID ,
            $limit
        );
    }

    /**
     * Returns product reviews based on category
     *
     * @param int $categoryID Category ID
     * @param int $level Category level
     * @param int $limit Limit rows count. If not passed - pagination will be applied
     *
     * @return array [$reviews, $allReviewsCount]
     *
     * @throws Exception
     */
    public static function product_reviews_category($categoryID, $level, $limit = null)
    {
        $joins = categories::product_joins();

        return self::_product_reviews('FROM products INNER JOIN ' . $joins[$level] . ' AS pc ON pc.cat_id = ' . $categoryID . ' AND pc.prod_id = products.id WHERE products.visible = 1 AND products.active = 1', $limit);
    }

    /**
     * Returns product reviews based on trademark
     *
     * @param int $tmID Trademark ID
     * @param int $limit Limit rows count. If not passed - pagination will be applied
     *
     * @return array [$reviews, $allReviewsCount]
     *
     * @throws Exception
     */
    public static function product_reviews_tm($tmID, $limit = null)
    {
        return self::_product_reviews('FROM products WHERE products.visible = 1 AND products.active = 1 AND products.tm_id = ' . $tmID, $limit);
    }

    /**
     * Returns product reviews based on type page
     *
     * @param int $typePageID Type page ID
     * @param int $limit Limit rows count. If not passed - pagination will be applied
     *
     * @return array [$reviews, $allReviewsCount]
     *
     * @throws Exception
     */
    public static function product_reviews_type_page($typePageID, $limit = null)
    {
        return self::_product_reviews('FROM products INNER JOIN rk_prod_type_page AS ptp ON ptp.product_id = products.id AND ptp.type_page_id = ' . (int)$typePageID, $limit);
    }

    /**
     * Returns product reviews based on country page
     *
     * @param int $countryPageID Type page ID
     * @param int $limit Limit rows count. If not passed - pagination will be applied
     *
     * @return array [$reviews, $allReviewsCount]
     *
     * @throws Exception
     */
    public static function product_reviews_country_page($countryPageID, $limit = null)
    {
        return self::_product_reviews('FROM products INNER JOIN rk_prod_country_page AS pcp ON pcp.product_id = products.id AND pcp.country_page_id = ' . (int)$countryPageID, $limit);
    }

    /**
     * Returns product reviews based on type page & trademark
     *
     * @param int $typePageID Type page ID
     * @param int $tmID Trademark ID
     * @param int $limit Limit rows count. If not passed - pagination will be applied
     *
     * @return array [$reviews, $allReviewsCount]
     *
     * @throws Exception
     */
    public static function product_reviews_type_page_tm($typePageID, $tmID, $limit = null)
    {
        return self::_product_reviews('FROM products INNER JOIN rk_prod_type_page AS ptp ON ptp.product_id = products.id AND ptp.type_page_id = ' . ((int)$typePageID) . ' WHERE products.tm_id = ' . ((int)$tmID), $limit);
    }

    /**
     * Returns product reviews
     *
     * @param string $productsSql Partial query to retrieve products (Starting from 'FROM products', without alias for products table!)
     * @param int  $limit Limit rows count. If not passed - pagination will be applied
     *
     * @return array [$reviews, $allReviewsCount]
     *
     * @throws Exception
     */
    private static function _product_reviews($productsSql, $limit = null)
    {
        if ($limit === null) {
            $count = (int)DB::get_field('cnt', 'SELECT COUNT(r.id) AS cnt FROM rk_prod_review AS r WHERE r.visible = 1 AND r.prod_id IN (SELECT products.id ' . $productsSql . ')');

            Pagination::setValues($count, REVIEWS_ON_PAGE);
        }

        $reviews = DB::get_rows('SELECT r.name, r.date, r.comment, r.city,
                p.id AS product_id, IF(p.main_id = 0, p.name, (SELECT pm.name FROM products AS pm WHERE pm.id = p.main_id)) AS product_name, IF(p.main_id = 0, IF(p.url = "", p.id, p.url), (SELECT IF(pm.url = "", pm.id, pm.url) FROM products AS pm WHERE pm.id = p.main_id)) AS product_url, p.available, p.special_price, p.price,
                t.name AS tm,
                pp.src, pp.alt, pp.title photo_title
            FROM rk_prod_review AS r
            INNER JOIN products AS p ON p.id = r.prod_id
            INNER JOIN tm AS t ON t.id = p.tm_id
            LEFT JOIN rk_prod_photo AS pp ON pp.id = p.id
            WHERE r.visible = 1 AND r.prod_id IN (
                SELECT products.id ' . $productsSql . '
            )
            ORDER BY r.id DESC ' .
            ($limit ? 'LIMIT 0, ' . $limit : Pagination::sqlLimit())
        );

        return $limit ? $reviews : [
            $reviews,
            $count
        ];
    }
}
