﻿<?php

/**
 * Class for work with errors
 */
class rk_error
{

    /**
     * Query error
     */
    private static $query;


    /**
     * Mail info about error
     * @param  int $errno error number
     * @param  string $errstr error string
     * @param  string $errfile error file
     * @param  string $errline error line
     * @param array $backtrace Debug backtrace
     */
    static function send_error($errno = null, $errstr = null, $errfile = null, $errline = null, $backtrace = null)
    {
        $url = (!empty($_SERVER['SERVER_NAME']) ? 'https://' . $_SERVER['SERVER_NAME'] : '') . (!empty($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '');
        $our_email = array(env('ADMIN_EMAIL') => env('ADMIN_NAME'));
        //$our_email = array('crazyfedor@mail.ru' => 'Саня - спаситель:)');
        $from = array('sales@roskosmetika.ru' => 'Роскосметика');
        $theme = 'Информация об ошибках сайта Роскосметика';
        $body = "<p>Ошибка при выполнении сценария.</p>" .
                    ($url ? '<p>Адрес: ' . $url . '</p>': '') .
                    "<p>Referer: " . (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '<b>нет</b>') . "</p>
                    <p>Session: [" . (isset($_COOKIE[session_name()]) ? $_COOKIE[session_name()] : '') . "]</p>
                    <p>Ошибка при обращении к контроллеру:&nbsp;<b>$errfile</b></p>
                    <p>на строке номер:&nbsp;<b>$errline</b></p>
                    <p>Номер ошибки:&nbsp;<b>$errno!</b>&nbsp;<b>$errstr</b></p>";

        $query = self::$query;

        if (!is_null($query)) $body .= "<p><b>Ошибка в следующем запросе:</b>&nbsp;&quot;$query&quot;</p>";

        if ($backtrace) {
            $body .= '<h4>Debug backtrace:</h4>' . format_backtrace($backtrace);
        }

        @mailer::mail($theme, $from, $our_email, $body);
    }


    /**
     * Set query for error
     * @param string $query error
     */
    static function set_query($query)
    {
        self::$query = $query;
    }

}
