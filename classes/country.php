<?php
/**
 * Created by PhpStorm.
 * User: janpoul
 * Date: 09.04.18
 * Time: 15:18
 */

class Country
{


    /**
     * Get countries
     * @return array
     */
    static function get_list()
    {
        $query = "SELECT cc.id, cc.name FROM cdb_country cc LEFT JOIN tm ON  tm.country_id = cc.id LEFT JOIN products p ON p.tm_id=tm.id  WHERE tm.visible = 1 AND  p.visible = 1 AND p.active = 1 GROUP BY cc.id";
        return db::get_rows($query);

    }

    /**
     * Update table rk_country_page
     * @param $countryList
     * @throws Exception
     */
    static function update_table($countryList)
    {
        //Id  стран, присутствующих в таблице
        $country_ids = db::get_row("SELECT GROUP_CONCAT(rp.country_id) country_id FROM rk_country_page rp WHERE 1");

        $ids = (isset($country_ids['country_id']) && !is_null($country_ids['country_id'])) ? explode(',', $country_ids['country_id']) : [];

        $countryId=[];
        $countryName = [];
        foreach ($countryList as $country) {
            $is_vis [] = $country['id'];

            if (!in_array($country['id'], $ids)) {
                DB::query('INSERT INTO rk_country_page (url, name, h1, country_id) VALUES ("' . helpers::translit($country['name']) . '-kosmetika","' . $country['name'] . '","' . $country['name'] . '",' . (int)$country['id'] . ')');
                $countryName[] = $country['name'];
                $countryId[] = $country['id'];
            }
        }

        if (count($countryName) > 0) {
            mailer::mail('Добавлена новая страна.',
                array('news@roskosmetika.ru' => 'Площадка u29842 (роскосметика)'),
                array('roskosmetika_it@mail.ru' => 'Тех отдел. Роскосметика.','roskosmetika_content@mail.ru'=>'Отдел контента. Роскосметика.','roskosmetika_marketing@mail.ru'=>'Отдел маркетинга. Роскосметика.'),
                '<html><body><div>Добавлена новая страна.</div><div>Добавлены страны: ' . implode(',', $countryName) . '</div></body></html>');
            $countryIdList = implode(',',$countryId);
            $countryPpageId = db::get_row("SELECT GROUP_CONCAT(id) ids FROM rk_country_page WHERE country_id IN({$countryIdList})");

            if(count($countryPpageId)>0){
                Country::update_prod_country($countryPpageId['ids']);
            }
        }
    }

    public static function update_prod_country($idList = '')
    {
        if ($idList===''){
            //Чистим таблицу связей
            DB::query("TRUNCATE TABLE rk_prod_country_page");

            //Пишем таблицу связей
            DB::query("INSERT INTO rk_prod_country_page (product_id,country_page_id,created) SELECT p.id, rp.id, NOW() FROM products p LEFT JOIN tm ON p.tm_id=tm.id LEFT JOIN rk_country_page rp ON tm.country_id = rp.country_id WHERE tm.visible = 1 AND p.visible = 1 AND p.active = 1 AND rp.visible=1");
        }else{
            //Удаляем все записи для id
            DB::query("DELETE FROM rk_prod_country_page WHERE country_page_id IN({$idList})");
            //Перезаписываем таблицу связей
            DB::query("INSERT INTO rk_prod_country_page (product_id,country_page_id,created) SELECT p.id, rp.id, NOW() FROM products p LEFT JOIN tm ON p.tm_id=tm.id LEFT JOIN rk_country_page rp ON tm.country_id = rp.country_id WHERE tm.visible = 1 AND p.visible = 1 AND p.active = 1 AND rp.id IN({$idList})");
        }
        mailer::mail('Обновлены связки для стран.',
            array('news@roskosmetika.ru' => 'Площадка u29842 (роскосметика)'),
            array('roskosmetika_it@mail.ru' => 'Тех отдел. Роскосметика.'),
            '<html><body><div>Обновлены связки для стран.</div></body></html>');
    }

    /**
     * Update field in rk_country_page
     * @param $field
     * @param $value
     * @param $id
     */
    static function update_table_field($field, $value, $id)
    {
        DB::query('UPDATE rk_country_page SET ' . $field . '="' . $value . '" WHERE id IN(' . $id . ')');

    }

    /**
     * Get tm for countries
     * @param $country_id
     * @param int $limit
     * @return array
     */
    public static function get_brands($country_id, $limit = 7)
    {

        $list = DB::get_rows("SELECT tm.name as name, tm.id AS id FROM cdb_product_ballance cpb LEFT JOIN products p ON p.id = cpb.product_id LEFT JOIN tm ON tm.id = p.tm_id WHERE tm.country_id ={$country_id} AND tm.visible=1 GROUP BY tm.name ORDER BY cpb.sold DESC LIMIT {$limit} ");

        $brandName = [];
        $brandId = [];

        foreach ($list as $element) {
            $brandName[] = $element['name'];
            $brandId[] = $element['id'];
        }

        return ['name' => implode(', ', $brandName), 'id' => implode(',', $brandId)];
    }

    /**
     * Get visible countries
     * @param bool $visibleOnly
     * @return array
     */
    public static function get_countries($visibleOnly = true)
    {
        return DB::get_rows(
            "SELECT id,IF( url =  '', id, url ) url, 	name, image,h1,title,description,keywords,text,visible,country_id,created FROM rk_country_page" . ($visibleOnly ? " WHERE visible = 1" : " ") . " ORDER BY name");
    }

    /**
     * Returns country page row
     *
     * @param int $id Country page ID
     *
     * @return array
     */
    public static function get_country($id)
    {
        return DB::get_row('SELECT c.* FROM rk_country_page AS c WHERE c.id = ' . ((int)$id));
    }

    /**
     * Update all field in countries
     * @param $id
     * @param $url
     * @param $name
     * @param $h1
     * @param $title
     * @param $description
     * @param $keywords
     * @param $visible
     * @param $text
     * @param null $image
     * @return mixed
     */
    public static function update($id, $url, $name, $h1, $title, $description, $keywords, $visible, $text, $image = null)
    {

        DB::query('UPDATE rk_country_page SET url = "' . DB::escape($url) . '", `name` = "' . DB::escape($name) . '", h1 = "' . DB::escape($h1) . '", title = "' . DB::escape($title) . '", description = "' . DB::escape($description) . '", keywords = "' . DB::escape($keywords) . '", `text` = "' . DB::escape($text) . '", visible = "' . DB::escape($visible) . '"' . ($image ? ', `image` = "' . DB::escape($image) . '"' : '') . ' WHERE id = ' . (int)$id);

        return $id;
    }

    /**
     * Change visible country
     * @param $args
     */
    public static function change_visible($args)
    {
        DB::query('UPDATE rk_country_page SET visible = "' . DB::escape(!$args[1]) . '" WHERE id = ' . (int)$args[0]);
    }


    /**
     * Returns country page row by url
     *
     * @param string $url Type page URL
     * @param bool $ajax AJAX request
     *
     * @return array
     **/
    public static function output($url, $ajax = false)
    {
        $data = DB::get_row('SELECT p.* FROM rk_country_page AS p WHERE p.url = "' . DB::escape($url) . '" AND p.visible = 1');

        if (!$data) {
            return null;
        }

        $data = [
            'country' => $data,
        ];

        $filter_condition = $ajax ? Product::filter_condition('p') : '';
        $price_condition = $ajax ? Product::price_condition('p') : '';
        $tm_condition = $ajax ? Product::tm_condition('p') : '';
        $country_condition = $ajax ? Product::country_condition('p') : '';

        $innerJoins = 'INNER JOIN rk_prod_country_page AS pсp ON pсp.country_page_id = ' . $data['country']['id'] . ' AND pсp.product_id = p.id
                    INNER JOIN rk_prod_desc pd  ON (p.id = pd.id)
                    INNER JOIN tm ON tm.id = p.tm_id';

        $where = '(p.visible = 1 OR IF((SELECT COUNT(pp.id) FROM products AS pp WHERE pp.visible = 1 AND pp.main_id = p.id), 1, 0))
                    AND p.main_id = 0 AND (p.active = 1 OR p.price = 0 AND p.new = 1) ';

        $whereFull = $where . $filter_condition . $tm_condition . $price_condition /*. $country_condition*/
        ;

        Pagination::setValues((int)DB::get_field('cnt', 'SELECT COUNT(p.id) AS cnt FROM products AS p ' . $innerJoins . ' WHERE ' . $whereFull));

        $query = 'SELECT p.id, IF(p.url = "", p.id, p.url ) url, p.main_id, p.name, p.short_description, p.description, p.price, p.special_price, p.pack, p.new, p.hit, p.available, p.for_proff, 
                    p._max_discount AS discount, p._max_discount_days  AS is_sales,
                    pd.eng_name, pd.use, pd.ingredients, pd.synonym, p.tm_id,
                    ps.title, ps.description seo_desc, ps.keywords,
                    pp.src, pp.alt, pp.title photo_title,
                    tm.name tm, IF(tm.url = "", tm.id, tm.url ) tm_url, td.country,
                    (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.hit = 1 LIMIT 1) AS pack_hit,
                    (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.new = 1 LIMIT 1) AS pack_new,
                    IF(p.active = 0 AND p.price = 0 AND p.new = 1 AND p.visible = 1, 1, 0) AS soon
                    FROM products AS p
                    ' . $innerJoins . '
                    LEFT JOIN rk_prod_seo ps    ON (ps.id = p.id)
                    LEFT JOIN rk_prod_photo pp  ON (pp.id = p.id)
                    LEFT JOIN rk_tm_desc td     ON (p.tm_id = td.id)
                    WHERE ' . $whereFull . '
                    ORDER BY ' . Product::filter_order('p', false, []) .
            Pagination::sqlLimit();

        $products = DB::get_rows($query);

        $data['products'] = filters::set_products_info($products);

        if (!$ajax) {
            $data['tms'] = tm::country_page_tms($data['country']['country_id']);

            $data['reviews'] = review::product_reviews_country_page($data['country']['id'], 2);

            if ($data['reviews']) {
                $data['reviews_title'] = helpers::grammatical_case($data['country']['h1'] ?: $data['country']['name'], helpers::CASE_TYPE_PREPOSITIONAL_EXTENDED, true);
            }

            $data['filters'] = [];

            $main_filters = DB::get_rows('SELECT f.id, f.name FROM rk_filters AS f WHERE f.parent_id = 0 AND f.visible = 1 ORDER BY f.name');

            for ($i = 0, $n = count($main_filters); $i < $n; ++$i) {
                $query = 'SELECT f.id, f.name
                      FROM products AS p
                      INNER JOIN rk_prod_filter AS pf ON pf.prod_id = p.id
                      INNER JOIN rk_filters AS f ON f.id = pf.filter_id AND f.visible = 1 AND f.parent_id = ' . $main_filters[$i]['id'] . '
                      INNER JOIN rk_prod_country_page AS pcp ON pcp.product_id = p.id AND pcp.country_page_id = ' . $data['country']['id'] . '
                      WHERE p.visible = 1
                          AND p.active = 1
                          AND f.visible = 1
                      GROUP BY f.id
                      ORDER BY f.name';

                $filters = db::get_rows($query);

                if (count($filters) > 1) {
                    $data['filters'][$main_filters[$i]['name']] = helpers::explode_filters($filters);
                }
            }

            $hasH1 = !!$data['country']['h1'];

            if (!$hasH1) {
                $data['country']['h1'] = $data['country']['name'];
            }

            if (!$data['country']['title']) {

                $tmList = Country::get_brands((int)$data['country']['country_id'], 5);

                $data['country']['title'] = $data['country']['h1'] . ' ' . $tmList['name'] . ' и другие, официальный сайт - купить в интернет магазине Роскосметика по цене от ' . Products::get_product_min_price($tmList['id']) . ' руб. удобные способы оплаты и оперативная доставка, включая возможность самовывоза!';
                country::update_table_field('title', $data['country']['title'], $data['country']['id']);
            }

            if (!$data['country']['description']) {
                $tmList = Country::get_brands((int)$data['country']['country_id'], 7);
                $data['country']['description'] = $data['country']['h1'] . ' ' . $tmList['name'] . ' и другие. Выбрать и заказать косметику в интернет-магазине Роскосметика с доставкой по Москве, Санкт-Петербургу, России! В нашем каталоге огромный ассортимент, скидки и лучшие цены!';
                country::update_table_field('description', $data['country']['description'], $data['country']['id']);
            }

            $data['base_links'] = link_base::get_links(link_base::TYPE_PAGE_TYPE_ID, $data['country']['id']);

            if (count($products) <= 12) {
                $data['popular_products'] = Product::popular_products_block(8, null, null, $data['country']['id']);
            }
        } else {
            $data['filters'] = $data['products'] ? filters::available_filters($where . $tm_condition . $price_condition . $country_condition, $innerJoins) : null;
        }

        $data['filter_tms'] = $data['products'] ? filters::available_tms($where . $filter_condition . $price_condition . $country_condition, $innerJoins, 'p', $ajax) : null;

        $data['filter_prices'] = $data['products'] ? filters::price_range($where . $filter_condition . $tm_condition . $country_condition, $innerJoins) : null;

        //$data['filter_countries'] = $data['products'] ? filters::available_countries($where . $filter_condition . $price_condition . $tm_condition, $innerJoins, 'p', $ajax) : null;

        return $data;
    }
}