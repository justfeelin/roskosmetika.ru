<?php

/**
 * Work with TM.
 * @author CheS
 */
class tm
{

    /**
     * Choice tm.
     * @param  boolean $desc include description
     * @param  boolean $seo include SEO parameters
     * @param  boolean $photo include photo info
     * @param  boolean $visible only visible
     * @return array|null      arrray of TM
     */
    static function get_list($desc = TRUE, $seo = TRUE, $photo = TRUE, $visible = TRUE)
    {


        $query = 'SELECT tm.id, tm.name, tm.url, tm.short_desc, tm.desc, tm.country_id, DATE_FORMAT(tm.lastmod ,"%Y-%m-%d") lastmod ' .
            ($desc ? ' ,td.country, td.synonym, ts.title, td.alt_name' : '') .
            ($seo ? ' ,ts.title, ts.description seo_desc, ts.keywords' : '') .
            ($photo ? ' ,tp.src, tp.alt, tp.title photo_title ' : '') .
            'FROM tm ' .
            ($desc ? 'LEFT JOIN rk_tm_desc td  ON ( td.id = tm.id ) ' : '') .
            ($seo ? 'LEFT JOIN rk_tm_seo ts   ON ( ts.id = tm.id ) ' : '') .
            ($photo ? 'LEFT JOIN rk_tm_photo tp ON ( tp.id = tm.id ) ' : '') .
            ($visible ? ' WHERE tm.visible = 1 ' : '') .
            'ORDER BY tm.name';

        return DB::get_rows($query);
    }


    /**
     * Choice  brands for selection
     * @param  array $tm_id tm id for export
     * @return array         return results
     */
    static function get_export_tm($tm_id)
    {

        $tm_id = (int)$tm_id;

        $query = "SELECT tm.id, tm.name, tm.url, tm.short_desc, tm.desc, tm.visible, tm.country_id,
                         td.synonym, td.country, td.alt_name,
                         ts.title, ts.description seo_desc, ts.keywords,
                         tp.src, tp.alt, tp.title photo_title
                  FROM `tm` 
                  LEFT JOIN `rk_tm_desc`  td ON ( td.id = tm.id ) 
                  LEFT JOIN `rk_tm_seo`   ts ON ( ts.id = tm.id ) 
                  LEFT JOIN `rk_tm_photo` tp ON ( tp.id = tm.id ) 
                  WHERE tm.id = $tm_id
                  ORDER BY tm.id";

        return DB::get_row($query);
    }

    /**
     * Get TMs for main menu
     * @return array array of main menu tm
     */
    static function get_tm_menu()
    {
        return db::get_rows("SELECT id, name, IF( url =  '', id, url ) url, tm_menu FROM tm WHERE visible = 1 AND _has_products = 1 " . (TM_FOR_PROF ? 'AND prof = 1' : '') . ' ORDER BY name');
    }

    /**
     * Get by ID  URL for redirect tm
     * @param  int $tm_id id of tm
     * @return string       URL of tm
     */
    static function redirect($tm_id)
    {

        return DB::get_field('url', "SELECT url FROM tm  WHERE id = $tm_id AND visible = 1");

    }


    /**
     * Get by URL id to get tm and products
     * @param  string $url url of tm
     * @return int          ID of tm
     */
    static function get_id_by_url($url)
    {

        return (int)DB::get_field('id', "SELECT id FROM tm WHERE url = '$url' AND visible = 1");

    }


    /**
     * Output products, filters  for tm
     * @param  int $id tm ID
     * @param  int $line_id Line ID
     * @param  bool $flag flag for start work - TRUE, if FALSE - stop work and return FALSE
     * @param bool $ajax Return only products
     * @return array         products of tm withfilters
     */
    static function tm_output($id, $line_id, $flag, $ajax)
    {

        //  return array begin value
        $data = [];

        if ($flag) {

            $filter_condition = $ajax ? Product::filter_condition('p') : '';
            $price_condition = $ajax ? Product::price_condition('p') : '';

            // only tm
            if ($id && !$line_id) {

                $query = "SELECT tm.id, tm.name, tm.name tm_name, tm.url, tm.url tm_url, tm.short_desc, tm.desc,
                                   td.country, td.synonym, ts.title, td.alt_name,
                                   ts.h1, ts.title, ts.description seo_desc, ts.keywords, 
                                   tp.src, tp.alt, tp.title photo_title
                            FROM tm 
                                 LEFT JOIN rk_tm_desc td  ON ( td.id = tm.id )
                                 LEFT JOIN rk_tm_seo ts   ON ( ts.id = tm.id ) 
                                 LEFT JOIN rk_tm_photo tp ON ( tp.id = tm.id )
                            WHERE tm.id = $id
                            AND   tm.visible = 1
                            LIMIT 1";

                $tm = DB::get_row($query);

                $tm_id = (int)$tm['id'];

                $data['noindex_description'] = $tm_id === 21 || $tm_id === 83 ? true : null;

                if (!$ajax) {

                    $data['line'] = false;
                    $data['tm'] = $tm;

                    //  filters
                    $data['filters'] = [];
                    $query = "SELECT id, name FROM rk_filters WHERE parent_id = 0 AND visible = 1 ORDER BY name";

                    $main_filters = DB::get_rows($query);

                    foreach ($main_filters as $key => $main_filter) {
                        $query = "SELECT  f.id, f.name
                          FROM   products AS pd, rk_prod_filter AS pf, rk_filters AS f                                                 
                          WHERE  pd.tm_id = $id
                          AND    pd.visible = 1
                          AND    pd.active = 1
                          AND    pd.id = pf.prod_id
                          AND    pf.filter_id = f.id
                          AND    f.visible = 1
                          AND    f.parent_id = {$main_filter['id']}
                          GROUP BY f.id 
                          ORDER BY f.name";
                        $filters = db::get_rows($query);

                        if (count($filters) > 1) {
                            $data['filters'][$main_filter['name']] = helpers::explode_filters($filters);
                        }
                    }
                }

                $where = '(p.visible = 1 OR IF((SELECT COUNT(pp.id) FROM products AS pp WHERE pp.visible = 1 AND pp.main_id = p.id), 1, 0))
                    AND p.main_id = 0 AND (p.active = 1 OR p.price = 0 AND p.new = 1) ' . $filter_condition;

                $whereFull = $where . $price_condition;

                $innerJoins = 'INNER JOIN tm ON tm.id = ' . $tm_id . ' AND tm.id = p.tm_id';

                Pagination::setValues((int)DB::get_field('cnt', 'SELECT COUNT(p.id) AS cnt FROM products AS p ' . $innerJoins . ' WHERE ' . $whereFull));
                $data['show_desc'] = Pagination::getValues('page') > 1 ? FALSE : ($tm['desc'] ?: FALSE);

                $query = 'SELECT p.id, p.name, p.short_description, IF( p.url = "", p.id, p.url ) url, p.price, p.new, p.hit, p.available, p.special_price, p.pack, tm.name tm, IF(tm.url = "", tm.id, tm.url ) tm_url,
                               p._max_discount AS discount, p._max_discount_days  AS is_sales,
                               pp.src, pp.alt, pp.title photo_title,
                               (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.hit = 1 LIMIT 1) AS pack_hit,
                               (SELECT pv.id FROM products pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.new = 1 LIMIT 1) pack_new,
                               IF(p.active = 0 AND p.price = 0 AND p.new = 1 AND p.visible = 1, 1, 0) AS soon
                        FROM products p
                        ' . $innerJoins . '
                        LEFT JOIN rk_prod_photo pp ON ( pp.id = p.id )
                        WHERE ' . $whereFull . '
                        GROUP BY p.id
                        ORDER BY ' . Product::filter_order('p', false, [
                            'tm' => $tm['name'],
                        ]) . Pagination::sqlLimit();

                $products = DB::get_rows($query);
                $data['products'] = filters::set_products_info($products, $tm_id);

                if (!$ajax) {
                    $cat_in_tm = tm_in_cat::get_categories($id);

                    $categoryIDs = array();

                    for ($i = 0, $n = count($cat_in_tm); $i < $n; ++$i) {
                        $categoryIDs[] = intval($cat_in_tm[$i]['id']);
                    }

                    switch (true) {
                        case sizeof($categoryIDs) > sizeof(array_intersect(array(7, 775), $categoryIDs)):
                            if (!$data['tm']['seo_desc']) {
                                $data['tm']['seo_desc'] = 'Выбрать и заказать косметику ' . $data['tm']['name'] . ' в интернет-магазине Роскосметика с доставкой по Москве, Санкт-Петербургу, России! В нашем каталоге огромный ассортимент, скидки и лучшие цены!';
                            }

                            break;

                        case in_array(7, $categoryIDs, true):
                            if (!$data['tm']['seo_desc']) {
                                $data['tm']['seo_desc'] = 'Выбрать и заказать косметологическое и массажное оборудование ' . $data['tm']['name'] . ' в интернет-магазине Роскосметика с доставкой по Москве, Санкт-Петербургу, России! В нашем каталоге огромный ассортимент, скидки и лучшие цены!';
                            }

                            break;

                        case in_array(775, $categoryIDs, true):
                            if (!$data['tm']['seo_desc']) {
                                $data['tm']['seo_desc'] = 'Выбрать и заказать здоровое питание ' . $data['tm']['name'] . ' в интернет-магазине Роскосметика с доставкой по Москве, Санкт-Петербургу, России! В нашем каталоге огромный ассортимент, скидки и лучшие цены!';
                            }

                            break;
                    }

                    $info = DB::get_row('SELECT COUNT(p.id) AS cnt, MIN(p.price) AS min_price, MAX(p.price) AS max_price
                                                        FROM products AS p
                                                        INNER JOIN tm ON tm.id = ' . $id . ' AND tm.id = p.tm_id
                                                        WHERE p.visible = 1
                                                            AND p.active = 1
                                                            AND p.price > 0');
                    //  Open Graph data
                    $ogName = html_entity_decode($data['tm']['name']);
                    $ogAltName = html_entity_decode($data['tm']['alt_name']);

                    $data['tm']['og_title'] = self::alt_name_format('Продукция ' . $ogName . ' на выгодных условиях от интернет магазина Роскосметика', $ogName, $ogAltName);

                    if ($info['cnt'] && $info['min_price']) {
                        $data['tm']['og_description'] = self::alt_name_format('Продукция ' . $ogName . ' от интернет магазина Роскосметика – это ' . count_morphology($info['cnt'] . ' товаров|товар|товара') . ' по цене от ' . ((int)$info['min_price']) . ' руб, удобные способы оплаты и оперативная доставка, включая возможность самовывоза!', $ogName, $ogAltName);
                    }

                    $data['tm']['title'] = $data['tm']['title'] ? ($data['tm']['title'] . ($info['cnt'] && $info['min_price'] ? ' - цена от ' . (int)$info['min_price'] . ' руб. с доставкой по России' : '')) : 'Косметика ' . $data['tm']['name'] . ' – купить в интернет-магазине из Москвы по хорошей цене. Официальный сайт ' . ($data['tm']['alt_name'] ?: $data['tm']['name']) . ', косметика' . ($info['cnt'] && $info['min_price'] ? ' по цене от ' . (int)$info['min_price'] . ' руб.' : '') . ' с доставкой';


                    $data['tm']['seo_desc'] = self::alt_name_format($data['tm']['seo_desc'], $data['tm']['name'], $data['tm']['alt_name']);
                    $data['tm']['name'] = ($data['tm']['h1'] ? $data['tm']['h1'] : self::alt_name_format($data['tm']['name'], $data['tm']['name'], $data['tm']['alt_name']));

                    if ($info['cnt']) {
                        $data['tm']['title'] = helpers::replace_seo_values(
                            $data['tm']['title'],
                            [
                                'MIN_PRICE' => (int)$info['min_price'],
                                'MAX_PRICE' => (int)$info['max_price'],
                            ]
                        );

                        $data['tm']['seo_desc'] = helpers::replace_seo_values(
                            $data['tm']['seo_desc'],
                            [
                                'MIN_PRICE' => (int)$info['min_price'],
                                'MAX_PRICE' => (int)$info['max_price'],
                            ]
                        );

                        $data['tm']['desc'] = helpers::replace_seo_values(
                            $data['tm']['desc'],
                            [
                                'MIN_PRICE' => (int)$info['min_price'],
                                'MAX_PRICE' => (int)$info['max_price'],
                            ]
                        );
                    }

                    $data['reviews'] = review::product_reviews_tm($id, 2);
                    $data['sort_popularity'] = true;

                    $data['crumbs'] = [
                        [
                            'name' => 'Все бренды',
                            'url' => '/tm',
                        ],
                        [
                            'name' => $tm['name'],
                        ],
                    ];

                    $data['categories_html'] = self::_categories_html(
                        (int)$data['tm']['id'],
                        $data['tm']['url'],
                        DB::get_rows("SELECT IF(l.url = '', l.id, l.url) AS url, l.name FROM rk_lines AS l WHERE l.tm_id = $id AND l._has_products = 1")
                    );

                    if (count($products) <= 12) {
                        $data['popular_products'] = Product::popular_products_block(8, $data['tm']['id']);
                    }
                } else {
                    $data['filters'] = $data['products'] ? filters::available_filters($whereFull, $innerJoins) : null;
                }

                $data['filter_prices'] = $data['products'] ? filters::price_range($where, $innerJoins) : null;
            } elseif ($id && $line_id) {
                $query = "SELECT rk_lines.id, tm.name tm_name, tm.url tm_url, rk_tm_desc.alt_name, rk_tm_desc.country AS country, rk_lines.name, rk_lines.url, rk_lines.short_desc, rk_lines.desc, rk_lines.tm_id, ld.alt_name AS line_alt_name,
                                 tp.src AS tm_src, tp.alt AS tm_alt, tp.title AS tm_photo_title,
                                 ld.synonym,
                                 ls.h1, ls.title, ls.description seo_desc, ls.keywords, 
                                 lp.src, lp.alt, lp.title photo_title
                            FROM tm, rk_tm_desc, `rk_lines`
                                 LEFT JOIN rk_lines_desc ld  ON ( ld.id = rk_lines.id )
                                 LEFT JOIN rk_lines_seo ls   ON ( ls.id = rk_lines.id ) 
                                 LEFT JOIN rk_lines_photo lp ON ( lp.id = rk_lines.id )
                                 LEFT JOIN rk_tm_photo AS tp ON tp.id = $id
                            WHERE rk_lines.id = $line_id
                            AND   rk_lines.visible = 1
                            AND   rk_lines.tm_id = $id
                            AND   tm.id = rk_lines.tm_id
                            AND   rk_tm_desc.id = tm.id
                            LIMIT 1";

                $line= DB::get_row($query);

                $line_id = $line['id'];

                $data['noindex_description'] = null;

                if (!$ajax) {
                    //  filters
                    $data['line'] = true;
                    $data['tm'] = $line;

                    $data['filters'] = FALSE;
                    $query = "SELECT id, name FROM rk_filters WHERE parent_id = 0 AND visible = 1 ORDER BY name";

                    $main_filters = DB::get_rows($query);

                    foreach ($main_filters as $key => $main_filter) {
                        $query = "SELECT  f.id, f.name
                          FROM   products AS pd, rk_prod_filter AS pf, rk_filters AS f, rk_prod_lines AS pl
                          WHERE  pl.line_id = $line_id
                          AND    pl.prod_id = pd.id
                          AND    pd.tm_id = $id
                          AND    pd.visible = 1
                          AND    pd.active = 1
                          AND    pd.id = pf.prod_id
                          AND    pf.filter_id = f.id
                          AND    f.visible = 1
                          AND    f.parent_id = {$main_filter['id']}
                          GROUP BY f.id 
                          ORDER BY f.name";
                        $filters = DB::get_rows($query);

                        if (count($filters) > 1) {
                            $data['filters'][$main_filter['name']] = helpers::explode_filters($filters);
                        }
                    }

                    $data['crumbs'] = [
                        [
                            'name' => 'Все бренды',
                            'url' => '/tm',
                        ],
                        [
                            'name' => $line['tm_name'],
                            'url' => '/tm/' . $line['tm_url'],
                        ],
                        [
                            'name' => $line['name'],
                        ]
                    ];

                    $data['categories_html'] = self::_categories_html(
                        (int)$data['tm']['tm_id'],
                        $data['tm']['tm_url'],
                        DB::get_rows("SELECT IF( url =  '', id, url ) url, name FROM `rk_lines` WHERE tm_id = $id AND id != $line_id"),
                        false
                    );
                }

                $innerJoins = 'INNER JOIN tm ON tm.id = p.tm_id
                    INNER JOIN rk_prod_lines AS pl ON pl.line_id = ' . $line_id . ' AND pl.prod_id = p.id';

                $where = '(p.visible = 1 AND p.active = 1 OR IF((SELECT COUNT(pp.id) FROM products AS pp WHERE pp.visible = 1 AND pp.active = 1 AND pp.main_id = p.id), 1, 0))
                    AND p.main_id = 0 AND (p.active = 1 OR p.price = 0 AND p.new = 1) ' . $filter_condition;

                $whereFull = $where . $price_condition;

                Pagination::setValues((int)DB::get_field('cnt', 'SELECT COUNT(p.id) AS cnt FROM products AS p ' . $innerJoins . ' WHERE ' . $whereFull));
                $data['show_desc'] = Pagination::getValues('page') > 1 ? FALSE : ($line['desc'] ?: FALSE);

                $query = 'SELECT p.id, p.name, p.short_description, IF( p.url = "", p.id, p.url ) url, p.price, p.new, p.hit, p.available, p.special_price, p.pack, tm.name tm, IF(tm.url = "", tm.id, tm.url ) tm_url, p.for_proff, 
                               p._max_discount AS discount, p._max_discount_days  AS is_sales,
                               pp.src, pp.alt, pp.title photo_title,
                               (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.hit = 1 LIMIT 1) AS pack_hit,
                               (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.new = 1 LIMIT 1) pack_new,
                               IF(p.active = 0 AND p.price = 0 AND p.new = 1 AND p.visible = 1, 1, 0) AS soon
                        FROM products p
                    ' . $innerJoins . '
                    LEFT JOIN rk_prod_photo AS pp ON ( pp.id = p.id )
                    WHERE ' . $whereFull . '
                    GROUP BY p.id
                    ORDER BY ' . Product::filter_order('p') . Pagination::sqlLimit();

                $products = DB::get_rows($query);

                $data['products'] = filters::set_products_info($products);

                if (!$ajax) {
                    $ogName = html_entity_decode($data['tm']['name']);
                    $ogTmName = html_entity_decode($data['tm']['tm_name']);
                    $ogTmAltName = html_entity_decode($data['tm']['alt_name']);

                     $data['tm']['og_title'] = self::alt_name_format('"' . $ogName . '" от ' . $ogTmName . ' на выгодных условиях от интернет магазина Роскосметика', $ogTmName, $ogTmAltName);

                    $info = DB::get_row('SELECT COUNT(p.id) AS cnt, MIN(p.price) AS min_price, MAX(p.price) AS max_price
                                        FROM products AS p
                                        ' . $innerJoins . '
                                        WHERE p.visible = 1
                                            AND p.active = 1
                                            AND p.price > 0');

                    if ($info['cnt'] && $info['min_price']) {
                         $data['tm']['og_description'] = self::alt_name_format('"' . $ogName . '" от ' . $ogTmName . ' в интернет магазине Роскосметика – это ' . count_morphology($info['cnt'] . ' товаров|товар|товара') . ' по цене от ' . ((int)$info['min_price']) . ' руб, удобные способы оплаты и оперативная доставка, включая возможность самовывоза!', $ogTmName, $ogTmAltName);
                    }

                    $data['tm']['title'] = $data['tm']['title'] ?: 'Косметика ' . ($data['tm']['tm_name']) . ' ' . $data['tm']['name'] . ', купить ' . ($data['tm']['alt_name'] ?: $data['tm']['tm_name']) . ' ' . ($data['tm']['line_alt_name'] ?: $data['tm']['name']) . ' ' . ($info['cnt'] && $info['min_price'] ? '– цена от ' . ((int)$info['min_price']) . ' р. ' : '') . 'в интернет-магазине с доставкой';

                    $data['tm']['seo_desc'] = $data['tm']['line_alt_name']
                        ? 'Вы можете купить косметику серии ' . $data['tm']['tm_name'] . ' ' . $data['tm']['name'] . ' (' . ($data['tm']['alt_name'] ? $data['tm']['alt_name'] . ' ' : '') . $data['tm']['line_alt_name'] . ') из каталога интернет-магазина Роскосметика. Подробные описания, настоящие отзывы и актуальные цены ждут вас!'
                        : 'Вы можете купить косметику серии "' . $data['tm']['name'] . '" от ' . $data['tm']['tm_name'] . ' ' . ($data['tm']['alt_name'] ? '(' . $data['tm']['alt_name'] . ') ' : '') . 'из каталога интернет-магазина Роскосметика. Подробные описания, настоящие отзывы и актуальные цены ждут вас!';

                    if ($info['cnt']) {
                        $data['tm']['title'] = helpers::replace_seo_values(
                            $data['tm']['title'],
                            [
                                'MIN_PRICE' => (int)$info['min_price'],
                                'MAX_PRICE' => (int)$info['max_price'],
                            ]
                        );

                        $data['tm']['seo_desc'] = helpers::replace_seo_values(
                            $data['tm']['seo_desc'],
                            [
                                'MIN_PRICE' => (int)$info['min_price'],
                                'MAX_PRICE' => (int)$info['max_price'],
                            ]
                        );

                        $data['tm']['desc'] = helpers::replace_seo_values(
                            $data['tm']['desc'],
                            [
                                'MIN_PRICE' => (int)$info['min_price'],
                                'MAX_PRICE' => (int)$info['max_price'],
                            ]
                        );
                    }

                    $data['tm']['name'] = $data['tm']['h1'] ? $data['tm']['h1'] : ($data['tm']['line_alt_name']
                        ? $data['tm']['tm_name'] . ' ' . $data['tm']['name'] . ' (' . ($data['tm']['alt_name'] ? $data['tm']['alt_name'] . ' ' : '') . $data['tm']['line_alt_name'] . ')'
                        : $data['tm']['name'] . ' от ' . $data['tm']['tm_name'] . ($data['tm']['alt_name'] ? ' (' . $data['tm']['alt_name'] . ')' : ''));

                    $data['tm']['src'] = $line['src'] && $line['src'] !== 'none.jpg' ? $line['src'] : $line['tm_src'];
                    $data['tm']['alt'] = $line['alt'] ?: $line['tm_alt'];
                    $data['tm']['photo_title'] = $line['photo_title'] ?: $line['tm_photo_title'];

                    if (count($products) <= 12) {
                        $data['popular_products'] = Product::popular_products_block(8, $data['tm']['id']);
                    }
                } else {
                    $data['filters'] = $data['products'] ? filters::available_filters($whereFull, $innerJoins) : null;
                }

                $data['filter_prices'] = $data['products'] ? filters::price_range($where, $innerJoins) : null;
            }

            if (!$ajax && $data) {
                $data['base_links'] = link_base::get_links($line_id ? link_base::TRADEMARK_LINE_TYPE_ID : link_base::TRADEMARK_TYPE_ID, $id, $line_id ?: 0);
            }
        }

        return $data ?: false;
    }

    /**
     * Fetches trademark(s) by ID(s)
     *
     * @param int|array $id ID or array of IDs
     * @param bool $seo Include SEO info
     *
     * @return array
     */
    static function get_by_id($id, $seo = false)
    {
        $multiple = is_array($id);

        $rows = DB::get_rows('SELECT t.id, t.name, IF( t.url =  "", t.id, t.url ) url, t.tm_menu, tp.src, td.alt_name, t.desc ' .
            ($seo ? ', ts.title, ts.description, ts.keywords ' : '') .
            'FROM tm t
            LEFT JOIN rk_tm_desc AS td ON ( t.id = td.id )
            LEFT JOIN rk_tm_photo AS tp ON ( t.id = tp.id) ' .
            ($seo ? 'LEFT JOIN rk_tm_seo AS ts ON ts.id = t.id ' : '') .
            'WHERE t.id ' . ($multiple ? 'IN (' . implode(', ', $id) . ')' : '= ' . intval($id))
        );

        return $multiple ? $rows : (!empty($rows[0]) ? $rows[0] : null);
    }

    /**
     * Fetches trademark & products for trademark inside category
     *
     * @param int $trademark_id Trademark ID
     * @param int $category_id Category ID
     * @param int $level Category level (From 1 to 3)
     * @param int $parent_category_id Parent category ID (To fetch sibling categories)
     * @param bool $ajax Return only products
     *
     * @return array
     */
    public static function output_tm_in_category($trademark_id, $category_id, $level, $parent_category_id, $ajax)
    {
        if ($level < 1 || $level > 3) {
            return false;
        }

        $trademark_id = (int)$trademark_id;
        $category_id = (int)$category_id;
        $level = (int)$level;

        $tm_in_cat_info = DB::get_row(
            'SELECT tc.h1, tc.title, tc.description, tc.keywords, IF(tc.full_desc = "", tc.full_desc_synth, tc.full_desc) AS full_desc
            FROM rk_tm_in_cat AS tc
            WHERE tc.tm_id = ' . $trademark_id . ' AND tc.cat_id = ' .$category_id
        );

        if (!$tm_in_cat_info) {
            return false;
        }

        $data = array();

        $join = Categories::product_joins();

        $filter_condition = $ajax ? Product::filter_condition('p') : '';
        $price_condition = $ajax ? Product::price_condition('p') : '';

        $query = "SELECT tm.id, tm.name, tm.url, tm.short_desc, tm.desc,
                               td.country, td.synonym, ts.title, td.alt_name,
                               ts.title, ts.description seo_desc, ts.keywords,
                               tp.src, tp.alt, tp.title photo_title
                        FROM tm
                             LEFT JOIN rk_tm_desc td  ON ( td.id = tm.id )
                             LEFT JOIN rk_tm_seo ts   ON ( ts.id = tm.id )
                             LEFT JOIN rk_tm_photo tp ON ( tp.id = tm.id )
                        WHERE tm.id = $trademark_id
                        AND   tm.visible = 1
                        LIMIT 1";

        $data['tm'] = DB::get_row($query);

        if (!$ajax) {
            $data['category'] = Categories::get_by_id($category_id);

            if (!$data['tm'] || !$data['category']) {
                return false;
            }

            $data['filters'] = [];
            $query = "SELECT id, name FROM rk_filters WHERE parent_id = 0 AND visible = 1 ORDER BY name";

            $main_filters = DB::get_rows($query);

            foreach ($main_filters as $key => $main_filter) {
                $query = "SELECT  f.id, f.name
                          FROM   products AS pd, rk_prod_filter AS pf, rk_filters AS f, $join[$level] AS pc
                          WHERE  pc.cat_id = $category_id
                          AND    pc.prod_id = pd.id
                          AND    pd.tm_id = $trademark_id
                          AND    pd.visible = 1
                          AND    pd.active = 1
                          AND    pd.id = pf.prod_id
                          AND    pf.filter_id = f.id
                          AND    f.visible = 1
                          AND    f.parent_id = {$main_filter['id']}
                          GROUP BY f.id
                          ORDER BY f.name";
                $filters = DB::get_rows($query);

                if (count($filters) > 1) {
                    $data['filters'][$main_filter['name']] = helpers::explode_filters($filters);
                }
            }

            $data['tms'] = self::category_tms($category_id, $trademark_id);

            $data['categories'] = categories::subcategories($level > 2 ? $parent_category_id : $category_id, $level > 1 ? 3 : 2, $trademark_id);

            $organikCategory = $data['category']['id'] == 1448 || $data['category']['parent_1'] == 1448 || $data['category']['parent_2'] == 1448;

            $rawH1 = $tm_in_cat_info['h1'] ?: (($organikCategory && $data['category']['h1_2_organik'] ? $data['category']['h1_2_organik'] : ($data['category']['h1'] ?: $data['category']['name'])) . ' ' . $data['tm']['name']);

            $data['h1'] = self::alt_name_format($rawH1, $data['tm']['name'], $data['tm']['alt_name']);
            $data['desc'] = $tm_in_cat_info['full_desc'];

            $altName = $tm_in_cat_info['h1'] ? null : ($data['category']['h1'] ? $data['category']['h1_alt_number'] : $data['category']['name_alt_number']);
            $nameIsPlural = $tm_in_cat_info['h1'] ? null : ($data['category']['h1'] ? $data['category']['h1_plural'] : $data['category']['name_plural']);

            $data['seo_desc'] = $tm_in_cat_info['description'] ? self::alt_name_format($tm_in_cat_info['description'], $data['tm']['name'], $data['tm']['alt_name']) : 'Выбрать и заказать ' . $data['h1'] . ' в интернет-магазине Роскосметика с доставкой по Москве, Санкт-Петербургу, России! В нашем каталоге огромный ассортимент, скидки и лучшие цены!';
            $data['keywords'] = $tm_in_cat_info['keywords'] ? $tm_in_cat_info['keywords'] : '';

            $info = DB::get_row('SELECT COUNT(p.id) AS cnt, MIN(p.price) AS min_price, MAX(p.price) AS max_price
                                    FROM products AS p
                                    INNER JOIN tm ON tm.id = ' . $trademark_id . ' AND tm.id = p.tm_id
                                    WHERE p.id IN (SELECT pc.prod_id FROM ' . $join[$level] . ' AS pc WHERE pc.cat_id = ' . $category_id . ')
                                        AND p.visible = 1
                                        AND p.active = 1
                                        AND p.price > 0');


            $data['title'] = ($tm_in_cat_info['title'] ? $tm_in_cat_info['title'] : (
                helpers::numeric_name(($data['category']['h1'] ?: $data['category']['name']), $altName, $nameIsPlural, true) .
                ' ' .
                ($data['tm']['name'] ?: $data['tm']['alt_name']) .
                ', купить ' .
                helpers::grammatical_case(
                    helpers::numeric_name(($data['category']['h1'] ?: $data['category']['name']), $altName, $nameIsPlural, false),
                    helpers::CASE_TYPE_ACCUSATIVE,
                    true
                ) .
                ' ' .
                ($data['tm']['alt_name'] ?: $data['tm']['name']) .
                ' в интернет-магазине' )) .

                (
                    $info['cnt'] && $info['min_price']
                        ? ', цена от ' . (int)$info['min_price'] . ' руб.'
                        : ''
                );

            $ogH1 = html_entity_decode($rawH1);
            $ogTm = html_entity_decode($data['tm']['name']);
            $ogAltTm = html_entity_decode($data['tm']['alt_name']);

            $data['tm']['og_title'] = self::alt_name_format($ogH1 . ' на выгодных условиях от интернет магазина Роскосметика', $ogTm, $ogAltTm);

            if ($info['cnt'] && $info['min_price']) {
                $data['tm']['og_description'] = self::alt_name_format($ogH1 . ' от интернет магазина Роскосметика – это ' . count_morphology($info['cnt'] . ' товаров|товар|товара') . ' по цене от ' . ((int)$info['min_price']) . ' руб, удобные способы оплаты и оперативная доставка, включая возможность самовывоза!', $ogTm, $ogAltTm);
            }

            if ($info['cnt']) {
                $data['title'] = helpers::replace_seo_values(
                    $data['title'],
                    [
                        'MIN_PRICE' => (int)$info['min_price'],
                        'MAX_PRICE' => (int)$info['max_price'],
                    ]
                );

                $data['seo_desc'] = helpers::replace_seo_values(
                    $data['seo_desc'],
                    [
                        'MIN_PRICE' => (int)$info['min_price'],
                        'MAX_PRICE' => (int)$info['max_price'],
                    ]
                );

                $data['desc'] = helpers::replace_seo_values(
                    $data['desc'],
                    [
                        'MIN_PRICE' => (int)$info['min_price'],
                        'MAX_PRICE' => (int)$info['max_price'],
                    ]
                );
            }
        }

        $innerJoins = 'INNER JOIN tm ON tm.id = ' . $trademark_id . ' AND tm.id = p.tm_id
            INNER JOIN ' . $join[$level] . ' AS pc ON pc.cat_id = ' . $category_id . ' AND pc.prod_id = p.id
            INNER JOIN rk_prod_desc AS pd ON pd.id = p.id';

        $where = '(p.visible = 1 OR IF((SELECT COUNT(pp.id) FROM products AS pp WHERE pp.visible = 1 AND pp.main_id = p.id), 1, 0))
            AND p.main_id = 0 AND (p.active = 1 OR p.price = 0 AND p.new = 1) ' . $filter_condition;

        $whereFull = $where . $price_condition;

        Pagination::setValues((int)DB::get_field('cnt', 'SELECT COUNT(p.id) AS cnt FROM products AS p ' . $innerJoins . ' WHERE ' . $whereFull));

        $query = 'SELECT p.id, IF(p.url = "", p.id, p.url ) url, p.main_id, p.name, p.short_description, p.description, p.price, p.special_price, p.pack, p.new, p.hit, p.available, p.for_proff, 
            p._max_discount AS discount, p._max_discount_days  AS is_sales,
            pd.eng_name, pd.use, pd.ingredients, pd.synonym, p.tm_id,
            ps.title, ps.description seo_desc, ps.keywords,
            pp.src, pp.alt, pp.title photo_title,
            tm.name tm, IF(tm.url = "", tm.id, tm.url ) tm_url, td.country,
            (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.hit = 1 LIMIT 1) AS pack_hit,
            (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.new = 1 LIMIT 1) pack_new,
            IF(p.active = 0 AND p.price = 0 AND p.new = 1 AND p.visible = 1, 1, 0) AS soon
            FROM products AS p
            ' . $innerJoins . '
            LEFT JOIN rk_prod_seo AS ps    ON ( ps.id = p.id )
            LEFT JOIN rk_prod_photo AS pp  ON ( pp.id = p.id )
            LEFT JOIN rk_tm_desc AS td     ON ( p.tm_id = td.id )
            WHERE ' . $whereFull . '
            ORDER BY ' . Product::filter_order('p', false, [
                'categoryID' => $category_id,
                'tm' => $data['tm']['name'],
            ]) .
            Pagination::sqlLimit();

        $products = DB::get_rows($query);
        $data['products'] = filters::set_products_info($products);

        if ($ajax) {
            $data['filters'] = $data['products'] ? filters::available_filters($whereFull, $innerJoins) : null;
        } else {
            $data['reviews'] = review::product_reviews_category_tm($category_id, $level, $trademark_id, 2);
            $data['sort_popularity'] = true;

            if ($data['reviews']) {
                $data['reviews_title'] = self::alt_name_format(
                    helpers::grammatical_case(
                        $organikCategory && $data['category']['h1_2_organik'] ? $data['category']['h1_2_organik'] : ($tm_in_cat_info['h1'] ?: ($data['category']['h1'] ?: $data['category']['name']))
                        ,
                        helpers::CASE_TYPE_PREPOSITIONAL_EXTENDED,
                        true
                    ) .
                        ($organikCategory && $data['category']['h1_2_organik'] || !$tm_in_cat_info['h1'] ? ' ' . $data['tm']['name'] : ''),
                    $data['tm']['name'],
                    $data['tm']['alt_name']
                );
            }

            $data['base_links'] = link_base::get_links(
                link_base::CATEGORY_TM_TYPE_ID,
                $trademark_id,
                $level === 1 ? $category_id : ($level === 2 ? $data['category']['parent_1'] : $data['category']['parent_2']),
                $level === 3 ? $data['category']['parent_1'] : ($level === 2 ? $category_id : 0),
                $level === 3 ? $category_id : 0
            );

            if (count($products) <= 12) {
                $data['popular_products'] = Product::popular_products_block(8, $data['tm']['id'], $data['category']['id']);
            }
        }

        $data['filter_prices'] = $data['products'] ? filters::price_range($where, $innerJoins) : null;

        return $data ?: false;
    }

    /**
     * Returns trademarks located within category
     *
     * @param int $category_id Category ID
     * @param int $trademark_id Trademark ID to exclude
     *
     * @return array
     */
    public static function category_tms($category_id, $trademark_id = null)
    {
        $category_id = (int)$category_id;
        $trademark_id = (int)$trademark_id;

        $query = "SELECT t.name, IF(t.url = '', t.id, t.url) AS url
                        FROM tm AS t
                        WHERE t.visible = 1 " . ($trademark_id ? "AND t.id != $trademark_id " : '') . "AND t.id IN (
                            SELECT tc.tm_id FROM rk_tm_in_cat AS tc WHERE tc.cat_id = $category_id AND tc._has_products = 1
                        )";

        return DB::get_rows($query);
    }

    /**
     * Returns trademarks located within category
     *
     * @param int $countryID Type page ID
     * @param int $tmID Trademark ID to exclude
     *
     * @return array
     */
    public static function country_page_tms($countryID, $tmID = null)
    {
        $countryID = (int)$countryID;

        $query = 'SELECT t.name, IF(t.url = "", t.id, t.url) AS url
                FROM tm AS t
                WHERE t.visible = 1 ' . ($tmID ? 'AND t.id != ' . (int)$tmID : '') . ' AND t.country_id = ' . $countryID;

        return DB::get_rows($query);
    }

    /**
     * Returns trademarks located within category
     *
     * @param int $typePageID Type page ID
     * @param int $tmID Trademark ID to exclude
     *
     * @return array
     */
    public static function type_page_tms($typePageID, $tmID = null)
    {
        $typePageID = (int)$typePageID;

        $query = 'SELECT t.name, IF(t.url = "", t.id, t.url) AS url
                FROM tm AS t
                WHERE t.visible = 1 ' . ($tmID ? 'AND t.id != ' . (int)$tmID : '') . ' AND t.id IN (
                    SELECT ttp.tm_id FROM rk_tm_in_type_page AS ttp WHERE ttp.type_page_id = ' . $typePageID . ' AND ttp._has_products = 1
                )';

        return DB::get_rows($query);
    }

    /**
     * Parses url and retrieves tm's information
     *
     * @param array $args Controller's function arguments
     * @param bool $noRedirect Do not redirect
     *
     * @return array ['ids' => [id1, id2], 'urls' => [url1, url2]]
     */
    public static function parse_url($args, $noRedirect = false)
    {
        if (empty($args[0])) {
            return null;
        }

        $item_2 = false;
        $redirect = false;
        $redirect_2 = false;
        $id_2 = false;

        // only tm
        $item = $args[0];

        if (is_numeric($item)) {
            $id = (int)$item;

            $redirect = $id > 0 ? tm::redirect($id) : false;

            if (!$redirect) {
                $id = false;
            }
        } else {
            $item = DB::mysql_secure_string($item);
            $id = tm::get_id_by_url($item);
        }

        if (!$id) {
            return null;
        }

        //  line in tm
        if ($id && isset($args[1])) {
            $item_2 = $args[1];

            if (is_numeric($item_2)) {
                $id_2 = (int)$item_2;
                $redirect_2 = $id_2 > 0 ? lines::redirect($id_2, $id) : false;

                if (!$redirect_2) {
                    return null;
                }
            } else {
                $item_2 = DB::mysql_secure_string($item_2);
                if ($id) {
                    $id_2 = lines::get_id_by_url($item_2, $id);
                }
            }

            if (!$id_2) {
                return null;
            }
        }

        //  redirect
        if (!$noRedirect && ($redirect || $redirect_2)) {
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: /tm/' . ($redirect ?: $item) . '/' . ($redirect_2 ?: $item_2));

            exit();
        }

        return [
            'ids' => [
                $id,
                $id_2,
            ],
            'urls' => [
                $item,
                $item_2,
            ],
        ];
    }

    /**
     * Parses category-tm url
     * 
     * @param array $args URL arguments
     * @param string $redirectBase Redirect base URL (With both leading and trailing slashes)
     * @param bool $returnNames Return category names
     *
     * @return array null if not found. Array:
     * [
     *      'n' => `Number of url parts`,
     *      'category_ids' => `Array of categories IDs`,
     *      'category_urls' => `Array of categories URLs`,
     *      'tm_id' => `Trademark ID`,
     *      'tm_url' => `Trademark URL`,
     *      'category_names' => `Array of categories names (If $returnNames is true)
     * ]
     */
    public static function parse_category_tm_url($args, $redirectBase = '/category-tm/', $returnNames = false)
    {
        $not_found = false;

        $category_ids = [];
        $category_urls = [];
        $redirect = [];

        $n = count($args);

        if ($n < 2) {
            return null;
        }

        for ($i = 0; $i < $n - 1; ++$i) {
            $parent_id = $i > 0 ? $category_ids[$i - 1] : null;

            if (is_numeric($args[$i])) {
                $category = Categories::get_by_id((int)$args[$i], $parent_id);

                if (!$category) {
                    $not_found = true;

                    break;
                }

                $category_ids[$i] = (int)$args[$i];
                $category_urls[$i] = $category['url'];

                if (!is_numeric($category['url'])) {
                    $redirect[$i] = $category['url'];
                }
            } else {
                $category_id = Categories::get_id_by_url(DB::mysql_secure_string($args[$i]), $parent_id);

                if (!$category_id) {
                    $not_found = true;

                    break;
                }

                if (!$redirect) {
                    $category_ids[$i] = $category_id;
                    $category_urls[$i] = $args[$i];
                }
            }
        }

        if (!$not_found) {
            if (is_numeric($args[$i])) {
                $trademark = tm::get_by_id($args[$i]);

                if (!$trademark) {
                    $not_found = true;
                } else {
                    if (!is_numeric($trademark['url'])) {
                        $redirect[$i] = $trademark['url'];
                    } else {
                        if (!$redirect) {
                            $tm_id = $trademark['id'];
                            $tm_url = $trademark['url'];
                        }
                    }
                }
            } else {
                $tm_id = tm::get_id_by_url(DB::mysql_secure_string($args[$i]));

                if (!$tm_id) {
                    $not_found = true;
                } else {
                    $tm_url = $args[$i];
                }
            }
        }

        if ($not_found) {
            return null;
        }

        if ($redirectBase && $redirect) {
            $redirects = [];

            for ($i = 0; $i < $n; ++$i) {
                $redirects[] = isset($redirect[$i]) ? $redirect[$i] : $args[$i];
            }

            redirect($redirectBase . implode('/', $redirects));
        }

        return [
            'n' => $n,
            'category_ids' => $category_ids,
            'category_urls' => $category_urls,
            'tm_id' => $tm_id,
            'tm_url' => $tm_url,
        ] + (
            $returnNames
                ? [
                    'category_names' => [
                        DB::get_field('name', 'SELECT IF(c.h1 = "" OR c.h1 IS NULL, c.name, c.h1) AS name FROM rk_categories AS c WHERE c.id = ' . $category_ids[0]),
                        $n > 2 ? DB::get_field('name', 'SELECT IF(c.h1 = "" OR c.h1 IS NULL, c.name, c.h1) AS name FROM rk_categories AS c WHERE c.id = ' . $category_ids[1]) : null,
                        $n > 3 ? DB::get_field('name', 'SELECT IF(c.h1 = "" OR c.h1 IS NULL, c.name, c.h1) AS name FROM rk_categories AS c WHERE c.id = ' . $category_ids[2]) : null,
                    ],
                ]
                : []
        );
    }

    /**
     * Appends alternative trademark name for first name occurrence
     *
     * @param string $text Original text, containing trademark name
     * @param string $tm Trademark name
     * @param string $altName Alternative trademark name
     *
     * @return string
     */
    public static function alt_name_format($text, $tm, $altName)
    {
        return $text && $altName ? preg_replace('/' . preg_quote($tm) . '/', $tm . ' (' . $altName . ')', $text, 1) : $text;
    }

    /**
     * Updates trademark alt_name, title, description, keywords
     *
     * @param int $tmID Trademark ID
     * @param string $altName Alternative name
     * @param string $title Title value
     * @param string $description Description value
     * @param string $keywords Keywords value
     * @param string $pageText Page text
     *
     * @return bool
     */
    public static function update_seo($tmID, $altName, $title, $description, $keywords, $pageText)
    {
        return !!DB::query('UPDATE rk_tm_desc SET alt_name = "' . DB::escape($altName) . '" WHERE id = ' . ((int)$tmID))
        && !!DB::query('UPDATE rk_tm_seo SET title = "' . DB::escape($title) . '", description = "' . DB::escape($description) . '", keywords = "' . DB::escape($keywords) . '" WHERE id = ' . ((int)$tmID))
        && !!DB::query('UPDATE tm SET `desc` = "' . DB::escape($pageText) . '" WHERE id = ' . ((int)$tmID));
    }

    /**
     * Returns rendered categories list for given trademark
     *
     * @param int $tmID Trademark ID
     * @param string $tmURL Trademark URL
     * @param array $lines Tm lines
     * @param bool $withCategories Add categories block
     *
     * @return string
     */
    private static function _categories_html($tmID, $tmURL, $lines, $withCategories = true)
    {
        return Template::get_tpl(
            '2016/tm_categories',
            null,
            [
                'tm_url' => $tmURL,
                'categories' => $withCategories ? DB::get_rows('SELECT c.name, c.url FROM rk_categories AS c WHERE c.level = 1 AND c.id IN (SELECT tc.cat_id FROM rk_tm_in_cat AS tc WHERE tc.tm_id = ' . $tmID . ' AND tc._has_products = 1) AND c.is_tag = 0 AND c._has_products = 1') : null,
                'lines' => $lines,
            ]
        );
    }

    /**
     * Get list of the countries for brand
     * @return array           list of countries
     */
    static function list_of_countries()
    {
        return DB::get_rows('SELECT `id`, `name`, `ean`, `ean_org`, `key`, `access_type` FROM `cdb_country` ORDER BY `name`');
    }
}
