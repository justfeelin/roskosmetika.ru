<?php

/**
 * Articles and categories of articles
 */
class Articles
{

    /**
     * Return articles to layout
     * @param boolean $all only visible
     * @return array|null
     */
    static function get_all($all = false)
    {

        // choice articles (parent_id = 0).
        $query = 'SELECT id, url, name, parent_id, visible, lastmod
                  FROM rk_articles
                  WHERE parent_id = 0 ' . ($all ? '' : 'AND  visible = 1 ') . '
                  ORDER BY pos';

        $parts = DB::get_rows($query);

        foreach ($parts as $key => $part) {
            // Choice articles for group
            $query = "SELECT id, IF( url =  '', id, url ) url, name, visible
                      FROM rk_articles
                      WHERE parent_id = {$part['id']}" . ($all ? '' : ' AND visible = 1 ') . "
                      ORDER BY pos";

            $parts[$key]['articles'] = DB::get_rows($query);
        }

        return $parts;
    }

    /**
     * Get list of articles
     * @param  boolean $visible True - only visible, False - all
     * @return array            array of articles
     */
    static function get_list($visible = TRUE)
    {

        $query = "SELECT id, name, IF( url =  '', id, url ) url, DATE_FORMAT( lastmod ,'%Y-%m-%d') lastmod 
                  FROM  rk_articles
                  WHERE " . ($visible ? "visible = 1 " : '1') .
            ' AND parent_id != 0
                    ORDER BY name';

        return DB::get_rows($query);
    }

    /**
     * Возвращает статью по id.
     * @param integer $art_id Id статьи.
     * @param boolean $all Выборка в независимости от видимости статьи.
     * @return <type>
     */
    static function get_art($art_id, $all = false, $url = FALSE)
    {

        $return = array();

        $query = 'SELECT id, IF( url =  "", id, url ) url, name, description, title, keywords, text, parent_id, synonym
                  FROM rk_articles
                  WHERE ' . ($url ? 'url = "' . $art_id . '"' : "id = $art_id") .
            ($all ? '' : ' AND  visible = 1 ') . ' LIMIT 1';


        $return['article'] = db::get_row($query);

        if ($all) {
            $return = $return['article'];
        } else {

            if ($return['article']) {

                $return['products'] = FALSE;

                $query = "SELECT p.id, p.name, p.short_description, IF( p.url =  '', p.id, p.url ) url, p.price, p.new, p.hit, p.available, p.special_price, p.pack, tm.name tm, IF(tm.url =  '', tm.id, tm.url ) tm_url, di.id day_item, p.for_proff,
                                 p._max_discount AS discount, p._max_discount_days  AS is_sales,
                                 pp.src, pp.alt, pp.title photo_title,
                                 (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.new = 1 LIMIT 1) AS pack_new,
                                 (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.hit = 1 LIMIT 1) AS pack_hit,
                                 0 AS soon
                          FROM rk_articleprods AS ap, tm,  products AS pd
                          LEFT JOIN products AS p ON p.id = IF(pd.main_id = 0, pd.id, pd.main_id)
                          LEFT JOIN rk_prod_photo pp  ON ( pp.id = p.id ) 
                          LEFT JOIN rk_day_item di   ON ( di.prod_id = p.id  AND UNIX_TIMESTAMP(di.begin) <= UNIX_TIMESTAMP(NOW())  AND   UNIX_TIMESTAMP(NOW()) <= UNIX_TIMESTAMP(di.end))  
                          WHERE  ap.article_id = {$return['article']['id']}
                          AND    ap.prod_id = pd.id
                          AND    p.tm_id = tm.id
                          AND    p.visible = 1 AND p.active = 1
                          GROUP BY p.id";


                $products = DB::get_rows($query);

                if ($products) {

                    shuffle($products);

                    foreach ($products as $key => $product) {

                        $product['discount'] = $product['discount'];

                        $return['products'][$key] = $product;
                        $return['products'][$key]['packs'] = Product::get_packs($product['id']);
                        if ($key == 4) break;
                    }

                }

                $return['categories'] = FALSE;

                $words = Search::prepare_string($return['article']['name'], FALSE);
                $words = $words['words'];

                $query = "SELECT c.id, c.name, c._full_url, c.h1
                          FROM rk_article_cat_3 AS ac3, rk_categories AS c
                          LEFT JOIN rk_cat_desc AS cd ON ( c.id = cd.id )
                          INNER JOIN rk_categories AS c2 ON c2.id = c.parent_1 AND c2.visible = 1
                          INNER JOIN rk_categories AS c1 ON c1.id = c.parent_2 AND c1.visible = 1
                          WHERE ac3.article_id = {$return['article']['id']}
                          AND   ac3.cat_id = c.id
                          AND   c.visible = 1 
                          AND   c.canonical IS NULL AND (";

                foreach ($words as $key => $word) {
                    if ($key != 0) $query .= ' OR ';

                    $query .= " c.h1 LIKE '%$word%' OR cd.desc LIKE '%$word%'";

                }


                $query .= ') ORDER BY c.name';

                $return['categories'] = DB::get_rows($query);

            } else {
                $return = $return['article'];
            }

        }

        return $return;

    }

    /**
     * Добавление новую статью или категорию.
     * @param string $name Название статьи или категории.
     * @param string $title Title статьи или категории.
     * @param string $discription Description статьи и категории.
     * @param string $keywords Keywords статьи и категории.
     * @param string $text Текст статьи или описание категории.
     * @param integer $parent_id Родитель статьи или категории.
     * @return boolean
     */
    static function add_art($url, $name, $title, $description, $keywords, $text = '', $synonym, $parent_id = 0)
    {

        $parent_id = (int)$parent_id;

        if (!get_magic_quotes_gpc()) {
            $url = DB::mysql_secure_string($url);
            $name = DB::mysql_secure_string($name);
            $title = DB::mysql_secure_string($title);
            $description = DB::mysql_secure_string($description);
            $keywords = DB::mysql_secure_string($keywords);
            $text = DB::mysql_secure_string($text);
            $synonym = DB::mysql_secure_string($synonym);
        }

        // Проверяем есть ли родитель для новой статьи.
        if ($parent_id != 0) {

            $query = "SELECT id
                      FROM rk_articles
                      WHERE id = $parent_id";
            if (!DB::get_num_rows($query)) {
                return 0;
            }
        }

        $query = "SELECT MAX(pos) AS max_pos
                  FROM rk_articles
                  WHERE parent_id = $parent_id";
        $next_pos = DB::get_field('max_pos', $query) + 1;

        $query = "INSERT INTO rk_articles (id, url, name, text, title, description, keywords, synonym, visible, pos, parent_id)
                  VALUES (0, '$url' ,'$name', '$text', '$title', '$description', '$keywords', '$synonym', 0,
                                  $next_pos, $parent_id)";

        return DB::query($query);
    }

    /**
     * Сохраняет изменения в статье или категории.
     * @param integer $id Id сохраняемой статьи или категории.
     * @param string $name Название статьи или категории.
     * @param string $title Title статьи или категории.
     * @param string $discription Description статьи или категории.
     * @param string $keywords Keywords статьи или категории.
     * @param string $synonym синонимы статьи.
     * @param string $text Текст статьи или описание категории.
     * @return boolean
     */
    static function set_art($id, $url, $name, $title, $description, $keywords, $synonym, $text)
    {

        $id = (int)$id;

        if (!get_magic_quotes_gpc()) {
            $url = DB::mysql_secure_string($url);
            $name = DB::mysql_secure_string($name);
            $title = DB::mysql_secure_string($title);
            $description = DB::mysql_secure_string($description);
            $keywords = DB::mysql_secure_string($keywords);
            $synonym = DB::mysql_secure_string($synonym);
            $text = DB::mysql_secure_string($text);
        }

        $query = "UPDATE rk_articles
                  SET url = '$url',
                      name = '$name',
                      text = '$text',
                      description = '$description',
                      title = '$title',
                      keywords = '$keywords',
                      synonym = '$synonym'
                  WHERE id = $id";

        return DB::query($query);
    }

    /**
     * Удаляет статьи или категорию.
     * @param integer $art_id Id статьи или категории.
     * @return boolean
     */
    static function del_art($art_id)
    {

        $art_id = (int)$art_id;

        $query = "DELETE FROM rk_articles
                  WHERE id = $art_id
                     OR parent_id = $art_id";

        return DB::query($query);
    }

    /**
     * Проверка URL
     * @param string $url URL.
     * @return boolean
     */
    static function check_url($url, $id = 0)
    {

        $query = "SELECT COUNT(url) kol
                  FROM rk_articles
                  WHERE url = '$url'
                  " . ($id ? " AND id != $id" : '');

        return DB::get_row($query);
    }

    /**
     * Изменяет статус отображения статьи или категории.
     * @param integer $art_id Id статьи или категории.
     * @return boolean
     */
    static function show_art($art_id)
    {

        $art_id = (int)$art_id;

        $query = "UPDATE rk_articles
                  SET visible= (IF(visible = 1, 0, 1))
                  WHERE id = $art_id";

        return DB::query($query);
    }

}

?>