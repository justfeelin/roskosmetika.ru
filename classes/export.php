<?php

/**
 * Класс для работы с экспортом
 */
class export
{
    /**
     * Подключение библиотек и открытие файла
     * @param  string $file имя файла
     * @return PHPExcel        массив данных файла
     */
    static function open_file($file)
    {
        self::_includeLibrary();

        return PHPExcel_IOFactory::load(SITE_PATH . 'excel_templates' . DIRECTORY_SEPARATOR . $file);
    }


    /**
     * Функция конвертации html в markdown
     * @param  string $text Текст для конвертации
     * @return srting       Возвращаемый  конвертированный текст
     */
    static function html_to_markdown($text)
    {

        $result = $text;

        if ($text) {
            //подключаем библиотеку, конвертируем в markdown
            require_once(SITE_PATH . 'includes/HTML_To_Markdown.php');

            $text = str_replace(array('&nbsp;', 'Р'), array(' ', 'PPP'), $text);

            $result = new HTML_To_Markdown($text);

            $return = $result->output();
            $return = str_replace('PPP', 'Р', $return);
            return $return;
        }

    }


    /**
     * Преобразует строку  (антитипограф)
     * @param  string $text Строка до преобразования
     * @return string       Строка после преобразования
     */
    static function anti_typograf($text)
    {

        return preg_replace(array("'&nbsp;'", "'&ndash;'", "'&quot;'", "'&amp;'", "'&mdash;'", "'&laquo;'", "'&raquo;'"), array(' ', '-', '"', '&', '-', '"', '"'), trim($text));

    }

    /**
     * Create new excel files
     * @return PHPExcel Object of Excel
     */
    static function create_new_file()
    {
        self::_includeLibrary();

        return new PHPExcel();
    }

    /**
     * Outputs excel file to browser (By download)
     *
     * @param PHPExcel $excel Excel file
     * @param string $name Download name
     * @param bool $xlsm Use Excel 2007 output
     *
     * @throws Exception
     */
    public static function download_excel($excel, $name, $xlsm = false)
    {
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename=' . $name . '.' . ($xlsm ? 'xlsm' : 'xls'));

        $writer = PHPExcel_IOFactory::createWriter($excel, $xlsm ? 'Excel2007' : 'Excel5');
        $writer->save('php://output');

        exit;
    }

    /**
     * Includes PhpExcel library files
     */
    private static function _includeLibrary()
    {
        static $included;

        if ($included === null) {
            require SITE_PATH . 'includes/PHPExcel.php';
            require_once SITE_PATH . 'includes/PHPExcel/IOFactory.php';

            $included = true;
        }
    }
}
