<?php

/**
 * Work with criteo
 */
class criteo
{


    /**
     * js main criteo code
     * @return string  js code in string
     */
    static function start_code()
    {
        return " window.criteo_q = window.criteo_q || []; 
                 window.criteo_q.push( { event: \"setAccount\", account: 27207 },
                                       { event: \"setHashedEmail\", email: \"" . users::session_email() . "\" },
                                       { event: \"setSiteType\", type:
                                        /(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i.test(navigator.userAgent) ? 't' : (/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i.test(navigator.userAgent) ? 'm' : 'd')
        },";

    }

    /**
     * js criteo code for main page
     * @return string  js code in string
     */
    static function main_page()
    {
        return self::start_code() . "{ event: \"viewHome\" });";

    }


    /**
     * js criteo code for list of products page
     * @param  array $products products list
     * @return string  js code in string
     */
    static function product_list ($products)
    {
        $list = '';
        $max = count($products);

        for ($i=0; $i < $max; $i++) { 
           $list .= $products[$i]['id'] . ($i != ($max-1) ? '" , "' : '');

        }

        return self::start_code() . "{ event: \"viewList\", item:[ \"" . $list . "\" ]});";

    }

    /**
     * js criteo code for product page
     * @param  int $product ID product
     * @return string  js code in string
     */
    static function product($product)
    {
        return self::start_code() . "{ event: \"viewItem\", item: \"$product\" });";
    }

    /**
     * Generate criteo tracking code for order
     * @param  array  $order_items  array of order items
     * @return string               JS code 
     */
    static function cart($order_items)
    {
        $quan = count($order_items);

        if ($quan === 0) {
            return '';
        }

        $return = self::start_code() . "{ event: \"viewBasket\", item: [";

        for ($i=0; $i < $quan; $i++) {
            $return .= "{ id: \"{$order_items[$i]['id']}\", price: {$order_items[$i]['price']},  quantity: {$order_items[$i]['quantity']} }";
            $return .= (($quan - 1) == $i) ? '' : ',';
        }

        $return .= ']});';

        return $return;
    }

    /**
     * Generate criteo tracking code for order
     * @param  int    $order_number Order number
     * @param  array  $order_items  array of order items
     * @return string JS code 
     */
    static function ok_page($order_number, $order_items) 
    {

        $return = self::start_code() . "{event: \"trackTransaction\",
            id: \"$order_number\",
            item: [";

        $quan = count($order_items);  

        for ($i=0; $i < $quan; $i++) { 
            $return .= "{ id: \"{$order_items[$i]['id']}\", price: {$order_items[$i]['price']}, quantity: {$order_items[$i]['quantity']} }";
            $return .= (($quan - 1) == $i) ? '' : ',';
          }  

        $return .= ']});';
      
        return $return;
    }
}

?>