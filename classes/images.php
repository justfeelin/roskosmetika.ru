<?php

/*
 * Обработка изображений для загрузки на сайт
 */

class images
{

    /**
     * Изменение размера картинки.
     * @param string $img путь к изменяемой картинке.
     * @param integer $width новая ширина.
     * @param integer $heigh новая высота.
     */

    static function set_img_size($img, $width, $height)
    {
//Создаем изображение в зависимости от типа исходного файла
// для упрощения, считаю, что расширение соответствует типу файла
        switch (strtolower(strrchr($img, '.'))) {
            case ".jpg":
                $src_image = @ImageCreateFromJPEG($img);
                break;
            case ".gif":
                $src_image = @ImageCreateFromGIF($img);
                break;
            case ".png":
                $src_image = ImageCreateFromPNG($img);
                break;
            default:
                return FALSE;
        }


        $src_width = ImageSX($src_image);
        $src_height = ImageSY($src_image);


        if (($width <= $src_width) || ($height >= $src_height)) {
            $ratio_width = $src_width / $width;
            $ratio_height = $src_height / $height;

            if ($ratio_width < $ratio_height) {
                $dest_width = intval($src_width / $ratio_height);
                $dest_height = $height;
            } else {
                $dest_width = $width;
                $dest_height = intval($src_height / $ratio_width);
            }


            $res_image = ImageCreateTrueColor($dest_width, $dest_height);
            imagefill($res_image, 0, 0, 0xffffff);


            ImageCopyResampled($res_image, $src_image, 0, 0, 0, 0, $dest_width, $dest_height, $src_width, $src_height);

            switch (strtolower(strrchr($img, '.'))) {
                case ".jpg":
                    ImageJPEG($res_image, $img, 100); // 100 - максимальное качество
                    break;
                case ".gif":
                    ImageGIF($res_image, $img);
                    break;
                case ".png":
                    ImagePNG($res_image, $img);
                    break;
            }
            return TRUE;
        }
    }

    /**
     * Check photo existence 
     * @param  string $file_name file name
     * @return int               0 or file ID
     */
    static function check_photo($file_name) {

        return (int)DB::get_field('id', "SELECT id FROM __prod_images_map WHERE file_name='$file_name'");
    }

}

?>
