<?php

abstract class ProdCert
{
    /**
     * Directory containing product certificates
     */
    const FOLDERS_DIR = SITE_PATH . 'www/images/prod_cert';

    /**
     * URL containing product certificates
     */
    const FOLDERS_URL = DOMAIN_FULL . '/images/prod_cert';

    /**
     * Returns certificates list
     *
     * @param bool $withImages Include linked images in 'images' key for each data row
     * @param bool $withCounts Include 'files', 'products', 'end_date' keys containing count of corresponding data
     * @param string $sort Sorting key
     * @param bool $sortAscending Sort ascending
     *
     * @return array
     */
    public static function get_list($withImages = true, $withCounts = false, $sort = null, $sortAscending = true)
    {
        $data = DB::get_rows(
            'SELECT * FROM (
                SELECT c.* ' .
                (
                    $withCounts
                        ? ', (SELECT COUNT(cf.id) FROM cdb_certificates_files AS cf WHERE cf.cert_id = c.id) AS files,
                                    (SELECT COUNT(cp.prod_id) FROM cdb_cert_prod AS cp WHERE cp.cert_id = c.id) AS products '
                        : ''
                ) .
            'FROM cdb_certificates AS c
            ) AS c' .
            (
                $sort !== null
                    ? (
                        ' ORDER BY c.' . ($sort === 'files' ? 'files' : ($sort === 'products' ? 'products' : 'end_date')) . ' ' . ($sortAscending ? 'ASC' : 'DESC')
                    )
                    : ''
            )
        );

        if ($withImages) {
            for ($i = 0, $n = count($data); $i < $n; ++$i) {
                $data[$i]['images'] = DB::get_rows('SELECT cf.* FROM cdb_certificates_files AS cf WHERE cf.cert_id = ' . $data[$i]['id']);
            }
        }

        return $data;
    }

    /**
     * Returns product certificate by ID
     *
     * @param int $id Certificate ID
     * @param bool $withImages Include linked images in 'images' key
     *
     * @return array
     */
    public static function get_by_id($id, $withImages = true)
    {
        $id = (int)$id;

        $data = DB::get_row('SELECT c.* FROM cdb_certificates AS c WHERE c.id = ' . $id);

        if ($data) {
            if ($withImages) {
                $data['images'] = DB::get_rows('SELECT cf.* FROM cdb_certificates_files AS cf WHERE cf.cert_id = ' . $id);
            }
        }

        return $data;
    }

    /**
     * Updates product certificate
     *
     * @param int $id Certificate ID. null if new certificate should be created
     * @param string $name Certificate displayed name
     * @param string $folderUrl Images folder name
     * @param string $endDate Certificate end date (YYYY-MM-DD). Can be blank
     *
     * @return int Certificate ID
     *
     * @throws Exception
     */
    public static function update($id, $name, $folderUrl, $endDate)
    {
        $id = (int)$id;

        if (!preg_match('/^[a-z0-9_]+$/i', $folderUrl)) {
            throw new Exception('Неверное название папки');
        }

        if (!!DB::get_field('id', 'SELECT c.id FROM cdb_certificates AS c WHERE c.folder_url = "' . DB::escape($folderUrl) . '"' . ($id ? ' AND c.id !=' . $id : ''))) {
            throw new Exception('Такая папка уже существует');
        }

        if ($id) {
            $oldFolder = DB::get_field('folder_url', 'SELECT c.folder_url FROM cdb_certificates AS c WHERE c.id = ' . $id);

            if ($oldFolder !== $folderUrl) {
                rename(self::FOLDERS_DIR . '/' . $oldFolder, self::FOLDERS_DIR . '/' . $folderUrl);
            }

            DB::query('UPDATE cdb_certificates SET name = "' . DB::escape($name) . '", folder_url = "' . DB::escape($folderUrl) . '", end_date = ' . ($endDate ? '"' . DB::escape($endDate) . '"' : 'NULL') . ' WHERE id = ' . $id);
        } else {
            mkdir(self::FOLDERS_DIR . '/' . $folderUrl);

            chmod(self::FOLDERS_DIR . '/' . $folderUrl, 0775);

            DB::query('INSERT INTO cdb_certificates (name, folder_url, end_date) VALUES ("' . DB::escape($name) . '", "' . DB::escape($folderUrl) . '", ' . ($endDate ? '"' . DB::escape($endDate) . '"' : 'NULL') . ')');

            $id = DB::get_last_id();
        }

        return $id;
    }

    /**
     * Deletes product certificate
     *
     * @param int $id Certificate ID
     */
    public static function delete($id)
    {
        $id = (int)$id;

        helpers::unlink_recursive(self::FOLDERS_DIR . '/' . DB::get_field('folder_url', 'SELECT c.folder_url FROM cdb_certificates AS c WHERE c.id = ' . $id));

        DB::query('DELETE FROM cdb_certificates WHERE id = ' . $id);
        DB::query('DELETE FROM cdb_certificates_files WHERE cert_id = ' . $id);
    }

    /**
     * Adds image to certificate
     *
     * @param int $certID Certificate ID
     * @param string $imageFile Image file path to copy from
     */
    public static function add_image($certID, $imageFile)
    {
        $certID = (int)$certID;

        $folder = DB::get_field('folder_url', 'SELECT c.folder_url FROM cdb_certificates AS c WHERE c.id = ' . $certID);

        if ($folder) {
            DB::query('INSERT INTO cdb_certificates_files (cert_id, file_name) VALUES (' . $certID . ', "")');

            $id = DB::get_last_id();

            $filename = $id . '.jpg';

            DB::query('UPDATE cdb_certificates_files SET file_name = "' . $filename . '" WHERE id = ' . $id);

            copy($imageFile, self::FOLDERS_DIR . '/' . $folder . '/' . $filename);

            chmod(self::FOLDERS_DIR . '/' . $folder . '/' . $filename, 0664);
        }
    }

    /**
     * Updates certificate image
     *
     * @param int $fileID File ID
     * @param string $imageFile New image file path to copy from
     */
    public static function update_image($fileID, $imageFile)
    {
        $fileID = (int)$fileID;

        $filename = DB::get_field(
            'filename',
            'SELECT CONCAT(c.folder_url, "/", cf.file_name) AS filename
            FROM cdb_certificates_files AS cf
            INNER JOIN cdb_certificates AS c ON c.id = cf.cert_id
            WHERE cf.id = ' . $fileID
        );

        if ($filename) {
            unlink(self::FOLDERS_DIR . '/' . $filename);

            copy($imageFile, self::FOLDERS_DIR . '/' . $filename);

            chmod(self::FOLDERS_DIR . '/' . $filename, 0664);
        }
    }

    /**
     * Deletes product certificate file
     *
     * @param int $id File ID
     *
     * @return bool
     */
    public static function delete_image($id)
    {
        $id = (int)$id;

        $image = DB::get_field(
            'image',
            'SELECT CONCAT(c.folder_url, "/", cf.file_name) AS image
            FROM cdb_certificates_files AS cf
            INNER JOIN cdb_certificates AS c ON c.id = cf.cert_id
            WHERE cf.id = ' . $id
        );

        if ($image) {
            unlink(self::FOLDERS_DIR . '/' . $image);

            DB::query('DELETE FROM cdb_certificates_files WHERE id = ' . $id);

            return true;
        }

        return false;
    }

    /**
     * Adds products to category
     *
     * @param int $certificateID Certificate ID
     * @param array $productIDs Product IDs
     */
    public static function add_products($certificateID, $productIDs)
    {
        $certificateID = (int)$certificateID;

        for ($i = 0, $n = count($productIDs); $i < $n; ++$i) {
            $pID = (int)$productIDs[$i];

            if ($pID > 0) {
                DB::query('INSERT INTO cdb_cert_prod (prod_id, cert_id) VALUES (' . $pID . ', ' . $certificateID . ') ON DUPLICATE KEY UPDATE prod_id = prod_id');
            }
        }
    }

    /**
     * Returns linked to certificate products
     *
     * @param int $certificateID Certificate ID
     *
     * @return array
     */
    public static function linked_products($certificateID)
    {
        return DB::get_rows(
            'SELECT p.id, p.name, p.pack, p.short_description
            FROM products AS p
            INNER JOIN cdb_cert_prod AS cp ON cp.prod_id = p.id AND cp.cert_id = ' . ((int)$certificateID)
        );
    }

    /**
     * Removes products from certificate
     *
     * @param int $certificateID Certificate ID
     * @param array $productIDs Product IDs
     */
    public static function remove_products($certificateID, $productIDs)
    {
        $certificateID = (int)$certificateID;

        for ($i = 0, $n = count($productIDs); $i < $n; ++$i) {
            DB::query('DELETE FROM cdb_cert_prod  WHERE cert_id = ' . $certificateID . ' AND prod_id = '. ((int)$productIDs[$i]));
        }
    }
}
