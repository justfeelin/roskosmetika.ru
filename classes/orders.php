<?php

/**
 * Working with orders
 */
class Orders
{
    /**
     * Done order status ID
     */
    const STATUS_ID_DONE = 1;

    /**
     * Cancelled order status ID
     */
    const STATUS_ID_CANCEL = 2;

    /**
     * Current order status ID
     */
    const STATUS_ID_CURRENT = 3;

    /**
     * New order status ID
     */
    const STATUS_ID_NEW = 4;

    /**
     * Returned order status ID
     */
    const STATUS_ID_RETURN = 5;

    /**
     * Assembling order status ID
     */
    const STATUS_ID_ON_STOCK = 7;

    /**
     * Assembled order status ID
     */
    const STATUS_ID_STOCK_DONE = 8;

    /**
     * Delivering order status ID
     */
    const STATUS_ID_DELIVERY = 9;

    /**
     * Cancelled by client order status ID
     */
    const STATUS_ID_CANCEL_CLIENT = 10;

    /**
     * Corrupted order status ID
     */
    const STATUS_ID_CORRUPT = 11;

    /**
     * Products waiting order status ID
     */
    const STATUS_ID_WAITING = 12;

    /**
     * Ready for assembly order status ID
     */
    const STATUS_ID_READY_FOR_ASSEMBLY = 13;

    /**
     * Product ID to use as present
     */
    const PRESENT_PRODUCT_ID = 22222;

    /**
     * Free consultation product ID
     */
    const CONSULTATION_PRODUCT_ID = 12524;

    /**
     * Free help product product ID
     */
    const HELP_PRODUCT_ID = 12525;

    /**
     * Free consultation + help product ID
     */
    const CONSULTANT_HELP_PRODUCT_ID = 12526;

    /**
     * Custom shipping type ID
     */
    const CUSTOM_SHIPPING = 5;

    /**
     * Minimal order sum to get present
     */
    const PRESENT_SUM = 5000;

    /**
     * Minimum order sum to get shipping compensation
     */
    const SHIPPING_COMPENSATION_SUM = 4000;

    /**
     * Shipping compensation for orders exceeding self::SHIPPING_COMPENSATION_SUM
     */
    const SHIPPING_COMPENSATION = 181;

    /**
     * Beznal payment type ID
     */
    const PAYMENT_ID_BEZNAL = 2;

    /**
     * Walletone payment type ID
     */
    const PAYMENT_ID_WALLETONE = 3;

    /**
     * Robokassa payment type ID
     */
    const PAYMENT_ID_ROBOKASSA = 7;

    /**
     * Sberbank acquiring payment type ID
     */
    const PAYMENT_ID_SBER_ACQUIRING = 9;

    /**
     * Payway type 7SKY
     */
    const PAYWAY_ID_7SKY = 1;

    /**
     * Payway type R-Cosmetic
     */
    const PAYWAY_ID_RCOSMETIC = 3;

    /**
     * Session key to store immediate order payment link
     */
    const CERTIFICATE_PAY_SESSION_KEY = 'pay-order-link';

    /**
     * Courier delivery type ID
     */
    const DELIVERY_COURIER_TYPE_ID = 1;

    /**
     * Post delivery type ID
     */
    const DELIVERY_POST_TYPE_ID = 2;

    /**
     * Company delivery type ID
     */
    const DELIVERY_COMPANY_TYPE_ID = 3;

    /**
     * PickPoint delivery type ID
     */
    const DELIVERY_PICKPOINT_TYPE_ID = 4;

    /**
     * Pickup delivery type ID
     */
    const DELIVERY_PICKUP_TYPE_ID = 6;

    /**
     * @var array Shipping types values
     */
    public static $shippings = [
        self::CUSTOM_SHIPPING => '- Не выбрано -',
        1 => 'Курьер',
        2 => 'Почта',
        3 => 'Транспортная компания',
        4 => 'Пункт выдачи или PickPoint',
        6 => 'Самовывоз из офиса',
    ];

    /**
     * Roskosmetika agent ID
     */
    const AGENT_ID_RK = 2;

    /**
     * Admitad agent ID
     */
    const AGENT_ID_ADMITAD = 9;

    /**
     * Admitad-offline agent ID
     */
    const AGENT_ID_ADMITAD_OFFLINE = 10;

    /**
     * Promo-code agent ID
     */
    const AGENT_ID_PROMO = 11;

    /**
     * Promo-code with discount_type = 1 agent ID
     */
    const AGENT_ID_PROMO_SIMPLE = 12;

    /**
     * Criteo agent ID
     */
    const AGENT_ID_CRITEO = 13;

    /**
     * Newsletter agent ID
     */
    const AGENT_ID_NEWSLETTER = 14;

    /**
     * Leadza facebook agent ID
     */
    const AGENT_ID_FB_LEADZA = 15;

    /**
     * Referral agent ID
     */
    const AGENT_ID_REFERRAL = 16;

    /**
     * cancel order
     * @param integer $order_id
     * @param integer $user_id
     * @return array
     */
    static function cancel_order($order_id, $user_id = null)
    {

        $order_id = (int)$order_id;

        $query = 'UPDATE orders
                  SET status_id = ' . self::STATUS_ID_CANCEL_CLIENT . ', client_note = CONCAT("!!! ОТМЕНЁН КЛИЕНТОМ !!! ", client_note)
                  WHERE order_number = ' . $order_id . '
                  AND status_id = ' . self::STATUS_ID_NEW .
                  ($user_id ? ' AND (SELECT p.client_id FROM shipping_persons AS p WHERE p.id = orders.shipping_person_id) = ' . $user_id : '');

        DB::query($query);

        $query = "SELECT o.order_number, o.name, o.surname, o.patronymic,
                         DATE_FORMAT(o.added_date, '%e.%m.%Y') AS added_date, a.region_id
                  FROM orders AS o
                  LEFT JOIN address AS a ON a.id = o.address_id
                  WHERE o.order_number = $order_id
                  LIMIT 1";

        return DB::get_row($query);
    }

    /**
     * CHeck order by id and user
     * @param integer $order_id
     * @param integer $user_id
     * @return array
     */
    static function check_order($order_id, $user_id)
    {
        $order_id = (int)$order_id;
        $user_id = (int)$user_id;

        $query = 'SELECT o.order_number
                  FROM orders AS o
                  INNER JOIN shipping_persons AS p ON p.id = o.shipping_person_id AND p.client_id = ' . $user_id . '
                  WHERE o.order_number = ' .$order_id;

        return DB::get_num_rows($query);
    }

    /**
     * Get all users orders where status != 'uncomp'
     * @param integer $user_id
     * @return array
     */
    static function get_user_orders($user_id)
    {
        $user_id = (int)$user_id;

        $query = "SELECT o.order_number, o.added_date, o.shipping_id, o.shipping_date, o.status_id, o.site_order_number, o.invoice, o.address_id, o.delivery, o.paid, o.cost, o.agent_id, o.payway_id, o.payment_id, o.carrier_id, o.carrier_number,
                  co.delivery_date,
                  IF(
                      od.coupon_id IS NOT NULL,
                      IF(
                          od.admitad_coupon != 0,
                          IFNULL((SELECT ap.discount FROM rk_admitad_promo_codes AS ap WHERE ap.id = od.coupon_id), 0),
                          IFNULL((SELECT p.discount FROM rk_promo_codes AS p WHERE p.id = od.coupon_id), 0)
                      ),
                      0
                  ) AS promo_discount,              
                  (SELECT SUM(pd.amount) FROM payments_done AS pd WHERE pd.order_number = o.order_number) AS fact_paid,
                  c.name AS carrier_name,
                  a.region_id
                  FROM orders AS o
                  LEFT JOIN cdb_orders AS co ON co.order_number = o.order_number
                  LEFT JOIN order_discounts AS od ON od.order_number = o.order_number
                  LEFT JOIN address AS a ON a.id = o.address_id
                  LEFT JOIN carriers AS c ON c.id = o.carrier_id
                  WHERE o.client_id = $user_id AND o.auto_order = 0
                  ORDER BY added_date DESC";

        return DB::get_rows($query);
    }

    /**
     * Get order info by real id
     * @param integer $order_id
     * @return array
     */
    static function get_order_by_real_id($order_id)
    {
        $order_id = (int)$order_id;

        $query = 'SELECT o.*, p.client_id, a.region_id
            FROM orders AS o
            INNER JOIN shipping_persons AS p ON p.id = o.shipping_person_id
            LEFT JOIN address AS a ON a.id = o.address_id
            WHERE o.order_number = ' . $order_id;

        return DB::get_row($query);
    }

    /**
     * Get order info by site order number
     *
     * @param integer $order_id
     * @return array
     */
    public static function get_order_by_site_number($order_id)
    {
        $order_id = (int)$order_id;

        $query = 'SELECT o.*, p.client_id, a.region_id
            FROM orders AS o
            INNER JOIN shipping_persons AS p ON p.id = o.shipping_person_id
            LEFT JOIN address AS a ON a.id = o.address_id
            WHERE o.site_order_number = ' . $order_id;

        return DB::get_row($query);
    }

    /**
     * Get order info by base number
     *
     * @param int $baseNumber Base number
     *
     * @return array
     */
    public static function get_order_by_base_number($baseNumber)
    {
        $query = 'SELECT * FROM orders WHERE order_number = ' . ((int)$baseNumber);

        return DB::get_row($query);
    }

    /**
     * Returns order info by order_number
     *
     * @param int $id Order number
     * @param bool $bySiteID Search by site order number
     * 
     * @return array
     */
    public static function get_by_id($id, $bySiteID = false)
    {
        return DB::get_row('SELECT o.*, a.region_id AS region_id,
                co.date_of_change,
                a.address,
                s.name AS status_name, s.key AS status_key
            FROM orders AS o
            INNER JOIN cdb_orders AS co ON co.order_number = o.order_number
            LEFT JOIN address AS a ON a.id = o.address_id
            LEFT JOIN `status` AS s ON s.id = o.status_id
            WHERE o.' . ($bySiteID ? 'site_order_number' : 'order_number') . ' = ' . (int)$id);
    }

    /**
     * Return order by number
     * @param int $order_number number of order
     * @param bool $skipPresent Do not add present product to result
     *
     * @return array
     */
    static function get_order_by_number($order_number, $skipPresent = false)
    {
        $order_number = (int)$order_number;

        $query = "SELECT o.order_number AS order_id,
                         o.site_order_number,
                         o.payment_id,
                         p.id,
                         IF(p.main_id = 0, p.name, pd.name) AS name,
                         p.pack,
                         c.price AS old_price,
                         c.final_price AS price,
                         IF(p.main_id = 0, p.url, pd.url) AS url,
                         pp.src,
                         c.quantity,
                         (c.quantity * c.final_price) AS sum
                  FROM products AS p
                  INNER JOIN orders AS o ON o.order_number =  $order_number
                  INNER JOIN orders_items AS c ON c.order_number = o.order_number AND c.product_id = p.id
                  LEFT JOIN products AS pd ON p.main_id != 0 AND pd.id = p.main_id
                  LEFT JOIN rk_prod_photo AS pp ON pp.id = p.id
                  " . ($skipPresent ? 'WHERE c.product_id != ' . self::PRESENT_PRODUCT_ID : '') . "
                  ORDER BY p.name, c.price";

        return DB::get_rows($query);
    }

    /**
     * Returns amount to be paid
     *
     * @param int $order_number Order number
     *
     * @return float
     */
    public static function to_pay($order_number)
    {
        return (float)DB::get_field('to_pay', 'SELECT (o.cost - IF((SELECT SUM(p.amount) FROM payments_done AS p WHERE p.order_number = o.order_number) IS NOT NULL, (SELECT SUM(p.amount) FROM payments_done AS p WHERE p.order_number = o.order_number), 0)) AS to_pay
            FROM orders AS o
            WHERE o.order_number = ' . ((int)$order_number) . '
            LIMIT 1');
    }

    /**
     * Returns order cart items by order ID
     *
     * @param int $orderID Order ID
     *
     * @return array
     */
    public static function get_order_products($orderID)
    {
        return DB::get_rows('SELECT o.order_number AS order_id,
                         p.id,
                         IF(p.main_id = 0, p.name, pd.name) AS name,
                         p.pack,
                         c.price AS old_price,
                         c.final_price AS price,
                         IF(p.main_id = 0, p.url, pd.url) AS url,
                         pp.src,
                         c.quantity,
                         (c.quantity * c.final_price) sum
                  FROM products AS p
                  INNER JOIN orders AS o ON o.order_number = ' . ((int)$orderID) . '
                  INNER JOIN orders_items AS c ON c.order_number = o.order_number AND c.product_id = p.id
                  LEFT JOIN products AS pd ON p.main_id != 0 AND pd.id = p.main_id
                  LEFT JOIN rk_prod_photo AS pp ON pp.id = p.id
                  ORDER BY p.name, c.price');
    }

    /**
     * Returns order's products sum
     *
     * @param int $order_number Order number
     *
     * @return int
     */
    public static function get_order_products_sum($order_number)
    {
        return (int)DB::get_field('sm', 'SELECT SUM(c.quantity * c.final_price) AS sm
            FROM orders_items AS c
            INNER JOIN orders AS o ON o.order_number = c.order_number AND o.order_number = ' . (int)$order_number);
    }

    /**
     * Creates new order & order items
     *
     * @param string $name Client name
     * @param string $surname Client surname
     * @param string $patronymic Client patronymic
     * @param string $email Client email
     * @param string $phone Client phone
     * @param int $clientID Client ID
     * @param array $products Products array. Each element can be number representing product ID or array with required "id" key and optional keys: "quantity", "price", "oldPrice"
     * @param string $note Order note
     * @param int $discount Order discount value
     * @param string $address Order address value
     * @param int $regionID Order address region ID
     * @param int $shippingTypeID Shipping type ID
     * @param int $shippingPrice Shipping price
     * @param int $agentID Agent ID
     * @param bool $autoOrder Mark order as automatically generated
     * @param int $finalCost Order final cost
     * @param array $pickupData Pickup data [`Pickup code ("pickpoint", "sdek"), `Pickup point code`]
     *
     * @return array [order_number, site_order_number]
     */
    public static function insert_order($name, $surname, $patronymic, $email, $phone, $clientID, $products, $note, $discount = 0, $address = '', $regionID = null, $shippingTypeID = null, $shippingPrice = null, $agentID = null, $autoOrder = false, $finalCost = null, $pickupData = null)
    {
        $shippingTypeID = $shippingTypeID ? (int)$shippingTypeID : self::CUSTOM_SHIPPING;

        $personID = self::shipping_person($name, $surname, $patronymic, $phone, $email, $clientID);
        $addressID = self::address_id($address, $regionID, '', $clientID, $pickupData);

        $randomID = self::random_id();

        $cost = 0;

        $hasCertificate = false;

        $original_cost = 0;

        for ($i = 0, $productsN = count($products); $i < $productsN; ++$i) {
            if (is_numeric($products[$i])) {
                $product = Product::get_by_id($products[$i]);

                if ($product) {
                    $oldPrice = roundPrice($product['price']);

                    $price = $discount > 0 && $product['apply_discount'] ? roundPrice($oldPrice * (100.0 - $discount) / 100.0) : $oldPrice;

                    $specialPrice = (int)$product['special_price'] > 0 ? roundPrice($product['special_price']) : 0;

                    if ($specialPrice > 0 && $specialPrice < $price) {
                        $price = $specialPrice;
                    }

                    $products[$i] = [
                        'id' => $products[$i],
                        'quantity' => 1,
                        'oldPrice' => $oldPrice,
                        'price' => $price,
                    ];
                }
            } else {
                $product = DB::get_row('SELECT p.price, p.special_price, p.apply_discount FROM products AS p WHERE p.id = ' . $products[$i]['id']);
            }

            if (!$product) {
                array_splice($products, $i, 1);

                --$i;
                --$productsN;

                continue;
            }

            if (!isset($products[$i]['quantity'])) {
                $products[$i]['quantity'] = 1;
            }

            if (!isset($products[$i]['price'])) {
                $products[$i]['price'] = (int)$product['special_price'] ? $product['special_price'] : $product['price'];
            }

            if (!isset($products[$i]['oldPrice'])) {
                $products[$i]['oldPrice'] = $product['price'];
            }

            if ($products[$i]['id'] == Product::CERTIFICATE_ID) {
                $hasCertificate = true;
            }

            $products[$i]['price'] = roundPrice($products[$i]['price']);
            $products[$i]['oldPrice'] = roundPrice($products[$i]['oldPrice']);
            $products[$i]['specialPrice'] = roundPrice($product['special_price']);
            $products[$i]['applyDiscount'] = (int)$product['apply_discount'];

            $cost += $products[$i]['price'] * $products[$i]['quantity'];

            //  calculate original cost
            $original_cost += $products[$i]['oldPrice'] * $products[$i]['quantity'];

        }

        $cost += (int)$shippingPrice;

        $query = 'INSERT INTO orders (status_id, client_id, agent_id, site_order_number, added_date, name, surname, patronymic, phone, email, address_id, shipping_person_id, shipping_id, delivery, client_note, cost, original_cost, discount, auto_order)
            VALUES (' . ($hasCertificate ? self::STATUS_ID_CURRENT : self::STATUS_ID_NEW) . ', ' . $clientID . ', ' . ($agentID ?: Orders::AGENT_ID_RK). ', ' . $randomID . ', NOW(), "' . DB::escape($name) . '", "' . DB::escape($surname) . '", "' . DB::escape($patronymic) . '", "' . DB::escape($phone) . '", "' . DB::escape($email) . '", ' . $addressID . ', ' . $personID . ', ' . $shippingTypeID . ', ' . ($shippingPrice === null ? 'NULL' : (int)$shippingPrice) . ', "' . DB::escape($note) . '", ' . ((int)$finalCost ?: $cost) . ', ' . $original_cost . ', ' . $discount . ', ' . ($autoOrder ? 1 : 0) . ')';
        DB::query($query);

        $newOrderId = DB::get_last_id();

        for ($i = 0; $i < $productsN; ++$i) {
            DB::query('INSERT INTO orders_items (order_number, product_id, quantity, price, special_price, final_price, apply_discount, position)
                VALUES (' . $newOrderId .', ' . $products[$i]['id'] . ', ' . $products[$i]['quantity'] . ', ' . ($products[$i]['id'] == Product::CERTIFICATE_ID ? $products[$i]['price'] : $products[$i]['oldPrice']) . ', ' . $products[$i]['specialPrice'] . ', ' . $products[$i]['price'] . ', ' . ($products[$i]['applyDiscount'] ? '1' : '0') . ', ' . ($i + 1) . ')');
        }

        return [
            $newOrderId,
            $randomID,
        ];
    }

    /**
     * Create new order.
     *
     * @param string $name Client name
     * @param string $surname Client surname
     * @param string $patronymic Client patronymic
     * @param string $organization Client organization type
     * @param string $phone Client phone number
     * @param string $email Client email
     * @param array $region Shipping region row
     * @param int $shippingTypeID Shipping type ID
     * @param string $address Shipping address
     * @param string $note Order note
     * @param array $cartInfo Cart info from Cart::$data
     * @param int $clientID Client ID
     * @param bool|string $sendManagerEmail Whether to send confirmation email to client. If string - email subject
     * @param bool $updateRkOrder Set order_number for rk_order
     * @param bool $autoOrder Mark order as automatically generated
     * @param array $pickupData Pickup data [`Pickup code ("pickpoint", "sdek"), `Pickup point code`]
     *
     * @return array [order_id, random_order_id]
     */
    static function new_order($name, $surname, $patronymic, $organization, $phone, $email, $region, $shippingTypeID, $address, $note, $cartInfo, $clientID = 0, $sendManagerEmail = true, $updateRkOrder = true, $autoOrder = false, $pickupData = null)
    {
        $cost = (int)$cartInfo['info']['final_sum'];

        $orderID = Cart::orderID();

        $regionID = $region ? (int)$region['id'] : 0;

        $agentID = DB::get_field('agent_id', '(SELECT o.agent_id FROM rk_orders AS o WHERE o.id = ' . $orderID . ' LIMIT 1)');

        $products = [];

        for ($i = 0, $n = count($cartInfo['products']); $i < $n; ++$i) {
            $addProduct = [
                'id' => $cartInfo['products'][$i]['id'],
                'quantity' => $cartInfo['products'][$i]['quantity'],
                'price' => $cartInfo['products'][$i]['price'],
            ];

            if ($cartInfo['products'][$i]['id'] != Product::CERTIFICATE_ID) {
                DB::query(
                    'UPDATE rk_orders_items SET
                    price = ' . ((int)$cartInfo['products'][$i]['price']) . ',
                    special_price = (SELECT p.special_price FROM products AS p WHERE p.id = ' . $cartInfo['products'][$i]['id'] . '),
                    apply_discount = (SELECT p.apply_discount FROM products AS p WHERE p.id = ' . $cartInfo['products'][$i]['id'] . ')
                    WHERE product_id = ' . $cartInfo['products'][$i]['id'] . ' AND rk_order_id = ' . $orderID
                );
            }

            $products[] = $addProduct;
        }

        unset($addProduct);

        list($newOrderId, $randomID) = self::insert_order($name, $surname, $patronymic, $email, $phone, $clientID, $products, $note, (int)$cartInfo['info']['discount'], $address, $regionID, $shippingTypeID, $cartInfo['info']['shipping_price'], $agentID, $autoOrder, $cartInfo['info']['final_sum'], $pickupData);

        if ($updateRkOrder) {
            DB::query('UPDATE rk_orders SET order_number = ' . $newOrderId . ', end_date = NOW() WHERE id = ' . $orderID);
        }

        DB::query('INSERT INTO order_discounts (order_number, coupon_id, referral_id, sum_discount, client_auto_discount, admitad_coupon, own_referral_discount, client_main_discount, custom_discount) SELECT
            ' . $newOrderId . ',
            o.promo_code_id,
            o.referral_id,
            ' . $cartInfo['info']['discounts']['sum'] . ',
            ' . $cartInfo['info']['discounts']['clientAuto'] . ',
            o.admitad_promo,
            o.own_referral_discount,
            ' . $cartInfo['info']['discounts']['clientManual'] . ',
            0
            FROM rk_orders AS o
            WHERE o.id = ' . $orderID . '
        ');

        if (check::email($email)) {
            @unsubscribe::add_self($email, 'self', 'roskosmetika.ru', $name, $surname, $patronymic, $regionID);
        }

        DB::query('INSERT INTO cdb_orders (order_number, old_delivery, discount_sum) VALUES (' . $newOrderId . ', ' . ((int)$cartInfo['info']['shipping_price_original'] ?: 0) . ', (SELECT SUM(oi.price * oi.quantity) - ' . $cartInfo['info']['promo_sum'] . ' FROM orders_items AS oi WHERE oi.order_number = ' . $newOrderId . '))');

        if ($sendManagerEmail) {
            //  mail to manager
            $subject = is_string($sendManagerEmail) ? $sendManagerEmail : 'Заказ Роскосметика -';

            if ($region) {
                $subject .= ' ' . $region['name'];
            }

            if (!empty($_SESSION[referral::REFERRAL_KEY])) {
                referral::addActivation($_SESSION[referral::REFERRAL_KEY]['id']);

                $_SESSION[referral::REFERRAL_KEY] = 0;

                setcookie(referral::REFERRAL_KEY, '', $_SERVER['REQUEST_TIME'] - 9999999, '/');
            } elseif ($clientID && $cartInfo['info']['discount_code'] === Cart::OWN_REFERRAL_CODE) {
                referral::addUsage($clientID);
            }

            $subject .= ' Р-' . $randomID;

            $id = $clientID ?: '';

            $email = DEBUG ? env('ADMIN_EMAIL') : $email;

            if ($email) {
                Template::$utm_source = 'site';
                Template::$utm_medium = 'newsletter';
                Template::$utm_campaign = 'order-ok';
                Template::$utm_term = $email;
            }

            $content = [
                'order_id' => $randomID,
                'order_number' => $newOrderId,
                'order_time' => date("H:i:s"),
                'order_date' => date("d.m.Y"),
                'products' => $cartInfo['products'],
                'discount_sum' => $cost,
                'name' => $name,
                'surname' => $surname,
                'patronymic' => $patronymic,
                'organization' => $organization,
                'address' => $address,
                'phone' => $phone,
                'region' => $region ? $region['name'] : null,
                'duration' => $region ? (string)($region['duration_min'] . '-' . $region['duration_max'] . ' ' . helpers::num_ending((int)$region['duration_max'])) : NULL,
                'email' => $email,
                'id' => $id,
                'etc' => $note,
            ] + Cart::$data['info'];

            $content['shipping_info'] = ($content['shipping_price_original'] == NULL ? orders::shipping_info($content['final_sum']) : NULL);

            $send_to = DEBUG ? [
                env('ADMIN_EMAIL') => 'Заказ с roskosmetika.ru (DEBUG)',
            ] : [
                '2255483@mail.ru' => 'Заказ с roskosmetika.ru',
                'roskosmetika_it@mail.ru' => 'Заказ с roskosmetika.ru',
            ];

            $send_from = [
                'sales@roskosmetika.ru' => 'Роскосметика',
            ];

            $message_to_manager = Template::mail_template('mail_order_manager_confirm', 'Заказ Роскосметика', $content);
            @mailer::mail($subject, $send_from, $send_to, $message_to_manager);
        }

        if ($cartInfo['info']['present'] !== false) {
            Orders::add_present($newOrderId);
        }

        return [
            $newOrderId,
            $randomID,
        ];
    }

    /**
     * Add new Pre order
     * @param  string $name users name
     * @param  string $surname users surname
     * @param  string $patronymic users patronymic
     * @param  int $user_id users id (if isset)
     * @param  string $phone users phone
     * @param  int $prod_id product id
     * @param  string $email email
     * @param  int $check_email flag for send mail
     * @param  int $check_sms flag for send sms
     * @return BOOL               query success of fail
     */
    static function new_pre_order($prod_id, $user_id, $surname, $name, $patronymic, $email, $phone,$check_email,$check_sms)
    {
        $prod_id = (int)$prod_id;
        $user_id = (int)$user_id;

        $query = "INSERT INTO rk_pre_order (prod_id, client_id, order_number,by_email,by_sms, name, email, phone, date)
                         VALUES ($prod_id, $user_id, 0, $check_email, $check_sms, '" . DB::escape($surname . ' ' . $name . ' ' . $patronymic) . "', '" . DB::escape($email) . "', '" . DB::escape($phone) . "', NOW())";

        return !!DB::query($query);
    }

    /**
     * Returns shipping region by ID, or all regions if no ID provided
     *
     * @param int $regionID Region ID
     * @param bool $orderByPriority Order regions list by "priority" field
     *
     * @return array|null
     */
    public static function get_regions ($regionID = null, $orderByPriority = false)
    {
        return $regionID === null ? DB::get_rows('SELECT * FROM shipping_regions ORDER BY ' . ($orderByPriority ? 'priority, ' : '') . 'name') : ($regionID > 0 ? DB::get_row('SELECT * FROM shipping_regions WHERE id = ' . (int)$regionID) : null);
    }

    /**
     * Checks POST against region & shipping type values (`region` and `shipping` keys)
     *
     * @return array [regionInfo, shippingTypeCode]
     */
    public static function checkPostShipping()
    {
        $region = $shippingTypeCode = null;

        if (!empty($_POST['region']) && $_POST['region'] > 0) {
            $region = self::get_regions($_POST['region']);

            if ($region) {
                if (
                    !empty($_POST['shipping'])
                    && isset(self::$shippings[$_POST['shipping']])
                    && ($_POST['shipping'] == self::CUSTOM_SHIPPING || isset($region['price_' . $_POST['shipping']]))
                ) {
                    $shippingTypeCode = (int)$_POST['shipping'];
                } else {
                    $min = $minID = null;

                    foreach (self::$shippings as $id => $name) {
                        if ($id === self::CUSTOM_SHIPPING) {
                            continue;
                        }

                        if ($min === null || $region['price_' . $id] !== null && $region['price_' . $id] < $min) {
                            $min = $region['price_' . $id];
                            $minID = $id;
                        }
                    }

                    if ($minID !== null) {
                        $shippingTypeCode = $minID;
                    }
                }
            }
        }

        return [
            $region,
            $shippingTypeCode
        ];
    }

    /**
     * Marks order as paid
     *
     * @param int $orderID Order number
     */
    public static function mark_payment($orderID)
    {
        DB::query('UPDATE orders SET paid = 1 WHERE order_number = ' . ((int)$orderID));
    }

    /**
     * Returns whether order with given referral ID exists
     *
     * @param int $referralID Referral code ID
     * @param int $userID Order user ID
     *
     * @return bool
     */
    public static function check_referral_order($referralID, $userID = 0)
    {
        return !!DB::get_field('order_number', 'SELECT o.order_number
            FROM orders AS o
            INNER JOIN order_discounts AS od ON od.order_number = o.order_number AND od.referral_id = ' . ((int)$referralID) .
            ($userID ? ' INNER JOIN clients AS c ON c.id = ' . ((int)$userID) . ' AND c.id = (SELECT sp.client_id FROM shipping_persons AS sp WHERE sp.id = o.shipping_person_id LIMIT 1)': '') .
            ' WHERE o.status_id != ' . self::STATUS_ID_CANCEL . ' AND o.status_id != ' . self::STATUS_ID_CANCEL_CLIENT . ' LIMIT 1');
    }

    /**
     * Returns last pre order ID
     *
     * @return int
     */
    public static function get_max_pre_order_id()
    {
        return (int)DB::get_field('max_id', 'SELECT MAX(o.id) AS max_id FROM rk_pre_order AS o');
    }

    /**
     * Returns shipping person ID. Creates new one if nothing found
     * @param string $name Person name
     * @param string $surname Person surname
     * @param string $patronymic Person patronymic
     * @param string $phone Person phone number
     * @param string $email Person email
     * @param int $clientID Client ID
     *
     * @return int
     */
    public static function shipping_person($name, $surname, $patronymic, $phone, $email, $clientID = 0)
    {
        $clientID = (int)$clientID;

        $name = DB::mysql_secure_string($name);
        $surname = DB::mysql_secure_string($surname);
        $patronymic = DB::mysql_secure_string($patronymic);
        $phone = DB::mysql_secure_string($phone);
        $email = DB::mysql_secure_string($email);

        $personID = (int)DB::get_field('id', 'SELECT p.id
            FROM shipping_persons AS p
            WHERE p.name =  "' . $name . '"
                AND p.surname = "' . $surname . '"
                AND p.patronymic = "' . $patronymic . '"
                AND p.phone = "' . $phone . '"
                AND p.email = "' . $email . '"
                AND p.client_id = ' . $clientID
        );

        if (!$personID) {
            DB::query('INSERT INTO shipping_persons (client_id, name, surname, patronymic, email, phone, is_default) VALUES (' . $clientID . ', "' . $name . '", "' . $surname . '", "' . $patronymic . '", "' . $email . '", "' . $phone . '", ' . ($clientID ? 'IF((SELECT COUNT(a.id) FROM address AS a WHERE a.client_id = ' . $clientID. ') > 0, 0, 1)' : '0') . ')');

            $personID = DB::get_last_id();
        }

        return $personID;
    }

    /**
     * Returns shipping address ID. Creates new one if nothing found
     * @param string $address Address
     * @param int $regionID Region ID
     * @param string $note Address note
     * @param int $clientID Client ID
     * @param array $pickupData Pickup data [`Pickup code ("pickpoint", "sdek"), `Pickup point code`]
     *
     * @return int
     */
    public static function address_id($address, $regionID, $note, $clientID = 0, $pickupData = null)
    {
        $clientID = (int)$clientID;
        $regionID = (int)$regionID;

        $address = DB::mysql_secure_string($address);

        $addressID = (int)DB::get_field('id', 'SELECT a.id
            FROM address AS a
            WHERE a.address =  "' . DB::mysql_secure_string($address). '"
                AND a.region_id = ' . $regionID . '
                AND a.client_id = ' . $clientID);

        if (!$addressID) {
            DB::query('INSERT INTO address (address, region_id, address_note, client_id, `is_default`, actual, addr_type, code) VALUES ("' . $address . '", ' . $regionID . ', "' . DB::mysql_secure_string($note) . '", ' . $clientID . ', ' . ($clientID ? 'IF((SELECT COUNT(a.id) FROM address AS a WHERE a.client_id = ' . $clientID. ') > 0, 0, 1)' : '0') . ', 1, "' . ($pickupData ? ($pickupData[0] === 'pickpoint' ? 'pickpoint' : 'sdek') : 'home') . '", "' . ($pickupData ? DB::escape($pickupData[1]) : '') . '")');

            $addressID = DB::get_last_id();
        }

        return $addressID;
    }

    /**
     * Adds free present product into order
     *
     * @param int $orderID Order ID
     */
    public static function add_present($orderID)
    {
        $orderID = (int)$orderID;

        if (!DB::get_row('SELECT c.id FROM orders_items AS c WHERE c.order_number = ' .$orderID . ' AND c.product_id = ' . Orders::PRESENT_PRODUCT_ID)) {
            DB::query('INSERT INTO orders_items (order_number, product_id, quantity, price, final_price, apply_discount, position) VALUES (' . $orderID . ', ' . Orders::PRESENT_PRODUCT_ID . ', 1, 0, 0, 0, (SELECT COUNT(oi.id) + 1 FROM orders_items AS oi WHERE oi.order_number = ' . $orderID .'))');
        }
    }

    /**
     * Returns random ID not used at the moment
     *
     * @return int
     */
    public static function random_id()
    {
        do {
            $randomID = rand(1000000, 9999999);
        } while (!!DB::get_field('order_number', 'SELECT order_number FROM orders WHERE site_order_number = ' . $randomID));

        return $randomID;
    }

    /**
     * Sets site_order_number for order
     *
     * @param int $orderID Order number
     * @param int $randomID Site order number to set
     *
     * @return bool
     */
    public static function set_site_order_number($orderID, $randomID)
    {
        DB::query('UPDATE orders SET site_order_number = ' . ((int)$randomID) . ' WHERE order_number = ' . ((int)$orderID) . ' AND site_order_number = 0');

        return DB::last_query_info() !== 0;
    }

    /**
     * Marks order as current, sets callback day to now
     *
     * @param int $orderNumber Order number
     */
    public static function mark_as_current_callback($orderNumber)
    {
        DB::query('UPDATE orders SET status_id = ' . Orders::STATUS_ID_CURRENT . ' WHERE order_number = ' . (int)$orderNumber);
        DB::query('UPDATE cdb_orders SET call_back_date = NOW() WHERE order_number = ' . (int)$orderNumber);
    }

    /**
     * Updates order status
     *
     * @param int $orderNumber Order number
     * @param int $statusID New status ID
     */
    public static function set_status($orderNumber, $statusID)
    {
        DB::query('UPDATE orders SET status_id = ' . ((int)$statusID) . ' WHERE order_number = ' . ((int)$orderNumber));
    }

    /**
     * Returns region's shipping compensations
     *
     * @param int $regionID Region ID
     *
     * @return array
     */
    public static function get_region_compensations($regionID)
    {
        return DB::get_rows('SELECT src.* FROM shipping_regions_compensations AS src WHERE src.region_id = ' . ((int)$regionID) . ' ORDER BY src.minimal_sum');
    }

    /**
     * Deletes region shipping compensation
     *
     * @param int $compensationID Compensation ID
     *
     * @return bool
     *
     */
    public static function delete_region_compensation($compensationID)
    {
        return !!DB::query('DELETE FROM shipping_regions_compensations WHERE id = ' . ((int)$compensationID));
    }

    /**
     * Updates region shipping compensation
     *
     * @param int $compensationID Compensation ID
     * @param int $minimalSum Minimal order sum
     * @param int $compensationCourier Courier compensation value
     * @param int $compensationPost Post compensation value
     * @param int $compensationCompany Company compensation value
     * @param int $compensationPickPoint PickPoint compensation value
     * @param int $compensationPickup Pickup compensation value
     */
    public static function update_region_compensation($compensationID, $minimalSum, $compensationCourier, $compensationPost, $compensationCompany, $compensationPickPoint, $compensationPickup)
    {
        DB::query(
            'UPDATE shipping_regions_compensations SET minimal_sum = ' . ((int)$minimalSum) .
            ', compensation_1 = ' . ($compensationCourier === '' ? 'NULL' : (int)$compensationCourier) .
            ', compensation_2 = ' . ($compensationPost === '' ? 'NULL' : (int)$compensationPost) .
            ', compensation_3 = ' . ($compensationCompany === '' ? 'NULL' : (int)$compensationCompany) .
            ', compensation_4 = ' . ($compensationPickPoint === '' ? 'NULL' : (int)$compensationPickPoint) .
            ', compensation_6 = ' . ($compensationPickup === '' ? 'NULL' : (int)$compensationPickup) .
            ' WHERE id = ' . ((int)$compensationID)
        );
    }

    /**
     * Creates region shipping compensation
     *
     * @param int $regionID Region ID
     * @param int $minimalSum Minimal order sum
     * @param int $compensationCourier Courier compensation value
     * @param int $compensationPost Post compensation value
     * @param int $compensationCompany Company compensation value
     * @param int $compensationPickPoint PickPoint compensation value
     * @param int $compensationPickup Pickup compensation value
     */
    public static function add_region_compensation($regionID, $minimalSum, $compensationCourier, $compensationPost, $compensationCompany, $compensationPickPoint, $compensationPickup)
    {
        DB::query('INSERT INTO shipping_regions_compensations (region_id, minimal_sum, compensation_1, compensation_2, compensation_3, compensation_4, compensation_6) VALUES (' . ((int)$regionID) . ', ' . ((int)$minimalSum) . ', ' . ($compensationCourier === '' ? 'NULL' : (int)$compensationCourier) . ', ' . ($compensationPost === '' ? 'NULL' : (int)$compensationPost) . ', ' . ($compensationCompany === '' ? 'NULL' : (int)$compensationCompany) . ', ' . ($compensationPickPoint === '' ? 'NULL' : (int)$compensationPickPoint) . ', ' . ($compensationPickup === '' ? 'NULL' : (int)$compensationPickup) . ')');
    }

    /**
     * Return shipping info for no region
     * @param  float $final_sum  orders final sum
     * @return string            shipping info (min and max price)
     */
    public static function shipping_info($final_sum)
    {
        $data = DB::get_rows("SELECT * FROM rk_shipping_info WHERE 1");

        $return = 'Вы не выбрали регион или способ доставки.<br>';

        $return .= 'Минимальная сумма доставки - ' . $data[0]['price'] . 
                       "руб. {$data[0]['region']}({$data[0]['shipping_type']}, доставка: {$data[0]['duration_min']}-{$data[0]['duration_max']} " .
                       helpers::num_ending((int)$data[0]['duration_max']) . ').<br>' ;

        $return .= 'Максимальная сумма доставки - ' . $data[1]['price'] . 
                       "руб. {$data[1]['region']}({$data[1]['shipping_type']}, доставка: {$data[1]['duration_min']}-{$data[1]['duration_max']} " .
                       helpers::num_ending((int)$data[1]['duration_max']) . ').<br>' ;


        if($final_sum >= 5500) {
            $return .= 'Для Вашего заказа возможна бесплатная доставка по Москве и Санкт-Петербургу.<br>';
        } 

        if($final_sum > self::SHIPPING_COMPENSATION_SUM && $final_sum < 5500) {

            $return .= "Для Вашего заказа возможна компенсация доставки до {$data[2]['price']} руб.<br>";

        }

        return $return;
    }

    /**
     * Calculate max and min shipping info
     * @param  string $type         data type
     * @param  int    $field        field number
     * @param  array  $exceptions   exception id for shipping regions
     * @return array                data array
     */
    public static function calculate_shipping_info($type, $field, $exceptions) 
    {
        return DB::get_row('SELECT sr.price_' . $field . ' AS price, sr.name AS region, st.name AS shipping_type, sr.duration_min As duration_min, sr.duration_max  AS duration_max 
                            FROM shipping_regions sr LEFT JOIN shipping_types st ON st.id = ' . $field . '  
                            WHERE price_' . $field . ' = (SELECT '. $type .'(sr.price_'. $field . ') FROM shipping_regions sr WHERE sr.id NOT IN(' . implode(', ', $exceptions) . ') AND sr.price_' . $field . ' <> 0 AND sr.price_' . $field . ' IS NOT NULL) ORDER BY sr.priority ASC LIMIT 1');
    }
}
