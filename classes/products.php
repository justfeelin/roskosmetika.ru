<?php

/**
 * Working with products.
 */
class products
{

    /**
     * Return random products
     * @return array
     */
    static function get_rand_products()
    {
        //get random products
        $query = 'SELECT p.id, p.url, p.name, p.pack, p.short_description, p.price, p.special_price, p_p.src, 
                        IF(((SELECT COUNT(pr.id)
                             FROM products AS pr
                             WHERE pr.main_id = p.id AND pr.visible = 1 AND pr.active = 1) > 0), 1, 0) other_pack
                  FROM products AS p
                  LEFT JOIN rk_prod_photo p_p ON ( p_p.id = p.id )
                  WHERE p.visible = 1
                  AND p.active = 1
                  AND p.available =1
                  AND p.main_id = 0
                  LIMIT 10';

        $products = DB::get_rows($query);

        $result = array();
        //set other packs for product (if it exist)
        foreach ($products as $key => $product) {

            if ($product['other_pack']) {

                $query = "SELECT p.pack, p.price, p.special_price, p_p.src
                              FROM products p 
                              LEFT JOIN rk_prod_photo p_p ON ( p_p.id = p.id ) 
                              WHERE p.main_id = {$product['id']} AND p.visible = 1 AND p.active = 1";

                $products[$key]['other_pack'] = DB::get_rows($query);

            }
        }


        return $result;
    }


    /**
     * Get info for non-main packs
     * @param  array $products array of products
     * @return array           array of products with full info
     */
    static function get_pack_info($products)
    {
        for ($i = 0, $n = count($products); $i < $n; ++$i) {
            if ($products[$i]['main_id']) {
                $update = DB::get_row("SELECT IF(url =  '', id, url ) AS url, name, available, visible FROM products WHERE id = {$products[$i]['main_id']}");

                $products[$i]['url'] = $update['url'];
                $products[$i]['name'] = $update['name'];

                if (isset($products['available'])) {
                    $products['available'] = $update['available'];
                }

                if (isset($products[$i]['visible'])) {
                    $products[$i]['visible'] = $update['visible'];
                }
            }

            unset($products[$i]['main_id']);
        }

        return $products;
    }


    /**
     * Output products, filters  for special group of products
     *
     * @param int $id special group ID
     * @param bool $ajax Return only products
     *
     * @return array products of special group with filters
     */
    static function special_group_output($id, $ajax)
    {
        $data = $sortOptions = [];

        switch ($id) {
            case 'new':
                $condition = 'p.new = 1';

                $data['sort_popularity'] = true;

                $sortOptions['new'] = true;

                break;

            case 'hit':
                $condition = 'p.hit = 1';
        
                break;

            case 'sales':
                $condition = '(p.special_price > 0 OR (SELECT COUNT(psq.id) FROM products psq WHERE psq.main_id = p.id AND psq.special_price > 0 AND p.special_price = 0) > 0)';

                $data['sort_popularity'] = true;

                $sortOptions['sales'] = true;

                break;

            case 'hurry':
                $condition = '(p._foreign_stock_remains = 1 OR (SELECT COUNT(psq.id) FROM products psq WHERE psq.main_id = p.id AND psq._foreign_stock_remains = 1 AND p._foreign_stock_remains = 0) > 0)';

                break;

            default:
                return null;

                break;
        }

        $filter_condition = $ajax ? Product::filter_condition('p', false) : '';
        $tm_condition = $ajax ? Product::tm_condition('p') : '';
        $price_condition = $ajax ? Product::price_condition('p') : '';
        $cat_condition = $ajax ? Product::category_condition('p') : '';
        $country_condition = $ajax ? Product::country_condition('p') : '';
        $filter_order = Product::filter_order('p', false, $sortOptions);

        $innerJoins = ' INNER JOIN cdb_products AS cp ON cp.id = p.id
            INNER JOIN rk_prod_desc pd ON (p.id = pd.id)
            INNER JOIN tm ON tm.id = p.tm_id';

        $where = $condition . ' AND p.main_id = 0 AND p.visible = 1 AND (p.active = 1 OR p.price = 0 AND p.new = 1)' . (TM_FOR_PROF && $id != 'hurry' ? ' AND tm.prof = 1 ' : '');

        $fullWhere = $where . $filter_condition . $tm_condition . $price_condition . $cat_condition . $country_condition;

        Pagination::setValues((int)DB::get_field('cnt', 'SELECT COUNT(p.id) AS cnt FROM products AS p ' . $innerJoins . ' WHERE ' . $fullWhere));

        $query = 'SELECT p.id, IF(p.url = "", p.id, p.url ) AS url, p.main_id, p.name, p.short_description, p.description, p.price, p.special_price, p.pack, p.new, p.hit, p.available, p.for_proff, 
                             p._max_discount AS discount, p._max_discount_days  AS is_sales,
                             pd.eng_name, pd.use, pd.ingredients, pd.synonym, p.tm_id,
                             ps.title, ps.description seo_desc, ps.keywords,
                             pp.src, pp.alt, pp.title photo_title,
                             tm.name AS tm, IF(tm.url = "", tm.id, tm.url ) AS tm_url, td.country,
                             (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.hit = 1 LIMIT 1) AS pack_hit,
                             (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.new = 1 LIMIT 1) AS pack_new,
                             IF(p.active = 0 AND p.price = 0 AND p.new = 1 AND p.visible = 1, 1, 0) AS soon
                        FROM products AS p ' .
                        $innerJoins . '
                        LEFT JOIN rk_prod_seo ps    ON ( ps.id = p.id )
                        LEFT JOIN rk_prod_photo pp  ON ( pp.id = p.id )
                        LEFT JOIN rk_tm_desc td     ON ( p.tm_id = td.id )
                        WHERE ' . $fullWhere . '
                        ORDER BY ' . $filter_order .
                        Pagination::sqlLimit();

        $data['products'] = filters::set_products_info(DB::get_rows($query));

        if ($ajax) {
            $data['filters'] = $data['products'] ? filters::available_filters($where . $tm_condition . $price_condition . $cat_condition . $country_condition, $innerJoins) : null;
        }

        $data['filter_tms'] = $data['products'] ? filters::available_tms($where . $filter_condition . $price_condition . $cat_condition . $country_condition, $innerJoins, 'p', $ajax) : null;
        $data['filter_prices'] = $data['products'] ? filters::price_range($where . $filter_condition . $tm_condition . $cat_condition . $country_condition, $innerJoins) : null;
        $data['filter_categories'] = $data['products'] ? filters::available_categories($where . $filter_condition . $price_condition . $tm_condition . $country_condition, $innerJoins, 'p', $ajax) : null;
        $data['filter_countries'] = $data['products'] ? filters::available_countries($where . $filter_condition . $tm_condition . $price_condition . $cat_condition, $innerJoins, 'p', $ajax) : null;

        return $data;
    }

    /**
     * Get list of all products
     * @param  bool $active 1 - only active, 0 - all
     * @return array            array of products
     */
    static function get_list($active = TRUE)
    {

        $query = "SELECT id, name, IF( url =  '', id, url ) url, DATE_FORMAT( lastmod ,'%Y-%m-%d') lastmod 
                  FROM  products
                  WHERE visible = 1 " . ($active ? ' AND active = 1' : '') .
            ' AND main_id = 0
                    ORDER BY name';

        return DB::get_rows($query);
    }


    /**
     * products for third level of category without copy
     * @param  bool $active true - only visible, false - all
     * @param  bool $available true - only active, false - all
     * @param  bool $packs true - with packs, false - withoput packs
     * @param  bool $min_price use minimum price for feed
     * @return array             array of products
     */
    static function third_level_list($active = TRUE, $available = TRUE, $packs = TRUE, $equip = 0, $min_price = FALSE)
    {

        $return = FALSE;

        if ($equip) {

            $query = "SELECT p.id, p.main_id, p.name, p.short_description, IF( p.url =  '', p.id, p.url ) url, IF (p.special_price > 0, p.special_price, p.price) price, p.price real_price, p.special_price real_special_price, p.available,  p.pack,
                                   tm.name tm, IF(tm.url =  '', tm.id, tm.url ) tm_url,  td.country,
                                   pp.src, pp.pic_quant,
                                   pc1.cat_id,
                                   IF( cs.subcat_id = 71 , 1, 0 ) subcat
                            FROM   tm, rk_prod_cat_lvl_1 pc1,  rk_prod_cat_lvl_2 pc2, rk_cat_subcat cs, rk_tm_desc td, products AS p
                                                                                                                  LEFT JOIN rk_prod_photo pp ON ( pp.id = p.id )   
                            WHERE  pc1.prod_id = p.id
                            AND    pc1.cat_id != 7
                            AND    pc1.cat_id != 775
                            AND    pc2.prod_id = p.id
                            AND    cs.cat_id = pc2.cat_id
                            AND    p.tm_id = tm.id
                            AND    tm.id = td.id 
                            AND    p.main_id = 0 
                            AND    p.for_proff = 0
                            AND    p.visible = 1
                            "
                . ($min_price ?  "AND price >=  $min_price ": '')            
                . ($active ? 'AND p.active = 1 ' : '')
                . ($available ? 'AND p.available = 1 ' : '')
                . ' GROUP BY pc1.prod_id ORDER BY pc1.prod_id';

        } else {

            $query = "SELECT p.id, p.main_id, p.name, p.short_description, IF( p.url =  '', p.id, p.url ) url, IF (p.special_price > 0, p.special_price, p.price) price, p.price real_price, p.special_price real_special_price, p.available,  p.pack,
                                   tm.name tm, IF(tm.url =  '', tm.id, tm.url ) tm_url,  td.country,
                                   pp.src, pp.pic_quant,
                                   pc.cat_id
                            FROM   rk_categories c, tm, rk_tm_desc td, rk_prod_cat pc,  products AS p
                            LEFT JOIN rk_prod_photo pp ON ( pp.id = p.id )   
                            WHERE  pc.cat_id = c.id
                            AND    c.visible = 1
                            AND    pc.prod_id = p.id
                            AND    p.tm_id = tm.id
                            AND    tm.id = td.id 
                            AND    p.main_id = 0
                            AND    p.for_proff = 0
                            AND    p.visible = 1
                            "
                . ($min_price ?  "AND price >=  $min_price ": '')            
                . ($active ? 'AND p.active = 1 ' : '')
                . ($available ? 'AND p.available = 1 ' : '')
                . ' GROUP BY pc.prod_id ORDER BY pc.prod_id';

        }


        $main = DB::get_rows($query);
        $return = $main;

        if ($packs) {
            foreach ($main as $prod_key => $product) {
                $query = "SELECT p.id, p.main_id, IF (p.special_price > 0, p.special_price, p.price) price, p.price real_price, p.special_price real_special_price, p.pack, p.available,
                                         pp.src, pp.pic_quant
                                  FROM   products AS p LEFT JOIN rk_prod_photo pp ON ( pp.id = p.id )                                                                       
                                  WHERE  p.main_id = {$product['id']}
                                  AND    p.visible = 1
                                  AND    p.for_proff = 0 "
                    . ($min_price ?  "AND price >=  $min_price ": '')               
                    . ($active ? 'AND p.active = 1 ' : '')
                    . ($available ? 'AND p.available = 1 ' : '');

                $another_pack = DB::get_rows($query);

                if ($another_pack) {

                    foreach ($another_pack as $new_prod) {
                        $new_prod['name'] = $product ['name'];
                        $new_prod['short_description'] = $product ['short_description'];
                        $new_prod['url'] = $product ['url'] . "#pack-{$new_prod['id']}";
                        $new_prod['tm'] = $product ['tm'];
                        $new_prod['country'] = $product ['country'];
                        $new_prod['cat_id'] = $product ['cat_id'];
                        $return[] = $new_prod;
                    }

                    $return[$prod_key]['url'] = $main[$prod_key]['url'] . "#pack-{$main[$prod_key]['id']}";

                }

            }
        }

        return $return;
    }

    /**
     * Return hurry sale main product.
     * @return array array hurry sale main product info
     */
    static function hurry_sale_product()
    {

        $query = "SELECT p.id, p.main_id, p.price, p.special_price, p.new, p.available, p.pack,  p._discount AS sale,
                         pp.src, pp.alt, pp.title photo_title   
                  FROM products AS p
                  LEFT JOIN rk_prod_photo pp  ON ( pp.id = p.id )
                  WHERE p._foreign_stock_remains = 1
                  AND p.visible = 1
                  AND p.active = 1
                  AND p.available = 1
                  ORDER BY p._discount DESC
                  LIMIT 1";

        $hurry = DB::get_row($query);

        if (!$hurry) {
            $hurry = DB::get_row(
                'SELECT p.id, p.main_id, p.price, p.special_price, p.new, p.available, p.pack, p._discount AS sale,
                    pp.src, pp.alt, pp.title photo_title
                FROM products AS p
                INNER JOIN rk_prod_photo AS pp ON pp.id = p.id AND pp.src != "none.jpg"
                WHERE p.main_id = 0 AND p.available = 1 AND p.visible = 1 AND p.active = 1 AND p.special_price > 0 AND p.main_stock = 1
                ORDER BY RAND()
                LIMIT 1'
            );
        }

        if (!$hurry) {

            $query = "SELECT p.id, p.main_id, p.price, p.special_price, p.new, p.available, p.pack, p._discount AS sale,
                             pp.src, pp.alt, pp.title photo_title   
                      FROM  products AS p
                      LEFT JOIN rk_prod_photo pp  ON ( pp.id = p.id )
                      WHERE p.id = 182
                      AND p.visible = 1
                      AND p.active = 1
                      LIMIT 1";

            $hurry = DB::get_row($query);
        }

        $main_id = $hurry['id'];

        if ($hurry['main_id']) $main_id = $hurry['main_id'];

        $spec_info = DB::get_row("SELECT p.name, p.short_description, IF( p.url =  '', p.id, p.url ) url,  tm.name tm, IF(tm.url =  '', tm.id, tm.url ) tm_url
                                  FROM tm, products AS p
                                  WHERE p.id = $main_id
                                  AND   p.visible = 1
                                  AND   p.active = 1
                                  AND   p.tm_id = tm.id
                                  LIMIT 1");

        $hurry['name'] = $spec_info['name'];
        $hurry['short_description'] = $spec_info['short_description'];
        $hurry['url'] = $spec_info['url'];
        $hurry['tm'] = $spec_info['tm'];
        $hurry['tm_url'] = $spec_info['tm_url'];

        return $hurry;
    }

    /**
     * Get hits for listing
     * @param  integer $limit limit for query
     * @return array          products IDs
     */
    static function get_hits($limit) 
    {
        $return = array();

        DB::get_rows("SELECT p.id  AS id FROM products p WHERE p.hit= 1 ORDER BY RAND() LIMIT $limit", 
                     function($hit) use(&$return) {
                        $return[] = $hit['id'];
                     });
        
        return $return;
    }

    /**
     * Get min price product
     * @param string $tmList
     * @return mixed
     */
    public static function get_product_min_price($tmList){
        $row = DB::get_row("SELECT price FROM products WHERE tm_id IN ({$tmList}) AND visible=1 AND active=1 AND price>0 ORDER BY price LIMIT 1 ");
        return $row['price'];
    }

}
