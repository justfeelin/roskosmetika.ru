<?php

class DbConnection extends mysqli
{
    /**
     * @var bool Debug flag
     */
    public $debug = false;

    /**
     * @var array Queries (Each row: [`Query`, `Timing info`])
     */
    private $_queries = [];

    /**
     * DbConnection constructor.
     *
     * Performs connection and sets default encoding to UTF-8
     *
     * @param string $host Connection hostname
     * @param string $username Connection username
     * @param string $password Connection password
     * @param string $dbname Database name
     * @param int $port Connection port
     * @param string $socket Connection socket
     */
    public function __construct($host, $username, $password, $dbname, $port = null, $socket = null)
    {
        $this->init();

        $this->set_opt(MYSQLI_OPT_CONNECT_TIMEOUT, 1);

        parent::__construct($host, $username, $password, $dbname, $port, $socket);

        if ($this->connect_errno) {
            exit;
        }

        parent::query('SET NAMES utf8');
        parent::query('set character_set_client=\'utf8\'');
        parent::query('set character_set_results=\'utf8\'');
        parent::query('set collation_connection=\'utf8_general_ci\'');
    }

    /**
     * Returns query row
     *
     * @param string $query Query string
     *
     * @return array|null
     */
    public function getRow($query)
    {
        $res = $this->query($query);

        $isTrue = $res === true;

        $row = $res && !$isTrue ? $res->fetch_assoc() : null;

        if ($res && !$isTrue) {
            $res->close();
        }

        return $row;
    }

    /**
     * Return all values of query
     *
     * @param string $query Query string
     * @param Callable $callback Callback to be executed for each row before appending to result. If returns null or nothing - row won't be appended
     *
     * @return array
     */
    public function getRows($query, $callback = null)
    {
        $rows = [];

        $res = $this->query($query);

        if ($res && $res !== true) {
            if ($callback) {
                while ($row = $res->fetch_assoc()) {
                    $row = $callback($row);

                    if ($row !== null) {
                        $rows[] = $row;
                    }
                }
            } else {
                while ($row = $res->fetch_assoc()) {
                    $rows[] = $row;
                }
            }

            $res->close();
        }

        return $rows;
    }

    /**
     * Returns field value
     *
     * @param string $field Field name
     * @param string $query DB query
     *
     * @return string
     */
    public function getField($field, $query)
    {
        $row = $this->getRow($query);

        return $row && isset($row[$field]) ? $row[$field] : null;
    }

    /**
     * $this->real_escape_string() alias
     *
     * @param mixed $value Value to escape
     *
     * @return string
     */
    public function escape($value)
    {
        return $this->real_escape_string($value);
    }

    /**
     * @inheritdoc
     */
    public function multi_query($query)
    {
        $this->debug && ($t = microtime(true));

        $res = parent::multi_query($query);

        $this->debug && ($this->_queries[] = array("[MULTI]\n" . $query, microtime(true) - $t));

        return $res;
    }

    /**
     * @inheritdoc
     */
    public function query($query, $resultmode = 'MYSQLI_STORE_RESULT')
    {
        $this->debug && ($t = microtime(true));

        $res = parent::query($query);

        if ($res === false) {
            rk_error::set_query($query);

            error_info($this->errno, $this->error, null, null);
        }

        $this->debug && ($this->_queries[] = array($query, microtime(true) - $t));

        return $res;
    }

    /**
     * Returns formatted queries info
     *
     * @return string
     */
    public function outputQueries()
    {
        $time = 0;

        $output = '';

        for ($i = 0, $n = count($this->_queries); $i < $n; ++$i) {
            $time += $this->_queries[$i][1];

            $output .= '<tr>
                            <td>' . ($i + 1) . '</td>
                            <td class="db-log-query-cell">' . $this->_queries[$i][0] . '</td>
                            <td>' . number_format($this->_queries[$i][1], 3) . 's</td>
                        </tr>';
        }

        return '<div id="db-log" style="position: fixed; left: 10px; right: 20px; bottom: 45px; height: 13px; background-color: white; font-size: 9px; overflow: hidden; z-index: 10">
        <table style="width: 100%">
            <thead>
                <tr>
                    <th width="3%"># (' . $n . ')</th>
                    <th width="91%">Query</th>
                    <th width="6%">Time (' . number_format($time, 3) . ') <a href="#" id="toggle-sql-log">[ /\ ]</a></th>
                </tr>
            </thead>
            <tbody>' . $output . '</tbody>
        </table>
        </div>
            <script type="text/javascript">
                window.common || (window.common = {});

                $("#toggle-sql-log").click(function () {
                    common.sqlLogOpened = !common.sqlLogOpened;

                    $("#db-log").height(common.sqlLogOpened ? 300 : 13).css("overflow-y", common.sqlLogOpened ? "scroll" : "hidden");

                    this.innerHTML = "[ " + (common.sqlLogOpened ? "\\\/" : "/\\\") + " ]";

                    return false;
                });
                
                document.write("<style>.db-log-query-cell {max-width: " + ($("body").width() - 200) + "px}</style>");
            </script>';
    }

    /**
     * Calls $this->close()
     */
    public function __destruct()
    {
        $this->close();
    }
}
