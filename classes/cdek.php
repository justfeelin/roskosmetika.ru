<?php
/**
 * Created by PhpStorm.
 * User: janpoul
 * Date: 24.04.18
 * Time: 11:05
 */

class cdek
{

    /** Request to API cdek
     * @param string $paramsName
     * @param int $paramsValue
     * @return mixed
     */
    public static function send_to_cdek($paramsName = '', $paramsValue = 0)
    {

        $address = 'https://integration.cdek.ru/pvzlist.php';
        $chp = curl_init($address);
        curl_setopt($chp, CURLOPT_HEADER, 0);
        curl_setopt($chp, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($chp, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($chp, CURLOPT_POST, false);
        if ($paramsName !== '') {
            curl_setopt($chp, CURLOPT_POSTFIELDS, '?' . $paramsName . '=' . $paramsValue);
        }
        return curl_exec($chp);
    }

    /** Get points for region
     * @param integer $region
     * @return array|bool
     */
    public static function get_points($region)
    {

        if (isset($region)) {
            return DB::get_rows("SELECT * FROM cdek c WHERE c.region_code = {$region}");
        }
        return false;
    }

    /**Get point for region
     * @param $id
     * @return array|bool
     */
    public static function get_point($id)
    {

        if (isset($id)) {
            //  vd('nen');
            return DB::get_row("SELECT * FROM cdek c WHERE c.id = {$id}");
        }
        return false;
    }

    /**Get prices for region
     * @param integer $region
     * @param integer $type (1- cdek, 2 - courier)
     * @return bool
     */
    public static function get_price($region, $type)
    {

        if (isset($region) && isset($type)) {

            $prices = DB::get_row("SELECT s.id,s.price_1,s.price_4 FROM shipping_regions s WHERE s.cdek = {$region}");
            //vd($prices);exit();
            $price['_0'] = ($type < 2) ? (int)$prices['price_4'] : (int)$prices['price_1'];
            $price['_1'] = (int)$prices['id'];
            if (($price['_1'] === 77) || ($price['_1'] === 78)) {

                $compens = DB::get_rows("SELECT s.minimal_sum,s.compensation_1,s.compensation_4 FROM shipping_regions_compensations s WHERE s.region_id = {$prices['id']}");
                foreach ($compens as $comp) {
                    $price['_'.(string)$comp['minimal_sum']] = ($type < 2) ? $prices['price_4'] - $comp['compensation_4'] : $prices['price_1'] - $comp['compensation_1'];
                }

            } else {

                $price['_4000'] = ($type < 2) ? $prices['price_4'] - orders::SHIPPING_COMPENSATION : $prices['price_1'] - orders::SHIPPING_COMPENSATION;
            }
            return $price;
        }
        return false;
    }

}