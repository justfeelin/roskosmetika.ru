<?php

/**
 * Класс для работы с рассылкой
 */
class unsubscribe
{

    /**
     * функция получения списка рассылки бызы клиентов
     * @param $source string  ресурс к которому относятся клиенты
     * @return array  список клиентов заданного ресурса
     */
    static function get_self_users($source, $status, $active)
    {

        $query = 'SELECT *
                  FROM rk_unsubscribe 
                  WHERE  block = 0 AND ' . ($active ? ' active >= 0 ' : ' active = 1 ') .
            'AND `status` = "' . DB::escape($status) . '"' .
            ($source == 'all' ? '' : " AND source = '" . DB::escape($source) . "'");

        return DB::get_rows($query);
    }


    /**
     * функция получения списка рассылки по ее номеру
     * @return array  список рассылки активных адресов по ее номеру
     * @param $number int  номер расылки
     */
    static function get_list($number)
    {

        $query = "SELECT *
                  FROM rk_unsubscribe 
                  WHERE block = 0
                  AND   active = 1
                  AND status = 'spam'
                  AND number  = " . (int)$number;

        return DB::get_rows($query);
    }


    /**
     * Функция изменения активности рассылки
     */

    static function  change_active($email, $hash)
    {
        $query = "UPDATE rk_unsubscribe SET active = 0
                  WHERE  email = LOWER('" . DB::escape($email) . "')
                  AND hash = '" . DB::escape($hash) . "'";

        return DB::query($query);
    }


    /**
     * Функция изменения типа клиента
     */

    static function  change_client_type($email, $hash, $client_type)
    {
        $query = "UPDATE rk_unsubscribe SET client_type = '" . DB::escape($client_type) . "'
                  WHERE  email = LOWER('" . DB::escape($email) . "')
                  AND hash = '" . DB::escape($hash) . "'";
        return DB::query($query);
    }


    /**
     * Функция изменения активности рассылки
     */

    static function  add_users_data($email, $hash, $first_name = '', $last_name = '', $patronymic = '', $client_type = 3, $manager = '', $region = 0)
    {
        $query = "UPDATE rk_unsubscribe SET
                      first_name = '" . DB::escape($first_name) . "',
                      last_name = '" . DB::escape($last_name) . "',
                      patronymic = '" . DB::escape($patronymic) . "',
                      client_type = '" . DB::escape($client_type) . "',
                      manager = '" . DB::escape($manager) . "',
                      region = '" . DB::escape($region) . "'
                  WHERE  email = LOWER('" . DB::escape($email) . "')
                  AND hash = '" . DB::escape($hash) . "'";

        return DB::query($query);
    }


    /**
     * Функция изменения активности рассылки
     */

    static function  set_active($email, $hash)
    {
        $query = "UPDATE rk_unsubscribe SET active = 1
                  WHERE  email = LOWER('" . DB::escape($email) . "')
                  AND hash = '" . DB::escape($hash) . "'";
        return DB::query($query);

    }


    /**
     * Функция изменения статуса адреса spam - внешний,  self  -  своя БД адресов.
     */

    static function  change_status($email, $status)
    {
        $query = "UPDATE rk_unsubscribe SET status = '" . DB::escape($status) . "'
                  WHERE  email = LOWER('" . DB::escape($email) . "')";
        return DB::query($query);

    }

    /**
     * Функция добавления нового email
     */

    static function add_new_email($email, $status, $max_number, $source, $first_name = '', $last_name = '', $patronymic = '', $client_type = 3, $manager = '', $region = 0)
    {


        $query = "INSERT rk_unsubscribe (email, hash, first_name, last_name, patronymic, client_type, manager, active, status, number, date, source, region)
                  VALUE (LOWER('" . DB::escape($email) . "'), md5(LOWER('" . DB::escape($email) . "')), '" . DB::escape($first_name) . "', '" . DB::escape($last_name) . "', '" . DB::escape($patronymic) . "', '" . DB::escape($client_type) . "', '" . DB::escape($manager) . "',  1 , '" . DB::escape($status) . "', " . ((int)$max_number + 1) .", CURDATE(), '" . DB::escape($source) . "', '" . DB::escape($region) . "')";

        return DB::query($query);
    }

    /**
     * Функция получения максимального номера рассылки
     * @return  $max_number  array  максимальный номер рассылки
     */

    static function add_max_number()
    {
        $query = 'SELECT MAX(number) max
                  FROM rk_unsubscribe';
        return DB::get_field('max', $query);
    }


    /**
     * Функция проверки существования email
     * @return array $exist_email
     */

    static function check_existence($email)
    {

        $query = "SELECT *
                  FROM rk_unsubscribe 
                  WHERE email = LOWER('" . DB::escape($email) . "')";

        return DB::get_row($query);
    }


    /**
     * Функция возвращающая значения различных номеров рассылки и дату
     * @return array $number_and_date
     */

    static function number_and_date()
    {
        $query = 'SELECT  number, date, source, comment, COUNT(email) AS sum
                  FROM rk_unsubscribe LEFT JOIN rk_unsub_comment ON number = mails_id
                  WHERE number != 0
                  GROUP BY number';

        return DB::get_rows($query);
    }


    /**
     * Изменяет номер рассылки для существующих email в списках рассылки.
     */

    static function  change_number($email, $number)
    {
        $query = "UPDATE rk_unsubscribe SET number = '" . ((int)$number) . "'
                  WHERE  email = LOWER('" . DB::escape($email) . "')";
        DB::query($query);

    }

    /**
     * Изменяет метку ресурса.
     * @param string $email - email для рассылки.
     * @param string $source - email ресурс.
     */

    static function  change_source($email, $source)
    {
        $query = "UPDATE rk_unsubscribe SET source = '" . DB::escape($source) . "'
                  WHERE  email = LOWER('" . DB::escape($email) . "')";
        return DB::query($query);

    }


    /**
     * Изменяет дату рассылки.
     * @param string $email - email для рассылки.
     */

    static function  change_date($email)
    {
        $query = "UPDATE rk_unsubscribe SET date = CURDATE()
                  WHERE  email = LOWER('" . DB::escape($email) . "')";
        return DB::query($query);

    }


    /**
     * Функция блокировки несуществующих email адресов
     * @param string $email - несуществующий адрес.
     */

    static function  change_block($email)
    {
        $query = "UPDATE rk_unsubscribe SET block = 1
                  WHERE  email = LOWER('" . DB::escape($email) . "')";
        return DB::query($query);

    }


    /**
     * Функция добавления адреса в базу рассылки, если он еще не внесен
     * @param string $email -  адрес.
     * @param string $status -  статус адреса.
     * @param string $host -  хост адреса.
     */
    static function add_self($email, $status, $host, $first_name = '', $last_name = '', $patronymic = '', $region = 0, $client_type = 3, $manager = '')
    {
        $exist_mail = self::check_existence($email);

        if ($exist_mail) {

            if ($exist_mail['status'] == 'spam') {
                @self::change_status($email, $status);
                @self::change_source($email, $host);
                @self::change_number($email, 0);
            }

            if (!isset($exist_mail['name']) || !isset($exist_mail['manager']) || !isset($exist_mail['region']))
                @self::add_users_data($email, $exist_mail['hash'], $first_name, $last_name, $patronymic, $client_type, $manager, $region);


            if ($exist_mail['client_type'] == 3 && $client_type == 2)
                @self::change_client_type($email, $exist_mail['hash'], 2);
        } else {
            self::add_new_email($email, $status, -1, $host, $first_name, $last_name, $patronymic, $client_type, $manager, $region);
        }
    }


    /**
     * Получение текста существующего комментария.
     * @param string $number - номер рассылки.
     */

    static function  get_comment($number)
    {
        $query = "SELECT *
                  FROM rk_unsub_comment
                  WHERE mails_id = " . (int)$number;
        return DB::get_row($query);

    }


    /**
     * Редактирование комментария рассылки, добавление нового, при отсутствии.
     * @param int $number
     * @param string $comment
     */
    static function edit_comment($number, $comment)
    {
        $number = (int)$number;

        $arr_exist = self::get_comment($number);

        if (empty($arr_exist)) {
            $query = "INSERT INTO rk_unsub_comment (mails_id , comment)
                   VALUES (" . ((int)$number) . ", '" . DB::escape($comment) . "')";
        } else {
            $query = "UPDATE rk_unsub_comment
                  SET comment = '" . DB::escape($comment) . "'
                  WHERE mails_id = " . ((int)$number);
        }

        return DB::query($query);
    }

    /**
     * Returns email hash from subscription table
     *
     * @param string $email E-mail
     *
     * @return string
     */
    public static function get_email_hash($email)
    {
        return DB::get_field('hash', 'SELECT u.hash FROM rk_unsubscribe AS u WHERE u.email = LOWER("' . DB::mysql_secure_string($email) . '") LIMIT 1');
    }
}
