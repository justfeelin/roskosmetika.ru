<?php

abstract class mytarget
{
    /**
     * js mytarget code for main page
     * 
     * @param array  $products       array of product data
     * @param int    $total_value   total price for products group 
     * @param string $event         name of event
     * @param int    $price_list_id Number of xml feed
     * @param string $type          type of action
     * 
     * @return string  js code in string
     */
    private static function set_event($products, $total_value, $event, $price_list_id = 1, $type = 'itemView')
    {

        $data = array('type'     => $type,
                      'pagetype' => $event,
                      'list'     => $price_list_id);

        $data['productid'] = $products;
        $data['totalvalue'] = $total_value;
              

        return ' var _tmr = _tmr || [];
                _tmr.push(' . json_encode($data) . ')';

    }


    /**
     * View product page code
     * @param  array $product Data array
     * @return string js code
     */
    static function view_product($product)
    {
        return self::set_event($product['id'], $product['special_price'] > 0 ? $product['special_price'] : $product['price'], 'product');
    }


    /**
     * View cart page code
     * @param  array $products Data array
     * @return string js code
     */
    static function init_checkout($products)
    {

        return self::set_event(array_column($products['products'], 'id'), $products['info']['final_sum'], 'cart');

    }

    /**
     * View ok page code 
     * @param  array $products Data array
     * @param  int   $order_price orders price
     * @return string js code
     */
    static function purchase($products, $order_price)
    {
    
        return self::set_event(array_column($products, 'id'), $order_price, 'purchase');

    }
}