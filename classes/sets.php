<?php

/**
 * Working with sets
 */
class sets
{

    /**
     * return all sets
     * @param  boolean $desc include description
     * @param  boolean $seo include seo
     * @param  boolean $photo include photo description
     * @return array          array of sets
     */
    static function get_sets($desc = TRUE, $seo = TRUE, $photo = TRUE)
    {

        $query = 'SELECT rk_sets.id, rk_sets.name, rk_sets.url, rk_sets.short_desc, rk_sets.desc ' .
            ($desc ? ' ,sd.synonym' : '') .
            ($seo ? ' ,ss.title, ss.description seo_desc, ss.keywords' : '') .
            ($photo ? ' ,sp.src, sp.alt, sp.title photo_title ' : '') .
            'FROM `rk_sets`  ' .
            ($desc ? 'LEFT JOIN rk_sets_desc sd  ON ( sd.id = rk_sets.id ) ' : '') .
            ($seo ? 'LEFT JOIN rk_sets_seo ss   ON ( ss.id = rk_sets.id ) ' : '') .
            ($photo ? 'LEFT JOIN rk_sets_photo sp ON ( sp.id = rk_sets.id AND sp.src != "none.jpg") ' : '') .
            'ORDER BY rk_sets.name';

        return DB::get_rows($query);
    }


    /**
     * return sets for export
     * @param  array $param export parameters
     * @return array         array of sets
     */
    static function get_export_sets($param)
    {

        //include check for NULL and ''
        if (!empty($param)) {
            $sort = '';

            foreach ($param as $field) {
                $sort .= " ($field = '')  ($field IS NULL) ";
                if ($field == 'sp.src') $sort .= " ($field = 'none.jpg') ";
            }

            $param = str_replace('  ', ' OR ', trim($sort));
            $param = " WHERE ($param) ";
        }


        $query = 'SELECT rk_sets.id, rk_sets.name, rk_sets.url, rk_sets.short_desc, rk_sets.desc,
                         sd.synonym, 
                         ss.title, ss.description seo_desc, ss.keywords,
                         sp.src, sp.alt, sp.title photo_title
                  FROM `rk_sets` 
                  LEFT JOIN `rk_sets_desc`  sd ON ( sd.id = rk_sets.id ) 
                  LEFT JOIN `rk_sets_seo`   ss ON ( ss.id = rk_sets.id ) 
                  LEFT JOIN `rk_sets_photo` sp ON ( sp.id = rk_sets.id AND sp.src != "none.jpg")' . $param .
            'ORDER BY rk_sets.id';

        return DB::get_rows($query);

    }

    /**
     * Get by ID  URL for redirect sets
     * @param  int $set_id id of set
     * @return string      URL of set
     */
    static function redirect($set_id)
    {

        return DB::get_field('url', "SELECT url FROM rk_sets  WHERE id = $set_id");

    }

    /**
     * Get by URL id to get sets and products
     * @param  string $url url of sets
     * @return int          ID of set
     */
    static function get_id_by_url($url)
    {

        return (int)DB::get_field('id', "SELECT id FROM rk_sets WHERE url = '$url'");

    }

    /**
     * Output products, filters  for set
     * @param  int $set_id set ID
     * @param  bool $flag flag for start work - TRUE, if FALSE - stop work and return FALSE
     * @param bool $ajax Ajax request, return only products
     *
     * @return array         products of set with filters
     */
    static function set_output($id, $flag, $ajax = false)
    {

        //  return array begin value
        $data = FALSE;

        if ($flag) {

            if (!$ajax) {
                // set
                $query = "SELECT rk_sets.id, rk_sets.name, rk_sets.url, rk_sets.short_desc, rk_sets.desc,
                               sd.synonym,
                               ss.title, ss.description seo_desc, ss.keywords,
                               sp.src, sp.alt, sp.title photo_title
                      FROM rk_sets  
                      LEFT JOIN rk_sets_desc sd  ON ( sd.id = rk_sets.id ) 
                      LEFT JOIN rk_sets_seo ss   ON ( ss.id = rk_sets.id ) 
                      LEFT JOIN rk_sets_photo sp ON ( sp.id = rk_sets.id AND sp.src != 'none.jpg')
                      WHERE rk_sets.id = $id 
                      LIMIT 1";

                $data['set'] = DB::get_row($query);

                //  filters
                $data['filters'] = [];
                $query = "SELECT id, name FROM rk_filters WHERE parent_id = 0 AND visible = 1 ORDER BY name";

                $main_filters = DB::get_rows($query);

                foreach ($main_filters as $key => $main_filter) {
                    $query = "SELECT  f.id, f.name
                          FROM   rk_prod_sets AS ps, rk_prod_filter AS pf, rk_filters AS f
                          WHERE  ps.set_id = $id
                          AND    ps.prod_id = pf.prod_id
                          AND    pf.filter_id = f.id
                          AND    f.visible = 1
                          AND    f.parent_id = {$main_filter['id']}
                          GROUP BY f.id 
                          ORDER BY f.name";
                    $filters = DB::get_rows($query);

                    if (count($filters) > 1) {
                        $data['filters'][$main_filter['name']] = helpers::explode_filters($filters);
                    }
                }
            }

            $filter_condition = $ajax ? Product::filter_condition('p') : '';
            $price_condition = $ajax ? Product::price_condition('p') : '';
            $tm_condition = $ajax ? Product::tm_condition('p') : '';
            $country_condition = $ajax ? Product::country_condition('p') : '';

            $innerJoins = 'INNER JOIN tm ON tm.id = p.tm_id
                INNER JOIN rk_prod_sets AS ps ON ps.set_id = ' . $id . ' AND ps.prod_id = p.id';

            $where = '(p.visible = 1 OR IF((SELECT COUNT(pp.id) FROM products AS pp WHERE pp.visible = 1 AND pp.main_id = p.id), 1, 0))
                AND p.main_id = 0 AND (p.active = 1 OR p.price = 0 AND p.new = 1) ';

            $fullWhere = $where . $filter_condition . $tm_condition . $price_condition . $country_condition;

            $query = 'SELECT p.id, p.name, p.short_description, IF( p.url = "", p.id, p.url ) url, p.price, p.new, p.hit, p.available, p.special_price, p.pack, tm.name tm, IF(tm.url = "", tm.id, tm.url ) tm_url,  p.for_proff,
                               p._max_discount AS discount, p._max_discount_days  AS is_sales,
                               pp.src, pp.alt, pp.title photo_title,
                               (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.hit = 1 LIMIT 1) AS pack_hit,
                               (SELECT pv.id FROM products AS pv WHERE pv.main_id = p.id AND pv.visible = 1 AND pv.active = 1 AND pv.new = 1 LIMIT 1) AS pack_new,
                               IF(p.active = 0 AND p.price = 0 AND p.new = 1 AND p.visible = 1, 1, 0) AS soon
               FROM products AS p
                ' . $innerJoins . '
                LEFT JOIN rk_prod_photo pp ON ( pp.id = p.id )
                WHERE ' . $fullWhere . '
              ORDER BY ' . Product::filter_order('p');

            $products = DB::get_rows($query);
            $data['products'] = $products ? filters::set_products_info($products) : array();

            if ($ajax) {
                $data['filters'] = $data['products'] ? filters::available_filters($where . $tm_condition . $price_condition . $country_condition, $innerJoins) : null;
            }

            $data['filter_tms'] = $data['products'] ? filters::available_tms($where . $filter_condition . $price_condition . $country_condition, $innerJoins, 'p', $ajax) : null;
            $data['filter_prices'] = $data['products'] ? filters::price_range($where . $filter_condition . $tm_condition . $country_condition, $innerJoins) : null;
            $data['filter_countries'] = $data['products'] ? filters::available_countries($where . $filter_condition . $tm_condition . $price_condition, $innerJoins, 'p', $ajax) : null;
            
            $data['found'] = Product::count_string(count($data['products']));
        }

        return $data ?: false;
    }

    /**
     * Get list of sets
     * @param  boolean $visible True - only visible, False - all
     * @return array            array of sets
     */
    static function get_list($visible = TRUE)
    {

        $query = "SELECT id, name, IF( url =  '', id, url ) url, DATE_FORMAT( lastmod ,'%Y-%m-%d') lastmod 
                  FROM  rk_sets
                  WHERE " . ($visible ? "visible = 1 " : '1') .
            ' ORDER BY name';

        return DB::get_rows($query);
    }

}

?>
