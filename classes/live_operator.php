<?php

/**
 * Live operator.
 */
class live_operator
{

    /**
     * Возвращает список вопросов.
     *
     * @param int $onPage
     * @return array
     */
    static function get_messages($onPage = null)
    {
        if ($onPage !== null) {
            $count = (int)DB::get_field('cnt', 'SELECT COUNT(l.name) AS cnt FROM rk_live_operator AS l');

            Pagination::setValues($count, $onPage);

            Pagination::setTemplateSettings();
        }

        $query = "SELECT *
                                   FROM rk_live_operator " .
        ($onPage !== null ? Pagination::sqlLimit() : '');

        return DB::get_rows($query);
    }

    /**
     * Добавляет вопрос.
     * @return boolean
     */
    static function add_message($name, $contacts, $msg, $host)
    {
        $probablyEmail = check::email($contacts);

        Orders::insert_order(
            $name,
            '',
            '',
            $probablyEmail ? $contacts : '',
            !$probablyEmail ? $contacts : '',
            Auth::is_auth(),
            [
                Orders::CONSULTATION_PRODUCT_ID,
            ],
            '!!! Заказ через онлайн-консультант ' . $host . ' !!!',
            0,
            '',
            null,
            null,
            null,
            null,
            true
        );

        $query = "INSERT INTO rk_live_operator (name, contacts, question, resource)
                                    VALUES ('" . DB::escape($name) . "',
                                                  '" . DB::escape($contacts) . "',
                                                  '" . DB::escape($msg) . "',
                                                  '" . DB::escape($host) . "')";


        if (Check::email($contacts)) {

            unsubscribe::add_self($contacts, 'lo', $host, $name);
        }

        return DB::query($query);
    }

}

?>