<?php

class Yamarket
{

    /**
     * @var array Products for yandex market
     */
    private static $_m_products = array();

    /**
     * Get token for working with YA Market API
     * @return string token
     */
    public static function get_token()
    {
        $tokenFile = __DIR__ . '/../yatoken/.yandex_token';
        $token = null;

        if (file_exists($tokenFile)) {
            $tokenInfo = explode("\n", file_get_contents($tokenFile));

            if (isset($tokenInfo[1]) && $tokenInfo[1] > $_SERVER['REQUEST_TIME'] + 300) {
                $token = $tokenInfo[0];
            }
        }
        
        return $token;
    }

    /**
     * Curl request for yandex market
     * @param  string  $url     cURL url
     * @param  array   $headers http headers
     * @param  array   $data    data for POST
     * @param  string  $method  http method
     * @param  boolean $json    POST data in json
     * @return json             result in JSON
     */
    public static function ya_request($url, $headers, $data, $method = 'GET', $json = FALSE)
    {

        $curl = curl_init();
        $post = FALSE;

        if ($data) 
        {
            $post = TRUE;

            $data = ($json ? json_encode($data) : http_build_query($data));
            
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);   
        }

        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_POST => $post,
            CURLOPT_HTTPHEADER =>  $headers,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_SSL_VERIFYPEER => FALSE,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_HEADER => FALSE,
            CURLOPT_TIMEOUT => 60,
            CURLOPT_FAILONERROR => FALSE,
            CURLOPT_FRESH_CONNECT => true,
        ]);

        $result = curl_exec($curl);

        $result = json_decode($result, true);   

        if ($curl)
        {
            curl_close($curl);
        }

        return $result;
    }

    /**
     * Get list of yandex market products
     * @return bool 
     */
    public static function set_yandexm_products()
    {
        self::$_m_products = json_decode(DB::get_field('products', 'SELECT products FROM __yandex_market_products WHERE 1 '));
    }

    /**
     * Сheck if the market ID is in the array
     * @param  int $id product id
     * @return bool    result
     */
    public static function check_id($id)
    {
        return in_array($id, self::$_m_products);
    }

}