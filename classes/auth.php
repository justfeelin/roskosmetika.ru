<?php

/**
 * login
 */
class Auth
{
    /**
     * user Id .
     * @var integer
     */
    static $user_id = 0;

    /**
     * check login.
     * @return integer|null null if no, client id if user login.
     */
    static function is_auth()
    {
        return self::$user_id = !empty($_SESSION['user']['id']) ? $_SESSION['user']['id'] : 0;
    }

    /**
     * return login user id.
     * @return integer
     */
    static function get_user_id()
    {
        return self::$user_id;
    }

    /**
     * client authentication.
     *
     * @param string $user client email or phone (Depending on $byEmail)
     * @param string $password client password.
     * @param bool $byEmail Login by email (By phone if false)
     *
     * @return boolean
     */
    static function login($user, $password, $byEmail = true)
    {
        $user = DB::mysql_secure_string($user);
        $password = DB::mysql_secure_string($password);

        $query = 'SELECT id, secure_cid FROM clients WHERE '; 

        if ($byEmail) {

            $query .= "email  = LOWER('$user')";

        } else {

            $user = filter_var($user, FILTER_SANITIZE_NUMBER_INT);
            $user = (mb_substr($user, 0, 1) == 8 ? substr_replace($user, '+7', 0, 1) : $user);

            $query .= "REPLACE(REPLACE(REPLACE(phone, ' ',''), '(', ''), ')', '') = '$user'";
        }
       
        $query .=  " AND password = '$password'";


        $data = DB::get_row($query);

        if ($data) {
            $oldOrderID = Cart::orderID();

            Users::update_session($data['id']);

            if ($oldOrderID) {
                if ($_SESSION['user']['order_id']) {
                    Cart::merge_items($oldOrderID, $_SESSION['user']['order_id']);

                    Cart::delete_order($oldOrderID);

                    Cart::orderID($_SESSION['user']['order_id']);
                } else {
                    Cart::change_client_id($oldOrderID, $_SESSION['user']['id']);

                    Cart::orderID($oldOrderID);
                }
            }

            return (int)$data['id'];
        } else {
            return false;
        }
    }

    /**
     * Returns current user organization type
     *
     * @return string
     */
    public static function organization_type()
    {
        static $return;

        if ($return === null) {
            $return = self::is_auth()
                ? (int)DB::get_field('type_id', 'SELECT c.type_id
                    FROM clients AS c
                    WHERE c.id = ' . self::$user_id)
                : null;
        }

        return $return;
    }

    /**
     * Performs $_SESSION['user'] initialization if empty
     */
    public static function init_session()
    {
        if (!isset($_SESSION['user'])) {
            if (
                isset($_COOKIE[Users::HASH_KEY])
                && Users::validate_hash($_COOKIE[Users::HASH_KEY])
                && (
                    ($client = Users::get_by_hash($_COOKIE[Users::HASH_KEY]))
                    || ($orderID = Cart::get_order_by_hash($_COOKIE[Users::HASH_KEY]))
                )
            ) {
                Users::update_session(!empty($client) ? $client['id'] : 0, empty($client) && !empty($orderID) ? $_COOKIE[Users::HASH_KEY] : null);

                if (!empty($orderID)) {
                    $_SESSION['user']['order_id'] = $orderID;
                }
            } else {
                Users::update_session(0);
            }
        }
    }
}
