<?php

abstract class DeviceCheck
{
    /**
     * @var string Cookie name for storing state that device check was done
     */
    private static $_cookie = 'dvc_chk';

    /**
     * @var string Pattern for checking URL to not redirect from desktop version
     */
    private static $_desktopOnly = '#^/landing/[a-z-]#i';

    /**
     * @var string Pattern for checking URL to not redirect from mobile version
     */
    private static $_mobileOnly = '';

    /**
     * Returns whether desktop device is used
     *
     * @return bool
     */
    public static function isDesktop()
    {
        static $return;

        if ($return === null) {
            /**
             * https://gist.github.com/dalethedeveloper/1503252/931cc8b613aaa930ef92a4027916e6687d07feac
             */

            $return = !empty($_SERVER['HTTP_USER_AGENT'])
                ? !(preg_match('/Mobile|iP(hone|od|ad)|Android|BlackBerry|IEMobile|Kindle|NetFront|Silk-Accelerated|(hpw|web)OS|Fennec|Minimo|Opera M(obi|ini)|Blazer|Dolfin|Dolphin|Skyfire|Zune/', $_SERVER['HTTP_USER_AGENT']))
                : true;
        }

        return $return;
    }

    /**
     * Checks user device and redirects if needed
     *
     * @param bool $forDesktop Whether current site is for desktop browsers
     * @param string $url Url to redirect to switch version
     * @param string $noRedirectDomain Allowed HTTP referer to not make redirect
     */
    public static function check($forDesktop, $url, $noRedirectDomain = null)
    {
        if (!isset($_COOKIE[self::$_cookie])) {
            //1 year expire
            setcookie(self::$_cookie, 1, time() + 31536000, '/');

            $isDesktop = self::isDesktop();

            $pattern = $forDesktop ? self::$_desktopOnly : self::$_mobileOnly;

            if ((!$pattern || !preg_match($pattern, $_SERVER['REQUEST_URI'])) && $isDesktop !== $forDesktop && (!$noRedirectDomain || empty($_SERVER['HTTP_REFERER']) || strpos($_SERVER['HTTP_REFERER'], $noRedirectDomain) !== 0)) {
                header('Location: ' . $url);

                exit;
            }
        }
    }

    /**
     * Generates link to be used to switch desktop/mobile domains
     *
     * @param string $domain Domain to be switched to
     *
     * @return string
     */
    public static function switch_link($domain)
    {
        return $domain . str_replace('"', '%22', $_SERVER['REQUEST_URI']);
    }
}
