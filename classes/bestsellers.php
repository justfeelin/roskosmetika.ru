<?php

/*
 * Create list of bestsellers
 */

class bestsellers
{
    /**
     * Get sort list of bestsellers in listing
     * @param  array $sort_type parameters fo listing
     * @return array              pIDs array
     */
    static function get_list($sort_type)
    {

       $return_IDs = array();


       if ($sort_type['type'] == 'categories') {

            $lvl = (int)DB::get_field('level', "SELECT level FROM rk_categories WHERE id = {$sort_type['id']}");

            $table = 'rk_prod_cat' . ($lvl < 3 ? '_lvl_' . $lvl :  '');

            $IDs = DB::get_rows('SELECT rpc.prod_id AS id
                                        FROM products p, ' . $table . " rpc LEFT JOIN cdb_product_ballance cpb ON (rpc.prod_id = cpb.product_id)
                                        WHERE rpc.prod_id = p.id
                                        AND   rpc.cat_id = {$sort_type['id']}
                                        AND   p.active = 1 
                                        ORDER BY cpb.sold DESC, p.price DESC");

       }

       if ($sort_type['type'] == 'new' || $sort_type['type'] == 'sales') {

            $IDs = DB::get_rows('SELECT p.id
                                 FROM products p LEFT JOIN cdb_product_ballance cpb ON (p.id = cpb.product_id)
                                 WHERE ' . ($sort_type['type'] == 'new' ? 'p.new = 1' : 'p.special_price > 0') . ' AND p.main_id = 0 AND p.visible = 1 AND (p.active = 1 OR p.price = 0 AND p.new = 1)
                                 ORDER BY cpb.sold DESC, p.price DESC');
       }

       if ($sort_type['type'] == 'search') {

            $preID = array();

            $results = Search::remote_search($sort_type['id'], null, FALSE, 1, FALSE, FALSE);

             for ($i=0; $i < count($results['products']); $i++) { 
                 $preID[] = $results['products'][$i];
             }

             $IDs = DB::get_rows('SELECT p.id
                     FROM products p LEFT JOIN cdb_product_ballance cpb ON (p.id = cpb.product_id)
                     WHERE p.id IN(' . implode(",", $preID) . ')
                     AND  p.main_id = 0 AND p.visible = 1 AND (p.active = 1 OR p.price = 0 AND p.new = 1)
                     ORDER BY cpb.sold DESC, p.price DESC');

       }

       if ($sort_type['type'] == 'tm') {

            $IDs = DB::get_rows("SELECT p.id
                                 FROM products p
                                 INNER JOIN tm ON tm.name = '" . DB::mysql_secure_string($sort_type['id']) . "' AND tm.id = p.tm_id
                                 LEFT JOIN cdb_product_ballance cpb ON (p.id = cpb.product_id)
                                 WHERE (p.visible = 1 OR IF((SELECT COUNT(pp.id) FROM products AS pp WHERE pp.visible = 1 AND pp.main_id = p.id), 1, 0))
                                 AND p.main_id = 0 AND (p.active = 1 OR p.price = 0 AND p.new = 1) 
                                 ORDER BY cpb.sold DESC, p.price DESC");
        
       }


       for ($i=0; $i < count($IDs); $i++) { 
           $return_IDs[] = $IDs[$i]['id'];
       }
      

       return $return_IDs;

    }

}
