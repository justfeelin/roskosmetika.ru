<?php

/**
 * Работа c партнерскими программами.
 */
class Partner
{

    /**
     * Возвращает список программ.
     * @param boolen $active Id программы.
     * @return array
     */
    static function get_list($active = FALSE)
    {

        $query = 'SELECT *
                  FROM rk_partner 
                  WHERE ' . ($active ? ' active = 1' : '1');

        return DB::get_rows($query);
    }

    /**
     * Возвращает параметры  файла программы.
     * @param integer $id Id программы.
     * @return array
     */
    static function get_partner($id)
    {

        $id = (int)$id;
        $query = "SELECT * FROM rk_partner WHERE id = $id";

        return DB::get_row($query);
    }


    /**
     * Проверяет file_name на существование.
     * @param char $file_name - имя файла партнерской программы
     * @return array
     */
    static function check_file_name($file_name)
    {

        $query = "SELECT *
                  FROM rk_partner
                  WHERE file_name = '$file_name'";
        return array('kol' => DB::get_num_rows($query),
            'id' => DB::get_field('id', $query));
    }


    /**
     * Добавляет партнерскую программу.
     * @param array $data - массив данных для добавления в партнерскую программу.
     * @return bool
     */
    static function add_partner($data)
    {

        $query = "INSERT INTO rk_partner (name,
                                       file_name,
                                       equip, 
                                       utm_source, 
                                       utm_medium, 
                                       utm_campaign, 
                                       catalog, 
                                       shop_name, 
                                       company, 
                                       shop_url, 
                                       currency, 
                                       delivery,
                                       local_delivery_cost,
                                       rk_categories, 
                                       url,
                                       price, 
                                       picture,
                                       store,
                                       pickup,
                                       vendor,
                                       description,
                                       sales_notes,
                                       country_of_origin, 
                                       active )
                  VALUES ('{$data['name']}', 
                          '{$data['file_name']}', 
                          '{$data['equip']}',
                          '{$data['utm_source']}', 
                          '{$data['utm_medium']}', 
                          '{$data['utm_campaign']}', 
                          '{$data['catalog']}', 
                          '{$data['shop_name']}', 
                          '{$data['company']}', 
                          '{$data['shop_url']}', 
                          '{$data['currency']}',
                          {$data['delivery']}, 
                          '{$data['local_delivery_cost']}',
                          {$data['categories']}, 
                          {$data['url']}, 
                          {$data['price']}, 
                          {$data['picture']}, 
                          {$data['store']},
                          {$data['pickup']},
                          {$data['vendor']},
                          {$data['description']},
                          '{$data['sales_notes']}',
                          {$data['country_of_origin']},
                          1)";

        return DB::query($query);
    }


    /**
     * Функция создания XML файла партнерской программы.
     * @param array $args Параметры файла.
     */
    static function create_file($args)
    {

        if (!empty($args)) {
            //  open file
            if (!$f = fopen(SITE_PATH . 'www' . DIRECTORY_SEPARATOR . 'partners' . DIRECTORY_SEPARATOR . "{$args['file_name']}" . '.xml', "wt"))
                return FALSE;

            $patterns = array('/&nbsp;/', '/&lt;p&gt;/', "/&lt;\/p&gt;/", '/&/', '/"/', '/>/', '/</', '/\'/');
            $replacements = array(' ', '', '', '&amp;', '&quot;', '&gt;', '&lt;', '&apos;');
            $link_size = 512;

            fwrite($f, "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
            fwrite($f, "<!DOCTYPE {$args['catalog']} SYSTEM \"shops.dtd\">\n");
            fwrite($f, "<{$args['catalog']} date=\"" . date('Y-m-d G:i') . "\"> \n");
            fwrite($f, "\t<shop>\n");
            fwrite($f, "\t\t<name>" . preg_replace($patterns, $replacements, $args['shop_name']) . "</name>\n");
            fwrite($f, "\t\t<company>" . preg_replace($patterns, $replacements, $args['company']) . "</company>\n");
            fwrite($f, "\t\t<url>http://www." . preg_replace($patterns, $replacements, $args['shop_url']) . "</url>\n");
            fwrite($f, "\t\t<email>" . env('ADMIN_EMAIL') . "</email>\n");
            fwrite($f, "\n");

            //  currency       
            fwrite($f, "\t\t<currencies>\n");
            fwrite($f, "\t\t\t<currency id=\"{$args['currency']}\" rate=\"1\" plus=\"0\"/>\n");
            fwrite($f, "\t\t</currencies>\n\n");

            //  category
            if ($args['categories']) {
                fwrite($f, "\t\t<categories>\n");

                if ($args['id'] != 1) {

                    // products
                    $prods = products::third_level_list(TRUE, FALSE, TRUE, $args['equip']);

                    $cats = categories::get_tree(TRUE, TRUE);

                    foreach ($cats as $cat) {
                        fwrite($f, "\t\t\t<category id=\"{$cat['id']}\" " . ($cat['parent_1'] ? "parentId=\"{$cat['parent_1']}\"" : '') . ">" . preg_replace($patterns, $replacements, $cat['name']) . "</category>\n");
                    }
                } else {

                    // products
                    $prods = products::third_level_list(TRUE, FALSE, TRUE, $args['equip'], 1000);

                    fwrite($f, "\t\t\t<category id=\"21\">Одежда, обувь и аксессуары</category>\n
                                  \t\t\t<category id=\"22\" parentId=\"21\">Красота</category>\n
                                  \t\t\t<category id=\"1\" parentId=\"22\">Уход за лицом</category>\n
                                  \t\t\t<category id=\"5\" parentId=\"22\">Уход за волосами</category>\n
                                  \t\t\t<category id=\"2\" parentId=\"22\">Уход за телом</category>\n
                                  \t\t\t<category id=\"4\" parentId=\"2\">Для ванны и душа</category>\n
                                  \t\t\t<category id=\"6\" parentId=\"2\">Аксессуары</category>\n
                                  \t\t\t<category id=\"3\" parentId=\"2\">Уход за руками</category>\n
                                  \t\t\t<category id=\"7\" parentId=\"2\">Уход за ногами</category>\n");
                }


                fwrite($f, "\t\t</categories>\n\n");
            }

            // local delivery cost
            if ($args['local_delivery_cost']) {
                fwrite($f, "\t\t<local_delivery_cost>{$args['local_delivery_cost']}</local_delivery_cost>\n\n");
            }

            fwrite($f, "\t\t<offers>\n");
            foreach ($prods as $item) {
                if (!empty($item['url'])) {
                    fwrite($f, "\t\t\t<offer id=\"{$item['id']}\" available=\"" . ($item['available'] == 1 ? "true" : "false") . "\">\n");

                    if ($args['url']) {
                        $url_main = "http://{$args['shop_url']}/product/{$item['url']}?utm_source={$args['utm_source']}&amp;utm_campaign={$item['id']}&amp;utm_medium={$args['utm_medium']}&amp;utm_term=";
                        $url_term = urlencode(preg_replace($patterns, $replacements, $item['name']));

                        if (strlen($url_main . $url_term) > $link_size) $url_term = "product_id_{$item['id']}";

                        fwrite($f, "\t\t\t\t<url>" . $url_main . $url_term . "</url>\n");
                    }

                    if ($item['real_special_price']) {
                        fwrite($f, "\t\t\t\t<price>{$item['real_special_price']}</price>\n");
                        fwrite($f, "\t\t\t\t<oldprice>{$item['real_price']}</oldprice>\n");
                    } else {
                        fwrite($f, "\t\t\t\t<price>{$item['price']}</price>\n");
                    }

                    if ($args['currency'])
                        fwrite($f, "\t\t\t\t<currencyId>{$args['currency']}</currencyId>\n");

                    if ($args['categories']) {
                        $this_cat_id = $item['cat_id'];
                        if ($args['equip'] && isset($item['subcat']) && $item['subcat']) $this_cat_id = 7;
                        fwrite($f, "\t\t\t\t<categoryId>$this_cat_id</categoryId>\n");
                    }

                    if ($args['picture']) {

                        $pic_src = $item['src'];
                        if ($item['pic_quant'] > 0) $pic_src = $item['id'] . '_1.jpg';

                        fwrite($f, "\t\t\t\t<picture>" . IMAGES_DOMAIN_FULL . "/images/prod_photo/" . Product::img_url($pic_src, $item['url']) . "</picture>\n");
                    }


                    if ($args['store'])
                        fwrite($f, "\t\t\t\t<store>true</store>\n");

                    if ($args['pickup'])
                        fwrite($f, "\t\t\t\t<pickup>true</pickup>\n");

                    if ($args['delivery'])
                        fwrite($f, "\t\t\t\t<delivery>true</delivery>\n");

                    fwrite($f, "\t\t\t\t<name>" . preg_replace($patterns, $replacements, $item['tm']) . ', ' . preg_replace($patterns, $replacements, $item['name']) . ", " . preg_replace($patterns, $replacements, $item['pack']) . "</name>\n");

                    if ($args['vendor'])
                        fwrite($f, "\t\t\t\t<vendor>" . preg_replace($patterns, $replacements, $item['tm']) . "</vendor>\n");

                    if ($args['description'])
                        fwrite($f, "\t\t\t\t<description>" . preg_replace($patterns, $replacements, $item['short_description']) . "</description>\n");

                    fwrite($f, "\t\t\t\t<manufacturer_warranty>true</manufacturer_warranty>\n");  

                    if ($args['sales_notes'])
                        fwrite($f, "\t\t\t\t<sales_notes>{$args['sales_notes']}</sales_notes>\n");

                    if ($args['country_of_origin'])
                        fwrite($f, "\t\t\t\t<country_of_origin>{$item['country']}</country_of_origin>\n");

                    fwrite($f, "\t\t\t</offer>\n");
                }
            }

            fwrite($f, "\t\t</offers>\n");
            fwrite($f, "\t</shop>\n");
            fwrite($f, "</{$args['catalog']}>");

            fclose($f);

            return TRUE;
        } else {
            return FALSE;
        }
    }


    /**
     * Сохраняет изменения в файле партнерской программы.
     * @param int $id - ID партнерской программы.
     * @param array $data - массив данных для добавления в партнерскую программу.
     * @return boolean
     */
    static function set_partner($id, $data)
    {


        $query = "UPDATE rk_partner
                  SET name = '{$data['name']}',
                      equip = '{$data['equip']}',
                      file_name = '{$data['file_name']}', 
                      utm_source = '{$data['utm_source']}', 
                      utm_medium = '{$data['utm_medium']}', 
                      utm_campaign = '{$data['utm_campaign']}', 
                      catalog = '{$data['catalog']}', 
                      shop_name = '{$data['shop_name']}', 
                      company = '{$data['company']}', 
                      shop_url = '{$data['shop_url']}', 
                      currency = '{$data['currency']}', 
                      delivery = {$data['delivery']}, 
                      local_delivery_cost = '{$data['local_delivery_cost']}',
                      rk_categories = {$data['categories']}, 
                      url = {$data['url']},
                      price = {$data['price']}, 
                      picture = {$data['picture']}, 
                      store = {$data['store']}, 
                      pickup = {$data['pickup']}, 
                      vendor = {$data['vendor']},  
                      description  = {$data['description']}, 
                      sales_notes = '{$data['sales_notes']}',
                      country_of_origin = {$data['country_of_origin']}
 
                  WHERE id = $id";

        return DB::query($query);
    }


    /**
     * Меняет состояние партнерской программы (активна/неактивна)
     * @param int $id - id партнерской программы
     * @return bool
     */
    static function change_active($id)
    {

        $id = (int)$id;
        $partner = self::get_partner($id);
        $active = (int)$partner['active'];
        $file = $partner['file_name'];

        if ($active) {

            if (unlink(SITE_PATH . 'www' . DIRECTORY_SEPARATOR . 'partners' . DIRECTORY_SEPARATOR . "$file" . '.xml')) {

                $query = "UPDATE rk_partner
                      SET active = 0
                      WHERE id = $id";
                db::query($query);
            }
        } else {
            if (self::create_file($partner)) {
                $query = "UPDATE rk_partner
                      SET active = 1
                      WHERE id = $id";
                db::query($query);
            }
        }
    }

    /**
     * Update all partners files
     * @return boolen if success - true and false if fail
     */
    static function update_all()
    {

        $partners = self::get_list(TRUE);

        foreach ($partners as $partner) {
            self::create_file($partner);
        }

    }
    
}

?>