<?php

require_once __DIR__ . '/../includes/setup.php';

DB::query('UPDATE orders_items SET product_status_id = 0 WHERE order_number IN (SELECT o.order_number FROM orders AS o WHERE o.added_date < (NOW() - INTERVAL 7 DAY) AND o.status_id = ' . Orders::STATUS_ID_WAITING . ')');
