<?php

require __DIR__ . '/../includes/setup.php';

$admin_email = [
    env('ADMIN_EMAIL') => env('ADMIN_NAME')
];


/**
 * Order statuses for select
 * @var array
 */
$orders_status = '1, 8, 9'; 

/**
 * Min last order cost
 * @var integer
 */
$min_order_cost = 1500;

/**
 * Client types for send
 * @var array
 */
$c_type_id = '3';

/**
 * Sending emails for report
 * @var integer
 */
$sending_emails = 0;

/**
 * Time limit in seconds for runing script
 * @var integer
 */
$script_time_limit = 300;

/**
 * Min email for sending one time
 * @var integer
 */
$one_time_send_min = 50;

/**
 * Start orders for querry
 * @var integer
 */
$orders_start = 273030;

/**
 * Set mail flag to 1
 *
 * @param int $order_id Order ID
 *
 * @return bool result
 */
function send_plus($order_id)
{
    $order_id = (int)$order_id;

    $query = "UPDATE cdb_orders SET sunlight_mail = 1 WHERE order_number = $order_id";

    return DB::query($query);
}

/**
 * Returns valid email from multiple (If available)
 *
 * @param string $email_1 First email
 * @param string $email_2 Second email
 *
 * @return string|null
 */
function check_email($email_1, $email_2)
{
    return $email_1 && check::email($email_1) ? $email_1 : ($email_2 && check::email($email_2) ? $email_2 : FALSE);
}

/**
 * Generate promo-code and write in base with parameters
 * @return string                   sunligt promo-code
 */
function get_promo_code()
{
    return DB::get_row('SELECT * FROM __sunlight_promo  sp WHERE sp.send = 0 LIMIT 1');
}

/**
 * Lock promo code after sending
 * @param  string $promo_code promo-code text
 * @return bulen              true/false
 */
function lock_promo_code($promo_code_id)
{
    return DB::query("UPDATE __sunlight_promo SET send = 1, date_send = NOW() WHERE id = $promo_code_id");
}

/**
 * Send message about lost cart to user
 *
 * @param string $mail Users email
 * @param string $sub Theme of mail
 * @param string $msg_text info text
 *
 * @return bool result of mail
 */
function msg_roskosmetika($mail, $sub, $msg_text)
{
    $to = DEBUG ? env('ADMIN_EMAIL') : $mail;
    $from = [
        'sales@roskosmetika.ru' => 'Роскосметика',
    ];

    Template::$utm_term = $to;

    $mailContent = Template::mail_template(
        'lc_return_client',
        $sub,
        [
            'text' => $msg_text,
            'neighbours_html' => Template::mail_neighbours(Product::cart_neighbours(Products::get_hits(30), false, 4)),
        ] 
    );

    try {
        $sent = @mailer::mail(
            $sub,
            $from,
            $to,
            $mailContent
        );
    } catch (Exception $e) {
        $sent = false;
    }

    return $sent;
}

// start script
$time_start = $time_end = microtime(true);
$time = 0;

Template::init('templates');

Template::$utm_source = 'rk-sunligh';
Template::$utm_medium = 'email';
Template::$utm_campaign = 'rk-sunlight-action-2019';

//  get orders
$query = "SELECT o.order_number, c.id AS client_id, c.type_id, o.shipping_person_id, o.cost, 
                 IF((c.email IS NOT NULL OR c.email <> ''), c.email, (SELECT sp.email FROM shipping_persons sp WHERE sp.id = o.shipping_person_id AND sp.email IS NOT NULL AND sp.email <> '' LIMIT 1)) AS c_email, 
                 IF((o.email IS NOT NULL OR o.email <> ''), o.email, (SELECT sp.email FROM shipping_persons sp WHERE sp.id = o.shipping_person_id AND sp.email IS NOT NULL AND sp.email <> '' LIMIT 1)) AS o_email, 
                 CONCAT(IFNULL(c.name, ''), ' ', IFNULL(c.patronymic, '')) AS name
          FROM  cdb_orders co, orders AS o
          LEFT JOIN clients AS c ON o.client_id = c.id
          WHERE o.order_number = co.order_number 
          AND o.auto_order = 0
          AND co.sunlight_mail = 0
          AND (c.email IS NOT NULL OR o.email IS NOT NULL OR (SELECT COUNT(sp.id) FROM shipping_persons sp WHERE sp.id = o.shipping_person_id AND sp.email IS NOT NULL AND sp.email <> '') > 0)
          AND (c.email <> '' OR o.email <> '' OR (SELECT COUNT(sp.id) FROM shipping_persons sp WHERE sp.id = o.shipping_person_id AND sp.email IS NOT NULL AND sp.email <> '') > 0)
          AND o.status_id IN (" . $orders_status . ")
          AND o.cost > $min_order_cost
          AND c.type_id IN (" . $c_type_id . ")
          AND  DATE_FORMAT(co.date_of_change, '%Y-%m-%d') = CURRENT_DATE()
          AND  o.order_number > $orders_start";

$orders = DB::get_rows($query);

for ($i=0; $i < count($orders); $i++)
{
  $client_email = check_email($orders[$i]['c_email'], $orders[$i]['o_email']);
  $promo_code = get_promo_code();

    if ($client_email && $promo_code) {

          $c_name = trim($orders[$i]['name']);
          $c_name = ($c_name != '' ? $c_name : FALSE);

          $msg_text = "<br>
                    <h1 style=\"font-family: Tahoma, Geneva, sans-serif;font-size: 16px;color: #575762;\">" . ($c_name ? ($c_name . ', здравствуйте') : 'Здравствуйте') . "!</h1>
                    <p style=\"font-family: Tahoma, Geneva, sans-serif;font-size: 12px;color: #575762;margin-top: 10px;margin-bottom: 16px; font-weight:bold;\">
                      Отличные новости! Новая акция от наших друзей - SUNLIGHT!
                    </p>
                    <p style=\"font-family: Tahoma, Geneva, sans-serif;font-size: 12px;color: #575762;margin-top: 8px;margin-bottom: 12px;\">
                      Только до 19 июля, SUNLIGHT дарит Вам промо-код на получение <span style=\"color:#f63e3c;\">подарка</span> в торговой точке ювелирной сети, а также <span style=\"color:#f63e3c;\">3000</span> бонусных рублей на бонусный счет!
                    </p>
                    <p style=\"font-family: Tahoma, Geneva, sans-serif;font-size: 12px;color: #575762;margin-top: 8px;margin-bottom: 12px;\">
                      Ваш подарочный промо-код:&nbsp;<span style=\"font-size: 14px; font-weight:bold;\">\"" . $promo_code['code'] . "\"</span>
                    </p>
                    <p style=\"font-family: Tahoma, Geneva, sans-serif;font-size: 12px;color: #575762;margin-top: 8px;margin-bottom: 20px;\">
                     <a style=\"color: #9b1d97;\" target=\"_blank\" href=\"https://www.roskosmetika.ru/page/partner-action" . apply_utm(null, 'rk-sunligh') . "\">Подробная информация и правила проведения акции.</a>
                    </p>
                    <p style=\"font-family: Tahoma, Geneva, sans-serif;font-style: italic;font-size: 10px;color: #575762;margin-top: 10px;margin-bottom: 6px;\">
                      <span style=\"color:#f63e3c;\">Подарочный промо код может быть использован только Вами.</span>
                    </p>";

          if (msg_roskosmetika($client_email, 'Подарки от наших друзей  - SUNLIGHT! Только до 19 июля!', $msg_text)) 
          {
              send_plus($orders[$i]['order_number']);
              lock_promo_code($promo_code['id']);
              ++$sending_emails;

              if (count($orders) > $one_time_send_min) sleep(2);
          }

          $time_end = microtime(true);
          $time = $time_end - $time_start;

          if ($time > $script_time_limit) break;
    }


}

if (count($orders) > 0) 
{
  if (!DEBUG) {
      mailer::mail(
          'Скрипт партнерской акции SUNLIGHT',
          [
              'sales@roskosmetika.ru' => 'Площадка u29842 (клиентская база)',
          ],
          $admin_email,
          '<html><body><div>Скрипт sunlight_action.php выполнен. Отправлено писем: ' . $sending_emails . '.</div></body></html>'
      );
  }
}