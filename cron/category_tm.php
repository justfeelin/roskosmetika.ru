<?php

require __DIR__ . '/../includes/setup.php';

optimization::category_tm();
sitemap::update_map();

mailer::mail(
    'Перелинковка category-tm',
    [
        'news@roskosmetika.ru' => 'Площадка u29842 (роскосметика)',
    ],
    [
        env('ADMIN_EMAIL') => env('ADMIN_EMAIL'),
    ],
    '<html><body><div>Скрипт category_tm.php выполнен.</div></body></html>'
);
