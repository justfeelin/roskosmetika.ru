<?php

require __DIR__ . '/../includes/setup.php';

$db1C = new DbConnection(env('DB_1C_HOST'), env('DB_1C_USER'), env('DB_1C_PASSWORD'), env('DB_1C_NAME'));

if ((int)$db1C->getField('flag', 'SELECT flag FROM _flags WHERE `name` = "ready"') !== 2) {
    return;
}

$exludeProductsSQL = implode(', ', Product::ignore_ids());

$clientIDs = [];

// products
DB::get_rows(
    'SELECT * FROM (
        SELECT p.id, p.pack,
            IF(
                p.main_id = 0,
                IF(
                    cp.supplier_name = "" OR cp.supplier_name IS NULL,
                    p.name,
                    cp.supplier_name
                ),
                (
                    SELECT IF(
                        mcp.supplier_name = "" OR mcp.supplier_name IS NULL,
                        mp.name,
                        mcp.supplier_name
                    )
                    FROM products AS mp
                    LEFT JOIN cdb_products AS mcp ON mcp.id = mp.id
                    WHERE mp.id = p.main_id
                )
            ) AS name,
            p.ss_odinass,
            p.rk_odinass,
            (
                SELECT IF(COUNT(o.order_number) > 0, 1, 0)
                FROM orders AS o
                WHERE o.order_number IN (
                    SELECT oi.order_number
                    FROM orders_items AS oi
                    WHERE oi.product_id = p.id
                )
                    AND o.payway_id = 1
            ) AS ss_count_orders,
            (
                SELECT IF(COUNT(o.order_number) > 0, 1, 0)
                FROM orders AS o
                WHERE o.order_number IN (
                    SELECT oi.order_number
                    FROM orders_items AS oi
                    WHERE oi.product_id = p.id
                )
                  AND o.payway_id = 3
            ) AS rk_count_orders,
            (
                SELECT IF(COUNT(a.id) > 0, 1, 0)
                FROM cdb_arrivals AS a
                WHERE a.id IN (
                    SELECT ai.arrival_id
                    FROM cdb_arrivals_items AS ai
                    WHERE ai.product_id = p.id
                )
                    AND a.contractor_id = 1
            ) AS ss_count_arrivals,
            (
                SELECT IF(COUNT(a.id) > 0, 1, 0)
                FROM cdb_arrivals AS a
                WHERE a.id IN (
                    SELECT ai.arrival_id
                    FROM cdb_arrivals_items AS ai
                    WHERE ai.product_id = p.id
                )
                    AND a.contractor_id = 2
            ) AS rk_count_arrivals
        FROM products AS p
        LEFT JOIN cdb_products AS cp ON cp.id = p.id
        WHERE p.id NOT IN (' . $exludeProductsSQL . ')
    ) AS p
    WHERE (
        p.ss_odinass = 0 AND (p.ss_count_orders > 0 OR p.ss_count_arrivals > 0)
        OR p.rk_odinass = 0 AND (p.rk_count_orders > 0 OR p.rk_count_arrivals > 0)
    )
    ',
    function ($product) use (&$db1C) {
        $name = html_entity_decode($product['name'] . ' ' . $product['pack']);

        $db1C->query('REPLACE INTO products (id, short_name, `name`, sync_flag) VALUES (' .  $product['id'] . ', "' . $db1C->escape(mb_substr($name, 0, 100)) . '", "' . $db1C->escape(mb_substr($name, 0, 250)) . '", 0)');
    }
);

// orders
DB::get_rows(
    'SELECT o.order_number, o.invoice, o.client_id, o.cost, o.invoice_date, o.delivery, o.payway_id,
        (SELECT SUM(oi.final_price * oi.quantity) FROM orders_items AS oi WHERE oi.order_number = o.order_number) AS products_sum,
        c.id AS c_client_id
    FROM orders AS o
    LEFT JOIN clients AS c ON c.id = o.client_id AND LTRIM(RTRIM(c.inn)) != "" AND c.inn IS NOT NULL
        AND (
            o.contractor_id = 1 AND c.ss_odinass = 0
            OR o.contractor_id = 2 AND c.rk_odinass = 0
        )
    WHERE
        o.odinass = 0
        AND o.status_id IN (' . implode(', ', [Orders::STATUS_ID_DONE, Orders::STATUS_ID_STOCK_DONE, Orders::STATUS_ID_DELIVERY]) . ')
        AND o.approved = 1
        AND o.payway_id IN (1, 3)
    ',
    function ($order) use (&$db1C, &$clientIDs, $exludeProductsSQL) {
        if ($order['c_client_id'] && !in_array((int)$order['c_client_id'], $clientIDs, true)) {
            $clientIDs[] = (int)$order['c_client_id'];
        }

        $costDifference = floatraw((float)$order['products_sum'] + (float)$order['delivery'] - (float)$order['cost']);

        DB::get_rows(
            'SELECT oi.id, oi.product_id, oi.quantity, oi.final_price
            FROM orders_items AS oi
            WHERE oi.order_number = ' . $order['order_number'] . '
                AND oi.product_id NOT IN (' . $exludeProductsSQL . ')
            ORDER BY oi.final_price DESC
            ',
            function ($oItem) use ($order, &$db1C, &$costDifference) {
                if ($costDifference > 0) {
                    $oldPrice = floatraw($oItem['final_price']);

                    $oItem['quantity'] = (int)$oItem['quantity'];

                    $oItem['final_price'] = $oldPrice * $oItem['quantity'] >= $costDifference
                        ? floatraw((int)((int)($oldPrice * 100.0) * $oItem['quantity'] - (int)($costDifference * 100.0)) / $oItem['quantity'] / 100.0)
                        : '0.01';

                    $costDifference = floatraw($costDifference - floatraw(($oldPrice - $oItem['final_price']) * $oItem['quantity']));
                }

                $db1C->query('REPLACE INTO orders_items (id, order_number, product_id, quantity, final_price) VALUES (' . $oItem['id'] . ', ' . $order['order_number'] . ', ' . $oItem['product_id'] . ', ' . ($oItem['quantity']) . ', ' . floatraw($oItem['final_price']) . ')');
            }
        );

        if ($costDifference != 0) {
            if ($costDifference > 0) {
                $products = DB::get_rows('SELECT oi.product_id, oi.quantity FROM orders_items AS oi WHERE oi.order_number = ' . $order['order_number'] . ' ORDER BY oi.quantity');

                $orderCost = floatraw((float)$order['cost'] - (float)$order['delivery']);

                $n = count($products);

                for ($i = 0; $i < $n; ++$i) {
                    $productIDs = [
                        $products[$i]['product_id'],
                    ];

                    $quantity = (int)$products[$i]['quantity'];

                    for ($y = 0; $y < $n; ++$y) {
                        if ($y !== $i) {
                            $productIDs[] = $products[$y]['product_id'];
                            $quantity += (int)$products[$y]['quantity'];
                        }

                        $productsSQL = implode(', ', $productIDs);

                        $info = DB::get_row(
                            'SELECT SUM(oio.quantity) AS others_quantity
                                    FROM orders_items AS oio
                                    WHERE oio.order_number = ' . $order['order_number'] . '
                                        AND oio.product_id NOT IN (' . $productsSQL . ')
                            '
                        );

                        if ($info['others_quantity'] == 0) {
                            continue;
                        }

                        $othersPrice = floatraw((int)($orderCost / (int)$info['others_quantity'] * 100) / 100.0);

                        $othersRemain = $orderCost - $othersPrice * (int)$info['others_quantity'];

                        if (!((int)($othersRemain * 100) % $quantity)) {
                            $db1C->query('UPDATE orders_items SET final_price = ' . floatraw($othersRemain / (float)$quantity) . ' WHERE order_number = ' . $order['order_number'] . ' AND product_id IN (' . $productsSQL . ')');

                            $db1C->query('UPDATE orders_items SET final_price = ' . floatraw($othersPrice) . ' WHERE order_number = ' . $order['order_number'] . ' AND product_id NOT IN (' . $productsSQL . ')');

                            break 2;
                        }
                    }
                }
            } else {
                $order['delivery'] = (float)$order['delivery'] - $costDifference;
            }
        }

        $db1C->query('REPLACE INTO orders (order_number, invoice, client_id, cost, invoice_date, delivery_price, contractor_id, sync_flag) VALUES (' . $order['order_number'] . ', "' . $db1C->escape($order['invoice']) . '", ' . ($order['c_client_id'] ? (int)$order['c_client_id'] : 0) . ', ' .floatraw($order['cost']) . ', "' . $db1C->escape($order['invoice_date']) . '", ' . floatraw($order['delivery']) . ', ' . ($order['payway_id'] == 1 ? 1 : 2) . ', 0)');
    }
);

// arrivals
DB::get_rows(
    'SELECT a.id, a.price, a.tax_price, a.invoice, a.invoice_date, a.client_id, a.contractor_id,
        c.id AS c_client_id,
        (SELECT SUM(ai.price * ai.quantity) FROM cdb_arrivals_items AS ai WHERE ai.arrival_id = a.id) AS products_sum
    FROM cdb_arrivals AS a
    LEFT JOIN clients AS c ON c.id = a.client_id AND LTRIM(RTRIM(c.inn)) != "" AND c.inn IS NOT NULL
        AND (
            a.contractor_id = 1 AND c.ss_odinass = 0
            OR a.contractor_id = 2 AND c.rk_odinass = 0
        )
    WHERE a.bw_flag = 1
        AND a.odinass = 0
        AND a.approved = 1
        AND a.contractor_id IN (1, 2)
    ',
    function ($arrival) use (&$db1C, &$clientIDs) {
        $db1C->query('REPLACE INTO arrivals (id, price, tax_price, invoice, invoice_date, client_id, contractor_id, sync_flag) VALUES (' . $arrival['id'] . ', ' . floatraw($arrival['price']) . ', ' . floatraw($arrival['tax_price']) . ', "' . $db1C->escape($arrival['invoice']) . '", "' . $db1C->escape($arrival['invoice_date']) . '", ' . $arrival['client_id'] . ', ' . ($arrival['contractor_id'] == 1 ? 1 : 2) . ', 0)');

        $arrival['price'] = floatraw($arrival['price']);

        $costDifference = floatraw((float)$arrival['products_sum'] - $arrival['price']);

        DB::get_rows(
            'SELECT ai.product_id, ai.quantity, ai.price, ai.tax_price, ai.tax FROM cdb_arrivals_items AS ai WHERE ai.arrival_id = ' . $arrival['id'] . ' ORDER BY ai.quantity, ai.price DESC',
            function ($aItem) use ($arrival, &$db1C, &$costDifference) {
                if ($costDifference > 0) {
                    $oldPrice = floatraw($aItem['price']);

                    $aItem['quantity'] = (int)$aItem['quantity'];

                    $aItem['price'] = $oldPrice * $aItem['quantity'] >= $costDifference
                        ? floatraw((int)((int)($oldPrice * 100.0) * $aItem['quantity'] - (int)($costDifference * 100.0)) / $aItem['quantity'] / 100.0)
                        : '0.01';

                    $costDifference = floatraw($costDifference - floatraw(($oldPrice - $aItem['price']) * $aItem['quantity']));
                }

                $db1C->query('INSERT INTO arrivals_items (arrival_id, product_id, quantity, price, tax_price, tax) VALUES (' . $arrival['id'] . ', ' . $aItem['product_id'] . ', ' . $aItem['quantity'] . ', ' . floatraw($aItem['price']) . ', ' . floatraw($aItem['tax_price']) . ', ' . ((int)$aItem['tax']) . ')');
            }
        );

        if ($costDifference != 0) {
            if ($costDifference > 0) {
                $products = DB::get_rows('SELECT ai.product_id, ai.quantity FROM cdb_arrivals_items AS ai WHERE ai.arrival_id = ' . $arrival['id'] . ' ORDER BY ai.quantity');

                $n = count($products);

                for ($i = 0; $i < $n; ++$i) {
                    $productIDs = [
                        $products[$i]['product_id'],
                    ];

                    $quantity = (int)$products[$i]['quantity'];

                    for ($y = 0; $y < $n; ++$y) {
                        if ($y !== $i) {
                            $productIDs[] = $products[$y]['product_id'];
                            $quantity += (int)$products[$y]['quantity'];
                        }

                        $productsSQL = implode(', ', $productIDs);

                        $info = DB::get_row(
                            'SELECT SUM(aio.quantity) AS others_quantity
                                    FROM cdb_arrivals_items AS aio
                                    WHERE aio.arrival_id = ' . $arrival['id'] . '
                                        AND aio.product_id NOT IN (' . $productsSQL . ')
                            '
                        );

                        if ($info['others_quantity'] == 0) {
                            continue;
                        }

                        $othersPrice = floatraw((int)($arrival['price'] / (int)$info['others_quantity'] * 100) / 100.0);

                        $othersRemain = $arrival['price'] - $othersPrice * (int)$info['others_quantity'];

                        if (!((int)($othersRemain * 100) % $quantity)) {
                            $db1C->query('UPDATE arrivals_items SET price = ' . floatraw($othersRemain / (float)$quantity) . ' WHERE arrival_id = ' . $arrival['id'] . ' AND product_id IN (' . $productsSQL . ')');

                            $db1C->query('UPDATE arrivals_items SET price = ' . floatraw($othersPrice) . ' WHERE arrival_id = ' . $arrival['id'] . ' AND product_id NOT IN (' . $productsSQL . ')');

                            break 2;
                        }
                    }
                }
            } else {
                $product = $db1C->getRow('SELECT ai.product_id, ai.quantity, ai.price, ai.tax_price, ai.tax FROM arrivals_items AS ai WHERE ai.arrival_id = ' . $arrival['id'] . ' ORDER BY ai.quantity, ai.price LIMIT 1');

                $add = floatraw(-1 * $costDifference);

                if ($product['quantity'] == 1) {
                    $db1C->query('UPDATE arrivals_items SET price = price + ' . $add . ' WHERE arrival_id = ' . $arrival['id'] . ' AND product_id = ' . $product['product_id'] . ' ORDER BY quantity, price LIMIT 1');
                } else {
                    $db1C->query('UPDATE arrivals_items SET quantity = quantity - 1 WHERE arrival_id = ' . $arrival['id'] . ' AND product_id = ' . $product['product_id']);

                    $db1C->query('INSERT INTO arrivals_items (arrival_id, product_id, quantity, price, tax_price, tax) VALUES(' . $arrival['id'] . ', ' . $product['product_id'] . ', 1, ' . $add . ' + ' . $product['price'] . ', ' . $product['tax_price'] . ', ' . $product['tax'] . ')');
                }
            }
        }

        if ($arrival['c_client_id'] && !in_array((int)$arrival['c_client_id'], $clientIDs, true)) {
            $clientIDs[] = (int)$arrival['c_client_id'];
        }
    }
);

// clients
if ($clientIDs) {
    DB::get_rows(
        'SELECT c.id, c.name, c.surname, c.patronymic, c.type_id, c.inn, c.bik, c.kpp, c.account, c.loro_account, c.address, c.phone, c.is_supplier
        FROM clients AS c
        WHERE c.id IN (' . implode(', ', $clientIDs) . ')
        ',
        function ($client) use (&$db1C) {
            $fio = [];

            if ($surname = trim($client['surname'])) {
                $fio[] = $surname;
            }

            if ($name = trim($client['name'])) {
                $fio[] = $name;
            }

            if ($patronymic = trim($client['patronymic'])) {
                $fio[] = $patronymic;
            }

            $db1C->query('REPLACE INTO clients (id, `name`, is_organization, inn, bik, kpp, `account`, loro_account, address, phone, is_supplier, sync_flag) VALUES (' . $client['id'] . ', "' . $db1C->escape(implode(' ', $fio)) . '", ' . ($client['type_id'] == 2 ? 1 : 0) . ', "' . $db1C->escape(trim($client['inn'])) . '", "' . $db1C->escape(trim($client['bik'])) . '", "' . $db1C->escape(trim($client['kpp'])) . '", "' . $db1C->escape(trim($client['account'])) . '", "' . $db1C->escape(trim($client['loro_account'])) . '", "' . $db1C->escape(trim($client['address'])) . '", "' . $db1C->escape(trim($client['phone'])) . '", ' . ($client['is_supplier'] ? 1 : 0) . ', 0)');
        }
    );
}

$db1C->query('UPDATE _flags SET flag = 1 WHERE `name` = "ready"');
