<?php

require __DIR__ . '/../includes/setup.php';

$t = microtime(true);

DB::query('INSERT INTO rk_unsubscribe (email, hash, status, active, date) SELECT c.email, MD5(c.email), "self", 1, NOW() FROM clients AS c WHERE c.email LIKE "%@%" AND c.email IS NOT NULL AND c.email NOT IN (SELECT r.email FROM rk_unsubscribe AS r) ON DUPLICATE KEY UPDATE rk_unsubscribe.email = rk_unsubscribe.email');

print 'Number of imported from clients: ' . DB::last_query_info() . "\n";

$updated = 0;

$res = DB::query('SELECT u.email, u.date,
    (SELECT c.id FROM clients AS c WHERE c.email = u.email LIMIT 1) AS client_id
    FROM rk_unsubscribe AS u
    LEFT JOIN clients AS c ON c.email = u.email
    WHERE u.status != "spam" AND u.active = 1 AND u.block = 0
    ');

print 'Total rows: ' . $res->num_rows . "\n";

$i = 1;

$remove_managers = '1, 2, 3, 4, 5, 9, 11, 19, 20, 22, 23, 25, 9999';

while ($row = $res->fetch_assoc()) {
    $recently = $buyer = $update = false;

    $monetary = $frequency = 0;

    $manager = null;

    $updates = [];

    if ($row['client_id']) {
        $recently = DB::get_field('added_date', 'SELECT MAX(o.added_date) AS added_date FROM orders AS o WHERE o.shipping_person_id IN (SELECT sp.id FROM shipping_persons AS sp WHERE sp.client_id = ' . $row['client_id'] . ') AND o.status_id = ' . Orders::STATUS_ID_DONE);

        if ($recently) {
            $buyer = true;

            $orders = DB::get_row('SELECT AVG(o.cost) AS cost, COUNT(o.order_number) AS cnt FROM orders AS o WHERE o.shipping_person_id IN (SELECT sp.id FROM shipping_persons AS sp WHERE sp.client_id = ' . $row['client_id'] . ') AND o.status_id = ' . Orders::STATUS_ID_DONE);

            $monetary = (int)$orders['cost'];
            $frequency = $orders['cnt'];

            $recently = substr($recently, 0, 10);
        }

        $client = DB::get_row('SELECT c.name, c.surname, c.patronymic, c.type_id,
            (SELECT a.region_id FROM address AS a WHERE a.client_id = c.id LIMIT 1) AS region_id,
            IF(m.id IN (' . $remove_managers . '), "Цыбульская Ольга", m.name) AS manager
            FROM clients AS c
            LEFT JOIN cdb_users AS m ON m.id = c.user_id
            WHERE c.id = ' . $row['client_id'] . ' AND (c.name != "" OR c.surname != "")');

        if ($client) {
            if ($client['name']) {
                $updates['first_name'] = $client['name'];
            }

            if ($client['surname']) {
                $updates['last_name'] = $client['surname'];
            }

            if ($client['patronymic']) {
                $updates['patronymic'] = $client['patronymic'];
            }

            if ($client['region_id']) {
                $updates['region'] = (int)$client['region_id'];
            }

            if ($client['type_id']) {
                $updates['client_type'] = (int)$client['type_id'];
            }

            if ($client['manager']) {
                $manager = $client['manager'];
            }
        }
    }

    $updates['manager'] = $manager ?: 'Цыбульская Ольга';

    if (!$recently) {
        $info = DB::get_row('SELECT COUNT(DISTINCT(DATE_FORMAT(cr.created, "%d.%c.%Y"))) AS cnt, MAX(cr.created) AS created FROM rk_credentials AS cr WHERE cr.email = "' . DB::mysql_secure_string($row['email']) . '"');

        if ($info) {
            $recently = $info['created'];
            $frequency = $info['cnt'];
        }
    }

    $updates += [
        'visitors' => $buyer ? 'buyer' : 'viewer',
        'frequency' => $frequency,
        'monetary' => $monetary,
        'recently' => $recently ?: $row['date'],
    ];

    $sqlSet = [];

    foreach ($updates as $field => $value) {
        $sqlSet[] = $field . ' = ' . (is_numeric($value) ? $value : '"' . DB::mysql_secure_string($value) . '"');
    }

    $query = 'UPDATE rk_unsubscribe SET ' . implode(', ', $sqlSet) . ' WHERE email = "' . DB::mysql_secure_string($row['email']) . '"';

    DB::query($query);

    if (DB::last_query_info()) {
        DB::query('UPDATE rk_unsubscribe SET updated = 1 WHERE email = "' . DB::mysql_secure_string($row['email']) . '"');

        ++$updated;
    }

    console_print('Row #' . $i . ' (' . (int)($i / $res->num_rows * 100) . '%), updated: ' . $updated . ' (' . ((int)($updated / $res->num_rows * 100)) . '%), ' . ((int)(microtime(true) - $t)) . ' sec.');

    ++$i;
}

$res->close();

console_print('Done in ' . ((int)(microtime(true) - $t)) . ' seconds, updated rows count: ' . $updated . "\n");
