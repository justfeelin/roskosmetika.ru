<?php

require __DIR__ . '/../includes/setup.php';

$today = date('Y-m-d');

$t = microtime(true);

$limit = 300;

$processed = 0;

DB::query('DELETE FROM `__cart_neighbours` WHERE updated < "' .  date('Y-m-d', $_SERVER['REQUEST_TIME'] - 24 * 60 * 60) . '"');

register_shutdown_function(function () use ($t, &$processed) {
    echo "Processed $processed products, " . (int)(microtime(true) - $t) . "sec taken\n";
});

DB::get_rows(
    'SELECT p.id FROM products AS p WHERE p.id != ' . Orders::PRESENT_PRODUCT_ID . ' AND p.visible = 1 AND p.main_id = 0 AND (p.id NOT IN (SELECT n.product_id FROM __cart_neighbours AS n WHERE n.updated = "' . $today . '"))',
    function ($product) use ($today, &$processed, $limit) {
        $productID = (int)$product['id'];

        $orderIDs = [];

        DB::get_rows('SELECT DISTINCT c.order_number FROM orders_items AS c WHERE c.product_id = ' . $productID . ' ORDER BY c.id DESC LIMIT 15', function ($order) use (&$orderIDs) {
            $orderIDs[] = (int)$order['order_number'];
        });

        if ($orderIDs) {
            $orders = implode(',', $orderIDs);

            DB::get_rows(
                'SELECT *
                FROM
                    (
                        SELECT
                            DISTINCT p.id,
                            (
                                SELECT SUM(cc.quantity) FROM orders_items AS cc WHERE cc.product_id = c.product_id AND cc.order_number IN (' . $orders . ')
                            ) AS cnt
                        FROM orders_items AS c
                        INNER JOIN products AS p ON p.id = c.product_id AND p.visible = 1 AND p.main_id = 0
                        WHERE
                            c.order_number IN (' . $orders . ')
                            AND c.product_id != ' . Orders::PRESENT_PRODUCT_ID . '
                            AND c.product_id != ' . $productID . '
                    ) AS p
                        ORDER BY p.cnt DESC
                    LIMIT 10',
                function ($neighbour) use ($productID, $today) {
                    DB::query('INSERT INTO __cart_neighbours (product_id, neighbour_id, cnt, updated) VALUES (' . $productID . ', ' . $neighbour['id'] . ', ' . $neighbour['cnt'] . ', "' . $today . '")');
                }
            );

            if (++$processed === $limit) {
                exit;
            }
        }
    }
);
