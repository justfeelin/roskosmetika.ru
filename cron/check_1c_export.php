<?php

require __DIR__ . '/../includes/setup.php';

$db1C = new DbConnection(env('DB_1C_HOST'), env('DB_1C_USER'), env('DB_1C_PASSWORD'), env('DB_1C_NAME'));

if ((int)$db1C->getField('flag', 'SELECT flag FROM _flags WHERE name = "ready"') !== 0) {
    return;
}

$db1C->getRows(
    'SELECT p.id,
        (
            SELECT COUNT(o.order_number)
            FROM orders AS o
            WHERE o.order_number IN (
                SELECT oi.order_number
                FROM orders_items AS oi
                WHERE oi.product_id = p.id
            )
                AND o.contractor_id = 1
                AND o.sync_flag = 1
        ) AS ss_count_orders,
        (
            SELECT COUNT(o.order_number)
            FROM orders AS o
            WHERE o.order_number IN (
                SELECT oi.order_number
                FROM orders_items AS oi
                WHERE oi.product_id = p.id
            )
                AND o.contractor_id = 2
                AND o.sync_flag = 1
        ) AS rk_count_orders,
        (
            SELECT COUNT(a.id)
            FROM arrivals AS a
            WHERE a.id IN (
                SELECT ai.arrival_id
                FROM arrivals_items AS ai
                WHERE ai.product_id = p.id
            )
                AND a.contractor_id = 1
                AND a.sync_flag = 1
        ) AS ss_count_arrivals,
        (
            SELECT COUNT(a.id)
            FROM arrivals AS a
            WHERE a.id IN (
                SELECT ai.arrival_id
                FROM arrivals_items AS ai
                WHERE ai.product_id = p.id
            )
                AND a.contractor_id = 2
                AND a.sync_flag = 1
        ) AS rk_count_arrivals
    FROM products AS p
    WHERE p.sync_flag = 1
    ',
    function ($product1C) use (&$db1C) {
        DB::query(
            'UPDATE products
            SET ss_odinass = IF(ss_odinass = 1 OR ' . ($product1C['ss_count_orders'] > 0 || $product1C['ss_count_arrivals'] > 0 ? 1 : 0) . ' > 0, 1, 0),
                rk_odinass = IF(rk_odinass = 1 OR ' . ($product1C['rk_count_orders'] > 0 || $product1C['rk_count_arrivals'] > 0 ? 1 : 0) . ' > 0, 1, 0)
            WHERE id = ' . $product1C['id']
        );

        $db1C->query('DELETE FROM products WHERE id = ' . $product1C['id']);
    }
);

$db1C->query('TRUNCATE TABLE products');

$db1C->getRows(
    'SELECT c.id,
        (
            SELECT COUNT(o.order_number)
            FROM orders AS o
            WHERE o.client_id = c.id
                AND o.contractor_id = 1
                AND o.sync_flag = 1
        ) AS ss_count_orders,
        (
            SELECT COUNT(o.order_number)
            FROM orders AS o
            WHERE o.client_id = c.id
                AND o.contractor_id = 2
                AND o.sync_flag = 1
        ) AS rk_count_orders,
        (
            SELECT COUNT(a.id)
            FROM arrivals AS a
            WHERE a.client_id = c.id
                AND a.contractor_id = 1
                AND a.sync_flag = 1
        ) AS ss_count_arrivals,
        (
            SELECT COUNT(a.id)
            FROM arrivals AS a
            WHERE a.client_id = c.id
                AND a.contractor_id = 2
                AND a.sync_flag = 1
        ) AS rk_count_arrivals
    FROM clients AS c WHERE c.sync_flag = 1',
    function ($client1C) use (&$db1C) {
        DB::query(
            'UPDATE clients
            SET ss_odinass = IF(ss_odinass = 1 OR ' . ($client1C['ss_count_orders'] > 0 || $client1C['ss_count_arrivals'] > 0 ? 1 : 0) . ' > 0, 1, 0),
                rk_odinass = IF(rk_odinass = 1 OR ' . ($client1C['rk_count_orders'] > 0 || $client1C['rk_count_arrivals'] > 0 ? 1 : 0) . ' > 0, 1, 0)
            WHERE id = ' . $client1C['id']
        );
    }
);

$db1C->query('TRUNCATE TABLE clients');

$db1C->getRows(
    'SELECT a.id FROM arrivals AS a WHERE a.sync_flag = 1',
    function ($arrival1C) use (&$db1C) {
        DB::query('UPDATE cdb_arrivals SET odinass = 1 WHERE id = ' . $arrival1C['id']);
    }
);

$db1C->query('TRUNCATE TABLE arrivals');
$db1C->query('TRUNCATE TABLE arrivals_items');

$db1C->getRows(
    'SELECT o.order_number FROM orders AS o WHERE o.sync_flag = 1',
    function ($order1C) use (&$db1C) {
        DB::query('UPDATE orders SET odinass = 1 WHERE order_number = ' . $order1C['order_number']);
    }
);

$db1C->query('TRUNCATE TABLE orders');
$db1C->query('TRUNCATE TABLE orders_items');

$db1C->query('UPDATE _flags SET flag = 2 WHERE `name` = "ready"');
