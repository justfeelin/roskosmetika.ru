<?php
require __DIR__ . '/../includes/setup.php';

sitemap::update_map();

mailer::mail(
    'Обновление sitemap',
    [
        'news@roskosmetika.ru' => 'Площадка u29842 (роскосметика)',
    ],
    [
        env('ADMIN_EMAIL') => env('ADMIN_NAME'),
    ],
    '<html><body><div>Скрипт sitemap.php выполнен.</div></body></html>'
);
