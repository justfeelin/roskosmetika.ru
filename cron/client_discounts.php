<?php

require __DIR__ . '/../includes/setup.php';

$sqlOrderStatuses = implode(', ', [
    Orders::STATUS_ID_DONE,
    Orders::STATUS_ID_STOCK_DONE,
    Orders::STATUS_ID_DELIVERY,
]);

DB::get_rows(
    'SELECT c.id, c.type_id, c.auto_discount,
        (SELECT COUNT(o.order_number) FROM orders AS o WHERE o.client_id = c.id AND o.status_id IN (' . $sqlOrderStatuses . ')) AS cnt_orders,
        (SELECT SUM(o.cost) FROM orders AS o WHERE o.client_id = c.id AND o.status_id IN (' . $sqlOrderStatuses . ')) AS sum_orders
    FROM clients AS c
    WHERE c.archive = 0',
    function ($client) {
        $discounts = [];

        if ($client['type_id'] == Users::CLIENT_TYPE_MASSEUR) {
            $discounts[] = 7;
        }

        if ($client['type_id'] == Users::CLIENT_TYPE_COMPANY) {
            $discounts[] = 10;
        }

        if ((int)$client['cnt_orders'] >= 2) {
            $discounts[] = 5;
        }

        $sumOrders = (int)$client['sum_orders'];

        switch (true) {
            case $sumOrders > 200000:
                $discounts[] = 15;

                break;

            case $sumOrders > 100000:
                $discounts[] = 10;

                break;

            case $sumOrders > 50000:
                $discounts[] = 7;

                break;
        }

        if ($discounts) {
            $discount = max($discounts);

            if ($discount != $client['auto_discount']) {
                DB::query('UPDATE clients SET auto_discount = ' . $discount . ' WHERE id = ' . $client['id']);
            }
        }
    }
);
