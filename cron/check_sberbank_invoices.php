<?php

require __DIR__ . '/../includes/setup.php';

DB::get_rows(
    'SELECT i.sberbank_id FROM rk_sberbank_invoice AS i WHERE i.paid = 0 AND i.created > (NOW() - INTERVAL 3 DAY)',
    function ($invoice) {
        Payment::check_sberbank_paid($invoice['sberbank_id']);
    }
);
