<?php

require_once __DIR__ . '/../includes/setup.php';

$offset = isset($argv[1]) ? (int)abs($argv[1]) : 1;

$gzip = function_exists('gzencode');

$file = SITE_PATH . 'tmp/' . uniqid('mindbox_export_clients_') . '.csv' . ($gzip ? '.gz' : '');

$f = fopen($file, 'w');

fputs(
    $f,
    implode(
        ';',
        [
            'RoskosmetikaWebSiteId',
            'Email',
            'MobilePhone',
            'SourceDateTime',
            'SourceActionTemplate',
            'SourcePointOfContact',
            'LastUpdateDateTime',
            'LastUpdateActionTemplate',
            'LastUpdatePointOfContact',
            'IsSubscribedByEmail',
        ]
    ) . "\r\n"
);

DB::get_rows(
    'SELECT c.id, IFNULL(c.email, "") AS email, IFNULL(c.phone, "") AS phone,
        IF(u.created IS NULL OR u.created = "0000-00-00 00:00:00", IF(u.date IS NULL OR u.date = "0000-00-00", c.date_of_change, CONCAT(u.date, " 00:00:00")), u.created) AS create_date,
        IF(u.email IS NULL, "PopalVBazyNotReg", "RegistrationOnSite") AS registration_type,
        "' . env('MINDBOX_IDENTITY') . '" AS source_type,
        IFNULL(c.date_of_change, "' . date('Y-m-d H:i:s') . '") AS change_date,
        "RedaktirovanieVLk" AS change_source,
        "' . env('MINDBOX_IDENTITY') . '" AS change_type,
        IF(u.active = 1, "true", "false") AS active
    FROM clients AS c
    LEFT JOIN rk_unsubscribe AS u ON u.email = c.email
    WHERE c.archive = 0
        AND c.created >= (NOW() - INTERVAL ' . $offset . ' HOUR) OR c.date_of_change >= (NOW() - INTERVAL ' . $offset . ' HOUR)
    ORDER BY c.id',
    function ($order) use (&$f) {
        $order['email'] = mb_convert_encoding($order['email'] , 'UTF-8' , 'UTF-8');
        $order['phone'] = mindbox::prepare_phone(mb_convert_encoding($order['phone'] , 'UTF-8' , 'UTF-8'));

        if ($order['create_date'] === $order['change_date']) {
            $order['change_date'] = date('Y-m-d H:i:s');
        }

        array_walk($order, function (&$value) {
            $value = escape_csv($value);
        });

        fputs(
            $f,
            implode(';', $order) .
            "\r\n"
        );
    }
);

fclose($f);

if ($gzip) {
    file_put_contents($file, gzencode(file_get_contents($file)));
}

$ch = curl_init(
    'https://roskosmetika-services.directcrm.ru/v2/import?' .
    http_build_query(
        [
            'operation' => 'DirectCrm.Customers.Import',
            'externalSystem' => 'RoskosmetikaWebSiteId',
            'brand' => env('MINDBOX_BRAND'),
            'csvCodePage' => 65001,
            'csvColumnDelimiter' => ';',
            'csvTextQualifier' => '"',
        ]
    )
);

curl_setopt_array(
    $ch,
    [
        CURLOPT_POST => true,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_TIMEOUT => 60,
        CURLOPT_FAILONERROR => false,
        CURLOPT_POSTFIELDS => file_get_contents($file),
        CURLOPT_HTTPHEADER => array_merge(
            [
                'Accept: application/xml',
                'Authorization: Basic ' . base64_encode(env('MINDBOX_USER') . ':' . env('MINDBOX_PASSWORD')),
                'Content-Type: text/csv',
                'Content-Length' => filesize($file),
            ],
            $gzip
                ? [
                'Content-Encoding: gzip',
            ]
                : []
        ),
    ]
);

$response = curl_exec($ch);

$info = curl_getinfo($ch);

if (($errno = curl_errno($ch)) || $info['http_code'] >= 300) {
    print 'Curl error: ' . curl_error($ch) . ' (Error number: ' . $errno . "). Request info:\n" . print_r($info, true) . "\n";

    if ($response) {
        print "\nResponse:\n" . $response . "\n";
    }
}

curl_close($ch);

unlink($file);
