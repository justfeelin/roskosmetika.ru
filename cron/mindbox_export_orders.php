<?php

require_once __DIR__ . '/../includes/setup.php';

$offset = isset($argv[1]) ? (int)abs($argv[1]) : 1;

DB::get_rows(
    'SELECT o.order_number
    FROM orders AS o
    INNER JOIN cdb_orders AS co ON co.order_number = o.order_number
    WHERE o.added_date >= (NOW() - INTERVAL ' . $offset . ' HOUR) OR co.date_of_change >= (NOW() - INTERVAL ' . $offset . ' HOUR)
        AND o.status_id != ' . Orders::STATUS_ID_NEW . '
    ORDER BY o.order_number',
    function ($order) {
        $return = mindbox::update_order($order['order_number']);

        if ($return) {
            print 'Order number: ' . $order['order_number'] . "\n" . $return . "\n";
        }
    }
);
