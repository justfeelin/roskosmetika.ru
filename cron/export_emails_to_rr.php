<?php

require __DIR__ . '/../includes/setup.php';

$file = SITE_PATH . 'tmp/export_emails_to_rr.csv';

$f = fopen($file, 'w');

fputs($f, 'Email,IsSubscribed,first_name,last_name,patronymic,client_type,manager,source,region,visitors,monetary,frequency,recently');

$res = DB::query('SELECT u.email, u.first_name, u.last_name, u.patronymic, u.client_type, u.manager, u.source, u.region, u.visitors, REPLACE(CONVERT(u.monetary, CHAR(20)), ".", ",") AS monetary, u.frequency, DATE_FORMAT(u.recently, "%m.%d.%Y") AS recently
              FROM rk_unsubscribe AS u
              WHERE u.status != "spam" AND u.active = 1 AND u.block = 0 AND u.updated = 1');

while ($row = $res->fetch_assoc()) {
    fputs(
        $f,
        "\n" .
        implode(
            ',',
            [
                escape_csv($row['email']),
                '"true"',
                escape_csv($row['first_name']),
                escape_csv($row['last_name']),
                escape_csv($row['patronymic']),
                escape_csv($row['client_type']),
                escape_csv($row['manager']),
                escape_csv($row['source']),
                escape_csv($row['region']),
                escape_csv($row['visitors']),
                escape_csv($row['monetary']),
                escape_csv($row['frequency']),
                escape_csv($row['recently']),
            ]
        )
    );
}

fclose($f);

$res->close();

$response = remote_request(
    'https://api.retailrocket.ru/api/2.0/partner/55cdc1e268e9a65c24215c3d/subscribers?apikey=55cdc1e268e9a65c24215c3e',
    file_get_contents($file),
    true,
    true,
    600
);

$status = !$response;

if ($status) {
    DB::query('UPDATE rk_unsubscribe SET updated = 0 WHERE status != "spam" AND active = 1 AND block = 0');
}

print ($status ? 'Success' : "Failure:\n" . print_r($response, true)) . "\n\n";

exit($status ? 0 : 1);
