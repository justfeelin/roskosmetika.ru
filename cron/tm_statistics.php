<?php

require_once __DIR__ . '/../includes/setup.php';

$offset = isset($argv[1]) ? (int)$argv[1] : 30;
$limit = isset($argv[2]) && $argv[2] > 0 ? (int)$argv[2] : 30;

statistics::cache_tms(date('Y-m-d', $_SERVER['REQUEST_TIME'] - 60 * 60 * 24 * $offset), date('Y-m-d', $_SERVER['REQUEST_TIME'] - 60 * 60 * 24 * ($offset - $limit + 1)));
