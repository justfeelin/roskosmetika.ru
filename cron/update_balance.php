<?php

require_once __DIR__ . '/../includes/setup.php';

$adminEmailTo = [
    env('ADMIN_EMAIL') => env('ADMIN_NAME')
];

$all_products = (((int)date('H') == 5 && (int)date('i') > 30) ? '' : ' WHERE p.active = 1 AND p.visible = 1');

$available = [];

$status = array('sold' => implode(', ', DB::get_rows("SELECT id FROM status WHERE ballance_type = 'sold'", 
                                            function ($row) {
                                                return (int)$row['id'];
                                            })),
                'hold' => implode(', ', DB::get_rows("SELECT id FROM status WHERE ballance_type = 'hold'", 
                                            function ($row) {
                                                return (int)$row['id'];
                                            })),
                'hold_plus' => implode(', ', DB::get_rows("SELECT id FROM status WHERE ballance_type = 'hold_plus'", 
                                            function ($row) {
                                                return (int)$row['id'];
                                            })));

$supplierIDs = DB::get_rows(
    'SELECT s.id FROM cdb_suppliers AS s WHERE s.auto_update = 1',
    function ($row) {
        return (int)$row['id'];
    }
);

DB::get_rows(
    'SELECT p.id, p.main_stock, p.available, cp.stock_id, p.always_available, p.active, p.visible,
        cp.supplier_id
    FROM products AS p
    INNER JOIN cdb_products AS cp ON cp.id = p.id' . $all_products,
    function ($product) use (&$available, $supplierIDs, $status) {

        $arrivals = (int)DB::get_field(
            'arrive',
            'SELECT SUM(ai.quantity) AS arrive
                        FROM products p
                        LEFT JOIN cdb_arrivals_items ai ON ai.product_id = p.id
                        WHERE p.id = ' . $product['id']
        );

        $sold = (int)DB::get_field(
            'sold',
            'SELECT SUM(oi.quantity) AS sold
                            FROM orders_items AS oi, orders o
                            WHERE oi.order_number = o.order_number
                            AND oi.product_id =' . $product['id'] . ' 
                            AND o.status_id IN (' . $status['sold'] . ')'
        );

        $float_box = (int)DB::get_field(
            'float_box',
            'SELECT(IFNULL ((SELECT SUM(cfbi.quantity)
                            FROM cdb_floatbox_oi AS cfbi, orders_items AS oi
                            WHERE cfbi.oi_id = oi.id 
                            AND oi.product_id =' . $product['id'] . '), 0)) AS float_box');

        //  use float box
        $sold = $sold + $float_box;

       DB::query(
                'REPLACE INTO cdb_product_ballance (product_id, arrive, sold, in_stock, hold, hold_plus)
                 VALUES (
                    ' . $product['id'] . ',
                    ' . $arrivals . ',
                    ' . $sold . ',
                    ' . ($arrivals-$sold) . ',
                    IFNULL((SELECT SUM(oi.quantity)
                            FROM orders_items AS oi, orders o
                            WHERE oi.order_number = o.order_number
                            AND oi.product_id =' . $product['id'] . ' 
                            AND o.status_id IN (' . $status['hold'] . ')), 0),
                    IFNULL((SELECT SUM(oi.quantity)
                            FROM orders_items AS oi, orders o
                            WHERE oi.order_number = o.order_number
                            AND oi.product_id =' . $product['id'] . ' 
                            AND o.status_id IN (' . $status['hold_plus'] . ')), 0)
                );'
       );

        $balance =
            (int)DB::get_field(
                'available',
                'SELECT (cp.arrive - cp.sold - cp.hold) AS available
                FROM cdb_product_ballance AS cp
                WHERE cp.product_id = ' . $product['id']
            );

        $updateAvailable = true;

        if (
            $supplierIDs
            && in_array((int)$product['supplier_id'], $supplierIDs, true)
        ) {
            DB::query('UPDATE products SET always_available = 0 WHERE id = ' . $product['id']);

            if ($balance <= 0) {
                $updateAvailable = false;
            }
        } else {
            if ($product['always_available']) {
                $balance = 1;
            }
        }

        if ($updateAvailable) {
            DB::query('UPDATE products SET available = ' . ($balance > 0 ? 1 : 0) . ' WHERE id = ' . $product['id']);
        }

        if (!$product['available'] && $balance > 0 && $product['active'] && $product['visible']) {
            $available[] = (int)$product['id'];
        }

        unset($arrivals, $sold, $balance, $updateAvailable);
    }
);

$sentMails = 0;
$send_sms = 0;

if ($available) {
    $date = date('Y-m-d', $_SERVER['REQUEST_TIME'] - 32 * 24 * 3600);

    DB::get_rows(
        'SELECT DISTINCT po.id, po.prod_id, po.client_id, po.name, po.email, po.phone, po.by_email, po.by_sms, po.date, p.pack, p.price, p.special_price,
              ROUND((p.special_price/p.price) * 100) AS percent,
              IF(p.url = "", IF(p.main_id !=  0, (SELECT IF(pp.url != "", pp.url, pp.id) FROM products AS pp WHERE id = p.main_id), p.id ), p.url) AS url,
              IF(p.main_id = 0, p.name, (SELECT pp.name FROM products AS pp WHERE pp.id = p.main_id)) AS prod_name
              FROM rk_pre_order AS po, products AS p
              WHERE po.prod_id = p.id
              AND   po.prod_id IN (' . implode(', ', $available) . ')
              AND   po.email REGEXP "^[\._a-zA-Z0-9-]+@[\.a-zA-Z0-9-]+\.[a-zA-Z]{2,6}$"
              AND   po.send_mail = 0
              AND   po.send_sms = 0
              ORDER BY date',
        function ($preOrder) use ($adminEmailTo, $date, &$sentMails, &$send_sms) {

            $flag_mail = 0;
            $flag_sms = 0;
            
            if ($preOrder['by_email'])
            {

                if (@mailer::mail(
                    'Товар появился в продаже. Ваша Роскосметика.',
                    [
                        'sales@roskosmetika.ru' => 'Роскосметика',
                    ],
                    DEBUG ? $adminEmailTo : [
                        $preOrder['email'] => $preOrder['email'],
                    ],
                    Template::mail_template(
                        'mail_pre_order_available',
                        '',
                        $preOrder,
                        false
                                           )
                                )
                    ) 
                {

                    ++$sentMails;
                    $flag_mail = 1;
                }
                        
            }

            if ($preOrder['by_sms'])
            {
                $phone = trim(str_replace(array(' ', '(', ')', '+7'), array('', '', '', '8'), $preOrder['phone']));
                
                sms::send_sms($phone, 'Товар с кодом: ' . $preOrder['prod_id'] . ', появился в продаже. Ваша Роскосметика.');
                
                ++$send_sms;
                $flag_sms = 1;
            }



            if ($preOrder['date'] > $date) {
                list($orderNumber) = Orders::insert_order(
                    $preOrder['name'],
                    '',
                    '',
                    $preOrder['email'],
                    $preOrder['phone'],
                    $preOrder['client_id'],
                    [
                        (int)$preOrder['prod_id'],
                    ],
                    '!!! Предзаказ !!! Внимание! Клиенту отправлено уведомление о появлении товара в продаже ' . $preOrder['date'],
                    0,
                    '',
                    null,
                    null,
                    null,
                    null,
                    true
                );
            } else {
                $orderNumber = 0;
            }

            DB::query("UPDATE rk_pre_order SET send_mail = $flag_mail, send_sms = $flag_sms" . ($orderNumber ? ', order_number = ' . $orderNumber : '') . ' WHERE id = ' . (int)$preOrder['id']);
        }
    );
}

if (!DEBUG && ($sentMails || $send_sms || !((int)date('H') % 3))) {
    mailer::mail(
        'Скрипт balance',
        [
            'sales@roskosmetika.ru' => 'Площадка u29842 (клиентская база)',
        ],
        $adminEmailTo,
        '<html><body><div>Скрипт update_balance.php выполнен. Отправлено писем о появившемся товаре: ' . $sentMails . ', СМС:' . $send_sms . '.</div></body></html>'
    );
}
