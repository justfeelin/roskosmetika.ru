<?php

require __DIR__ . '/../includes/setup.php';

$admin_email = [
    env('ADMIN_EMAIL') => env('ADMIN_NAME')
];


/**
 * Day interval for select orders
 * @var integer
 */
$day_interval = 45;
/**
 * Month auantity for calculate statistics 
 * @var integer
 */
$orders_month_perion = 7;
/**
 * Min orders for calculated period
 * @var integer
 */
$max_orders_by_perion = 2;
/**
 * Order statuses for select
 * @var array
 */
$orders_status = '1, 8, 9'; 
/**
 * Cancel orders status
 * @var string
 */
$cancel_orders_status = '2, 5, 10'; 
/**
 * Payments ids for calculate real margin
 * @var array
 */
$non_cash_ids = array('3', '5', '7', '9', '10');
/**
 * Discoun for no cash orders
 * @var integer
 */
$no_cash_discount = 3;
/**
 * Min calculated cash for promo code
 * @var integer
 */
$min_code_cash = 200;
/**
 * Min last order cost
 * @var integer
 */
$min_order_cost = 3500;
/**
 * Discounts cell by margin
 * @var array
 */
$margin_cell = array('margin'    => array(0 => 40, 1 => 30, 2 => 20),
                     'discounts' => array(0 => 20, 1 => 16, 2 => 13));
/**
 * Days for use promo-code
 * @var integer
 */
$days_for_use = 20;
/**
 * Sending emails for report
 * @var integer
 */
$sending_emails = 0;
/**
 * Time limit in seconds for runing script
 * @var integer
 */
$script_time_limit = 300;
/**
 * Min email for sending one time
 * @var integer
 */
$one_time_send_min = 50;
/**
 * Min margin in percent for organizations
 * @var integer
 */
$organization_min_margin = 30;
/**
 * Max client discount in roubles
 * @var integer
 */
$max_discount = 900;
/**
 * Return data for senpulse
 * @var bool | array
 */
$return_data = FALSE;

/**
 * Check orders in day interval
 * @param  int    $cliet_id             ID client
 * @param  int    $shipping_person_id   ID shipping person if no ID client
 * @param  int    $day_interval         param $day_interval
 * @param  string $orders_status        param $orders_status
 * @param  string orders_month_perio    param $orders_month_perio
 * @param  string $max_orders_by_perion param $max_orders_by_perion
 * @param  string $cancel_orders_status param cancel orders status
 * @return bool                         was orders or not
 */
function check_new_orders($cliet_id, $shipping_person_id, $day_interval, $orders_status, $orders_month_perion, $max_orders_by_perion, $cancel_orders_status) 
{
    $query = "SELECT COUNT(o.order_number) AS orders  
              FROM orders o, cdb_orders co 
              WHERE o.order_number = co.order_number 
              AND (o.client_id = " . $cliet_id . " OR o.shipping_person_id = " . $shipping_person_id . ")  
              AND o.status_id ";

    $result = (int)DB::get_field('orders', $query . " NOT IN (" . $cancel_orders_status . ") AND o.added_date > (CURRENT_DATE() - INTERVAL " . $day_interval . " day)");

    $result = ($result > 0 ? FALSE : TRUE);

    if ($result) 
    {
        $result = (int)DB::get_field('orders', $query . "IN (" . $orders_status . ") AND IF(co.shift_delivery_date IS NOT NULL, co.shift_delivery_date, co.delivery_date) BETWEEN (NOW() - INTERVAL " . $orders_month_perion . " MONTH) AND NOW()");
        $result =  ($result <= $max_orders_by_perion ? TRUE : FALSE);        
    }

    return $result;            
}

/**
 * Set mail flag to 1
 *
 * @param int $order_id Order ID
 *
 * @return bool result
 */
function send_plus($order_id)
{
    $order_id = (int)$order_id;

    $query = "UPDATE cdb_orders SET lc_return_mail = 1 WHERE order_number = $order_id";

    return DB::query($query);
}
/**
 * Comment in client card
 * @param  int    $client_id     orders client ID
 * @param  string $promo_code    generated promo code
 * @param  int    $discount_cash promo code cash
 * @return bool result
 */
function client_comment($client_id, $promo_code, $discount_cash) 
{
    $client_id = (int)$client_id;

    $query = "UPDATE clients c SET c.company_note = CONCAT( c.company_note, ' * " . date("Y-m-d") . ' клиенту отправлен персональный промо-код: ' . $promo_code . ', на скидку -' . $discount_cash . " руб.') WHERE c.id = $client_id";

    return DB::query($query);
}

/**
 * Generate password
 *
 * @param int $size Size of password
 *
 * @return string Password string
 */
function make_code($size = 6)
{
    //   generate password
    $arr = array('a', 'b', 'c', 'd', 'e', 'f',
        'g', 'h', 'i', 'j', 'k', 'l',
        'm', 'n', 'o', 'p', 'r', 's',
        't', 'u', 'v', 'x', 'y', 'z',
        'A', 'B', 'C', 'D', 'E', 'F',
        'G', 'H', 'I', 'J', 'K', 'L',
        'M', 'N', 'O', 'P', 'R', 'S',
        'T', 'U', 'V', 'X', 'Y', 'Z',
        '1', '2', '3', '4', '5', '6',
        '7', '8', '9', '0');

    $code = '';

    for ($i = 0; $i < $size; $i++) {
        // random array index
        $step_number = rand(0, count($arr) - 1);
        $code .= $arr[$step_number];
    }

    return $code;
}

/**
 * Returns valid email from multiple (If available)
 *
 * @param string $email_1 First email
 * @param string $email_2 Second email
 *
 * @return string|null
 */
function check_email($email_1, $email_2)
{
    return $email_1 && check::email($email_1) ? $email_1 : ($email_2 && check::email($email_2) ? $email_2 : FALSE);
}

/**
 * Generate promo-code and write in base with parameters
 * @param  int    $discount       rub discount < 0
 * @param  string $prefix         prefix for promo-code
 * @param  int    $days_for_use   param days for use
 * @param  int    $min_order_cost min order cost
 * @param  string $comment        comment for order
 * @return array                  new promo-code info
 */
function generate_promo_code($discount, $prefix, $days_for_use, $min_order_cost, $comment)
{
    do {
        $code = $prefix . make_code();
        $return = DB::get_row("SELECT p.id FROM rk_promo_codes AS p WHERE p.code = '$code'");
    } while ($return);

    $query = "INSERT INTO rk_promo_codes( code,
                                       type,
                                       create_date,
                                       end_date,
                                       min_sum,
                                       discount,
                                       discount_type,
                                       comment, 
                                       `use`)
                     VALUES ('$code',
                             'uniq',
                             NOW(),
                             (NOW() + INTERVAL " . $days_for_use . " DAY), 
                             $min_order_cost,
                             $discount,
                             1, '" .
                             $comment . ' ' . date("Y-m-d")  . "',
                             0)";

    DB::query($query);

    return DB::get_row('SELECT * FROM rk_promo_codes p WHERE p.id = ' . DB::get_last_id());
}


// start script
$time_start = $time_end = microtime(true);
$time = 0;

Template::init('templates');

Template::$utm_source = 'lc-return';
Template::$utm_medium = 'email';
Template::$utm_campaign = 'period-no-order';


//  get orders
$query = "SELECT o.order_number, c.id AS client_id, c.phone, c.type_id, o.shipping_person_id, o.cost, 
                (SELECT SUM(p.purchase_price * oi.quantity) FROM orders_items oi, products p WHERE oi.order_number = o.order_number AND oi.product_id = p.id) AS purchase_cost,
                 IFNULL(o.delivery, 0) AS delivery_cost, 
                 IFNULL(co.old_delivery, 0) - IFNULL(o.delivery, 0) AS delivery_compensation, co.delivery_date, co.shift_delivery_date, 
                 IF((c.email IS NOT NULL OR c.email <> ''), c.email, (SELECT sp.email FROM shipping_persons sp WHERE sp.id = o.shipping_person_id AND sp.email IS NOT NULL AND sp.email <> '' LIMIT 1)) AS c_email, 
                 IF((o.email IS NOT NULL OR o.email <> ''), o.email, (SELECT sp.email FROM shipping_persons sp WHERE sp.id = o.shipping_person_id AND sp.email IS NOT NULL AND sp.email <> '' LIMIT 1)) AS o_email, 
                 CONCAT(IFNULL(c.name, ''), ' ', IFNULL(c.patronymic, '')) AS name, o.payment_id
          FROM  cdb_orders co, orders AS o
          LEFT JOIN clients AS c ON o.client_id = c.id
          WHERE o.order_number = co.order_number 
          AND o.auto_order = 0
          AND co.lc_return_mail = 0
          AND (c.email IS NOT NULL OR o.email IS NOT NULL OR (SELECT COUNT(sp.id) FROM shipping_persons sp WHERE sp.id = o.shipping_person_id AND sp.email IS NOT NULL AND sp.email <> '') > 0)
          AND (c.email <> '' OR o.email <> '' OR (SELECT COUNT(sp.id) FROM shipping_persons sp WHERE sp.id = o.shipping_person_id AND sp.email IS NOT NULL AND sp.email <> '') > 0)
          AND o.status_id IN (" . $orders_status . ")
          AND IF(co.shift_delivery_date IS NOT NULL, co.shift_delivery_date, co.delivery_date) = (CURRENT_DATE() - INTERVAL " . $day_interval . " day)";

$orders = DB::get_rows($query);

for ($i=0; $i < count($orders); $i++)
{
  $client_email = check_email($orders[$i]['c_email'], $orders[$i]['o_email']);

  if (check_new_orders($orders[$i]['client_id'], $orders[$i]['shipping_person_id'], $day_interval, $orders_status, $orders_month_perion, $max_orders_by_perion, $cancel_orders_status) &&  $client_email && $orders[$i]['cost'] > 0)
  {  
      $margin_rub = floor($orders[$i]['cost'] - $orders[$i]['delivery_cost'] - $orders[$i]['purchase_cost'] - (in_array($orders[$i]['payment_id'], $non_cash_ids) ? $orders[$i]['cost'] * ($no_cash_discount/100) : 0) - $orders[$i]['delivery_compensation']);
      $margin_percent = floor(($margin_rub/($orders[$i]['cost'] - $orders[$i]['delivery_cost'])) * 100);

      $discount = 0;

      foreach ($margin_cell['margin'] as $key => $value) 
      {
          if ($margin_percent >= $value) 
          {
              if ($orders[$i]['type_id'] != 3 && $margin_percent < $organization_min_margin)
              {
                continue;
              } 
              $discount = $margin_cell['discounts'][$key];
              break;

          }
      }

      $discount_cash = floor($margin_rub * ($discount/100)); 
      $discount_cash = $discount_cash > $max_discount ? $max_discount : $discount_cash;


      if ($discount_cash >= $min_code_cash) {

            $c_name = trim($orders[$i]['name']);

            $promo_code = generate_promo_code(-$discount_cash, 'RC', $days_for_use, $min_order_cost, 'Реактивация. Персональный промо-код для клиента ID ' . $orders[$i]['client_id'] . ($c_name ? ' ' . $c_name : ''));

            $return_data['email'] = $client_email;
            $return_data['phone'] = $orders[$i]['phone'];
            $return_data['action_date'] = date("Y-m-d");
            $return_data['client_name'] = $c_name;
            $return_data['promo_code'] = $promo_code['code'];
            $return_data['promo_code_discount'] = $discount_cash;
            $return_data['pc_days_for_use'] = $days_for_use;
            $return_data['pc_min_price'] = $min_order_cost;

            $last_products = array_column(orders::get_order_products($orders[$i]['order_number']), 'id');

            $recomended = Product::cart_neighbours($last_products, false, 6);

              for ($j=0; $j < count($recomended); $j++) { 

                $return_data['recomended'][$j]['id'] = $recomended[$j]['id'];
                $return_data['recomended'][$j]['name'] = helpers::str_prepare($recomended[$j]['name'] . ' ' . $recomended[$j]['packs'][0]['pack'] . ', '. $recomended[$j]['tm']);
                $return_data['recomended'][$j]['price'] = $recomended[$j]['packs'][0]['special_price'] > 0 ?  (int)$recomended[$j]['packs'][0]['special_price'] : (int)$recomended[$j]['packs'][0]['price'];
                $return_data['recomended'][$j]['old_price'] = $recomended[$j]['packs'][0]['special_price'] > 0 ?  (int)$recomended[$j]['packs'][0]['price'] : 0;
                $return_data['recomended'][$j]['url'] = $recomended[$j]['url'];
                $return_data['recomended'][$j]['img'] = $recomended[$j]['packs'][0]['pic_quant'] > 0 ?  1 : 0;
                
              }  


            $result = sendpulse::reactivation_1($return_data);

            if ($result['result']) {

                send_plus($orders[$i]['order_number']);
                client_comment($orders[$i]['client_id'], $promo_code['code'], $discount_cash);
                ++$sending_emails;

                if (count($orders) > $one_time_send_min) sleep(2);

            }
 
            $time_end = microtime(true);
            $time = $time_end - $time_start;

            if ($time > $script_time_limit) break;
      }
  }

}

if (count($orders) > 0) 
{
  if (!DEBUG) {
      mailer::mail(
          'Скрипт реактивация клиентов',
          [
              'sales@roskosmetika.ru' => 'Площадка u29842 (клиентская база)',
          ],
          $admin_email,
          '<html><body><div>Скрипт return_client_by_promo_sale.php выполнен. Отправлено писем: ' . $sending_emails . '.</div></body></html>'
      );
  }
}