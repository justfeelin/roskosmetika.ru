<?php

require __DIR__ . '/../includes/setup.php';

/*
 * Рассылка списка записавшихся на мк
 */
$query = 'SELECT DISTINCT
    mcs.surname,
    mcs.name,
    mcs.phone,
    mcs.patronymic,
    mcs.price,
    mcs.source,
    mc.name AS mc_name,
    mc.date
    FROM rk_master_classes AS mc, rk_master_class_subscribes AS mcs
    WHERE ' . (DEBUG ? '' : 'DATE_FORMAT(mc.date, "%Y%m%d") = (CURRENT_DATE() + INTERVAL 1 day) AND ') .
    'mc.visibility = 1
    AND mcs.master_class_id = mc.id
    ORDER BY mcs.surname' .
    (DEBUG ? ' LIMIT 20' : '');

$subscribes_list = DB::get_rows($query);

if ($subscribes_list) {
    $filepath = realpath(__DIR__ . '/../tmp') . '/spisok-zapisavshihsya-na-master-class.xls';

    $excel = export::create_new_file();

    $sheet = $excel->getSheet(0);

    $sheet->getColumnDimension('A')->setWidth(4);
    $sheet->getColumnDimension('B')->setWidth(50);
    $sheet->getColumnDimension('C')->setWidth(20);
    $sheet->getColumnDimension('D')->setWidth(6);
    $sheet->getColumnDimension('E')->setWidth(20);

    $sheet
        ->getStyle("A1:E1")
        ->getFont()
        ->setBold(true);

    $sheet->mergeCells('A1:E1');

    $sheet
        ->getDefaultRowDimension()
        ->setRowHeight(-1);

    $date_name = '\'' . $subscribes_list[0]['mc_name'] . '\' Дата: '. $subscribes_list[0]['date'];

    $sheet->setCellValue('A1', $date_name);
    $sheet->setCellValue('A2', '№');
    $sheet->setCellValue('B2', 'ФИО');
    $sheet->setCellValue('C2', 'Телефон');
    $sheet->setCellValue('D2', 'Цена');
    $sheet->setCellValue('E2', 'Сайт');

    for ($i = 0, $n = count($subscribes_list); $i < $n; ++$i) {
        $sheet->setCellValue('A' . ($i + 3), $i + 1);
        $sheet->setCellValue('B' . ($i + 3), $subscribes_list[$i]['surname'] . ' ' . $subscribes_list[$i]['name'] . ' ' . $subscribes_list[$i]['patronymic']);
        $sheet->setCellValue('C' . ($i + 3), $subscribes_list[$i]['phone']);
        $sheet->setCellValue('D' . ($i + 3), $subscribes_list[$i]['price']);
        $sheet->setCellValue('E' . ($i + 3), $subscribes_list[$i]['source']);
    }

    $sheet->getStyle('A2:A' . ($n + 2))->applyFromArray([
        'alignment' => [
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        ]
    ]);

    $sheet
        ->getStyle('C3:C' . ($n + 2))
        ->applyFromArray([
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            ]
        ])
        ->getNumberFormat()
        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

    $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
    $writer->save($filepath);

    mailer::attachment($filepath, 'application/vnd.ms-excel');

    mailer::mail(
        'Список записавшихся на мастер-класс "' . $subscribes_list[0]['mc_name'] . '"',
        [
            'news@r-cosmetics.ru' => 'R-cosmetics',
        ],
        DEBUG ? [
            env('ADMIN_EMAIL'),
        ] : [
            'lukashinsa@gmail.com',
            'yegor007@mail.ru',
            '2255483@mail.ru',
            'ludmila@r-cosmetics.ru',
            'setlana79579@yandex.ru'
        ],
        ''
    );

    unlink($filepath);
}

echo "mc_subscribes active\n";
