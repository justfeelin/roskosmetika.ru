<?php

require __DIR__ . '/../includes/setup.php';

$orders = [];

$hourDate = date('Y-m-d H:', $_SERVER['REQUEST_TIME'] - 3600 * 2);

DB::get_rows(
    'SELECT o.id, o.order_number, o.start_date AS date, c.client_id, c.name, c.phone, c.email
        FROM rk_orders AS o
        INNER JOIN rk_credentials AS c ON c.order_id = o.id AND c.phone != ""
        WHERE o.start_date >= "' . $hourDate . '00:00" AND o.start_date <= "' . $hourDate . ':59:59"',
    function ($order) use (&$orders, $hourDate) {
        $products = Orders::get_order_products($order['order_number']);

        if (
            $products
            && !DB::get_field('cnt', 'SELECT COUNT(o.order_number) FROM orders AS o WHERE o.phone = "' . DB::mysql_secure_string($order['phone']) . '" AND o.added_date >= "' . $hourDate . '00:00" AND o.added_date <= "' . $hourDate . ':59:59" LIMIT 1')
        ) {
            $orders[] = $order + [
                'products' => $products,
            ];
        }
    }
);

if (!$orders) {
    exit(0);
}

$excel = export::create_new_file();

$sheet = $excel->getSheet(0);

$sheet->getColumnDimension('A')->setWidth(20);
$sheet->getColumnDimension('B')->setWidth(40);
$sheet->getColumnDimension('C')->setWidth(20);
$sheet->getColumnDimension('D')->setWidth(20);
$sheet->getColumnDimension('E')->setWidth(40);
$sheet->getColumnDimension('F')->setWidth(10);
$sheet->getColumnDimension('G')->setWidth(70);
$sheet->getColumnDimension('H')->setWidth(15);

$sheet->setCellValue('A1', 'Internet ID клиента');
$sheet->setCellValue('B1', 'ФИО');
$sheet->setCellValue('C1', 'Дата');
$sheet->setCellValue('D1', 'Телефон');
$sheet->setCellValue('E1', 'E-mail');
$sheet->setCellValue('F1', 'ID товара');
$sheet->setCellValue('G1', 'Название товара');
$sheet->setCellValue('H1', 'Цена товара');

$sheet->getStyle('A1:H1')->getFont()->setBold(true);

for ($i = 0, $line = 2, $n = count($orders); $i < $n; ++$i, ++$line) {
    $sheet->setCellValue('A' . $line, $orders[$i]['client_id']);
    $sheet->setCellValue('B' . $line, $orders[$i]['name']);
    $sheet->setCellValue('C' . $line, $orders[$i]['date']);
    $sheet->setCellValue('D' . $line, $orders[$i]['phone']);
    $sheet->setCellValue('E' . $line, $orders[$i]['email']);

    for ($y = 0, $m = count($orders[$i]['products']); $y < $m; ++$y, ++$line) {
        $sheet->setCellValue('F' . $line, $orders[$i]['products'][$y]['id']);
        $sheet->setCellValue('G' . $line, html_entity_decode($orders[$i]['products'][$y]['name'], ENT_QUOTES, 'UTF-8'));
        $sheet->setCellValue('H' . $line, $orders[$i]['products'][$y]['old_price']);
    }
}

$filepath = SITE_PATH . '/tmp/lost-carts-phones.xls';

$writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
$writer->save($filepath);

mailer::attachment($filepath, 'application/vnd.ms-excel');

$result = mailer::mail(
    'Брошенные корзины  ' . $hourDate . '00 - ' . date('Y-m-d H:00', $_SERVER['REQUEST_TIME'] - 3600),
    [
        'info@roskosmetika.ru' => DEBUG ? env('ADMIN_NAME') : 'Роскосметика',
    ],
    [
        DEBUG ? env('ADMIN_EMAIL') : '2255483@mail.ru' => DEBUG ? env('ADMIN_NAME') : 'Роскосметика',
    ],
    ''
);

unlink($filepath);

exit($result ? 0 : 1);
