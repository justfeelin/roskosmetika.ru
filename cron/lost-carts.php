<?php

require __DIR__ . '/../includes/setup.php';

/**
 * Set mail flag to 1
 *
 * @param int $order_id Order ID
 * @param string $field Flag field name
 *
 * @return bool result
 */
function send_plus($order_id, $field)
{
    $order_id = (int)$order_id;

    $query = "UPDATE rk_orders SET $field = 1 WHERE id = $order_id";

    return !!DB::query($query);
}

/**
 * Get products from card
 *
 * @param int $order_id Order ID
 *
 * @return array Array of products in order
 */
function get_products($order_id)
{
    $order_id = (int)$order_id;

    $query = "SELECT p.id, IF( p.url =  '', IF( p.main_id !=  0, (SELECT pp.url FROM products pp WHERE id = p.main_id), p.id ), p.url ) url, IF( p.name =  '', (SELECT ppp.name FROM products ppp WHERE ppp.id = p.main_id), p.name ) AS name, c.price, c.quantity
                  FROM rk_orders_items AS c, products AS p
                  WHERE c.rk_order_id =  $order_id
                  AND   c.product_id  = p.id
                  AND p.visible = 1";

    return DB::get_rows($query);
}

/**
 * Generate password
 *
 * @param int $size Size of password
 *
 * @return string Password string
 */
function make_code($size = 6)
{
    //   generate password
    $arr = array('a', 'b', 'c', 'd', 'e', 'f',
        'g', 'h', 'i', 'j', 'k', 'l',
        'm', 'n', 'o', 'p', 'r', 's',
        't', 'u', 'v', 'x', 'y', 'z',
        'A', 'B', 'C', 'D', 'E', 'F',
        'G', 'H', 'I', 'J', 'K', 'L',
        'M', 'N', 'O', 'P', 'R', 'S',
        'T', 'U', 'V', 'X', 'Y', 'Z',
        '1', '2', '3', '4', '5', '6',
        '7', '8', '9', '0');

    $code = '';

    for ($i = 0; $i < $size; $i++) {
        // random array index
        $step_number = rand(0, count($arr) - 1);
        $code .= $arr[$step_number];
    }

    return $code;
}

/**
 * Returns valid email from multiple (If available)
 *
 * @param string $email1 First email
 * @param string $email2 Second email
 *
 * @return string|null
 */
function getEmail($email1, $email2)
{
    return $email1 && check::email($email1) ? $email1 : ($email2 && check::email($email2) ? $email2 : null);
}

/**
 * Generate and check promo codes
 *
 * @param  int $discount Value of discount
 *
 * @return array Code row
 */
function generate_promo_code($discount)
{
    do {
        $code = make_code();
        $return = DB::get_row("SELECT p.id FROM rk_promo_codes AS p WHERE p.code = '$code'");
    } while ($return);

    $query = "INSERT INTO rk_promo_codes( code,
                                       type,
                                       create_date,
                                       discount,
                                       `use`)
                     VALUES ('$code',
                             'uniq',
                             NOW(),
                             $discount,
                             0)";

    DB::query($query);

    return DB::get_row('SELECT * FROM rk_promo_codes p WHERE p.id = ' . DB::get_last_id());
}

/**
 * Send message about lost cart to user
 *
 * @param string $mail Users email
 * @param string $sub Theme of mail
 * @param string $msg_text info text
 * @param array $order Order row
 * @param array $promo_code Promo code row
 *
 * @return bool result of mail
 */
function msg_roskosmetika($mail, $sub, $msg_text, $order, $promo_code = null)
{
    $to = DEBUG ? env('ADMIN_EMAIL') : $mail;
    $from = [
        'sales@roskosmetika.ru' => 'Роскосметика',
    ];

    Cart::orderID($order['order_id']);

    $region = $order['region_id'] ? Orders::get_regions($order['region_id']) : null;
    $shippingTypeCode = $order['client_shipping_type'] ? (int)$order['client_shipping_type'] : null;

    if ($region && $shippingTypeCode !== null) {
        Cart::setShipping($region, $shippingTypeCode);
    }

    $order = Cart::setInfo(false);

    for ($i = 0, $n = count($order['products']), $productIDs = []; $i < $n; ++$i) {
        $productIDs[] = (int)$order['products'][$i]['id'];
    }

    Template::$utm_term = $to;

    $mailContent = Template::mail_template(
        'mail_lost_cart',
        $sub,
        [
            'text' => $msg_text,
            'products' => $order['products'],
            'promo_code' => $promo_code,
            'neighbours_html' => Template::mail_neighbours(Product::cart_neighbours($productIDs, false, 4)),
        ] + $order['info']
    );

    try {
        $sent = @mailer::mail(
            $sub,
            $from,
            $to,
            $mailContent
        );
    } catch (Exception $e) {
        $sent = false;
    }

    return $sent;
}

Template::init((!DEBUG ? COMPRESSED_TEMPLATES . '/' : '') . 'templates');

Template::$utm_source = 'site';
Template::$utm_medium = 'newsletter';

Template::$utm_campaign = 'lost_cart_30_minutes';

//  lost carts fo 30 minutes
$query = "SELECT o.id AS order_id, o.client_id, o.region_id, o.shipping_id AS client_shipping_type,
          IF (u.name = '' OR u.name IS NULL, 'Уважаемый покупатель', u.name) AS name, u.email,
          c.email AS c_email
          FROM rk_orders AS o
          LEFT JOIN clients AS u ON u.id = o.client_id
          LEFT JOIN rk_credentials AS c ON c.order_id = o.id
          WHERE (SELECT COUNT(c.id) FROM rk_orders_items AS c WHERE c.rk_order_id = o.id) > 0
          AND   o.id IN (SELECT oi.rk_order_id FROM rk_orders_items AS oi WHERE oi.created BETWEEN (NOW() - INTERVAL 35 MINUTE) AND (NOW() - INTERVAL 5 MINUTE))
          AND   o.lost_cart_1 = 0
          AND   o.order_number = 0
          ORDER BY o.start_date";

DB::get_rows(
    $query,
    function ($order) {
        $email = getEmail($order['email'], $order['c_email']);

        if ($email) {
            $msg_text = "<h1 style=\"font-family: Tahoma, Geneva, sans-serif;font-size: 14px;color: #1B1B1B;\">Здравствуйте, {$order['name']}!</h1>
                      <p style=\"font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 4px;\">
                        Мы обратили внимание, что Вы начали оформлять заказ, но не завершили его.
                      </p>
                      <p style=\"font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 4px;margin-bottom: 4px;\">
                        Если Вам нужна помощь, просто свяжитесь с нашими специалистами по E-mail: <b>sales@roskosmetika.ru</b> или по бесплатному телефону: 8&nbsp;800&nbsp;775-54-83.
                      </p>
                      <p style=\"font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 4px;margin-bottom: 4px;\">
                        Наши сотрудники с удовольствием ответят на Ваши вопросы и помогут оформить заказ!
                      </p>
                      <p><a style=\"color: #9b1d97; font-size: 14px;\" target=\"_blank\" href=\"" . DOMAIN_FULL . "/cart" . apply_utm(null, 'back-to-cart') . "\">Вернуться в корзину сейчас</a></p>
                      <p style=\"font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 6px;\">
                        Список Ваших товаров в корзине:
                      </p>";

            if (msg_roskosmetika($email, 'Надеемся Ваш заказ еще актуален, не забудьте его оформить!', $msg_text, $order)) {
                send_plus($order['order_id'], 'lost_cart_1');
            }
        }
    }
);


Template::$utm_campaign = 'lost_cart_1_day';

//  lost carts fo 24 hours
$query = "SELECT o.id AS order_id, o.client_id, o.region_id, o.shipping_id AS client_shipping_type,
          u.id, IF (u.name = '' OR u.name IS NULL, 'Уважаемый покупатель', u.name) AS name, u.email,
          c.email AS c_email
          FROM rk_orders AS o
          LEFT JOIN clients AS u ON u.id = o.client_id
          LEFT JOIN rk_credentials AS c ON c.order_id = o.id
          WHERE (SELECT COUNT(c.id) FROM rk_orders_items c WHERE c.rk_order_id = o.id) > 0
          AND   o.start_date BETWEEN (NOW() - INTERVAL 2 DAY) AND (NOW() - INTERVAL 1 DAY)
          AND   o.lost_cart_1 != 0
          AND   o.lost_cart_2 = 0
          AND   o.order_number = 0
          ORDER BY o.start_date
          LIMIT 50";

DB::get_rows(
    $query,
    function ($order) {
        $email = getEmail($order['email'], $order['c_email']);

        if ($email) {
            $msg_text = "<h1 style=\"font-family: Tahoma, Geneva, sans-serif;font-size: 14px;color: #1B1B1B;\">Здравствуйте, {$order['name']}!</h1>
                      <p style=\"font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 4px;\">
                        Мы зарезервировали на складе товары Вашего заказа на несколько дней, чтобы у Вас было время подумать и проконсультироваться с
                        нашими специалистами по E-mail: <b>sales@roskosmetika.ru</b> или по бесплатному телефону: 8&nbsp;800&nbsp;775-54-83.
                      </p>
                      <p style=\"font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 4px;margin-bottom: 4px;\">
                        Наши сотрудники с удовольствием ответят на Ваши вопросы и помогут оформить заказ!
                      </p>
                      <p><a style=\"color: #9b1d97; font-size: 14px;\" target=\"_blank\" href=\"" . DOMAIN_FULL . "/cart" . apply_utm(null, 'back-to-cart') . "\">Продолжить оформление заказа</a></p>
                      <p style=\"font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 6px;\">
                        Список зарезервированных товаров в Вашей корзине:
                      </p>";

            if (msg_roskosmetika($email, 'Мы сохранили для вас товары, если Вы захотите оформить свой заказ!', $msg_text, $order)) {
                send_plus($order['order_id'], 'lost_cart_2');
            }
        }
    }
);

Template::$utm_campaign = 'lost_cart_5_days';

//  lost carts fo 5 days
$query = "SELECT o.id order_id, o.client_id, o.region_id, o.shipping_id AS client_shipping_type,
          IF (u.name = '' OR u.name IS NULL, 'Уважаемый покупатель', u.name) AS name, u.email,
          c.email AS c_email
          FROM rk_orders AS o
          LEFT JOIN clients AS u ON u.id = o.client_id
          LEFT JOIN rk_credentials AS c ON c.order_id = o.id
          WHERE (SELECT COUNT(c.id) FROM rk_orders_items c WHERE c.rk_order_id = o.id) > 0
          AND   o.start_date BETWEEN (NOW() - INTERVAL 6 DAY) AND (NOW() - INTERVAL 5 DAY)
          AND   o.lost_cart_2 != 0
          AND   o.lost_cart_3 = 0
          AND   o.order_number = 0
          ORDER BY o.start_date
          LIMIT 50";

DB::get_rows(
    $query,
    function ($order) {
        $email = getEmail($order['email'], $order['c_email']);

        if ($email) {
            $promo_code = generate_promo_code(3);

            $msg_text = "<h1 style=\"font-family: Tahoma, Geneva, sans-serif;font-size: 14px;color: #1B1B1B;\">Здравствуйте, {$order['name']}!</h1>
                      <p style=\"font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 4px;\">
                        Вы еще не решились оформить заказ? Специальный подарок от Роскосметики - промо код на скидку <b>3%</b> к Вашему заказу!
                      </p>
                      <p style=\"font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 4px;margin-bottom: 4px;\">
                        При оформлении заказа в <a style=\"color: #9b1d97;\" target=\"_blank\" href=\"" . DOMAIN_FULL . "/cart" . apply_utm(null, 'back-to-cart') . "\">корзине</a> введите в поле \"Промо код\"  Ваш подарочный промо&nbsp;код:&nbsp;<span style=\"font-size: 14px; font-weight:bold;\">\"" . $promo_code['code'] . "\"</span> и нажмите ОК.
                      </p>
                      <p><a style=\"color: #9b1d97; font-size: 14px;\" target=\"_blank\" href=\"" . DOMAIN_FULL . "/cart" . apply_utm(null, 'back-to-cart') . "\">Перейти в корзину сейчас</a></p>
                      <p style=\"font-family: Tahoma, Geneva, sans-serif;font-size: 13px;color: #1B1B1B;margin-top: 6px;margin-bottom: 6px;\">
                        Список товаров в Вашей корзине:
                      </p>";

            if (msg_roskosmetika($email, 'Дополнительная скидка 3% к Вашему заказу!', $msg_text, $order, $promo_code)) {
                send_plus($order['order_id'], 'lost_cart_3');
            }
        }
    }
);

$query = "SELECT o.id AS order_id, o.client_id, o.region_id, o.shipping_id AS client_shipping_type,
          IF (u.name = '' OR u.name IS NULL, 'Уважаемый покупатель', u.name) AS name, u.email,
          c.email AS c_email, c.phone
          FROM rk_orders AS o
          LEFT JOIN rk_credentials AS c ON c.order_id = o.id AND (c.email != '' OR c.phone != '')
          LEFT JOIN clients AS u ON u.id = o.client_id
          WHERE (SELECT COUNT(c.id) FROM rk_orders_items AS c WHERE c.rk_order_id = o.id) > 0
          AND   o.id IN (SELECT oi.rk_order_id FROM rk_orders_items AS oi WHERE oi.created BETWEEN (NOW() - INTERVAL 120 MINUTE) AND (NOW() - INTERVAL 85 MINUTE))
          AND   o.id NOT IN (SELECT oi.rk_order_id FROM rk_orders_items AS oi WHERE oi.created > (NOW() - INTERVAL 85 MINUTE))
          AND   o.lost_cart_order_number = 0
          AND   o.order_number = 0
          ORDER BY o.start_date";

DB::get_rows(
    $query,
    function ($order) {
        $email = $order['email'] && check::email($order['email']) ? $order['email'] : ($order['c_email'] && check::email($order['c_email']) ? $order['c_email'] : null);

        if ($email || $order['phone']) {
            Cart::orderID($order['order_id']);

            Cart::setInfo();

            list($orderNumber) = Orders::new_order(
                $order['name'],
                '',
                '',
                '',
                $order['phone'],
                $email,
                0,
                Orders::CUSTOM_SHIPPING,
                '',
                '!!! БРОШЕННАЯ КОРЗИНА !!!',
                Cart::$data,
                $order['client_id'],
                'CRM обработано. Заказ из брошенной корзины на Роскосметика ',
                false,
                true
            );

            DB::query('UPDATE rk_orders SET lost_cart_order_number = ' . $orderNumber . ' WHERE id = ' . $order['order_id']);
        }
    }
);
