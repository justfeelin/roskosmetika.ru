<?php

require_once __DIR__ . '/../includes/setup.php';

$token = vk::get_admin_token();

if ($token) {
    $groupID = env('VK_GROUP_ID');

    vk::export_new_products($token['token'], $groupID, 200);
    vk::update_products($token['token'], $groupID, 200);
    vk::categorize_products($token['token'], $groupID, 200);
} else {
    if (!caching::get(vk::ADMIN_TOKEN_MESSAGE_CACHE_KEY)) {
        mailer::mail(
            'Необходимо обновить token для администратора на vk.com',
            [
                'sales@roskosmetika.ru' => 'CRON roskosmetika.ru'
            ],
            [
                env('ADMIN_EMAIL') => env('ADMIN_NAME'),
            ],
            'Токена нет, либо он скоро истекает.
            <br>
            <br>
            Для обновления необходимо авторизоваться как администратор группы на vk.com <a href="' . DOMAIN_FULL . '/admin/vk/get_token" target="_blank">здесь</a>.'
        );

        caching::set(vk::ADMIN_TOKEN_MESSAGE_CACHE_KEY, true);
    }
}
