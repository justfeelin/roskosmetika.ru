module.exports = function (grunt) {
    var iniContent, debug, config,
        fs, fse, path,
        defaultTasks, jsTasks, cssTasks,
        landingI;

    require('load-grunt-tasks')(grunt);

    try {
        iniContent = grunt.file.read(__dirname + "/env.ini");
    } catch (error) {
        iniContent = null;
    }

    debug = !grunt.option('production') && (!iniContent || !/DEBUG ?=(?: ?0?|)\n/.test(iniContent));

    fs = require('fs');
    fse = require('fs-extra');
    path = require('path');

    grunt.registerMultiTask('compressTemplates', function () {
        var directories,
            commonCommand,
            exec, done,
            files,
            compressedDir,
            basename,
            filesCount, i, y, processed;

        if (!this.data.dir) {
            console.error("No 'dir' parameter, skipping");

            return;
        }

        if (!this.data.output_dir) {
            console.error("No 'output_dir' parameter, skipping");

            return;
        }

        directories = typeof this.data.dir === 'string' ? [this.data.dir] : this.data.dir;

        done = this.async();

        filesCount = 0;

        files = [];

        compressedDir = __dirname + '/' + this.data.output_dir;

        if (fs.existsSync(compressedDir)) {
            fse
                .removeSync(compressedDir);
        }

        fs
            .mkdirSync(compressedDir);

        fs
            .writeFileSync(compressedDir + '/empty', '');

        for (i = 0; i < directories.length; ++i) {
            files[i] = [];

            basename = path
                .basename(directories[i]);

            fs
                .mkdirSync(compressedDir + '/' + basename);

            fse
                .copySync(directories[i], compressedDir + '/' + basename);

            directories[i] = basename;

            grunt.file.recurse(
                compressedDir + '/' + directories[i],
                (function (i) {
                    return function (abspath, rootdir, subdir, filename) {
                        files[i].push([filename, subdir]);

                        fs
                            .writeFileSync(abspath, fs.readFileSync(abspath).toString().replace(/\?>\r?\n/g, '?>'));

                        ++filesCount;
                    };
                })(i)
            );
        }

        if (filesCount === 0) {
            done();

            return;
        }

        exec = require('child_process').exec;

        commonCommand = 'java -jar "' + __dirname + '/includes/HtmlCompressor/htmlcompressor.jar"' +
            ' -t html' +
            ' --preserve-php' +
            ' --compress-js' +
            ' --compress-css' +
            ' -c UTF-8';

        processed = 0;

        for (i = 0; i < directories.length; ++i) {
            if (files[i].length === 0) {
                continue;
            }

            for (y = 0; y < files[i].length; ++y) {
                (function (fileInfo, directory) {
                    var subdir, error;

                    /**
                     * Checks whether all files processed
                     */
                    function checkDone() {
                        if (processed === filesCount) {
                            done();
                        }
                    }

                    var writeError;

                    writeError = false;

                    subdir = fileInfo[1] ? fileInfo[1] + '/' : '';

                    try {
                        exec(
                            commonCommand + ' -o "' + directory + '/' + subdir + '"' + ' "' + directory + '/' + subdir + fileInfo[0] + '"',
                            function (compressError) {
                                ++processed;

                                if (!compressError) {
                                    console.log('Compressed ' + path.basename(directory) + '/' + subdir + fileInfo[0]);
                                } else {
                                    throw new Error('Compress error', 'COMPRESS');
                                }

                                checkDone();
                            }
                        );
                    } catch (error) {
                        writeError = error && error.code && error.code === 'EACCES';

                        if (writeError) {
                            ++processed;
                        }

                        grunt.log.error('Error ' + (writeError ? 'writing' : 'compressing') + ' ' + subdir + fileInfo[0]);
                    } finally {
                        if (writeError) {
                            checkDone();
                        }
                    }
                })(files[i][y], compressedDir + '/' + directories[i]);
            }
        }
    });

    grunt.registerMultiTask('imagesDomainReplace', function () {
        var i, domain;

        domain = /DOMAIN_IMAGES_FULL\s*=\s*([^\s][^\r\n]+)[\r\n]/.exec(iniContent);

        if (domain) {
            domain = domain[1];

            for (i = 0; i < this.data.sources.length; ++i) {
                grunt
                    .file
                    .write(
                        this.data.sources[i],
                        grunt
                            .file
                            .read(this.data.sources[i])
                            .replace(
                                /\{\{IMAGES_DOMAIN_FULL\}\}/g,
                                domain
                            )
                    );
            }
        } else {
            grunt.fail.fatal('Can\'t extract images domain from env.ini (IMAGES_DOMAIN_FULL key)');
        }
    });

    jsTasks = ['concat:js', 'imagesDomainReplace:js'];
    cssTasks = ['less:main', 'concat:css', 'imagesDomainReplace:css'];

    config = {
        concat: {
            js: {
                src: [
                    'www/js/jquery.min.js',
                    'assets/js/site.common.js',
                    'assets/js/lib/**/*.js',
                    'assets/lib/**/*.js',
                    'assets/js/jquery.storageapi.min.js',
                    'assets/js/jquery.form-validator.min.js',
                    'assets/js/respond.min.js',
                    'assets/js/common.js',
                    'assets/js/**/*.js'
                ],
                dest: 'www/js.js'
            },
            css: {
                src: [
                    'www/css/bootstrap.min.css',
                    'assets/lib/**/*.css',
                    'www/css.css'
                ],
                dest: 'www/css.css'
            }
        },
        less: {
            options: {
                compress: !debug,
                cleancss: !debug
            },
            main: {
                files: {
                    'www/css.css': [
                        'assets/less/common.less',
                        'assets/less/cart.less'
                    ],
                    'www/css/admin.css': 'assets/less/admin.less',
                    'assets/eskimobi/mcss.css' : 'assets/eskimobi/eskimobi.less'
                }
            }
        },
        uglify: {
            main: {
                files: {
                    'www/js.js': ['www/js.js'],
                    'www/eskimobi/mjs.js': ['assets/eskimobi/eskimobi.js'],

                }
            },
            options: {
                preserveComments: false
            }
        },
        cssmin: {
            main: {
                files: {
                    'www/css.css': 'www/css.css',
                     'www/eskimobi/mcss.css' : 'assets/eskimobi/mcss.css'

                }
            },
            options: {
                keepSpecialComments: 0
            }
        },
        imageEmbed: {
            main: {
                src: 'www/css.css',
                dest: 'www/css.css',
                options: {
                    baseDir: __dirname + '/www',
                    regexExclude: /(?:\/images\/src\/|\.(?:eot|woff|ttf|svg)(?:$|#|\?))/ig,
                    maxImageSize: 131072
                }
            },
            mobile: {
                src: 'www/eskimobi/mcss.css',
                dest: 'www/eskimobi/mcss.css',
                options: {
                    baseDir: __dirname + '/www',
                    regexExclude: /(?:\/images\/src\/|\.(?:eot|woff|ttf|svg)(?:$|#|\?))/ig,
                    maxImageSize: 131072
                }
            }
        },
        compressTemplates: {
            main: {
                dir: [
                    __dirname + '/templates',
                    __dirname + '/templates_admin',
                    __dirname + '/templates_landing'
                ],
                output_dir: 'templates_compressed'
            }
        },
        watch: {
            jsfiles: {
                files: [
                    'assets/js/**/*.js',
                    'assets/lib/**/*.js',
                    'www/js/client/**/*.js'
                ],
                tasks: jsTasks,
                options: {
                    spawn: false
                }
            },
            lessfiles: {
                files: [
                    'assets/less/**/*.less',
                    'assets/lib/**/*.css'
                ],
                tasks: cssTasks,
                options: {
                    spawn: false
                }
            }
        },
        imagesDomainReplace: {
            js: {
                sources: [
                    __dirname + '/www/js.js'
                ]
            },
            css: {
                sources: [
                    __dirname + '/www/css.css'
                ]
            }
        }
    };

    landingI = 0;

    grunt.file.expand({
        filter: 'isDirectory',
        cwd: __dirname + '/assets/landing/'
    }, '*').forEach(function (landingName) {
        ++landingI;

        grunt.file.expand({
            filter: 'isDirectory',
            cwd: __dirname + '/assets/landing/' + landingName + '/'
        }, '*').forEach(function (assetType) {
            var i, taskName, destFile;

            taskName = 'landing_' + landingI;

            switch (assetType) {
                case 'css':
                    destFile = __dirname + '/www/l/' + landingName + '/css.css';

                    config.concat[taskName + '_css'] = {
                        src: [
                            __dirname + '/assets/landing/' + landingName + '/css/main.css',
                            __dirname + '/assets/landing/' + landingName + '/css/**/*.css'
                        ],
                        dest: destFile
                    };

                    config.cssmin[taskName] = {
                        files: {}
                    };

                    config.cssmin[taskName].files[destFile] = destFile;

                    config.imageEmbed[taskName] = {
                        src: destFile,
                        dest: destFile,
                        options: {}
                    };

                    for (i in config.imageEmbed.main.options) {
                        if (!config.imageEmbed.main.options.hasOwnProperty(i)) {
                            continue;
                        }

                        config.imageEmbed[taskName].options[i] = config.imageEmbed.main.options[i];
                    }

                    delete config.imageEmbed[taskName].options.baseDir;

                    config.watch['landing_' + landingI + '_css'] = {
                        files: [__dirname + '/assets/landing/' + landingName + '/css/**/*.css'],
                            tasks: [
                                'concat:' + taskName + '_css'
                            ],
                            options: {
                            spawn: false
                        }
                    };

                    cssTasks.push('concat:' + taskName + '_css');

                    if (!debug) {
                        cssTasks.push('cssmin:' + taskName);

                        cssTasks.push('imageEmbed:' + taskName);

                        config.watch['landing_' + landingI + '_css'].tasks.push('cssmin:' + taskName);
                        config.watch['landing_' + landingI + '_css'].tasks.push('imageEmbed:' + taskName);
                    }

                    break;

                case 'js':
                    destFile = __dirname + '/www/l/' + landingName + '/js.js';

                    config.concat[taskName + '_js'] = {
                        src: [
                            __dirname + '/assets/landing/' + landingName + '/js/jquery.*js',
                            __dirname + '/assets/landing/' + landingName + '/js/jquery-*js',
                            __dirname + '/assets/landing/' + landingName + '/js/main.js',
                            __dirname + '/assets/landing/' + landingName + '/js/**/*.js'
                        ],
                        dest: destFile
                    };

                    config.uglify[taskName] = {
                        files: {}
                    };

                    config.uglify[taskName].files[destFile] = destFile;

                    config.watch['landing_' + landingI + '_js'] = {
                        files: [__dirname + '/assets/landing/' + landingName + '/js/**/*.js'],
                        tasks: [
                            'concat:' + taskName + '_js'
                        ],
                        options: {
                            spawn: false
                        }
                    };

                    jsTasks.push('concat:' + taskName + '_js');

                    if (!debug) {
                        jsTasks.push('uglify:' + taskName);

                        config.watch['landing_' + landingI + '_js'].tasks.push('uglify:' + taskName);
                    }

                    break;
            }
        });
    });

    defaultTasks = [];

    if (!debug) {
        jsTasks = jsTasks.concat(['uglify:main']);
        cssTasks = cssTasks.concat(['cssmin:main', 'imageEmbed:main', 'imageEmbed:mobile']);
        defaultTasks = defaultTasks.concat(['compressTemplates']);
    }

    defaultTasks = defaultTasks.concat(jsTasks, cssTasks);

    grunt.initConfig(config);

    grunt.registerTask('execute', defaultTasks);

    grunt.registerTask('default', ['execute', 'watch']);
};
