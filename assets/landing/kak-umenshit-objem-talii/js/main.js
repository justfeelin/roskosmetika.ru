$(function () {
    var slider_length, slider_active_id,
        timer_speed, slidertimer,
        $slider,
        today, tomorrow;

    $slider = $(".slider");

    slider_length = $slider.find("#sredstva .thumb").length;
    slider_active_id = 1;

    timer_speed = 7000;

    /**
     * Shows slider
     */
    function showSlider(sliderID) {
        $('#sredstva .span4').removeClass('active').filter('.sredstvo' + sliderID).addClass('active');

        slider_active_id = sliderID;

        $('#why_choose .sredstvo').removeClass('active').filter('#sredstvo' + sliderID).addClass('active');
    }

    /**
     * Slider change slider
     *
     * @param {Number} time
     *
     * @returns {number}
     */
    function timer(time) {
        return setInterval(function () {
            if (++slider_active_id > slider_length) {
                slider_active_id = 1;
            }

            showSlider(slider_active_id);
        }, time);
    }

    slidertimer = timer(timer_speed);

    $('#sredstva').mouseenter(function () {
        clearInterval(slidertimer)
    }).mouseleave(function () {
        slidertimer = timer(timer_speed);
    });

    $("#sredstva div.span4").each(function () {
        var $this, $thumb, sliderID;

        $this = $(this);
        $thumb = $this.find(".thumb");
        sliderID = this.getAttribute('data-id');

        $thumb.mouseenter(function () {
            showSlider(sliderID);
        });

        $this.find('p u').click(function () {
            showSlider(sliderID);

            return false;
        });
    });

    today = tomorrow = new Date();

    tomorrow.setDate(today.getDate()+1);

    $('.digits').countdown({
        image: "/l/kak-umenshit-objem-talii/img/jquery-countdown/digitsW.png",
        format: "hh:mm:ss",
        endTime: new Date(tomorrow.getFullYear(), tomorrow.getMonth(), tomorrow.getDate())
    });

    $('.to-cart').click(function () {
        var itemID;

        itemID = parseInt(this.getAttribute('data-id'), 10);

        if (itemID > 0) {
            $.ajax({
                url: '/cart/update/' + itemID + '/1',
                type: 'POST',
                dateType: 'json',
                timeout: 6000,
                beforeSend: function (XmlHttp) {
                    XmlHttp.setRequestHeader('Ajax', '1');
                },
                success: function () {
                    document.location.href = '/cart';
                },
                error: function () {
                    alert("Возникла ошибка при обработке запроса :(\n\nПопробуйте повторить запрос позже");
                }
            });
        }

        return false;
    });
});
