$(function () {
    var scrollInterval, $window, $btm;

    scrollInterval = 0;
    $window = $(window);

    $btm = $('#nabory-btm');

    $('.order-scroll').click(function () {
        var currentScroll, endScroll, scrollUp;

        if (scrollInterval) {
            window.clearInterval(scrollInterval);
        }

        endScroll = $btm.offset().top - $window.height();

        currentScroll = $window.scrollTop();

        scrollUp = currentScroll > endScroll;

        scrollInterval = window.setInterval(function () {
            currentScroll += 70 * (scrollUp ? -1 : 1);

            if (scrollUp ? currentScroll < endScroll : currentScroll > endScroll) {
                currentScroll = endScroll;
            }

            $window.scrollTop(currentScroll);

            if (currentScroll === endScroll) {
                window.clearInterval(scrollInterval);

                scrollInterval = 0;
            }
        }, 1);

        return false;
    });
});
