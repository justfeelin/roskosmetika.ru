/**
 * Подгружает дополнительные шаблоны в зависимости от url-а.
 * http://formvalidator.net/#configuration
 */
$(function () {
    'use strict';
    /* global storage, msg*/

    /**
     * Реализация записи на МК.
     */
    (function () {
        var modal = $('#mc-modal');

        if (!modal.length) {
            return;
        }

        var submitButton = modal.find('button[type="submit"]');
        var url = '/master_classes/subscribe';
        var $mc;

        $mc = $('#mc-modal');

        // Открытие окна записи на МК
        $('.master-class__submits').click(function () {
            var self = $(this);
            var mcName = self.data('name');
            var mc_id = self.data('id');

            modal.find('.mc-name').text(mcName);
            modal.find('#mc_id').val(mc_id);
            common.formStorageValues($mc, true);

            $mc.find('.form-group').removeClass('has-error');
            $mc.find('.form_error').remove();

            modal.modal('show');
        });

        $.validate({
            form: '#mc-modal',
            onSuccess: function () {
                var dat = {
                    'name': modal.find('.form-name').val(),
                    'phone': modal.find('.form-phone').val(),
                    'email': modal.find('.form-email').val(),
                    'mc_id': modal.find('#mc_id').val()
                };
                $.ajax({
                    url: url,
                    timeout: 6000,
                    type: 'POST',
                    data: dat,
                    dataType: 'json',
                    beforeSend: function (XmlHttp) {
                        submitButton.html('Записываем<span class="think"></span>');
                    },
                    success: function (data) {
                        submitButton.html('Записаться');

                        if (!data.errors) {
                            common.formStorageValues($mc);

                            modal.modal('hide');
                            $.alert(data.msg);
                        } else {
                            $mc.find('.form-group').removeClass('has-error');
                            $('#mc-modal .form_error').remove();

                            if (!$.isEmptyObject(data.errors.name)) {
                                $mc.find('.form-name').after('<p class="form_error">' + data.errors.name + '</p>');
                                $mc.find('.form-name').parents('.form-group').addClass('has-error');
                            }

                            if (!$.isEmptyObject(data.errors.phone)) {
                                $mc.find('.form-phone').after('<p class="form_error">' + data.errors.phone + '</p>');
                                $mc.find('.form-phone').parents('.form-group').addClass('has-error');
                            }

                            if (!$.isEmptyObject(data.errors.email)) {
                                $mc.find('.form-email').after('<p class="form_error">' + data.errors.email + '</p>');
                                $mc.find('.form-email').parents('.form-group').addClass('has-error');
                            }

                        }
                    },
                    error: function () {
                        modal.modal('hide');
                        submitButton.html('Записаться');
                        $.alert(msg.conError, 'Ошибка');
                    }
                });

                return false;
            }
        });
    })();

    /**
     * Реализация предзаказа
     */
    (function () {
        var modal = $('#pre-order-modal');
        var submitButton = modal.find('button[type="submit"]');
        var url = '/order/pre_order';

        // Открытие окна записи на МК
        common.$document.on('click', '.prod_waiting, .waiting', function () {
            var self = $(this);
            var prod_name = self.data('name');
            var prod_id = self.data('id');

            modal.find('.pre_p_name').text(prod_name);
            modal.find('.pre_p_img img').attr('src', common.IMG_DOMAIN + '/images/prod_photo/' + common.imgUrl('min_' + prod_id + '.jpg', this.getAttribute('data-prod-url')));
            modal.find('#p_id').val(prod_id);

            common.formStorageValues(modal, true);

            $('#pre-order-modal .form-group').removeClass('has-error');
            $('#pre-order-modal .form_error').remove();
            $('#pre-order-modal .form-check label').css('border-bottom','none');
            modal.modal('show');
        });

        $.validate({
            form: '#pre-order-modal',
            onSuccess: function () {
                var dat = {
                    'name': modal.find('.form-name').val(),
                    'email': modal.find('.form-email').val(),
                    'phone': modal.find('.form-phone').val(),
                    'check_mail': (modal.find('#fh_check_email').prop('checked') === true)? 1:0,
                    'check_sms': (modal.find('#fh_check_sms').prop('checked')=== true)? 1:0,
                    'prod_id': modal.find('#p_id').val()
                };

                $.ajax({
                    url: url,
                    timeout: 6000,
                    type: 'POST',
                    data: dat,
                    dataType: 'json',
                    beforeSend: function (XmlHttp) {
                        submitButton.html('Сообщить о поступлении<span class="think"></span>');
                    },
                    success: function (data) {
                        submitButton.html('Сообщить о поступлении');

                        if (data.errors) {
                            $('#pre-order-modal .form-group').removeClass('has-error');
                            $('#pre-order-modal .form_error').remove();


                            if (!$.isEmptyObject(data.errors.name)) {
                                $('#pre-order-modal .form-name').after('<p class="form_error">' + data.errors.name + '</p>');
                                $('#pre-order-modal .form-name').parents('.form-group').addClass('has-error');
                            }

                            if (!$.isEmptyObject(data.errors.phone)) {
                                $('#pre-order-modal .form-phone').after('<p class="form_error">' + data.errors.phone + '</p>');
                                $('#pre-order-modal .form-phone').parents('.form-group').addClass('has-error');
                            }

                            if (!$.isEmptyObject(data.errors.email)) {
                                $('#pre-order-modal .form-email').after('<p class="form_error">' + data.errors.email + '</p>');
                                $('#pre-order-modal .form-email').parents('.form-group').addClass('has-error');
                            }

                            if (!$.isEmptyObject(data.errors.check)) {
                                $('#pre-order-modal .form-check').after('<p class="form_error pre_order_center">' + data.errors.check + '</p>');
                                $('#pre-order-modal .form-check').parents('.form-group').addClass('has-error');
                                $('#pre-order-modal .form-check label').css('border-bottom','1px solid #f63e3c');
                            }

                        } else {
                            common.formStorageValues(modal);

                            modal.modal('hide');
                            $.alert(data.msg);
                        }
                    },
                    error: function () {
                        modal.modal('hide');
                        submitButton.html('Оформить предзаказ');
                        $.alert(msg.conError, 'Ошибка');
                    }
                });

                return false;
            }
        });
    })();

    /**
     * Реализация кнопки перезвонить
     */
    (function () {
        var modal = $('#recall-modal');
        var submitButton = modal.find('button[type="submit"]');
        var url = '/recall';

        // Открытие окна записи на МК
        $('.recall_button').click(function () {
            common.formStorageValues(modal, true);

            $('#recall-modal .form-group').removeClass('has-error');
            $('#recall-modal .form_error').remove();

            modal.modal('show');
        });

        $.validate({
            form: '#recall-form',
            onSuccess: function () {
                var dat = {
                    'name': modal.find('.form-name').val(),
                    'phone': modal.find('.form-phone').val()
                };
                $.ajax({
                    url: url,
                    timeout: 6000,
                    type: 'POST',
                    data: dat,
                    dataType: 'json',
                    beforeSend: function (XmlHttp) {
                        submitButton.html('Оформляем заявку<span class="think"></span>');
                    },
                    success: function (data) {
                        submitButton.html('Заказать звонок');

                        if (data.errors) {
                            $('#recall-modal .form-group').removeClass('has-error');
                            $('#recall-modal .form_error').remove();

                            if (!$.isEmptyObject(data.errors.name)) {
                                $('#recall-modal .form-name').after('<p class="form_error">' + data.errors.name + '</p>');
                                $('#recall-modal .form-name').parents('.form-group').addClass('has-error');
                            }

                            if (!$.isEmptyObject(data.errors.phone)) {
                                $('#recall-modal .form-phone').after('<p class="form_error">' + data.errors.phone + '</p>');
                                $('#recall-modal .form-phone').parents('.form-group').addClass('has-error');
                            }

                        } else {
                            common.formStorageValues(modal);

                            modal.modal('hide');
                            $.alert(data.msg);
                        }
                    },
                    error: function () {
                        modal.modal('hide');
                        submitButton.html('Заказать звонок');
                        $.alert(msg.conError, 'Ошибка');
                    }
                });

                return false;
            }
        });
    })();

    /**
     * Реализация "вопрос косметлогу".
     */
    (function () {
        var modal = $('#question-modal');

        if (!modal.length) {
            return;
        }

        var submitButton = modal.find('button[type="submit"]');
        var url = '/question/add_new';

        // Открытие окна "задать вопрос косметологу"
        $('.question__submit').click(function () {
            common.formStorageValues(modal, true);

            $('#question-modal .form-group').removeClass('has-error');
            $('#question-modal .form_error').remove();

            modal.modal('show');
        });

        // проверка полей формы
        $.validate({
            form: '#question-form',
            onSuccess: function () {
                var dat = {
                    'name': modal.find('.form-name').val(),
                    'email': modal.find('.form-email').val(),
                    'question': modal.find('.form-question').val()
                };

                $.ajax({
                    url: url,
                    timeout: 6000,
                    type: 'POST',
                    dataType: 'json',
                    data: dat,
                    beforeSend: function (XmlHttp) {
                        submitButton.html('Отправляем<span class="think"></span>');
                    },
                    success: function (data) {
                        submitButton.html('Задать вопрос');

                        if (data.errors) {
                            $('#question-modal .form-group').removeClass('has-error');
                            $('#question-modal .form_error').remove();

                            if (!$.isEmptyObject(data.errors.question)) {
                                $('#question-modal .form-question').after('<p class="form_error">' + data.errors.question + '</p>');
                                $('#question-modal .form-question').parents('.form-group').addClass('has-error');
                            }

                            if (!$.isEmptyObject(data.errors.email)) {
                                $('#question-modal .form-email').after('<p class="form_error">' + data.errors.email + '</p>');
                                $('#question-modal .form-email').parents('.form-group').addClass('has-error');
                            }
                        } else {
                            common.formStorageValues(modal);

                            modal.modal('hide');
                            $.alert(data.msg);
                        }
                    },
                    error: function () {
                        modal.modal('hide');
                        submitButton.html('Задать вопрос');
                        $.alert(msg.conError, 'Ошибка');
                    }
                });

                return false;
            }
        });
    })();

    /**
     * Реализация "вопрос косметлогу".
     */
    (function () {
        var modal = $('#all-question-modal');
        var submitButton = modal.find('button[type="submit"]');
        var url = '/question/add_new_all';

        // Открытие окна "задать вопрос косметологу"
        $('.right_question').click(function () {
            common.formStorageValues(modal, true);

            common.phoneInput($('#q-phone'));

            $('#all-question-modal .form-group').removeClass('has-error');
            $('#all-question-modal .form_error').remove();

            modal.modal('show');
        });

        // проверка полей формы
        $.validate({
            form: '#all-question-form',
            onSuccess: function () {
                var dat = {
                    'name': modal.find('.form-name').val(),
                    'email': modal.find('.form-email').val(),
                    'phone': modal.find('#q-phone').val(),
                    'question': modal.find('.form-question').val()
                };

                $.ajax({
                    url: url,
                    timeout: 6000,
                    type: 'POST',
                    dataType: 'json',
                    data: dat,
                    beforeSend: function () {
                        submitButton.html('Отправляем<span class="think"></span>');
                    },
                    success: function (data) {
                        submitButton.html('Задать вопрос');

                        if (data.errors) {
                            $('#all-question-modal .form-group').removeClass('has-error');
                            $('#all-question-modal .form_error').remove();

                            if (!$.isEmptyObject(data.errors.name)) {
                                $('#all-question-modal .form-name').after('<p class="form_error">' + data.errors.name + '</p>');
                                $('#all-question-modal .form-name').parents('.form-group').addClass('has-error');
                            }

                            if (!$.isEmptyObject(data.errors.question)) {
                                $('#all-question-modal .form-question').after('<p class="form_error">' + data.errors.question + '</p>');
                                $('#all-question-modal .form-question').parents('.form-group').addClass('has-error');
                            }

                            if (!$.isEmptyObject(data.errors.phone)) {
                                $('#all-question-modal #q-phone').after('<p class="form_error">' + data.errors.phone + '</p>');
                                $('#all-question-modal #q-phone').parents('.form-group').addClass('has-error');
                            }

                            if (!$.isEmptyObject(data.errors.email)) {
                                $('#all-question-modal .form-email').after('<p class="form_error">' + data.errors.email + '</p>');
                                $('#all-question-modal .form-email').parents('.form-group').addClass('has-error');
                            }
                        } else {
                            var new_name = false;
                            if (dat.name) {
                                new_name = dat.name
                            }
                            common.checkStorage(new_name, dat.email, false, false, false);
                            modal.modal('hide');
                            $.alert(data.msg);
                        }
                    },
                    error: function () {
                        modal.modal('hide');
                        submitButton.html('Задать вопрос');
                        $.alert(msg.conError, 'Ошибка');
                    }
                });

                return false;
            }
        });
    })();


    /**
     * Product review
     */
    (function () {
        var modal = $('#review-modal');

        if (!modal.length) {
            return;
        }

        var submitButton = modal.find('button[type="submit"]');
        var url = '/prod_review/add_comment';

        // open window add comment for product
        $('.reviw_link_tag').click(function (e) {
            e.preventDefault();

            var self = $(this);
            var prod_name = self.data('name');
            var prod_id = self.data('id');

            modal.find('.prod-name').html(prod_name);
            modal.find('#prod_id').val(prod_id);

            common.formStorageValues(modal, true);

            $('#review-modal .form-group').removeClass('has-error');
            $('#review-modal .form_error').remove();

            modal.modal('show');
        });

        // check form fields
        $.validate({
            form: '#comment-form',
            onSuccess: function () {
                var dat = {
                    'prod_id': modal.find('#prod_id').val(),
                    'name': modal.find('.form-name').val(),
                    'city': modal.find('#city').val(),
                    'email': modal.find('.form-email').val(),
                    'comment': modal.find('.form-comment').val()
                };

                $.ajax({
                    url: url,
                    timeout: 6000,
                    type: 'POST',
                    dataType: 'json',
                    data: dat,
                    beforeSend: function (XmlHttp) {
                        submitButton.html('Отправляем<span class="think"></span>');
                    },
                    success: function (data) {
                        submitButton.html('Оставить отзыв');

                        if (data.errors) {
                            $('#review-modal .form-group').removeClass('has-error');
                            $('#review-modal .form_error').remove();

                            if (!$.isEmptyObject(data.errors.comment)) {
                                $('#review-modal .form-comment').after('<p class="form_error">' + data.errors.comment + '</p>');
                                $('#review-modal .form-comment').parents('.form-group').addClass('has-error');
                            }

                            if (!$.isEmptyObject(data.errors.email)) {
                                $('#review-modal .form-email').after('<p class="form_error">' + data.errors.email + '</p>');
                                $('#review-modal .form-email').parents('.form-group').addClass('has-error');
                            }
                        } else {
                            common.formStorageValues(modal);

                            modal.modal('hide');
                            $.alert(data.msg);
                        }
                    },
                    error: function () {
                        modal.modal('hide');
                        submitButton.html('Оставить отзыв');
                        $.alert(msg.conError, 'Ошибка');
                    }
                });

                return false;
            }
        });
    })();


    /**
     * Реализация восстановить пароль.
     */
    (function () {
        var modal = $('#psw-recover-modal');
        var submitButton = modal.find('button[type="submit"]');
        var url = '/forgot';

        common.$document.on('click', '.backup_psw', function () {
            $('#psw-recover-form .form-group').removeClass('has-error');
            $('#psw-recover-form .form_error').remove();

            modal.modal('show');

            return false;
        });

        // проверка полей формы
        $.validate({
            form: '#psw-recover-form',
            onSuccess: function () {
                var dat = {
                    'email': modal.find('.form-email').val()
                };

                $.ajax({
                    url: url,
                    timeout: 6000,
                    type: 'POST',
                    dataType: 'json',
                    data: dat,
                    beforeSend: function () {
                        submitButton.html('Восстанавливаем<span class="think"></span>');
                    },
                    success: function (data) {
                        submitButton.html('Восстановить пароль');

                        if (data.errors) {
                            $('#psw-recover-forgm .form-group').removeClass('has-error');
                            $('#psw-recover-form .form_error').remove();

                            if (!$.isEmptyObject(data.errors.email)) {
                                $('#psw-recover-form .form-email').after('<p class="form_error">' + data.errors.email + '</p>');
                                $('#psw-recover-form .form-email').parents('.form-group').addClass('has-error');
                            }

                        } else {
                            modal.modal('hide');
                            $.alert(data.msg, '', true);
                        }
                    },
                    error: function () {
                        modal.modal('hide');
                        submitButton.html('Восстановление пароля');
                        $.alert(msg.conError, 'Ошибка');
                    }
                });

                return false;
            }
        });
    })();
});
