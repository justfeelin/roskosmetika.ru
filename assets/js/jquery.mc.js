/**
 * Обрабатывает переключение мастер-классов на главной.
 */
$.fn.mc = function () {
    'use strict';

    var timeout = 4000;
    var timeoutId;
    var itemClass = '.mc-item';
    var itemsContainer = $('.nearest_mc');
    var items = itemsContainer.find(itemClass);
    var activeMc = items.first();

    var setActive = function (target) {
        items.removeClass('active-mc').addClass('hidden');
        target.addClass('active-mc').removeClass('hidden');
        activeMc = target;
    };

    var prev = function () {
        setActive(activeMc.prev(itemClass).length ? activeMc.prev(itemClass) : items.last());
    };

    var next = function () {
        setActive(activeMc.next(itemClass).length ? activeMc.next(itemClass) : items.first());

        timeoutId = setTimeout(next, timeout);
    };

    if (items.length > 1) {
        items.find('.pointer_left').click(prev);
        items.find('.pointer_right').click(next);

        timeoutId = setTimeout(next, timeout);

        itemsContainer
            // отключайем таймер при навеледение на блок мк
            .mouseover(function () {
                clearTimeout(timeoutId);
                timeoutId = null;
            })
            // включаем таймер при наведение на блок мк
            .mouseout(function () {
                if (!timeoutId) {
                    timeoutId = setTimeout(next, timeout);
                }
            });
    } else {
        items.find('.pointer_left').remove();
        items.find('.pointer_right').remove();
        items.find('.nearest_mc_link').removeClass('col-xs-8').addClass('col-xs-12');
    }

    return this;
};