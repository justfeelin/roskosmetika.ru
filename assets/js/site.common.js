window.common = {
    _common: window.common,
    $window: $(window),
    $document: $(document),
    IMG_DOMAIN: '{{IMAGES_DOMAIN_FULL}}'
};

$.ajaxSetup({
    headers: {
        Ajax: 1
    }
});
