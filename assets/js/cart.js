if (common.cartPage) {
    var shippingsCopy;

    /**
     * Toggles readonly state for cart address field
     *
     * @param {String} [address] Address to set. If passed - field will be set as readonly
     */
    function setAddressField(address) {
        $('#cart-address')
            .val(address ? address : '')
            .prop('readonly', !!address);
    }

    common.gaPaveviewSent = true;
    ga('MainUATracker.send', 'pageview');

    /**
     * Updates shipping types option tag
     */
    common.updateShippingOptions = function() {
        var region, cartShipping, i, optionValue, option,
            y;

        region = document.getElementById('cart-region');

        if (region.selectedIndex === -1) {
            return;
        }

        cartShipping = document.getElementById('cart-shipping');

        if (!shippingsCopy) {
            shippingsCopy = cartShipping.cloneNode(true);
        }

        option = region.options[region.selectedIndex];

        cartShipping.innerHTML = '';

        y = 0;

        setAddressField();

        for (i = 0; i < shippingsCopy.options.length; ++i) {
            var price;

            optionValue = shippingsCopy.options[i].getAttribute('value');

            if (optionValue !== '5') {
                price = option.getAttribute('data-' + optionValue + '-price');

                if (price !== '') {
                    price = parseInt(price, 10);

                    cartShipping
                        .appendChild(
                            shippingsCopy
                                .options[i]
                                .cloneNode(false)
                        );

                    cartShipping
                        .options[y]
                        .innerHTML = shippingsCopy
                            .options[i]
                            .getAttribute('data-name') + ' (' + (price > 0 ? price + ' р.' : 'Бесплатно') + ')';

                    if (common.ng.$cartScope.cart.info.shipping == cartShipping.options[y].value) {
                        cartShipping
                            .selectedIndex = y;
                    }

                    ++y;
                }
            } else { 

                cartShipping
                    .appendChild(
                        shippingsCopy
                            .options[i]
                            .cloneNode(false)
                    );

                cartShipping
                    .options[y]
                    .innerHTML = shippingsCopy
                        .options[i]
                        .getAttribute('data-name');

                ++y;
            }
        }
    };

    $(function () {
        var $region, cartShipping, $shippingBox, $cartForm,
            ppValue,
            sdekMap, sdekPoints, choosenSdekPoint;

        /**
         * Scrolls to element
         *
         * @param {jQuery} $element Element to be scrolled to
         */
        function scrollToElement ($element) {
            if ($element.length) {
                var height_from_top = $element.offset().top;

                $("html:not(:animated)" + ",body:not(:animated)").animate({
                    scrollTop: height_from_top
                }, 600);
            }
        }

        /**
         * Shows SDEK map
         *
         * @param {Object} points SDEK points
         */
        function showSdek(points) {
            var i,
                clusterer, mapPoints;

            if (!sdekMap) {
                sdekMap = new ymaps.Map('sdek-map', {
                    center: [55.76, 37.64],
                    zoom: 9,
                    controls: [
                        'geolocationControl',
                        'searchControl',
                        'fullscreenControl',
                        'zoomControl',
                        'rulerControl'
                    ]
                });

                common
                    .$document
                    .on(
                        'click',
                        '.sdek-point-btn',
                        function () {
                            if (choosenSdekPoint) {
                                common.ng.$cartScope.cart.info.sdek = (choosenSdekPoint.type === 2 ? 'ПОСТАМАТ ' : '') + choosenSdekPoint.address;
                                common.ng.$cartScope.cart.info.sdek_code = choosenSdekPoint.code;

                                common.ng.$cartScope.cart.info.pickpoint = '';

                                setAddressField(choosenSdekPoint.address);

                                common.ng.$cartScope.$apply();

                                if (sdekMap.balloon) {
                                    sdekMap
                                        .balloon
                                        .close();
                                }

                                $('#sdek-modal')
                                    .modal('hide');
                            }

                            return false;
                        }
                    );

                $('#sdek-modal')
                    .on('shown.bs.modal', function () {
                        sdekMap
                            .container
                            .fitToViewport();
                    });

                mapPoints = [];

                for (i = 0; i < points.length; ++i) {
                    (function (point) {
                        var placemark;

                        placemark = new ymaps.Placemark(
                            [point.lat, point.lng],
                            {
                                hintContent: (point.type === 2 ? 'ПОСТАМАТ: ' : '') + point.address
                            },
                            {
                                iconLayout: 'default#image',
                                iconImageHref: '/images/sdek' + (point.type === 2 ? '_point' : '') + '.png',
                                iconImageSize: [35, 35],
                                iconImageOffset: [-17, -35]
                            }
                        );

                        mapPoints
                            .push(placemark);

                        sdekMap
                            .geoObjects
                            .add(placemark);

                        placemark
                            .events
                            .add('click', function (event) {
                                choosenSdekPoint = point;

                                event
                                    .get('target')['properties']
                                    .set(
                                        'balloonContent',
                                        '<div id="sdek-balloon">' +
                                        '<h4>' + (point.type === 2 ? 'ПОСТАМАТ ' : '') + point.address + '</h4>' +
                                        (point.name ? '<p><b>' + point.name + '</b></p>' : '') +
                                        (point.phone ? '<p>Тел.: ' + point.phone + '</p>' : '') +
                                        (point.work ? '<p>Режим работы: ' + point.work + '</p>' : '') +
                                        '<a href="" class="btn btn-primary sdek-point-btn">Выбрать</a>' +
                                        '</div>'
                                    );
                            });
                    })(points[i]);
                }

                clusterer = new ymaps.Clusterer();

                clusterer
                    .add(mapPoints);

                sdekMap
                    .geoObjects
                    .add(clusterer);
            }

            $('#sdek-map')
                .height(common.$window.height() - 200);

            $('#sdek-modal')
                .modal('show');
        }

        $('#cancel-promo-btn')
            .click(function () {
                if (!confirm('Отменить промо-код?')) {
                    return false;
                }

                $.post(
                    '/promo_codes/cancel',
                    function (o) {
                        if (o) {
                            o
                                .info
                                .promo_applied = false;

                            common
                                .ng
                                .$cartScope
                                .update(o);

                            $('.link_to_promo')
                                .show();
                        } else {
                            $
                                .alert('Возникла ошибка при отмене промо-кода, поробуйте повторить позже.');
                        }
                    },
                    'json'
                );

                return false;
            });

        $('#sdek-choose')
            .click(function () {
                if (sdekPoints) {
                    showSdek(sdekPoints);
                } else {
                    $.get(
                        '/cart/sdek',
                        function (points) {
                            if (points) {
                                sdekPoints = points;

                                showSdek(points);
                            } else {
                                $.alert('К сожалению, нам не удалось загрузить карту пунктов выдачи СДЭК :-(');
                            }
                        },
                        'json'
                    );
                }

                return false;
            });

        ppValue = '';

        // check order info and checkout
        $cartForm = $('#cart-form');

        //  take info from local storage
        common.formStorageValues($cartForm, true);

        $region = $('#cart-region');

        $shippingBox = $('#cart-shipping-box');

        cartShipping = document.getElementById('cart-shipping');

        $region.change(function () {
            var option;

            option = this.options[this.selectedIndex];

            common.ng.$cartScope.cart.info.pickpoint = ppValue = '';

            setAddressField();

            $shippingBox.show();

            common.updateShippingOptions();

            common.ng.$cartScope.cart.duration_min = parseInt(option.getAttribute('data-duration-min'), 10);
            common.ng.$cartScope.cart.duration_max = parseInt(option.getAttribute('data-duration-max'), 10);

            common.ng.$cartScope.updateShippingPrice(true);
        }).select2();

        $(cartShipping).change(function () {
            common.ng.$cartScope.cart.info.pickpoint = ppValue = '';

            common.ng.$cartScope.updateShippingPrice();

            setAddressField();
        });

        if ($region.val()) {
            $region.triggerHandler('change');
        }

        $('.cart_items, .cart_form').on('click', '.cart_checkout', function () {
            var fio, fioValue, phone, phoneVal, email, region, shipping, adress, etc, uid;

            if ($('body').hasClass('promo-input')) {
                alert('Пожалуйста, введите промо-код и нажмите кнопку «ОК»');

                return false;
            }

            if ($('[class*= pi_]').length) {
                $('.form_error').remove();
                $('.cart_user_info .form-group').removeClass('has-error');

                fio = $('.cu_fio');
                fioValue = fio.val();
                phone = $('.cu_phone');
                phoneVal = phone.val();
                email = $.trim($('.cu_email').val());
                ObjEmail = $('.cu_email');
                region = $('#cart-region').val();
                shipping = region && cartShipping.selectedIndex !== -1 ? cartShipping.options[cartShipping.selectedIndex].value : '';
                adress = $('.cu_adress').val();
                etc = $('.cu_etc').val();
                uid = $('.cu_uid').val();

                if (!fioValue || !phoneVal || !email) {
                    if (!fioValue) {
                        fio.parents('.form-group').addClass('has-error');
                        fio.after('<p class="form_error">Забыли&nbsp;представиться.</p>');
                    }
                    if (!phoneVal) {
                        phone.parents('.form-group').addClass('has-error');
                        phone.after('<p class="form_error">Забыли&nbsp;оставить&nbsp;телефон.<br>Мы&nbsp;не&nbsp;шлем&nbsp;рекламные&nbsp;СМС!</p>');
                    }
                    if (!email) {
                        ObjEmail.parents('.form-group').addClass('has-error');
                        ObjEmail.after('<p class="form_error">Забыли&nbsp;указать&nbsp;почту.<br>Мы&nbsp;отправим&nbsp;Вам&nbsp;информацию&nbsp;о&nbsp;заказе!</p>');
                    }

                    scrollToElement($('.cart_h2:last'));
                } else {
                    if (!(/^[\d\s()+-]{16,20}$/.test(phoneVal))) {
                        phone.parents('.form-group').addClass('has-error');
                        phone.after('<p class="form_error">Что-то&nbsp;не&nbsp;так&nbsp;с&nbsp;телефоном!<br>Введите&nbsp;корректный&nbsp;номер&nbsp;телефона&nbsp;.</p>');
                        scrollToElement($('.cart_h2:last'));
                    } else if(!(/^[\._a-zA-Z\d-]+@[\.a-zA-Z\d-]+\.[a-zA-Z]{2,6}$/.test(email))) {
                        ObjEmail.parents('.form-group').addClass('has-error');
                        ObjEmail.after('<p class="form_error">Что-то&nbsp;не&nbsp;так&nbsp;с&nbsp;Email&nbsp;адресом!<br>Введите&nbsp;корректный&nbsp;Email&nbsp;.</p>');
                        scrollToElement($('.cart_h2:last'));
                    }
                    else {
                        if (!email) email = false;
                        if (!region) region = false;

                        var submit_button = $(this);

                        //  ajax query for order
                        var dat = {
                            'flag_from_cart': true,
                            'fio': fioValue,
                            'phone': phoneVal,
                            'email': email,
                            'region': region,
                            'shipping': shipping,
                            'adress': adress,
                            'etc': etc,
                            'pickpoint': ppValue,
                            pickpoint_code: common.ng.$cartScope.cart.info.pickpoint || '',
                            sdek: common.ng.$cartScope.cart.info.sdek || '',
                            sdek_code: common.ng.$cartScope.cart.info.sdek_code || '',
                            deliverCertificate: document.getElementById('deliver-certificate').checked ? 1 : '',
                            'id': uid
                        };

                        submit_button.css('background', '#a0e161');
                        submit_button.html('Оформляем<span class="think"></span>');

                        $.ajax({
                            url: '/order',
                            timeout: 30000,
                            type: 'POST',
                            data: dat,
                            dataType: 'json',
                            success:
                                /**
                                 * @param {{success: Boolean, order_id: Number, sum: Number, discount: Number, products: [{id: Number, name: String, price: Number, quantity: Number, brand: String, variant: String, category: String}], fio_error: String, phone_error: String, mindboxTicket: String}} data
                                 */
                                function (data) {
                                    var i, orderData,
                                    redirectUrl;

                                    if (data.success && data.order_id) {
                                        redirectUrl = '/order/ok/' + data.order_id;

                                        //  update local storage
                                        common.formStorageValues($cartForm);

                                        for (i = 0; i < data.products.length; ++i) {
                                            ga('UniversalAnalytics.ec:addProduct', {
                                                id: '' + data.products[i].id,
                                                name: data.products[i].ua_name,
                                                category: data.products[i].category,
                                                brand: data.products[i].brand_url,
                                                variant: data.products[i].ua_pack,
                                                price: '' + data.products[i].price,
                                                quantity: data.products[i].quantity
                                            });

                                            // GUA main                                            
                                            ga('MainUATracker.ec:addProduct', {
                                                id: '' + data.products[i].id,
                                                name: data.products[i].ua_name,
                                                category: data.products[i].category,
                                                brand: data.products[i].brand_url,
                                                variant: data.products[i].ua_pack,
                                                price: '' + data.products[i].price,
                                                quantity: data.products[i].quantity
                                            });

                                        }

                                        orderData = {
                                            id: '' + data.order_id,
                                            affiliation: 'Online',
                                            revenue: '' + data.sum,
                                            tax: 0
                                        };

                                        if (data.shipping) {
                                            orderData.shipping = '' + data.shipping;
                                        }

                                        if (data.promo_discount) {
                                            orderData.coupon = data.promo_discount + '%';
                                        }

                                        ga('UniversalAnalytics.ec:setAction', 'purchase', orderData);

                                        // GUA Main
                                        orderData.id = ('' + data.order_number);
                                        ga('MainUATracker.ec:setAction', 'purchase', orderData);

                                        common.gaPageview(function () {
                                            location = redirectUrl;
                                        });

                                        setTimeout(
                                            function() {
                                                //just in case
                                                location = redirectUrl;
                                            },
                                            3000
                                        );
                                    } else {
                                        submit_button.css('background', '#00c5f0');
                                        submit_button.html('Оформить заказ');

                                        // view errors
                                        if (data.fio_error || data.phone_error) {
                                            $('.form_error').remove();
                                            $('.cart_user_info .form-group').removeClass('has-error');

                                            if (data.fio_error) {
                                                fio.parents('.form-group').addClass('has-error');
                                                fio.after('<p class="form_error">' + data.fio_error + '</p>');
                                            }
                                            if (data.phone_error) {
                                                phone.parents('.form-group').addClass('has-error');
                                                phone.after('<p class="form_error">' + data.phone_error + '</p>');
                                            }
                                        } else {
                                            if (data.errors) {
                                                $.alert(data.errors, 'Упс =(');
                                            }
                                        }
                                    }
                                },
                            error: function () {
                                submit_button.html('Оформить заказ');
                                submit_button.css('background', '#00c5f0');
                                $.alert('Нам очень стыдно, но что-то случилось с нашим сайтом, попробуйте попозже. Если ошибка повторится - Вы всегда можете нам позвонить и оформить заказ по телефону.', 'Упс =(');
                            }
                        });
                    }
                }
            } else {
                $.alert('В корзине нет товаров!', 'Корзина');
            }
        });

        // check promo code and imput
        $('.cart_form').on('click', '.promo_checkout', function () {
            var submit_button = $(this);

            $('.promo_fg .form_error').remove();
            $('.promo_fg').removeClass('has-error');

            var promo = $('.cu_promo');

            if (!promo.val()) {

                $('.promo_fg').addClass('has-error');
                promo.after('<p class="form_error">Забыли&nbsp;ввести&nbsp;промо-код.</p>');

            }
            else {
                //  ajax query for pre_order
                var dat = {
                    'code': promo.val()
                };

                submit_button
                    .removeClass('btn-default')
                    .addClass('btn-primary')
                    .prop('disabled', true)
                    .html('...');

                $.ajax({
                    url: '/promo_codes',
                    timeout: 6000,
                    type: 'POST',
                    data: dat,
                    dataType: 'json',
                    success: /**
                     *
                     * @param {{cart: {promo_discount: Number}, errors: String}} data
                     */
                    function (data) {
                        submit_button
                            .removeClass('btn-primary')
                            .addClass('btn-default')
                            .prop('disabled', false)
                            .html('OK');

                        if (data.success) {
                            if (data.cart) {
                                common.ng.$cartScope.update(data.cart);
                            }

                            promo
                                .val('');

                            $('.promo_form')
                                .hide();

                            $('body').removeClass('promo-input');
                        } else {
                            // view errors
                            if (data.msg) {

                                $('.promo_fg .form_error').remove();
                                $('.promo_fg').removeClass('has-error');

                                $('.promo_fg').addClass('has-error');
                                promo.after('<p class="form_error">' + data.msg + '</p>');


                            } else {

                                $.alert(data.errors, 'Упс =(');

                            }
                        }
                    },
                    error: function () {
                        submit_button
                            .removeClass('btn-primary')
                            .addClass('btn-default')
                            .prop('disabled', false)
                            .html('OK');

                        $.alert('Нам очень стыдно, но что-то случилось с нашим сайтом, попробуйте попозже. Если ошибка повторится - Вы всегда можете нам позвонить и оформить заказ по телефону.', 'Упс =(');
                    }
                });
            }
        });

        //  Show promo form
        $('.view_promo_form').on('click', function () {
            $(this).parents('.link_to_promo').hide();
            $('.promo_form').hide().fadeIn();
            $('body').addClass('promo-input');
        });

        $('#close-promo-input').click(function () {
            $('.promo_form').hide();
            $(this).closest('.promo_fg').find('.link_to_promo').hide().fadeIn();
            $('body').removeClass('promo-input');

           return false;
        });

        // phone with 8 in code
        $('.cu_phone').on('keyup', function () {
            var phone = $(this);

            if ($('.phone_with_8').length) {
                $('.phone_with_8').detach();
            }

            if (phone.val().charAt(4) == 8 && phone.val().charAt(5) == 9) {
                phone.before('<p class="phone_with_8">Обратите внимание!<br>Формат ввода: +7&nbsp;(XXX)&nbsp;XXXXXXX</p>');
            }
        });

        $('.shipping-compensation-info').popover({
            placement: 'right',
            content: function () {
                return (
                    common.ng.$cartScope.cart.info.shipping_compensation === true
                        ? 'Бесплатная доставка'
                        : (
                            'Компенсация<br>за доставку<br>до ' + common.priceFormat(common.ng.$cartScope.cart.info.shipping_compensation) + ' руб.'
                        )
                    ) +
                    '<br>при сумме заказа<br>от ' + common.priceFormat(common.ng.$cartScope.cart.info.shipping_compensation_sum) + ' руб.' +
                    (
                        !common.ng.$cartScope.cart.info.region || common.ng.$cartScope.cart.info.region !== 77
                            ? '<br><br>Предоставляется<br><strong>только</strong> при<br>предоплате заказа.'
                            : ''
                    );
            },
            html: true,
            trigger: 'hover'
        });

        $('#pp-choose').click(function () {
            setAddressField();

            PickPoint.open(
                /**
                 * Pick-Point event handler
                 *
                 * @param {{id: String, address: String}} o
                 */
                function (o) {
                    ppValue = o.address + ' (Постомат №' + o.id + ')';

                    common.ng.$cartScope.cart.info.pickpoint = o.id;

                    common.ng.$cartScope.cart.info.sdek = common.ng.$cartScope.cart.info.sdek_code = '';

                    setAddressField(o.address);

                    common.ng.$cartScope.$apply();
                }
            );

            return false;
        });

        common
            .addressAutocomplete(
                $('#cart-address')
            );
    });

    common.ng.app.filter('duration', function () {
        return function (input) {
            var m;

            if (m = /(\d)?(\d)$/.exec(input)) {
                input += ' ' + (
                    m[1] === '1' || m[1] && m[2] === '0' || m[2] > 4 && m[2] < 10
                        ? 'дней'
                        : (
                            m[2] === '1'
                                ? 'день'
                                : 'дня'
                        )
                );
            }

            return input;
        };
    });
}
