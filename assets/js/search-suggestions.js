$(function () {
    var $form, formAction, TO, searchValue, $input, input, $suggestions, suggestions, suggestionsContent;

    /**
     * Checks suggestions block height against window height and adds scrollbar if needed
     */
    function checkHeight () {
        var maxHeight, overflow;

        $suggestions.removeClass('scrollable');

        $suggestions.css('height', '');

        maxHeight = common.$window.height() - 150;
        overflow = $suggestions.height() > maxHeight;

        $suggestions.toggleClass('scrollable', overflow);

        $suggestions.css('height', overflow ? maxHeight : '');
    }

    $form = $("#search-form");
    formAction = $form.attr("action");
    searchValue = "";
    $suggestions = $("#search-suggestions");
    suggestions = false;
    $input = $form.find(".search-input").focus();
    suggestionsContent = false;

    $form.submit(function () {
        var value;

        value = $.trim($input.val());

        if (!/^[0-9]+$/.test(value) && value.length < 3) {
            return false;
        }
    });

    input = $input[0];

    $input.bind("keyup paste cut", function () {
        if (TO) {
            clearTimeout(TO);
        }

        TO = setTimeout(
            function () {
                var currentValue, productID;

                currentValue = $.trim(input.value);

                productID = parseInt(currentValue, 10) || 0;

                if ((productID < 1 && currentValue.length < 3) || currentValue === searchValue) {
                    if (currentValue !== searchValue) {
                        searchValue = currentValue;

                        $suggestions.hide();
                    }

                    suggestions = false;

                    return;
                }

                searchValue = currentValue;

                $.get(
                    formAction,
                    $form.serialize() + "&suggestions=1",
                    function (html) {
                        suggestionsContent = !!html;

                        if (html) {
                            $suggestions.html(html);

                            $suggestions
                                .find('.show-all-btn')
                                .click(function () {
                                    $suggestions
                                        .hide();

                                    suggestions = false;
                                });

                            checkHeight();

                            suggestions = true;
                        }

                        $suggestions.toggle(suggestionsContent);

                        if (suggestionsContent) {
                            common.uaProductList($suggestions.find('.ua-product-list'));

                            common.gaPageview();
                        }

                        $input.toggleClass("no-shadow", suggestionsContent);
                    },
                    "html"
                );
            },
            500
        );
    });

    $input.focus(function () {
        if (suggestionsContent) {
            suggestions = true;
            $suggestions.show();
        }
    });

    common.$document.click(function (event) {
        var $target;

        if (!suggestions) {
            return;
        }

        $target = $(event.target);

        if (!$target.closest($suggestions).length && !$target.is($suggestions) && !$target.is($input)) {
            $suggestions.hide();
        }
    });

    common.$window.resize(function () {
        if (suggestions) {
            checkHeight();
        }
    });
});
