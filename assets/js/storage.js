(function () {
    /**
     * Sets new value in localStorage, only if value is provided
     *
     * @param {String} name Name value
     * @param {String} email Email value
     * @param {String} region Region selected index value
     * @param {String} password Password value
     * @param {String} phone Phone value
     *
     * @returns {boolean}
     */
    function checkStorage (name, email, region, password, phone) {
        if (!!name) {
            storage.set('name', name);
        }

        if (!!email) {
            storage.set('email', email);
        }

        if (!!region) {
            storage.set('region', region);
        }

        if (!!password) {
            storage.set('password', password);
        }

        if (!!phone) {
            storage.set('phone', phone);
        }

        return true;
    }

    /**
     * Sets new values in localStorage
     *
     * @param {String} name Name value
     * @param {String} email Email value
     * @param {String} region Region selected index value
     * @param {String} password Password value
     * @param {String} phone Phone value
     *
     * @returns {boolean}
     */
    function setStorage (name, email, region, password, phone) {
        storage.set('name', name);
        storage.set('email', email);
        storage.set('region', region);
        storage.set('password', password);
        storage.set('phone', phone);

        return true;
    }

    /**
     * Returns user values from localStorage
     *
     * @returns {{name: String, email: String, password: String, region: String, shipping: String, address: String, phone: String}}
     */
    function getUser () {
        var fields, i, user;

        user = {};

        fields = ['name', 'email', 'password', 'region', 'shipping', 'address', 'phone'];

        for (i = 0; i < fields.length; ++i) {
            user[fields[i]] = common.storage.get(fields[i]) || '';
        }

        return user;
    }

    var storage;

    storage = $.localStorage;

    common.storage = storage;
    common.checkStorage = checkStorage;
    common.setStorage = setStorage;
    common.getUser = getUser;
})();
