(function () {
    var maxCount, i, key, products, ids, m, $name, id, minPrice, $prices, $viewedScope, $products;

    if (!('localStorage' in window)) {
        return;
    }

    maxCount = 20;

    key = 'viewed-products';

    ids = [];

    products = localStorage.getItem(key);

    products = products ? JSON.parse(products) : [];

    for (i = 0; i < products.length; ++i) {
        ids.push(products[i].id);
    }

    if (
        (m = /^\/product\/([a-z0-9_-]+)\/?$/i.exec(location.pathname))
        && ($name = $('h1')).length
        && (id = parseInt($name.attr('data-id'), 10)) > 0
        && ids.indexOf(id) === -1
    ) {
        minPrice = null;

        $prices = $('.row.product .in-stock .p-price');

        if (!$prices.length) {
            $prices = $('.row.product .p-price');
        }

        $prices.each(function () {
            var price;

            price = parseInt(this.innerHTML.replace(' ', ''), 10);

            if (price < minPrice || minPrice === null) {
                minPrice = price;
            }
        });

        if (products.length === maxCount) {
            products = products.slice(0, maxCount - 1);
        }

        products = [{
            id: id,
            url: m[1],
            name: $name.html(),
            img: $('.product .main_img')[0].hasAttribute('itemprop') ? 1 : 0,
            price: minPrice
        }].concat(products);

        localStorage.setItem(key, JSON.stringify(products));
    }

    common.ng.app.controller('viewed', ['$scope', function ($scope) {
        $scope.products = products;

        $viewedScope = $scope;
    }]);

    common.ng.app.directive('viewedDirective', function () {
        return function($scope) {
            var $productsBox;

            if ($scope.$last) {
                $productsBox = $products.find('.products-box');

                if ($productsBox.width() < products.length * 220) {
                    $productsBox.jcarousel();

                    $products.find('.arrow')
                        .show()
                        .on('jcarouselcontrol:inactive', function() {
                            $(this).addClass('inactive');
                        })
                        .on('jcarouselcontrol:active', function() {
                            $(this).removeClass('inactive');
                        });

                    $products.find('.prev')
                        .jcarouselControl({
                        target: '-=3'
                    });

                    $products.find('.next')
                        .jcarouselControl({
                        target: '+=3'
                    });
                }
            }
        };
    });

    if (products.length > 0) {
        $products = $('#viewed-products');

        $('#toggle-viewed').click(function () {
            $products.toggleClass('dnone');

            return false;
        });

        $products.find('.close').click(function () {
            $products.toggleClass('dnone');

            return false;
        });

        $products.find('.clear').click(function () {
            localStorage.removeItem(key);

            products = ids = [];

            $viewedScope.products = products;
            $viewedScope.$apply();

            $products.toggleClass('dnone');

            return false;
        });
    }
})();
