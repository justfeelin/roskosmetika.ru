(function () {
    var productPackID;

    common.$document.on('click', '.ua-product-link', function () {
        return common.uaProductClick($(this));
    });

    if (common.productPage) {
        productPackID = document.getElementsByTagName('h1')[0].getAttribute('data-id');

        ga('UniversalAnalytics.ec:addProduct', {
            id: productPackID,
            name: document.getElementById('product-name').innerHTML,
            brand: document.getElementById('product-tm').getAttribute('href').substr(4),
            variant: $('.one_pack[data-pack-id="' + productPackID + '"]').find('.pack').text()
        });

        ga('UniversalAnalytics.ec:setAction', 'detail');

        ga('MainUATracker.ec:addProduct', {
            id: productPackID,
            name: document.getElementById('product-name').innerHTML,
            brand: document.getElementById('product-tm').getAttribute('href').substr(4),
            variant: $('.one_pack[data-pack-id="' + productPackID + '"]').find('.pack').text()
        });

        ga('MainUATracker.ec:setAction', 'detail');

        common.productPage = true;
    }

    $('.ua-product-list').each(function () {
        common.uaProductList($(this), null, common.listPage);
    });
})();

$(function () {
    if (!common.gaPaveviewSent) {
        common.ABTests.init();

        common.gaPageview();
    }
});
