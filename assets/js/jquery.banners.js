'use strict';
/**
 * Включает автоматичский роутинг баннеров.
 * @param  {String} banners Класс контейнер для меню баннеров.
 */
$.fn.banners = function (banners) {

    var timeOut = 3000;
    var timeOutId;
    var activeClass = "active_banner_menu";

    var menuItems = $(banners + ' p');
    var activeItem = menuItems.first();

    var bannersContainer = $('.banner');

    var $bannersCopyAreas;

    /**
     * Находит следующий баннер и деляет его активным.
     * @return {Function} [description]
     */
    var next = function () {
        setActive(activeItem.next().length ? activeItem.next() : menuItems.first());
        timeOutId = setTimeout(next, timeOut);
    };

    /**
     * Устанавливаем новый активный баннер
     * @param {jQuery} newActive Ссылка на новый активный баннер
     */
    var setActive = function (newActive) {
        // а тут работаем с картинками
        var activeImg = newActive.attr('class');

        $('.banner img').fadeOut();
        $('.banner map').hide();

        $('.banner-copy-area')
            .hide();

        $('.banner img.' + activeImg).fadeIn();
        $('.banner map.' + activeImg).show();

        $('.banner-copy-area.' + activeImg)
            .show();

        // Тут работаем с менюшкой
        activeItem.removeClass(activeClass);
        newActive.addClass(activeClass);
        activeItem = newActive;
    };

    /**
     * Обработчик клика по меню баннеров
     */
    menuItems.click(function () {
        var $this = $(this);

        if (!$this.hasClass(activeClass)) {
            setActive($this);
        }
    });

    /**
     * Отключаем автоматическое переключение баннеров при наведелние мыши.
     */
    bannersContainer.mouseover(function () {
        clearTimeout(timeOutId);
        timeOutId = null;
    });

    /**
     * Включаем автоматическое переключение баннеров при уходе мыши с баннеров.
     */
    bannersContainer.mouseout(function () {
        if (!timeOutId) {
            timeOutId = setTimeout(next, timeOut);
        }
    });

    timeOutId = setTimeout(next, timeOut);

    $bannersCopyAreas = $('.banner-copy-area');

    if ($bannersCopyAreas.length) {
        new ZeroClipboard($bannersCopyAreas);
    }

    return this;
};