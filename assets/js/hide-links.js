/**
 * Links encryption client implementation
 */
(function () {
    /**
     * Returns decoded link
     *
     * @param {String} encodedLink Encoded link
     *
     * @returns {String}
     */
    function decodeLink(encodedLink) {
        var i, codes, decodedLink = "", offset;

        codes = encodedLink.split(".");

        for (i = 0; i < codes.length; ++i) {
            if (i === 0) {
                offset = parseInt(codes[i], 10);
                continue;
            }

            decodedLink += String.fromCharCode(parseInt(codes[i], 10) - offset);
        }

        return decodedLink;
    }

    common.decodeLink = decodeLink;
})();

$(function () {
    common.$document.on('click', '.hidden_link:not(.ua-product-link)', function () {
        var encodedLink, decodedLink;

        if (!this.hasAttribute("data-link") || !(encodedLink = this.getAttribute("data-link"))) {
            return;
        }

        decodedLink = common.decodeLink(encodedLink);

        this.removeAttribute("rel");
        this.setAttribute("href", decodedLink);
    });
});
