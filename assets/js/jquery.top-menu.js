/**
 * Логика верхнего меню.
 */
$.fn.topMenu = function () {
    'use strict';

    var menu = $('.menu-container');
    var menuItems = menu.find('.menu__item-link');
    var submenu = $('.sub_menu');
    var fixedTopMenu = $('.menu-fixed-top');
    var fixedTopMenuContainer = $('.menu-fixed-top-container');
    var staticMenu = $('.menu-static');
    var headerHeight = $('.header_menu').outerHeight();
    var footMenuHeight = $('.account_menu').outerHeight();
    var menu_item = $('.menu__item');
    var menuHeight = menu.outerHeight();
    // Величина прокрутки подмею (чтобы не прыгало прокрутна подменю при прилипании)
    var submenuScroll;
    // величина зазора между подменю и нижним меню.
    var GAP = 5,
        $logo = $('.logo');

    /**
     * Изменяет высоту видимого окно подменю.
     */
    var resizeSubmenu = function () {
        var windowHeight = common.$window.height();
        var windowScroll = common.$window.scrollTop();

        var newHeight = windowHeight - menuHeight - footMenuHeight - GAP;

        if (headerHeight > windowScroll) {
            newHeight = newHeight - headerHeight + windowScroll;
        }

        submenu.filter(':visible').css('max-height', newHeight);
    };

    /**
     * Поправляем скакание скрола у подменю при прилепании меню
     */
    var fixScroll = function () {
        submenu.filter(':visible').scrollTop(submenuScroll);
    };

    /**
     * Закрываем окно при потере фокуса мыши.
     */
    submenu.mouseleave(function () {
        submenu.hide();
        submenu.prev().removeClass('menu_active_group');
    });

    /**
     * Закрываем окно при потере фокуса c разделов основного меню
     */
    menu_item.mouseleave(function () {

        // если объект открыт
        if (submenu.length) {
            submenu.hide();
            submenu.prev().removeClass('menu_active_group');
        }
    });

    /**
     * Клик по категориям перовго уровня.
     */
    menuItems.mouseenter(function (event) {
        var self = $(this);
        var selfSubmenu = self.next('.sub_menu');

        // отменяем клик по умолчанию если не отображается подменю.
        if (self.next(':hidden').length) {
            event.preventDefault();
        }

        menuItems.removeClass('menu_active_group');
        submenu.hide();

        self.addClass('menu_active_group');
        selfSubmenu.show();

        var s = parseInt(selfSubmenu.offset().left + selfSubmenu.outerWidth());
        var m = parseInt(menu.offset().left + menu.outerWidth());

        if (s > m) {
            selfSubmenu.css('left', m - s + 4);
        }

        resizeSubmenu();
    });

    if (location.pathname === '/cart') {
        return this;
    }

    /**
     * Сохраняем величину прокрутки у подменю для исключения скакания
     * скролу у подменю при прилипании/отлипании меню.
     */
    submenu.scroll(function () {
        submenuScroll = $(this).scrollTop();
    });

    /**
     * Отслеживания скрола страницы для прилипания верхнего меню.
     */
    common.$window.scroll(function () {

        var scrollY;

        if(!window.Mobi) {

            scrollY = common.$window.scrollTop();
            $logo.toggleClass('scrolled', scrollY >= 13);

            if ((scrollY > headerHeight)) {
                $logo
                    .css('margin-top', '0px');

                if (fixedTopMenu.filter(':hidden').length) {
                    
                    fixedTopMenu.show();
                    fixedTopMenuContainer.append(menu);
                    resizeSubmenu();
                    fixScroll();
                }
            } else {
                $logo.css('margin-top', '');

                if (fixedTopMenu.filter(':visible').length) {

                    fixedTopMenu.hide();
                    staticMenu.append(menu);
                    fixScroll();

                } else {
                    resizeSubmenu();
                }
            }

        }    

    })
    /**
     * Изменяем размеры подменю при изменение размера окна браузера.
     */
        .resize(function () {
            resizeSubmenu();
        });

    return this;
};