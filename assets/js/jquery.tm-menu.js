'use strict';
$.fn.tmMenu = function () {
    /**
     * Toggles submenu block
     *
     * @param {jQuery} $pointer Pointer element
     * @param {jQuery} $tmSubmenu Submenu element
     */
    function toggleBlock($pointer, $tmSubmenu) {
        $pointer
            .toggleClass('tm_pointer_active');

        $tmSubmenu
            .toggle();
    }

    $('.tm-menu_all')
        .each(function () {
            var $allTm, $holder,
                $inCategoryBlock,
                $tmSubmenu, $pointer;

            function _toggleBlock() {
                toggleBlock($pointer, $tmSubmenu);
            }

            $allTm = $(this);

            $holder = $allTm
                .closest('.tms-box');

            $tmSubmenu = $holder
                .find('.sub_tm_menu');

            $pointer = $holder
                .find('.pointer');

            $inCategoryBlock = $allTm
                .closest('.tms');

            if ($inCategoryBlock.length === 1) {
                $inCategoryBlock
                    .hover(_toggleBlock);
            } else {
                $allTm
                    .click(_toggleBlock);

                $tmSubmenu
                    .mouseleave(_toggleBlock);
            }
        });

    return this;
};
