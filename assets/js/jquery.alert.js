/**
 * Отображает окно alert.
 */
(function ($) {
    'use strict';

    /**
     * Displays alert message
     *
     * @param {String} msg Alert message body
     * @param {String} [head] Alert header
     * @param {Boolean} [asHtml] Use message body as HTML
     */
    $.alert = function (msg, head, asHtml, reload) {
        head = head || '&nbsp;';

        var modal = $('#alert-modal');

        modal.find('.alert-head').html(head);
        modal.find('.alert-msg')[asHtml ? 'html' : 'text'](msg);

        modal.modal('show');

        if (reload) {
            modal.on('hidden.bs.modal', function () {
                 document.location.reload();
            })
        } 
    };
})(jQuery);