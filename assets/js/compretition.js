'use strict';

$.fn.competition = function (main_selector) {
    $('.auth').on('click', main_selector, function (e) {
        var btn = $(this);
        btn.html('Регистрируем<span class="think"></span>');

        var e_mail = $(this).parents('.competition').find('input[name=e_mail]').val();
        var phone = $(this).parents('.competition').find('input[name=phone]').val();
        var name = $(this).parents('.competition').find('input[name=name]').val();

        if (!(/^[._a-zA-Z\d-]+@[.a-zA-Z\d-]+.[a-zA-Z]{2,6}$/.e_mail)) {

            $.alert('<p>Ошибка! Неверный электронный адрес.</p>', 'Участие в акции', true);
            return false;
        }
        if (phone = '') {
            $.alert('<p>Ошибка! Не задан телефон.</p>', 'Участие в акции', true);
            return false;
        }
        if (name = '') {
            $.alert('<p>Ошибка! Не задано имя.</p>', 'Участие в акции', true);
            return false;
        }

        var client_id = "<?php echo $user_id ?>";
        var comp_id = "<?php echo $competition['id'] ?>";

        $.ajax({
            url: '/competition/subscribe',
            data: {
                e_mail: e_mail,
                phone: phone,
                name: name,
                client_id: client_id,
                comp_id: comp_id
            },
            method: 'POST',
            dataType: 'json',
            timeout: 6000,
            success: function (result) {

                if (result.success) {
                    $('.competition')
                        .fadeOut(function () {
                            $.alert('<p>Вы успешно зарегистрировались!</p>', 'Участие в акции', true);
                        });

                } else {
                    if (result.email_error !== 0) {
                        $('input[name=e_mail]').after('<p class="form_error">' + result.email_error + '</p>');
                        $('input[name=e_mail]').parent().addClass('has-error');
                    }

                    if (result.all_error !== 0) {
                        $('input[name=e_mail]').after('<p class="form_error">' + result.all_error + '</p>');
                        $('input[name=e_mail]').parent().addClass('has-error');

                    }
                    // $('.competition')
                    //     .fadeOut(function () {
                    //         $.alert('<p>Ошибка регистрации!</p>', 'Участие в акции', true);
                    //     });
                }

                btn.text('Учавствовать');
            },
            error: function () {
                $.alert(data.msg);
            }
        });

    });
        return this;
};