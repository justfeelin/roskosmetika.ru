'use strict';


$.fn.prof = function () {

    var phone = 0;
    var email = 0;

    if(common.storage.get('phone')){
       phone = common.storage.get('phone'); 
    }
               
    if (common.storage.get('email')) {
       email = common.storage.get('email'); 
    } 

    var dat = {'phone': phone, 'email':email}

    $.ajax({
        url: '/prof/tm_prof_frontend',
        timeout: 6000,
        type: 'POST',
        dataType: 'json',
        data: dat,

        success: function (data) {
            if(data.msg == 'ok_prof') {
                document.location.reload();
            }
        }
    });

    return this;
};

/**
 * Add manual cookie for prof
 * @param  string prof_manual DOM-element for click            [
 */
$.fn.prof_manual = function (prof_manual) {

    $(prof_manual).click(function () {

    $("body").css("opacity", "0.5");
        
            $.ajax({
                url: '/prof',
                timeout: 6000,
                type: 'POST',
                dataType: 'json',
                data: {'prof': 1},
                success: function (data) {

                        $("body").css("opacity", "1");
                        $.alert(data.msg, "Ассортимент магазина", true, true);

                },
                error: function () {
                    $("body").css("opacity", "1");
                    $.alert(msg.conError, 'Ошибка');
                }
            });

    });

    return this;
};