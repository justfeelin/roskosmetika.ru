'use strict';
/**
 * Переключаем табы у товаров.
 */
$.fn.specialGroups = function () {
    var tabsItems = $('.special-menu__item-text');

    if (tabsItems.length !== 0) {
        common.uaProductList($('.special_prods'), 'Главная - ' + tabsItems[0].innerHTML);

        common.ABTests.init();

        common.gaPageview();
    }

    return this;
};
