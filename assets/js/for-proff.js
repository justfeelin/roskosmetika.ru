$(function () {
    var $modal, $form, $error;

    $modal = $('#for-proff-modal');

    $error = $modal.find('.error').hide();

    common.$document.on('click', '.for_proff', function () {
        var prod_id;

        prod_id = this.getAttribute('data-prod-id');

        $modal.find('.pre_p_name').text(this.getAttribute('data-prod-name'));
        $modal.find('.pre_p_img img').attr('src', common.IMG_DOMAIN + '/images/prod_photo/' + common.imgUrl((this.getAttribute('data-prod-img') ? 'min_' + prod_id : 'none') + '.jpg', this.getAttribute('data-prod-url')));
        $modal.find('.prod-id').val(prod_id);
        $modal.find('.pack-id').val(this.getAttribute('data-pack-id'));

        common.formStorageValues($modal, true);

        $modal.find('.form-group').removeClass('has-error');
        $modal.find('.form_error').remove();

        common.formStorageValues($modal, true);

        $modal.modal('show');

        return false;
    });

    $form = $modal.find('form').submit(function () {
        $.post(
            '/registration/for_proff',
            $form.serialize(),
            function (o) {
                if (o !== true) {
                    $error.html(o).show();
                } else {
                    $error.hide();

                    common.formStorageValues($modal);

                    $modal.modal('hide');

                    $.alert('Цена товара отправлена на указанный E-mail');
                }
            },
            'json'
        );

       return false;
    });
});
