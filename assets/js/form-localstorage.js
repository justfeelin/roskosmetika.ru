(function () {
    /**
     * Form localStorage getter/setter
     *
     * @param {jQuery} $form Form element
     * @param {Object|Boolean} [values] Values to be used for form's inputs values. If not passed - current values will be saved to localStorage. If true - values will be loaded from localStorage
     */
    function formStorageValues($form, values) {
        var i, inputs, $input, value;

        if (values === true) {
            values = common.getUser();
        }

        inputs = ['name', 'email', 'phone', 'password', 'region', 'shipping', 'address'];

        for (i = 0; i < inputs.length; ++i) {
            $input = $form.find('.l-' + inputs[i]).filter('input, select, textarea');

            if ($input.length) {
                if (values) {
                    $input.val(values[inputs[i]]);
                } else {
                    value = $input.val();

                    if (!!value) {
                        common.storage.set(inputs[i], value);
                    }
                }
            }
        }
    }

    common.formStorageValues = formStorageValues;
})();
