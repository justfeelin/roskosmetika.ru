$(function () {
    var $countries, $tms, $clear, $countriesLink;

    /**
     * Resets countries filter
     */
    function resetCountries() {
        $countries
            .removeClass('vis');

        $tms
            .addClass('vis');

        $countriesLink.addClass('vis');

        $('#all-tm .brand:not(:visible)')
            .show();

        $('#all-tm .country_link:not(:visible)')
            .show();


        $clear
            .addClass('hidden');

        $('#all-tm .country_link')
            .each(function () {
                var text = $(this).find('span').attr('data-name');
                $(this).find('span').text(text);
            });
    }

    if (document.getElementById('all-tm')) {
        $tms = $('#all-tm .tm');
        $countriesLink = $('#all-tm .country_link');
        $countries = $('.countries .country')
            .click(function () {
                var $active;

                $(this)
                    .toggleClass('vis');

                $active = $countries
                    .filter('.vis');
                if ($active.length) {
                    $tms
                        .removeClass('vis');
                    $countriesLink
                        .removeClass('vis');
                    $active
                        .each(function () {
                            $tms
                                .filter('.c' + this.getAttribute('data-i'))
                                .addClass('vis');
                            $countriesLink
                                .filter('.d' + this.getAttribute('data-i'))
                                .addClass('vis');
                        });

                    $('#all-tm .brand')
                        .each(function () {
                            var $this, visible;

                            $this = $(this);

                            visible = $this
                                .is(':visible');

                            if ($this.find('.tm.vis').length) {
                                if (!visible) {
                                    $this
                                        .show();
                                }
                            } else {
                                if (visible) {
                                    $this
                                        .hide();
                                }
                            }
                        });
                    $('#all-tm .country_link')
                        .each(function () {
                            var $this, visible;

                            $this = $(this);
                            debugger;
                            visible = $this
                                .is(':visible');
                            var text = $(this).find('span').attr('data-name');
                            text = "Вся "+ text.toLowerCase();
                            $(this).find('span').text(text);
                            if ($this.hasClass('vis')) {
                                if (!visible) {
                                    $this
                                        .show();
                                }
                            } else {
                                if (visible) {
                                    $this
                                        .hide();
                                }
                            }
                        });
                    $clear
                        .removeClass('hidden');
                } else {
                    resetCountries();
                }

                return false;
            });

        $clear = $('#clear-countries')
            .click(function () {
                resetCountries();
                return false;
            })
    }
});
