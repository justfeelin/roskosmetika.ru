$(function () {
    var $referralPage, $referralCode, code, zeroClipboardClient,
        $loginForm, $formButtons, $authSubmit, doLogin;

    $referralPage = $('#referral');

    if ($referralPage.length === 0) {
        return;
    }

    if (!$referralPage.hasClass('not-registered')) {
        $referralCode = $('#referral-code');

        $referralCode.focus(function () {
            if ($referralCode[0].select) {
                $referralCode[0].select();
            }
        })[0].focus();

        code = $referralCode
            .val();

        zeroClipboardClient = new ZeroClipboard($('#copy-code'));

        zeroClipboardClient
            .on('copy', function(event) {
                event.clipboardData.setData('text/plain', code);
            });
    } else {
        $loginForm = $referralPage
            .find('.login-form');

        $authSubmit = $loginForm
            .find('.login-button');

        doLogin = true;

        $formButtons = $loginForm
            .find('.switch a');

        $formButtons
            .click(function () {
                if ($(this).hasClass('active')) {
                    return false;
                }

                doLogin = !doLogin;

                $authSubmit
                    .toggleClass('dologin', doLogin)
                    .toggleClass('doregister', !doLogin);

                $authSubmit
                    .val(doLogin ? 'Вход' : 'Регистрация');

                $formButtons
                    .removeClass('active')
                    .filter('.' + (doLogin ? 'login' : 'registration'))
                    .addClass('active');

                $loginForm
                    .toggleClass('dologin', doLogin)
                    .toggleClass('doregister', !doLogin);

                return false;
            });

        $.fn.user_login('.login-button.dologin');
        $.fn.registration('.login-button.doregister');
    }
});
