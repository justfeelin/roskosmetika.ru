$(function () {
    $('.read_more_desc').each(function () {
        var $this, smallHeight, maxHeight, $buttonBox, $buttonWrap, $button, $arrow, expanded;

        $this = $(this);

        smallHeight = parseInt(this.getAttribute('data-size'), 10) || 145;

        maxHeight = $this.css('height', 'auto').height();

        $this.css('height', maxHeight <= smallHeight ? '' : smallHeight);

        if (maxHeight <= smallHeight) {
            return;
        }

        expanded = false;

        $buttonBox = $('<div>' +
            '<span class="read_more_gradient"></span>' +
            '<span class="read_more">' +
            '<span class="btn-title">Читать дальше</span>' +
            '<span class="read_more_chevron glyphicon glyphicon-chevron-down"></span>' +
            '</span>' +
            '</div>').insertAfter($this);

        $arrow = $buttonBox.find('.read_more_chevron');

        $buttonWrap = $buttonBox.find('.read_more');

        $button = $buttonWrap.find('.btn-title');

        $buttonWrap.click(function () {
            expanded = !expanded;

            $buttonWrap.toggleClass('read_more_down', expanded);
            $buttonBox.toggleClass('expanded', expanded);

            $arrow.toggleClass('glyphicon-chevron-down', !expanded);
            $arrow.toggleClass('glyphicon-chevron-up', expanded);

            $button.html(expanded ? 'Свернуть' : 'Читать&nbsp;дальше&nbsp;');

            $this.animate({
                height: expanded ? maxHeight : smallHeight
            });

            return false;
        });
    });
});
