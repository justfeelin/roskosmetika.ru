$(function () {
    var m, $anchor, newOffset;

    if (
        location.hash
        && (m = /^#([a-z0-9_-]+)$/i.exec(location.hash))
        && ($anchor = $('#' + m[1])).length
        && $anchor.hasClass('anchor')
    ) {
        newOffset = $anchor.offset().top - 100;

        if (newOffset < 0) {
            newOffset = 0;
        }

        setTimeout(function () {
            common.$window.scrollTop(newOffset);
        }, 500);
    }
});
