angular.element(document).ready(function () {

    window.onpopstate = function() {
        location.reload();
    };

    /**
     * Initiates pages click events handlers
     */
    function initPaginationLinks() {
        $paginationBox
            .find('.pagination a')
            .click(function () {
                common.listPage = parseInt(this.getAttribute('data-page'), 10);

                window.history.pushState(null,null, document.location.href);
                common.showAll = !common.listPage;

                updateProducts(true, true);

                return false;
            });

        $paginationBox
            .find('.on-page a')
            .click(function () {
                common.listPage = 1;

                common.productsOnPage = parseInt(this.innerHTML, 10);
                window.history.pushState(null,null, document.location.href);

                updateProducts(true, true);

                return false;
            });
    }

    /**
     * Marks active filter groups buttons
     */
    function updateFilterButtons() {
        $filterGroups
            .each(function (i) {
                var $group;

                $group = $filterGroups
                    .eq(i);

                $group
                    .toggleClass(
                        'dropdown-check',
                        $group
                            .find('.' + activeClass)
                            .length !== 0
                    );

                $group
                    .toggleClass(
                        'unavailable',
                        $group
                            .find('.available')
                            .length === 0
                    );
            });

        $priceRangeBox
            .toggleClass('dropdown-check', !edgePrices(true));
    }

    /**
     * Serializes filter form data, stores result in formData
     */
    function serializeForm() {
        formData = $form.serialize();
    }

    /**
     * Returns whether selected price range covers minimum && maxium values
     *
     * @param {Boolean} [returnSummary] Return whether both lower & upper price are on edges. If false - will return array of flags ([lowerEdge, upperEdge])
     *
     * @returns {Boolean|Array}
     */
    function edgePrices(returnSummary) {
        var lowerEdge, upperEdge;

        lowerEdge = priceValues[0] === priceRange.min;
        upperEdge = priceValues[1] === priceRange.max;

        return returnSummary
            ? lowerEdge && upperEdge
            : [
            lowerEdge,
            upperEdge
        ];
    }

    /**
     * Sets price range inputs names (If not edge values)
     */
    function setPriceRangeNames() {
        var edged;

        edged = edgePrices();

        if (edged[0]) {
            $priceFromLabel
                .removeAttr('name');
        } else {
            $priceFromLabel
                .attr(
                    'name',
                    $priceFromLabel
                        .attr('data-name')
                );
        }

        if (edged[1]) {
            $priceToLabel
                .removeAttr('name');
        } else {
            $priceToLabel
                .attr(
                    'name',
                    $priceToLabel
                        .attr('data-name')
                );
        }
    }

    /**
     * Updates hash & loads filtered products
     *
     * @param {Boolean} [immediately] Perform update immediately, without delay
     * @param {Boolean} [scrollToTop] Scroll to top of products list after update
     */
    function updateProducts(immediately, scrollToTop) {
        if (updateTO) {
            clearTimeout(updateTO);
        }

        updateTO = setTimeout(function () {
            var data, newUrl,
                urlParams, hashParams,
                $loadingBoxes;

            urlParams = [];
            hashParams = formData ? [formData] : [];

            if (search) {
                urlParams.push('search=' + search);
            }

            if (common.showAll) {
                urlParams.push('showAll');
            } else {
                if (common.listPage > 1) {
                    urlParams.push('p=' + common.listPage);
                }
            }

            if (sort) { 
                hashParams.push('s=' + sort + (!sortAscending ? '&sort-desc' : ''));
            }

            if (common.productsOnPage !== common.defaultProductsOnPage) {
                urlParams.push('onp=' + common.productsOnPage);
            }

            newUrl = canonicalUrl + (urlParams.length !== 0 ? (canonicalUrl.indexOf('?') === -1 ? '?' : '&') + urlParams.join('&') : '') + (hashParams.length !== 0 ? '#' + decodeURIComponent(hashParams.join('&')) : '');

            if (newUrl === currentUrl) {
                return;
            }

            $clearFilters.toggle(!!(urlParams.length !== 0 || hashParams.length !== 0));

            if (location.href !== newUrl && window.history && history.replaceState) {
                history.replaceState(undefined, undefined, newUrl);
            }

            data = [];

            if (urlParams.length !== 0) {
                data = data.concat(urlParams);
            }

            if (hashParams.length !== 0) {
                data = data.concat(hashParams);
            }

            data.push('_ajax=' + Math.random());//Prevent Chrome from returning cached JSON result on back button press

            $loadingBoxes = $('.products-loading')
                .addClass('loading');

            //TODO cache results?

            $.get(
                newUrl,
                data.join('&'),
                /**
                 * @param {{found: Boolean, products: String, pagination: String, filters: Array, tms: Array, countries: Array}} o
                 */
                function (o) {
                    var offset, productsOffset, $foundBox, Interval,
                        priceEdges,
                        $productList;

                    $products.html(o.products ? common.ng.$compile(o.products)(common.ng.$cartScope) : '');

                    $paginationBox.html(o.pagination ? o.pagination : '');

                    $loadingBoxes
                        .removeClass('loading');

                    currentUrl = newUrl;

                    $foundBox = document.getElementById('foundCount');

                    if ($foundBox) {
                        $foundBox.innerHTML = o.count ? o.count : ' ';
                    }

                    $filters
                        .addClass('available');

                    if (o.filters) {
                        $filters
                            .not('.tm, .cid')
                            .removeClass('available')
                            .filter(
                                o.filters.map(function (filterID) {
                                    return '.f' + filterID;
                                })
                                .join(',')
                            )
                            .addClass('available');
                    }

                    updateFilterButtons();

                    if (o.tms) {
                        $filters
                            .filter('.tm')
                            .removeClass('available')
                            .filter(
                                o.tms.map(function (filterID) {
                                    return '.tm' + filterID;
                                })
                                .join(',')
                            )
                            .addClass('available');
                    }

                    if (o.countries) {
                        $filters
                            .filter('.cid')
                            .removeClass('available')
                            .filter(
                                o
                                    .countries
                                    .map(function (filterID) {
                                        return '.cid' + filterID;
                                    })
                                    .join(',')
                            )
                            .addClass('available');
                    }

                    if (o.categories) {
                        $filters
                            .filter('.cat')
                            .removeClass('available')
                            .filter(
                                o.categories.map(function (filterID) {
                                    return '.cat' + filterID;
                                })
                                    .join(',')
                            )
                            .addClass('available');
                    }

                    if (o.found) {
                        $productList = $('.product-list');
                        
                        initPaginationLinks();

                        common.uaProductList($productList, null, common.listPage);

                        common.gaPageview();

                        common.preOrderPopoverInit();

                        common.packsHoverInit($productList);
                    }

                    if (o.prices && $priceRangeElement !== null) {
                        priceEdges = edgePrices();

                        if (o.prices[0] !== priceRange.min) {
                            priceRange.min = o.prices[0];
                        }

                        if (o.prices[1] !== priceRange.max) {
                            priceRange.max = o.prices[1];
                        }

                        if (priceValues[0] < priceRange.min || priceEdges[0]) {
                            priceValues[0] = priceRange.min;
                        }

                        if (priceValues[1] > priceRange.max || priceEdges[1]) {
                            priceValues[1] = priceRange.max;
                        }

                        $priceRangeElement[0]
                            .noUiSlider
                            .updateOptions(
                                {
                                    range: priceRange.min === priceRange.max
                                        ? {
                                            min: priceRange.min,
                                            max: priceRange.min + 1
                                        }
                                        : priceRange
                                }
                            );

                        $priceRangeElement[0]
                            .noUiSlider
                            .set(priceValues);
                    }

                    if (scrollToTop) {
                        offset = common.$window.scrollTop();

                        productsOffset = $products.offset().top - 200;

                        if (offset > productsOffset) {
                            Interval = setInterval(function () {
                                offset -= 100;

                                if (offset < productsOffset) {
                                    offset = productsOffset;
                                }

                                common.$window.scrollTop(offset);

                                if (offset === productsOffset) {
                                    clearInterval(Interval);
                                }
                            }, 1);
                        }
                    }

                    if (o.show_desc !== undefined) {

                        var descr_box = $('.listing_desc div');

                        if (o.show_desc && (descr_box.length === 0)) {
                            $('.listing_desc').append('<div itemprop="description" data-size="145">' + o.show_desc + '</div>');
                        } else {
                            if(descr_box.length > 0) {
                                descr_box.remove();
                            }
                        }

                    }
                },
                "json"
            ).error(function () {
                $loadingBoxes
                    .removeClass('loading');
            });
        }, immediately ? 0 : 400);
    }

    var $form, formData, $filters, $clearFilters, $filterGroups,
        $products,
        $canonical, canonicalUrl, currentUrl,
        activeClass, updateTO, applyFilter,
        sort, sortAscending,
        $paginationBox,
        match,
        search, encodedSearch,
        $priceRangeElement, $priceFromLabel, $priceToLabel, priceValues, priceRange, priceTO, $priceRangeBox;

    $form = $("#filter-form");

    if (!$form.length) {
        return;
    }

    $clearFilters = $('.remove_filters');

    $filterGroups = $('.filter-group');

    search = location.pathname === '/search' ? $.trim(document.getElementById('search-input').value) : '';

    encodedSearch = search ? encodeURIComponent(search) : '';

    formData = '';

    $canonical = $("link[rel='canonical']");

    canonicalUrl = $canonical.length === 1 ? $canonical.attr("href") : location.pathname;

    $products = $(".product-list");

    activeClass = "active_filter";

    $filters = $form.find(".fi");

    updateTO = 0;

    applyFilter = false;

    $paginationBox = $('.pagination-box');

    $('.sort-link').each(function () {
        var $this, $siblings, currentSort, ascending,
            hashActive,
            defaultAscending;

        /**
         * Sets sort link active
         */
        function setActive() {
            sort = currentSort;

            sortAscending = ascending;

            $this
                .addClass('active-sort')
                .toggleClass('down', !ascending)
                .toggleClass('up', ascending);

            $siblings.removeClass('active-sort');
        }

        $this = $(this);

        defaultAscending = !$this.hasClass('up');

        $siblings = $this.siblings();

        currentSort = $this.attr('data-sort');

        hashActive = (new RegExp('(?:#|&)s=' + currentSort)).test(location.hash);

        if (hashActive || $this.hasClass('active-sort')) {
            ascending = hashActive ? !/sort-desc/.test(location.hash) : !defaultAscending;

            setActive();

            if (!$this.hasClass('default-sort') || !ascending) {
                applyFilter = true;
            }
        } else {
            ascending = true;
        }

        $this.click(function () {
            ascending = sort === currentSort ? !ascending : defaultAscending;

            setActive();

            common.listPage = 1;

            updateFilterButtons();

            serializeForm();

            updateProducts(true);

            return false;
        });
    });

    $filters.each(function () {
        /**
         * Toggles filter active status
         */
        function updateActive() {
            $filter.toggleClass(activeClass, active);

            if (active) {
                $input.attr("name", name);
            } else {
                $input.removeAttr("name");
            }
        }

        var $filter, $input, active, name,
            isTM, isCountry, isCategory;

        /**
         * Sets new active state
         *
         * @param {Boolean} newState New state value
         */
        this.setActive = function (newState) {
            active = newState;

            updateActive();
        };

        $filter = $(this);

        $input = $filter.find(".fin");

        name = $input.attr("data-name");

        isTM = $filter.hasClass('tm');
        isCountry = isTM ? false : $filter.hasClass('cid');
        isCategory = isCountry ? false : $filter.hasClass('cat');


        active = (new RegExp((isTM ? 't' : (isCountry ? 'cid' : (isCategory ? 'c' : 'f'))) + '\\[\\]=' + $input.val() + '(?:$|&)')).test(location.hash);

        if (active) {
            updateActive();

            applyFilter = true;
        }

        $filter.click(function () {
            if (!$filter.hasClass('available')) {
                return false;
            }

            this.setActive(!active);

            common.listPage = 1;

            updateFilterButtons();

            serializeForm();

            updateProducts(true);
        });
    });

    $clearFilters.click(function () {
        $filters.each(function () {
            this.setActive(false);
        });

        common.listPage = 1;

        updateFilterButtons();

        serializeForm();

        updateProducts();

        return false;
    });

    $('.clear-group').click(function () {
        $(this)
            .closest('.filter-group')
            .find('.fi')
            .each(function() {
                this
                    .setActive(false);
            });

        updateFilterButtons();

        serializeForm();

        updateProducts(true);

        return false;
    });

    common.showAll = !common.productPage && !common.cartPage && /(?:&|\?)showAll(?:&|$)/.test(location.search);

    if (!common.showAll && !common.productPage && !common.cartPage && (match = /(?:&|\?)p=(\d+)(?:&|$)/.exec(location.search))) {
        common.listPage = parseInt(match[1], 10);
    } else {
        common.listPage = 1;
    }

    if (match = /(?:&|\?)onp=(\d+)(?:&|$)/.exec(location.search)) {
        common.productsOnPage = parseInt(match[1], 10);
    }

    if (match = /(?:#|&)s=([a-z]+)(?:&|$)/.exec(location.hash)) {
        sort = match[1];

        sortAscending = !/(?:#|&)sort-desc(?:&|$)/.test(location.hash);
    }

    currentUrl = canonicalUrl + (common.showAll ? '?showAll' : (common.listPage > 1 ? '?p=' + common.listPage : ''));

    $priceRangeElement = $('#price-range');

    if ($priceRangeElement.length === 1) {
        priceTO = 0;

        priceValues = [
            parseInt($priceRangeElement.attr('data-from'), 10),
            parseInt($priceRangeElement.attr('data-to'), 10)
        ];

        priceRange = {
            min: priceValues[0],
            max: priceValues[1]
        };

        if ((match = /(?:#|&)pr\[0\]=(\d+)(?:&|$)/.exec(location.hash)) && match[1] != priceRange.min) {
            priceValues[0] = parseInt(match[1], 10);

            applyFilter = true;
        }

        if ((match = /(?:#|&)pr\[1\]=(\d+)(?:&|$)/.exec(location.hash)) && match[1] != priceRange.max) {
            priceValues[1] = parseInt(match[1], 10);

            applyFilter = true;
        }

        noUiSlider
            .create(
                $priceRangeElement[0],
                {
                    start: priceValues,
                    connect: true,
                    range: priceRange.min === priceRange.max
                        ? {
                            min: priceRange.min,
                            max: priceRange.min + 1
                        }
                        : priceRange
                }
            );

        $priceFromLabel = $('#price-from')
            .appendTo(
                $priceRangeElement
                    .find('.noUi-handle-lower')
            );

        $priceToLabel = $('#price-to')
            .appendTo(
                $priceRangeElement
                    .find('.noUi-handle-upper')
            );

        setPriceRangeNames();

        $priceRangeBox = $('#price-range-box');

        $priceRangeBox
            .find('.clear-group')
            .click(function () {
                if (edgePrices((true))) {
                    return false;
                }

                common.listPage = 1;

                priceValues = [
                    priceRange.min,
                    priceRange.max
                ];

                $priceRangeElement[0]
                    .noUiSlider
                    .set(priceValues);

                setPriceRangeNames();

                serializeForm();

                updateProducts(true);

                return false;
            });

        $priceRangeElement[0]
            .noUiSlider
            .on(
                'update',
                function (newValues) {
                    $priceFromLabel
                        .val(parseInt(newValues[0], 10));

                    $priceToLabel
                        .val(parseInt(newValues[1], 10));
                }
            );

        $priceRangeElement[0]
            .noUiSlider
            .on(
                'change',
                function (newValues) {
                    if (priceTO !== 0) {
                        clearTimeout(priceTO);
                    }

                    priceTO = setTimeout(
                        function () {
                            priceValues = [
                                parseInt(newValues[0], 10),
                                parseInt(newValues[1], 10)
                            ];

                            common.listPage = 1;

                            setPriceRangeNames();

                            priceTO = 0;

                            serializeForm();

                            updateProducts(true);
                        },
                        100
                    );
                }
            );
    } else {
        $priceRangeElement = null;
    }

    if (applyFilter) {
        updateFilterButtons();

        serializeForm();

        updateProducts(true);
    }

    initPaginationLinks();
});
