(function () {
    'use strict';

    var page_path, ajax_timeout, i, m,
        $productList,
        $foundLowerPriceModal, $foundLowerPriceScope,
        productPackID, productPackVisible,
        $productDescription, maxHeight;

    common.productPage = !!document.getElementById('product-page');

    common.cartPage = !!document.getElementById('cart-page');


    /**
     * Calls .mask() for phone input
     *
     * @param {jQuery} $input Phone input element
     */
    common.phoneInput = function ($input) {
        $input.mask(
            '+7? (999) 99999999',
            {
                autoclear: false
            }
        );
    };

    page_path = location.pathname;
    ajax_timeout = 20000;

    /* jshint camelcase: false, evil: true, multistr: true */
    /* exported msg */

    var msg = {
        conError: 'Нам очень стыдно, но что-то случилось с нашим сайтом, попробуйте попозже.'
    };

    /******************************************Yandex map block begin******************************************/

    var moscow_map;


    $('a[href="#tab2"]').on('click', function () {

        showCdekMap();
    });

    $("#cdek").change(function () {

        showCdekMap();
    });

    $("#courier").change(function () {

        var region = $("#courier").val();
        adDeliveryInfo(region, 2);

    });

    /**
     * Add cdek point on map
     */
    $("#cdek_point").change(function () {

        moscow_map.geoObjects.removeAll();

        var id_sdek = $("#cdek_point").val(), dat;

        dat = {'id_sdek': id_sdek};
        $.ajax({
            url: '/cdek/get_points',
            timeout: 6000,
            type: 'POST',
            data: dat,
            dataType: 'json',
            success: function (data) {

                var point = data.point;


                moscow_map.setCenter([Number(point.coord_y), Number(point.coord_x)], 14, {
                    checkZoomRange: true
                });


                moscow_map.geoObjects.add(new ymaps.Placemark([Number(point.coord_y), Number(point.coord_x)], {
                    balloonContent: '<div id="sdek-balloon">' +
                    '<h4>' + (point.type === 2 ? 'ПОСТАМАТ ' : '') + point.full_address + '</h4>' +
                    (point.name ? '<p><b>' + point.name + '</b></p>' : '') +
                    (point.phone ? '<p>Тел.: ' + point.phone + '</p>' : '') +
                    (point.work_time ? '<p>Режим работы: ' + point.work_time + '</p>' : '') +
                    '</div>'
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: '/images/heart' + '.png',
                    iconImageSize: [35, 35],
                    iconImageOffset: [-17, -35]
                }))

            }
        });

    });


    /**
     * Add prices delivery for cdek points and courier
     */
    function adDeliveryInfo(region, type) {

        var dat = {
            'region': region,
            'type': type
        }, name;

        if (type < 2) {
            name = '_cdek';
        } else {
            name = '_courier';
        }

        $.ajax({
            url: '/cdek/get_delivery_price',
            timeout: 6000,
            type: 'POST',
            data: dat,
            dataType: 'json',
            success: function (data) {
                var price = data.price,
                    text;
                if (price._1 === 77) {
                    if (type < 2) {
                        text = "<ul>\n" +
                            "                                        <li>заказов на&nbsp;сумму <strong>менее 4 000 рублей</strong>&nbsp;&mdash; " + price._0 + "&nbsp;рублей.\n" +
                            "                                        </li>\n" +
                            "                                        <li>заказов на&nbsp;сумму <strong>более 4 000 рублей</strong>&nbsp;&mdash;&nbsp; " + price._4000 + "\n" +
                            "                                            рублей.\n" +
                            "                                        </li>\n" +
                            "                                    </ul>";


                    } else {
                        text = "<ul>\n" +
                            "                                        <li>заказов на&nbsp;сумму <strong>менее 4 000 рублей</strong>&nbsp;&mdash; " + price._0 + "&nbsp;рублей.\n" +
                            "                                        </li>\n" +
                            "                                        <li>заказов на&nbsp;сумму <strong>от 4 000 до 5 500</strong>&nbsp;&mdash;&nbsp; " + price._4000 + "\n" +
                            "                                            рублей.\n" +
                            "                                        </li>\n" +
                            "\n" +
                            "                                        <li>заказов на&nbsp;сумму <strong>более 5 500 рублей</strong>&nbsp;&mdash;\n" +
                            "                                            <strong>бесплатно.</strong></li>\n" +
                            "                                    </ul>";
                    }

                } else if (price._1 === 78) {
                    if (type < 2) {
                        text = "<ul>\n" +
                            "                                        <li>заказов на&nbsp;сумму <strong>менее 4 000 рублей</strong>&nbsp;&mdash; " + price._0 + "&nbsp;рублей.\n" +
                            "                                        </li>\n" +
                            "                                        <li>заказов на&nbsp;сумму <strong>от 4 000 до 5 500</strong>&nbsp;&mdash;&nbsp; " + price._4000 + "\n" +
                            "                                            рублей.\n" +
                            "                                        </li>\n" +
                            "\n" +
                            "                                        <li>заказов на&nbsp;сумму <strong>более 5 500 рублей</strong>&nbsp;&mdash;\n" +
                            "                                            <strong>бесплатно.</strong></li>\n" +
                            "                                    </ul>";
                    } else {
                        text = " <ul>\n" +
                            "                                        <li>заказов на&nbsp;сумму <strong>менее 4 000 рублей</strong>&nbsp;&mdash; " + price._0 + "&nbsp;рублей.\n" +
                            "                                        </li>\n" +
                            "                                        <li>заказов на&nbsp;сумму <strong>от 4 000 до 5 500</strong>&nbsp;&mdash;&nbsp; " + price._4000 + "\n" +
                            "                                            рублей.\n" +
                            "                                        </li>\n" +
                            "\n" +
                            "                                        <li>заказов на&nbsp;сумму <strong>более 5 500 рублей</strong>&nbsp;&mdash;\n" +
                            "                                            " + price._5500 + " рублей.</li>\n" +
                            "\n" +
                            "                                    </ul>";
                    }
                } else {

                    text = "<ul>\n" +
                        "                                        <li>заказов на&nbsp;сумму <strong>менее 4 000 рублей</strong>&nbsp;&mdash; " + price._0 + "&nbsp;рублей.\n" +
                        "                                        </li>\n" +
                        "                                        <li>заказов на&nbsp;сумму <strong>более 4 000 рублей</strong>&nbsp;&mdash;&nbsp; " + price._4000 + "\n" +
                        "                                            рублей.\n" +
                        "                                        </li>\n" +
                        "                                    </ul>";
                }

                $("#deliv_mes" + name).html(text);
            }
        });

    }

    /**
     * Add cdek points on map and creating a list point address
     */

    function showCdekMap() {

        moscow_map.geoObjects.removeAll();

        $("#cdek_point").empty();

        var dat, region = $("#cdek").val(),dat_select2=[],flag=1;

        dat = {'cdek': region};


        adDeliveryInfo(region, 1);

        $.ajax({
            url: '/cdek/get_points',
            timeout: 6000,
            type: 'POST',
            data: dat,
            dataType: 'json',
            success: function (data) {

                var points = data.points;

                var myCollection = new ymaps.GeoObjectCollection();

                for (i = 0; i < points.length; ++i) {

                    (function (point) {
                        if(flag===1){
                            $('#cdek_point').prepend('<option value="' + point.id + '">' + point.full_address+','+point.metro_station + '</option>').attr("selected", "selected");
                            $('span[id="select2-chosen-3"]').empty().append(point.full_address+','+point.metro_station);
                            flag=0;
                        }else{
                        $('#cdek_point').prepend('<option value="' + point.id + '">' + point.full_address + ',' + point.metro_station + '</option>');}

                        myCollection.add(new ymaps.Placemark([Number(point.coord_y), Number(point.coord_x)], {
                            balloonContent: '<div id="sdek-balloon">' +
                            '<h4>' + (point.type === 2 ? 'ПОСТАМАТ ' : '') + point.full_address + '</h4>' +
                            (point.name ? '<p><b>' + point.name + '</b></p>' : '') +
                            (point.phone ? '<p>Тел.: ' + point.phone + '</p>' : '') +
                            (point.work_time ? '<p>Режим работы: ' + point.work_time + '</p>' : '') +
                            '</div>'
                        }, {
                            iconLayout: 'default#image',
                            iconImageHref: '/images/heart' + '.png',
                            iconImageSize: [35, 35],
                            iconImageOffset: [-17, -35]
                        }))

                    })(points[i]);
                }

               // $('span[id="select2-chosen-3"]').append(flag);

                moscow_map.geoObjects.add(myCollection);
                moscow_map.setBounds(myCollection.getBounds());
            }
        });
    }


    /**
     * Initialization select2, yamps and launch delivery prices for courier
     */
    $(document).ready(function () {

        if(document.location.pathname == '/page/delivery'){
            $(".delivery-select2").select2();
            $(".courier-select2").select2();
            $(".point-select2").select2();

            ymaps.ready(function () {
                moscow_map = new ymaps.Map("first_map", {
                    center: [55.76, 37.64],
                    zoom: 17
                });
            });

            var region = $("#courier").val();
            adDeliveryInfo(region, 2);
        }
    });

    /******************************************Yandex map block end******************************************/

    $(function () {
        var $recommended,
            $lvl3Groups, lvl3TO,
            $cartPopup, $cartPopupBadge, cartPopupTO, documentCartPopupTouch,
            $oneClickPopup,
            vars, pair, dt;

        /**
         * Shows cart popup
         */
        function showCartPopup() {
            $cartPopup && $cartPopup
                .stop()
                .fadeIn(200);
        }

        /**
         * Hides cart popup
         */
        function hideCartPopup() {
            if ($cartPopup && $cartPopup.is(':visible')) {
                $cartPopup
                    .stop()
                    .fadeOut(200);
            }
        }

        $().topMenu()
            .tmMenu()
            .show_diff_packs('.one_pack')
            .banners('.banner_menu')
            .prof()
            .competition('.reg_comp')
            .mc()
            .specialGroups()
            .auth_window()
            .hide_reg_login()
            .registration('.reg_btn')
            .subscribe('.subscribe_button')
            .user_login('.log_in_btn')
            .add_to_favourite()
            .articles_group('.articles_part')
            .search_groups('.search_group')
            .add_scroll_to_top()
            .empty_search('.search')

        ;

        $('.auth .auth-btn')
            .click(function () {
                $.get(
                    '/login/' + this.getAttribute('data-type'),
                    {
                        r: location.pathname + location.search + location.hash
                    },
                    function (url) {
                        if (url) {
                            location = (url);
                        }
                    }
                );

                return false;
            });

        var banner = $('.active_banner_menu').attr('class');
        banner = /banner_\d+/.exec(banner);
        $('.banner .' + banner).show();

        // add countdown
        if (page_path === '/' || (page_path.indexOf('/order/ok/') + 1)) {
            var end_cd = new Date(Number($('.action_end .end_cd').text()) * 1000);
            $('.action_end .cd_hour').countdown({until: end_cd, layout: '{hnn}'});
            $('.action_end .cd_min').countdown({until: end_cd, layout: '{mnn}'});
            $('.action_end .cd_sec').countdown({until: end_cd, layout: '{snn}'});
        }

        if ($('.filters').length && $('.product-list').length) {
            $('.product-list').css('min-height', $('.filters').height() + 100);
        }

        if (common.storage.get('email')) {
            $('.login_window [type="text"]').attr('value', common.storage.get('email'));
        } else {
            if (common.storage.get('phone'))
                $('.login_window [type="text"]').attr('value', common.storage.get('phone'));
        }

        if (common.storage.get('password')) {
            $('.login_window [type="password"]').attr('value', common.storage.get('password'));
        }

        //  Add bootstrap tooltip
        $('[data-toggle="tooltip"]').tooltip();

        /**
         * Initiates popover for pre cart icons
         */
        common.preOrderPopoverInit = function () {
            $('.pre_cart_title').popover({
                placement: "bottom",
                content: "Мы сообщим Вам о поступлении товара.<br>Время ожидания не более месяца.",
                trigger: "hover",
                html: true
            });
        };

        common.preOrderPopoverInit();

        if (page_path === '/cart' || page_path === '/room/delivery') {
            common.phoneInput($("#phone"));

            if (page_path === '/cart') {
                $('.ac_button').fadeOut();
            }
        }

        if (common.productPage) {
            $recommended = $('#recommended-products');

            if ($recommended.length && $recommended.find('.product-item').length > 3) {
                $recommended.jcarousel({
                    list: '.recommended-holder',
                    items: '.product-item'
                });

                $('#recommended-prev')
                    .jcarouselControl({
                        target: '-=4'
                    });

                $('#recommended-next')
                    .jcarouselControl({
                        target: '+=4'
                    });

                $('.recomended_products .arrow')
                    .show()
                    .on('jcarouselcontrol:inactive', function () {
                        $(this).addClass('inactive');
                    })
                    .on('jcarouselcontrol:active', function () {
                        $(this).removeClass('inactive');
                    });
            }
        }

        $('.ng-init').removeClass('ng-init');

        $lvl3Groups = $('.expandable .lvl_3_group')
            .filter('.filter-group')
            .each(function () {
                var $this,
                    windowWidth, itemWidth,
                    $pad, $lis,
                    columns, i, y, inColumn, yEnd,
                    $ul;

                $this = $(this)
                    .addClass('hover');

                if ($this.find('.lvl_3_block').height() > 400 && (windowWidth = common.$window.width() - 75) > 1000) {
                    $pad = $this
                        .find('.filters-pad');

                    $lis = $pad
                        .addClass('expanded-width')
                        .find('.filter > li');

                    itemWidth = $lis
                        .eq(0)
                        .outerWidth();

                    $lis
                        .appendTo('body');

                    columns = parseInt(windowWidth / itemWidth, 10);

                    inColumn = Math.ceil($lis.length / columns);

                    $pad
                        .find('.filter')
                        .remove();

                    for (i = 0, y = 0; i < columns; ++i) {
                        yEnd = y + inColumn;

                        if (yEnd > $lis.length - 1) {
                            yEnd = $lis.length - 1;
                        }

                        $ul = $('<ul class="filter">')
                            .append(
                                $lis
                                    .slice(y, yEnd)
                            )
                            .appendTo($pad);

                        y = yEnd;
                    }

                    $pad
                        .width(columns * itemWidth);
                }

                $this
                    .removeClass('hover');
            })
            .end()
            .hover(
                function () {
                    var $self;

                    $self = $(this);

                    if ($self.hasClass('unavailable')) {
                        return;
                    }

                    if (lvl3TO) {
                        clearTimeout(lvl3TO);
                    }

                    lvl3TO = setTimeout(
                        function () {
                            var offsetX, $box, $pad, padOffset, boxOffset,
                                padWidth, boxWidth;

                            $lvl3Groups
                                .removeClass('hover');

                            $self
                                .addClass('hover');

                            offsetX = $self
                                .data('menu-offset');

                            if (offsetX === undefined) {
                                $box = $self
                                    .closest('.filters-box');

                                $pad = $self
                                    .parent()
                                    .find('.filters-pad');

                                padWidth = $pad.outerWidth();
                                boxWidth = $box.outerWidth();

                                if (padWidth > boxWidth) {
                                    offsetX = -(padWidth - $self.offset().left - $self.outerWidth() + 60);
                                } else {
                                    padOffset = $pad.offset().left + padWidth;
                                    boxOffset = $box.offset().left + boxWidth;

                                    if (padOffset > boxOffset) {
                                        $pad
                                            .css('right', 0);

                                        padOffset = $pad.offset().left + $pad.outerWidth();

                                        offsetX = padOffset - boxOffset;
                                    } else {
                                        offsetX = '';
                                    }
                                }

                                $self
                                    .data('menu-offset', offsetX);

                                $pad
                                    .css('right', offsetX);
                            }
                        },
                        50
                    );
                },
                function () {
                    if (lvl3TO) {
                        clearTimeout(lvl3TO);
                    }

                    lvl3TO = setTimeout(
                        function () {
                            $lvl3Groups
                                .removeClass('hover');
                        },
                        200
                    );
                }
            );

        if (!common.cartPage && ($cartPopup = $('#cart-popup')).length === 1) {
            cartPopupTO = 0;

            $cartPopupBadge = $('.cart-info-badge');

            $cartPopupBadge
                .bind(
                    'touchstart',
                    function () {
                        $cartPopup.toggleClass('bottom', $(this).hasClass('bottom-badge'));

                        if (!documentCartPopupTouch) {
                            common
                                .$document
                                .bind(
                                    'touchstart',
                                    function (e) {
                                        var $target;

                                        $target = $(e.target);

                                        if (
                                            !$target.is($cartPopupBadge) && $target.closest($cartPopupBadge).length === 0
                                            && !$target.is($cartPopup) && $target.closest($cartPopup).length === 0
                                        ) {
                                            hideCartPopup();
                                        }
                                    });

                            documentCartPopupTouch = true;
                        }

                        if ($(this).is($cartPopupBadge)) {
                            showCartPopup();

                            return false;
                        }
                    }
                )
                .add($cartPopup)
                .hover(
                    function () {
                        var $this;

                        if (cartPopupTO !== 0) {
                            clearTimeout(cartPopupTO);
                        }

                        if (common.ng.$cartScope.cart.info.final_sum !== 0) {
                            $this = $(this);

                            if (!$this.is($cartPopup)) {
                                $cartPopup.toggleClass('bottom', $this.hasClass('bottom-badge'));
                            }

                            showCartPopup();
                        }
                    },
                    function () {
                        if (cartPopupTO !== 0) {
                            clearTimeout(cartPopupTO);
                        }

                        cartPopupTO = setTimeout(
                            hideCartPopup,
                            150
                        );
                    }
                );
        }

        /**
         * Adds event listener for form inputs with user credentials (With '.l-email', '.l-phone')
         *
         * @param {String} type Credential type ('email', 'phone')
         */
        function setCredentialHook(type) {
            var TO;

            common
                .$document
                .on(
                    'keyup change paste',
                    '.l-' + type,
                    function () {
                        var input;

                        if (TO) {
                            clearTimeout(TO);
                        }

                        input = this;

                        TO = setTimeout(
                            function () {
                                var value,
                                    data;

                                value = $.trim(input.value);

                                if (
                                    value !== ''
                                    && (
                                        type === 'name'
                                        || (new RegExp('^' + (type === 'email' ? '[a-z0-9\\._-]{2,}@[a-z0-9\\.-]{2,}\\.[a-z]{2,5}' : '[\\d\\s()+-]{10,}') + '$')).test(value)
                                    )
                                    && common.storage.get(type) !== value
                                ) {
                                    common
                                        .storage
                                        .set(type, value);

                                    data = {
                                        session: common.storage.get('session_id')
                                    };

                                    data[type] = value;

                                    $
                                        .post(
                                            '/auth/credentials',
                                            data
                                        );
                                }
                            },
                            1000
                        );
                    }
                );
        }

        setCredentialHook('name');
        setCredentialHook('email');
        setCredentialHook('phone');

        /**
         * Shows one click buy popup, initiates form logic
         */
        function showOneClickBuyPopup() {
            var showCarousel;

            if (!$oneClickPopup) {
                $oneClickPopup = $('#one-click-buy-popup');

                $oneClickPopup
                    .find('form')
                    .submit(function () {
                        $.post(
                            '/order/fast_order',
                            $(this).serialize(),
                            function (o) {
                                if (o) {
                                    $oneClickPopup
                                        .fadeOut(function () {
                                            $.alert('<p>Ваш заказ успешно оформлен.</p><p>Мы свяжется с Вами по указанному телефону<br>в ближайшее время.</p>', 'Заказ оформлен!', true);
                                        });

                                    common
                                        .ng
                                        .$cartScope
                                        .update(o);
                                }
                            },
                            'json'
                        );

                        return false;
                    });

                $oneClickPopup
                    .find('.prs')
                    .jcarousel({
                        list: '.prs-holder',
                        items: '.pr'
                    });

                $('#one-click-buy-prev')
                    .jcarouselControl({
                        target: '-=2'
                    });

                $('#one-click-buy-next')
                    .jcarouselControl({
                        target: '+=2'
                    });

                $oneClickPopup
                    .find('.arrow')
                    .show()
                    .on('jcarouselcontrol:inactive', function () {
                        $(this).addClass('inactive');
                    })
                    .on('jcarouselcontrol:active', function () {
                        $(this).removeClass('inactive');
                    });

                common.phoneInput($oneClickPopup.find('.phone-input'));

                common.formStorageValues($oneClickPopup, true);
            }

            showCarousel = $oneClickPopup.find('.pr').length > 4;

            if (showCarousel) {
                $oneClickPopup
                    .find('.prs')
                    .jcarousel('reload');
            }

            $oneClickPopup
                .find('.arrow')
                .toggle(showCarousel);

            $oneClickPopup
                .fadeIn();
        }

        /**
         * Shows one click buy popup, adds product to cart if needed
         *
         * @param {Number} [productID] Product ID to add to cart
         */
        function oneClickBuy(productID) {
            var i, productInCart;

            if (productID) {
                productID = parseInt(productID, 10);

                productInCart = false;

                for (i = 0; i < common.ng.$cartScope.cart.products.length; ++i) {
                    if (common.ng.$cartScope.cart.products[i].id === productID) {
                        productInCart = true;

                        break;
                    }
                }

                if (!productInCart) {
                    common
                        .ng
                        .$cartScope
                        .cartProduct(productID, 1, null, true, showOneClickBuyPopup);

                    return;
                }
            }

            showOneClickBuyPopup();
        }

        common.oneClickBuy = oneClickBuy;

        $('.modal-popup')
            .each(function () {
                var $popup;

                $popup = $(this);

                $popup
                    .find('.close-btn, .bg')
                    .click(function () {
                        $popup
                            .fadeOut(function () {
                                if ($popup.hasClass('once')) {
                                    $popup
                                        .remove();
                                }
                            });

                        return false;
                    });
            });

        //  Close top banner discount
        $('.top-b .close').click(function () {
            $(this).closest('.top-b').remove();

            document.cookie = '_notopb=1; expires=Thu, 27 Jun 2030 12:00:00 GMT; path=/';

            return false;
        });

        //  Close top line professional mode
        $('.prof_top_line p .prof_close').click(function () {

            $(this).closest('.prof_top_line').remove();

            //document.cookie = '_no_prof_line=1; expires=Thu, 27 Jun 2030 12:00:00 GMT; path=/';

            return false;
        });

        if (document.getElementById('room-delivery')) {
            common
                .addressAutocomplete(
                    $('#delivery-address')
                );
        }

        if (location.search) {
            vars = location.search.substr(1).split('&');

            for (i = 0; i < vars.length; ++i) {
                pair = vars[i].split('=');

                if (pair[0] === 'utm_source') {
                    dt = new Date();

                    dt.setTime(dt.getTime() + (10000 * 24 * 60 * 60 * 1000));

                    document.cookie = 'criteo_dd=' + (pair[1] === 'criteo' ? 1 : 0) + '; expires=' + dt.toUTCString() + '; path=/';

                    break;
                }
            }
        }
    });


// search groups navigation
    $.fn.search_groups = function (main_selector) {

        $(main_selector).on('click', function () {

            var clicked = $(this).attr('class');
            clicked = clicked.match(/st_\d+/g);
            clicked = /\d+/.exec(clicked);

            $('.search_titles span').removeClass('st_active');
            $(this).find('span').addClass('st_active');

            $('.search_result').slideUp(600).removeClass('sg_active');
            $('.sg_' + clicked).slideDown(600).addClass('sg_active');
        });

        return this;
    };

//hide reg and login for click 
    $.fn.hide_reg_login = function () {
        common.$document.click(function (event) {
            if ($(event.target).closest(".auth").length)
                return;
            $(".reg_window, .login_window").fadeOut();
            $(".reg, .login").removeClass('active');
            event.stopPropagation();

        });

        return this;
    };


// show different packs
    $.fn.show_diff_packs = function (main_selector) {
        var updateGallery, zoomConfig,
            packID, productURL,
            $allPacks,
            $discountBox,
            $discount,
            $discount_sales,
            $discount_end_box,
            $discount_end,
            $discount_end_day;

        if (!common.productPage) {
            return this;
        }

        $allPacks = $(main_selector);

        /**
         * Updates zoom gallery according to selected pack
         */
        updateGallery = function () {
            var $gallery, $previewLink, $img;

            $('.zoomContainer').remove();
            $('.zoomWindowContainer').remove();

            if ($allPacks.length > 1) {

                $allPacks
                    .removeClass('active_pack')
                    .filter('[data-pack-id="' + packID + '"]')
                    .addClass('active_pack');

                $('.product-gallery')
                    .hide()
                    .find('.preview-link')
                    .removeClass('gallery_active');

            }

            $gallery = $('#gallery-' + packID);

            $img = $('#zoom_img')
                .attr('style', '');

            if ($img.parent('.zoomWrapper').length) {
                $img
                    .unwrap();
            }

            if ($gallery.length) {
                $previewLink = $gallery
                    .show()
                    .find('.preview-link')
                    .eq(0)
                    .addClass('gallery_active');

                zoomConfig
                    .gallery = 'gallery-' + packID;

                $img
                    .removeData('elevateZoom')
                    .attr('src', $previewLink.attr('data-image'))
                    .data('zoom-image', $previewLink.attr('data-zoom-image'))
                    .elevateZoom(zoomConfig);
            } else {
                $img
                    .attr('src', common.IMG_DOMAIN + '/images/prod_photo/' + common.imgUrl(packID + '.jpg', productURL));
            }
        };

        packID = /pack-(\d+)(?:$|[^\d])/g.exec(location.hash);

        packID = (packID ? packID[1] : $('.main_id_pack').attr('data-pack-id'));

        productURL = /\/product\/(.+)$/.exec(location.pathname)[1];


        zoomConfig = {
            scrollZoom: false,
            zoomWindowWidth: 415,
            zoomWindowHeight: 410,
            borderSize: 3,
            borderColour: '#00c5f0',
            galleryActiveClass: 'gallery_active',
            imageCrossfade: true,
            responsive: true
        };

        updateGallery();

        productPackVisible = false;

        if ($allPacks.length > 1) {
            $discountBox = $('#product-discount');
            $discount_end_box = $('#product-discount-end');

            if ($discountBox.length) {
                $discount = $('#discount-value-hurry');
            }

            if ($discount_end_box.length) {
                $discount_sales = $('#discount-value-sales');
                $discount_end = $('#discount-end-value');
                $discount_end_day = $('#discount-end-day-value');
            }

            var active_pack,
                discount,
                discount_end,
                discount_end_day;

            active_pack = $('.active_pack');

            if ($discountBox.length) {

                discount_end = active_pack.attr('data-discount-end');

                if (discount_end == 0) {
                    discount = active_pack.attr('data-discount');

                    $discount.text(discount);
                }

                $discountBox.toggle((discount_end == 0) && (discount > 0));
            }

            if ($discount_end_box.length) {

                discount_end = active_pack.attr('data-discount-end');

                if (discount_end > 0) {
                    discount = active_pack.attr('data-discount');
                    discount_end_day = active_pack.attr('data-discount-end-day');

                    $discount_sales.text(discount);

                    $discount_end.text(discount_end);

                    $discount_end_day.text(discount_end_day);
                }

                $discount_end_box.toggle((discount_end > 0) && (discount > 0));
            }


            $allPacks.mouseenter(function () {
                var $this,
                    $pricesBox,
                    discount,
                    discount_end,
                    discount_end_day,
                    discount_sales;

                $this = $(this);

                discount = 0;
                discount_sales = 0;
                discount_end = 0;
                discount_end_day = 0;

                if ($discountBox.length) {

                    $pricesBox = $this.find('.price2_and_counter');
                    discount_end = this.getAttribute('data-discount-end');

                    if ($pricesBox.length && (discount_end == 0)) {
                        discount = this.getAttribute('data-discount');

                        $discount.text(discount);
                    }

                    $discountBox.toggle(($pricesBox.length > 0) && (discount_end == 0));
                }

                if ($discount_end_box.length) {

                    $pricesBox = $this.find('.price2_and_counter');
                    discount_end = this.getAttribute('data-discount-end');

                    if ($pricesBox.length && (discount_end > 0)) {
                        discount_sales = this.getAttribute('data-discount');
                        discount_end_day = this.getAttribute('data-discount-end-day');

                        $discount_sales.text(discount_sales);

                        $discount_end.text(discount_end);

                        $discount_end_day.text(discount_end_day);
                    }

                    $discount_end_box.toggle(($pricesBox.length > 0) && (discount_end > 0));
                }

                packID = $this
                    .attr('data-pack-id');

                productPackVisible = $this
                    .hasClass('available-pack');

                if (history.replaceState) {
                    history.replaceState(undefined, undefined, '#pack-' + packID);
                } else {
                    location.replace('#pack-' + packID);
                }

                updateGallery();
            });
        }

        return this;
    };


//  hide and show artikles groups
    $.fn.articles_group = function (main_selector) {

        $(main_selector).on('click', function () {
            var articles = $(this).next('.articles_block');
            if (articles.css('display') == 'block') {
                articles.slideUp();
                $(this).find('span').css('border-bottom', '1px solid');

            } else {
                $('.articles_block').hide();
                articles.slideToggle();
                $(main_selector).find('span').css('border-bottom', '1px solid');
                $(this).find('span').css('border-bottom', 'none');
            }
        });

        return this;
    };

    $('#product-tabs .tab-btn')
        .click(function () {
            $('#product-tabs .tab-content')
                .addClass('hidden')
                .filter('.tab' + this.getAttribute('data-tab'))
                .removeClass('hidden');

            $('#product-tabs .tab-btn')
                .removeClass('vis');

            $(this)
                .addClass('vis');

            return false;
        });

//add to favourite
    $.fn.add_to_favourite = function () {
        $('.add_to_fav').on('click', function (e) {
            e.preventDefault();
            //  AJAX adding
            var url = common.decodeLink($(this).attr('data-link'));

            $.ajax({
                url: url,
                dataType: 'json',
                timeout: ajax_timeout,
                success: function (result) {

                    if (result.success == 1) {
                        var favourites = $('.account_menu .cart_favourites');
                        var fav_quan = parseInt(favourites.text(), 10);
                        favourites.text(fav_quan + 1);
                    }
                    $.alert(result.msg);
                },
                error: function () {
                    $.alert(data.msg);
                }
            });
        });

        return this;
    };

//auth window (reg or login)
    $.fn.auth_window = function () {
        $('.login, .reg, .fast_reg').on('click', function () {

            var margin_left = parseInt($('.bottom_auth').position().left, 10) - 10;
            $('.login_in_bottom, .reg_in_bottom').css('margin-left', margin_left + 'px');

            var active_window;
            var button = /\w+/.exec($(this).attr('class')) + '';

            if ((button === 'reg') || (button === 'fast_reg')) {
                if (button === 'fast_reg') {
                    button = 'reg';
                }

                active_window = $(this).parents('.auth').find('.reg_window');
                $(this).parents('.auth').find('.login').removeClass('active');
                $(this).parents('.auth').find('.login_window').fadeOut().addClass('hidden');
            } else {
                active_window = $(this).parent().find('.login_window');
                $(this).parent().find('.reg').removeClass('active');
                $(this).parent().find('.reg_window').fadeOut().addClass('hidden');
            }

            var win_display = active_window.css('display');

            if (win_display === 'none') {
                active_window.removeClass('hidden').fadeIn();
                $(this).parent().find('.' + button).addClass('active');
            } else {
                active_window.removeClass('hidden').fadeOut();
                $(this).parent().find('.' + button).removeClass('active');
            }
        });

        return this;
    };

//login
    $.fn.user_login = function (main_selector) {
        $('.auth').on('click', main_selector, function (e) {
            var redirectUrl;

            e.preventDefault();

            redirectUrl = this.getAttribute('data-url') || '/room';

            var btn = $(this);
            btn.html('Входим<span class="think"></span>');

            var remember_me = 0;
            var user = $(this).parents('.auth').find('.login_user').val();
            var password = $(this).parents('.auth').find('.login_password').val();

            if (!user) {
                user = 0;
            }
            if (!password) {
                password = 0;
            }
            if ($(this).parents('.auth').find('.remember .rem_ok').attr('class') !== undefined) {
                remember_me = 1;
            }

            $.ajax({
                url: '/login',
                data: {
                    user: user,
                    password: password
                },
                method: 'POST',
                dataType: 'json',
                timeout: 6000,
                success: function (result) {
                    if (!result.success) {
                        $('.form_error').remove();
                        $('.auth_error').remove();
                        $('.form-group').removeClass('has-error');

                        if (result.user_error !== 0) {
                            $('.login_user')
                                .parent()
                                .addClass('has-error')
                                .end()
                                .after('<p class="form_error">' + result.user_error + '</p>');
                        }

                        if (result.password_error !== 0) {
                            $('.login_password').after('<p class="form_error">' + result.password_error + '</p>');
                            $('.login_password').parent().addClass('has-error');
                        }

                        if (result.msg !== 0) {
                            $('.login_password').after('<div class="auth_error">' + result.msg + '</div>');
                            $('.login_window .form-group').addClass('has-error');
                        }
                    } else {
                        common.setStorage(result.name, result.email, result.region, password, result.phone);

                        location = redirectUrl;
                    }

                    btn.text('Войти');
                },
                error: function () {
                    $.alert(data.msg);
                }
            });
        });
        return this;
    };

//  registration
    $.fn.registration = function (main_selector) {
        $('.auth').on('click', main_selector, function (e) {
            var redirectUrl;

            e.preventDefault();

            redirectUrl = this.getAttribute('data-url') || '/room';

            var btn = $(this);
            btn.html('Регистрируем<span class="think"></span>');

            var email = $(this).parents('.auth').find('.reg_email').val();
            var password = $(this).parents('.auth').find('.reg_password').val();

            if (!email) {
                email = 0;
            }
            if (!password) {
                password = 0;
            }

            $.ajax({
                url: '/email_approve/add',
                data: {
                    email: email,
                    password: password
                },
                method: 'POST',
                dataType: 'json',
                timeout: 6000,
                success: function (result) {
                    if (result.loggedIn) {

                        common.checkStorage(false, email, false, password, false);

                        location = redirectUrl;

                        return;
                    }

                    if (!result.success) {

                        $('.form_error').remove();
                        $('.form-group').removeClass('has-error');
                        $('.auth_error').remove();
                        $('.reg_error').remove();

                        if (result.email_error !== 0) {
                            $('.reg_email').after('<p class="form_error">' + result.email_error + '</p>');
                            $('.reg_email').parent().addClass('has-error');
                        }

                        if (result.password_error !== 0) {
                            $('.reg_password').after('<p class="form_error">' + result.password_error + '</p>');
                            $('.reg_password').parent().addClass('has-error');
                        }

                        if (result.msg !== 0 && result.msg !== undefined) {
                            $('.reg_password').after('<div class="reg_error">' + result.msg + '</div>');
                            $('.reg_window .form-group').addClass('has-error');
                        }
                    } else {
                        $.alert('На указанный Вами адрес отправлено письмо. Чтобы подтвердить E-mail и завершить регистрацию - перейдите по ссылке в письме.', 'Регистрация');
                    }

                    btn.text('Зарегистрировать');
                },
                error: function () {
                    $.alert(data.msg);
                }
            });

        });
        return this;
    };

//  mail subscribe
    $.fn.subscribe = function (main_selector) {
        $('.subscribe').on('click', main_selector, function (e) {
            e.preventDefault();

            var btn = $(this);
            var url = '/unsubscribe/sub';
            var sub_email = $('.subscribe .sub_mail').val();
            var data = {
                'sub': sub_email
            };

            $.ajax({
                url: url,
                timeout: 6000,
                type: 'POST',
                dataType: 'json',
                data: data,
                beforeSend: function (XmlHttp) {
                    btn.html('Подписка<span class="think"></span>');
                },
                success: function (data) {

                    btn.html('Подписаться');

                    if (data.errors) {
                        $('.subscribe .form_error').remove();

                        if (data.errors.email) {
                            $('.subscribe .sub_form').after('<p class="form_error">' + data.errors.email + '</p>');
                        }
                    } else {

                        $('.subscribe .form_error').remove();
                        if (data.success) {
                            if (data.type == 'just') {
                                $('.subscribe .sub_form').detach();
                                $('.subscribe .sub_desc').after('<p class="font_accent">Указанный email уже подписан на нашу рассылку</p>');
                            }
                            if (data.type == 'ok') {
                                $('.subscribe .sub_form').detach();
                                $('.subscribe .sub_desc').after('<p class="font_ok">' + data.msg + '</p>');
                                common.checkStorage(false, sub_email, false, false, false);
                            }
                            if (data.type == 'next_one') {
                                $('.subscribe .sub_form').detach();
                                $('.subscribe .sub_desc').after('<p><span class="font_accent">Вы отказались от подписки на нашу рассылку по указанному email.</span> <a  class="font_ok" href ="' + data.link + '">Активизировать рассылку</a></p>');
                            }

                        } else {
                            $.alert(data.msg);
                        }

                    }
                },
                error: function () {
                    $.alert(data.msg);
                }
            });

        });
        return this;
    };

    /**
     * Scroll to top button
     */
    $.fn.add_scroll_to_top = function () {

        $('body').append('<span class="scroll_top">Наверх</span>');
        var scroll_button = $('.scroll_top');

        var show_scroll_top = function () {
            if (common.$window.scrollTop() > 300) {
                scroll_button.fadeIn();
            } else {
                scroll_button.fadeOut();
            }
        };
        scroll_button.click(function () {

            $("html:not(:animated)" + ",body:not(:animated)").animate({
                scrollTop: 0
            }, 500);
            return false;
        });

        common.$window.scroll(function () {
            show_scroll_top();
        });
        show_scroll_top();

        return this;
    };

    $.fn.empty_search = function (main_selector) {

        $(main_selector + ' ').on('click', 'button', function (e) {

            if (!$(main_selector + ' input').val()) {
                e.preventDefault();
            }

        });
        return false;
    };

    /**
     * Executes function on deffered condition
     *
     * @param {Function} condition Must return `true` if condition is met
     * @param {Function} callback Function to be executed
     */
    function callOnCondition(condition, callback) {
        var interval;

        interval = setInterval(function () {
            if (condition()) {
                callback();

                clearInterval(interval);
            }
        }, 10);
    }

    if (window.yandexEvents) {
        callOnCondition(
            function () {
                return window.yaCounter482478;
            },
            function () {
                for (i = 0; i < window.yandexEvents.length; ++i) {
                    window.yaCounter482478.reachGoal(window.yandexEvents[i][0], window.yandexEvents[i][1] ? window.yandexEvents[i][1] : {});
                }
            }
        );
    }

    m = /\/category(?:-tm)?\/([^\/]+)(?:\/|$)?/.exec(location.pathname);

    if (m) {
        $('.menu__item-link[href="/category/' + m[1] + '"]')
            .addClass('current');
    }

    $(document).ready(function () {
        $('.brands')
            .each(function () {
                var h, col, cols;
                var blh = $('body').css('line-height');
                cols = parseInt(blh.replace(/[^\d\.]/g, "")) + 6;
                h = $(window).height();
                col = Number($(this).attr('count-br'));
                if ((0.6 * h) < (col * cols)) {
                    $(this).addClass('rk_scroll');
                }
            });
    });

    /**
     * Returns formatted price
     *
     * @param input Original price value
     *
     * @returns {*}
     */
    common.priceFormat = function (input) {
        if (isNaN(input)) {
            return input;
        } else {
            input = parseInt(input, 10).toString();

            return input.length > 3 ? input.substr(0, input.length - 3) + ' ' + input.substr(input.length - 3) : input;
        }
    };

    angular.module('priceFilter', [])
        .filter('price', function () {
            return function (input) {
                return common
                    .priceFormat(input);
            }
        });

    common.ng = {
        app: angular.module('rk', ['priceFilter']).config(['$sceProvider', function ($sceProvider) {
            $sceProvider.enabled(false);
        }])
    };

    if (common.productPage) {
        $('.hot_icon')
            .each(function () {
                var $this;

                $this = $(this);

                $this
                    .popover({
                        placement: 'bottom',
                        content: $this.find('.text').html(),
                        trigger: 'hover',
                        delay: {
                            hide: 150
                        },
                        container: this,
                        html: true
                    });
            });

        common.ng.app.controller('found-lower-price', ['$scope', function ($scope) {
            $foundLowerPriceScope = $scope;
        }]);

        $foundLowerPriceModal = $('#found-lower-modal');

        $('#found-cheaper a')
            .click(function () {
                var $pack;

                $pack = $('.active_pack');

                if ($pack.length === 0) {
                    $pack = $('.one_pack:first');
                }

                $foundLowerPriceScope
                    .product = {
                    name: document.getElementById('product-name').innerHTML,
                    tm: $('#product-tm').text(),
                    id: $pack.attr('data-pack-id'),
                    price: $pack.find('.p-price').text(),
                    src: $('.main_img').attr('src')
                };

                $foundLowerPriceScope
                    .$apply();

                common.formStorageValues($foundLowerPriceModal, true);

                $foundLowerPriceModal
                    .fadeIn(150);

                return false;
            });

        common
            .phoneInput($('#found-lower-phone'));

        $('#found-lower-modal')
            .find('.close')
            .click(function () {
                $foundLowerPriceModal
                    .fadeOut(150);

                return false;
            })
            .end()
            .find('form')
            .submit(function () {
                var form, errors,
                    i, $i;

                $i = $foundLowerPriceModal
                    .find('.i')
                    .removeClass('has-error');

                errors = [];

                if ($.trim(document.getElementById('found-lower-name').value) === '') {
                    errors
                        .push('name');
                }

                if (!/^[a-z0-9-_\.]{2,}@[a-z0-9-\.]{2,}\.[a-z]{2,5}$/.test($.trim(document.getElementById('found-lower-email').value))) {
                    errors
                        .push('email');
                }

                if (!/^\+7 \(\d{3}\) \d{7,8}$/.test($.trim(document.getElementById('found-lower-phone').value))) {
                    errors
                        .push('phone');
                }

                if (!/[a-z0-9\.-]{2,}\.[a-z]{2,5}/.test($.trim(document.getElementById('found-lower-url').value))) {
                    errors
                        .push('url');
                }

                if (errors.length !== 0) {
                    for (i = 0; i < errors.length; ++i) {
                        $i
                            .filter('.i-' + errors[i])
                            .addClass('has-error');
                    }
                } else {
                    form = this;

                    $.post(
                        '/product/found_lower_price',
                        $(form).serialize(),
                        function (o) {
                            if (o) {
                                $foundLowerPriceModal
                                    .fadeOut();

                                form
                                    .reset();
                            }

                            $.alert(
                                o
                                    ? 'Мы подготовили для Вас индивидуальное предложение, проверьте Вашу электронную почту!'
                                    : 'К сожалению, возникла ошибка при отправке запроса. Попробуйте повторить запрос позже',
                                o
                                    ? 'Ваша заявка отправлена!'
                                    : ''
                            );
                        },
                        'json'
                    );
                }

                return false;
            })
            .end()
            .find('.bg')
            .click(function () {
                $foundLowerPriceModal
                    .fadeOut(150);

                return false;
            })
            .end()
            .find('.bdg')
            .each(function (i) {
                $(this)
                    .popover({
                        container: this,
                        placement: 'right',
                        content: $('.hot_icons .hot_icon[data-i="' + this.getAttribute('data-i') + '"]').find('.text').html(),
                        trigger: 'hover',
                        delay: {
                            hide: 250
                        },
                        html: true
                    });
            });

        $('#one-click-buy')
            .click(function () {
                common.oneClickBuy(productPackVisible ? productPackID : $('.available-pack:first').attr('data-pack-id'));

                return false;
            });

        $productDescription = $('.descr-box');

        maxHeight = Math.max($('.img-box').height(), $('.product_pack_price').height());

        if ($productDescription.height() > maxHeight) {
            $productDescription.addClass('read_more_desc').attr('data-size', maxHeight);
        }
    }

    common.ajaxTimeout = ajax_timeout;

    /**
     * Initiates packs hover picture change for product list
     *
     * @param {jQuery} $productList Product list element
     */
    common.packsHoverInit = function ($productList) {
        $productList
            .find('.product-in-list')
            .each(function () {
                var $variants;

                $variants = $(this)
                    .find('.variant');

                if ($variants.length !== 1) {
                    $variants
                        .mouseenter(function () {
                            $(this)
                                .closest('.product-in-list')
                                .find('.product-image')
                                .attr('src', common.IMG_DOMAIN + '/images/prod_photo/' + common.imgUrl('s_' + this.getAttribute('data-id') + '.jpg', this.getAttribute('data-prod-url')));
                        });
                }
            });
    };

    $productList = $('.product-list');

    if ($productList.length !== 0) {
        common.packsHoverInit($productList);
    }

    common.msg = msg;

    common.productsOnPage = common.defaultProductsOnPage = 40;


    /**
     * Returns product image URL with added part (E.g. 1.jpg => 1-product-url.jpg)
     *
     * @param {String} src Original image URL
     * @param {String} add Part to add
     *
     * @returns {String}
     */
    common.imgUrl = function (src, add) {
        var m;

        return add && src !== 'none.jpg' && (m = /^(.+)\.([a-zA-Z]+)$/.exec(src)) ? m[1] + '-' + add + '.' + m[2] : src;
    };

    /**
     * Initiates address autocomplete
     *
     * @param {jQuery} $input Input element
     */
    common.addressAutocomplete = function ($input) {
        $input
            .autocomplete(
                {
                    source: '/cart/address',
                    minLength: 4
                }
            );
    };
})();
