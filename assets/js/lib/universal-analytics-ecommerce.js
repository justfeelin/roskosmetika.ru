(function (undefined) {
    "use strict";

    /**
     * Returns product in list row info
     *
     * @param {jQuery} $product Product in list element
     *
     * @returns {{name: String, brand: String, variants: Array}} variants: [{id: String, name: String}]
     */
    function productRowInfo($product) {
        var info;

        info = {
            name: $product
                .find('.product-item__name')
                .text(),

            //English only brands allowed
            brand: $product
                .find('.prod_link_name')
                .attr('data-brand'),

            variants: []
        };

        $product
            .find('.cart_info.variant')
            .each(function () {
                info.variants.push({
                    id: this.getAttribute('data-id'),
                    name: $(this)
                        .find('.pack-name')
                        .text()
                })
            });

        return info;
    }

    /**
     * Removes bad space characters from value
     *
     * @param {String} value Original value
     *
     * @returns {String}
     */
    function prepareUAValue(value) {
        return value.replace(/\s+/g, ' ');
    }

    /**
     * Parses products list & initiates UA logic for each product
     *
     * @param {jQuery} $list Products list. If not passed - .product-list on page will be used
     * @param {String} [listName] List name. If not passed - will be taken from "data-list" attribute of products list
     * @param {Number} [page] Page number. Default 1
     */
    function parseProductsList($list, listName, page) {
        var $products, pageOffset;

        if (!listName) {
            listName = $list.attr('data-list');

            if (!listName) {
                return;
            }
        } else {
            $list.attr('data-list', listName);
        }

        $products = $list
            .find('.product-in-list:visible');

        pageOffset = (page ? page - 1 : 0) * common.productsOnPage + 1;

        $products
            .each(function (i) {
                parseProductInList(listName, $(this), $products.length > 10, pageOffset + i);
            });
    }

    /**
     * Initiates UA logic for product element
     *
     * @param {String} listName Products list name
     * @param {jQuery} $product Product element
     * @param {Boolean} noNames Do not provide product names (In order to not exceed request's max data length)
     * @param {Number} position Position in list
     */
    function parseProductInList(listName, $product, noNames, position) {
        var info, i;

        info = productRowInfo($product);

        for (i = 0; i < info.variants.length; ++i) {
            uaProductInListView(listName, info.name, info.brand, info.variants[i].id, info.variants[i].name, noNames, position);
        }
    }

    /**
     * Sends UA product in list view event
     *
     * @param {String} listName Products list name
     * @param {String} name Product name
     * @param {String} brand Product brand
     * @param {String} id Pack ID
     * @param {String} variant Pack name
     * @param {Boolean} noName Do not pass name
     * @param {Number} position Position in list
     */
    function uaProductInListView(listName, name, brand, id, variant, noName, position) {
        var data;

        data = {
            id: id,
            brand: brand,
            variant: prepareUAValue(variant),
            list: prepareUAValue(listName)
        };

        if (position) {
            data.position = position;
        }

        if (!noName) {
            data.name = prepareUAValue(name);
        }

        ga('UniversalAnalytics.ec:addImpression', data);
        ga('MainUATracker.ec:addImpression', data);
    }

    /**
     * Product in list click handler
     *
     * @param {jQuery} $link Link in product row
     */
    function productClick($link) {
        var $product, info, listName, linkTarget, redirect;

        $product = $link.closest('.product-in-list');

        info = productRowInfo($product);

        listName = common.cartPage ? 'Корзина' : getListName($product);

        linkTarget = $link.attr('target');

        redirect = !listName || !linkTarget
            ? function () {
                var href;

                href = $link.attr('href');

                location = href === '#' && $link.hasClass('hidden_link') ? common.decodeLink($link.attr('data-link')) : href;
            }
            : null;

        if (!listName) {
            redirect();

            return;
        }

        if (!linkTarget) {
            //just in case
            setTimeout(
                redirect,
                3000
            );
        }

        ga('UniversalAnalytics.ec:addProduct', {
            id: info.variants[0].id,
            name: info.name,
            brand: info.brand,
            variant: info.variants[0].name,
            position: (common.showAll ? 0 : (common.listPage > 1 ? (common.listPage - 1) * common.productsOnPage : 0)) + $product.index() + 1
        });

        ga('UniversalAnalytics.ec:setAction', 'click', {
            list: listName
        });

        ga('UniversalAnalytics.send', 'event', 'UX', 'click', 'Results', {
            hitCallback: linkTarget ? $.noop : redirect
        });

        //  GUA Main
        ga('MainUATracker.ec:addProduct', {
            id: info.variants[0].id,
            name: info.name,
            brand: info.brand,
            variant: info.variants[0].name,
            position: (common.showAll ? 0 : (common.listPage > 1 ? (common.listPage - 1) * common.productsOnPage : 0)) + $product.index() + 1
        });

        ga('MainUATracker.ec:setAction', 'click', {
            list: listName
        });

        ga('MainUATracker.send', 'event', 'UX', 'click', 'Results', {
            hitCallback: linkTarget ? $.noop : redirect
        });

        return linkTarget ? undefined : false;
    }

    /**
     * Returns list name
     *
     * @param {jQuery} $item Item element within the list element
     *
     * @returns {String}
     */
    function getListName($item) {
        return $item.closest('.ua-product-list').attr('data-list')
    }

    /**
     * Sends google analytics 'pageview' request
     *
     * @param {Function} [callback] Callback to be called on success
     */
    function gaPageview(callback) {
        var data;

        common.gaPaveviewSent = true;

        if (callback) {
            data = {
                hitCallback: callback
            };
        } else {
            data = undefined;
        }

        ga('UniversalAnalytics.send', 'pageview', data);

        //  GUA Main
        ga('MainUATracker.send', 'pageview', data);
    }

    common.uaProductList = parseProductsList;
    common.uaListName = getListName;
    common.uaProductClick = productClick;
    common.gaPageview = gaPageview;
})();
