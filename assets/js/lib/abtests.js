(function () {
    var initiated;

    /**
     * AB-tests class
     */
    common.ABTests = {
        /**
         * Initiates google custom parameters
         */
        init: function () {
            var test;

            if (initiated) {
                return;
            }

            initiated = true;

            for (test in ABTests) {
                if (ABTests.hasOwnProperty(test)) {
                    ga('UniversalAnalytics.set', 'dimension' + ABTests[test].analytics, ABTests[test].value === true ? 'yes' : (ABTests[test].value === false ? 'no' : ABTests[test].value));
                }
            }
        },

        /**
         * Returns specified AB-test use case
         *
         * @param {String} testCode AB-test code value
         * @param {String} [neededCase] Check for provided test case
         *
         * @returns string|bool|null Used test case code, true if boolean AB-test (Or provided case matched), false if test is not being performed or provided case didn't match or null if specified test is not used for current session
         */
        check: function (testCode, neededCase) {
            return ABTests[testCode] ? (neededCase !== undefined ? ABTests[testCode] === neededCase : ABTests[testCode]) : null;
        }
    };
})();
