(function () {
    var $addedScope, $cart,
        certificateTO;

    /**
     * Returns shipping inputs values or null if no values selected
     *
     * @param {Boolean} [changedRegion] Whether region changed (No need to pass current shipping type)
     *
     * @return {{region: Number, shipping: String}|null}
     */
    function shippingFormData(changedRegion) {
        var region, shipping,
            data;

        region = document.getElementById('cart-region');
        shipping = changedRegion ? null : document.getElementById('cart-shipping');

        if (
            region !== null
            && (
                changedRegion
                    ? true
                    : (shipping !== null && region.selectedIndex !== -1 && shipping.selectedIndex !== -1))
        ) {
            data = {
                region: region.options[region.selectedIndex].value
            };

            if (!changedRegion) {
                data.shipping = shipping.options[shipping.selectedIndex].value;
            }

            return data;
        } else {
            return null;
        }
    }

    /**
     * Sets showAction property for first product in products list
     *
     * @param {Array} products Cart products array
     */
    function productsActionTitle(products) {
        var i;

        if (!common.cartPage) {
            return;
        }

        for (i = 0; i < products.length; ++i) {
            if (products[i].special) {
                products[i].showAction = true;

                break;
            }
        }
    }

    common.ng.app.controller('cart', ['$scope', '$compile', function ($cartScope, $compile) {
        var popoverTO;

        $cartScope.imgUrl = common.imgUrl;

        $cartScope.Math = Math;

        /**
         * Updates product in cart
         *
         * @param {Number} productID Product ID
         * @param {Number} quantity Set flag. -1 - remove one row from cart, 1 - add one row to cart, 0 - completely remove product from cart
         * @param {Event} event Original click event
         * @param {Boolean} [noPopup] Do not show added product popup
         * @param {Function} [callback] Callback to be called on success
         * @param {String} [rrMethod] RetailRocket method name used to fetch products
         * @param {Number} [rrProductID] RetailRocket product ID used to get products
         */
        $cartScope.cartProduct = function (productID, quantity, event, noPopup, callback, rrMethod, rrProductID) {
            var data, email, shippingData, i, decreasedProduct;

            if (event) {
                event.preventDefault();
            }

            quantity = parseInt(quantity, 10);

            if (isNaN(quantity) || [-1, 0, 1].indexOf(quantity) === -1) {
                return;
            }

            data = {};

            email = common.storage.get('email');

            if (email && /^[\._a-zA-Z\d-]+@[\.a-zA-Z\d-]+\.[a-zA-Z]{2,6}$/.test(email)) {
                data.email = email;
            }

            if (noPopup) {
                data.noPopup = true;
            }

            shippingData = shippingFormData();

            if (shippingData) {
                $.extend(data, shippingData);
            }

            if (quantity !== 1) {
                for (i = 0; i < common.ng.$cartScope.cart.products.length; ++i) {
                    if (common.ng.$cartScope.cart.products[i].id === productID) {
                        decreasedProduct = {
                            id: '' + productID,
                            name: common.ng.$cartScope.cart.products[i].name,
                            brand: common.ng.$cartScope.cart.products[i].brand,
                            variant: common.ng.$cartScope.cart.products[i].pack,
                            price: '' + common.ng.$cartScope.cart.products[i].price,
                            quantity: common.ng.$cartScope.cart.products[i].quantity - 1
                        };

                        break;
                    }
                }
            }

            if (rrMethod) {
                try {
                    rrApi
                        .recomAddToCart(rrProductID, {
                            methodName: rrMethod
                        });
                } catch (e) {
                }
            }

            $.ajax({
                url: '/cart/update/' + productID + '/' + quantity,
                type: 'POST',
                data: data,
                dataType: 'json',
                timeout: common.ajaxTimeout,
                success: /**
                 * @param {{info: Object, products: Array, added: {id: Number, name: String, pack: String, tm: String, price: Number, quantity: quantity, presentPopup: Boolean}}} data
                 */
                function (data) {
                    var listName, i, gaInfo,
                        hasRegularProducts;
                        // inboxer_data;

                    if (data.success) {
                        $cartScope.update(data);

                        if (quantity === 1 && !common.cartPage) {
                            yaCounter482478.reachGoal('click_to_cart');

                            if (!noPopup) {
                                $addedScope.show(data.added);
                            }

                            //  Retail Rocket
                            // if (data.added && data.added.id) {
                            //     try {
                            //         rrApi.addToBasket(data.added.id);
                            //     } catch(e) {

                            //     }
                            // }
                            
                            // VK retargeting                      
                                
                            VK.Retargeting.Init("VK-RTRG-352543-4tMLM"); 
                            VK.Retargeting.Hit();

                            const eventParams = { 
                                                  "products" :  [{"id": data.added.id, "price": data.added.price}], 
                                                  "currency_code" : "RUR"
                                                }; 

                            VK.Retargeting.ProductEvent(3335, "add_to_cart", eventParams);

                            // My target
                            var _tmr = _tmr || [];
                            _tmr.push({
                                "type" : "itemView",
                                "productid" : data.added.id,
                                "pagetype" : "cart", 
                                "totalvalue" : data.added.price,
                                "list": "1"
                            });
                                                  
                        }

                        //  inboxer count
                        // inboxer_data= {items: []};

                        // for (i = 0; i < data.products.length; ++i) {

                        //     inboxer_data.items.push ({
                        //         id: data.products[i].id,
                        //         qnt: data.products[i].quantity,
                        //         price: data.products[i].price
                        //     });
                        // }

                        // try{
                        //     gazeApi.cart(inboxer_data);    
                        // } catch(e) {}


                        try {
                            listName = common.cartPage || !event ? 'Корзина' : common.uaListName($(event.target));

                            if (!listName && common.productPage) {
                                listName = 'Страница товара';
                            }

                            if (listName) {
                                if (quantity === 1) {
                                    for (i = 0; i < data.products.length; ++i) {
                                        if (data.products[i].id === productID) {
                                            gaInfo = {
                                                id: '' + productID,
                                                name: data.products[i].name,
                                                brand: data.products[i].brand_url,
                                                variant: data.products[i].pack,
                                                price: '' + data.products[i].price,
                                                quantity: data.products[i].quantity
                                            };

                                            break;
                                        }
                                    }
                                } else {
                                    if (decreasedProduct) {
                                        gaInfo = decreasedProduct;
                                    }
                                }

                                if (gaInfo) {
                                    ga('UniversalAnalytics.ec:addProduct', gaInfo);

                                    ga('UniversalAnalytics.ec:setAction', quantity === 1 ? 'add' : 'remove');
                                    ga('UniversalAnalytics.send', 'event', listName, 'click', 'add to cart');

                                    //  GUA main
                                    ga('MainUATracker.ec:addProduct', gaInfo);

                                    ga('MainUATracker.ec:setAction', quantity === 1 ? 'add' : 'remove');
                                    ga('MainUATracker.send', 'event', listName, 'click', 'add to cart');
                                }
                            }
                        } catch(e) {

                        }

                        if (data.presentPopup) {
                            $('#cart-present-popup')
                                .delay(1000)
                                .fadeIn();
                        }

                        if (common.showPopup && !common.cartPage) {
                            for (i = 0; i < data.products.length; ++i) {
                                if (
                                    data.products[i].id !== 12524
                                    && data.products[i].id !== 12525
                                    && data.products[i].id !== 12526
                                    && data.products[i].id !== 22222
                                ) {
                                    setTimeout(
                                        common.showPopup,
                                        5000
                                    );

                                    break;
                                }
                            }
                        }

                        if (common.cartPage) {
                            common.updateShippingOptions();
                        }

                        if (callback) {
                            callback();
                        }
                    } else {
                        $.alert(data.msg);
                    }
                },
                error: function () {
                    $.alert('Нам очень стыдно, но что-то случилось с нашим сайтом, попробуйте попозже.');
                }
            });
        };

        /**
         * Updates controller scope and calls $apply() if necessary
         *
         * @param {{products: Array, info: Object, user: Object}} data New cart data. Each key is optional
         */
        $cartScope.update = function (data) {
            var i, fields, update;

            fields = ['products', 'info', 'user'];

            update = false;

            for (i = 0; i < fields.length; ++i) {
                if (data.hasOwnProperty(fields[i]) && data[fields[i]]) {
                    $cartScope.cart[fields[i]] = data[fields[i]];

                    update = true;
                }
            }

            if (update) {
                if ($cartScope.cart.products && $cartScope.cart.products.length > 0) {
                    productsActionTitle($cartScope.cart.products);
                }

                $cartScope.$apply();
            }
        };

        /**
         * Updates shipping price & renders view
         *
         * @param {Boolean} [changedRegion] Region changed (Not shipping type)
         */
        $cartScope.updateShippingPrice = function (changedRegion) {
            var formData;

            formData = shippingFormData(changedRegion);

            $.post(
                '/cart/shipping',
                formData ? formData : null,
                function (data) {
                    var input, i, j;

                    $.extend($cartScope.cart.info, data);

                    common.storage.set('region', data.region ? data.region : '');
                    common.storage.set('shipping', data.shipping ? data.shipping : '');

                    $cartScope.$apply();

                    if (data.shipping) {
                        input = document.getElementById('cart-shipping');

                        for(i = 0, j = input.options.length; i < j; ++i) {
                            if(input.options[i].value == data.shipping) {
                                input.selectedIndex = i;

                               break;
                            }
                        }

                        if (changedRegion) {
                            if (common.cartPage) {
                                common
                                    .updateShippingOptions();
                            }
                        }

                        if (
                            common.ng.$cartScope.cart.info.shipping_compensation
                            && common.ng.$cartScope.cart.info.region !== 77
                            && !common.storage.get('shcompal')
                        ) {

                            $('.shipping-compensation-info')
                                .popover('show');

                            setTimeout(
                                function () {
                                    $('.shipping-compensation-info')
                                        .popover('hide');
                                },
                                3000
                            );

                            common.storage.set('shcompal', 1);
                        }
                    }
                }
            );
        };

        $cartScope.cart = common.cartInfo;

        if ($cartScope.cart.products && $cartScope.cart.products.length > 0) {
            productsActionTitle($cartScope.cart.products);
        }

        common.ng.$compile = $compile;
        common.ng.$cartScope = $cartScope;

        if (common.cartPage) {
            $cartScope.$watch('cart.products', function () {
                if (popoverTO) {
                    clearTimeout(popoverTO);
                }

                popoverTO = setTimeout(
                    function () {
                        var $noDiscounts;

                        $noDiscounts = $('#cart-page .cart_items_block .no-discount:visible');

                        if ($noDiscounts.length !== 0) {
                            $noDiscounts
                                .popover({
                                    placement: 'right',
                                    content: 'Скидки не распространяются:<br> - на товары из раздела «Оборудование»<br> - подарочные сертификаты<br> - бренды и товары, указанные в разделе<br> «Система скидок»',
                                    html: true,
                                    trigger: 'hover'
                                });
                        }
                    },
                    200
                );
            });
        }
    }]);

    common.ng.app.controller('added-product', ['$scope', function ($scope) {
        var $cartLink;

        /**
         * Shows 'product added to cart' popup
         *
         * @param {Object} product Product info (Used to render ng template)
         */
        $scope.show = function (product) {
            var cartWidth, cartHeight, marginTop, marginLeft;

            $scope.product = product;

            $scope.$apply();

            $cart.clearQueue().stop().removeClass('hidden');

            if (common.$window.width() < 790) {
                $cart.addClass('fixed');

                marginTop = marginLeft = '';
            } else {
                $cart.show();

                cartWidth = $cart.width();
                cartHeight = $cart.height();

                marginTop = -cartHeight - 38;
                marginLeft = parseInt($cartLink.position().left, 10) - (cartWidth / 2) + ($cartLink.width() / 2) - 5;

                $cart.removeClass('fixed');
            }

            $cart.css({
                display: 'none',
                marginTop: marginTop,
                marginLeft: marginLeft
            }).fadeIn().focus().delay(1500).fadeOut(1500);
        };

        $cartLink = $('.ac_cart .ac_menu_solid');

        $scope.product = {
            src: 'none.jpg'
        };

        $addedScope = $scope;
    }]);

    $cart = $('.show_cart');

    $cart.hover(
        function() {
            $cart.clearQueue().stop().removeClass('hidden').css({
                display: 'block',
                opacity: 1
            });
        },
        function() {
            $cart.clearQueue().stop().removeClass('hidden').delay(1500).fadeOut(1500);
        }
    );

    $('.cart_items')
        .bind('keyup paste cut', '.certificate-box input', function (event) {

            if (certificateTO) {
                clearTimeout(certificateTO);
            }

            certificateTO = setTimeout(
                function () {
                    var price;

                    certificateTO = 0;

                    price = parseInt($.trim(event.target.value), 10);

                    if (price < 1) {
                        return;
                    }

                    $.post(
                        '/cart/certificate',
                        {
                            price: price
                        },
                        function (cartInfo) {
                            if (cartInfo) {
                                common
                                    .ng
                                    .$cartScope
                                    .update(cartInfo);
                            }
                        },
                        'json'
                    );
                },
                500
            );
        });
})();
