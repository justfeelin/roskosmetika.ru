﻿<?php
header("HTTP/1.0 500 Internal Server Error");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="status" content="HTTP/1.0 500 Internal Server Error">
    <meta http-equiv="HTTP request status" content="HTTP/1.0 500 Internal Server Error">
    <title>Ошибка 500</title>
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="stylesheet" href="/css.css">
</head>
<body itemscope itemtype="http://schema.org/WebPage">
<div class="container">
    <div class="header">
        <div class="row">
            <div class="col-xs-3 header_shipping">
            </div>
            <div class="col-xs-6 align_center text_500">Интернет-магазин профессиональной косметики</div>
            <div class="col-xs-3">
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3 logo">
                <a href="/"><img src="/images/logo_new.png" width="285" height="50"
                                 alt="Роскосметика - интернет-магазин натуральной косметики"></a>
            </div>
            <div class="col-xs-6 align_center text_500">"Роскосметика"</div>
            <div class="col-xs-3 phone">
                <p class="phone_number">8&nbsp;800&nbsp;775-54-83</p>

                <p class="under_number">Бесплатно по России</p>
            </div>
        </div>
    </div>
    <div class="row pre_500">
        <div class="col-xs-6">
            <p>На нашем сайте произошел технический сбой, который уже устраняется специалистами.</p>

            <p>Приносим Вам наши извинения за неудобства.</p>

            <p>Попробуйте перейти на <a href="https://www.roskosmetika.ru/">главную</a> страницу. Если Вы снова попадете
                на эту страницу - то придется подождать, пока специалисты не восстановят работу сайта, спасибо за
                понимание.</p>
        </div>
        <div class="col-xs-6 subscribe">
            <div class="row sub_backgroung">
                <div class="col-xs-1 sub_line"></div>
                <div class="col-xs-11">
                    <div class="rew_sub_area">
                        <p class="rew_sub_title">Есть вопросы и нужна помощь?

                        <p>

                        <div class="sub_desc">
                            <p>Позвоните нам с 08:00 до 20:00</p>

                            <p class="phone_number">8&nbsp;800&nbsp;775-54-83</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <img class="error_img" src="/images/500.png">
    </div>
</div>
</body>
</html>
