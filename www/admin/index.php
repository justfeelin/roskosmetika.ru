<?php

try {
    mb_internal_encoding('UTF-8');

    require_once '../../includes/setup.php';

    Template::init((!DEBUG ? COMPRESSED_TEMPLATES . '/' : '') . 'templates_admin');

    Template::add_css('bootstrap.min.css');
    Template::add_css('font-awesome.min.css');
    Template::add_css('admin.css');

    Template::add_script('jquery.min.js');
    Template::add_script('bootstrap.min.js');
    Template::add_script('ckeditor/ckeditor.js');
    Template::add_script('admin/admin_common.js');

    Router::setPath('../../admin_controllers');
    Router::delegate();

    Template::output('layout');
} catch (Exception $e) {
    echo $e->getMessage();
}

/**
 * Функция обратоки ошибок в скрипте, вызываемая опционально
 * Заносит информацию об ошибке в глобальную переменную.
 */
function catch_error($errno, $errstr, $errfile, $errline)
{
    $GLOBALS['main_error'] = array('text' => $errstr, 'line' => $errline);
}
