<?php

try {
    require_once '../includes/setup.php';

    // masterhost sets port as 80 on HTTPS, facebook authorization fails to work
    $_SERVER['SERVER_PORT'] = 443;

    Template::init((!DEBUG ? COMPRESSED_TEMPLATES . '/' : '') . 'templates');

    $user_id = Auth::is_auth();

    //  flag view TM only for professional
    $prof_data = users::tm_prof_backend($user_id);

    define('TM_FOR_PROF', FALSE);
    define('REAL_PROF', $prof_data['real_prof']);

    Template::set('is_auth', $user_id);

    Template::set('canonical', check::canonical_url());

    if ($user_id) Template::set('favourites', Favorites::count_favourites($user_id));

    /**
     * Whether request was done by AJAX call
     */
    define('AJAX_REQUEST', isset($_SERVER['HTTP_AJAX']));

    Template::set('main_menu', cache_item((TM_FOR_PROF ? 'p_main_menu' : 'main_menu') . (CART_PAGE ? '_cart' : ''), function () {
        return Template::get_tpl('main_menu', null, Categories::get_main_menu());
    }, 300));//5 minutes

    Router::setPath('../controllers');
    Router::delegate();

    if (Template::get('brands_html') === null) {
        Template::set('brands_html', Cached::brands_list_html());
    }

    Template::setTrackingEvents();

    if(strpos($_SERVER['REQUEST_URI'], '/product/') === FALSE) {
        if (!isset($_COOKIE['email_ok']) && !isset($_COOKIE['popup_time'])) {
            if (empty($GLOBALS['referral_popup'])) {
                Template::add_script('../assets/popup/js.js');
                Template::add_script('../assets/popup/form-email.js');
                //Template::add_script('../assets/email-popup/js.js');
                Template::add_css('../assets/popup/css.css');
                //Template::add_css('../assets/email-popup/css/style.css');
            }

            //Template::set('email_popup', true);
        }

        if (!empty($GLOBALS['referral_popup'])) {
            Template::add_script('../assets/popup/js.js');
            Template::add_script('../assets/popup/form-email.js');
            Template::add_css('../assets/popup/css.css');
            Template::add_css('../assets/referral-popup/css.css');

            Template::set('referral_popup', true);
        }
    }

    if (!CART_PAGE) {
        Cart::setInfo();
    }

    Template::set('cartInfo', json_encode(Cart::$data));

    Template::set('isDesktop', DeviceCheck::isDesktop());

    Template::set('tm_for_prof', TM_FOR_PROF);

    Template::output('layout');
} catch (Exception $e) {
    echo $e->getMessage();
}
