$(function () {
    $('.popup form').submit(function () {
        var url, value;

        url = this.getAttribute('data-url');

        value = $.trim(document.getElementById('email-popup-input').value);

        if (value !== '' && document.getElementById('email-popup-checkbox').checked) {
            ga('UniversalAnalytics.send', 'event', 'EmailPopup', 'completed');

            //  GUA Main
            ga('MainUATracker.send', 'event', 'EmailPopup', 'completed');

            $.post(
                '/unsubscribe/popup',
                {
                    email: value
                },
                function (o) {
                    if (o) {
                        common
                            .mindbox_action(
                                'identify',
                                'FooterSubscribe',
                                {
                                    identificator: {
                                        provider: 'email',
                                        identity: value
                                    },
                                    data: {
                                        subscriptions: [
                                            {
                                                pointOfContact: 'Email',
                                                isSubscribed: true,
                                                valueByDefault: true
                                            }
                                        ]
                                    }
                                }
                            );

                        common.storage.set('email', value);

                        ga('UniversalAnalytics.send', 'event', 'Subscription', 'subscribe');

                        //  GUA Main
                        ga('MainUATracker.send', 'event', 'Subscription', 'subscribe');
                    }

                    if (window.popupCallback) {
                        popupCallback(o, url);

                        return;
                    }

                    if (o) {
                        if (url) {
                            document.location.href = url;
                        } else {
                            common.closePopup();
                        }
                    }
                },
                'json'
            );
        }

        return false;
    });

    $('#email-popup-checkbox')
        .change(function () {
            $('.popup .yes')
                .toggleClass('disabled', !this.checked);
        });
});
