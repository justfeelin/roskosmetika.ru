$(function () {
    var $popup, showOffset, expireDate, showTime, matches,
        showTO;

    if (common.cartPage) {
        return;
    }

    /**
     * Shows popup
     */
    function showPopup() {
        var dt;

        $popup = $('.popup')
            .show();

        $popup.find('.popup-close, .bg').click(function () {
            ga('UniversalAnalytics.send', 'event', 'EmailPopup', $(this).hasClass('refuse') ? 'refused' : 'false');

            //  GUA Main
            ga('MainUATracker.send', 'event', 'EmailPopup', $(this).hasClass('refuse') ? 'refused' : 'false');

            closePopup();

            return false;
        });

        dt = new Date();

        dt.setDate(dt.getDate() + 7);

        document.cookie = "popup_time=1" +
            "; expires=" + dt.toGMTString() +
            "; path=/;";

        common.closePopup = closePopup;

        ga('UniversalAnalytics.send', 'event', 'EmailPopup', 'viewed');

        //  GUA Main
        ga('MainUATracker.send', 'event', 'EmailPopup', 'viewed');

        if (showTO) {
            clearTimeout(showTO);
            showTO = 0;
        }

        common.showPopup = null;
    }

    /**
     * Initiates popup show timeout
     *
     * @param {Number} seconds Time offset in seconds
     */
    function initShowPopup(seconds) {
        showTO = setTimeout(
            showPopup,
            seconds > 0 ? seconds * 1000 : 0
        );
    }

    /**
     * Closes popup
     */
    function closePopup() {
        $popup.fadeOut(250, function () {
            $popup.remove();
        });
    }

    showTime = null;

    showOffset = document.cookie.indexOf('show_popup');

    if (showOffset === -1) {
        expireDate = new Date();
        expireDate.setTime(expireDate.getTime() + 60 * 1000);

        document.cookie = "show_popup=" + expireDate.getTime() +
            "; expires=" + expireDate.toGMTString() +
            "; path=/";

        showTime = document.getElementById('referral-popup') ? 5 : 60;
    } else {
        matches = /show_popup=(\d+);/.exec(document.cookie.substr(showOffset));

        if (matches) {
            showTime = parseInt((parseInt(matches[1], 10) - (new Date()).getTime()) / 1000, 10);
        }
    }

    common
        .showPopup = showPopup;

    if (showTime !== null) {
        initShowPopup(showTime);
    }
});
