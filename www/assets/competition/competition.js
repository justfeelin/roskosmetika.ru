$('.competition #phone').on('input', function () {
    $('.competition #phone').mask(
        '+7? (999) 99999999',
        {
            autoclear: false
        });
});



$('.reg_comp').on('click', function (e) {
    var btn = $(this);
    //btn.html('Регистрируем<span class="think"></span>');

    var e_mail = $(this).parents('.competition').find('input[name=e_mail]').val();
    var phone = $(this).parents('.competition').find('input[name=phone]').val();
    var name = $(this).parents('.competition').find('input[name=name]').val();
    var pattern = /^[._a-zA-Z\d-]+@[.a-zA-Z\d-]+.[a-zA-Z]{2,6}$/i;
    var pattern_phone = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/i;

    var client_id = $('.competition form').attr('comp_user');
    var comp_id = $('.competition form').attr('comp_id');


    $("p.form_error").remove();
    if (!pattern.test(e_mail)) {
        $('.form-group').removeClass('has-error');
        $('input[name=e_mail]').after('<p class="form_error">Введён некорректный адрес</p>');
        $('input[name=e_mail]').parent().addClass('has-error');
        return false;
    } else {
        $('input[name=e_mail]').parent().removeClass('has-error');
    }

    if (!pattern_phone.test(phone)) {
        $('.form-group').removeClass('has-error');
        $('input[name=phone]').after('<p class="form_error">Введён некорректный телефон</p>');
        $('input[name=phone]').parent().addClass('has-error');
        return false;
    } else {
        $('input[name=phone]').parent().removeClass('has-error');
    }

    if (name === '') {
        $('input[name=name]').after('<p class="form_error">Введите имя</p>');
        $('input[name=name]').parent().addClass('has-error');
        return false;
    } else {
        $('input[name=name]').parent().removeClass('has-error');
    }

    $.ajax({
        url: '/competition/subscribe',
        timeout: 6000,
        type: 'POST',
        data: {
            email: e_mail,
            phone: phone,
            name: name,
            client_id: client_id,
            comp_id: comp_id
        },
        dataType: 'json'
    }).done(function (result) {
        //btn.html('Учавствовать');
        btn.html('Регистрируем<span class="think"></span>');
        if (result.success) {
            $('.competition')
                .fadeOut(function () {
                    $.alert('<p>Вы успешно зарегистрировались!</p>', 'Участие в акции', true);
                });

        } else {
            $('.form-group').removeClass('has-error');

            if (result.email_error !== 0) {
                $('input[name=e_mail]').after('<p class="form_error">' + result.email_error + '</p>');
                $('input[name=e_mail]').parent().addClass('has-error');
            }

            if (result.all_error !== 0) {
                $('input[name=e_mail]').after('<p class="form_error">' + result.all_error + '</p>');
                $('input[name=e_mail]').parent().addClass('has-error');

            }
            $('.competition')
                .fadeOut(function () {
                    $.alert('<p>Ошибка регистрации!</p>', 'Участие в акции', true);
                });
        }
    }).fail(function () {
        $.alert('<p>Ошибка регистрации!</p>', 'Участие в акции', true);
    });
    return false;

});
