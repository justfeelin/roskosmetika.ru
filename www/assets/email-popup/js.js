$(function () {
    var date;

    if (common.storage.get('email')) {
        date = new Date();

        date.setDate(date.getDate() + 10000);

        document.cookie = 'email_ok=1; expires=' + date.toUTCString() + '; path=/';
    } else {
        window.popupCallback = function (o) {
            var seconds, intervalID;

            if (o) {
                $('#email-popup .fright')
                    .fadeOut();

                $('#email-popup-success')
                    .fadeIn();

                document
                    .getElementById('popup-discount-bar')
                    .style
                    .display = '';

                seconds = 10;

                intervalID = setInterval(
                    function () {
                        var timeElement;

                        timeElement =
                            document
                                .getElementById('email-popup-success-time');

                        --seconds;

                        if (seconds === 0 || !timeElement) {
                            clearInterval(intervalID);

                            $('.popup')
                                .fadeOut();

                            return;
                        }

                        timeElement
                            .innerHTML = seconds;
                    },
                    1000
                );
            } else {
                $.alert('Этот адрес уже зарегистрирован');
            }
        };
    }

    $('#check').click(function () {
        var checkbox;
        checkbox = $('#email-popup-checkbox');

        checkbox[0].checked=!checkbox[0].checked;

        checkbox.trigger('change')

        $(this).toggleClass('checked');
        return false;
    })

});
