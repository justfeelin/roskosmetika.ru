#!/bin/bash
echo "compiling styles... "
lessc ./eskimobi.less > ./eskimobi.css
lessc -x ./eskimobi.less > ./eskimobi.min.css
uglifyjs eskimobi.js > eskimobi.min.js
echo "done!"
