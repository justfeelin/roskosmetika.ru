// Новое по каталогу
$(function () {
    $().day_item();
    $().bootstrap_file();
});


/**
 * Add or edit day item
 */

jQuery.fn.day_item = function () {

    jQuery(document).on("click", '.da_btn', function () {

        var form = jQuery(this).parents(".da_row");

        var dat = {
            'begin': form.find(".da_begin").attr("real-value"),
            'end': form.find(".da_end").attr("real-value"),
            'prod_id': form.find(".da_prod_id").val(),
            'sale': form.find(".da_sale").val(),
            'tag': form.find(".da_tag").val()
        };

        $.ajax({

            url: '/admin/day_item/save',
            timeout: 6000,
            type: 'POST',
            data: dat,
            dataType: 'json',
            beforeSend: function (XmlHttp) {
                XmlHttp.setRequestHeader('Ajax', '1');
                form.find(".da_btn").removeClass('btn-primary');
                form.find(".da_btn").addClass('btn-success');
                form.find(".da_btn").html('.....');
            },
            success: function (data) {

                form.find(".da_btn").removeClass('btn-success');
                form.find(".da_btn").addClass('btn-primary');
                form.find(".da_btn").html('Save');

                if (data.errors) {

                    jQuery('.da_row .form-group').removeClass('has-error');

                    if (!jQuery.isEmptyObject(data.errors.prod_id)) {
                        form.find('.da_prod_id').parent().addClass('has-error');
                    }

                    if (!jQuery.isEmptyObject(data.errors.sale)) {
                        form.find('.da_sale').parent().addClass('has-error');
                    }

                }
            },
            error: function () {

                form.find(".da_btn").removeClass('btn-success');
                form.find(".da_btn").addClass('btn-primary');
                form.find(".da_btn").html('Save');
                alert('Ошибка. Что-то у Вас с кармой. Надо попозже попробовать.');
            }
        });


    });

};

jQuery.fn.bootstrap_file = function () {

    jQuery('.input-file').change(function () {
        jQuery('.subfile').val(jQuery(this).val());
    });

};


$(document).ready(function () {

    //установки для редактирования страниц
    if ($('.choise').val() == 'limit')
        $('.limit_time').show();
    else {
        $('.limit_time').hide();
    }

    $('.choise').change(function () {
        var value = $(this).val();

        if (value == 'limit')
            $('.limit_time').show();
        else {
            $('.limit_time').hide();
        }
    })
    //установки для редактирования баннеров

    if ($('.add_under').attr('checked') == true)
        $('.under_pic').show();
    else {
        $('.under_pic').hide();
    }

    $('.add_under').change(function () {
        var value = $(this).attr('checked');
        if (value == true)
            $('.under_pic').show();
        else {
            $('.under_pic').hide();
        }
    })

    //установки ckeditor & ckfinder
    if ($("#editor").length) {
        CKEDITOR.replace('editor', {});

    }

    $('.btn-confirm')
        .click(function () {
            if (!confirm(this.getAttribute('data-confirm') || '!')) {
                return false;
            }
        });
});