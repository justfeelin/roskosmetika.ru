$(function () {
    /**
     * Checks date filter type for trademarks filter, toggles date filters
     */
    function checkDateFilter() {
        var byTm, byAdded;

        byTm = document.getElementById('type-tm').checked;
        byAdded = $('.by-date-chb.added')[0].checked;

        $('.date-filter.added')
            .toggle(!byTm || byAdded);

        $('.date-filter.delivery')
            .toggle(!byTm || !byAdded);
    }

    /**
     * Checks type filter, toggles date blocks
     */
    function checkTypeFilter() {
        var byTm;

        byTm = document.getElementById('type-tm').checked;

        $('#by-date-filter')
            .toggle(byTm);

        checkDateFilter();
    }

    /**
     * Сhecking the date interval*
     */
    function checkDateInterval() {

        var Date1 = new Date ($('#date-added-from').val());
        var Date2 = new Date ($('#date-added-to').val());

        var Days = Math.floor(((Date2.getTime() - Date1.getTime())/(1000*60*60*24))+1);
        if(Days>31){
            $('.date-filter .form_error').remove();
            $('#date-added-to').parents('.dates-box').after('<p class="form_error" style="color: #f63e3c">Промежуток времени не может превышать 31 день</p>');
            $('#date-added-to').css('color', '#f63e3c');
            $('#date-added-from').css('color', '#f63e3c');
            $('input.btn-primary').css('display', 'none');
        }else if(Days<0){
            $('.date-filter .form_error').remove();
            $('#date-added-to').parents('.dates-box').after('<p class="form_error" style="color: #f63e3c">Первая дата должна быть меньше второй!</p>');
            $('#date-added-to').css('color', '#f63e3c');
            $('#date-added-from').css('color', '#f63e3c');
            $('input.btn-primary').css('display', 'none');
        }
        else{
            $('input.btn-primary').css('display', 'inline-block');
            $('.date-filter .form_error').remove();
            $('#date-added-to').css('color', 'rgb(85, 85, 85)');
            $('#date-added-from').css('color', 'rgb(85, 85, 85)');
        }
    }


    $('.date-input')
        .datepicker({
            dateFormat: 'yy-mm-dd'
        });

    $('.clear-date-btn')
        .click(function () {
            $(this)
                .closest('.dates-box')
                .find('.date-input')
                .val('');

            return false;
        });

    $('#stat-form :radio')
        .change(function () {
            if (this.checked) {
                $('#date-added-to').css('color', 'rgb(85, 85, 85)');
                $('#date-added-from').css('color', 'rgb(85, 85, 85)');
                $('input.btn-primary').css('display', 'inline-block');
                $('.date-filter .form_error').remove();
                $('#delivery-box')
                    .css(
                        'opacity',
                        $(this).hasClass('no-delivery') ? 0 : 1
                    );

                $('#day_to')
                    .css(
                        'opacity',
                        $(this).hasClass('one_day') ? 0 : 1
                    );

                if ($("input[value='sku_sales']").prop("checked")) {
                    if ($('#date-added-from').val() && $('#date-added-to').val()) {
                        checkDateInterval();
                    }
                }
            }
        })
        .filter(':checked')
        .triggerHandler('change');

    $('.by-date-chb')
        .change(checkDateFilter);

    $('[name="stat_type"]')
        .change(function () {
            checkTypeFilter();
        });

    $('#date-added-from').change(function () {
        if($("input[value='sku_sales']").prop("checked")) {
            checkDateInterval();
        }
    });

    $('#date-added-to').change(function () {
        if($("input[value='sku_sales']").prop("checked")) {
            checkDateInterval();
        }
    });

    checkTypeFilter();
});
