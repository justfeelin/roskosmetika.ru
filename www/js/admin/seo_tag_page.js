$(function () {
    $('#categories-list')
        .select2();

    $('.parent-type-radio')
        .change(function () {
            $('#parent-tag-select')
                .toggle(!this.value);

            $('#parent-type-page-select')
                .toggle(!!this.value);
        });
});
