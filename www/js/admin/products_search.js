$(function () {
    var copyFromtags;

    copyFromtags = false;

    $('#products-search-name-tag')
        .select2()
        .on('change', function (e) {
            $('#products-search-name')
                .val(
                    $.trim(
                        e
                            .added
                            .text
                    )
                )
                .triggerHandler('change');
        });

    $('#products-search')
        .on('click', '.results-box .check-all', function () {
            $(this)
                .closest('.products-box')
                .find('.product-chb')
                .prop('checked', this.checked);
        })
        .find('.search-form')
        .submit(function () {
            var copyFromTagsElement;

            copyFromTagsElement = document
                .getElementById('do-copy-tags');

            if (copyFromTagsElement) {
                copyFromTagsElement
                    .value = copyFromtags ? 1 : '';
            }

            $.post(
                document.location.href,
                $(this).serialize(),
                function (o) {
                    if (o.hasOwnProperty('copied')) {
                        alert(o.copied ? 'Товары скопированы!' : 'Необходимо выбрать теги');
                    } else {
                        $('#products-search .results-box')
                            .html(o.html);
                    }
                },
                'json'
            );

            copyFromtags = false;

            return false;
        });

    $('#products-search-show-added')
        .click(function () {
            $.post(
                document.location.href,
                {
                    show_added: 1
                },
                function (o) {
                    $('#products-search .results-box')
                        .html(o.html);
                },
                'json'
            );

            return false;
        });

    $('#copy-from-tags-btn')
        .click(function () {
            if (!confirm('Скопировать товары?')) {
                return false;
            }

            copyFromtags = true;
        });
});
