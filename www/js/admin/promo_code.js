$(function () {
    var chars,
        $endDateInput;

    chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

    $('#action-tabs [data-toggle]')
        .on('show.bs.tab', function (event) {
            var cheapestDiscount;

            $('#action-type-id')
                .val(event.target.getAttribute('data-action-type-id'));

            cheapestDiscount = !!event.target.getAttribute('data-cheapest-discount');

            $('#common-promo-box')
                .toggle(!cheapestDiscount);

            $('#promo-code-discount')
                .prop('required', !cheapestDiscount);
        });

    $('#generate-promo-btn')
        .click(function () {
            var length, code, i;

            length = parseInt(document.getElementById('generate-promo-length').value, 10) || 6;

            code = '';

            for (i = 0; i < length; ++i) {
                code += chars[parseInt(Math.random() * chars.length, 10)];
            }

            document.getElementById('promo-code-code').value = code;

            return false;
        });

    $('.rouble-radio')
        .change(function () {
            $('#certificate-box')
                .toggle(!!this.value);

            $('#filters-box')
                .toggle(!this.value || !document.getElementById('certificate-input').checked);
        });

    $('#certificate-input')
        .change(function () {
            $('#filters-box')
                .toggle(!this.checked);
        });

    $endDateInput = $('#promo-code-end-date');

    $endDateInput
        .datepicker({
            dateFormat: 'yy-mm-dd 00:00:00',
            defaultDate: $endDateInput.val()
        });

    $('#cheapest-discount-all-chb')
        .change(function () {
            $('#cheapest-discount-product-ids')
                .toggle(!this.checked);
        });
});
