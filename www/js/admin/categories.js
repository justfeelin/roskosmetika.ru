$(function () {
    "use strict";

    var URL;

    /**
     * Shows item
     *
     * @param {Number} id Item ID
     */
    function showItem(id) {
        var $parent, level;

        $parent = $('.cat_link[data-id="' + id + '"]').closest('.parent');

        level = parseInt($parent.attr('data-level'), 10);

        if (level === 1) {
            $('.level_2_item, .level_2_item .cat_link').removeClass('unactive');
        }

        $('.level_' + level + '_item')
            .addClass('unactive')
                .filter('[data-id="' + id + '"]')
                .removeClass('unactive')
            .end()
                .find('.cat_link')
                .addClass('unactive')
                    .filter('[data-id="' + id + '"]')
                    .removeClass('unactive');

        $('.level_2, .level_3').hide();

        $('.parent[data-id="' + id + '"]').show();

        if (level === 2) {
            $parent.show();
        }
    }

    /**
     * Updates location hash
     *
     * @param {Number} id Clicked item ID
     */
    function updateUrl(id) {
        var level, i;

        level = parseInt($('[data-id="' + id + '"').closest('.parent').attr('data-level'), 10);

        URL[level - 1] = id;

        if (level === 1) {
            URL[1] = 0;
        }

        document.location.hash = '';

        for (i = 0; i < 2; ++i) {
            if (URL[i]) {
                document.location.hash += (i > 0 ? '/' : '') + URL[i];
            } else {
                break;
            }
        }
    }

    /**
     * Parses location hash & shows selected categories
     */
    function parseUrl() {
        var i, urls;

        urls = document.location.hash.substr(1).split('/');

        if (urls.length === 0) {
            return;
        }

        for (i = 0; i < urls.length; ++i) {
            URL[i] = urls[i];

            if (urls[i]) {
                showItem(urls[i]);
            }
        }
    }

    /**
     * Shows error message
     */
    function showError() {
        alert('Возникла ошибка! =(');
    }

    URL = [
        0,
        0
    ];

    parseUrl();

    $('.download').click(function () {
        var value;

        value = this.getAttribute('value');

        document.location.href = value;
        window.status = value;
    });

    $('#categories .cat_link').click(function () {
        updateUrl(this.getAttribute('data-id'));

        parseUrl();

        return false;
    });

    $('.new_cat').click(function () {
        var id, $subcat, $form, emptySubcat;

        id = this.getAttribute('data-id') || 0;
        $form = $('#add-form-' + $(this).closest('.parent').attr('data-id'));

        $subcat = $form.find('.subcat-id');

        if ($subcat.length !== 0) {
            $subcat.val(id);
        }

        emptySubcat = $subcat.length === 0 || $subcat.val() === '0';

        $form.find('.new-subcat').toggle(emptySubcat).find('input').prop('required', emptySubcat);

        $form.dialog({
            title: this.getAttribute('title'),
            modal: true
        });

        return false;
    });

    $('.add-form')
        .submit(function () {
            $.post(
                '/admin/categories/add_category',
                $(this).serialize(),
               function (result) {
                   if (result) {
                       document.location.reload(true);
                   } else {
                       showError();
                   }
                },
                'json'
            )
                .error(showError);

            return false;
        })
        .find('.subcat-id').change(function () {
            $(this).closest('form').find('.new-subcat').toggle(this.options[this.selectedIndex].value === '0').find('input').prop('required', this.options[this.selectedIndex].value === '0');
        });
});