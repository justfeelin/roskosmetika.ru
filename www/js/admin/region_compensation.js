$(function () {
    $('#add-compensation')
        .click(function () {
            $('#compensation-tmpl .compensation-holder')
                .clone()
                .show()
                .appendTo('#compensations');

            return false;
        });

    $('#compensations')
        .on('click', '.delete-compensation', function () {
            var id, _this;

            id = this.getAttribute('data-id');

            if (id) {
                if (!confirm('Удалить компенсацию?')) {
                    return false;
                }

                _this = this;

                $.post(
                    document.location.href,
                    {
                        delete_compensation: id
                    },
                    function (o) {
                        if (o) {
                            $(_this)
                                .closest('.compensation-holder')
                                .remove();
                        } else {
                            alert('Произошла ошибка при удалении компенсации');
                        }
                    },
                    'json'
                );
            } else {
                $(this)
                    .closest('.compensation-holder')
                    .remove();
            }

            return false;
        });

    $(document)
        .on('change', '.free-shipping-chb', function () {
            $(this)
                .closest('.compensation-box')
                .find('.compensation-input')
                .val('')
                .prop('readonly', this.checked);
        });
});
