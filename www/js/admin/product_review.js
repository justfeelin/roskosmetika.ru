$(function () {
    var $date = $("#reviewDate");

    $date.datepicker({
        dateFormat: "yy-mm-dd",
        defaultDate: $date.val()
    });
});
