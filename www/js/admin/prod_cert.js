$(function () {
    $('#add-image')
        .click(function () {
            $('#image-template .cert-image')
                .clone()
                .find('.img-file')
                .hide()
                .end()
                .show()
                .appendTo($('#cert-images'));

            return false;
        });

    $('#cert-images')
        .on('click', '.btn-delete', function () {
            var id, _this;

            id = this.getAttribute('data-id');

            if (id) {
                if (!confirm('Удалить изображение?')) {
                    return false;
                }

                _this = this;

                $.post(
                    document.location.href,
                    {
                        delete_image: id
                    },
                    function (o) {
                        if (o) {
                            $(_this)
                                .closest('.cert-image')
                                .remove();
                        } else {
                            alert('Возникла ошибка при удалении изображения');
                        }
                    },
                    'json'
                );
            } else {
                $(this)
                    .closest('.cert-image')
                    .remove();
            }
        });

    $('#data-end-date')
        .datepicker({
            dateFormat: 'yy-mm-dd'
        });

    $('#clear-end-date')
        .click(function () {
            document
                .getElementById('data-end-date')
                .value = '';

            return false;
        })
});
