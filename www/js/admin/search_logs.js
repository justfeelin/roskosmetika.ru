$(function () {
    $(".logs-date").each(function () {
        var $input;

        $input = $(this);

        $input.datepicker({
            dateFormat: "yy-mm-dd",
            defaultDate: $input.val()
        });
    });

    $("#reset-btn").click(function () {
        $("#logs-form .form-control").val("");

        return false;
    });
});
