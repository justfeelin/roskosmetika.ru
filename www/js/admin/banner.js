$(function () {
    var coords, size,
        copyArea, defaultValue;

    copyArea = document.getElementById('copy-area');

    if (copyArea) {
        defaultValue = copyArea
            .value;

        /**
         * Updates copy area coordinates & size input value
         */
        function updateCopyArea() {
            copyArea
                .value = coords.join(',') +
                    ',' +
                    size.join(',');
        }

        $('#copy-area-enabled')
            .change(function () {
                $('#banner-copy-area, #copy-text-box')
                    .toggle(this.checked);
            });

        $('#banner-copy-area')
            .draggable({
                containment: 'parent',
                stop: function (event, ui) {
                    coords = [
                        ui
                            .position
                            .left,
                        ui
                            .position
                            .top
                    ];

                    updateCopyArea();
                }
            })
            .resizable({
                containment: 'parent',
                stop: function (event, ui) {
                    size = [
                        ui
                            .size
                            .width,
                        ui
                            .size
                            .height
                    ];

                    updateCopyArea();
                }
            });


        if (defaultValue) {
            defaultValue = defaultValue
                .split(',');

            coords = defaultValue
                .slice(0, 1);

            size = defaultValue
                .slice(2, 3);
        } else {
            coords = [0, 0];
            size = [200, 100];
        }
    }

    $('#add-date-btn')
        .click(function () {
            $('#date-tmpl-box .banner-date')
                .clone()
                .show()
                .find('.dt-input')
                .datepicker({
                    dateFormat: 'yy-mm-dd'
                })
                .end()
                .appendTo('#dates-box');

            return false;
        });

    $('#dates-box')
        .on('click', '.delete-date-btn', function () {
            if (confirm('Удалить дату?')) {
                $(this)
                    .closest('.banner-date')
                    .remove();
            }

            return false;
        })
        .find('.dt-input')
        .each(function () {
            $(this)
                .datepicker({
                    dateFormat: 'yy-mm-dd',
                    defaultDate: this.value
                });
        });
});
