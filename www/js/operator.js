function Operator(conf){
    // Varible declaration.
    this.animation_time = 600;

    conf = conf || {};

    this.borderDefault   = conf.borderDefault   || 'gray';
    this.borderOk        = conf.borderOk        || '#D7DF23';
    this.borderError     = conf.borderError     || 'red';

    this.nameWrong       = conf.nameWrong       || 'Введите корректное имя';
    this.nameEmpty       = conf.nameEmpty       || 'Представьтесь';
    this.contactsWrong   = conf.contactsWrong   || 'Введите корректный телефон или email';
    this.contactsEmpty   = conf.contactsEmpty   || 'Введите Ваш телефон или email';
    this.msgEmpty        = conf.msgEmpty        || 'Ваш вопрос?';

    this.headerColor     = conf.headerColor     || '#D7DF23';
    this.headerText      = conf.headerText      || 'Задать вопрос';
    this.headerTextColor = conf.headerTextColor || '#000000'
    this.text            = conf.text            || 'Мы с удовольствием ответим на ваши вопросы.';
    this.autoPush        = conf.autoPush        || 0;
    this.delayTime       = conf.delayTime       || 2000;

    this.wnd = '\
<div id="op_wnd" style="display:none;z-index:2;position:fixed;-position:absolute;left:50%;margin-left:-200px;width:400px;font-family: Tahoma,Arial,sans-serif;font-size:10pt;border: 1px solid #dedede;color:black">\n\
    <div style="padding:16px;background:white">\n\
        <form id="op_form" action="" method="POST" style="margin:0">\n\
        <table cellpadding="0" cellspacing="0" width="360" border="0">\n\
            <tr><td colspan="2" style="padding-bottom:14px;border-bottom:none;font-family: Tahoma,Arial,sans-serif;font-size:10pt!important;">'+this.text+'</td></tr>\n\
            <tr><td style="border-bottom:none;font-family: Tahoma,Arial,sans-serif;font-size:10pt!important;" width="135" valign="top">Имя:</td><td width="225" style="border-bottom:none;"><input id="op_form_name" style="margin: 0 0 14px 0;width:100%;border:1px solid gray"></input></td></tr>\n\
            <tr><td style="border-bottom:none;font-family: Tahoma,Arial,sans-serif;font-size:10pt!important;" valign="top">Телефон или e-mail:</td><td style="border-bottom:none;"><input id="op_form_contacts" style="margin: 0 0 14px 0;width:100%;border:1px solid gray"></input></td></tr>\n\
            <tr><td style="border-bottom:none;font-family: Tahoma,Arial,sans-serif;font-size:10pt!important;" valign="top">Вопрос:</td><td style="border-bottom:none;"><textarea id="op_form_msg" style="margin: 0 0 14px 0;width:100%;height:160px;resize:none;border:1px solid gray"></textarea></td></tr>\n\
            <tr><td style="border-bottom:none;">&nbsp;</td><td style="border-bottom:none;"><input type="submit" id="op_form_submit" value="Отправить"></td></tr>\n\
        </table>\n\
        </form>\n\
    </div>\n\
    <div class="pngFix" style="-display:none;position:absolute;top:-20px;left:-20px;height:20px;width:440px;background:url(\'http://www.roskosmetika.ru/js/img/op.png\') -41px 1px no-repeat"></div>\n\
    <div class="pngFix" style="-display:none;position:absolute;top:0;left:-20px;width:20px;height:100%;margin:0;background:url(\'http://www.roskosmetika.ru/js/img/op.png\') 5px 0 repeat-y"></div>\n\
    <div class="pngFix" style="-display:none;position:absolute;top:0;right:-20px;width:20px; height: 100%;background:url(\'http://www.roskosmetika.ru/js/img/op.png\') -23px 0 repeat-y"></div>\n\
    <div class="pngFix" style="-display:none;position:absolute;bottom:-20px;left:-20px;width:440px;height:20px;background:url(\'http://www.roskosmetika.ru/js/img/op.png\') -40px -20px no-repeat"></div>\n\
    <div class="pngFix" id="op_close" style="position:absolute;top:-12px;right:-10px;height:26px;width:26px;background:url(\'http://www.roskosmetika.ru/js/img/op.png\') -480px -8px no-repeat;cursor:pointer"></div>\n\
</div>';
    this.label = '<div id="op_label" style="z-index:2;position:fixed;-position:absolute;bottom:0;left:50%;padding:4px 18px;background-color:'+this.headerColor+';text-align:center;font-size:11pt;color:'+this.headerTextColor+';cursor:pointer;text-decoration:underline">'+this.headerText+'</div>';

    $('body').append(this.wnd);
    $('body').append(this.label);

    if (document.all && !document.querySelector) {
        $('body').prepend('<link rel="stylesheet" href="http://www.roskosmetika.ru/js/css/op_common.css">');
    }

    this.label = $('#op_label');
    this.wnd = $('#op_wnd');
    this.close = $('#op_close');
    this.form = $('#op_form');
    this.form_name = $('#op_form_name');
    this.form_contacts = $('#op_form_contacts');
    this.form_msg = $('#op_form_msg');
    this.form_submit = $('#op_form_submit');

    this.label.css('margin-left', '-' + $('#op_label').outerWidth()/2 + 'px');

    // Auto push
    var page_count = this.GetCookie('page_count') || 1;
    var msg_sent = this.GetCookie('msg_sent') || 0;

    var self = this;
    function showWnd() {
        self.ShowWindow();
        self.SetCookie('page_count', ++page_count, null, '/');
    }
    if (this.autoPush == page_count && !msg_sent) {
        setTimeout(showWnd, this.delayTime);
    } else {
        this.SetCookie('page_count', ++page_count, null, '/');
    }

    //  Bind events hendler
    this.label.click(this, this.ShowWindow);
    this.close.click(this, this.HideWindow);
    this.form_name.focusout(this, this.CheckName);
    this.form_contacts.focusout(this, this.CheckContacts);
    this.form_msg.focusout(this, this.CheckMsg)
    this.form_submit.click(this, function(e){
        e.preventDefault();

        e.data.CheckName(e);
        e.data.CheckContacts(e);
        e.data.CheckMsg(e);

        // Send request to server
        if (!$('.op_warning').length) {
            $.ajax({
                jsonp: 'callback',
                jsonpCallback: 'reply',
                dataType: 'jsonp',
                url: 'http://www.roskosmetika.ru/lo',
                data: 'name='+$('#op_form_name').val()+'&contacts='+$('#op_form_contacts').val()+'&msg='+$('#op_form_msg').val(),
                success: function(result) {
                    if (result.success == 1){
                        alert('Сообщение отравлено. В ближайшее время мы Вам ответим.');
                        e.data.SetCookie('msg_sent', 1, null, '/');
                        e.data.HideWindow();
                    }
                    else{
                        if (result.name) {
                            $('#op_form_name').val(result.name);
                        }
                        if (result.contacts) {
                            $('#op_form_contacts').val(result.contacts);
                        }
                        if (result.msg) {
                            $('#op_form_msg').val(result.msg);
                        }
                        alert('Введени некорректные данные');
                    }
                }
            });
            return 1;
        }
        // Don't sent request
        else {
            return 0;
        }
    });

    $(window).resize(this, function(e){
        e.data.wnd.css('top', window.innerHeight/2 - e.data.wnd.outerHeight()/2 + 'px');
    });
}
/*
 * Show window and hide label.
 */
Operator.prototype.ShowWindow = function(e) {
    e = e || {};
    e = e.data || this;
    if (document.all && window.XMLHttpRequest){
        e.wnd.css('top', document.documentElement.clientHeight/2 - e.wnd.outerHeight()/2 + 'px');
    }else {
        e.wnd.css('top', window.innerHeight/2 - e.wnd.outerHeight()/2 + 'px');
    }
    e.wnd.fadeIn(e.animation_time);
    e.form_name.focus();
    e.label.slideUp(e.animation_time);
}
/*
 * Hide window and show label.
 */
Operator.prototype.HideWindow = function(e) {
    e = e || {};
    e = e.data || this;
    e.wnd.fadeOut(e.animation_time);
    e.label.slideDown(e.animation_time);
}
/*
 * Show warning for element whit text.
 */
Operator.prototype.ShowWarning = function(element, text) {
    var pointer_end = '<div class="pngFix" style="position:absolute;top:0;right: -28px;width:28px;height: 48px;background: url(\'http://www.roskosmetika.ru/js/img/op.png\') -925px -0px no-repeat; ">';
    var pointer = '<div class="op_warning pngFix" style="white-space:nowrap;z-index:4;position:absolute;top:' + (element.position().top + (element.outerHeight()/2 - 19)) + 'px;left:' + (element.outerWidth() + element.position().left) + 'px;height:28px;padding:10px 4px 0px 35px;font-family: Tahoma,sans-serif;font-size:10pt;color:white;background: url(\'http://www.roskosmetika.ru/js/img/op.png\') -510px -0px no-repeat;">'+text+pointer_end+'</div>';

    element.css('border-color', this.borderError);
    if (element.siblings('.op_warning').length){
        element.siblings('.op_warning').html(text+pointer_end);
    }
    else {
        $(element).after(pointer);
    }
}
/*
 * Hide warnign for element.
 */
Operator.prototype.HideWarning = function(element) {
    element.siblings('.op_warning').hide().remove();
    element.css('border-color', this.borderOk);
};
/*
 * Check name. Hide or show warning.
 */
Operator.prototype.CheckName = function(e){
    if (!e.data.form_name.val()){
        e.data.ShowWarning(e.data.form_name, e.data.nameEmpty);
    }
    else if (!/^[\w\d\sа-яё-]+$/i.test(e.data.form_name.val())) {
        e.data.ShowWarning(e.data.form_name, e.data.nameWrong);
    }
    else {
        e.data.HideWarning(e.data.form_name);
    }
}
/*
 * Check contacts and hide or show warning.
 */
Operator.prototype.CheckContacts = function(e) {
    if (!e.data.form_contacts.val()){
        e.data.ShowWarning(e.data.form_contacts, e.data.contactsEmpty);
    }
    else if (!(/^[\d\s-()+]{7,}$/.test(e.data.form_contacts.val()) || /^[\._a-z\d-]+@[\.a-z\d-]+\.[a-z]{2,6}$/.test(e.data.form_contacts.val()))) {
        e.data.ShowWarning(e.data.form_contacts, e.data.contactsWrong);
    }
    else {
        e.data.HideWarning(e.data.form_contacts);
    }
}
/*
 * Check message and show or hide warning.
 */
Operator.prototype.CheckMsg = function(e) {
    if(!e.data.form_msg.val()) {
        e.data.ShowWarning(e.data.form_msg, e.data.msgEmpty);
    }
    else {
        e.data.HideWarning(e.data.form_msg);
    }
}
/*
 * Set cookies.
 */
Operator.prototype.SetCookie = function(name, value, expires, path, domain, secure) {
    document.cookie = name + "=" + escape(value) +
        ((expires) ? "; expires=" + expires : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
}
/**
 * Get cookies.
 */
Operator.prototype.GetCookie = function(name) {
    var cookie = " " + document.cookie;
    var search = " " + name + "=";
    var setStr = null;
    var offset = 0;
    var end = 0;
    if (cookie.length > 0) {
        offset = cookie.indexOf(search);
        if (offset != -1) {
            offset += search.length;
            end = cookie.indexOf(";", offset)
            if (end == -1) {
                end = cookie.length;
            }
            setStr = unescape(cookie.substring(offset, end));
        }
    }
    return(setStr);
}
